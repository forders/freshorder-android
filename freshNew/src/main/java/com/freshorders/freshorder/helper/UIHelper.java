package com.freshorders.freshorder.helper;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.http.Request;
import com.freshorders.freshorder.http.Response;
import com.freshorders.freshorder.http.VolleySingleton;

public class UIHelper {
    private IDBHelperProgressDialog helperProgressDialog;
    private Context mContext;
    private DatabaseHandler databaseHandler;

    public interface IDBHelperProgressDialog {
        void showDialog(final String msg);

        void closeDialog();
    }

    public UIHelper(final Context context, final IDBHelperProgressDialog idbHelperProgressDialog) {
        this.mContext = context;
        this.helperProgressDialog = idbHelperProgressDialog;
    }

    public void execute() {
        databaseHandler = new DatabaseHandler(mContext);
        final Cursor c = databaseHandler.getSyncData();
        Log.d("UIHelper", "execute");
        if (c != null && c.getCount() > 0) {
            Log.d("UIHelper", "cursor not null");
            if (c.moveToFirst()) {

                do {
                    String id = c.getString(0);
                    String url = c.getString(1);
                    String data = c.getString(2);
                    Boolean flag = (c.getInt(c.getColumnIndex("flag")) == 1);
                    if(!flag){
                        Log.d("UIHelper", "url is: " + url);
                        Log.d("UIHelper", "data is: " + data);
                        try {
                            syncOfflineData(url, data, id);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        helperProgressDialog.showDialog("Sync data.."+c.getCount() + "records");
                    }
                } while (c.moveToNext());

            }
            c.close();
        }
    }


    private synchronized void syncOfflineData(final String url,
                                              final String data, final String rowId) throws Exception {
        final Request request = new Request(url, Request.Method.POST, Request.REQUEST_DEFAULT);
        request.setRequestBody(data);
        VolleySingleton.getInstance().connect(request, new VolleySingleton.ResponseListener() {
            @Override
            public void onSuccess(Response res) {
                Log.d("syncOfflineData", "onSuccess status: " + res.response);
                Log.d("syncOfflineData", "onSuccess message: " + res.message);
                databaseHandler.updateSyncTable(Integer.valueOf(rowId), 1);
                helperProgressDialog.closeDialog();
            }

            @Override
            public void onFailure(Response res) {
                Log.d("syncOfflineData", "onFailure message: " + res.message);
                helperProgressDialog.closeDialog();

            }
        });
    }
}
