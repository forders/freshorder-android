package com.freshorders.freshorder.helper;


import android.util.Log;

/**
 * Created by Karthika on 12/03/2018.
 */

public abstract class StoppableRunnable  implements Runnable {

    public volatile boolean mIsStopped = false;

    public abstract void stoppableRun();

    public void run() {
        setStopped(false);
        while(!mIsStopped) {
            Log.e("TEST", "WHILE LOOP: "+mIsStopped );
            stoppableRun();
            stop();
        }
    }

    public boolean isStopped() {

        return mIsStopped;
    }

    public void setStopped(boolean isStop) {
        Log.e("TEST", "setStopped: "+isStop );
        if (mIsStopped != isStop) {
            Log.e("TEST", "setStopped: "+mIsStopped);
            mIsStopped = isStop;
        }
    }

    public void stop() {
        Log.e("TEST", "stop: "+"STOP" );
        setStopped(true);
    }
}
