package com.freshorders.freshorder.helper;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.widget.Button;
import android.widget.Toast;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.ui.DistanceCalculationActivity;
import com.freshorders.freshorder.utils.Constants;
import org.json.JSONArray;
import java.util.Date;


/**
 * Created by Shibu on 23/05/2018.
 */

public class DistanceCalculatorService extends Service
{
    public static final int TWO_MINUTES = 1 * 30 * 1000;
    public static Boolean isRunning = false;
    public static String  strBatchId = "";
    public static DatabaseHandler databaseHandler;
    public LocationManager mLocationManager;
    public LocationUpdaterListener mLocationListener;
    public Location previousBestLocation = null;
    public  Location previousLocation = null;
    public  Location currentLocation = null;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        databaseHandler = new DatabaseHandler(getApplicationContext());
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mLocationListener = new LocationUpdaterListener();
        super.onCreate();
    }

    Handler mHandler = new Handler();
    Runnable mHandlerTask = new Runnable(){
        @Override
        public void run() {
            if (!isRunning) {
                startListening();
            }
            mHandler.postDelayed(mHandlerTask, TWO_MINUTES);
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        strBatchId="BID"+(new Date().getTime());
        mHandlerTask.run();
        return START_STICKY;
    }

    protected void toastDisplay(String msg) {
        Toast toast = Toast.makeText(getApplicationContext(), msg,
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();

    }

    @Override
    public void onDestroy() {
        double dblTotalDistance=0;
        dblTotalDistance = databaseHandler.getTotalDistance(strBatchId);
        toastDisplay("Total Distance:"+dblTotalDistance);
        JSONArray jsonArray=null;
        Cursor cursor=databaseHandler.getSMGeoLogs(strBatchId);
        if(cursor!=null)
        {
            if(cursor.getCount()> 0)
            {
                jsonArray=databaseHandler.getCursorItems(cursor);
            }
        }
        //tracklog(jsonArray);
        if(isNetAvailable()) {
            DistanceCalculationActivity.syncGeoLogs(getBaseContext());
        }
        else{
            toastDisplay("No Internet..Logs saved but not synced.");
        }
        stopListening();
        mHandler.removeCallbacks(mHandlerTask);
        super.onDestroy();
    }

    public boolean isNetAvailable() {
    try {
        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mNetwork = connectionManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        Object result = null;
        if (mWifi.isConnected() || mNetwork.isConnected()) {
            return true;
        } else if (result == null) {

            return false;
        }
    } catch (Exception e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
    }
    return false;
}
    private void startListening() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (mLocationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER))
                mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, mLocationListener);

            if (mLocationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER))
                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mLocationListener);
        }
        isRunning = true;
    }

    private void stopListening() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mLocationManager.removeUpdates(mLocationListener);
        }
        isRunning = false;
    }

    public class LocationUpdaterListener implements LocationListener
    {
        @Override
        public void onLocationChanged(Location location) {
            currentLocation=location;
            if(previousLocation==null)
            {
                previousLocation=currentLocation;
            }
            float floatArValues[]=new float[5];
            Location.distanceBetween(previousLocation.getLatitude(),previousLocation.getLongitude(),currentLocation.getLatitude(),currentLocation.getLongitude(),floatArValues);
            try
            {
                ContentValues jsonObjectLocationTrace=new ContentValues();
                jsonObjectLocationTrace.put("suserid",Constants.USER_ID);
                jsonObjectLocationTrace.put("batchid",strBatchId);
                jsonObjectLocationTrace.put("logtime",new Date().toString());
                jsonObjectLocationTrace.put("geolat",""+currentLocation.getLatitude());
                jsonObjectLocationTrace.put("geolong",""+currentLocation.getLongitude());
                jsonObjectLocationTrace.put("pgeolat",""+previousLocation.getLatitude());
                jsonObjectLocationTrace.put("pgeolong",""+previousLocation.getLongitude());
                jsonObjectLocationTrace.put("status","Active");
                jsonObjectLocationTrace.put("updateddt",new Date().toString());
                jsonObjectLocationTrace.put("distance",""+(int)floatArValues[0]);
                jsonObjectLocationTrace.put("amount","0");
                jsonObjectLocationTrace.put("issynced",0);
                databaseHandler.addSMGeoLogs(jsonObjectLocationTrace);
                DistanceCalculationActivity.setTravelDetails();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            previousLocation=currentLocation;

            if (isBetterLocation(location, previousBestLocation)) {
                previousBestLocation = location;
                try {
                    Log.e("BetterLocation",""+location);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                finally {
                    stopListening();
                }
            }
        }

        @Override
        public void onProviderDisabled(String provider) {
            stopListening();
        }

        @Override
        public void onProviderEnabled(String provider) { }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) { }
    }

    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(), currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /** Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    public void showAlertDialogToast( String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();

        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

}