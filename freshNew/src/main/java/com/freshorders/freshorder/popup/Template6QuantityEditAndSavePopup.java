package com.freshorders.freshorder.popup;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.MerchantOrderDetailDomain;
import com.freshorders.freshorder.domain.MerchantOrderDomainSelected;
import com.freshorders.freshorder.milk.MilkOrderPlace;
import com.freshorders.freshorder.model.Template6Model;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.ProgressAnimate;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.freshorders.freshorder.utils.Constants.DOT;

public class Template6QuantityEditAndSavePopup {

    public interface ParentListener{
        void onPopupDismiss();
    }

    private Context mContext;
    private View popupView;
    private PopupWindow popup;
    private View rootWindow;
    private DatabaseHandler databaseHandler ;
    private Map<Integer, Template6Model> merchantOrderDetailList;
    private ArrayList<MerchantOrderDetailDomain> arraylistMerchantOrderDetailList;
    Map<String, String> pairCheck;
    private Map<Integer, EditText> editViews = new LinkedHashMap<>();

    private View progressOverlay;
    private TextView tvRequirementMissing;
    private Template6QuantityEditAndSavePopup.ParentListener parentListener;

    private boolean milk, fat, snf, temperature, lrvalue;
    private String MILK, FAT, SNF, TEMPERATURE, LRVALUE;
    private String ZERO = "0";
    private TextView tvShowCost;

    public Template6QuantityEditAndSavePopup(Context mContext,
                                             View rootWindow,
                                             Map<String, String> pairCheck,
                                             Map<Integer, Template6Model> merchantOrderDetailList,
                                             ArrayList<MerchantOrderDetailDomain> arraylistMerchantOrderDetailList,
                                             Template6QuantityEditAndSavePopup.ParentListener parentListener) {
        this.mContext = mContext;
        this.rootWindow = rootWindow;
        this.merchantOrderDetailList = merchantOrderDetailList;
        this.arraylistMerchantOrderDetailList = arraylistMerchantOrderDetailList;
        this.pairCheck = pairCheck;
        this.parentListener = parentListener;
        databaseHandler = new DatabaseHandler(mContext);
        createPopup();

    }

    private void createPopup() {
        try {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (layoutInflater != null) {
                popupView = layoutInflater.inflate(R.layout.popup_edit_template6, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showPopup() {
        popup = new PopupWindow(mContext);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setContentView(popupView);
        addWidgetsToListener(popupView);
        // Set popup window animation style.
        popup.setAnimationStyle(R.anim.up_from_bottom);
        popup.setTouchable(true);
        popup.setFocusable(true);
        popup.setOutsideTouchable(false);
        //popup.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        popup.update();
        popup.showAtLocation(rootWindow, Gravity.CENTER, 0, 0);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                parentListener.onPopupDismiss();
            }
        });

    }

    private void showMarqueeText(String message){
        tvRequirementMissing.setText(message);
        tvRequirementMissing.setSelected(true); // should need
        tvShowCost.setText(Constants.STR_DEFAULT_FLOAT_VALUE);
    }

    private void setCost(String cost){
        Double total = Double.parseDouble(cost);
        Double netPrice = (Math.round(total * 100.0) / 100.0);
        String sNetPrice = String.format(Locale.getDefault(), "%.2f", netPrice);
        tvShowCost.setText(sNetPrice);
        tvRequirementMissing.setText(Constants.EMPTY);
    }

    private void setStringCost(String cost){
        tvShowCost.setText(cost);
        tvRequirementMissing.setText(Constants.EMPTY);
    }

    private void addWidgetsToListener(View popupView) {

        tvRequirementMissing = (TextView) popupView.findViewById(R.id.id_milk_pop_marquee);
        tvRequirementMissing.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        tvRequirementMissing.setSelected(true); // should need
        //tvRequirementMissing.setSingleLine(true);

        progressOverlay = popupView.findViewById(R.id.progress_overlay);
        TextView tvMerchantName = (TextView) popupView.findViewById(R.id.id_milk_pop_show_merchant_name);
        tvMerchantName.setText(Constants.Merchantname);

        tvShowCost = (TextView) popupView.findViewById(R.id.id_milk_pop_show_cost);
        final Button btnCancel = (Button) popupView.findViewById(R.id.id_milk_pop_cancel);
        final Button btnViewCost = (Button) popupView.findViewById(R.id.id_milk_pop_view_cost);
        final Button btnSave = (Button) popupView.findViewById(R.id.id_milk_pop_save);

        btnViewCost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean check = requirementCheck();
                if(check){
                    if(calculateCost() > 0) {
                        setCost(String.valueOf(calculateCost()));
                    }else {
                        showMarqueeText(Constants.MILK_INPUT_NOT_MATCHING);
                    }
                }else {
                    showMarqueeText(Constants.MILK_REQUIREMENT_NOTIFICATION);
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean check = requirementCheck();
                if(check){
                    Double total = calculateCost();
                    if(total > 0) {
                        setCost(String.valueOf(total));
                        btnSave.setVisibility(View.INVISIBLE); ////
                        showProgress(); ///
                        // Order place process start
                        new MilkOrderPlace(mContext, databaseHandler, merchantOrderDetailList,
                                arraylistMerchantOrderDetailList, total,
                                new MilkOrderPlace.MilkOrderPlaceListener() {
                                    @Override
                                    public void onOrderPlaced(String msg) {
                                        hideProgress();
                                        showD("Success" , msg);
                                    }

                                    @Override
                                    public void onOrderFailed(String errorMSG) {
                                        hideProgress();
                                        showD("Failed" , errorMSG);
                                    }
                                }).milkDataPack();

                    }else {
                        showMarqueeText(Constants.MILK_INPUT_NOT_MATCHING);
                    }
                }else {
                    showMarqueeText(Constants.MILK_REQUIREMENT_NOTIFICATION);
                }
            }
        });


        TableLayout table = (TableLayout) popupView.findViewById(R.id.id_milk_pop_tbl_root);
        TableRow.LayoutParams tableRowParams =
                new TableRow.LayoutParams (TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.WRAP_CONTENT,9f);

        Log.e("merchantOrderDetailList","........................."+merchantOrderDetailList.size());

        for(int row = 0; row < arraylistMerchantOrderDetailList.size(); row++) {


            for (Map.Entry entry : merchantOrderDetailList.entrySet()) {
                Log.e("merchantOrderDetailList", "........................." + merchantOrderDetailList.size());
                int rowId = (int) entry.getKey();
                Template6Model valueObj = (Template6Model) entry.getValue();
                String quantity = valueObj.getQuantity();
                String productCode = valueObj.getProductCode();

                String productName = arraylistMerchantOrderDetailList.get(row).getprodname();

                if(productCode.equals(arraylistMerchantOrderDetailList.get(row).getprodcode())) {
                    if (!quantity.isEmpty()) {
                        final TableRow tr = new TableRow(mContext);
                        tr.setLayoutParams(tableRowParams);
                        // TextView for Product Name
                        TextView tv_productName = new TextView(mContext);
                        tv_productName.setText(productName);
                        tv_productName.setGravity(Gravity.END);
                        tr.addView(tv_productName, new TableRow.LayoutParams
                                (0, TableRow.LayoutParams.WRAP_CONTENT, 4f));
                        // Edit Text For Values
                        final EditText eTxtValue = new EditText(mContext);
                        eTxtValue.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                        eTxtValue.setText(quantity);
                        eTxtValue.setTag(rowId);
                        eTxtValue.setId(rowId);
                        eTxtValue.setFocusable(false);
                        editViews.put(rowId, eTxtValue);/////////////////////
                        eTxtValue.addTextChangedListener(new TextWatcher() {

                            @Override
                            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                            }

                            @Override
                            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                            }

                            @Override
                            public void afterTextChanged(Editable editable) {
                                String value = editable.toString().trim();
                                int position = (int) eTxtValue.getTag();
                                Template6Model temp = merchantOrderDetailList.get(position);
                                temp.setQuantity(value);
                                merchantOrderDetailList.put(position, temp);
                            }
                        });

                        tr.addView(eTxtValue, new TableRow.LayoutParams
                                (0, TableRow.LayoutParams.WRAP_CONTENT, 4f));
                        // TextView Edit row
                        final TextView tv_productEdit = new TextView(mContext);
                        Typeface fontEdit = Typeface.createFromAsset(mContext.getAssets(),
                                "fontawesome-webfont.ttf");
                        tv_productEdit.setText(mContext.getResources().getText(R.string.editicon));
                        tv_productEdit.setTypeface(fontEdit);
                        tv_productEdit.setTag(rowId);
                        tv_productEdit.setTextColor(Color.parseColor("#03acec"));
                        tv_productEdit.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
                        tv_productEdit.setGravity(Gravity.CENTER);
                        tv_productEdit.setClickable(true);
                        tv_productEdit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                int currentRow = (int) tv_productEdit.getTag();
                                for (Map.Entry entry: editViews.entrySet()) {
                                    EditText eTxt = (EditText) entry.getValue();
                                    int id = (int) entry.getKey();
                                    if (currentRow == id) {
                                        eTxt.setFocusable(true);
                                        eTxt.setClickable(true);
                                        eTxt.setFocusableInTouchMode(true);
                                        eTxt.requestFocus();
                                        showKeyboard();////
                                        setStringCost("Recalculate");////////////////
                                    } else {
                                        eTxt.setFocusable(false);
                                        eTxt.clearFocus();
                                    }
                                }
                                Log.e("eTxtValue", "..........................");
                            }
                        });

                        tr.addView(tv_productEdit, new TableRow.LayoutParams
                                (0, TableRow.LayoutParams.WRAP_CONTENT, 1f));

                        table.addView(tr);
                    }
                    break;
                }
            }
        }
        // initial cost setup
        requirementCheck();
        setCost(String.valueOf(calculateCost())); ////////
    }

    public boolean requirementCheck() {

        milk = fat = snf = temperature = lrvalue = false;
        for (Map.Entry entry : merchantOrderDetailList.entrySet()) {
            Template6Model temp = (Template6Model) entry.getValue();
            String code = String.valueOf(temp.getProductCode());
            String value = String.valueOf(temp.getQuantity());
            switch (code) {
                case Constants.MILK:
                    milk = (value.length() > 0 && (!(value.length() == 1 && value.startsWith(DOT))));
                    MILK = milk ? value : ZERO;
                    break;
                case Constants.FAT:
                    fat = (value.length() > 0 && (!(value.length() == 1 && value.startsWith(DOT))));
                    FAT = fat ? value : ZERO;
                    break;
                case Constants.SNF:
                    snf = (value.length() > 0 && (!(value.length() == 1 && value.startsWith(DOT))));
                    SNF = snf ? value : ZERO;
                    break;
                case Constants.TEMPERATURE:
                    temperature = (value.length() > 0 && (!(value.length() == 1 && value.startsWith(DOT))));
                    TEMPERATURE = temperature ? value : ZERO;
                    break;
                case Constants.LRVALUE:
                    lrvalue = (value.length() > 0 && (!(value.length() == 1 && value.startsWith(DOT))));
                    LRVALUE = lrvalue ? value : ZERO;
                    break;
            }
        }
        return milk && (fat && snf || fat && temperature && lrvalue);
    }

    private Float getPrice(String fat, String snf){
        Log.e("FATSNF","............FAT::"+ fat + "  SNF::"+ snf);
        Cursor cur = databaseHandler.getMilkPrice(fat, snf);
        if(cur != null && cur.getCount() > 0) {
            cur.moveToFirst();
            return cur.getFloat(0);
        }
        return 0f;
    }

    private Double calculateCost(){

        if(milk && fat && snf){
            try {
                Float price = getPrice(FAT, SNF);
                return price * Double.parseDouble(MILK);
            }catch (Exception e){
                e.printStackTrace();
            }

        }else if(milk && temperature && lrvalue){
            try {
                Cursor cur = databaseHandler.getSNFValue(TEMPERATURE, LRVALUE);
                if(cur != null) {
                    cur.moveToFirst();
                    float snf = cur.getFloat(0);
                    Log.e("SNF",".........................receivedSNFFromTable::"+snf);
                    float price = getPrice(FAT, String.valueOf(snf));
                    return price * Double.parseDouble(MILK);
                }
            }catch (Exception e){
                e.printStackTrace();
            }

        }
        return 0d;
    }

    public void showKeyboard(){
        InputMethodManager inputMethodManager = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }
    }

    public void closeKeyboard(){
        InputMethodManager inputMethodManager = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
            inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        }
    }

    private void showProgress(){
        ProgressAnimate.animateView(progressOverlay, View.VISIBLE, 0.4f, 200);
    }

    private void hideProgress(){
        ProgressAnimate.animateView(progressOverlay, View.GONE, 0, 200);
    }

    private void showD(final String title, final String msg){
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                showAlertDialog(title, msg);
            }
        }, 30);
    }

    public void showAlertDialog(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setCancelable(false);
        builder.setMessage(message);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(popup != null){
                            popup.dismiss();
                        }
                        dialog.cancel();
                    }
                });


        AlertDialog alert = builder.create();
        alert.show();

        Button buttonbackground = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground.setBackgroundColor(Color.parseColor("#A9E2F3"));
    }



}
