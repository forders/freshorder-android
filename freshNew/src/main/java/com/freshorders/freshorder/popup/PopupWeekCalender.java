package com.freshorders.freshorder.popup;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.freshorders.freshorder.R;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.freshorders.freshorder.model.HierarchyObjModel;
import com.freshorders.freshorder.utils.Log;
import com.freshorders.freshorder.weekcalendar.WeekCalendar;
import com.freshorders.freshorder.weekcalendar.WeekCalendar;
import com.freshorders.freshorder.weekcalendar.listener.OnDateClickListener;
import com.freshorders.freshorder.weekcalendar.listener.OnWeekChangeListener;

public class PopupWeekCalender {

    public interface WeekPopupListener{
        public void onDismiss();
        public void isPopupAvail(boolean isShowing);
        public void interruptOccur(String msg);
    }
    PopupWeekCalender.WeekPopupListener listener;

    private Context mContext;
    private static final String TAG = PopupWeekCalender.class.getSimpleName();

    public PopupWeekCalender(Context mContext, View rootView,DisplayMetrics displayMetrics, WeekPopupListener listener) {
        this.listener = listener;
        this.mContext = mContext;
        this.rootView = rootView;
        this.displayMetrics = displayMetrics;
        createPopup();
    }

    private View popupView;
    private View rootView;

    private WeekCalendar weekCalendar;
    private DateTimeFormatter dateTimeFormatter;
    private TextView tvSelectedDate;
    private PopupWindow popup;
    private DisplayMetrics displayMetrics;

    private void createPopup() {
        try {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (layoutInflater != null) {
                popupView = layoutInflater.inflate(R.layout.popup_week_view, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() {
        dateTimeFormatter = DateTimeFormat.forPattern("dd/MM/yyyy");
        tvSelectedDate = popupView.findViewById(R.id.id_week_selected_date);

        Button todaysDate = (Button) popupView.findViewById(R.id.today);
        Button selectedDate = (Button) popupView.findViewById(R.id.selectedDateButton);
        Button startDate = (Button) popupView.findViewById(R.id.startDate);
        todaysDate.setText(new DateTime().toLocalDate().toString() + " (Reset Button)");
        selectedDate.setText(new DateTime().plusDays(50).toLocalDate().toString()
                + " (Set Selected Date Button)");
        startDate.setText(new DateTime().plusDays(7).toLocalDate().toString()
                + " (Set Start Date Button)");

        weekCalendar = (WeekCalendar) popupView.findViewById(R.id.weekCalendar);
        weekCalendar.setOnDateClickListener(new OnDateClickListener() {
            @Override
            public void onDateClick(DateTime dateTime) {
                Toast.makeText(mContext, "You Selected " + dateTime.toString(), Toast
                        .LENGTH_SHORT).show();
                //DateTime parsedDateTimeUsingFormatter = DateTime.parse(dateTime.toString(), dateTimeFormatter);
                String date = dateTimeFormatter.print(dateTime);
                Log.e(TAG, "..........DATE:::" + date);
                tvSelectedDate.setText(date);
            }

        });



        weekCalendar.setOnWeekChangeListener(new OnWeekChangeListener() {
            @Override
            public void onWeekChange(DateTime firstDayOfTheWeek, boolean forward) {
                Toast.makeText(mContext, "Week changed: " + firstDayOfTheWeek +
                        " Forward: " + forward, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void showPopupWindow() {
        try {



            int pxWidth = displayMetrics.widthPixels;
            float dpWidth = pxWidth / displayMetrics.density;
            int pxHeight = displayMetrics.heightPixels;
            float dpHeight = pxHeight / displayMetrics.density;
            // Set alert dialog width equal to screen width 90%
            int dialogWindowWidth = (int) (pxWidth * 0.8f);
            // Set alert dialog height equal to screen height 60%
            int dialogWindowHeight = (int) (pxHeight * 0.6f);

            //popWindowInflated = LayoutInflater.from(mContext).inflate(R.layout.popup_survey_payment, null);

            popup = new PopupWindow(mContext);
            popup.setWidth(dialogWindowWidth);
            popup.setHeight(dialogWindowHeight);

            popup.setContentView(popupView);

            popup.setTouchInterceptor(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    return false;
                }
            });

            popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                    //isClosed = true;
                    if(listener != null) {
                        listener.onDismiss();
                        listener.isPopupAvail(false);
                    }
                }
            });
            // Displaying the popup at the specified location, + offsets.
            //////////////popup.showAtLocation(popWindowInflated, Gravity.CENTER, 0, 0);  hide by Kumaravel 01-07-2019
            popup.showAtLocation(rootView, Gravity.CENTER, 0, 0);
            ////addWidgetsToListener(popWindowInflated);
            popup.setFocusable(true);
            popup.setTouchable(true);
            popup.setOutsideTouchable(false);
            // Set popup window animation style.
            popup.setAnimationStyle(R.anim.slide_in_up);
            popup.update();

            init();

        } catch (Exception e) {
            e.printStackTrace();
            if(listener != null){
                listener.interruptOccur(mContext.getResources().getString(R.string.technical_fault) + "  :" + e.getMessage());
            }
            if(popup != null){
                popup.dismiss();
            }
        }
    }
}
