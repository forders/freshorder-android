package com.freshorders.freshorder.popup;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.PopupWindow;

import com.freshorders.freshorder.R;

public class Template2ShowPaymentPopup {

    private Context mContext;
    private View popupView;
    private PopupWindow popup;
    private View parentToken;

    public Template2ShowPaymentPopup(Context mContext, View windowToken) {
        this.mContext = mContext;
        this.parentToken = windowToken;
    }

    public View createPopup() {
        try {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (layoutInflater != null) {
                popupView = layoutInflater.inflate(R.layout.payment_type_template2, null);
                return popupView;
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public PopupWindow showPopup() {
        popup = new PopupWindow(mContext);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setContentView(popupView);
        // Set popup window animation style.
        popup.setAnimationStyle(R.anim.up_from_bottom);
        popup.setTouchable(true);
        popup.setFocusable(true);
        popup.setOutsideTouchable(false);
        popup.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        popup.update();
        popup.showAtLocation(parentToken, Gravity.CENTER, 0, 0);

        return popup;

    }

    public void closePopup(){
        popup.dismiss();
    }
}
