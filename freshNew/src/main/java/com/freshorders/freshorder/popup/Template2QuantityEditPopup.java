package com.freshorders.freshorder.popup;

import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.MerchantOrderDomainSelected;
import com.freshorders.freshorder.ui.CreateOrderActivity;
import com.freshorders.freshorder.ui.MerchantOrderDetailActivity;
import com.freshorders.freshorder.utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;

import static com.freshorders.freshorder.utils.Constants.SHOULD_NOT_EMPTY_DATA;
import static com.freshorders.freshorder.utils.Constants.SHOULD_NOT_ONLY_DOT;
import static com.freshorders.freshorder.utils.Constants.filter;

public class Template2QuantityEditPopup {

    private Context mContext;
    private View popupView;
    private PopupWindow popup;
    private View rootWindow;
    private String closingStock, orderQuantity;
    private int position;
    String prodid;
    private ArrayList<MerchantOrderDomainSelected> arraylistMerchantOrderDetailList;
    HashMap<String, String[]> itemListUOM;
    String uom;
    private String changedUOM = "";
    private ArrayAdapter<String> adapter = null;

    private DatabaseHandler databaseHandler ;

    public Template2QuantityEditPopup(Context mContext, String closingStock,
                                      String orderQuantity, View rootWindow,
                                      int position, String prodid,
                                      ArrayList<MerchantOrderDomainSelected> arraylistMerchantOrderDetailList,
                                      HashMap<String, String[]> itemListUOM, String uom) {
        this.mContext = mContext;
        this.closingStock = closingStock;
        this.orderQuantity = orderQuantity;
        this.rootWindow = rootWindow;
        this.position = position;
        this.prodid = prodid;
        this.arraylistMerchantOrderDetailList = arraylistMerchantOrderDetailList;
        this.itemListUOM = itemListUOM;
        this.uom = uom;
        databaseHandler = new DatabaseHandler(mContext);
        createPopup();
        changedUOM = uom; ////////// user doesn't select any one we use already selected

        String[] array = getUOMByProductId(prodid);
        adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, array);
    }

    private void createPopup() {
        try {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (layoutInflater != null) {
                popupView = layoutInflater.inflate(R.layout.popup_edit_template2, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showPopup() {
        popup = new PopupWindow(mContext);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setContentView(popupView);
        addWidgetsToListener(popupView);
        // Set popup window animation style.
        popup.setAnimationStyle(R.anim.up_from_bottom);
        popup.setTouchable(true);
        popup.setFocusable(true);
        popup.setOutsideTouchable(false);
        popup.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        popup.update();
        popup.showAtLocation(rootWindow, Gravity.CENTER, 0, 0);

    }

    private void addWidgetsToListener(View popupView) {
        final EditText eTxtStock = (EditText) popupView.findViewById(R.id.editTextClosingStock);
        final EditText eTxtQuantity = (EditText) popupView.findViewById(R.id.editTextOrderQuantity);
        final TextView txtNotification = (TextView) popupView.findViewById(R.id.id_pop_template2_alert);
        Button btnCancel = (Button) popupView.findViewById(R.id.id_pop_template2_edit_btn_cancel);
        Button btnUpdate = (Button) popupView.findViewById(R.id.id_pop_template2_edit_btn_update);
        if(MyApplication.getInstance().isTemplate8ForTemplate2()){
            eTxtQuantity.setVisibility(View.INVISIBLE);
        }
        eTxtStock.setText(closingStock);
        eTxtQuantity.setText(orderQuantity);
        Spinner spinUOM = (Spinner) popupView.findViewById(R.id.id_template2_atv_uom);
        spinUOM.setAdapter(adapter);
        int spinnerPosition = -1;
        Log.e("UOM", ",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,UOM" + uom);
        spinnerPosition = adapter.getPosition(uom);
        spinUOM.setSelection(spinnerPosition);


        final TextView tvUOM = (TextView) rootWindow.findViewById(R.id.id_template2_uom);
        TextView tvItemName = (TextView) rootWindow.findViewById(R.id.textViewName);
        String itemName = tvItemName.getText().toString().trim();
        TextView tvPopupItemName = (TextView) popupView.findViewById(R.id.id_pop_template2_order_name);
        tvPopupItemName.setText(itemName);

        spinUOM.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                changedUOM =  ((TextView) view).getText().toString();/////////
                tvUOM.setText(changedUOM);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String stock = eTxtStock.getText().toString().trim();
                String quantity = eTxtQuantity.getText().toString().trim();

                if(MyApplication.getInstance().isTemplate8ForTemplate2()){
                    if(!stock.isEmpty()){
                        if(stock.length() == 1) {
                            if (stock.startsWith(".")) {
                                txtNotification.setVisibility(View.VISIBLE);
                                txtNotification.setText(SHOULD_NOT_ONLY_DOT);
                            } else {
                                txtNotification.setVisibility(View.INVISIBLE);
                                TextView tvClosingStock = (TextView) rootWindow.findViewById(R.id.textViewClosingStock);
                                TextView tvUOM = (TextView) rootWindow.findViewById(R.id.textViewUOM);
                                tvUOM.setText(changedUOM);
                                tvClosingStock.setText(stock);
                                updateStockQuantity(stock, Constants.EMPTY);
                            }
                        }else {
                            txtNotification.setVisibility(View.INVISIBLE);
                            TextView tvClosingStock = (TextView) rootWindow.findViewById(R.id.textViewClosingStock);
                            TextView tvUOM = (TextView) rootWindow.findViewById(R.id.textViewUOM);
                            tvUOM.setText(changedUOM);
                            tvClosingStock.setText(stock);
                            updateStockQuantity(stock, Constants.EMPTY);
                        }
                    }else {
                        txtNotification.setVisibility(View.VISIBLE);
                        txtNotification.setText(SHOULD_NOT_EMPTY_DATA);
                    }
                    return;
                }

                if(!stock.isEmpty() && !quantity.isEmpty()){
                    if(stock.length() == 1 || quantity.length() == 1){
                        if(stock.startsWith(".") || quantity.startsWith(".")){
                            txtNotification.setVisibility(View.VISIBLE);
                            txtNotification.setText(SHOULD_NOT_ONLY_DOT);
                        }else {
                            txtNotification.setVisibility(View.INVISIBLE);
                            TextView tvClosingStock = (TextView) rootWindow.findViewById(R.id.textViewClosingStock);
                            TextView tvQuantity = (TextView) rootWindow.findViewById(R.id.textViewQuantity);
                            tvClosingStock.setText(stock);
                            tvQuantity.setText(quantity);
                            updateStockQuantity(stock, quantity);
                        }
                    }else {
                        txtNotification.setVisibility(View.INVISIBLE);
                        TextView tvClosingStock = (TextView) rootWindow.findViewById(R.id.textViewClosingStock);
                        TextView tvQuantity = (TextView) rootWindow.findViewById(R.id.textViewQuantity);
                        tvClosingStock.setText(stock);
                        tvQuantity.setText(quantity);
                        updateStockQuantity(stock, quantity);
                    }
                }else {
                    txtNotification.setVisibility(View.VISIBLE);
                    txtNotification.setText(SHOULD_NOT_EMPTY_DATA);
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
            }
        });

    }

    private void updateStockQuantity(String stock, String quantity){

        if (Constants.USER_TYPE.equals("M")) {
            for(int i = 0; i < MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.size(); i++){
                MerchantOrderDomainSelected item = MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.get(i);
                if (item.getprodid().equals(prodid)) {
                    item.setOutstock(stock);
                    item.setCurrStock(stock);
                    item.setqty(quantity);
                    if(!changedUOM.isEmpty()){
                        item.setorderuom(changedUOM);
                    }
                    MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.set(i, item);
                    MyApplication.previousSelectedForSearch.put(prodid, item);
                    break;
                }
            }
        }else {
            for(int i = 0; i < CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size(); i++) {
                MerchantOrderDomainSelected item = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i);
                if (item.getprodid().equals(prodid)) {
                    item.setOutstock(stock);
                    item.setCurrStock(stock);
                    item.setqty(quantity);
                    if(!changedUOM.isEmpty()){
                        item.setorderuom(changedUOM);
                    }
                    CreateOrderActivity.arraylistMerchantOrderDetailListSelected.set(i, item);
                    MyApplication.previousSelectedForSearch.put(prodid, item); // for search and back operation
                    break;
                }
            }
        }

        arraylistMerchantOrderDetailList.get(position).setqty(quantity);
        arraylistMerchantOrderDetailList.get(position).setOutstock(stock);

        popup.dismiss();
    }

    private String[] getUOMByProductId(String prodid){
        Cursor cur = null;
        String[] array;
        try {
            cur = databaseHandler.getUOMforproduct(prodid);
            array = new String[cur.getCount()];
            cur.moveToFirst();
            for (int i = 0; i < cur.getCount(); i++) {
                //array[0] = item.getUOM();
                array[i] = cur.getString(cur.getColumnIndex("uomname"));
                cur.moveToNext();
            }
            return array;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }finally {
            if(cur != null){
                cur.close();
            }
        }
    }
}
