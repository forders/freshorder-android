package com.freshorders.freshorder.popup;

import android.content.Context;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.PopupWindow;

import com.freshorders.freshorder.R;

public class PendingSurveyPopup {

    private Context mContext;
    private View popWindowInflated;
    private PopupWindow popup;
    private View rootWindow;
    private DisplayMetrics displayMetrics;

    public PendingSurveyPopup(Context mContext, View rootWindow, DisplayMetrics displayMetrics) {
        this.mContext = mContext;
        this.rootWindow = rootWindow;
        this.displayMetrics = displayMetrics;
    }

    public void showPopupWindow() {
        try {

            int pxWidth = displayMetrics.widthPixels;
            float dpWidth = pxWidth / displayMetrics.density;
            int pxHeight = displayMetrics.heightPixels;
            float dpHeight = pxHeight / displayMetrics.density;
            // Set alert dialog width equal to screen width 50%
            int dialogWindowWidth = (int) (pxWidth * 0.5f);
            // Set alert dialog height equal to screen height 70%
            int dialogWindowHeight = (int) (pxHeight * 0.7f);

            popWindowInflated = LayoutInflater.from(mContext).inflate(R.layout.popup_survey, null);

            popup = new PopupWindow(mContext);
            popup.setWidth(dialogWindowWidth);
            popup.setHeight(dialogWindowHeight);

            popup.setContentView(popWindowInflated);

            popup.setTouchInterceptor(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    return false;
                }
            });

            popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {

                }
            });
            // Displaying the popup at the specified location, + offsets.
            popup.showAtLocation(popWindowInflated, Gravity.CENTER, 0, 0);
            addWidgetsToListener(popWindowInflated);
            popup.setFocusable(true);
            popup.setTouchable(true);
            popup.setOutsideTouchable(false);

            popup.update();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addWidgetsToListener(final View popWindowInflated) {

        RecyclerView recyclerView = (RecyclerView) popWindowInflated.findViewById(R.id.id_RV_popup_survey);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mContext.getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        //PopupRecyclerViewAdapter adapter = new PopupRecyclerViewAdapter(mContext,data);
        //recyclerView.setAdapter(adapter);
    }
}
