package com.freshorders.freshorder.popup;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.model.PaymentChequeDetailModel;
import com.freshorders.freshorder.model.PaymentCollectionModel;
import com.freshorders.freshorder.view.LazyDatePicker;
import com.google.gson.Gson;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class PopupSurveyPayment implements AdapterView.OnItemSelectedListener {

    public interface PaymentCompleteListener{
        void success(PaymentCollectionModel result);
        void failed();
    }

    private Context mContext;
    private View popWindowInflated;
    private PopupWindow popup;
    private View rootWindow;
    private DisplayMetrics displayMetrics;
    private PopupSurveyPayment.PaymentCompleteListener listener;


    String merchantName = "";
    private LinearLayout llCheque, llAmount;
    private LinearLayout ll_NEFT_RTGS;
    private EditText txtAmount;
    private static final String DATE_FORMAT = "dd-MM-yyyy";
    private DatePicker datePicker;
    private EditText txtChequeDate;
    private EditText txtBankName;
    private EditText txtChequeNo;
    private EditText txt_NEFT_RTGS_Date;

    private static int selectedIndex = 0;

    PaymentCollectionModel result;
    private double balamount;
    private View parentView;

    public PopupSurveyPayment(Context mContext,View parent, View rootWindow,
                              DisplayMetrics displayMetrics,
                              String merchantName,
                              double balamount,
                              PopupSurveyPayment.PaymentCompleteListener listener) {
        this.mContext = mContext;
        this.parentView = parent;
        this.rootWindow = rootWindow;
        this.displayMetrics = displayMetrics;
        this.merchantName = merchantName;
        this.balamount = balamount;
        this.listener = listener;
        createPopup();
        result = new PaymentCollectionModel();
    }

    private void createPopup() {
        try {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (layoutInflater != null) {
                popWindowInflated = layoutInflater.inflate(R.layout.popup_survey_payment, null);  //popup_survey_payment
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showPopupWindow() {
        try {

            int pxWidth = displayMetrics.widthPixels;
            float dpWidth = pxWidth / displayMetrics.density;
            int pxHeight = displayMetrics.heightPixels;
            float dpHeight = pxHeight / displayMetrics.density;
            // Set alert dialog width equal to screen width 90%
            int dialogWindowWidth = (int) (pxWidth * 0.9f);
            // Set alert dialog height equal to screen height 60%
            int dialogWindowHeight = (int) (pxHeight * 0.6f);

            //popWindowInflated = LayoutInflater.from(mContext).inflate(R.layout.popup_survey_payment, null);

            popup = new PopupWindow(mContext);
            popup.setWidth(dialogWindowWidth);
            popup.setHeight(dialogWindowHeight);

            popup.setContentView(popWindowInflated);

            popup.setTouchInterceptor(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    return false;
                }
            });

            popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {

                }
            });
            // Displaying the popup at the specified location, + offsets.
            //////////////popup.showAtLocation(popWindowInflated, Gravity.CENTER, 0, 0);  hide by Kumaravel 01-07-2019
            popup.showAtLocation(parentView, Gravity.CENTER, 0, 0);
            addWidgetsToListener(popWindowInflated);
            popup.setFocusable(true);
            popup.setTouchable(true);
            popup.setOutsideTouchable(false);
            // Set popup window animation style.
            popup.setAnimationStyle(R.anim.slide_in_up);
            popup.update();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addWidgetsToListener(final View popWindowInflated) {

        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT, Locale.getDefault());
        Date today = Calendar.getInstance().getTime();
        String strDate = dateFormat.format(today);
        TextView txtTodayDate = popWindowInflated.findViewById(R.id.id_pay_collection_today_date);
        txtTodayDate.setText(strDate);

        TextView txtName = popWindowInflated.findViewById(R.id.id_pay_collection_name);
        txtName.setText(merchantName);

        Spinner spinPayMode = popWindowInflated.findViewById(R.id.id_pay_collection_spin_pay_mode);
        txtAmount = popWindowInflated.findViewById(R.id.id_sur_pay_ET_amount);
        llAmount = popWindowInflated.findViewById(R.id.LL_pay_collection_amount_show_hide);

        llCheque = popWindowInflated.findViewById(R.id.LL_pay_collection_cheque_show_hide);
        txtBankName = popWindowInflated.findViewById(R.id.id_sur_pay_ET_bank_name);
        txtChequeNo = popWindowInflated.findViewById(R.id.id_sur_pay_ET_cheque_no);
        txtChequeDate = popWindowInflated.findViewById(R.id.id_sur_pay_ET_cheque_date);

        ll_NEFT_RTGS = popWindowInflated.findViewById(R.id.LL_pay_collection_NEFT_RTGS_show_hide);
        txt_NEFT_RTGS_Date = popWindowInflated.findViewById(R.id.id_sur_pay_ET_NEFT_RTGS_date);

        Button btnSubmit = popWindowInflated.findViewById(R.id.id_sur_pay_btn_submit);
        btnSubmit.setOnClickListener(submit);

        spinPayMode.setOnItemSelectedListener(this);
        spinPayMode.setSelection(0);

        txtChequeDate.setOnClickListener(chequeDate);
        txt_NEFT_RTGS_Date.setOnClickListener(_NEFT_RTGS_Date);
    }

    private void showDate(final EditText rootView){
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.popup_outlet_stock_date_entry);

        datePicker = (DatePicker) dialog.findViewById(R.id.datePicker);
        // Define min & max date for sample
        Date minDate = LazyDatePicker.stringToDate("01-01-2000", DATE_FORMAT);
        Date maxDate = LazyDatePicker.stringToDate("12-31-3018", DATE_FORMAT);
        // Init LazyDatePicker
        LazyDatePicker lazyDatePicker = (LazyDatePicker) dialog.findViewById(R.id.lazyDatePicker);
        lazyDatePicker.setDateFormat(LazyDatePicker.DateFormat.DD_MM_YYYY);
        lazyDatePicker.setMinDate(minDate);
        lazyDatePicker.setMaxDate(maxDate);

        lazyDatePicker.setOnDatePickListener(new LazyDatePicker.OnDatePickListener() {
            @Override
            public void onDatePick(Date dateSelected) {
                String selectedDate = LazyDatePicker.dateToString(dateSelected, DATE_FORMAT);
                Toast.makeText(mContext,
                        "Selected date: " + selectedDate,
                        Toast.LENGTH_SHORT).show();
                ((EditText)rootView).setText(selectedDate);
                dialog.dismiss();
            }
        });

        Button btnOk = (Button) dialog.findViewById(R.id.id_outlet_stock_date_ok);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int day = datePicker.getDayOfMonth();
                int month = datePicker.getMonth() + 1;
                int year = datePicker.getYear();

                String monthLength =String.valueOf(month);//
                if(monthLength.length()==1){
                    monthLength = "0"+monthLength;
                }
                String dayLength =String.valueOf(day);
                if(dayLength.length()==1){
                    dayLength = "0"+dayLength;
                }

                String selectedDate = dayLength + "-" + monthLength + "-" + String.valueOf(year);
                ((EditText)rootView).setText(selectedDate);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private View.OnClickListener submit = new View.OnClickListener(){
        @Override
        public void onClick(View v) {

            String[] mTestArray = mContext.getResources().getStringArray(R.array.survey_payment_mode);
            String amount = txtAmount.getText().toString();

            if(amount.length() <= 0 && selectedIndex != 0){
                showAlertDialogToast("Should enter amount");
            }else {

                if(!(amount.length() == 1 && amount.contains("."))) {

                    try {
                        if(selectedIndex == 0){
                            amount = "0"; //for default value
                        }
                        double enteredAmount = Double.parseDouble(amount);
                        enteredAmount = Double.parseDouble(new DecimalFormat("##.##").format(enteredAmount));
                        amount = String.valueOf(enteredAmount);

                        if (balamount < enteredAmount) {
                            showAlertDialogToast("The amount you enter is greater than the balance amount");
                            return;
                        }
                        switch (selectedIndex) {
                            case 0:
                                result.setNotes(mTestArray[selectedIndex]);
                                result.setPymtmode(mTestArray[selectedIndex]);
                                result.setUpdateddt(getDateTime());
                                result.setPayamt("0");

                                listener.success(result);
                                popup.dismiss();
                                break;
                            case 1:
                                enteredAmount = Double.valueOf(amount);
                                result.setNotes(mTestArray[selectedIndex]);
                                result.setPymtmode(mTestArray[selectedIndex]);
                                result.setUpdateddt(getDateTime());
                                result.setPayamt(amount);

                                listener.success(result);
                                popup.dismiss();
                                break;
                            case 2:
                                PaymentChequeDetailModel chequeModel = new PaymentChequeDetailModel();
                                String bankName = txtBankName.getText().toString().trim();
                                String chequeNo = txtChequeNo.getText().toString().trim();
                                String chequeDate = txtChequeDate.getText().toString().trim();
                                if (!(bankName.length() > 0 && chequeNo.length() > 0 && chequeDate.length() > 0)) {
                                    showAlertDialogToast("Should enter all details");
                                    return;
                                } else {
                                    chequeModel.setBankName(bankName);
                                    chequeModel.setChequeNumber(chequeNo);
                                    chequeModel.setChequeDate(chequeDate);
                                }
                                Gson gson = new Gson();
                                String json = gson.toJson(chequeModel);
                                enteredAmount = Double.valueOf(amount);
                                result.setNotes(json);
                                result.setPymtmode(mTestArray[selectedIndex]);
                                result.setUpdateddt(getDateTime());
                                result.setPayamt(amount);

                                listener.success(result);
                                popup.dismiss();
                                break;
                            case 3:
                                enteredAmount = Double.valueOf(amount);
                                String transferDate = txt_NEFT_RTGS_Date.getText().toString().trim();
                                if (!(transferDate.length() > 0)) {
                                    showAlertDialogToast("Should enter all details");
                                    return;
                                }
                                result.setNotes("Transfer Date : " + transferDate);
                                result.setPymtmode(mTestArray[selectedIndex]);
                                result.setUpdateddt(getDateTime());
                                result.setPayamt(amount);

                                listener.success(result);
                                popup.dismiss();
                                break;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        showAlertDialogToast(e.getMessage());
                        popup.dismiss();
                    }
                }else {
                    showAlertDialogToast("Should enter valid amount");
                }
            }
        }
    };

    private View.OnClickListener chequeDate = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            //popup.dismiss();
            showDate(txtChequeDate );

        }
    };

    private View.OnClickListener _NEFT_RTGS_Date = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            //popup.dismiss();
            showDate(txt_NEFT_RTGS_Date );

        }
    };

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        showHideView(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void showHideView(int position){

        switch (position) {
            case 0:
                llAmount.setVisibility(View.GONE);
                llCheque.setVisibility(View.GONE);
                ll_NEFT_RTGS.setVisibility(View.GONE);
                selectedIndex = 0;
                break;
            case 1:
                llAmount.setVisibility(View.VISIBLE);
                llCheque.setVisibility(View.GONE);
                ll_NEFT_RTGS.setVisibility(View.GONE);
                selectedIndex = 1;
                break;
            case 2:
                llAmount.setVisibility(View.VISIBLE);
                llCheque.setVisibility(View.VISIBLE);
                ll_NEFT_RTGS.setVisibility(View.GONE);
                selectedIndex = 2;
                break;
            case 3:
                llAmount.setVisibility(View.VISIBLE);
                llCheque.setVisibility(View.GONE);
                ll_NEFT_RTGS.setVisibility(View.VISIBLE);
                selectedIndex = 3;
                break;
        }
    }

    private String getDateTime() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm",Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    public void showAlertDialogToast(String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }
}
