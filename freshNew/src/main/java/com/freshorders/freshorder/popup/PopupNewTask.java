package com.freshorders.freshorder.popup;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.freshorders.freshorder.R;
import com.warkiz.widget.IndicatorSeekBar;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class PopupNewTask {

    private Button btnSun;
    private Button btnMon;
    private Button btnTue;
    private Button btnWed;
    private Button btnThu;
    private Button btnFri;
    private Button btnSat;

    private CheckBox chkAllDay;
    private Spinner spEventRepetition;
    private LinearLayout llHoursSelect;
    private LinearLayout llWeekSelect;
    private LinearLayout llMonthSelect;

    public interface PopupListener{
        public void dismiss();
        public void isPopupAvail(boolean isShowing);
        public void interruptOccur(String msg);
    }

    private Context mContext;
    private View popupView;
    private View rootView;
    private PopupMyTask.PopupListener listener;
    private DisplayMetrics displayMetrics;
    private DateTimeFormatter dateTimeFormatter;

    private String head, percentage;

    public PopupNewTask(Context mContext,
                       View rootView,
                       DisplayMetrics displayMetrics,
                       PopupMyTask.PopupListener listener) {
        this.listener = listener;
        this.mContext = mContext;
        this.rootView = rootView;
        this.displayMetrics = displayMetrics;
        createPopup();
    }

    private PopupWindow popup;

    private void createPopup() {
        try {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (layoutInflater != null) {
                popupView = layoutInflater.inflate(R.layout.popup_event_select, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showPopupWindow() {

        try {

            int pxWidth = displayMetrics.widthPixels;
            float dpWidth = pxWidth / displayMetrics.density;
            int pxHeight = displayMetrics.heightPixels;
            float dpHeight = pxHeight / displayMetrics.density;
            // Set alert dialog width equal to screen width 90%
            int dialogWindowWidth = (int) (pxWidth * 0.8f);
            // Set alert dialog height equal to screen height 60%
            int dialogWindowHeight = (int) (pxHeight * 0.6f);

            //popWindowInflated = LayoutInflater.from(mContext).inflate(R.layout.popup_survey_payment, null);

            popup = new PopupWindow(mContext);
            popup.setWidth(dialogWindowWidth);
            popup.setHeight(dialogWindowHeight);

            popup.setContentView(popupView);

            popup.setTouchInterceptor(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    return false;
                }
            });

            popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                    //isClosed = true;
                    if(listener != null) {
                        listener.dismiss();
                    }
                }
            });
            // Displaying the popup at the specified location, + offsets.
            //////////////popup.showAtLocation(popWindowInflated, Gravity.CENTER, 0, 0);  hide by Kumaravel 01-07-2019
            popup.showAtLocation(rootView, Gravity.CENTER, 0, 0);

            addWidgetsToListener(popupView);

            popup.setFocusable(true);
            popup.setTouchable(true);
            popup.setOutsideTouchable(false);
            // Set popup window animation style.
            popup.setAnimationStyle(R.anim.slide_in_up);
            popup.update();


        } catch (Exception e) {
            e.printStackTrace();
            if(listener != null){
                listener.interruptOccur(mContext.getResources().getString(R.string.technical_fault) + "  :" + e.getMessage());
            }
            if(popup != null){
                popup.dismiss();
            }
        }
    }

    private void addWidgetsToListener(View view){

        btnSun = view.findViewById(R.id.id_event_week_btn_sun);
        btnMon = view.findViewById(R.id.id_event_week_btn_mon);
        btnTue = view.findViewById(R.id.id_event_week_btn_tue);
        btnWed = view.findViewById(R.id.id_event_week_btn_wed);
        btnThu = view.findViewById(R.id.id_event_week_btn_thu);
        btnFri = view.findViewById(R.id.id_event_week_btn_fri);
        btnSat = view.findViewById(R.id.id_event_week_btn_sat);

        btnSun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeBackground(R.id.id_event_week_btn_sun);
            }
        });

        btnMon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeBackground(R.id.id_event_week_btn_mon);
            }
        });

        chkAllDay = view.findViewById(R.id.id_event_chkBox_all_day);
        spEventRepetition = view.findViewById(R.id.id_event_SP_repetition);
        llHoursSelect = view.findViewById(R.id.id_event_LL_repeat_daily);
        llWeekSelect = view.findViewById(R.id.id_event_LL_repeat_weekly);
        llMonthSelect = view.findViewById(R.id.id_event_LL_repeat_monthly);

        chkAllDay.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    spEventRepetition.setVisibility(View.GONE);
                    llHoursSelect.setVisibility(View.GONE);
                }else {
                    spEventRepetition.setVisibility(View.VISIBLE);
                    int position = spEventRepetition.getSelectedItemPosition();
                    showRepetitionSelect(position);
                }
            }
        });

        spEventRepetition.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                showRepetitionSelect(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void showRepetitionSelect(int position){


        switch (position){

            case 0:
                llHoursSelect.setVisibility(View.GONE);
                llWeekSelect.setVisibility(View.GONE);
                llMonthSelect.setVisibility(View.GONE);
                break;
            case 1:
                llHoursSelect.setVisibility(View.VISIBLE);
                llWeekSelect.setVisibility(View.GONE);
                llMonthSelect.setVisibility(View.GONE);
                break;
            case 2:
                llWeekSelect.setVisibility(View.VISIBLE);
                llHoursSelect.setVisibility(View.GONE);
                llMonthSelect.setVisibility(View.GONE);
                break;
            case 3:
                llMonthSelect.setVisibility(View.VISIBLE);
                llHoursSelect.setVisibility(View.GONE);
                llWeekSelect.setVisibility(View.GONE);
                break;
        }

    }

    private void changeBackground(int id){

        String uri = "@drawable/circle_button_event_week_days";  // where circle_button_event_week_days (without the extension) is the file
        int imageResource = mContext.getResources().getIdentifier(uri, null, mContext.getPackageName());
        Drawable res = mContext.getResources().getDrawable(imageResource);

        String uriPressed = "@drawable/circle_button_event_week_days_pressed";  // where circle_button_event_week_days (without the extension) is the file
        int resourcePressed = mContext.getResources().getIdentifier(uriPressed, null, mContext.getPackageName());
        Drawable drwPressed = mContext.getResources().getDrawable(resourcePressed);


        switch (id){

            case R.id.id_event_week_btn_sun:
                btnSun.setBackground(drwPressed);
                btnSun.setTextColor(ContextCompat.getColor(mContext, R.color.white));

                btnMon.setBackground(res);
                btnMon.setTextColor(ContextCompat.getColor(mContext, R.color.black));
                btnTue.setBackground(res);
                btnMon.setTextColor(ContextCompat.getColor(mContext, R.color.black));
                btnWed.setBackground(res);
                btnWed.setTextColor(ContextCompat.getColor(mContext, R.color.black));
                btnThu.setBackground(res);
                btnThu.setTextColor(ContextCompat.getColor(mContext, R.color.black));
                btnFri.setBackground(res);
                btnFri.setTextColor(ContextCompat.getColor(mContext, R.color.black));
                btnSat.setBackground(res);
                btnSat.setTextColor(ContextCompat.getColor(mContext, R.color.black));

                break;
            case R.id.id_event_week_btn_mon:
                btnMon.setBackground(drwPressed);
                btnMon.setTextColor(ContextCompat.getColor(mContext, R.color.white));

                btnSun.setBackground(res);
                btnSun.setTextColor(ContextCompat.getColor(mContext, R.color.black));
                btnTue.setBackground(res);
                btnMon.setTextColor(ContextCompat.getColor(mContext, R.color.black));
                btnWed.setBackground(res);
                btnWed.setTextColor(ContextCompat.getColor(mContext, R.color.black));
                btnThu.setBackground(res);
                btnThu.setTextColor(ContextCompat.getColor(mContext, R.color.black));
                btnFri.setBackground(res);
                btnFri.setTextColor(ContextCompat.getColor(mContext, R.color.black));
                btnSat.setBackground(res);
                btnSat.setTextColor(ContextCompat.getColor(mContext, R.color.black));

                break;
        }
    }

}
