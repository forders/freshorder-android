package com.freshorders.freshorder.popup;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.activity.DailySalesReportActivity;
import com.freshorders.freshorder.adapter.ClosingStockAuditDetailAdapter;
import com.freshorders.freshorder.adapter.DSRAdapter;
import com.freshorders.freshorder.adapter.DSRDetailAdapter;
import com.freshorders.freshorder.model.DSRDetailModel;
import com.freshorders.freshorder.model.StockAuditDisplayModel;
import com.freshorders.freshorder.model.StockAuditSendStock;
import com.freshorders.freshorder.ui.ClosingStockAudit;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import static android.widget.LinearLayout.VERTICAL;

public class PopupDSRDetail {

    private Context mContext;
    private View popupView;
    private PopupWindow popup;
    private View rootWindow;

    private boolean isClosed = true;

    private RecyclerView recyclerview;
    private DSRDetailAdapter rvAdapter;
    private String brName;
    private String date;

    private List<DSRDetailModel> listItems;

    private float totalAmount = 0f;
    private String totalSales = "";


    public PopupDSRDetail(View rootWindow,
                                  Context mContext,
                                  List<DSRDetailModel> listItems,
                          String brName,
                          String date,
                          String totalSales) {
        this.rootWindow = rootWindow;
        this.listItems = listItems;
        this.mContext = mContext;
        this.brName = brName;
        this.date = date;
        this.totalSales = totalSales;

        calculateTotal();
        createPopup();



    }
    private void calculateTotal(){
        for(int i = 0; i < listItems.size(); i++){
            String str = listItems.get(i).getValue().replaceAll("[^\\d.]", "");
            totalAmount = totalAmount +  Float.parseFloat((str));
        }
    }

    private void createPopup() {
        try {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (layoutInflater != null) {
                popupView = layoutInflater.inflate(R.layout.popup_dsr_detail, null);
                setRecyclerView();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showPopup() {

        isClosed = false;

        popup = new PopupWindow(mContext);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setContentView(popupView);
        addWidgetsToListener(popupView);
        // Set popup window animation style.
        popup.setAnimationStyle(R.anim.up_from_bottom);
        popup.setTouchable(true);
        popup.setFocusable(true);
        popup.setOutsideTouchable(false);
        popup.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        popup.update();
        popup.showAtLocation(rootWindow, Gravity.CENTER, 0, 0);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                isClosed = true;
            }
        });
    }

    private void addWidgetsToListener(View view){
        TextView tvBrName = view.findViewById(R.id.id_dsr_det_result_br_name);
        TextView tvDate = view.findViewById(R.id.id_dsr_det_result_date);
        tvBrName.setText(brName);
        tvDate.setText(date);

        Button btnClose = view.findViewById(R.id.id_DSR_DT_close);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(popup != null){
                    popup.dismiss();
                }
            }
        });
    }

    private void setRecyclerView(){
        recyclerview = (RecyclerView) popupView.findViewById(R.id.id_DSR_DT_RV);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        recyclerview.setLayoutManager(layoutManager);
        //Add Divider
        ///////DividerItemDecoration itemDecor = new DividerItemDecoration(mContext, VERTICAL);
        ////////////recyclerview.addItemDecoration(itemDecor);
        //creating recyclerView adapter
        rvAdapter = new DSRDetailAdapter(mContext, listItems,popupView, String.valueOf(totalAmount)/*totalSales*/);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        //setting adapter to recyclerView
        recyclerview.setAdapter(rvAdapter);
    }


}
