package com.freshorders.freshorder.popup;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.ClosingStockAuditDetailAdapter;
import com.freshorders.freshorder.asyntask.ClosingStockAuditSendAsyncTask;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.model.StockAuditDisplayModel;
import com.freshorders.freshorder.model.StockAuditSendStock;
import com.freshorders.freshorder.model.StockAuditWholeModel;
import com.freshorders.freshorder.ui.ClosingStockAudit;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Log;
import com.freshorders.freshorder.utils.Utils;
import com.google.gson.Gson;

import org.json.JSONStringer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static android.widget.LinearLayout.VERTICAL;

public class PopupClosingStockAudit {

    private List<StockAuditSendStock> stockList;

    private Context mContext;
    private View popupView;
    private PopupWindow popup;
    private View rootWindow;

    private boolean isClosed = true;

    private RecyclerView mRecyclerView;
    private ClosingStockAuditDetailAdapter adapter;

    private List<StockAuditDisplayModel> mArrayList;
    private LinkedHashMap<Integer,Integer> enteredList;
    private LinkedHashMap<Integer,Integer> enteredStockList;//////////////need to remove only temp
    private LinkedHashMap<Integer,StockAuditDisplayModel> enteredListPair;
    private LinearLayout lineHeaderProgress ;

    ClosingStockAudit.PopupCloseI popupCloseI;

    public PopupClosingStockAudit(View rootWindow,
                                  Context mContext,
                                  List<StockAuditDisplayModel> arrayList,
                                  LinkedHashMap<Integer,Integer> enteredLists,
                                  LinkedHashMap<Integer,Integer> enteredStockList,
                                  LinkedHashMap<Integer,StockAuditDisplayModel> enteredListPairs,
                                  ClosingStockAudit.PopupCloseI popupCloseI) {
        mArrayList = arrayList;
        enteredList = enteredLists;
        enteredListPair = enteredListPairs;
        this.mContext = mContext;
        this.rootWindow = rootWindow;
        this.enteredStockList = enteredStockList;
        this.popupCloseI = popupCloseI;
        stockList = new ArrayList<>();
        createPopup();

    }

    private void createPopup() {
        try {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (layoutInflater != null) {
                popupView = layoutInflater.inflate(R.layout.popup_closing_stock_audit_detail, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showPopup() {

        isClosed = false;

        popup = new PopupWindow(mContext);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setContentView(popupView);
        addWidgetsToListener(popupView);
        // Set popup window animation style.
        popup.setAnimationStyle(R.anim.up_from_bottom);
        popup.setTouchable(true);
        popup.setFocusable(true);
        popup.setOutsideTouchable(false);
        popup.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        popup.update();
        popup.showAtLocation(rootWindow, Gravity.CENTER, 0, 0);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                isClosed = true;
            }
        });
    }

    private void setRecyclerView(View popupView){
        mRecyclerView = popupView.findViewById(R.id.id_RV_audit_detail);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(layoutManager);
        //Add Divider
        DividerItemDecoration itemDecor = new DividerItemDecoration(mRecyclerView.getContext(), VERTICAL);
        mRecyclerView.addItemDecoration(itemDecor);
        //creating recyclerView adapter
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        //////adapter = new ClosingStockAuditDetailAdapter(mArrayList, enteredList, enteredListPair); /////////need un comment
        adapter = new ClosingStockAuditDetailAdapter(mArrayList, enteredList,enteredStockList, enteredListPair);
        mRecyclerView.setAdapter(adapter);
    }

    private void addWidgetsToListener(final View popupView) {
        lineHeaderProgress = popupView.findViewById(R.id.lineHeaderProgress_home);
        setRecyclerView(popupView);
        Button btnSubmit = popupView.findViewById(R.id.id_audit_btn_submit_to_online);
        btnSubmit.setOnClickListener(btnSubmitListener);
        Button btnBack = popupView.findViewById(R.id.id_audit_btn_back);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupCloseI.dismiss();
                popup.dismiss();
            }
        });

    }

    public boolean isPopupVisible(){
        return isClosed;
    }

    public void close(){
        popup.dismiss();
    }

    View.OnClickListener btnSubmitListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Calendar c = Calendar.getInstance();
            System.out.println("Current time => "+c.getTime());
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
            String formattedDate = df.format(c.getTime());

            if(!(Constants.USER_FULLNAME != null && Constants.USER_FULLNAME.length() > 0)){
                getName();
            }

            if(Constants.USER_FULLNAME != null && Constants.USER_FULLNAME.length() > 0){
                if(enteredList.size() > 0) {
                    ////////////////////////////////////chang start

                    for(Map.Entry m : enteredList.entrySet()){
                        StockAuditSendStock item = new StockAuditSendStock();

                        item.setAuditqty((Integer) m.getValue());
                        Integer id = (Integer) m.getKey();
                        item.setStkauditid(id);

                        if(enteredStockList.get(id) != null){
                            item.setStockqty(enteredStockList.get(id));
                        }else {
                            StockAuditDisplayModel cur = enteredListPair.get(id);
                            if(cur != null){
                                item.setStockqty(cur.getStockqty());
                            }else {
                                item.setStockqty(0); ////////this should not happen
                            }
                        }
                        stockList.add(item);
                    }

                    /*for(int i = 0; i < mArrayList.size(); i++){
                        StockAuditDisplayModel cur = mArrayList.get(i);
                        int id = cur.getStkauditid();
                        if(enteredList.get(id) != null && enteredStockList.get(i) != null){
                            StockAuditSendStock item = new StockAuditSendStock();
                            item.setStkauditid(id);
                            item.setAuditqty(enteredList.get(id));
                            item.setStockqty(enteredStockList.get(id));
                            stockList.add(item);
                        }else if(enteredList.get(id) != null){
                            StockAuditSendStock item = new StockAuditSendStock();
                            item.setStkauditid(id);
                            item.setAuditqty(enteredList.get(id));
                            item.setStockqty(0);
                            stockList.add(item);
                        }else if(enteredStockList.get(i) != null){
                            StockAuditSendStock item = new StockAuditSendStock();
                            item.setStkauditid(id);
                            item.setAuditqty(0);
                            item.setStockqty(enteredStockList.get(id));
                            stockList.add(item);
                        }
                    }  */

                    ///////////////////////////////////////////chang close

                    StockAuditWholeModel obj = new StockAuditWholeModel();
                    obj.setUpdatedby(Constants.USER_FULLNAME);
                    obj.setUpdateddt(formattedDate);
                    obj.setAuditarray(stockList);

                    String strArrays = new Gson().toJson(obj);
                    Log.e("Send","..............." + strArrays);
                    String strArray = new Gson().toJson(stockList);
                    Log.e("Sample","..............." + strArrays);

                    try {
                        JSONStringer jsonStringer = new JSONStringer().object()
                                .key("updateddt").value(formattedDate)
                                .key("updatedby").value(Constants.USER_FULLNAME)
                                .key("auditarray")
                                .value(strArray)
                                .endObject();
                        Log.e("Sample","..............." + jsonStringer);
                        show();
                        JsonServiceHandler jsonServiceHandler = new JsonServiceHandler(Utils.strClosingStockAuditSubmit, strArrays, mContext);
                        ClosingStockAuditSendAsyncTask send = new ClosingStockAuditSendAsyncTask(jsonServiceHandler, new ClosingStockAuditSendAsyncTask.CompleteListener() {
                            @Override
                            public void onSuccess(String msg) {
                                hide();
                                showAlertDialogToastOK("Audit successfully completed");
                            }

                            @Override
                            public void onFailed(String msg) {
                                hide();
                                showAlertDialogToast(msg);
                            }

                            @Override
                            public void onException(String msg) {
                                hide();
                                showAlertDialogToast(msg);
                            }
                        });
                        send.execute();
                    } catch (Exception e) {
                        hide();
                        e.printStackTrace();
                        showAlertDialogToast("Somthink went wrong" + e.getMessage());
                    }
                }else {
                    showAlertDialogToast("Please make audit first");
                }
            }else {
                showAlertDialogToast("Can't get login detail please re-login");
                Log.e("fail","............................");
            }

        }
    };

    private String getName(){
        DatabaseHandler databaseHandler = new DatabaseHandler(mContext.getApplicationContext());
        final Cursor cur;
        cur = databaseHandler.getDetails();
        if(cur != null && cur.getCount() > 0) {
            cur.moveToFirst();
            Constants.USER_ID = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_id));
            Constants.USER_FULLNAME = cur.getString(cur
                    .getColumnIndex(DatabaseHandler.KEY_name));
            cur.close();
        }
        return Constants.USER_FULLNAME;
    }

    public void showAlertDialogToast(String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    public void showAlertDialogToastOK(String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
        builder1.setCancelable(false);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        if(popup != null){
                            popup.dismiss();
                        }
                        Intent menu = new Intent(mContext, ClosingStockAudit.class);
                        mContext.startActivity(menu);
                        ((Activity) mContext).finish();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    private void show(){
        lineHeaderProgress.setVisibility(View.VISIBLE);
    }

    private void hide(){
        lineHeaderProgress.setVisibility(View.GONE);
    }
}
