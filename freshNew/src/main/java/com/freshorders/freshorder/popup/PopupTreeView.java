package com.freshorders.freshorder.popup;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.model.HierarchyObjModel;
import com.freshorders.freshorder.treeview.Pair;
import com.freshorders.freshorder.treeview.TreeAdapter;
import com.freshorders.freshorder.treeview.TreeNode;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class PopupTreeView implements View.OnClickListener{

    public interface OnDismissListener{

        public void onDismiss(HierarchyObjModel selectedItem);

    }

    OnDismissListener listener;

    private Context mContext;
    private View popupView;
    private PopupWindow popup;
    private View rootWindow;
    private boolean isClosed = true;

    /////////////////////////////////
    public TreeAdapter mAdapter;
    public TreeNode[] treeNodes;
    //public TreeNode[] treeNodes1;

    private Map<Integer, Pair> in = new LinkedHashMap<>();
    private int inc = 0;
    List<Pair> list;

    ListView listView;
    private String filePath;
    private EditText eTxtPlace;

    private DisplayMetrics displayMetrics;

    public HierarchyObjModel selectedItem;

    private boolean isManagerApp = false;

    public PopupTreeView(View rootWindow,
                         Context mContext,
                         String filePath,
                         EditText eTxtPlace,
                         DisplayMetrics displayMetrics,
                         OnDismissListener listener){
        this.mContext = mContext;
        this.rootWindow = rootWindow;
        this.filePath = filePath;
        this.eTxtPlace = eTxtPlace;
        this.displayMetrics = displayMetrics;
        this.listener = listener;
        selectedItem = new HierarchyObjModel();
        createPopup();

    }

    public PopupTreeView(View rootWindow,
                         Context mContext,
                         String filePath,
                         EditText eTxtPlace,
                         DisplayMetrics displayMetrics,
                         boolean isManagerApp,
                         OnDismissListener listener){
        this.mContext = mContext;
        this.rootWindow = rootWindow;
        this.filePath = filePath;
        this.eTxtPlace = eTxtPlace;
        this.displayMetrics = displayMetrics;
        this.isManagerApp = isManagerApp;
        this.listener = listener;
        selectedItem = new HierarchyObjModel();
        createPopup();

    }

    private void createPopup() {
        try {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (layoutInflater != null) {
                popupView = layoutInflater.inflate(R.layout.popup_hierarchy_place, null);
                read();
                read1();
                if(isManagerApp){
                    finalListForManagerApp();
                }else {
                    finalList();
                }
                setListView();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showPopup() {

        isClosed = false;

        popup = new PopupWindow(mContext);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setHeight(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setContentView(popupView);
        //addWidgetsToListener(popupView);
        // Set popup window animation style.
        popup.setAnimationStyle(R.anim.up_from_bottom);
        popup.setTouchable(true);
        popup.setFocusable(true);
        popup.setOutsideTouchable(false);
        popup.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        popup.update();
        popup.showAtLocation(rootWindow, Gravity.CENTER, 0, 0);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                isClosed = true;
            }
        });
    }

    public void showPopupWindow() {
        try {

            int pxWidth = displayMetrics.widthPixels;
            float dpWidth = pxWidth / displayMetrics.density;
            int pxHeight = displayMetrics.heightPixels;
            float dpHeight = pxHeight / displayMetrics.density;
            // Set alert dialog width equal to screen width 90%
            int dialogWindowWidth = (int) (pxWidth * 0.6f);
            // Set alert dialog height equal to screen height 60%
            int dialogWindowHeight = (int) (pxHeight * 0.6f);

            //popWindowInflated = LayoutInflater.from(mContext).inflate(R.layout.popup_survey_payment, null);

            popup = new PopupWindow(mContext);
            popup.setWidth(dialogWindowWidth);
            popup.setHeight(dialogWindowHeight);

            popup.setContentView(popupView);

            popup.setTouchInterceptor(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    return false;
                }
            });

            popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                    isClosed = true;
                    listener.onDismiss(selectedItem);
                }
            });
            // Displaying the popup at the specified location, + offsets.
            //////////////popup.showAtLocation(popWindowInflated, Gravity.CENTER, 0, 0);  hide by Kumaravel 01-07-2019
            popup.showAtLocation(rootWindow, Gravity.CENTER, 0, 0);
            ////addWidgetsToListener(popWindowInflated);
            popup.setFocusable(true);
            popup.setTouchable(true);
            popup.setOutsideTouchable(false);
            // Set popup window animation style.
            popup.setAnimationStyle(R.anim.slide_in_up);
            popup.update();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setListView(){
        listView = popupView.findViewById(R.id.id_hierarchy_list);
        mAdapter = new TreeAdapter(mContext, treeNodes);
        mAdapter.on_click_listener = this;
        listView.setAdapter(mAdapter);
    }

    private void finalListForManagerApp(){
        List<Pair> childList = new ArrayList<>();
        Map<Integer, Integer> pare_child = new HashMap<>();
        treeNodes = new TreeNode[list.size()];
        int count = 0;
        for(Pair cur: list){  /// this is root node purpose
            String parentId = cur.getParentId();
            if(parentId == null || parentId.equalsIgnoreCase("null") || parentId.isEmpty()){
                treeNodes[count] = new TreeNode(null, cur.getName(), cur);
                pare_child.put(Integer.valueOf(cur.getChildId()),count);
                //////////list.remove(count);
                count++;
            }else {
                boolean haveParent = false;

                for(Pair curI: list){
                    String childId = curI.getChildId();
                    if(childId.equalsIgnoreCase(parentId)){
                        haveParent = true;
                        break;
                    }
                }
                if(haveParent) {
                    childList.add(cur);
                }else {
                    treeNodes[count] = new TreeNode(null, cur.getName(), cur);
                    pare_child.put(Integer.valueOf(cur.getChildId()),count);
                    //////////list.remove(count);
                    count++;
                }
            }
        }

        for(Pair cur: childList){
            Gson gson = new Gson();
            String jsonInString = gson.toJson(cur);
            Log.e("Child", "............................................." + jsonInString);
            String parentId = cur.getParentId();
            if( parentId != null){
                Object objIndex = pare_child.get(Integer.valueOf(parentId));
                if(objIndex != null) {
                    int index = (int) objIndex;
                    treeNodes[count] = new TreeNode(treeNodes[index], cur.getName(), cur);
                    pare_child.put(Integer.valueOf(cur.getChildId()), count);
                    count++;
                }
            }
        }
    }

    private void finalList(){
        List<Pair> childList = new ArrayList<>();
        Map<Integer, Integer> pare_child = new HashMap<>();
        treeNodes = new TreeNode[list.size()];
        int count = 0;
        for(Pair cur: list){
            String parentId = cur.getParentId();
            if(parentId == null || parentId.equalsIgnoreCase("null") || parentId.isEmpty()){
                treeNodes[count] = new TreeNode(null, cur.getName(), cur);
                pare_child.put(Integer.valueOf(cur.getChildId()),count);
                //////////list.remove(count);
                count++;
            }else {
                childList.add(cur);
            }
        }

        for(Pair cur: childList){
            Gson gson = new Gson();
            String jsonInString = gson.toJson(cur);
            Log.e("Child", "............................................." + jsonInString);
            String parentId = cur.getParentId();
            if( parentId != null){
                Object objIndex = pare_child.get(Integer.valueOf(parentId));
                if(objIndex != null) {
                    int index = (int) objIndex;
                    treeNodes[count] = new TreeNode(treeNodes[index], cur.getName(), cur);
                    pare_child.put(Integer.valueOf(cur.getChildId()), count);
                    count++;
                }
            }
        }
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            //InputStream is = getAssets().open("hierarchy.json");
            //FileInputStream fin = mContext.openFileInput(filePath);
            File file = new File(filePath);
            InputStream is = new FileInputStream(file);
            int size = is.available();
            byte[] buffer = new byte[size];
            int read = is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private void read1(){

        list = new ArrayList<>();

        for(Map.Entry<Integer, Pair> entry : in.entrySet()) {
            Integer key = entry.getKey();
            Pair value = entry.getValue();
            ///////////////////
            list.add(value);
            ////////////////
            Gson gson = new Gson();
            // 2. Java object to JSON string
            String jsonInString = gson.toJson(value);
            Log.e("!!!!!!!!!!", "level" + key + "value ..." + jsonInString);
        }

    }

    private void read(){

        String inputJson = loadJSONFromAsset();

        if(inputJson == null){
            showAlertDialogToast(mContext.getResources().getString(R.string.data_not_load_when_login));
            return;
        }

        System.out.println("input json: " + inputJson);

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode rootNode = null;
        try {
            rootNode = objectMapper.readTree(inputJson);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.printf("root: %s type=%s%n", rootNode, rootNode.getNodeType());
        traverse(rootNode, 1);
    }


    private void traverse(JsonNode node, int level) {
        if (node.getNodeType() == JsonNodeType.ARRAY) {
            traverseArray(node, level);
        } else if (node.getNodeType() == JsonNodeType.OBJECT) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                traverseObject(node, level);
            }else {
                traverseObject_old_form(node, level);
                Log.e("Main....",".................................................................");
            }

        } else {
            throw new RuntimeException("Not yet implemented");
        }
    }

    private void traverseObject(final JsonNode node, final int level) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            node.fieldNames().forEachRemaining(new Consumer<String>() {
                @Override
                public void accept(String fieldName) {
                    JsonNode childNode = node.get(fieldName);
                    printNode(childNode, fieldName, level);
                    //for nested object or arrays
                    if (traversable(childNode)) {
                        traverse(childNode, level + 1);
                    }
                }
            });
        }
    }

    private void traverseObject_old_form(final JsonNode node, final int level) {
        Iterator<String> stringIterator = node.fieldNames();
        while (stringIterator.hasNext()){
            String fieldName = stringIterator.next();
            JsonNode childNode = node.get(fieldName);
            printNode(childNode, fieldName, level);
            //////////////////////////////////////
            String value = null;
            if (node.isTextual()) {
                value = node.textValue();
            } else if (node.isNumber()) {
                value = String.valueOf(node.numberValue());
            }//t
            //////////formTree(fieldName, value, inc);
            //////////////////////////////////
            //for nested object or arrays
            if (traversable(childNode)) {
                traverse(childNode, level + 1);
            }
        }
        inc++;

    }

    private void formTree(String name, String value, int level){
        if(name.equalsIgnoreCase("levelid") ||
                name.equalsIgnoreCase("parentlevelid") ||
                name.equalsIgnoreCase("name")) {
            if (in.get(level) != null) {
                Pair from = in.get(level);
                if (from != null) {
                    switch (name) {
                        case "levelid":
                            from.setChildId(value);
                            break;
                        case "parentlevelid":
                            from.setParentId(value);
                            break;
                        case "name":
                            from.setName(value);
                            break;
                    }
                    in.put(level, from);
                }
            } else {
                Pair p = new Pair();
                switch (name){
                    case "levelid" :
                        p.setChildId(value);
                        break;
                    case "parentlevelid" :
                        p.setParentId(value);
                        break;
                    case "name" :
                        p.setName(value);
                        break;
                }
                in.put(level, p);
            }
        }
    }

    private void traverseArray(JsonNode node, int level) {
        for (JsonNode jsonArrayNode : node) {
            printNode(jsonArrayNode, "arrayElement", level);
            inc++;////////////////////
            if (traversable(jsonArrayNode)) {
                traverse(jsonArrayNode, level + 1);
            }
        }
    }

    private boolean traversable(JsonNode node) {
        return node.getNodeType() == JsonNodeType.OBJECT ||
                node.getNodeType() == JsonNodeType.ARRAY;
    }

    private void printNode(JsonNode node, String keyName, int level) {
        if (traversable(node)) {
            System.out.printf("%" + (level * 4 - 3) + "s|-- %s=%s type=%s%n",
                    "", keyName, node.toString(), node.getNodeType());

        } else {
            Object value = null;
            if (node.isTextual()) {
                value = node.textValue();
            } else if (node.isNumber()) {
                value = node.numberValue();
            }//todo add more types
            System.out.printf("%" + (level * 4 - 3) + "s|-- %s=%s type=%s%n",
                    "", keyName, value, node.getNodeType());

            addTo(keyName, String.valueOf(value), inc);
        }
    }

    private void addTo(String name, String value, int level){
        if(name.equalsIgnoreCase("levelid") ||
                name.equalsIgnoreCase("parentlevelid") ||
                name.equalsIgnoreCase("name")) {
            if (in.get(level) != null) {
                Pair from = in.get(level);
                if (from != null) {
                    switch (name) {
                        case "levelid":
                            from.setChildId(value);
                            break;
                        case "parentlevelid":
                            from.setParentId(value);
                            break;
                        case "name":
                            from.setName(value);
                            break;
                    }
                    in.put(level, from);
                }
            } else {
                Pair p = new Pair();
                switch (name){
                    case "levelid" :
                        p.setChildId(value);
                        break;
                    case "parentlevelid" :
                        p.setParentId(value);
                        break;
                    case "name" :
                        p.setName(value);
                        break;
                }
                in.put(level, p);
            }
        }
    }


    @Override
    public void onClick(View view) {

        TreeNode n;

        n = (TreeNode) view.getTag();

        if(view instanceof CheckBox){
            n.is_open = ((CheckBox) view).isChecked();
            this.mAdapter.clear();
            this.mAdapter.addAll(TreeNode.Get_Visible_Nodes(null, this.treeNodes));
        }else {
            List<String> name = new ArrayList<>();
            name.add(n.data.toString());
            Pair cur = n.pair;
            selectedItem.setLevelid(Integer.parseInt(cur.getChildId()));
            String parentId = cur.getParentId();
            if(parentId == null || parentId.isEmpty()
                    || parentId.equalsIgnoreCase("null")){
                selectedItem.setParentlevelid(-1);//////////////
            }else {
                selectedItem.setParentlevelid(Integer.parseInt(parentId));
            }
            selectedItem.setName(cur.getName());
            while (true) {
                if (n.parent != null) {
                    name.add(n.parent.data.toString());
                    n = n.parent;
                }else {
                    break;
                }
            }
            Log.e("Print -", name.toString());
            StringBuilder place = new StringBuilder();
            for(int i = name.size() - 1; i >= 0; i--){
                String temp = name.get(i);
                if(i == 0){
                    place.append(temp);
                }else {
                    place.append(temp).append("-");
                }
            }
            eTxtPlace.setText(place.toString());
            //Gson gson = new Gson();
            //String json = gson.toJson(selectedItem);
            //Log.e("JSON", "..................::" + json);
            popup.dismiss();
            //Toast.makeText(mContext,n.data.toString(),Toast.LENGTH_LONG).show();
        }
    }


    public void showAlertDialogToast(String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

}
