package com.freshorders.freshorder.popup;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.domain.MerchantOrderDomainSelected;
import com.freshorders.freshorder.ui.CreateOrderActivity;
import com.freshorders.freshorder.ui.MerchantOrderDetailActivity;
import com.freshorders.freshorder.utils.Constants;

import java.util.ArrayList;

import static com.freshorders.freshorder.utils.Constants.SHOULD_NOT_EMPTY_DATA;
import static com.freshorders.freshorder.utils.Constants.SHOULD_NOT_ONLY_DOT;

public class Template1QuantityEditPopup {

    private Context mContext;
    private View popupView;
    private PopupWindow popup;
    private View rootWindow;
    private String closingStock, orderQuantity;
    private int position;
    String prodid;
    private ArrayList<MerchantOrderDomainSelected> arraylistMerchantOrderDetailList;

    public Template1QuantityEditPopup(Context mContext, String closingStock,
                                      String orderQuantity, View rootWindow,
                                      int position, String prodid,
                                      ArrayList<MerchantOrderDomainSelected> arraylistMerchantOrderDetailList) {
        this.mContext = mContext;
        this.closingStock = closingStock;
        this.orderQuantity = orderQuantity;
        this.rootWindow = rootWindow;
        this.position = position;
        this.prodid = prodid;
        this.arraylistMerchantOrderDetailList = arraylistMerchantOrderDetailList;
        createPopup();
    }

    private void createPopup() {
        try {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (layoutInflater != null) {
                popupView = layoutInflater.inflate(R.layout.popup_edit_template1, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showPopup() {
        popup = new PopupWindow(mContext);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setContentView(popupView);
        addWidgetsToListener(popupView);
        // Set popup window animation style.
        popup.setAnimationStyle(R.anim.up_from_bottom);
        popup.setTouchable(true);
        popup.setFocusable(true);
        popup.setOutsideTouchable(false);
        popup.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        popup.update();
        popup.showAtLocation(rootWindow, Gravity.CENTER, 0, 0);

    }

    private void addWidgetsToListener(View popupView) {
        final EditText eTxtStock = (EditText) popupView.findViewById(R.id.editTextClosingStock);
        final EditText eTxtQuantity = (EditText) popupView.findViewById(R.id.editTextOrderQuantity);
        final TextView txtNotification = (TextView) popupView.findViewById(R.id.id_pop_template1_alert);
        Button btnCancel = (Button) popupView.findViewById(R.id.id_pop_template1_edit_btn_cancel);
        Button btnUpdate = (Button) popupView.findViewById(R.id.id_pop_template1_edit_btn_update);
        eTxtStock.setText(closingStock);
        eTxtQuantity.setText(orderQuantity);

        TextView tvItemName = (TextView) rootWindow.findViewById(R.id.textViewName);
        String itemName = tvItemName.getText().toString().trim();
        TextView tvPopupItemName = (TextView) popupView.findViewById(R.id.id_pop_template1_order_name);
        tvPopupItemName.setText(itemName);

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String stock = eTxtStock.getText().toString().trim();
                String quantity = eTxtQuantity.getText().toString().trim();
                if(!stock.isEmpty() && !quantity.isEmpty()){
                    if(stock.length() == 1 || quantity.length() == 1){
                        if(stock.startsWith(".") || quantity.startsWith(".")){
                            txtNotification.setVisibility(View.VISIBLE);
                            txtNotification.setText(SHOULD_NOT_ONLY_DOT);
                        }else {
                            txtNotification.setVisibility(View.INVISIBLE);
                            TextView tvClosingStock = (TextView) rootWindow.findViewById(R.id.textViewClosingStock);
                            TextView tvQuantity = (TextView) rootWindow.findViewById(R.id.textViewQuantity);
                            tvClosingStock.setText(stock);
                            tvQuantity.setText(quantity);
                            updateStockQuantity(stock, quantity);
                        }
                    }else {
                        txtNotification.setVisibility(View.INVISIBLE);
                        TextView tvClosingStock = (TextView) rootWindow.findViewById(R.id.textViewClosingStock);
                        TextView tvQuantity = (TextView) rootWindow.findViewById(R.id.textViewQuantity);
                        tvClosingStock.setText(stock);
                        tvQuantity.setText(quantity);
                        updateStockQuantity(stock, quantity);
                    }
                }else {
                    txtNotification.setVisibility(View.VISIBLE);
                    txtNotification.setText(SHOULD_NOT_EMPTY_DATA);
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
            }
        });

    }

    private void updateStockQuantity(String stock, String quantity){

        if (Constants.USER_TYPE.equals("M")) {
            for(int i = 0; i < MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.size(); i++){
                MerchantOrderDomainSelected item = MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.get(i);
                if (item.getprodid().equals(prodid)) {
                    item.setOutstock(stock);
                    item.setCurrStock(stock);//////////
                    item.setqty(quantity);
                    MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.set(i, item);
                    MyApplication.previousSelectedForSearch.put(prodid, item);
                    break;
                }
            }
        }else {
            for(int i = 0; i < CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size(); i++) {
                MerchantOrderDomainSelected item = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i);
                if (item.getprodid().equals(prodid)) {
                    item.setOutstock(stock);
                    item.setCurrStock(stock);//////////
                    item.setqty(quantity);
                    CreateOrderActivity.arraylistMerchantOrderDetailListSelected.set(i, item);
                    MyApplication.previousSelectedForSearch.put(prodid, item); // for search and back operation
                    break;
                }
            }
        }

        arraylistMerchantOrderDetailList.get(position).setqty(quantity);
        arraylistMerchantOrderDetailList.get(position).setOutstock(stock);

        popup.dismiss();
    }
}
