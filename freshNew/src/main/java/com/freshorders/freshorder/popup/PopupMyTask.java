package com.freshorders.freshorder.popup;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.warkiz.widget.IndicatorSeekBar;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/*

 for seek bar

 Copyright (C) 2017 zhuangguangquan

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */

public class PopupMyTask {

    public interface PopupListener{
        public void dismiss();
        public void isPopupAvail(boolean isShowing);
        public void interruptOccur(String msg);
    }

    private Context mContext;
    private View popupView;
    private View rootView;
    private PopupMyTask.PopupListener listener;
    private DisplayMetrics displayMetrics;
    private DateTimeFormatter dateTimeFormatter;

    private String head, percentage;

    public PopupMyTask(Context mContext,
                       String head,
                       String percentage,
                       View rootView,
                       DisplayMetrics displayMetrics,
                       PopupMyTask.PopupListener listener) {
        this.listener = listener;
        this.mContext = mContext;
        this.rootView = rootView;
        this.displayMetrics = displayMetrics;
        this.head = head;
        this.percentage = percentage;
        createPopup();
    }

    private PopupWindow popup;

    private void createPopup() {
        try {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (layoutInflater != null) {
                popupView = layoutInflater.inflate(R.layout.popup_my_task, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showPopupWindow() {

        try {

            int pxWidth = displayMetrics.widthPixels;
            float dpWidth = pxWidth / displayMetrics.density;
            int pxHeight = displayMetrics.heightPixels;
            float dpHeight = pxHeight / displayMetrics.density;
            // Set alert dialog width equal to screen width 90%
            int dialogWindowWidth = (int) (pxWidth * 0.8f);
            // Set alert dialog height equal to screen height 60%
            int dialogWindowHeight = (int) (pxHeight * 0.6f);

            //popWindowInflated = LayoutInflater.from(mContext).inflate(R.layout.popup_survey_payment, null);

            popup = new PopupWindow(mContext);
            popup.setWidth(dialogWindowWidth);
            popup.setHeight(dialogWindowHeight);

            popup.setContentView(popupView);

            popup.setTouchInterceptor(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    return false;
                }
            });

            popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
                @Override
                public void onDismiss() {
                    //isClosed = true;
                    if(listener != null) {
                        listener.dismiss();
                    }
                }
            });
            // Displaying the popup at the specified location, + offsets.
            //////////////popup.showAtLocation(popWindowInflated, Gravity.CENTER, 0, 0);  hide by Kumaravel 01-07-2019
            popup.showAtLocation(rootView, Gravity.CENTER, 0, 0);

                    addWidgetsToListener(popupView);

            popup.setFocusable(true);
            popup.setTouchable(true);
            popup.setOutsideTouchable(false);
            // Set popup window animation style.
            popup.setAnimationStyle(R.anim.slide_in_up);
            popup.update();


        } catch (Exception e) {
            e.printStackTrace();
            if(listener != null){
                listener.interruptOccur(mContext.getResources().getString(R.string.technical_fault) + "  :" + e.getMessage());
            }
            if(popup != null){
                popup.dismiss();
            }
        }
    }

    private void addWidgetsToListener(View view){
        TextView task = view.findViewById(R.id.id_pop_task_task_name);
        TextView dueDate = view.findViewById(R.id.id_pop_task_date);
        IndicatorSeekBar seekBar = view.findViewById(R.id.id_pop_task_seek_bar);

        task.setText(head);
        seekBar.setProgress(Float.parseFloat(percentage));

        dateTimeFormatter = DateTimeFormat.forPattern("dd-MM-yyyy");

        String date = dateTimeFormatter.print(new DateTime());
        dueDate.setText(date);

    }
}
