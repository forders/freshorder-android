package com.freshorders.freshorder.popup;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.domain.DealerOrderDetailDomain;
import com.freshorders.freshorder.ui.SalesmanOrderDetailsActivity;

import static com.freshorders.freshorder.utils.Constants.QUANTITY_TEXT_FOR_TEMPLATE;
import static com.freshorders.freshorder.utils.Constants.SHOULD_NOT_EMPTY_DATA;
import static com.freshorders.freshorder.utils.Constants.SHOULD_NOT_ONLY_DOT;

public class Template1QuantityUpdatePopup {

    private Context mContext;
    private View popupView;
    private PopupWindow popup;
    private View rootWindow;
    private String closingStock, orderQuantity;
    private int curPosition;

    public Template1QuantityUpdatePopup(Context mContext, String closingStock,
                                        String orderQuantity, View rootWindow, int curPosition) {
        this.mContext = mContext;
        this.closingStock = closingStock;
        this.orderQuantity = orderQuantity;
        this.rootWindow = rootWindow;
        this.curPosition = curPosition;
        createPopup();
    }

    private void createPopup() {
        try {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (layoutInflater != null) {
                popupView = layoutInflater.inflate(R.layout.popup_edit_template1, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showPopup() {
        popup = new PopupWindow(mContext);
        popup.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setContentView(popupView);
        addWidgetsToListener(popupView);
        // Set popup window animation style.
        popup.setAnimationStyle(R.anim.up_from_bottom);
        popup.setTouchable(true);
        popup.setFocusable(true);
        popup.setOutsideTouchable(false);
        popup.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        popup.update();
        popup.showAtLocation(rootWindow, Gravity.CENTER, 0, 0);

    }

    private void addWidgetsToListener(View popupView) {
        final EditText eTxtStock = (EditText) popupView.findViewById(R.id.editTextClosingStock);
        final EditText eTxtQuantity = (EditText) popupView.findViewById(R.id.editTextOrderQuantity);
        final TextView txtNotification = (TextView) popupView.findViewById(R.id.id_pop_template1_alert);
        Button btnCancel = (Button) popupView.findViewById(R.id.id_pop_template1_edit_btn_cancel);
        Button btnUpdate = (Button) popupView.findViewById(R.id.id_pop_template1_edit_btn_update);
        eTxtStock.setText(closingStock);
        eTxtQuantity.setText(orderQuantity);

        eTxtStock.setVisibility(View.GONE);

        TextView tvItemName = (TextView) rootWindow.findViewById(R.id.textViewProductName);
        String itemName = tvItemName.getText().toString().trim();
        TextView tvPopupItemName = (TextView) popupView.findViewById(R.id.id_pop_template1_order_name);
        tvPopupItemName.setText(itemName);

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String stock = eTxtStock.getText().toString().trim();
                String quantity = eTxtQuantity.getText().toString().trim();
                if(!quantity.isEmpty()){  //!stock.isEmpty() &&
                    if(quantity.length() == 1){  // stock.length() == 1 ||
                        if(quantity.startsWith(".")){ // stock.startsWith(".") ||
                            txtNotification.setVisibility(View.VISIBLE);
                            txtNotification.setText(SHOULD_NOT_ONLY_DOT);
                        }else {
                            txtNotification.setVisibility(View.INVISIBLE);
                            TextView tvQuantity = (TextView) rootWindow.findViewById(R.id.textViewQty);
                            String formattedQuantity =   quantity + QUANTITY_TEXT_FOR_TEMPLATE;
                            tvQuantity.setText(formattedQuantity);
                            calculatePrice(curPosition, quantity);
                        }
                    }else {
                        txtNotification.setVisibility(View.INVISIBLE);
                        TextView tvQuantity = (TextView) rootWindow.findViewById(R.id.textViewQty);
                        String formattedQuantity = quantity + QUANTITY_TEXT_FOR_TEMPLATE;
                        tvQuantity.setText(formattedQuantity);
                        calculatePrice(curPosition, quantity);
                    }
                }else {
                    txtNotification.setVisibility(View.VISIBLE);
                    txtNotification.setText(SHOULD_NOT_EMPTY_DATA);
                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popup.dismiss();
            }
        });

    }

    private void calculatePrice(int curPosition, String quantity){
        DealerOrderDetailDomain item = SalesmanOrderDetailsActivity.arraylistDealerOrderDetailList.get(curPosition);
        item.setqty(quantity);
        Double updtpval = Integer.parseInt(quantity) * (Double.parseDouble(item.getProdprice()) + (Double.parseDouble(item.getProdprice()) * (Double.parseDouble(item.getProductTax()) / 100)));
        item.setPvalue(String.valueOf(updtpval));

        SalesmanOrderDetailsActivity.arraylistDealerOrderDetailList.set(curPosition , item);


        popup.dismiss();

    }
}
