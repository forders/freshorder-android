package com.freshorders.freshorder.db;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.util.Log;

import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.freshorders.freshorder.service.PrefManager.KEY_PATH_HIERARCHY_PLACE_FILE;
import static com.freshorders.freshorder.utils.Constants.MILK_CHECK_TBL_PRIMARY_KEY_VALUE;

public class DatabaseHandler extends SQLiteOpenHelper {

    //02-08-2019
    public static final String TBL_DISTRIBUTOR = "distributor";
    public static final String KEY_DISTRIBUTOR_SERIAL_NO = "srno";
    public static final String KEY_DISTRIBUTOR_ID = "distributorid";
    public static final String KEY_DISTRIBUTOR_NAME = "distributorname";
    public static final String KEY_DISTRIBUTOR_MOBILE = "mobileno";



    //17-07-2019
    public static final String TBL_MENU = "menu";
    public static final String KEY_MENU_SERIAL_NO = "srno";
    public static final String KEY_MENU_NAME = "name";
    public static final String KEY_MENU_IS_VISIBLE = "isVisible";
    public static final String KEY_MENU_CODE = "code";

    //22/05/2019
    public static final String TBL_LOCATION = "locationTBL";
    public static final String KEY_LOCATION_SERIAL_NO = "srno";
    public static final String KEY_LOCATION_DATE = "date";
    public static final String KEY_LOCATION_LOCATION = "location";
    public static final String KEY_LOCATION_TIME = "time";
    public static final String KEY_LOCATION_DATE_TIME = "datetime";
    public static final String KEY_LOCATION_ACCURACY = "accuracy";
    public static final String KEY_LOCATION_IS_SERVER = "isServer";
    public static final String KEY_LOCATION_LAT= "lat";
    public static final String KEY_LOCATION_LON= "lon";
    public static final String KEY_LOCATION_P_LAT= "p_lat";
    public static final String KEY_LOCATION_P_LON= "p_lon";
    public static final String KEY_LOCATION_PREVIOUS_ID = "pre_id";
    public static final String KEY_LOCATION_UNIQUE_ID = "unique_id";
    public static final String KEY_LOCATION_SUSER_ID = "suser_id";

    //21/05/2019
    public static final String TBL_ERROR = "errorTBL";
    public static final String KEY_ERROR_SERIAL_NO = "srno";
    public static final String KEY_ERROR_DATE = "date";
    public static final String KEY_ERROR_MESSAGE = "message";
    public static final String KEY_ERROR_TIME = "time";

    //Kumaravel 07/03/2019
    public static final String KEY_MERCHANT_ORDER_ID = "id";
    public static final String KEY_MERCHANT_ORDER_MERCHANT_OFFLINE_ID = "merchant_offline_id";
    public static final String KEY_MERCHANT_ORDER_ORDER_OFFLINE_ID = "order_offline_id";
    public static final String KEY_MERCHANT_ORDER_MERCHANT_ID = "merchant_id";
    //Kumaravel 31/01/2019
    public static final String KEY_DISTRIBUTOR_STOCK_ID = "id";
    public static final String KEY_DISTRIBUTOR_STOCK_URL = "url";
    public static final String KEY_DISTRIBUTOR_STOCK_SYNC_DATA = "syncData";
    public static final String KEY_DISTRIBUTOR_STOCK_MIGRATION_STATUS = "isMigrated";
    public static final String KEY_DISTRIBUTOR_STOCK_FAILED_REASON = "failedReason";
    //Kumaravel 31/01/2019
    public static final String KEY_SURVEY_ID = "id";
    public static final String KEY_SURVEY_URL = "url";
    public static final String KEY_SURVEY_SYNC_DATA = "syncData";
    public static final String KEY_SURVEY_MIGRATION_STATUS = "isMigrated";
    public static final String KEY_SURVEY_FAILED_REASON = "failedReason";
    public static final String KEY_SURVEY_MERCHANT_PRIMARY_KEY = "merchant_primary_key_for_servey";
    //Kumaravel 26/01/2019
    public static final String KEY_PKD_ID = "id";
    public static final String KEY_PKD_URL = "url";
    public static final String KEY_PKD_SYNC_DATA = "syncData";
    public static final String KEY_PKD_MIGRATION_STATUS = "isMigrated";
    public static final String KEY_PKD_FAILED_REASON = "failedReason";
    public static final String KEY_PKD_MERCHANT_PRIMARY_KEY = "merchant_primary_key";
    //Kumaravel 19/01/2019 PostNote Key
    public static final String KEY_POST_NOTE_ID = "id";
    public static final String KEY_POST_NOTE_USER_ID = "muserid";
    public static final String KEY_POST_NOTE_DESCRIPTION = "particular";
    public static final String KEY_POST_NOTE_D_USER_ID = "duserid";
    public static final String KEY_POST_NOTE_IS_NEED_TO_MIGRATE = "isNeedToMigrate";
    public static final String KEY_POST_NOTE_FAILED_REASON = "failedReason";
    public static final String KEY_POST_NOTE_IMAGE = "image";
    //Kumaravel 10/01/2019
    public static final String KEY_ORDER_INFO_ID = "id";
    public static final String KEY_ORDER_INFO_ORDER_ID = "orderId";
    public static final String KEY_IS_ORDER_PROCESSED = "isOrderProcessed";

    // Kumaravel 25/12/2018
    public static final String KEY_TEMPLATE_ID = "id";
    public static final String KEY_TEMPLATE_USER_ID = "userid";
    public static final String KEY_TEMPLATE_KEY = "templatekey";
    public static final String KEY_TEMPLATE_VALUE = "templatevalue";

    // Milk rate checking Table
    public static final String KEY_MILK_CHECK_ID = "id";
    public static final String KEY_FAT_SNF_COLUMN_COUNT = "fatsnfcount";
    public static final String KEY_SNF_CAL_COLUMN_COUNT = "snfcalcount";
    public static final String KEY_IS_FAT_SNF_TBL_LOADED = "isfatsnfloaded";
    public static final String KEY_IS_SNF_CAL_TBL_LOADED = "issnfcalloaded";

    // Dairy (Milk) FAT SNF Rate table
    public static final String KEY_FAT_SNF_ID = "id";
    public static final String KEY_FAT_SNF_RATE_ID = "fatsnfrateid";
    public static final String KEY_USR_ID = "userid";
    public static final String KEY_FAT = "fat";
    public static final String KEY_SNF_VALUE = "snfvalue";
    public static final String KEY_RATE = "rate";
    public static final String KEY_STATUS = "status";
    public static final String KEY_CREATED_BY = "createdby";
    public static final String KEY_CREATED_DATE = "createddt";
    public static final String KEY_LAST_UPDATED_BY = "lastupdatedby";
    public static final String KEY_LAST_UPDATED_DATE = "lastupdateddt";

    // SNF Cal table
    public static final String KEY_SNF_CAL_ID = "id";
    public static final String KEY_SNF_CALC__ID = "snfcalcid";
    public static final String KEY_SNF_CAL_USR_ID = "userid";
    public static final String KEY_SNF_CAL_TEMPERATURE = "temperature";
    public static final String KEY_SNF_CAL_IR_VALUE = "lrvalue";
    public static final String KEY_SNF_CAL_SNF_VALUE = "snfvalue";
    public static final String KEY_SNF_CAL_STATUS = "status";
    public static final String KEY_SNF_CAL_CREATED_BY = "createdby";
    public static final String KEY_SNF_CAL_CREATED_DATE = "createddt";
    public static final String KEY_SNF_CAL_LAST_UPDATED_BY = "lastupdatedby";
    public static final String KEY_SNF_CAL_LAST_UPDATED_DATE = "lastupdateddt";


    //clientvisit
    public static final String KEY_planid = "id";
    public static final String KEY_salesid = "suserid";
    public static final String KEY_merchid = "muserid";
    public static final String KEY_plandt = "orderplndt";
    public static final String KEY_remarks = "remarks";
    public static final String KEY_status = "status";
    public static final String KEY_updtdate = "updateddt";
    public static final String KEY_lat = "geolat";
    public static final String KEY_long = "geolong";
    public static final String KEY_dealid = "duserid";
    public static final String KEY_clientstatus = "clientstatus";
    public static final String KEY_Merchantname = "mname";

    //signin
    public static final String KEY_id = "id";
    public static final String KEY_username = "username";
    public static final String KEY_password = "password";
    public static final String KEY_usertype = "usertype";
    public static final String KEY_name = "name";
    public static final String KEY_payment = "payment";
    public static final String KEY_paymentStatus = "paymentStatus";
    public static final String KEY_email = "email";
    public static final String KEY_paymentdate = "nextpymtdt";
    public static final String KEY_clientdealid= "dealid";
    public static final String KEY_selectedBeat= "beat";
    public static final String KEY_selectedDate= "date";

    //Dealerproduct
    public static final String KEY_companyname = "companyname";
    public static final String KEY_userid = "userid";
    public static final String KEY_prodcode = "prodcode";
    public static final String KEY_prodid = "prodid";
    public static final String KEY_prodname = "prodname";
    public static final String KEY_updateddt = "updateddt ";
    public static final String KEY_currentdate = "currentdate ";
    public static final String KEY_quantity = "quantity";
    public static final String KEY_freeqty = "freeqty";
    public static final String KEY_prodprice = "prodprice";
    public static final String KEY_prodtax = "prodtax";
    public static final String KEY_mfgdate = "mfgdate ";
    public static final String KEY_expdate = "expdate ";
    public static final String KEY_Prodbatchno = "Prodbatchno ";
    public static final String KEY_ProdUOM = "uom";
    public static final String KEY_serialNo = "serialno";
    public static final String KEY_unitGram = "unitgram";
    public static final String KEY_unitperuom = "unitperuom";
    public static final String KEY_prodimage = "prodimage";
    public static final String KEY_currstock = "currstock";

    //Constants Table

    public static final String KEY_Key = "key";
    public static final String KEY_Value = "value";

    //Dealertable
    public static final String KEY_Dcompanyname = "companyname";
    public static final String KEY_Duserid = "userid";
    public static final String KEY_usrdlrid = "usrdlrid";
    public static final String KEY_Dfullname = "fullname";
    public static final String KEY_Daddress = "address";
    public static final String KEY_date = "currentdate";


    //merchant table
    public static final String KEY_Mcompanyname = "companyname";
    public static final String KEY_Muserid = "userid";
    public static final String KEY_Mfullname = "fullname";
    public static final String KEY_MFlag = "cflag";
    public static final String KEY_MOBILENUMER = "mobileno";
    public static final String KEY_EMAIL = "emailid";
    public static final String KEY_ADDRESS = "address";
    public static final String KEY_CITY = "city";
    public static final String KEY_PINCODE = "pincode";
    public static final String KEY_TIN = "tin";
    public static final String KEY_PAN = "pan_no";
    public static final String KEY_CUSTOMDATA = "customdata";
    public static final String KEY_SURVEY = "survey";
    public static final String KEY_DAY = "day";
    public static final String KEY_Date = "date";
    public static final String KEY_STOCKIEST = "stockiest";
    public static final String KEY_ROUTE = "route";
    public static final String KEY_STATE= "state";
    public static final String KEY_HIERARCHY_OBJ = "hierarchyobj";
    public static final String KEY_HIERARCHY_TEXT = "hierarchytext";
    public static final String KEY_MERCHANT_GEO_LAT = "geolat";
    public static final String KEY_MERCHANT_GEO_LON = "geolong";

    //orded detail
    public static final String KEY_order = "oflnordid";
    public static final String KEY_flag = "isfree";
    public static final String KEY_oprodid = "prodid";
    public static final String KEY_qty = "qty";
    public static final String KEY_index = "ordslno";
    public static final String KEY_duserid = "duserid ";
    public static final String KEY_orderStatus = "ordstatus";
    public static final String KEY_Dcompany = "Dcompany";
    public static final String KEY_ProdCode = "productcode";
    public static final String KEY_ProdName = "productname";
    public static final String KEY_orddtlId = "rowid";
    public static final String KEY_Oupdateddt = "updateddt";
    public static final String KEY_Odeliverydt = "deliverydt";
    public static final String KEY_prate = "prate";
    public static final String KEY_ptax = "ptax";
    public static final String KEY_pvalue = "pvalue";
    public static final String KEY_isreaddtl = "isread ";//ordimage
    public static final String KEY_ordimage = "ordimage ";
    public static final String KEY_expirydate = "expirydt ";
    public static final String  KEY_manufdate= "manufdt";
    public static final String KEY_batchno = "batchno";
    public static final String KEY_outstock = "stock";
    public static final String KEY_uom = "default_uom";
    public static final String KEY_order_uom = "ord_uom";
    public static final String KEY_default_uom_total = "cqty";
    public static final String KEY_delivery_date = "deliverydate";
    public static final String KEY_delivery_time = "deliverytime";
    public static final String KEY_delivery_mode = "deliverytype";
    public static final String KEY_current_stock = "currstock";


    //orderheader
    public static final String KEY_UpdateStatus = "updatestatus";
    public static final String KEY_Paymtype = "iscash";
    public static final String KEY_Orderdate = "orderdt";
    public static final String KEY_MOuserid = "muserid";
    public static final String KEY_SOuserid = "suserid ";
    public static final String KEY_pushStatus = "pushstatus ";
    public static final String KEY_orderno = "oflnordid ";
    public static final String KEY_Mname = "mname ";
    public static final String KEY_onlineOrderNo = "onlineorderno ";
    public static final String KEY_offOrderno = "oflnordno ";
    public static final String KEY_aprxordval = "aprxordval ";
    public static final String KEY_geolat = "geolat";
    public static final String KEY_geolong = "geolong";
    public static final String KEY_starttime = "start_time";
    public static final String KEY_endtime = "end_time";
    public static final String KEY_followup = "followup";

    //Scheme Table
    public static final String KEY_scheme_prodid = "schemeProdid";
    public static final String KEY_scheme_duserid = "schemeDuserid";
    public static final String KEY_scheme_startDate = "schemeStartdate";
    public static final String KEY_scheme_endDate = "schemeEnddate";
    public static final String KEY_scheme_discount = "schemeDiscount";
    public static final String KEY_scheme_type = "schemeType";
    public static final String KEY_scheme_freeProdid= "schemeFreeprodid";
    public static final String KEY_scheme_status = "schemeStatus";
    public static final String KEY_scheme_remark = "schemeRemark";
    public static final String KEY_scheme_freeQty = "schemeFreeQty";

    //Track Table
    public static final String KEY_tracklat = "geolat";
    public static final String KEY_tracklng = "geolong";
    public static final String KEY_trackdate = "logtime";
    public static final String KEY_trackstatus = "status";
    public static final String KEY_tracksuserid = "suserid";
    public static final String KEY_trackupdateddt = "updateddt";
    public static final String KEY_trackdistance = "distance";
    public static final String KEY_trackamount = "amount";

    //beat Table
    public static final String KEY_beatid = "id";
    public static final String KEY_beatmuserid = "muserid";
    public static final String KEY_beatsuserid = "suserid";
    public static final String KEY_beatduserid = "duserid";
    public static final String KEY_day_mon = "day_mon";
    public static final String KEY_day_tue = "day_tue";
    public static final String KEY_day_wed= "day_wed";
    public static final String KEY_day_thu = "day_thu";
    public static final String KEY_day_fri = "day_fri";
    public static final String KEY_day_sat = "day_sat";
    public static final String KEY_day_sun = "day_sun";
    public static final String KEY_beatstatus = "status";
    public static final String KEY_beatcreateddt = "createddt";
    public static final String KEY_beatupdateddt = "updateddt";
    public static final String KEY_beatflag = "flag";
    public static final String KEY_mid = "mid";
    public static final String KEY_beattype = "beattype";
    public static final String KEY_beatname = "beatnm";
    public static final String KEY_fullname= "fullname";
    public static final String KEY_beatusertype = "usertype";
    public static final String KEY_beatcompanyname = "companyname";



    // setting Table
    public static final String KEY_settingUserid = "userid";
    public static final String KEY_settingrefkey= "refkey";
    public static final String KEY_settingrefvalue = "refvalue";
    public static final String KEY_settingstatus= "status";
    public static final String KEY_settingupdateddt= "updateddt";
    public static final String KEY_settingremarks= "remarks";



    //product setting Table
    public static final String KEY_ProdStt_prodid = "prodid";
    public static final String KEY_refkey= "refkey";
    public static final String KEY_refvalue = "refvalue";
    public static final String KEY_muserid = "muserid";
    public static final String KEY_ProdStt_status= "status";
    public static final String KEY_ProdStt_updateddt= "updateddt";
    public static final String KEY_ProdStt_duserid= "duserid";

    //product uom Table
    public static final String KEY_Produom_prodid = "prodid";
    public static final String KEY_Produom_uomid= "uomid";
    public static final String KEY_Produom_uomname = "uomname";
    public static final String KEY_Produom_uomvalue = "uomvalue";
    public static final String KEY_Produom_status= "status";
    public static final String KEY_Produom_updateddt= "updateddt";
    public static final String KEY_Produom_duserid= "duserid";
    public static final String KEY_Produom_isdefault= "isdefault";

    //customdata table
    public static final String KEY_customfield_custmfldid = "custmfldid";
    public static final String KEY_customfield_duserid= "duserid";
    public static final String KEY_customfield_custmfldvalue = "custmfldvalue";

    //sync table
    public static final String KEY_REQUEST_PARAMS = "reqparams";
    public static final String KEY_REQUEST_URL = "requrl";
    public static final String KEY_SYNC_ID = "id";

    public static final String KEY_DIST_STOCK_ENTRY = "stockentry";
    public static final String KEY_OUTLET_STOCK_ENTRY = "outletstockentry";
    public static final String KEY_STOCK_ID = "id";
    public static final String KEY_OUTLET_STOCK_ID = "id";

    /* public static final String KEY_customfield_usertype = "usertype";
     public static final String KEY_customfield_basecustmfldid= "basecustmfldid";
     public static final String KEY_customfield_refkey= "refkey";
     public static final String KEY_customfield_refvalue= "refvalue";
     public static final String KEY_customfield_reflabel= "reflabel";
     public static final String KEY_customfield_datatype= "datatype";
     public static final String KEY_customfield_uicomptype= "uicomptype";
     public static final String KEY_customfield_status= "status";
     public static final String KEY_customfield_lastupdateddt= "lastupdateddt";
     public static final String KEY_customfield_lastupdatedby= "lastupdatedby";*/
    public static final String TBL_MERCHANT_ORDER_RELATION = "merchant_order_relation";
    public static final String STOCK_DETAIL = "stockOrderDetail";
    public static final String STOCK_HEADER = "stockOrderHeader";
    //Kumaravel 19/01/2019  26/01/2019
    public static final String TBL_DISTRIBUTOR_STOCK_SYNC = "distributor_stock_sync";
    public static final String TBL_SURVEY_SYNC = "survey_sync";
    public static final String TBL_PKD_OUTLET_STOCK_SYNC = "pkd_outlet_sync";
    public static final String TBL_POST_NOTE = "postnote";
    //Kumaravel 10/01/2019
    public static final String TBL_ORDER_INFO = "orderinfo";
    //kumaravel...
    public static final String TBL_TEMPLATE_TYPE = "templatetype";
    public static final String TBL_MILK_DATA_CHECK = "milkdatacheck";
    public static final String TBL_FAT_SNF_RATE = "fatsnfrate";
    public static final String TBL_SNF_CAL = "snfcal";
    //Tabel Names
    public static final String TBL_SM_GEOLOG="tbl_salesman_geolog";
    public static final String TBL_CONSTANTS = "constants";
    public static final String Track_Log = "tracklog";
    public static final String PROD_NAME = "prodtable";
    public static final String TABLE_NAME = "signintable";
    public static final String DEAL_NAME = "dealertable";
    public static final String MERCH_NAME = "merchanttable";
    public static final String ORDER_DETAIL = "oredrdetail";
    public static final String ORDER_HEADER = "orderheader";
    public static final String Client_visit = "clientvisit";
    public static final String Scheme_Table= "scheme";
    public static final String Beat_Table= "beat";
    public static final String User_Setting_Table= "usersetting";
    public static final String Product_Setting_Table= "productSetting";
    public static final String Product_UOM_Table= "productuom";
    public static final String Custom_Field_Table= "customfield";
    public static final String SYNC_TABLE = "synctable";
    public static final String DIST_STOCK_ENTRY_TABLE = "diststockentry";
    public static final String OUTLET_STOCK_ENTRY_TABLE = "outletstockentry";
    public static final String DATA_BASE_NAME = "freshorders";
    private static final int DATABASE_VERSION = 1;

    private static final String CREATE_TBL_SM_GEOLOG= "create table if not exists "+ TBL_SM_GEOLOG +"(rowid INTEGER PRIMARY KEY AUTOINCREMENT, batchid VARCHAR, suserid INTEGER, logtime TIMESTAMP DEFAULT CURRENT_TIMESTAMP, geolat VARCHAR,geolong VARCHAR, pgeolat VARCHAR,pgeolong VARCHAR,status VARCHAR,updateddt  TIMESTAMP DEFAULT CURRENT_TIMESTAMP,distance INTEGER,amount double,issynced INTEGER);";
    private static final String CREATE_TBL_CONSTANTS = "create table if not exists "+ TBL_CONSTANTS +"(rowid INTEGER PRIMARY KEY AUTOINCREMENT, key  VARCHAR,value VARCHAR);";
    private static final String CREATE_FRESH_SIGNIN_TABLE = "create table if not exists signintable(username  VARCHAR,password VARCHAR,usertype VARCHAR,name VARCHAR,id VARCHAR,payment VARCHAR,paymentStatus VARCHAR,email VARCHAR,nextpymtdt VARCHAR,dealid VARCHAR,beat VARCHAR,date VARCHAR);";
    private static final String CREATE_FRESH_PROD_TABLE = "create table if not exists prodtable(rowid INTEGER PRIMARY KEY AUTOINCREMENT,userid  INTEGER,companyname VARCHAR,prodcode VARCHAR,prodid VARCHAR,prodname VARCHAR,quantity VARCHAR,freeqty VARCHAR,updateddt VARCHAR,currentdate VARCHAR,prodprice VARCHAR,prodtax VARCHAR,Prodbatchno VARCHAR,mfgdate VARCHAR,expdate VARCHAR,uom VARCHAR,serialno VARCHAR,unitgram VARCHAR,unitperuom VARCHAR,prodimage VARCHAR,currstock VARCHAR);";
    private static final String CREATE_FRESH_DEAL_TABLE = "create table if not exists dealertable(rowid INTEGER PRIMARY KEY AUTOINCREMENT,companyname VARCHAR,usrdlrid INTEGER,userid INTEGER,fullname VARCHAR,address VARCHAR,currentdate VARCHAR);";

    private static final String CREATE_FRESH_MERCH_NAME =
            "create table if not exists merchanttable(rowid INTEGER PRIMARY KEY AUTOINCREMENT,companyname VARCHAR," +
                    "userid VARCHAR,fullname VARCHAR,mobileno VARCHAR,cflag VARCHAR,emailid VARCHAR,address VARCHAR," +
                    "city VARCHAR,pincode VARCHAR,tin VARCHAR,pan_no VARCHAR,customdata VARCHAR,survey VARCHAR,day VARCHAR," +
                    "date VARCHAR, stockiest VARCHAR,route VARCHAR,state VARCHAR, hierarchytext VARCHAR NOT NULL DEFAULT ''," +
                    " hierarchyobj VARCHAR NOT NULL DEFAULT '', oflnordid VARCHAR NOT NULL DEFAULT ''," +
                    " geolat VARCHAR NOT NULL DEFAULT '0',geolong VARCHAR NOT NULL DEFAULT '0');";

    private static final String CREATE_FRESH_ORDER_DETAIL= "create table if not exists oredrdetail(rowid INTEGER PRIMARY KEY AUTOINCREMENT ,oflnordid INTEGER ,duserid INTEGER,Dcompany VARCHAR,prodid VARCHAR,qty INTEGER,isfree VARCHAR,ordstatus VARCHAR,ordslno VARCHAR,productcode VARCHAR,productname VARCHAR,updateddt TIMESTAMP DEFAULT CURRENT_TIMESTAMP,deliverydt VARCHAR,prate VARCHAR," +
            "ptax VARCHAR,pvalue VARCHAR,isread VARCHAR,stock VARCHAR,batchno VARCHAR,manufdt VARCHAR,expirydt VARCHAR,ordimage VARCHAR,default_uom VARCHAR,ord_uom VARCHAR,cqty VARCHAR,deliverytime VARCHAR,deliverytype VARCHAR,currstock VARCHAR, FOREIGN KEY(oflnordid) REFERENCES orderheader(oflnordid));";

    private static final String CREATE_FRESH_ORDER_HEADER = "create table if not exists orderheader(oflnordid INTEGER PRIMARY KEY AUTOINCREMENT ," +
            "suserid INTEGER,muserid INTEGER,mname VARCHAR,iscash VARCHAR,pushstatus VARCHAR," +
            "orderdt VARCHAR,onlineorderno VARCHAR,oflnordno VARCHAR,aprxordval VARCHAR,pymnttotal VARCHAR," +
            "geolat VARCHAR,geolong VARCHAR,start_time VARCHAR,end_time VARCHAR,followup VARCHAR);";

    private static final String CREATE_FRESH_Client_visit = "create table if not exists clientvisit(id INTEGER PRIMARY KEY AUTOINCREMENT ," +
            "suserid INTEGER,muserid INTEGER,orderplndt VARCHAR,remarks VARCHAR,status VARCHAR," +
            " updateddt TIMESTAMP DEFAULT CURRENT_TIMESTAMP,geolat VARCHAR ,geolong VARCHAR," +
            "duserid VARCHAR,clientstatus VARCHAR, mname VARCHAR, rowid INTEGER NOT NULL DEFAULT 0);";

    private static final String CREATE_FRESH_track_log= "create table if not exists tracklog(rowid INTEGER PRIMARY KEY AUTOINCREMENT ,suserid INTEGER,geolat VARCHAR,geolong VARCHAR,logtime VARCHAR, status VARCHAR,updateddt VARCHAR,distance VARCHAR,amount VARCHAR);";
    private static final String CREATE_Scheme_Table= "create table if not exists scheme(rowid INTEGER PRIMARY KEY AUTOINCREMENT ,schemeProdid INTEGER,schemeDuserid VARCHAR,schemeStartdate VARCHAR,schemeEnddate VARCHAR, schemeDiscount VARCHAR,schemeType VARCHAR,schemeFreeprodid VARCHAR,schemeFreeQty VARCHAR,schemeStatus VARCHAR,schemeRemark VARCHAR);";

    private static final String CREATE_Beat_Table= "create table if not exists beat(rowid INTEGER PRIMARY KEY AUTOINCREMENT,id INTEGER,muserid VARCHAR,suserid VARCHAR,duserid VARCHAR, " +
            "day_mon VARCHAR,day_tue VARCHAR,day_wed VARCHAR,day_thu VARCHAR,day_fri VARCHAR,day_sat VARCHAR,day_sun VARCHAR,status VARCHAR,createddt VARCHAR,updateddt VARCHAR,flag VARCHAR," +
            "mid VARCHAR,beatnm VARCHAR,beattype VARCHAR,fullname VARCHAR,usertype VARCHAR,companyname VARCHAR, oflnordid VARCHAR NOT NULL DEFAULT '');";

    private static final String CREATE_ProductSetting_Table= "create table if not exists productSetting(rowid INTEGER PRIMARY KEY AUTOINCREMENT,prodid INTEGER,refkey VARCHAR,refvalue VARCHAR,muserid VARCHAR, status VARCHAR,updateddt VARCHAR,duserid VARCHAR);";

    private static final String CREATE_Usersetting_Table= "create table if not exists usersetting(rowid INTEGER PRIMARY KEY AUTOINCREMENT,userid INTEGER,refkey VARCHAR,refvalue VARCHAR NOT NULL DEFAULT '',updateddt VARCHAR, status VARCHAR,remarks VARCHAR);";

    private static final String CREATE_Product_UOM= "create table if not exists productuom(rowid INTEGER PRIMARY KEY AUTOINCREMENT,uomid INTEGER,prodid VARCHAR,duserid VARCHAR,uomname VARCHAR, uomvalue VARCHAR,status VARCHAR,updateddt VARCHAR,isdefault VARCHAR);";
    private static final String CREATE_Custom_Field= "create table if not exists customfield(" + KEY_SYNC_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,custmfldid INTEGER,duserid VARCHAR,custmfldvalue VARCHAR);";

    private static final String CREATE_SYNC_TABLE = "CREATE TABLE IF NOT EXISTS synctable (" + KEY_SYNC_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, flag INTEGER DEFAULT 0, requrl varchar, reqparams varchar);";

    private static final String CREATE_DIST_STOCK_ENTRY_TABLE = "CREATE TABLE IF NOT EXISTS diststockentry (" + KEY_STOCK_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, stockentry varchar);";

    private static final String CREATE_OUTLET_STOCK_ENTRY_TABLE = "CREATE TABLE IF NOT EXISTS outletstockentry (" + KEY_OUTLET_STOCK_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, outletstockentry varchar);";


    private static final String CREATE_STOCK_ORDER_DETAIL= "create table if not exists stockOrderDetail(rowid INTEGER PRIMARY KEY AUTOINCREMENT ,oflnordid INTEGER ,duserid INTEGER,Dcompany VARCHAR,prodid VARCHAR,qty INTEGER,isfree VARCHAR,ordstatus VARCHAR,ordslno VARCHAR,productcode VARCHAR,productname VARCHAR,updateddt TIMESTAMP DEFAULT CURRENT_TIMESTAMP,deliverydt VARCHAR,prate VARCHAR," +
            "ptax VARCHAR,pvalue VARCHAR,isread VARCHAR,stock VARCHAR,batchno VARCHAR,manufdt VARCHAR,expirydt VARCHAR,ordimage VARCHAR,default_uom VARCHAR,ord_uom VARCHAR,cqty VARCHAR,deliverytime VARCHAR,deliverytype VARCHAR,currstock VARCHAR, FOREIGN KEY(oflnordid) REFERENCES stockOrderHeader(oflnordid));";
    private static final String CREATE_STOCK_ORDER_HEADER = "create table if not exists stockOrderHeader(oflnordid INTEGER PRIMARY KEY AUTOINCREMENT ,suserid INTEGER,muserid INTEGER,mname VARCHAR,iscash VARCHAR,pushstatus VARCHAR,orderdt VARCHAR,onlineorderno VARCHAR,oflnordno VARCHAR,aprxordval VARCHAR,pymnttotal VARCHAR,geolat VARCHAR,geolong VARCHAR,start_time VARCHAR,end_time VARCHAR,followup VARCHAR);";


    //Kumaravel 10/01/2019

    private static final String CREATE_MERCHANT_ORDER_RELATION_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TBL_MERCHANT_ORDER_RELATION + "(" +
                    KEY_MERCHANT_ORDER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
                    KEY_MERCHANT_ORDER_MERCHANT_OFFLINE_ID + " TEXT ," +
                    KEY_MERCHANT_ORDER_ORDER_OFFLINE_ID + " TEXT ," +
                    KEY_MERCHANT_ORDER_MERCHANT_ID + " TEXT " + ")";

    private static final String CREATE_ORDER_INFO_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TBL_ORDER_INFO + "(" +
                    KEY_ORDER_INFO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
                    KEY_ORDER_INFO_ORDER_ID + " INTEGER ," +
                    KEY_IS_ORDER_PROCESSED + " BOOLEAN " + ")";
    //kumaravel...
    private static final String CREATE_TEMPLATE_TYPE_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TBL_TEMPLATE_TYPE + "(" +
                    KEY_TEMPLATE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
                    KEY_TEMPLATE_USER_ID + " INTEGER ," +
                    KEY_TEMPLATE_KEY + " VARCHAR ," +
                    KEY_TEMPLATE_VALUE + " VARCHAR " + ")";

    private static final String CREATE_MILK_DATA_CHECK_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TBL_MILK_DATA_CHECK + "(" +
                    KEY_MILK_CHECK_ID + " INTEGER PRIMARY KEY ," +
                    KEY_FAT_SNF_COLUMN_COUNT + " LONG ," +
                    KEY_SNF_CAL_COLUMN_COUNT + " LONG ," +
                    KEY_IS_FAT_SNF_TBL_LOADED + " BOOLEAN ," +
                    KEY_IS_SNF_CAL_TBL_LOADED + " BOOLEAN " + ")";


    private static final String CREATE_FAT_SNF_RATE_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TBL_FAT_SNF_RATE + "(" +
                    KEY_FAT_SNF_ID + " INTEGER PRIMARY KEY ," +
                    KEY_FAT_SNF_RATE_ID + " INTEGER ," +
                    KEY_USR_ID + " INTEGER ," +
                    KEY_FAT + " FLOAT ," +
                    KEY_SNF_VALUE + " FLOAT ," +
                    KEY_RATE + " FLOAT ," +
                    KEY_STATUS + " TEXT ," +
                    KEY_CREATED_BY + " TEXT ," +
                    KEY_CREATED_DATE + " TEXT ," +
                    KEY_LAST_UPDATED_BY + " TEXT ," +
                    KEY_LAST_UPDATED_DATE + " TEXT " + ")";

    private static final String CREATE_SNF_CAL_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TBL_SNF_CAL + "(" +
                    KEY_SNF_CAL_ID + " INTEGER PRIMARY KEY ," +
                    KEY_SNF_CALC__ID + " INTEGER ," +
                    KEY_SNF_CAL_USR_ID + " INTEGER ," +
                    KEY_SNF_CAL_TEMPERATURE + " FLOAT ," +
                    KEY_SNF_CAL_IR_VALUE + " FLOAT ," +
                    KEY_SNF_CAL_SNF_VALUE + " FLOAT ," +
                    KEY_SNF_CAL_STATUS + " TEXT ," +
                    KEY_SNF_CAL_CREATED_BY + " TEXT ," +
                    KEY_SNF_CAL_CREATED_DATE + " TEXT ," +
                    KEY_SNF_CAL_LAST_UPDATED_BY + " TEXT ," +
                    KEY_SNF_CAL_LAST_UPDATED_DATE + " TEXT " + ")";


    private static final String CREATE_POST_NOTE_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TBL_POST_NOTE + "(" +
                    KEY_POST_NOTE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    KEY_POST_NOTE_USER_ID + " TEXT ," +
                    KEY_POST_NOTE_D_USER_ID + " TEXT ," +
                    KEY_POST_NOTE_DESCRIPTION + " TEXT ," +
                    KEY_POST_NOTE_IS_NEED_TO_MIGRATE + " INTEGER ," +
                    KEY_POST_NOTE_IMAGE + " BLOB ," +
                    KEY_POST_NOTE_FAILED_REASON + " TEXT " + ")";

    private static final String CREATE_PKD_DATA_CAPTURE_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TBL_PKD_OUTLET_STOCK_SYNC + "(" +
                    KEY_PKD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
                    KEY_PKD_URL + " VARCHAR ," +
                    KEY_PKD_MIGRATION_STATUS + " INTEGER DEFAULT 0 ," +
                    KEY_PKD_FAILED_REASON + " VARCHAR ," +
                    KEY_PKD_SYNC_DATA + " VARCHAR ," +
                    KEY_PKD_MERCHANT_PRIMARY_KEY + " INTEGER DEFAULT 0 " +")";

    private static final String CREATE_SURVEY_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TBL_SURVEY_SYNC + "(" +
                    KEY_SURVEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
                    KEY_SURVEY_URL + " VARCHAR ," +
                    KEY_SURVEY_MIGRATION_STATUS + " INTEGER DEFAULT 0 ," +
                    KEY_SURVEY_FAILED_REASON + " VARCHAR ," +
                    KEY_SURVEY_SYNC_DATA + " VARCHAR ," +
                    KEY_SURVEY_MERCHANT_PRIMARY_KEY + " INTEGER DEFAULT 0 " +")";

    private static final String CREATE_DISTRIBUTOR_STOCK_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TBL_DISTRIBUTOR_STOCK_SYNC + "(" +
                    KEY_DISTRIBUTOR_STOCK_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
                    KEY_DISTRIBUTOR_STOCK_URL + " VARCHAR ," +
                    KEY_DISTRIBUTOR_STOCK_MIGRATION_STATUS + " INTEGER DEFAULT 0 ," +
                    KEY_DISTRIBUTOR_STOCK_FAILED_REASON + " VARCHAR ," +
                    KEY_DISTRIBUTOR_STOCK_SYNC_DATA + " VARCHAR " + ")";

    private static final String CREATE_ERROR_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TBL_ERROR + "(" +
                    KEY_ERROR_SERIAL_NO + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
                    KEY_ERROR_MESSAGE + " TEXT ," +
                    KEY_ERROR_DATE + " TEXT ," +
                    KEY_ERROR_TIME + " TEXT " + ")";

    private static final String CREATE_LOCATION_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TBL_LOCATION + "(" +
                    KEY_LOCATION_SERIAL_NO + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
                    KEY_LOCATION_LOCATION + " BLOB ," +
                    KEY_LOCATION_DATE + " TEXT ," +
                    KEY_LOCATION_TIME + " TEXT ," +
                    KEY_LOCATION_DATE_TIME + " TEXT ," +
                    KEY_LOCATION_ACCURACY + " FLOAT ," +
                    KEY_LOCATION_LAT + " REAL ," +
                    KEY_LOCATION_LON + " REAL ," +
                    KEY_LOCATION_P_LAT + " REAL ," +
                    KEY_LOCATION_P_LON + " REAL ," +
                    KEY_LOCATION_SUSER_ID + " TEXT ," +
                    KEY_LOCATION_PREVIOUS_ID + " INTEGER DEFAULT 0 ," +
                    KEY_LOCATION_UNIQUE_ID + " INTEGER DEFAULT 0 ," +
                    KEY_LOCATION_IS_SERVER + " INTEGER DEFAULT 0 " + ")";

    private static final String CREATE_MENU_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TBL_MENU + "(" +
                    KEY_MENU_SERIAL_NO + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
                    KEY_MENU_NAME + " TEXT ," +
                    KEY_MENU_IS_VISIBLE + " BOOLEAN ," +
                    KEY_MENU_CODE + " TEXT " + ")";

    private static final String CREATE_DISTRIBUTOR_TABLE =
            "CREATE TABLE IF NOT EXISTS " + TBL_DISTRIBUTOR + "(" +
                    KEY_DISTRIBUTOR_SERIAL_NO + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
                    KEY_DISTRIBUTOR_ID + " TEXT ," +
                    KEY_DISTRIBUTOR_NAME + " TEXT ," +
                    KEY_DISTRIBUTOR_MOBILE + " TEXT " + ")";



    private static volatile DatabaseHandler instance;
    private final SQLiteDatabase db;

    /**
            * We use a Singleton to prevent leaking the SQLiteDatabase or Context.
     * @return {@link DatabaseHandler}
     */
    public static DatabaseHandler getInstance(Context c) {
        if (instance == null) {
            synchronized (DatabaseHandler.class) {
                if (instance == null) {
                    instance = new DatabaseHandler(c);
                }
            }
        }
        return instance;
    }

    /**
     * Provide access to our database.
     */
    public SQLiteDatabase getDb() {
        return db;
    }

    // integer primary key autoincrement,
    public DatabaseHandler(Context applicationcontext) {

        super(applicationcontext, DATA_BASE_NAME, null, DATABASE_VERSION);
        Log.v("LOGCAT", "dbCreated");
        this.db = getWritableDatabase();

    }

    @Override
    public void onCreate(SQLiteDatabase sdb) {
        // String CREATE_JOB_TABLE = "CREATE TABLE " + TABLE_NAME+ "("+ KEY_id+
        // "  , "+ KEY_username + " TEXT , " + KEY_password + " TEXT )";
        sdb.execSQL(CREATE_TBL_SM_GEOLOG);
        sdb.execSQL(CREATE_FRESH_SIGNIN_TABLE);
        sdb.execSQL(CREATE_FRESH_PROD_TABLE);
        sdb.execSQL(CREATE_FRESH_DEAL_TABLE);
        sdb.execSQL(CREATE_FRESH_MERCH_NAME);
        sdb.execSQL(CREATE_FRESH_ORDER_HEADER);
        sdb.execSQL(CREATE_Beat_Table);
        sdb.execSQL(CREATE_FRESH_ORDER_DETAIL);
        sdb.execSQL(CREATE_FRESH_Client_visit);
        sdb.execSQL(CREATE_FRESH_track_log);
        sdb.execSQL(CREATE_Scheme_Table);
        sdb.execSQL(CREATE_ProductSetting_Table);
        sdb.execSQL(CREATE_Usersetting_Table);
        sdb.execSQL(CREATE_Product_UOM);
        sdb.execSQL(CREATE_Custom_Field);
        sdb.execSQL(CREATE_TBL_CONSTANTS);
        sdb.execSQL(CREATE_SYNC_TABLE);
        sdb.execSQL(CREATE_DIST_STOCK_ENTRY_TABLE);
        sdb.execSQL(CREATE_OUTLET_STOCK_ENTRY_TABLE);
        Log.v("LOGCAT", "Table Constants Created.");
//        addConstant("OUTLETCOUNT","0");
        Log.v("LOGCAT", "Table smart Created");
        Log.e("LOGCAT", "Table smart Created");
        //kumaravel
        sdb.execSQL(CREATE_TEMPLATE_TYPE_TABLE);
        sdb.execSQL(CREATE_MILK_DATA_CHECK_TABLE);
        sdb.execSQL(CREATE_FAT_SNF_RATE_TABLE);
        sdb.execSQL(CREATE_SNF_CAL_TABLE);
        //Kumaravel 10/01/2019
        sdb.execSQL(CREATE_ORDER_INFO_TABLE);
        sdb.execSQL(CREATE_POST_NOTE_TABLE);
        sdb.execSQL(CREATE_PKD_DATA_CAPTURE_TABLE);
        sdb.execSQL(CREATE_SURVEY_TABLE);
        sdb.execSQL(CREATE_DISTRIBUTOR_STOCK_TABLE);
        sdb.execSQL(CREATE_STOCK_ORDER_HEADER);
        sdb.execSQL(CREATE_STOCK_ORDER_DETAIL);
        sdb.execSQL(CREATE_MERCHANT_ORDER_RELATION_TABLE);

        sdb.execSQL(CREATE_ERROR_TABLE);
        sdb.execSQL(CREATE_LOCATION_TABLE);
        sdb.execSQL(CREATE_MENU_TABLE);
        sdb.execSQL(CREATE_DISTRIBUTOR_TABLE);

        Log.e("LOGCAT", "Table CREATE_TEMPLATE_TYPE_TABLE Crated");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

                db.execSQL(CREATE_SYNC_TABLE);
                db.execSQL(CREATE_DIST_STOCK_ENTRY_TABLE);
                db.execSQL(CREATE_OUTLET_STOCK_ENTRY_TABLE);

        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + Beat_Table);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + PROD_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + DEAL_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + MERCH_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + ORDER_DETAIL);
        db.execSQL("DROP TABLE IF EXISTS " + ORDER_HEADER);
        db.execSQL("DROP TABLE IF EXISTS " + Client_visit);
        db.execSQL("DROP TABLE IF EXISTS " + Scheme_Table);
        db.execSQL("DROP TABLE IF EXISTS " + Product_Setting_Table);
        db.execSQL("DROP TABLE IF EXISTS " + User_Setting_Table);
        db.execSQL("DROP TABLE IF EXISTS " + Product_UOM_Table);
        db.execSQL("DROP TABLE IF EXISTS " + Custom_Field_Table);
        db.execSQL("DROP TABLE IF EXISTS " + SYNC_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + DIST_STOCK_ENTRY_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + OUTLET_STOCK_ENTRY_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + DIST_STOCK_ENTRY_TABLE);
        //kumaravel changed this  5/1/2019
        db.execSQL("DROP TABLE IF EXISTS " + TBL_TEMPLATE_TYPE);
        db.execSQL("DROP TABLE IF EXISTS " + TBL_MILK_DATA_CHECK);
        db.execSQL("DROP TABLE IF EXISTS " + TBL_FAT_SNF_RATE);
        db.execSQL("DROP TABLE IF EXISTS " + TBL_SNF_CAL);
        db.execSQL("DROP TABLE IF EXISTS " + TBL_ORDER_INFO);
        db.execSQL("DROP TABLE IF EXISTS " + TBL_POST_NOTE);
        db.execSQL("DROP TABLE IF EXISTS " + TBL_PKD_OUTLET_STOCK_SYNC);
        db.execSQL("DROP TABLE IF EXISTS " + TBL_SURVEY_SYNC);
        db.execSQL("DROP TABLE IF EXISTS " + TBL_DISTRIBUTOR_STOCK_SYNC);
        db.execSQL("DROP TABLE IF EXISTS " + STOCK_DETAIL);
        db.execSQL("DROP TABLE IF EXISTS " + STOCK_HEADER );
        db.execSQL("DROP TABLE IF EXISTS " + TBL_MERCHANT_ORDER_RELATION );

        db.execSQL("DROP TABLE IF EXISTS " + TBL_MENU);
        db.execSQL("DROP TABLE IF EXISTS " + TBL_DISTRIBUTOR);
        // Create tables again
        onCreate(db);
    }

    public Long loadDistributor(List<ContentValues> values) throws Exception{
        int size = values.size();
        Long count = 0L;
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        for(int i = 0; i < size; i++){
            db.insertOrThrow(TBL_DISTRIBUTOR, null, values.get(i));
            count++;
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
        return count;
    }
    public Cursor getDistributorIdByName(String distributorName){
        String selectQuery = "SELECT distributorid FROM " + TBL_DISTRIBUTOR+" where distributorname = "+"'"+distributorName+"'" ;
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery(selectQuery, null);
    }

    public Long loadMenu(ContentValues values) throws Exception{
        SQLiteDatabase db = this.getWritableDatabase();
        return db.insertOrThrow(TBL_MENU, null, values);
    }

    public Long loadFullMenu(List<ContentValues> values) throws Exception{
        int size = values.size();
        Long count = 0L;
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        for(int i = 0; i < size; i++){
            db.insertOrThrow(TBL_MENU, null, values.get(i));
            count++;
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        db.close();
        return count;
    }

    public Cursor getMenuItems(){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM "+TBL_MENU;
        return db.rawQuery(selectQuery, null);
    }

    public Long insertLocation(ContentValues values) throws Exception{
        SQLiteDatabase db = this.getWritableDatabase();
        return db.insertOrThrow(TBL_LOCATION, null, values);
    }
    public Cursor getMaxRowIdFromLocation(String today){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT MAX(srno) FROM locationTBL WHERE date = '" + today + "'";
        return db.rawQuery(selectQuery, null);
    }

    public Cursor getMaxRowFromLocation(String today){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM locationTBL WHERE srno = (SELECT MAX(srno)  FROM locationTBL) AND date = '" + today + "'";
        return db.rawQuery(selectQuery, null);
    }

    public Cursor getAllLocation(){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM locationTBL order by srno desc";
        return db.rawQuery(selectQuery, null);
    }

    public Cursor getAllLocations(){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM locationTBL order by srno asc";
        return db.rawQuery(selectQuery, null);
    }

    public Cursor getAllUnSyncLocation(){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM locationTBL where isServer = 0 order by srno asc limit 50";
        return db.rawQuery(selectQuery, null);
    }

    public int removeAllSuccessfulPoints(String selectionClause, String[] selectionArgs){

        SQLiteDatabase db = getWritableDatabase();
        String idsCSV = TextUtils.join(",", selectionArgs);
        if (db != null) {
            int dCount = db.delete(DatabaseHandler.TBL_LOCATION, "unique_id IN (" + idsCSV + ")", null);
            db.close();
            return dCount;
        }
        //return db.delete(DatabaseHandler.TBL_LOCATION,selectionClause, selectionArgs);
        return 0;
    }

    public void setDistancePointSuccessBySequence(long id){
        SQLiteDatabase sdbh= this.getWritableDatabase();
        String strSql = "UPDATE "+TBL_LOCATION+" SET "+ KEY_LOCATION_IS_SERVER +" = '"+1+"' WHERE " + KEY_LOCATION_UNIQUE_ID +" = '"+ id +"'";
        sdbh.execSQL(strSql);
        sdbh.close();
    }


    public Cursor getAllUnSyncLocationByDate(String date){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM locationTBL where isServer = 0 AND date BETWEEN " + "'" + date +"' AND "+ "'" + date + "'" +"order by srno desc";
        return db.rawQuery(selectQuery, null);
    }

    public Cursor getLocationsById(int id){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM locationTBL where srno" + "='" + id + "'";
        return db.rawQuery(selectQuery, null);
    }

    public void deleteSuccessLocationById(long id){
        String strQuery = "DELETE FROM locationTBL where srno" + "='" + id + "' AND isServer = 1";
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL(strQuery);
    }


    public Cursor getLocationsForPreById(Long id){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM locationTBL where srno" + "='" + id + "'";
        return db.rawQuery(selectQuery, null);
    }

    public Cursor getUnSyncLocationsById(long id){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM locationTBL where srno" + "='" + id + "' AND isServer = 0";
        return db.rawQuery(selectQuery, null);
    }

    public void setDistancePointSuccess(long id){
        SQLiteDatabase sdbh= this.getWritableDatabase();
        String strSql = "UPDATE "+TBL_LOCATION+" SET "+ KEY_LOCATION_IS_SERVER +" = '"+1+"' WHERE " + KEY_LOCATION_SERIAL_NO +" = '"+ id +"'";
        sdbh.execSQL(strSql);
        sdbh.close();
    }

    public Cursor getLocationsByDate(String date){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM locationTBL where date LIKE  :date";
        return db.rawQuery(selectQuery, null);
    }

    public void insertError(ContentValues values){
        SQLiteDatabase db = this.getWritableDatabase();
        db.insertOrThrow(TBL_ERROR, null, values);
    }

    public Cursor getAllError(){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM errorTBL order by srno desc";
        return db.rawQuery(selectQuery, null);
    }

    public void loadMOR(ContentValues values){
        SQLiteDatabase db = this.getWritableDatabase();
        db.insertOrThrow(TBL_MERCHANT_ORDER_RELATION, null, values);
    }

    public void setMerchantIdByIdsInMOR(String[] ids, String setMerchantId){
        SQLiteDatabase sdbh= this.getWritableDatabase();
        for(int i = 0; i < ids.length; i++) {
            String id = ids[i];
            ContentValues contentValues = new ContentValues();
            contentValues.put(KEY_MERCHANT_ORDER_MERCHANT_ID, setMerchantId);
            String whereClause = "id=?";
            String whereArgs[] = {String.valueOf(id)};
            sdbh.update(TBL_MERCHANT_ORDER_RELATION, contentValues, whereClause, whereArgs);
        }
        sdbh.close();
    }

    public void setMerchantIdByIdInMOR(String setMerchantId, String offlineMerchantId){
        SQLiteDatabase sdbh= this.getWritableDatabase();
        String strSql = "UPDATE "+TBL_MERCHANT_ORDER_RELATION+" SET "+ KEY_MERCHANT_ORDER_MERCHANT_ID +" = '"+setMerchantId+"' WHERE " + KEY_MERCHANT_ORDER_MERCHANT_OFFLINE_ID +" = '"+ offlineMerchantId+"'";
        sdbh.execSQL(strSql);
        sdbh.close();
    }

    public Cursor getMORMerchantIdByOrderId(String byOrderId){
        String selectQuery = "SELECT "+ KEY_MERCHANT_ORDER_MERCHANT_ID +" FROM " + TBL_MERCHANT_ORDER_RELATION + " WHERE " + KEY_MERCHANT_ORDER_ORDER_OFFLINE_ID + "='" + byOrderId + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery(selectQuery, null);
    }

    public Cursor getMORByOfflineMerchantId(String byOfflineMerchantId){
        String selectQuery = "SELECT * FROM " + TBL_MERCHANT_ORDER_RELATION + " WHERE " + KEY_MERCHANT_ORDER_MERCHANT_OFFLINE_ID + "='" + byOfflineMerchantId +"'";
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery(selectQuery, null);
    }

    public Cursor getMORByOrderId(String offlineOrderId){
        String selectQuery = "SELECT * FROM " + TBL_MERCHANT_ORDER_RELATION + " WHERE " + KEY_MERCHANT_ORDER_ORDER_OFFLINE_ID + "='" + offlineOrderId + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery(selectQuery, null);
    }

    public void loadDistributorStock(ContentValues values){
        SQLiteDatabase db = this.getWritableDatabase();
        db.insertOrThrow(TBL_DISTRIBUTOR_STOCK_SYNC, null, values);
    }

    public void loadSurvey(ContentValues values){
        SQLiteDatabase db = this.getWritableDatabase();
        db.insertOrThrow(TBL_SURVEY_SYNC, null, values);
    }

    public void loadPKD(ContentValues values){
        SQLiteDatabase db = this.getWritableDatabase();
        db.insertOrThrow(TBL_PKD_OUTLET_STOCK_SYNC, null, values);
    }

    public Cursor getPendingSurvey(){
        String selectQuery = "SELECT * FROM " + TBL_SURVEY_SYNC + " WHERE " + KEY_SURVEY_MIGRATION_STATUS + "=1" ;
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery(selectQuery, null);
    }

    public Cursor getPKDPending(){
        String selectQuery = "SELECT * FROM " + TBL_PKD_OUTLET_STOCK_SYNC + " WHERE " + KEY_PKD_MIGRATION_STATUS + "=1" ;
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery(selectQuery, null);
    }

    public Cursor getDistributorStockPending(){
        String selectQuery = "SELECT * FROM " + TBL_DISTRIBUTOR_STOCK_SYNC + " WHERE " + KEY_DISTRIBUTOR_STOCK_MIGRATION_STATUS + "=1" ;
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery(selectQuery, null);
    }

    public Cursor getFailedSurvey(){
        String selectQuery = "SELECT * FROM " + TBL_SURVEY_SYNC + " WHERE " + KEY_SURVEY_MIGRATION_STATUS + "=-1" ;
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery(selectQuery, null);
    }

    public Cursor getPKDFailed(){
        String selectQuery = "SELECT * FROM " + TBL_PKD_OUTLET_STOCK_SYNC + " WHERE " + KEY_PKD_MIGRATION_STATUS + "=-1" ;
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery(selectQuery, null);
    }

    public Cursor getDistributorStockFailed(){
        String selectQuery = "SELECT * FROM " + TBL_DISTRIBUTOR_STOCK_SYNC + " WHERE " + KEY_DISTRIBUTOR_STOCK_MIGRATION_STATUS + "=-1" ;
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery(selectQuery, null);
    }

    public void updateSurveySync(int id, int isNeedToMigrate, String failedReason){
        SQLiteDatabase sdbh = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_SURVEY_MIGRATION_STATUS, isNeedToMigrate);
        contentValues.put(KEY_SURVEY_FAILED_REASON, failedReason);
        String whereClause = "id=?";
        String whereArgs[] = {String.valueOf(id)};
        sdbh.update(TBL_SURVEY_SYNC, contentValues, whereClause, whereArgs);
    }

    public void updatePKDSync(int id, int isNeedToMigrate, String failedReason){
        SQLiteDatabase sdbh = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_PKD_MIGRATION_STATUS, isNeedToMigrate);
        contentValues.put(KEY_PKD_FAILED_REASON, failedReason);
        String whereClause = "id=?";
        String whereArgs[] = {String.valueOf(id)};
        sdbh.update(TBL_PKD_OUTLET_STOCK_SYNC, contentValues, whereClause, whereArgs);
    }

    public void updateDistributorStockSync(int id, int isNeedToMigrate, String failedReason){
        SQLiteDatabase sdbh = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_DISTRIBUTOR_STOCK_MIGRATION_STATUS, isNeedToMigrate);
        contentValues.put(KEY_DISTRIBUTOR_STOCK_FAILED_REASON, failedReason);
        String whereClause = "id=?";
        String whereArgs[] = {String.valueOf(id)};
        sdbh.update(TBL_DISTRIBUTOR_STOCK_SYNC, contentValues, whereClause, whereArgs);
    }

    public void loadPostNote(ContentValues values){
        SQLiteDatabase db = this.getWritableDatabase();
        db.insertOrThrow(TBL_POST_NOTE, null, values);
    }

    public Cursor getPostNotePending(){
        String selectQuery = "SELECT * FROM " + TBL_POST_NOTE + " WHERE " + KEY_POST_NOTE_IS_NEED_TO_MIGRATE + "=1" ;
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery(selectQuery, null);
    }

    public Cursor getPostNoteFailed(){
        String selectQuery = "SELECT * FROM " + TBL_POST_NOTE + " WHERE " + KEY_POST_NOTE_IS_NEED_TO_MIGRATE + "=-1" ;
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery(selectQuery, null);
    }

    public void updatePostNote(int id, int isNeedToMigrate, String failedReason){
        SQLiteDatabase sdbh = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_POST_NOTE_IS_NEED_TO_MIGRATE, isNeedToMigrate);
        contentValues.put(KEY_POST_NOTE_FAILED_REASON, failedReason);
        String whereClause = "id=?";
        String whereArgs[] = {String.valueOf(id)};
        sdbh.update(TBL_POST_NOTE, contentValues, whereClause, whereArgs);
    }

    public Cursor getOrderStatus(int orderId){
        String selectQuery = "SELECT " + KEY_IS_ORDER_PROCESSED + " FROM " + TBL_ORDER_INFO + " WHERE " + KEY_ORDER_INFO_ORDER_ID + "='" + orderId + "'" ;
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery(selectQuery, null);
    }

    public Cursor getOrderHeaderId(String onlineOrderId){
        //String selectQuery = "SELECT  sum(amount) FROM " +TBL_SM_GEOLOG + " where batchid='"+strBatchId+"'";
        String selectQuery = "SELECT oflnordid FROM " + ORDER_HEADER + " WHERE onlineorderno='" + onlineOrderId + "'" ;
        Log.e("getOrderHeaderId", "........." + selectQuery);
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery(selectQuery, null);
    }

    public Long addOrderInfo(List<ContentValues> valuesList){
        int size = valuesList.size();
        Long count = 0L;
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        for(int i = 0; i < size; i++){
            db.insertOrThrow(TBL_ORDER_INFO, null, valuesList.get(i));
            count++;
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        return count;
    }

    public void loadHeader(ContentValues values){
        SQLiteDatabase db = this.getWritableDatabase();
        db.insertOrThrow(ORDER_HEADER, null, values);
    }

    public Long addBulkOrderHeader(List<ContentValues> valuesList){
        int size = valuesList.size();
        Long count = 0L;
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        for(int i = 0; i < size; i++){
            db.insertOrThrow(ORDER_HEADER, null, valuesList.get(i));
            count++;
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        return count;
    }

    public Long addBulkOrderDetail(List<ContentValues> valuesList){
        int size = valuesList.size();
        Long count = 0L;
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        for(int i = 0; i < size; i++){
            db.insertOrThrow(ORDER_DETAIL, null, valuesList.get(i));
            count++;
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        return count;
    }

    public Long addBulkSNFCalTblValues(List<ContentValues> valuesList){
        int size = valuesList.size();
        Long count = 0L;
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
        for(int i = 0; i < size; i++){
            db.insertOrThrow(TBL_SNF_CAL, null, valuesList.get(i));
            count++;
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        return count;
    }

    public Cursor getMilkPrice(String fat, String snf){
        String selectQuery = "SELECT " + KEY_RATE + " FROM " + TBL_FAT_SNF_RATE + " WHERE " + KEY_FAT + "='" + fat + "'" + "AND "+ KEY_SNF_VALUE + "='" + snf + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery(selectQuery, null);
    }

    public Cursor getSNFValue(String temperature, String lrvalue){
        String selectQuery = "SELECT " + KEY_SNF_CAL_SNF_VALUE + " FROM " + TBL_SNF_CAL + " WHERE " + KEY_SNF_CAL_TEMPERATURE + "='" + temperature + "'" + "AND "+ KEY_SNF_CAL_IR_VALUE + "='" + lrvalue + "'";
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery(selectQuery, null);
    }

    public boolean deleteBulkSNFCalTblValues()
    {   String strQuery = "delete FROM " + TBL_SNF_CAL;
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL(strQuery);
        return false;
    }

    public Long addBulkFatSNFTblValues(List<ContentValues> valuesList){
        int size = valuesList.size();
        Long count = 0L;
        SQLiteDatabase db = this.getWritableDatabase();
        db.beginTransaction();
            for(int i = 0; i < size; i++){
                db.insertOrThrow(TBL_FAT_SNF_RATE, null, valuesList.get(i));
                count++;
            }
        db.setTransactionSuccessful();
        db.endTransaction();
        return count;
    }

    public boolean deleteBulkFatSNFTblValues()
    {   String strQuery = "delete FROM " + TBL_FAT_SNF_RATE;
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL(strQuery);
        return false;
    }

    public void addDairyDataCheckTbl(ContentValues values){
        SQLiteDatabase sdbh = this.getWritableDatabase();
        sdbh.insertOrThrow(TBL_MILK_DATA_CHECK,null,values);
        sdbh.close();
    }

    public void updateDairyDataCheckFatSnf(Long fatSnfCount, boolean isSuccess){
        SQLiteDatabase sdbh = this.getWritableDatabase();
        //String strQuery = "update  " +TBL_MILK_DATA_CHECK +" set `"+KEY_FAT_SNF_COLUMN_COUNT+"`='"+fatSnfCount+"' where "+KEY_Key+" = '"+strKey+"'";

        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_FAT_SNF_COLUMN_COUNT, fatSnfCount);
        contentValues.put(KEY_IS_FAT_SNF_TBL_LOADED, isSuccess);
        String whereClause = "id=?";
        String whereArgs[] = {String.valueOf(MILK_CHECK_TBL_PRIMARY_KEY_VALUE)};
        sdbh.update(TBL_MILK_DATA_CHECK, contentValues, whereClause, whereArgs);
    }

    public void updateDairyDataCheckSnfCal(Long SnfCalCount, boolean isSuccess){
        SQLiteDatabase sdbh = this.getWritableDatabase();
        //String strQuery = "update  " +TBL_MILK_DATA_CHECK +" set `"+KEY_FAT_SNF_COLUMN_COUNT+"`='"+fatSnfCount+"' where "+KEY_Key+" = '"+strKey+"'";

        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_SNF_CAL_COLUMN_COUNT, SnfCalCount);
        contentValues.put(KEY_IS_SNF_CAL_TBL_LOADED, isSuccess);
        String whereClause = "id=?";
        String whereArgs[] = {String.valueOf(MILK_CHECK_TBL_PRIMARY_KEY_VALUE)};
        sdbh.update(TBL_MILK_DATA_CHECK, contentValues, whereClause, whereArgs);
    }

    public Cursor getDairyDataCheckTbl() {
        String selectQuery = "SELECT  * FROM " +TBL_MILK_DATA_CHECK +
                " WHERE " + KEY_MILK_CHECK_ID +" = " + MILK_CHECK_TBL_PRIMARY_KEY_VALUE ;
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery(selectQuery, null);
    }

    public boolean addTemplateType(ContentValues values) {
        SQLiteDatabase sdbh = this.getWritableDatabase();
        sdbh.insert(TBL_TEMPLATE_TYPE,null,values);
        sdbh.close();
        return false;
    }

    public Cursor getTemplateType() {
        String selectQuery = "SELECT  * FROM " +TBL_TEMPLATE_TYPE;
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery(selectQuery, null);
    }

    public Cursor getLastTemplateType() {
        String selectQuery = "SELECT * FROM " + TBL_TEMPLATE_TYPE + " ORDER BY " + KEY_TEMPLATE_ID + " DESC LIMIT 1 ";
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery(selectQuery, null);

    }

    @SuppressLint("NewApi")
    public boolean addConstant(String strKey, String strValue ) {
        SQLiteDatabase sdbh = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_Key, strKey);
        contentValues.put(KEY_Value, strValue);
        sdbh.insert(TBL_CONSTANTS,null,contentValues);
        sdbh.close();
        return false;
    }

    public Cursor getSyncData(){
        String selectQuery = "SELECT  * FROM " +SYNC_TABLE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public Cursor getSyncDataForSurvey(String url){
        String selectQuery = "SELECT  * FROM " +SYNC_TABLE + " WHERE requrl='" + url +"' AND flag = 0";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public Cursor getStockEntryData(){
        String selectQuery = "SELECT  * FROM " +DIST_STOCK_ENTRY_TABLE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public Cursor getOutletStockEntryData(){
        String selectQuery = "SELECT  * FROM " + OUTLET_STOCK_ENTRY_TABLE;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public Cursor getSMGeoLogs() {
        String selectQuery = "SELECT  * FROM " +TBL_SM_GEOLOG;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public Cursor getUnSyncedSMGeoLogs() {
        String selectQuery = "SELECT  * FROM " +TBL_SM_GEOLOG+ " where issynced=0";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public double getUnSyncedTotalDistance() {
        String selectQuery = "SELECT  ifnull(sum(distance),0) FROM " +TBL_SM_GEOLOG;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        double dblValue=cursor.getDouble(0);
        cursor.close();
        return dblValue;
    }

    public Cursor getSMGeoLogs(String strBatchId) {
        String selectQuery = "SELECT  * FROM " +TBL_SM_GEOLOG+ " where batchid='"+strBatchId+"'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public double getTotalDistance(String strBatchId) {
        String selectQuery = "SELECT  sum(distance) FROM " +TBL_SM_GEOLOG + " where batchid='"+strBatchId+"'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        double dblValue=cursor.getDouble(0);
        cursor.close();
        return dblValue;
    }
    public double getTotalAmount(String strBatchId) {
        String selectQuery = "SELECT  sum(amount) FROM " +TBL_SM_GEOLOG + " where batchid='"+strBatchId+"'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        double dblValue=cursor.getDouble(0);
        cursor.close();
        return dblValue;
    }

    public void removeSyncedSMGeoLogs()
    {   String strQuery = "delete FROM " +TBL_SM_GEOLOG+ " where issynced=1";
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL(strQuery);
    }

    public void removeAllGeoLogs()
    {   String strQuery = "delete FROM " +TBL_SM_GEOLOG;
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL(strQuery);
    }

    public boolean addSMGeoLogs(ContentValues contentValues ) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TBL_SM_GEOLOG,null,contentValues);
        db.close();
        return false;
    }

    public JSONArray getCursorItems(Cursor cursor)
    {
        JSONArray jsonArCursorItems=null;
        try {
            if(cursor !=null )
            {
                jsonArCursorItems=new JSONArray();
            }
            while (cursor.moveToNext()) {
                JSONObject jsonObjectCursorItem=new JSONObject();
                for (int i=0;i<cursor.getColumnCount();i++){
                    jsonObjectCursorItem.put(cursor.getColumnName(i),cursor.getString(i));
                }
                System.out.println("-->jsonObjectCursorItem"+jsonObjectCursorItem);
                jsonArCursorItems.put(jsonObjectCursorItem);
                System.out.println("-->jsonArCursorItems:"+jsonArCursorItems);
            }
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        finally {
            cursor.close();
        }
        return jsonArCursorItems;
    }

    public Cursor getConstants() {
        String selectQuery = "SELECT  * FROM " +TBL_CONSTANTS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }
    public String getConstantValue(String strKey) {
        String strValue=null;
        String selectQuery = "SELECT  `"+KEY_Value+"` FROM " +TBL_CONSTANTS +" where "+KEY_Key+" = '"+strKey+"'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor!=null && cursor.getCount()>0)
        {
            cursor.moveToFirst();
            strValue=cursor.getString(0);
        }
        return strValue;
    }

    public void setConstantValue(String strKey,String strValue) {
        String strQuery = "update  " +TBL_CONSTANTS +" set `"+KEY_Value+"`='"+strValue+"' where "+KEY_Key+" = '"+strKey+"'";
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL(strQuery);
    }

    public Cursor getDetails() {
        String selectQuery = "SELECT  * FROM " + TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }
    public Cursor getDatewiseUnSyncedSMGeoLogs() {
        String selectQuery="SELECT strftime ('%Y-%m-%d',`logtime`) `logtime`,sum(distance) as `distance`,sum(amount) as `amount`  FROM tbl_salesman_geolog where suserid='"+Constants.USER_ID+"' group by   strftime('%Y-%m-%d',`logtime`) order by  strftime('%Y-%m-%d',`logtime`) desc";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }
    public Cursor getDatewiseUnSyncedSmGeoLogs() {
        String selectQuery="SELECT  `logtime`,sum(distance) as `distance`,sum(amount) as `amount`  FROM " +TBL_SM_GEOLOG +" where suserid='"+Constants.USER_ID+"' group by  `logtime` order by `logtime` desc";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }
    public Cursor getCutomDetail() {
        String selectQuery = "SELECT  * FROM " + Custom_Field_Table;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }
    public Cursor getOfflineOrderID(String muserid)
    {
        String selectQuery = "SELECT  oflnordid FROM " + ORDER_HEADER +" where  muserid in ("+muserid+") ORDER BY oflnordid DESC  ";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public Cursor getProduct() {
        String selectQuery = "SELECT  * FROM " + PROD_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public Cursor getProductUnits(String prodid) {
        String selectQuery = "SELECT  unitgram,unitperuom FROM " + PROD_NAME+" where prodid in ("+prodid+")";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public Cursor getFilterProduct(String prodid) {
        String selectQuery = "SELECT  * FROM " + PROD_NAME +" where prodid in ("+prodid+")";
        Log.e("selectQuery",selectQuery);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public Cursor getUomUnit(String prodid,String uom) {
        String selectQuery = "SELECT uomvalue FROM " + Product_UOM_Table +" where prodid = "+prodid+" AND uomname = "+"'"+uom+"'";
        Log.e("selectQuery",selectQuery);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public Cursor
    getSchemeProduct(String prodid) {
        String selectQuery = "SELECT  * FROM " + PROD_NAME+" where prodid="+prodid;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public Cursor getdealer() {
        String selectQuery = "SELECT  * FROM " + DEAL_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }
    public Cursor getScheme(String prodid) {
        String selectQuery = "SELECT  * FROM " + Scheme_Table+" where schemeProdid="+prodid;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public Cursor getSchemeCount() {
        String selectQuery = "SELECT  * FROM " + Scheme_Table;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public Cursor getmerchant() {
        String selectQuery = "SELECT * FROM "+ MERCH_NAME +" ORDER BY companyname" ;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        return cursor;
    }

    public Cursor getMerchantById(int primaryKey) {
        String selectQuery = "SELECT * FROM "+ MERCH_NAME +" WHERE rowid =" + primaryKey ;
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery(selectQuery, null);
    }

    public Cursor getMerchantAddressByUserId(String mUserId) {
        String selectQuery = "SELECT * FROM "+ MERCH_NAME +" WHERE userid =" + mUserId ;
        SQLiteDatabase db = this.getReadableDatabase();
        return db.rawQuery(selectQuery, null);
    }

    public int getMerchantCount() {
        int intCount=0;
        String selectQuery = "SELECT count(*) FROM "+ MERCH_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor!=null)
        {
            cursor.moveToFirst();
            intCount=cursor.getInt(0);
        }
        return intCount;
    }

    public Cursor getmerchant1() {
        String selectQuery = "SELECT route FROM "+ MERCH_NAME +" ORDER BY companyname" ;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        return cursor;
    }

    public Cursor getStockiestList() {
        String selectQuery = "SELECT distinct stockiest FROM "+ MERCH_NAME +"  where stockiest is not null  and stockiest <> ''  and stockiest <> 'null'  ORDER BY stockiest" ;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        return cursor;
    }

    public Cursor getRouteList() {
        String selectQuery = "SELECT distinct route FROM "+ MERCH_NAME +"  where route is not null  and route <> ''  and route <> 'null'  ORDER BY route" ;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        return cursor;
    }

    public Cursor getCityList() {
        String selectQuery = "SELECT distinct city FROM "+ MERCH_NAME +"  where city is not null  and city <> ''  and city <> 'null'  ORDER BY city" ;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        return cursor;
    }


    public Cursor getmerchantsockiest() {
        String selectQuery = "SELECT distinct stockiest FROM "+ MERCH_NAME +" where stockiest is not null";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        return cursor;
    }
    public Cursor getmerchantwithoutorder() {
        String selectQuery = "SELECT * FROM "+ MERCH_NAME ;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        return cursor;
    }

    public void setMerchantOfflineId(String id, int merchantPrimaryId){
        SQLiteDatabase sdbh= this.getWritableDatabase();
        String strSql = "UPDATE "+MERCH_NAME+" SET oflnordid = '"+id+"' WHERE rowid = '"+ merchantPrimaryId+"'";
        sdbh.execSQL(strSql);
        sdbh.close();
    }

    public Cursor getmerchantoncurrentday(String beatmerchant) {
        Log.e("merchant",beatmerchant);
        String selectQuery = "SELECT * FROM "+ MERCH_NAME +" where userid in ( "+beatmerchant+" )" ;
        Log.e("query",selectQuery);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        return cursor;
    }
    public Cursor getmerchantDetails(String muserid) {
        String selectQuery = "SELECT * FROM "+ MERCH_NAME +" where userid="+muserid;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        return cursor;
    }

    public Cursor getDistributor() {
        String selectQuery = "SELECT DISTINCT  fullname  FROM "+ Beat_Table;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public Cursor getStockOrderDetail(String ordno) {
        String selectQuery = "SELECT  * FROM " +STOCK_DETAIL + " where oflnordid="+"'"+ordno+"'" +"ORDER BY ordslno ASC, isfree DESC";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public Cursor getorderdetail(String ordno) { ////////////// qty >0 changed to qty>=0 important Kumaravel
        String selectQuery = "SELECT  * FROM oredrdetail where qty>=0 AND oflnordid="+"'"+ordno+"'" +"ORDER BY ordslno ASC, isfree DESC";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public Cursor getorderdetailForAcknowledge(String ordno) { ////////////// qty >0 changed to qty>=0 important Kumaravel
        String selectQuery = "SELECT  * FROM oredrdetail where qty>0 AND oflnordid="+"'"+ordno+"'" +"ORDER BY ordslno ASC, isfree DESC";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }


    public Cursor getUpdatedBeat(String muserid,String beatname) {
        String selectQuery = "SELECT  * FROM beat where muserid= "+"'"+muserid+"'"+" AND beatnm="+"'"+beatname+"'" ;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public Cursor getDistributorBeat(String distributorName) {
        String selectQuery = "SELECT DISTINCT beatnm  FROM beat where fullname= "+"'"+distributorName+"'" ;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public Cursor getBeatdetails(String beatname) {
        String selectQuery = "SELECT *  FROM beat where beatnm= "+"'"+beatname+"'" ;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public Cursor getStockOrderHeaderStatus(String ordno) {
        String selectQuery = "SELECT  pushstatus FROM " + STOCK_HEADER + " where stockOrderHeader.oflnordid="+ordno ;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public Cursor getOrderHeaderStatus(String ordno) {
        String selectQuery = "SELECT  pushstatus FROM orderheader where orderheader.oflnordid="+ordno ;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public Cursor getStockOrderHeaderByStatus(String status) {
        String selectQuery = "SELECT  * FROM " + STOCK_HEADER+" where pushstatus ='" +status +"'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public Cursor getAllNewClient() {
        String selectQuery = "SELECT * from orderheader where pushstatus IN ('No Order', 'Outlet')";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public Cursor getStockOrderHeader() {
        String selectQuery = "SELECT  * FROM " + STOCK_HEADER+" where pushstatus ='Pending'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }
    public Cursor isOrderHeaderForNewUser() {
        String selectQuery = "SELECT  * FROM " + ORDER_HEADER+" where muserid ='New User'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }
    public Cursor getorderheader() {
        String selectQuery = "SELECT  * FROM " + ORDER_HEADER+" where pushstatus ='Pending'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }
    public Cursor getFailedOrderHeader() {
        String selectQuery = "SELECT  * FROM " + ORDER_HEADER+" where pushstatus ='Failed'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }
    public Cursor getAllStockOrderHeader() {
        String selectQuery = "SELECT  * FROM "+STOCK_HEADER;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }
    public Cursor getallorderheader() {
        String selectQuery = "SELECT  * FROM orderheader";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }
    public Cursor getNoOrders() {
        String selectQuery = "SELECT  * FROM " + ORDER_HEADER+" where pushstatus ='No Order'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }
    public Cursor getNewMerchantOrders() {
        String selectQuery = "SELECT  * FROM " + ORDER_HEADER+" where pushstatus ='Outlet'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }
    public Cursor getTracklog() {
        String selectQuery = "SELECT  * FROM " + Track_Log;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }
    public Cursor getProductSetting() {
        String selectQuery = "SELECT  * FROM " + Product_Setting_Table;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }
    public Cursor getProductUOM() {
        String selectQuery = "SELECT  * FROM " + Product_UOM_Table;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public Cursor getUOMforproduct(String prodid) {
        String selectQuery = "SELECT DISTINCT uomname FROM " + Product_UOM_Table +" where prodid = " + prodid;
        Log.e("query",selectQuery);
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public Cursor getUserSetting() {
        String selectQuery = "SELECT  * FROM " + User_Setting_Table;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }
    public String getSalesmanDataUserSetting(String strKey) {
        String strValue="";//String strValue=null;
        String selectQuery = "SELECT  `"+KEY_settingrefvalue+"` FROM " +User_Setting_Table +" where "+KEY_settingrefkey+" = '"+strKey+"'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor!=null && cursor.getCount()>0)
        {
            cursor.moveToFirst();
            strValue=cursor.getString(0);
        }
        return strValue;
    }
    public Cursor getSetingProduct(String muserid) {
        String selectQuery = "SELECT * FROM " + Product_Setting_Table+" where muserid = "+muserid;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }
    public Cursor getorder() {
        String selectQuery = "SELECT  * FROM " + ORDER_HEADER+","+ORDER_DETAIL+" where ("+ORDER_HEADER+".oflnordid="+ORDER_DETAIL+".oflnordid"+ ") AND (" +ORDER_HEADER+".pushstatus='Pending')";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public Cursor getclientvisit() {
        String selectQuery = "SELECT  * FROM " + Client_visit;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public Cursor getbeat() {
        String selectQuery = "SELECT  * FROM " + Beat_Table;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }
    public Cursor getdistributorid(String distname) {
        String selectQuery = "SELECT duserid FROM " + Beat_Table+" where fullname = "+"'"+distname+"'" ;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public Cursor getdistributoridFromDisTable(String distname) {
        String selectQuery = "SELECT distributorid FROM " + TBL_DISTRIBUTOR+" where distributorname = "+"'"+distname+"'" ;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public Cursor getbeatid(String beatname) {
        String selectQuery = "SELECT id FROM " + Beat_Table+" where beatnm = "+"'"+beatname+"'" ;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public Cursor getbeatMerchant(String beatname) {
        String selectQuery = "SELECT muserid FROM " + Beat_Table+" where beatnm = "+"'"+beatname+"'" ;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public Cursor getapproximatevalue(String distname) {
        String selectQuery = "SELECT aprxordval FROM " + ORDER_HEADER+" where oflnordid = "+"'"+distname+"'" ;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }
    public Cursor getbeatwithMerchant(String dayofWeek) {
        String day ="";
        String selectQuery = null;
        if(dayofWeek.equals("Monday")){
            day=KEY_day_mon;
            Log.e("dbday","Monday");
        }else if(dayofWeek.equals("Tuesday")){
            day=KEY_day_tue;
            Log.e("dbday","Tuesday");
        }else if(dayofWeek.equals("Wednesday")){
            day=KEY_day_wed;
            Log.e("dbday","Wednesday");
        }else if(dayofWeek.equals("Thursday")){
            day=KEY_day_thu;
            Log.e("dbday","Thursday");
        }else if(dayofWeek.equals("Friday")){
            day=KEY_day_fri;
            Log.e("dbday","Friday");
        }else if(dayofWeek.equals("Saturday")){
            day=KEY_day_sat;
            Log.e("dbday","Saturday");
        }else if(dayofWeek.equals("Sunday")){
            day=KEY_day_sun;
            Log.e("dbday","Sunday");
        }

        selectQuery = "SELECT  muserid  FROM " + Beat_Table+" where "+day+" ='Y'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    @SuppressLint("NewApi")
    public void addDetails(ContentValues values) {
        SQLiteDatabase sdbh = this.getWritableDatabase();



        String selectQuery = "SELECT  * FROM " + TABLE_NAME;
        Cursor cursor = sdbh.rawQuery(selectQuery, null);
        if (cursor.getCount() != 0) {
            sdbh.update(TABLE_NAME, values, KEY_username + " = ?",
                    new String[] { "1" });
        } else {
            sdbh.insertWithOnConflict(TABLE_NAME, null, values,
                    SQLiteDatabase.CONFLICT_REPLACE);// (TABLE_NAME, null,
            // values);
        }
        sdbh.close();
    }


    public Cursor getClientVisit(String status) {
        Log.e("status",status);

        Cursor cursor;
        String selectQuery ;
        if(status.equals("Success")){
            String date = "yyyy-MM-dd";
            SimpleDateFormat inputFormat = new SimpleDateFormat(date, Locale.getDefault());
            String dt =inputFormat.format(new Date());
            selectQuery = "SELECT  * FROM " + Client_visit+" where (clientstatus ="+"'"+status+"'"+") AND (date(orderplndt) = "+"'"+dt+"')"+"ORDER BY id DESC";
        }else {
            selectQuery = "SELECT  * FROM " + Client_visit+" where clientstatus ="+"'"+status+"'" +"ORDER BY id DESC";
        }

        SQLiteDatabase db = this.getReadableDatabase();
        cursor = db.rawQuery(selectQuery, null);

        return cursor;
    }

    public Cursor getClientVisitByDate(String status, String date) {

        Cursor cursor;
        String selectQuery = "SELECT  * FROM " + Client_visit+" where (clientstatus ="+"'"+status+"'"+") AND (date(updateddt) = "+"'"+date+"')"+" ORDER BY id DESC";
        SQLiteDatabase db = this.getReadableDatabase();
        cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }


    public Cursor getSuccessheader(String status,String date) {
        Log.e("status",status);
        Log.e("date",date);
        Cursor cursor;

        if(status.equals("Success")){
            //String selectQuery="SELECT DISTINCT "+ORDER_HEADER+".oflnordid, "+ORDER_HEADER+".muserid, "+ORDER_DETAIL+".isread, "+ORDER_HEADER+".orderdt, "+ORDER_HEADER+".iscash, mname, onlineorderno, oflnordno, aprxordval, pushstatus, pymnttotal, "+ORDER_HEADER+".suserid "+" FROM "+ORDER_HEADER+" JOIN "+ ORDER_DETAIL +" ON "+ ORDER_HEADER+".oflnordid = "+ORDER_DETAIL+".oflnordid "+" where ( " +ORDER_HEADER+".pushstatus ="+"'"+status+"'" +" ) "+" ORDER BY "+ORDER_HEADER+".oflnordid DESC";
            String selectQuery = "SELECT  * FROM " + ORDER_HEADER+" where (pushstatus ="+"'"+status+"'"+") AND (date(orderdt) = "+"'"+date+"')"+"ORDER BY oflnordid DESC"; //(orderdt="+"'"+date+"')
            SQLiteDatabase db = this.getReadableDatabase();
            cursor = db.rawQuery(selectQuery, null);
        }else{
            String selectQuery = "SELECT  * FROM " + ORDER_HEADER+" where pushstatus ="+"'"+status+"'" +"ORDER BY oflnordid DESC";
            SQLiteDatabase db = this.getReadableDatabase();
            cursor = db.rawQuery(selectQuery, null);
        }
        return cursor;
    }

    public Cursor getStockSuccessheader(String status,String date) {
        Log.e("status",status);
        Log.e("date",date);
        Cursor cursor;

        if(status.equals("Success")){
            //String selectQuery="SELECT DISTINCT "+ORDER_HEADER+".oflnordid, "+ORDER_HEADER+".muserid, "+ORDER_DETAIL+".isread, "+ORDER_HEADER+".orderdt, "+ORDER_HEADER+".iscash, mname, onlineorderno, oflnordno, aprxordval, pushstatus, pymnttotal, "+ORDER_HEADER+".suserid "+" FROM "+ORDER_HEADER+" JOIN "+ ORDER_DETAIL +" ON "+ ORDER_HEADER+".oflnordid = "+ORDER_DETAIL+".oflnordid "+" where ( " +ORDER_HEADER+".pushstatus ="+"'"+status+"'" +" ) "+" ORDER BY "+ORDER_HEADER+".oflnordid DESC";
            String selectQuery = "SELECT  * FROM " + STOCK_HEADER+" where (pushstatus ="+"'"+status+"'"+") AND (date(orderdt) = "+"'"+date+"')"+"ORDER BY oflnordid DESC"; //(orderdt="+"'"+date+"')
            SQLiteDatabase db = this.getReadableDatabase();
            cursor = db.rawQuery(selectQuery, null);
        }else{
            String selectQuery = "SELECT  * FROM " + STOCK_HEADER+" where pushstatus ="+"'"+status+"'" +"ORDER BY oflnordid DESC";
            SQLiteDatabase db = this.getReadableDatabase();
            cursor = db.rawQuery(selectQuery, null);
        }


        return cursor;
    }

    public void insertSyncData(final String reqUrl, final String reqParams){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_REQUEST_URL, reqUrl);
        contentValues.put(KEY_REQUEST_PARAMS, reqParams);
        sqLiteDatabase.insert(SYNC_TABLE, null, contentValues);
    }

    public void insertStockEntry(final String stockEntry){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_DIST_STOCK_ENTRY, stockEntry);
        sqLiteDatabase.insert(DIST_STOCK_ENTRY_TABLE, null, contentValues);
    }

    public void insertOutletStockEntry(final String stockEntry){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_OUTLET_STOCK_ENTRY, stockEntry);
        sqLiteDatabase.insert(OUTLET_STOCK_ENTRY_TABLE, null, contentValues);
    }

    public boolean addclientvisit( String duserid,String merchantlist,String discription, String date, double latitude, double longtitude,String status,String salesid,String clientstatus,String mname,int merchantPrimaryKey) {
        SQLiteDatabase sdbh = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(KEY_salesid, salesid);
        contentValues.put(KEY_merchid, merchantlist);
        contentValues.put(KEY_plandt, date);
        contentValues.put(KEY_remarks, discription);
        contentValues.put(KEY_status, status);
        contentValues.put(KEY_lat, latitude);
        contentValues.put(KEY_long, longtitude);
        contentValues.put(KEY_dealid, duserid);
        contentValues.put(KEY_clientstatus, clientstatus);
        contentValues.put(KEY_Merchantname, mname);
        contentValues.put("rowid", merchantPrimaryKey);  //Kumaravel

        sdbh.insert(Client_visit, null, contentValues);

        sdbh.close();
        return false;
    }


    @SuppressLint("NewApi")
    public boolean addtracklog( String suserid,String lat, String lng, String date, String status, String updatdedt,String distance,String amount) {
        SQLiteDatabase sdbh = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(KEY_tracksuserid, suserid);
        contentValues.put(KEY_tracklat, lat);
        contentValues.put(KEY_tracklng, lng);
        contentValues.put(KEY_trackdate, date);
        contentValues.put(KEY_trackstatus, status);
        contentValues.put(KEY_trackupdateddt, updatdedt);
        contentValues.put(KEY_trackdistance, distance);
        contentValues.put(KEY_trackamount, amount);

        sdbh.insert(Track_Log,null,contentValues);

        sdbh.close();
        return false;
    }

    //UOM table

    @SuppressLint("NewApi")
    public boolean addProductUOM( String uomid,String prodid, String duserid, String uomname,String uomvalue,String updateddt, String status,String isdefault) {
        SQLiteDatabase sdbh = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(KEY_Produom_uomid, uomid);
        contentValues.put(KEY_Produom_prodid, prodid);
        contentValues.put(KEY_Produom_duserid, duserid);
        contentValues.put(KEY_Produom_updateddt, updateddt);
        contentValues.put(KEY_Produom_status, status);
        contentValues.put(KEY_Produom_uomname, uomname);
        contentValues.put(KEY_Produom_uomvalue, uomvalue);
        contentValues.put(KEY_Produom_isdefault, isdefault);
        sdbh.insert(Product_UOM_Table,null,contentValues);

        sdbh.close();
        return false;
    }

    @SuppressLint("NewApi")
    public boolean addUserSetting( String userid,String refkey, String refvalue, String updateddt, String status, String remarks) {
        SQLiteDatabase sdbh = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(KEY_settingUserid, userid);
        contentValues.put(KEY_settingrefkey, refkey);
        contentValues.put(KEY_settingrefvalue, refvalue);
        contentValues.put(KEY_settingupdateddt, updateddt);
        contentValues.put(KEY_settingstatus, status);
        contentValues.put(KEY_settingremarks, remarks);

        sdbh.insert(User_Setting_Table,null,contentValues);

        sdbh.close();
        return false;
    }

    @SuppressLint("NewApi")
    public boolean addCustomField(String duserid, String custmfldid, String custmfldvalue ) {
        SQLiteDatabase sdbh = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(KEY_customfield_custmfldid, custmfldid);
        contentValues.put(KEY_customfield_duserid, duserid);
        contentValues.put(KEY_customfield_custmfldvalue, custmfldvalue);
        sdbh.insert(Custom_Field_Table,null,contentValues);

        sdbh.close();
        return false;
    }
    @SuppressLint("NewApi")
    public boolean addProductStting( String prodid,String refkey, String refvalue, String muserid, String status, String updateddt, String duserid) {
        SQLiteDatabase sdbh = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(KEY_ProdStt_prodid, prodid);
        contentValues.put(KEY_refkey, refkey);
        contentValues.put(KEY_refvalue, refvalue);
        contentValues.put(KEY_muserid, muserid);
        contentValues.put(KEY_ProdStt_status, status);
        contentValues.put(KEY_ProdStt_updateddt, updateddt);
        contentValues.put(KEY_ProdStt_duserid, duserid);

        sdbh.insert(Product_Setting_Table,null,contentValues);

        sdbh.close();
        return false;
    }

    @SuppressLint("NewApi")
    public boolean addMerchantNew(String companyname, String userid, String fullname,String merchantmobileno,String mflag,String email,String address,String city,String pincode,String tin,String survey,
                               String day, String date,String pan_no, String customdata,String strStockiest,String strRoute,String strState,
                                  String hierarchyobj, String hierarchytext, String geolat, String geolang) {
        //JSONArray jsonArCustomData
        SQLiteDatabase sdbh = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_Mcompanyname, companyname);
        contentValues.put(KEY_Muserid, userid);
        contentValues.put(KEY_Mfullname, fullname);
        contentValues.put(KEY_MOBILENUMER, merchantmobileno);
        contentValues.put(KEY_MFlag,mflag);
        contentValues.put(KEY_EMAIL,email);
        contentValues.put(KEY_ADDRESS,address);
        contentValues.put(KEY_CITY,city);
        contentValues.put(KEY_PINCODE,pincode);
        contentValues.put(KEY_TIN,tin);
        contentValues.put(KEY_CUSTOMDATA,customdata);
        contentValues.put(KEY_SURVEY,survey);
        contentValues.put(KEY_DAY,day);
        contentValues.put(KEY_Date,date);
        contentValues.put(KEY_PAN,pan_no);
        contentValues.put(KEY_STOCKIEST,strStockiest);
        contentValues.put(KEY_ROUTE,strRoute);
        contentValues.put(KEY_STATE,strState);

        ///////////////////////////////////////
        contentValues.put(KEY_HIERARCHY_OBJ,hierarchyobj);
        contentValues.put(KEY_HIERARCHY_TEXT,hierarchytext);
        //////////////////////////////////////
        contentValues.put(KEY_MERCHANT_GEO_LAT, geolat);
        contentValues.put(KEY_MERCHANT_GEO_LON, geolang);
        ////////////////////////////////26-09-2019

        sdbh.insert(MERCH_NAME,null,contentValues);

			/*sdbh.insertWithOnConflict(MERCH_NAME, null, values,
					SQLiteDatabase.CONFLICT_REPLACE);*/
        sdbh.close();
        return false;
    }

    @SuppressLint("NewApi")
    public boolean addmerchant(String companyname, String userid, String fullname,String merchantmobileno,String mflag,String email,String address,String city,String pincode,String tin,String survey,
                               String day, String date,String pan_no, String customdata,String strStockiest,String strRoute,String strState ) {
        //JSONArray jsonArCustomData
        SQLiteDatabase sdbh = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_Mcompanyname, companyname);
        contentValues.put(KEY_Muserid, userid);
        contentValues.put(KEY_Mfullname, fullname);
        contentValues.put(KEY_MOBILENUMER, merchantmobileno);
        contentValues.put(KEY_MFlag,mflag);
        contentValues.put(KEY_EMAIL,email);
        contentValues.put(KEY_ADDRESS,address);
        contentValues.put(KEY_CITY,city);
        contentValues.put(KEY_PINCODE,pincode);
        contentValues.put(KEY_TIN,tin);
        contentValues.put(KEY_CUSTOMDATA,customdata);
        contentValues.put(KEY_SURVEY,survey);
        contentValues.put(KEY_DAY,day);
        contentValues.put(KEY_Date,date);
        contentValues.put(KEY_PAN,pan_no);
        contentValues.put(KEY_STOCKIEST,strStockiest);
        contentValues.put(KEY_ROUTE,strRoute);
        contentValues.put(KEY_STATE,strState);

        sdbh.insert(MERCH_NAME,null,contentValues);

			/*sdbh.insertWithOnConflict(MERCH_NAME, null, values,
					SQLiteDatabase.CONFLICT_REPLACE);*/
        sdbh.close();
        return false;
    }
    @SuppressLint("NewApi")
    public boolean adddealer( String companyname,String usrdlrid, String userid, String fullname, String address,String Dorderdate) {
        SQLiteDatabase sdbh = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_Dcompanyname, companyname);
        contentValues.put(KEY_usrdlrid, usrdlrid);
        contentValues.put(KEY_Duserid, userid);
        contentValues.put(KEY_Dfullname, fullname);
        contentValues.put(KEY_Daddress, address);
        contentValues.put(KEY_date, Dorderdate);

        sdbh.insert(DEAL_NAME,null,contentValues);


        sdbh.close();
        return false;
    }

    @SuppressLint("NewApi")
    public boolean addSchemes( String prodid,String duserid, String startdate, String enddate, String discount,
                               String schemetype, String freeprodid,String freeqty,String status,String remark) {
        SQLiteDatabase sdbh = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(KEY_scheme_prodid, prodid);
        contentValues.put(KEY_scheme_duserid, duserid);
        contentValues.put(KEY_scheme_startDate, startdate);
        contentValues.put(KEY_scheme_endDate, enddate);
        contentValues.put(KEY_scheme_discount, discount);
        contentValues.put(KEY_scheme_type, schemetype);
        contentValues.put(KEY_scheme_freeProdid, freeprodid);
        contentValues.put(KEY_scheme_freeQty, freeqty);
        contentValues.put(KEY_scheme_status, status);
        contentValues.put(KEY_scheme_remark, remark);

        sdbh.insert(Scheme_Table,null,contentValues);

        sdbh.close();
        return false;
    }
    @SuppressLint("NewApi")

    public boolean addProd( String userid,String companyname, String prodcode , String prodid, String prodname,String qty,String fqty,String updateddt,String Porderdate,String prodprice,
                            String prodtax,String batchno,String mfgdate,String expdate,String uom,String serialno,String unitgram,String unitperuom,String prodimage, String currstock) {
        SQLiteDatabase sdbh = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_userid, userid);
        contentValues.put(KEY_companyname, companyname);
        contentValues.put(KEY_prodcode, prodcode);
        contentValues.put(KEY_prodid, prodid);
        contentValues.put(KEY_prodname, prodname);
        contentValues.put(KEY_quantity, qty);
        contentValues.put(KEY_freeqty, fqty);
        contentValues.put(KEY_updateddt, updateddt);
        contentValues.put(KEY_currentdate, Porderdate);
        contentValues.put(KEY_prodprice, prodprice);
        contentValues.put(KEY_prodtax, prodtax);
        contentValues.put(KEY_Prodbatchno, batchno);
        contentValues.put(KEY_mfgdate, mfgdate);
        contentValues.put(KEY_expdate, expdate);
        contentValues.put(KEY_ProdUOM, uom);
        contentValues.put(KEY_serialNo, serialno);
        contentValues.put(KEY_unitGram, unitgram);
        contentValues.put(KEY_unitperuom, unitperuom);
        contentValues.put(KEY_prodimage,prodimage);
        contentValues.put(KEY_currstock,currstock);

        Log.e("uom",uom);

        sdbh.insert(PROD_NAME,null,contentValues);

        sdbh.close();
        return false;
    }

    @SuppressLint("NewApi")
    public boolean addStockOrderDetail( String orderid,String duserid,String  dcomany,String prodid , String qty, String flag,String status,String index,String prodCode, String prodName,String deliverydt,String prate,String ptax,Double pvalue,
                                   String isread,String outstock,String batchno,String manufdate,String expirydate,String ordimage,String uom,String orderuom,Double defaultUomTotal,
                                   String deliverytime,String deliverymode, String currstock) {
        SQLiteDatabase sdbh = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_order, orderid);
        contentValues.put(KEY_duserid, duserid);
        contentValues.put(KEY_oprodid, prodid);
        contentValues.put(KEY_Dcompany, dcomany);
        contentValues.put(KEY_qty, qty);
        contentValues.put(KEY_flag, flag);
        contentValues.put(KEY_orderStatus, status);
        contentValues.put(KEY_index, index);
        contentValues.put(KEY_ProdCode, prodCode);
        contentValues.put(KEY_ProdName, prodName);
        contentValues.put(KEY_prate, prate);
        contentValues.put(KEY_ptax, ptax);
        contentValues.put(KEY_pvalue, pvalue);
        contentValues.put(KEY_isreaddtl, isread);
        contentValues.put(KEY_batchno, batchno);
        contentValues.put(KEY_manufdate, manufdate);
        contentValues.put(KEY_expirydate, expirydate);
        contentValues.put(KEY_outstock, outstock);
        contentValues.put(KEY_ordimage, ordimage);
        contentValues.put(KEY_uom, uom);
        contentValues.put(KEY_order_uom, orderuom);
        contentValues.put(KEY_default_uom_total, defaultUomTotal);
        contentValues.put(KEY_delivery_mode, deliverymode);
        contentValues.put(KEY_delivery_time, deliverytime);
        contentValues.put(KEY_Odeliverydt, deliverydt);
        contentValues.put(KEY_current_stock,currstock);

        sdbh.insert(STOCK_DETAIL,null, contentValues);

        sdbh.close();
        return false;

    }

    @SuppressLint("NewApi")
    public boolean addorderdetail( String orderid,String duserid,String  dcomany,String prodid , String qty, String flag,String status,String index,String prodCode, String prodName,String deliverydt,String prate,String ptax,Double pvalue,
                                   String isread,String outstock,String batchno,String manufdate,String expirydate,String ordimage,String uom,String orderuom,Double defaultUomTotal,
                                   String deliverytime,String deliverymode, String currstock) {
        SQLiteDatabase sdbh = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_order, orderid);
        contentValues.put(KEY_duserid, duserid);
        contentValues.put(KEY_oprodid, prodid);
        contentValues.put(KEY_Dcompany, dcomany);
        contentValues.put(KEY_qty, qty);
        contentValues.put(KEY_flag, flag);
        contentValues.put(KEY_orderStatus, status);
        contentValues.put(KEY_index, index);
        contentValues.put(KEY_ProdCode, prodCode);
        contentValues.put(KEY_ProdName, prodName);
        contentValues.put(KEY_prate, prate);
        contentValues.put(KEY_ptax, ptax);
        contentValues.put(KEY_pvalue, pvalue);
        contentValues.put(KEY_isreaddtl, isread);
        contentValues.put(KEY_batchno, batchno);
        contentValues.put(KEY_manufdate, manufdate);
        contentValues.put(KEY_expirydate, expirydate);
        contentValues.put(KEY_outstock, outstock);
        contentValues.put(KEY_ordimage, ordimage);
        contentValues.put(KEY_uom, uom);
        contentValues.put(KEY_order_uom, orderuom);
        contentValues.put(KEY_default_uom_total, defaultUomTotal);
        contentValues.put(KEY_delivery_mode, deliverymode);
        contentValues.put(KEY_delivery_time, deliverytime);
        contentValues.put(KEY_Odeliverydt, deliverydt);
        contentValues.put(KEY_current_stock,currstock);

        sdbh.insert(ORDER_DETAIL,null, contentValues);

        sdbh.close();
        return false;

    }
    @SuppressLint("NewApi")
    public boolean addStockOrderHeader(String suseid,String muserid,String mname, String paymentType , String pushStatus,String orderDate,String onlineordno,
                                  String offlineOrdNo,String aprxordval,double geolat,double geolong,
                                  String starttime,String endtime,String followup) {
        SQLiteDatabase sdbh = this.getWritableDatabase();

        Log.e("suseid", suseid);
        Log.e("geolat", String.valueOf(geolat));
        Log.e("aprxordval", aprxordval);
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_SOuserid, suseid);
        contentValues.put(KEY_MOuserid, muserid);
        contentValues.put(KEY_Paymtype, paymentType);
        contentValues.put(KEY_pushStatus, pushStatus);
        contentValues.put(KEY_Mname, mname);
        contentValues.put(KEY_onlineOrderNo, onlineordno);
        contentValues.put(KEY_offOrderno, offlineOrdNo);
        contentValues.put(KEY_Orderdate, orderDate);
        contentValues.put(KEY_aprxordval, aprxordval);
        contentValues.put(KEY_geolat, geolat);
        contentValues.put(KEY_geolong, geolong);
        contentValues.put(KEY_starttime, starttime);
        contentValues.put(KEY_endtime, endtime);
        contentValues.put(KEY_followup, followup);
        Log.e("otside", "otside");

        sdbh.insert(STOCK_HEADER, null, contentValues);
        Log.e("outside", "outside");
        sdbh.close();
        return false;
    }
    @SuppressLint("NewApi")
    public boolean addorderheader(String suseid,String muserid,String mname, String paymentType , String pushStatus,String orderDate,String onlineordno,
                                  String offlineOrdNo,String aprxordval,double geolat,double geolong,
                                  String starttime,String endtime,String followup) {
        SQLiteDatabase sdbh = this.getWritableDatabase();

        ///Log.e("suseid", suseid);  this produce exception
        Log.e("geolat", String.valueOf(geolat));
        Log.e("aprxordval", aprxordval);
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_SOuserid, suseid);
        contentValues.put(KEY_MOuserid, muserid);
        contentValues.put(KEY_Paymtype, paymentType);
        contentValues.put(KEY_pushStatus, pushStatus);
        contentValues.put(KEY_Mname, mname);
        contentValues.put(KEY_onlineOrderNo, onlineordno);
        contentValues.put(KEY_offOrderno, offlineOrdNo);
        contentValues.put(KEY_Orderdate, orderDate);
        contentValues.put(KEY_aprxordval, aprxordval);
        contentValues.put(KEY_geolat, geolat);
        contentValues.put(KEY_geolong, geolong);
        contentValues.put(KEY_starttime, starttime);
        contentValues.put(KEY_endtime, endtime);
        contentValues.put(KEY_followup, followup);
        Log.e("otside", "otside");

        sdbh.insert(ORDER_HEADER, null, contentValues);
        Log.e("outside", "outside");
        sdbh.close();
        return false;
    }

    public boolean addCompanyModelbeat( String beatid,String muserid,String suserid, String duserid, String day_mon, String day_tue,String day_wed,String day_thu,String day_fri,String day_sat,String day_sun,String status,String createddt,String updateddt,
                                        String flag,String mid,String beattype,String beatname,String fullname,String usertype,String companyname) {
        SQLiteDatabase sdbh = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(KEY_beatid, beatid);
        contentValues.put(KEY_beatmuserid, muserid);
        contentValues.put(KEY_beatsuserid, suserid);
        contentValues.put(KEY_beatduserid, duserid);
        contentValues.put(KEY_day_mon, day_mon);
        contentValues.put(KEY_day_tue, day_tue);
        contentValues.put(KEY_day_wed, day_wed);
        contentValues.put(KEY_day_thu, day_thu);
        contentValues.put(KEY_day_fri, day_fri);
        contentValues.put(KEY_day_sat, day_sat);
        contentValues.put(KEY_day_sun, day_sun);
        contentValues.put(KEY_beatstatus, status);
        contentValues.put(KEY_beatcreateddt, createddt);
        contentValues.put(KEY_beatupdateddt, updateddt);
        contentValues.put(KEY_beatflag, flag);
        contentValues.put(KEY_mid, mid);
        contentValues.put(KEY_beatcompanyname, companyname);
        contentValues.put(KEY_beatname, beatname);
        contentValues.put(KEY_fullname, fullname);
        contentValues.put(KEY_beatusertype, usertype);
        contentValues.put(KEY_beattype, beattype);

        sdbh.insert(Beat_Table, null, contentValues);

        sdbh.close();
        return false;
    }

    public boolean addDistributorModelbeat( String beatid,String muserid,String suserid, String duserid, String day_mon, String day_tue,String day_wed,String day_thu,String day_fri,String day_sat,String day_sun,String status,String createddt,String updateddt,
                                            String flag,String mid,String beattype,String beatname) {
        SQLiteDatabase sdbh = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(KEY_beatid, beatid);
        contentValues.put(KEY_beatmuserid, muserid);
        contentValues.put(KEY_beatsuserid, suserid);
        contentValues.put(KEY_beatduserid, duserid);
        contentValues.put(KEY_day_mon, day_mon);
        contentValues.put(KEY_day_tue, day_tue);
        contentValues.put(KEY_day_wed, day_wed);
        contentValues.put(KEY_day_thu, day_thu);
        contentValues.put(KEY_day_fri, day_fri);
        contentValues.put(KEY_day_sat, day_sat);
        contentValues.put(KEY_day_sun, day_sun);
        contentValues.put(KEY_beatstatus, status);
        contentValues.put(KEY_beatcreateddt, createddt);
        contentValues.put(KEY_beatupdateddt, updateddt);
        contentValues.put(KEY_beatflag, flag);
        contentValues.put(KEY_mid, mid);
        contentValues.put(KEY_beatname, beatname);
        contentValues.put(KEY_beattype, beattype);

        sdbh.insert(Beat_Table, null, contentValues);

        sdbh.close();
        return false;
    }

    public void setBeatOfflineId(String id, int headerId){
        SQLiteDatabase sdbh= this.getWritableDatabase();
        String strSql = "UPDATE "+Beat_Table+" SET oflnordid = '"+id+"' WHERE rowid = '"+ headerId+"'";
        sdbh.execSQL(strSql);

        sdbh.close();
    }

    public void updateSelectedBeat(String beatname,String userid,String date){
        SQLiteDatabase sdbh= this.getWritableDatabase();
        //String strSql = "UPDATE "+Beat_Table+" SET "+KEY_selectedBeat +" = '"+beatname+"',"+KEY_selectedDate+" = '"+date+"' WHERE "+KEY_id+" = '"+ userid+"'";
        String strSql = "UPDATE "+TABLE_NAME+" SET "+KEY_selectedBeat +" = '"+beatname+"',"+KEY_selectedDate+" = '"+date+"' WHERE "+KEY_id+" = '"+ userid+"'";
        sdbh.execSQL(strSql);

        sdbh.close();
    }

    public void updatesignintable(ContentValues values){
        SQLiteDatabase sdbh= this.getWritableDatabase();


        String strSql = "UPDATE "+TABLE_NAME+" SET "+KEY_paymentdate +" = '"+values.get(KEY_paymentdate)+"' WHERE "+KEY_id+" = '"+ values.get(KEY_id)+"'";
        sdbh.execSQL(strSql);

        sdbh.close();
    }
    public void updateUserDetails(ContentValues values){
        SQLiteDatabase sdbh= this.getWritableDatabase();


        String strSql = "UPDATE "+TABLE_NAME+" SET "+KEY_name +" = '"+values.get(KEY_name)+"',"+KEY_payment +" = '"+values.get(KEY_payment)+"' WHERE "+KEY_username+" = '"+ values.get(KEY_username)+"'";
        sdbh.execSQL(strSql);

        sdbh.close();
    }

    public void updateMerchant(ContentValues values){
        SQLiteDatabase sdbh= this.getWritableDatabase();


        String strSql = "UPDATE "+MERCH_NAME+" SET "+KEY_Muserid +" = '"+values.get(KEY_Muserid)+"',"+KEY_MFlag +" = '"+values.get(KEY_MFlag)+"' WHERE "+KEY_MOBILENUMER+" = '"+ values.get(KEY_MOBILENUMER)+"'";
        sdbh.execSQL(strSql);

        sdbh.close();
    }
    public void updateBeatMercahant(String muserid,String rowid){
        SQLiteDatabase sdbh= this.getWritableDatabase();


        String strSql = "UPDATE "+Beat_Table+" SET "+KEY_beatmuserid +" = '"+muserid+"' WHERE "+KEY_mid+" = '"+rowid+"'";
        sdbh.execSQL(strSql);
        Log.e("newmuserid",muserid);
        sdbh.close();
    }

    public void updateCompanyBeatMerchant(String muserid,String name){
        SQLiteDatabase sdbh= this.getWritableDatabase();

        String strSql = "UPDATE "+Beat_Table+" SET "+KEY_beatmuserid +" = '"+muserid+"' WHERE "+KEY_beatname+" = '"+name+"'";
        sdbh.execSQL(strSql);
        Log.e("updateCompanyBeat",muserid);
        sdbh.close();
    }

    public void updateStockOrderHeader(ContentValues values){
        SQLiteDatabase sdbh= this.getWritableDatabase();


        String strSql = "UPDATE "+STOCK_HEADER+" SET "+KEY_pushStatus +" = '"+values.get(KEY_pushStatus)+"',"+KEY_onlineOrderNo +" = '"+values.get(KEY_onlineOrderNo)+"' WHERE "+KEY_orderno+" = '"+ values.get(KEY_orderno)+"'";
        sdbh.execSQL(strSql);

        sdbh.close();
    }

    public void updateOrderHeader(ContentValues values){
        SQLiteDatabase sdbh= this.getWritableDatabase();


        String strSql = "UPDATE "+ORDER_HEADER+" SET "+KEY_pushStatus +" = '"+values.get(KEY_pushStatus)+"',"+KEY_onlineOrderNo +" = '"+values.get(KEY_onlineOrderNo)+"' WHERE "+KEY_orderno+" = '"+ values.get(KEY_orderno)+"'";
        sdbh.execSQL(strSql);

        sdbh.close();
    }

    public void updateorderDetail(ContentValues values){
        SQLiteDatabase sdbh= this.getWritableDatabase();


        String strSql = "UPDATE "+ORDER_DETAIL+" SET "+KEY_qty +" = '"+values.get(KEY_qty)+"',"+KEY_orderStatus +" = '"+values.get(KEY_orderStatus)+"',"+KEY_prate +" = '"+values.get(KEY_prate)+"',"+KEY_Oupdateddt +" = '"+values.get(KEY_Oupdateddt)+"',"+KEY_order_uom +" = '"+values.get(KEY_order_uom)+"',"
                +KEY_Odeliverydt +" = '"+values.get(KEY_Odeliverydt)+"',"+KEY_delivery_time +" = '"+values.get(KEY_delivery_time)+"',"+KEY_default_uom_total +" = '"+values.get(KEY_default_uom_total)+"',"+KEY_pvalue +" = '"+values.get(KEY_pvalue)+"',"+KEY_delivery_mode +" = '"+values.get(KEY_delivery_mode)
                +"' WHERE "+KEY_orddtlId+" = '"+ values.get(KEY_orddtlId)+"'" ;
        sdbh.execSQL(strSql);

        sdbh.close();
    }

    public void updateorderHeader(ContentValues values,String status){
        SQLiteDatabase sdbh= this.getWritableDatabase();
        String strSql="";

        if(status.equals("Success")){
            strSql = "UPDATE "+ORDER_HEADER+" SET "+KEY_aprxordval +" = '"+values.get(KEY_aprxordval)+"' WHERE "+KEY_onlineOrderNo+" = '"+ values.get(KEY_onlineOrderNo)+"'" ;


        }else{
            strSql = "UPDATE "+ORDER_HEADER+" SET "+KEY_aprxordval +" = '"+values.get(KEY_aprxordval)+"' WHERE "+KEY_orderno+" = '"+ values.get(KEY_orderno)+"'" ;

        }

        sdbh.execSQL(strSql);

        sdbh.close();
    }
    public void delete() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM signintable");
        db.execSQL("DELETE FROM prodtable");
        db.execSQL("DELETE FROM dealertable");
        db.execSQL("DELETE FROM merchanttable");
        db.execSQL("DELETE FROM oredrdetail");
        db.execSQL("DELETE FROM orderheader");
        db.execSQL("DELETE FROM clientvisit");
        db.execSQL("DELETE FROM beat");
        db.execSQL("DELETE FROM scheme");
        db.execSQL("DELETE FROM productSetting");
        db.execSQL("DELETE FROM usersetting");
        db.execSQL("DELETE FROM productuom");
        db.execSQL("DELETE FROM synctable");
        db.execSQL("DELETE FROM diststockentry");
        db.execSQL("DELETE FROM outletstockentry");
        Constants.Merchantname="";
        Constants.SalesMerchant_Id="";
        // delete all rows in a table
        //Kumaravel 5 / 1 /2019
        db.execSQL("DELETE FROM " + TBL_TEMPLATE_TYPE);
        db.execSQL("DELETE FROM " + TBL_MILK_DATA_CHECK);
        db.execSQL("DELETE FROM " + TBL_FAT_SNF_RATE);
        db.execSQL("DELETE FROM " + TBL_SNF_CAL);
        db.execSQL("DELETE FROM " + TBL_ORDER_INFO);
        db.execSQL("DELETE FROM " + TBL_POST_NOTE);
        db.execSQL("DELETE FROM " + TBL_PKD_OUTLET_STOCK_SYNC);
        db.execSQL("DELETE FROM " + TBL_SURVEY_SYNC);
        db.execSQL("DELETE FROM " + TBL_DISTRIBUTOR_STOCK_SYNC);
        db.execSQL("DELETE FROM " + STOCK_DETAIL);
        db.execSQL("DELETE FROM " + STOCK_HEADER);
        db.execSQL("DELETE FROM " + TBL_MERCHANT_ORDER_RELATION);
        ///Kumaravel 18-07-2019
        db.execSQL("DELETE FROM " + TBL_ERROR);
        db.execSQL("DELETE FROM " + TBL_LOCATION);
        db.execSQL("DELETE FROM " + TBL_MENU);
        db.execSQL("DELETE FROM " + TBL_SM_GEOLOG);
        db.execSQL("DELETE FROM " + Track_Log);
        //////////////05-08-2019
        db.execSQL("DELETE FROM " + TBL_DISTRIBUTOR);
        db.close();
    }

    public void synTableDelete(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM prodtable");
        db.execSQL("DELETE FROM dealertable");
        db.execSQL("DELETE FROM merchanttable");
        db.execSQL("DELETE FROM clientvisit");
        db.execSQL("DELETE FROM beat");
        db.execSQL("DELETE FROM scheme");
        db.execSQL("DELETE FROM productSetting");
        db.execSQL("DELETE FROM usersetting");
        db.execSQL("DELETE FROM productuom");
        db.execSQL("DELETE FROM synctable");
        db.execSQL("DELETE FROM diststockentry");
        db.execSQL("DELETE FROM outletstockentry");
        //db.execSQL("DELETE FROM postnote");

        //////////////05-08-2019
        db.execSQL("DELETE FROM " + TBL_DISTRIBUTOR);

        Constants.Merchantname="";
        Constants.SalesMerchant_Id="";
        db.close();

        //Kumaravel
        //to maintain order for one day we wont delete order tables
        //db.execSQL("DELETE FROM oredrdetail");
        //db.execSQL("DELETE FROM orderheader");

      /*  SalesManOrderActivity.moveto="sync";
        SalesManOrderActivity.getData();*/

        // SalesManOrderActivity.offlineDealers();
    }

    public void updateSyncTable(final int id, final int flag){
        SQLiteDatabase database= this.getWritableDatabase();
        String strSql = "UPDATE "+SYNC_TABLE+" SET flag = '"+flag+"' WHERE "+KEY_SYNC_ID+" = '"+id+"'";
        database.execSQL(strSql);
        database.close();
    }

    public boolean deleteSyncTable(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(SYNC_TABLE, KEY_SYNC_ID +  "=" + id, null) > 0;
    }

    public void deletescheme(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM scheme");

        Constants.Merchantname="";
        Constants.SalesMerchant_Id="";
        db.close();
    }

    public SQLiteDatabase updateDB(SQLiteDatabase db){
        return this.getWritableDatabase();
    }


    public Cursor getDistributorNameById(String duserid) {
        String selectQuery = "SELECT fullname FROM " + Beat_Table+" where  duserid= "+"'"+duserid+"'" ;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public Cursor getAllPendingToChange() {
        String selectQuery = "SELECT * from orderheader where pushstatus IN ('No Order', 'Pending')";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }

    public Cursor getAllPendingOutlet() {
        String selectQuery = "SELECT * from orderheader where pushstatus = 'Outlet'";
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        return cursor;
    }
}
