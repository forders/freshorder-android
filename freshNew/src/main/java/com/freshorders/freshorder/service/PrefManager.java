package com.freshorders.freshorder.service;

import android.content.Context;
import android.content.SharedPreferences;


import com.freshorders.freshorder.R;


public class PrefManager {

    private SharedPreferences.Editor editor;
    private SharedPreferences pref;

    public static final String KEY_IS_FULL_MIGRATION_SET = "is_full_migration_set";
    public static final String KEY_IS_CRASH_HAPPENED = "is_crash_happened";
    public static final String KEY_PATH_CRASH_FILE = "crash_file_path";
    public static final String KEY_PATH_MIGRATION_FILE = "migration_file_path";
    public static final String KEY_PATH_HIERARCHY_PLACE_FILE = "hierarchy_place";
    public static final String KEY_IS_HIERARCHY_PLACE_FILE_LOADED = "is_hierarchy_place_file_loaded";
    public static final String KEY_IS_MIGRATION_SET = "is_migration_set";
    public static final String KEY_IS_NEW_CLIENT_ORDER_MIGRATION_SET = "is_new_client_migration_set";
    public static final String KEY_IS_CONNECTIVITY_FIRED = "is_connectivity_fired"; // this key for avoid multiple time network change triggered
    public static final String KEY_IS_CLOSING_MIGRATION_START = "is_closing_migration_start";
    public static final String KEY_IS_FAILED_CLOSING_MIGRATION_START = "is_failed_closing_stock_migration_start";
    public static final String KEY_IS_FAILED_ORDER_MIGRATION_START = "is_failed_order_migration_start";
    public static final String KEY_MANAGER_TYPE = "manager_type";
    public static final String KEY_IS_MY_DAY_PLAN_LEAVE = "is_my_day_plan_leave";
    public static final String KEY_MY_DAY_PLAN_LEAVE_DATE = "my_day_plan_leave_date";

    public static final String KEY_IS_USER_MOVING = "is_user_moving";

    public static final String KEY_BUG_FILE_PATH = "bug_file_bath" ;

    public static final String MyPREFERENCES = "LocationTracking" ;
    public static String status="Status";

    //Kumaravel to maintain distributor name
    public static final String KEY_DISTRIBUTOR_NAME = "distributor_name" ;

    public static final String IS_DELIVERED_FCM_TOKEN = "is_delivered_fcm_token";
    public static final String FCM_TOKEN = "fcm_token";



    public PrefManager(Context context) {
        String PREF_NAME = context.getResources().getString(R.string.app_name);
        int PRIVATE_MODE = 0;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public boolean clearAllPreference(Context context) {
        SharedPreferences settings = context.getSharedPreferences(context.getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        boolean clear = settings.edit().clear().commit();
        return clear ;
    }

    public boolean clearPreferenceByKey(String key, Context context){
        SharedPreferences preferences = context.getSharedPreferences(context.getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        return preferences.edit().remove(key).commit();
    }

    public void setStringDataByKey(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public String getStringDataByKey(String key) {
        return pref.getString(key, "");
    }

    public int getIntegerDataByKey(String key) {
        return pref.getInt(key, 0);
    }

    public void setIntegerDataByKey(String key, int value){
        editor.putInt(key, value);
        editor.commit();
    }



    public boolean getBooleanDataByKey(String key) {
        return pref.getBoolean(key, false);
    }

    public void setBooleanDataByKey(String key, boolean isTrue) {
        editor.putBoolean(key, isTrue);
        editor.commit();
    }



    public static Boolean getStatus(Context c1) {
        SharedPreferences sharedpreferences = c1.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        return sharedpreferences.getBoolean(status,false);
    }
    public static void setStatus(Context c1,Boolean value) {
        SharedPreferences sharedpreferences = c1.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putBoolean(status, value);
        editor.apply();
    }
}
