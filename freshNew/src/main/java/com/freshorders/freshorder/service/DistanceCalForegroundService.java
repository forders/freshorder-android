package com.freshorders.freshorder.service;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.interfaces.GPSPoints;
import com.freshorders.freshorder.model.GPSPointsDetail;
import com.freshorders.freshorder.model.PointsResponse;
import com.freshorders.freshorder.syncadapter.adapter.SyncAdapterManager;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.volleylib.APIClient;
import com.freshorders.freshorder.volleylib.APIInterface;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class DistanceCalForegroundService extends Service implements LocationListener {

    private static final String TAG = "DistanceCal";

    SyncAdapterManager syncAdapterManager;

    public String bugPath;

    public GPSPoints api;
    public static long previousId = 0l;
    private static boolean isFirst = true;
    DatabaseHandler databaseHandler;
    // flag for GPS status
    boolean isGPSEnabled = false;
    int minutes =1;
    // flag for GPS status
    boolean canGetLocation = false;

    int notiID =1001;
    int accuracy = 500;

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 5; // 10 meters
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 30 * 1; // 1 minute
    // Declaring a Location Manager
    protected LocationManager locationManager;
    //  private FusedLocationProviderClient fusedLocationClient;
    String NOTIFICATION_CHANNEL_ID = "Channel_location1";
    Disposable disposable;

    APIInterface apiInterface;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        databaseHandler = new DatabaseHandler(getApplicationContext());
        syncAdapterManager = new SyncAdapterManager(getApplicationContext());
        syncAdapterManager.initializePeriodicSync();
        bugPath = new PrefManager(getApplicationContext()).getStringDataByKey(PrefManager.KEY_BUG_FILE_PATH);
        Toast.makeText(this, "Service Created", Toast.LENGTH_LONG).show();

        apiInterface = APIClient.getClient().create(APIInterface.class);

        insertError("SERVICE INITIATED");

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        insertError("start");


        Toast.makeText(this, "Creating Notification", Toast.LENGTH_SHORT).show();

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant") NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_HIGH);

            // Configure the notification channel.
            notificationChannel.setDescription("Channel description");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }




        Notification notification = getMyNotification("Tracking Route...");
        startForeground(notiID, notification);

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        // getting network status


        if (!isGPSEnabled) {
            // no network provider is enabled
            showSettingsAlert();
            insertError("GPS is Disabled");
        }
        //  fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        Observable.interval(minutes, TimeUnit.MINUTES)
                .subscribeOn(Schedulers.newThread())
                .repeat()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Long>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(Long aLong) {
                        getLocation();
                    }

                    @Override
                    public void onError(Throwable e) {
                        insertError(e.getMessage()+"");
                    }

                    @Override
                    public void onComplete() {
                        insertError("complete");
                    }
                });
        return START_STICKY;
    }

    private void insertError(String message) {
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

        String date = simpleDateFormat.format(new Date(System.currentTimeMillis()));
        System.out.println(date);

        String pattern1 = "HH : mm : ss";
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(pattern1);

        String time = simpleDateFormat1.format(new Date(System.currentTimeMillis()));
        System.out.println(time);

        ContentValues cv = new ContentValues();
        cv.put(DatabaseHandler.KEY_ERROR_MESSAGE, message);
        cv.put(DatabaseHandler.KEY_ERROR_DATE, date);
        cv.put(DatabaseHandler.KEY_ERROR_TIME, time);
        databaseHandler.insertError(cv);
        //AppDatabase.getAppDatabase(DistanceCalForegroundService.this).errorDao().insert(new errorModel(message,date,time));
    }


    @Override
    public void onDestroy() {
        Toast.makeText(this, "Service Stopped", Toast.LENGTH_LONG).show();
        if(!disposable.isDisposed()) {
            disposable.dispose();
        }
        insertError("destroy");
        Intent restartService = new Intent("RestartService");
        sendBroadcast(restartService);
    }

    public void getLocation() {
        try {

            // getting GPS status

            this.canGetLocation = true;



            // if GPS Enabled get lat/long using GPS Services


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    Activity#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for Activity#requestPermissions for more details.

                    insertError("permission");
                    return ;
                }
            }
                 /*       fusedLocationClient.getLastLocation()
                               .addOnSuccessListener(new OnSuccessListener<Location>() {
                                   @Override
                                   public void onSuccess(Location location) {
                                        if(location!=null)
                                        {
                                            if(location.hasAccuracy())
                                            {
                                                if(location.getAccuracy()<=accuracy)
                                                {
                                                    insertlocation(location);
                                                }
                                                else
                                                {

                                                    insertError("location LOW accuracy : "+location.getAccuracy()+" by GPS API");
                                                }
                                            }
                                            else
                                            {
                                                insertError("location NO accuracy by fuesd API");
                                            }
                                        }
                                        else
                                        {
                                            insertError("location null by fuesd API");
                                        }
                                   }
                               });*/
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    MIN_TIME_BW_UPDATES,
                    MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

            Log.d("GPS Enabled", "GPS Enabled");
                  /*      if (locationManager != null) {
                       Location     location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);

                            if (location != null) {
                                if(location.hasAccuracy())
                                {
                                    if(location.getAccuracy()<=accuracy)
                                    {
                                        insertlocation(location);
                                    }
                                    else
                                    {
                                        insertError("location LOW accuracy : "+location.getAccuracy()+" by GPS API");
                                    }
                                }
                                else
                                {
                                    insertError("location NO accuracy by GPS API");
                                }

                            }
                            else
                            {
                                insertError("location null by GPS API");
                            }
                        }*/


        } catch (Exception e) {
            e.printStackTrace();
            insertError(e.getMessage()+"");
        }
    }




    private void insertlocation(Location location) {

        if(PrefManager.getStatus(DistanceCalForegroundService.this))
        {
            try{

                if (Constants.USER_ID == null || Constants.USER_ID.isEmpty()) {
                    Cursor curs;
                    curs = databaseHandler.getDetails();
                    if (curs != null && curs.getCount() > 0) {
                        curs.moveToFirst();
                        Constants.USER_ID = curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_id));
                        Log.e("Constants.USER_ID", Constants.USER_ID);
                        curs.close();
                    }
                }

                String pattern = "dd-MM-yyyy";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, Locale.getDefault());

                String date = simpleDateFormat.format(new Date(System.currentTimeMillis()));
                System.out.println(date);

                String pattern1 = "HH:mm:ss";
                SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(pattern1, Locale.getDefault());

                String time = simpleDateFormat1.format(new Date(System.currentTimeMillis()));
                System.out.println(time);
                updateNotification("Last Entry "+date+ ","+time);

                SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                String dateTime = ft.format(new Date(System.currentTimeMillis()));
                long preId = 0;
                Cursor cur_ = databaseHandler.getMaxRowIdFromLocation(date);
                if(cur_ != null && cur_.getCount() >0){
                    cur_.moveToFirst();
                    if(cur_.getString(0) != null) {
                        preId = cur_.getLong(0);
                    }
                }
                Gson gson = new Gson();
                ContentValues values = new ContentValues();
                values.put(DatabaseHandler.KEY_LOCATION_LOCATION, gson.toJson(location).getBytes());
                values.put(DatabaseHandler.KEY_LOCATION_DATE,date);
                values.put(DatabaseHandler.KEY_LOCATION_TIME,time);
                values.put(DatabaseHandler.KEY_LOCATION_DATE_TIME,dateTime);
                values.put(DatabaseHandler.KEY_LOCATION_ACCURACY,location.getAccuracy());
                values.put(DatabaseHandler.KEY_LOCATION_LAT,location.getLatitude());
                values.put(DatabaseHandler.KEY_LOCATION_LON,location.getLongitude());
                values.put(DatabaseHandler.KEY_LOCATION_PREVIOUS_ID,preId);
                values.put(DatabaseHandler.KEY_LOCATION_SUSER_ID,Constants.USER_ID);

                long cur_time  = 0;
                ////cur_time = cur_time + Integer.parseInt(Constants.USER_ID);

                /////////////final Locale locale = getApplicationContext().getResources().getConfiguration().locale;
                //////////////DateTime.setLocale(locale);

                String current = formatDateTime(new Date(System.currentTimeMillis()));
                Log.e("Date","............................current" + current);
                //current = Constants.USER_ID + current;
                cur_time = Long.parseLong(current);

                values.put(DatabaseHandler.KEY_LOCATION_UNIQUE_ID,cur_time);

                long id = databaseHandler.insertLocation(values);

                //JsonServiceHandler JsonServiceHandler = new JsonServiceHandler(Utils.strSaveTracklog, getApplicationContext());
                try {
                    if (isNetAvail(getApplicationContext())) {
                        sendPointsThroughRetrofit(id, databaseHandler, bugPath);/////////
                    } else {
                        Log.e(TAG, "Mobile Data Not Avail......");
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    new com.freshorders.freshorder.utils.Log(bugPath).ee(TAG,"............."+ e.getMessage());
                }
                ///sendPointsThroughRetrofit(id,databaseHandler,JsonServiceHandler);

                //syncAdapterManager.syncImmediately();

                /*new LocationTrackAsyncTask.SendPointAsyncTask(id,databaseHandler,JsonServiceHandler,
                        new LocationTrackAsyncTask.LocationCompleteListener() {
                    @Override
                    public void onComplete(Result result) {
                        Log.e(TAG,".................complete");
                    }
                }).execute();  */

            }catch (Exception e){
                e.printStackTrace();
                new com.freshorders.freshorder.utils.Log(bugPath).ee(TAG,"............."+ e.getMessage());
            }
            //AppDatabase.getAppDatabase(DistanceCalForegroundService.this).locationDao().insert(new locationModel(location, date, time, location.getAccuracy()));
        }
    }









    private GPSPoints getRetrofit(){
        if(api == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(GPSPoints.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            api = retrofit.create(GPSPoints.class);
        }
        return api;
    }

    @Override
    public void onLocationChanged(Location location) {

        if (location != null) {
            if(location.hasAccuracy())
            {
                if(location.getAccuracy()<=accuracy)
                {
                    insertlocation(location);
                }
                else
                {
                    insertError("location LOW accuracy : "+location.getAccuracy()+" by GPS API");
                }
            }
            else
            {
                insertError("location NO accuracy by GPS API");
            }

        }
        else
        {
            insertError("location null by GPS API");
        }

    }

    @Override
    public void onProviderDisabled(String provider) {

        insertError("Provider is Disabled");
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }
    public void showSettingsAlert(){
        Toast.makeText(DistanceCalForegroundService.this,"GPS IS DISABLE",Toast.LENGTH_LONG).show();
    }

    private Notification getMyNotification(String text) {
        // The PendingIntent to launch our activity if the user selects

        // this notification
        Bitmap icon = BitmapFactory.decodeResource(getResources(),
                R.drawable.appiconnew);


        /**
         * This is the method that can be called to update the Notification
         */
        return new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setContentTitle("Location Tracking")
                .setTicker("Location Tracking")
                .setContentText(text)
                .setSmallIcon(R.drawable.appiconnew)
                .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                .setOngoing(true).build();
    }
    private void updateNotification(String text) {

        Notification notification = getMyNotification(text);

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(notiID, notification);
    }

    private boolean isNetAvail(Context context){
        try {
            boolean isConnected,isMobile = false;
            ConnectivityManager cm =
                    (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = null;
            if (cm != null) {
                activeNetwork = cm.getActiveNetworkInfo();
            }else {
                return false;
            }

            isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
            if (activeNetwork != null) {
                isMobile = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE;
            }
            if(isConnected && isMobile){
                return true;
            }
        return false;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public String formatDateTime(java.util.Date date) {
        SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault());
        if (date == null) {
            return "";
        } else {
            return DATE_TIME_FORMAT.format(date);
        }
    }





    private void addToQueue(final GPSPointsDetail jsonStringer,final DatabaseHandler databaseHandler, final String bugPath){
        Log.e(TAG,"................retro...........Trig");
        //final Call<PointsResponse> call = getRetrofit().sendPoints(jsonStringer);
        if(apiInterface != null) {
            final Call<PointsResponse> call = apiInterface.sendPoints(jsonStringer);

            call.enqueue(new Callback<PointsResponse>() {
                @Override
                public void onResponse(Call<PointsResponse> call, Response<PointsResponse> response) {
                    try {
                        if (response != null) {
                            if (response.isSuccessful()) {
                                //long currentId = jsonStringer.getLong("rowid");
                                long currentId = jsonStringer.getRowid();
                                new com.freshorders.freshorder.utils.
                                        Log(bugPath).
                                        ee(TAG, ".........currentId...." + currentId + ".....Success:::");
                                Log.e(TAG, "..........success....." + response.toString());
                                if (response.body().getStatus().equals("true")) {
                                    Log.e(TAG, "..........success.....Data::::" + response.body().getData());
                                    Log.e(TAG, "..........success.....Request::pre_id::" + jsonStringer.getPre_id());
                                    //long currentId = jsonStringer.getLong("rowid");
                                    databaseHandler.setDistancePointSuccess(currentId);
                                    //databaseHandler.deleteSuccessLocationById(jsonStringer.getLong("pre_id"));
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<PointsResponse> call, Throwable throwable) {
                    try {
                        //long currentId = jsonStringer.getLong("rowid");
                        long currentId = jsonStringer.getRowid();
                        new com.freshorders.freshorder.utils.
                                Log(bugPath).
                                ee(TAG, ".........currentId...." + currentId + ".....Reason:::" + throwable.getMessage());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
        }else {
            new com.freshorders.freshorder.utils.Log(bugPath).ee(TAG, "............Retrofit not created.");
        }

    }


    public void sendPointsThroughRetrofit(long rowId,
                                          DatabaseHandler databaseHandler, String bugPath){
        Log.e(TAG,"...........................Trig");

        try{

            if (Constants.USER_ID == null || Constants.USER_ID.isEmpty()) {
                Cursor curs;
                curs = databaseHandler.getDetails();
                if (curs != null && curs.getCount() > 0) {
                    curs.moveToFirst();
                    Constants.USER_ID = curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_id));
                    Log.e("Constants.USER_ID", Constants.USER_ID);
                    curs.close();
                }else {
                    return ;
                }
            }

            Cursor cursor = databaseHandler.getUnSyncLocationsById(rowId);
            if(cursor != null && cursor.getCount() > 0) {
                Log.e(TAG,"...........................dataFound");
                cursor.moveToFirst();
                Location start = new Location("locationA");
                Location end = new Location("locationB");
                double lat = 0, lon = 0, preLat = 0, preLon = 0;
                JSONObject item = new JSONObject();
                item.put("rowid", rowId);
                item.put("batchid", 0);
                item.put("suserid", Constants.USER_ID);
                item.put("logtime", cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_DATE_TIME)));
                lat = cursor.getDouble(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_LAT));
                lon = cursor.getDouble(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_LON));
                start.setLatitude(lat);
                start.setLongitude(lon);
                long preId = cursor.getLong(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_PREVIOUS_ID));
                Cursor curs = databaseHandler.getLocationsForPreById(preId);
                if (curs != null && curs.getCount() > 0) {
                    Log.e(TAG,"...........................previousFound");
                    curs.moveToFirst();
                    preLat = curs.getDouble(curs.getColumnIndex(DatabaseHandler.KEY_LOCATION_LAT));
                    preLon = curs.getDouble(curs.getColumnIndex(DatabaseHandler.KEY_LOCATION_LON));
                    end.setLatitude(preLat);
                    end.setLongitude(preLon);
                    curs.close();
                } else {
                    return;
                }

                double temp_distance = start.distanceTo(end);
                temp_distance = temp_distance / 1000;
                Log.e(TAG, ".................temp_distance::::KM::" + temp_distance);

                item.put("geolat", lat);
                item.put("geolong", lon);
                item.put("pgeolat", preLat);
                item.put("pgeolong", preLon);
                item.put("status", "Active");
                item.put("updateddt", cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_DATE_TIME)));
                item.put("distance", temp_distance);
                item.put("amount", 0);
                item.put("issynced", 1);
                item.put("pre_id", cursor.getLong(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_PREVIOUS_ID)));
                item.put("seqid", cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_UNIQUE_ID)));

                GPSPointsDetail obj = new GPSPointsDetail(rowId,0,Constants.USER_ID,cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_DATE_TIME)),
                        lat,lon,preLat,preLon,"Active",cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_DATE_TIME)),
                        temp_distance,0,1,cursor.getLong(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_PREVIOUS_ID)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_UNIQUE_ID)));

                //addToQueue(item,databaseHandler, bugPath);/////////////////
                addToQueue(obj,databaseHandler, bugPath);/////////////////
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}