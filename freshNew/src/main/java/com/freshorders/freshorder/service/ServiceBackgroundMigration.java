package com.freshorders.freshorder.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.Nullable;
import android.widget.Toast;

import com.freshorders.freshorder.toonline.Migration;
import com.freshorders.freshorder.utils.Log;

public class ServiceBackgroundMigration extends IntentService {

    private String TAG = ServiceBackgroundMigration.class.getSimpleName();
    private Context mContext;

    public ServiceBackgroundMigration() {
        super("ServiceBackgroundMigration");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        mContext = getApplicationContext();
        Toast.makeText(mContext,"Background Migration Starts....",Toast.LENGTH_LONG).show();
        Log.e(TAG, "Background Service Starts................");
        new Migration(mContext).doAllMigration();//////////
    }
}
