package com.freshorders.freshorder.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.Nullable;
import android.util.Log;

import com.freshorders.freshorder.toonline.BendingOrderMigrateToOnline;
import com.freshorders.freshorder.ui.SplashActivity;
import com.freshorders.freshorder.utils.NotificationUtil;


public class BackgroundMigrationService extends IntentService {

    private Context mContext;
    private static final int CHANNEL_ID = 1000;
    private static final int CHANNEL_ID_FAILED = 1001;
    private static final String MIGRATION_TITLE = "Bending Data Moved Status";
    BendingOrderMigrateToOnline obj;

    public BackgroundMigrationService() {
        super("BackgroundMigrationService");
    }



    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        //this.mContext = getApplicationContext();
            Log.e("onHandleIntent",".................Inside");
            obj = new BendingOrderMigrateToOnline(mContext, new BendingOrderMigrateToOnline.ServerMigrationListener() {
            @Override
            public void onMigrationFinished(boolean isSuccess, String statusMSG, boolean isException, String excMSG) {
                Log.e("BackgroundMigration","......................isSuccess" + isSuccess + ":::excMSG::"+excMSG);
                String result = "";
                if(isSuccess){
                    result = "Pending Order Migrated to Server Successfully. To See Result Click me";
                    NotificationUtil notificationUtil = new NotificationUtil(CHANNEL_ID);
                    notificationUtil.showNotification(mContext, SplashActivity.class, MIGRATION_TITLE, result);
                }else if(isException){
                    result = "Pending Order Migration Failed ..." + excMSG ;
                    NotificationUtil notificationUtil = new NotificationUtil(CHANNEL_ID_FAILED);
                    notificationUtil.showNotification(mContext, SplashActivity.class, MIGRATION_TITLE, result);
                }
                if(obj != null && obj.getListener() != null){
                    obj.setListenerNULL();
                }
            }
        });
        obj.migration();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        this.mContext = getApplicationContext();
        return super.onStartCommand(intent, flags, startId);
    }
}


