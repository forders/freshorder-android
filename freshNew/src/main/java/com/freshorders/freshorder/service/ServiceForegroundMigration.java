package com.freshorders.freshorder.service;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.toonline.Migration;
import com.freshorders.freshorder.ui.SplashActivity;
import com.freshorders.freshorder.utils.Log;

import java.util.Objects;

import static com.freshorders.freshorder.utils.Constants.NOTIFICATION_MIG_ID;

public class ServiceForegroundMigration extends IntentService {

    public interface ForegroundMigrationCommunication{
        void fullMigrationComplete();
    }

    private static int FOREGROUND_ID = 5432;
    String NOTIFICATION_CHANNEL_ID = "FreshOrders_channel_id_01";
    private Context mContext;
    private Notification migNotification;

    String bugPath;


    public ServiceForegroundMigration() {
        super("ServiceForegroundMigration");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
            mContext = getApplicationContext();
        bugPath = new PrefManager(mContext).getStringDataByKey(PrefManager.KEY_BUG_FILE_PATH);
        new Log(bugPath).ee("ServiceForeground", "............................onHandleIntent" );
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForeground(FOREGROUND_ID,getOreoNotification()); // new change 28/02/2019
        }else {
            //startForeground(FOREGROUND_ID,getNotification());
            startForeground(FOREGROUND_ID,getOreoNotification()); // new change 28/02/2019
        }

        //new Migration(mContext).doAllMigration();//////////
        new Migration(mContext, migNotification,
                new ForegroundMigrationCommunication() {
                    @Override
                    public void fullMigrationComplete() {
                        stopService();
                    }
                }).doAllMigration();//////////
        // stopForeground(true);
    }

    @Override
    public void onDestroy() {
        // The service is no longer used and is being destroyed
        new Log(bugPath).ee("ServiceForeground", "........onDestroy" );
        ////////////stopForeground(true);
    }

    public void stopService(){
        stopForeground(false);///this is ready to close notification
        NotificationManager notificationManager = (NotificationManager) getApplication().getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.cancel(NOTIFICATION_MIG_ID);
        }
    }

    @SuppressLint("WrongConstant")
    public Notification getOreoNotification()
    {
        String content = "Pending Data Moving To Server";
        final int icon = R.drawable.appiconnew;
        final Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                + "://" + mContext.getPackageName() + "/raw/alarm_buzzer");

        Intent intent = new Intent(this, SplashActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent,0);



        NotificationCompat.Builder foregroundNotification = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        NotificationChannel notificationChannel;
        NotificationManager notificationManager = (NotificationManager) getApplication().getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            AudioAttributes att = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                    .build();

            notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "FreshOrders  Notifications", NotificationManager.IMPORTANCE_HIGH);

            notificationChannel.setDescription("FreshOrders Data Channel");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setSound(alarmSound, att);
            notificationChannel.enableVibration(true);
            //notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            Objects.requireNonNull(notificationManager).createNotificationChannel(notificationChannel);
        }
        foregroundNotification.setOnlyAlertOnce(true);
        foregroundNotification.setOngoing(true);
        /////////////foregroundNotification.setAutoCancel(true); /////// this added for close notification
        foregroundNotification.setContentTitle("MovingOnline....")
                .setContentText(content)
                .setSmallIcon(icon)
                .setSound(alarmSound)
                .setContentIntent(pendingIntent);

        foregroundNotification.setProgress(100, 20, true);

        Notification notification = foregroundNotification.build();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Objects.requireNonNull(notificationManager).notify(NOTIFICATION_MIG_ID , notification);
        }
        migNotification = notification;
        return notification;
    }

    public Notification getNotification()
    {
        String content = "Pending Data Moving To Server";
        final int icon = R.drawable.appiconnew;
        final Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                + "://" + mContext.getPackageName() + "/raw/alarm_buzzer");

        Intent intent = new Intent(this, SplashActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent,0);

        NotificationCompat.Builder foregroundNotification = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);

        //foregroundNotification.setOnlyAlertOnce(true);
        //foregroundNotification.setOngoing(true);
        foregroundNotification.setAutoCancel(false); /////// this added for close notification
        foregroundNotification.setContentTitle("MovingOnline....")
                .setContentText(content)
                .setSmallIcon(icon)
                .setSound(alarmSound)
                .setContentIntent(pendingIntent);

        foregroundNotification.setProgress(100, 20, true);

        //Notification notification = foregroundNotification.build();
        ////////////////foregroundNotification.notify();
        migNotification = foregroundNotification.build();


        return migNotification;
    }
}
