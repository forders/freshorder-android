package com.freshorders.freshorder.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;

import androidx.annotation.Nullable;

import com.freshorders.freshorder.accuracy.location.track.LocationTrackForegroundService;
import com.freshorders.freshorder.activity.DistanceCalActivity;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.utils.Log;

import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import static com.freshorders.freshorder.service.PrefManager.KEY_IS_MY_DAY_PLAN_LEAVE;
import static com.freshorders.freshorder.service.PrefManager.KEY_MY_DAY_PLAN_LEAVE_DATE;
import static com.freshorders.freshorder.utils.Constants.DATE_PATTERN_FOR_APP;
import static com.freshorders.freshorder.utils.Constants.DATE_TIME_SUNDAY;
import static com.freshorders.freshorder.utils.Constants.MENU_DISTANCE_CALCULATION_CODE;
import static com.freshorders.freshorder.utils.Constants.SELF_START_TIME_LIVE_TRACK;
import static com.freshorders.freshorder.utils.Constants.SELF_STOP_TIME_LIVE_TRACK;

public class RunAfterBootService extends IntentService {

    private static final String TAG = RunAfterBootService.class.getSimpleName();

    private Context mContext;

    public RunAfterBootService() {
        super("RunAfterBoot");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        mContext = getApplicationContext();

        new Log(mContext).ee("RunAfterBoot","...........................RunAfterBootService..");

        DateTime dateTime = new DateTime();
        if( !(dateTime.getDayOfWeek() == DATE_TIME_SUNDAY/* Sunday*/) ) {
            if(isTimeLiesBetween(SELF_START_TIME_LIVE_TRACK, SELF_STOP_TIME_LIVE_TRACK)) {
                if (isTrackMenuFound()) {
                    if (PrefManager.getStatus(mContext)) {
                        if(!isMyDayPlanLeave()) {
                            try {
                                PrefManager.setStatus(mContext, true);
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                    mContext.startForegroundService(new Intent(mContext, LocationTrackForegroundService.class));
                                } else {
                                    mContext.startService(new Intent(mContext, LocationTrackForegroundService.class));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                new Log(mContext).ee(TAG, "...Error Produced in Auto Start Tracking........" + e.getMessage());
                            }
                        }else {
                            new Log(mContext).ee(TAG, "...MyDay Plan is Leave Wont Start Tracking........");
                        }
                    } else {
                        new Log(mContext).ee(TAG, "...Already Tracking Not Started No Need To Re-Schedule........");
                    }
                } else {
                    new Log(mContext).ee(TAG, "...Tracking Menu Not Found........");
                }
            }else {
                new Log(mContext).ee(TAG, "...Time not lies between........" + SELF_START_TIME_LIVE_TRACK +"--" + SELF_STOP_TIME_LIVE_TRACK);
            }
        }else {
            new Log(mContext).ee(TAG, "...Today Sunday Not Start Live Track........");
        }
    }

    @Override
    public void onDestroy() {
        // The service is no longer used and is being destroyed
        Log.e("RunAfterBoot", "........onDestroy" );
        ////////////stopForeground(true);
    }

    private boolean isTimeLiesBetween(int startTime , int endTime){

        DateTime dateTime = new DateTime();
        if( !(dateTime.getDayOfWeek() == DATE_TIME_SUNDAY/* Sunday*/) ) {
            DateTime todayStart = dateTime.withTimeAtStartOfDay();
            DateTime start = todayStart.plusHours(startTime);
            DateTime end = todayStart.plusHours(endTime);
            return dateTime.isAfter(start) && dateTime.isBefore(end);
        }else {
            return false;
        }
    }

    private boolean isTrackMenuFound(){
        DatabaseHandler db = new DatabaseHandler(mContext);
        Cursor menuCur = db.getMenuItems();
        if(menuCur != null && menuCur.getCount() > 0){
            menuCur.moveToFirst();
            for(int i = 0; i < menuCur.getCount(); i++) {
                int currentVisibility = menuCur.getInt(menuCur.getColumnIndex(DatabaseHandler.KEY_MENU_IS_VISIBLE));
                String currentMenuCode = menuCur.getString(menuCur.getColumnIndex(DatabaseHandler.KEY_MENU_CODE));
                if (MENU_DISTANCE_CALCULATION_CODE.equalsIgnoreCase(currentMenuCode)) {
                    if (currentVisibility == 1) {
                        return true;
                    }
                }
                menuCur.moveToNext();
            }
        }
        return false;
    }

    private boolean isMyDayPlanLeave(){

        PrefManager pre = new PrefManager(mContext);
        DateTime curDate = new DateTime();
        DateTimeFormatter formatter = DateTimeFormat.forPattern(DATE_PATTERN_FOR_APP);


        if(pre.getBooleanDataByKey(KEY_IS_MY_DAY_PLAN_LEAVE)){
            String myDayPlaneDate = pre.getStringDataByKey(KEY_MY_DAY_PLAN_LEAVE_DATE);
            if(!myDayPlaneDate.isEmpty()) {
                DateTime planDate = formatter.parseDateTime(myDayPlaneDate);
                return DateTimeComparator.getDateOnlyInstance().compare(curDate, planDate) == 0;
            }
        }

        return false;
    }
}
