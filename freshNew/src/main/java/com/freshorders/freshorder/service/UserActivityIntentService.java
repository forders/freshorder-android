package com.freshorders.freshorder.service;

import android.app.IntentService;
import android.content.Intent;

import androidx.annotation.Nullable;

import com.freshorders.freshorder.utils.Log;
import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

import java.util.List;

import static com.freshorders.freshorder.service.PrefManager.KEY_IS_USER_MOVING;
import static com.freshorders.freshorder.utils.Constants.CONFIDENCE;

public class UserActivityIntentService extends IntentService {

    protected static final String TAG = UserActivityIntentService.class.getSimpleName();

    private String bugFilePath ="";
    PrefManager pre ;

    public UserActivityIntentService() {
        super(TAG);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        pre = new PrefManager(getApplicationContext());
        bugFilePath = pre.getStringDataByKey(PrefManager.KEY_BUG_FILE_PATH);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if (ActivityRecognitionResult.hasResult(intent)) {
            //If data is available, then extract the ActivityRecognitionResult from the Intent//
            ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);
            //Get an array of DetectedActivity objects//
            handleDetectedActivities(result.getProbableActivities());
            /*PreferenceManager.getDefaultSharedPreferences(this)
                    .edit()
                    .putString(MainActivity.DETECTED_ACTIVITY,
                            detectedActivitiesToJson(detectedActivities))
                    .apply(); */
        }
    }

    private void handleDetectedActivities(List<DetectedActivity> probableActivities) {

        for(DetectedActivity activity : probableActivities){
            if(activity.getConfidence() > CONFIDENCE){
                switch (activity.getType()) {
                    case DetectedActivity.IN_VEHICLE: {
                        Log.p(bugFilePath, TAG, "User .... IN_VEHICLE: ");
                        pre.setBooleanDataByKey(KEY_IS_USER_MOVING, true);
                        break;
                    }
                    case DetectedActivity.ON_BICYCLE: {
                        Log.p(bugFilePath, TAG, "User .... ON_BICYCLE: ");
                        pre.setBooleanDataByKey(KEY_IS_USER_MOVING, true);
                        break;
                    }
                    case DetectedActivity.ON_FOOT: {
                        Log.p(bugFilePath, TAG, "User .... ON_FOOT: ");
                        pre.setBooleanDataByKey(KEY_IS_USER_MOVING, true);
                        break;
                    }
                    case DetectedActivity.RUNNING: {
                        Log.p(bugFilePath, TAG, "User .... RUNNING: ");
                        pre.setBooleanDataByKey(KEY_IS_USER_MOVING, true);
                        break;
                    }
                    case DetectedActivity.STILL: {
                        Log.p(bugFilePath, TAG, "User .... STILL: ");
                        pre.setBooleanDataByKey(KEY_IS_USER_MOVING, false);
                        break;
                    }
                    case DetectedActivity.TILTING: {
                        Log.p(bugFilePath, TAG, "User .... TILTING: ");
                        pre.setBooleanDataByKey(KEY_IS_USER_MOVING, false);
                        break;
                    }
                    case DetectedActivity.WALKING: {
                        Log.p(bugFilePath, TAG, "User .... WALKING: ");
                        pre.setBooleanDataByKey(KEY_IS_USER_MOVING, true);
                        break;
                    }
                    case DetectedActivity.UNKNOWN: {
                        Log.p(bugFilePath, TAG, "User .... UNKNOWN: ");
                        pre.setBooleanDataByKey(KEY_IS_USER_MOVING, true);
                        break;
                    }
                }
            }else {
                Log.p(bugFilePath, TAG, "Bellow Confidence.... Type: " + activity.getType());
            }
        }
    }
}
