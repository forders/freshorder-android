package com.freshorders.freshorder.service;

import android.app.ActivityManager;
import android.content.Context;

import com.freshorders.freshorder.utils.Log;

public class ServiceClass {

    public static boolean isServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (manager != null) {
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
        }else {
            Log.e("CheckService", "...............service check false");
            return false;
        }
        return false;
    }
}
