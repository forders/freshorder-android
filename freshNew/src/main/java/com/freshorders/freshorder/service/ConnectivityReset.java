package com.freshorders.freshorder.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ConnectivityReset extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // Migration checker reset
        new PrefManager(context).setBooleanDataByKey(PrefManager.KEY_IS_CONNECTIVITY_FIRED, false);
    }
}
