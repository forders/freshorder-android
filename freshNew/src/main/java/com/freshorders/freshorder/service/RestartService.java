package com.freshorders.freshorder.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.freshorders.freshorder.accuracy.location.track.LocationTrackForegroundService;
import com.freshorders.freshorder.utils.Log;

public class RestartService extends BroadcastReceiver {

    private static String TAG = RestartService.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {

        //context.startService(new Intent(context,DistanceCalForegroundService.class));
        if(PrefManager.getStatus(context.getApplicationContext()))
        {
            new Log(context).ee(TAG, "Service Re-Started by BroadCast Receiver");
            context.startService(new Intent(context, LocationTrackForegroundService.class));
        }
        else {
            new Log(context).ee(TAG, "Trig..BroadCast.......but user closed.....");
        }
    }
}