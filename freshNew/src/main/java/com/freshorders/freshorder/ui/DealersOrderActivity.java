package com.freshorders.freshorder.ui;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.DealerOrderListAdapter;
import com.freshorders.freshorder.adapter.MerchantOrderListAdapter;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.AppPaymentHistoryDomain;
import com.freshorders.freshorder.domain.DealerOrderDetailDomain;
import com.freshorders.freshorder.domain.DealerOrderListDomain;
import com.freshorders.freshorder.domain.MerchantOrderListDomain;
import com.freshorders.freshorder.domain.ViewPagerDomain;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class DealersOrderActivity extends Activity {

	TextView menuIcon, textViewAssetMenuProfile, textViewAssetMenuMyOrders, textViewAssetMenuProducts,
			textViewAssetMenuPayment, textViewAssetMenuComplaint,
			textViewAssetMenuSignout,textViewNodata,textViewAssetSearch,textViewHeader
			,textViewAssetMenuExport,textViewCross,textViewAssetClientVisit;
	ListView listViewOrders;
	public static int menuCliccked,selectedPosition;
	LinearLayout linearLayoutMenuParent, linearLayoutProfile,
			linearLayoutMyOrders, linearLayoutProducts,
			linearLayoutPayment, linearLayoutComplaint,linearLayoutClientVisit, linearLayoutSignout,linearlayoutSearchIcon,linearLayoutExport;
	JSONObject JsonAccountObject = null;
	JSONArray JsonAccountArray = null;
	DatabaseHandler databaseHandler;
	JsonServiceHandler JsonServiceHandler ;
	public static ArrayList<DealerOrderListDomain> arraylistDealerOrderList,arraylistSearchResults;
	public static String companyname, fullname, date, address, orderId,PaymentStatus="NULL",moveto="NULL";
	  Date dateStr = null;
	  public static String time,searchclick="0", strMsg = "";;
	  EditText editTextSearchField;
	  public static DealerOrderListDomain domainSelected; 
	  public static DealerOrderListAdapter adapter;
	public static String[] mThumbIds;
	public  static ArrayList<Bitmap> arrayListimagebitmap;
	public  static ArrayList<String> arraylistimagepath;
	public static  ArrayList<ViewPagerDomain> arraylistPdoductImages;
	public static ArrayList<DealerOrderDetailDomain> arraylistDealerOrderDetailList,arraylistimage;
	public static List<String> removedImage;
	public static ArrayList<AppPaymentHistoryDomain> arraypaymenthistory;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(1);
		setContentView(R.layout.dealer_my_order_screen);
		netCheck();
		databaseHandler = new DatabaseHandler(getApplicationContext());
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();
		myOrders();
		Cursor cur;
		cur = databaseHandler.getDetails();
		cur.moveToFirst();
		Log.e("Insertion Check",
				cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_username)));
		Log.e("Insertion Check",
				cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_password)));
		Log.e("Insertion Check",
				cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_usertype)));

		arraypaymenthistory=new ArrayList<AppPaymentHistoryDomain>();
		listViewOrders = (ListView) findViewById(R.id.listViewOrders);
		linearLayoutProfile = (LinearLayout) findViewById(R.id.linearLayoutProfile);
		linearLayoutMyOrders = (LinearLayout) findViewById(R.id.linearLayoutMyOrders);

		linearLayoutProducts = (LinearLayout) findViewById(R.id.linearLayoutProducts);
		linearLayoutPayment = (LinearLayout) findViewById(R.id.linearLayoutPayment);
		linearLayoutComplaint = (LinearLayout) findViewById(R.id.linearLayoutComplaint);
		linearLayoutSignout = (LinearLayout) findViewById(R.id.linearLayoutSignout);
		linearlayoutSearchIcon = (LinearLayout) findViewById(R.id.linearlayoutSearchIcon);
		linearLayoutExport= (LinearLayout) findViewById(R.id.linearLayoutExport);
		linearLayoutClientVisit= (LinearLayout) findViewById(R.id.linearLayoutClientVisit);

		menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
		textViewAssetSearch= (TextView) findViewById(R.id.textViewAssetSearch);
		textViewHeader= (TextView) findViewById(R.id.textViewHeader);
		textViewNodata = (TextView) findViewById(R.id.textViewNodata);
		textViewAssetMenuProfile = (TextView) findViewById(R.id.textViewAssetMenuProfile);
		textViewAssetMenuMyOrders = (TextView) findViewById(R.id.textViewAssetMenuMyOrders);

		textViewAssetMenuProducts = (TextView) findViewById(R.id.textViewAssetMenuProducts);
		textViewAssetMenuPayment = (TextView) findViewById(R.id.textViewAssetMenuPayment);
		textViewAssetMenuComplaint = (TextView) findViewById(R.id.textViewAssetMenuComplaint);
		textViewAssetMenuSignout = (TextView) findViewById(R.id.textViewAssetMenuSignout);
		textViewAssetMenuExport= (TextView) findViewById(R.id.textViewAssetMenuExport);
		textViewAssetClientVisit= (TextView) findViewById(R.id.textViewAssetClientVisit);
		editTextSearchField = (EditText) findViewById(R.id.editTextSearchField);
		textViewCross=(TextView) findViewById(R.id.textViewCross);
		linearLayoutMenuParent = (LinearLayout) findViewById(R.id.linearLayoutMenuParent);
		Typeface font = Typeface.createFromAsset(getAssets(),
				"fontawesome-webfont.ttf");
		menuIcon.setTypeface(font);

		textViewAssetSearch.setTypeface(font);
		textViewAssetMenuProfile.setTypeface(font);
		textViewAssetMenuMyOrders.setTypeface(font);

		textViewAssetMenuProducts.setTypeface(font);
		textViewAssetMenuPayment.setTypeface(font);
		textViewAssetMenuComplaint.setTypeface(font);
		textViewAssetMenuSignout.setTypeface(font);
		textViewAssetMenuExport.setTypeface(font);
		textViewAssetClientVisit.setTypeface(font);
		textViewCross.setTypeface(font);

		arraylistDealerOrderList=new ArrayList<DealerOrderListDomain>();
		arraylistSearchResults=new ArrayList<DealerOrderListDomain>();
		arrayListimagebitmap=new ArrayList<Bitmap>();
		arraylistPdoductImages = new ArrayList<ViewPagerDomain>();
		removedImage =new ArrayList<String>();
		arraylistDealerOrderDetailList=new ArrayList<DealerOrderDetailDomain>();
		arraylistimagepath= new  ArrayList<String>();
		arraylistimage=new ArrayList<DealerOrderDetailDomain>();
		
		linearlayoutSearchIcon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {


				// TODO Auto-generated method stub

				Log.e("size", String.valueOf(arraylistSearchResults.size()));


				if (searchclick == "0") {
					textViewHeader.setVisibility(TextView.GONE);
					editTextSearchField.setVisibility(EditText.VISIBLE);
					editTextSearchField.setFocusable(true);
					editTextSearchField.requestFocus();
					linearLayoutMenuParent.setVisibility(View.GONE);
					textViewCross.setVisibility(TextView.VISIBLE);
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
					Log.e("Keyboard", "Show");
					searchclick = "1";
				} else {
					textViewHeader.setVisibility(TextView.VISIBLE);
					editTextSearchField.setVisibility(EditText.GONE);
					textViewCross.setVisibility(TextView.GONE);

					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(linearlayoutSearchIcon.getWindowToken(), 0);
					searchclick = "0";
					}
				}
		});
		
		editTextSearchField.addTextChangedListener(new TextWatcher() {

			public void afterTextChanged(Editable s) {
				arraylistDealerOrderList = new ArrayList<DealerOrderListDomain>();
				String searchText = editTextSearchField.getText().toString().toLowerCase(Locale.getDefault());
				for (DealerOrderListDomain pd : arraylistSearchResults) {
					if (pd.getComName().toLowerCase(Locale.getDefault()).contains(searchText)) {
						arraylistDealerOrderList.add(pd);
					}
				}
				DealersOrderActivity.this.runOnUiThread(new Runnable() {

					@Override
					public void run() {
						adapter = new DealerOrderListAdapter(DealersOrderActivity.this, R.layout.item_dealer_order_screen, arraylistDealerOrderList);
						listViewOrders.setVisibility(View.VISIBLE);
						listViewOrders.setAdapter(adapter);
					}
				});

			}

			public void beforeTextChanged(CharSequence s, int start,
										  int count, int after) {
			}

			public void onTextChanged(CharSequence s, int start,
									  int before, int count) {
				textViewCross.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						editTextSearchField.setText("");
						if (editTextSearchField.equals("")) {
							InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
							imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
						}
					}
				});
				  /* String searchId=  editTextSearchField.getText().toString();
				   if(searchId.equals("")||searchId.equals(null)){
					   Log.e("searchId", searchId);
					   myOrders();
				   }else{
					   Log.e("searchId", searchId);
				 for(int i=0;i<arraylistDealerOrderList.size();i++){
					 if(arraylistDealerOrderList.get(i).getComName().toLowerCase(Locale.getDefault()).contains(searchId.toLowerCase(Locale.getDefault()))){
						 
							DealerOrderListDomain dod1=new DealerOrderListDomain();
							dod1.setOrderid(arraylistDealerOrderList.get(i).getOrderid());
							dod1.setFullname( arraylistDealerOrderList.get(i).getFullname());
							dod1.setComName(arraylistDealerOrderList.get(i).getComName());
							dod1.setDate(arraylistDealerOrderList.get(i).getDate());
							dod1.setAddress(arraylistDealerOrderList.get(i).getAddress());
							dod1.setMuserid(arraylistDealerOrderList.get(i).getMuserid());
							arraylistDealerOrderList.clear();
							arraylistDealerOrderList.add(dod1);
							
							DealerOrderListAdapter adapter=new DealerOrderListAdapter(DealersOrderActivity.this, R.layout.item_dealer_order_screen, arraylistDealerOrderList);
							listViewOrders.setVisibility(View.VISIBLE);
							listViewOrders.setAdapter(adapter);
							textViewNodata.setVisibility(View.GONE);
					 }else{
						listViewOrders.setVisibility(View.GONE);
							textViewNodata.setVisibility(View.VISIBLE);
							textViewNodata.setText("DATA NOT FOUND");
							
						
						}
				 }
				   }
			     */
			}
		});

		menuIcon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (menuCliccked == 0) {
					linearLayoutMenuParent.setVisibility(View.VISIBLE);
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(menuIcon.getWindowToken(), 0);
					editTextSearchField.setVisibility(EditText.GONE);
					textViewHeader.setVisibility(TextView.VISIBLE);
					textViewCross.setVisibility(TextView.GONE);
					menuCliccked = 1;
				} else {
					linearLayoutMenuParent.setVisibility(View.GONE);
					textViewHeader.setVisibility(TextView.VISIBLE);
					editTextSearchField.setVisibility(EditText.GONE);
					textViewCross.setVisibility(TextView.GONE);
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(menuIcon.getWindowToken(), 0);
					menuCliccked = 0;
				}

			}
		});

		
		listViewOrders.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
									int position, long id) {
				// TODO Auto-generated method stub
				domainSelected = arraylistDealerOrderList.get(position);
				companyname = arraylistDealerOrderList.get(position).getFullname();
				fullname = arraylistDealerOrderList.get(position).getComName();
				date = arraylistDealerOrderList.get(position).getDate();
				address = arraylistDealerOrderList.get(position).getAddress();
				orderId = arraylistDealerOrderList.get(position).getOrderid();
				//Log.e("position", String.valueOf(position));

				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(listViewOrders.getWindowToken(), 0);
				Intent to = new Intent(DealersOrderActivity.this, DealerOrderDetailsActivity.class);
				startActivity(to);
				finish();
				/*selectedPosition = position;
				listViewOrders.setAdapter(adapter);
				listViewOrders.setSelectionFromTop(position, 1);*/
			}
		});


		linearLayoutMenuParent.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
			}
		});

		linearLayoutProfile.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				if(netCheckwithoutalert()==true){
					getDetails("profile");
				}
				else{
					showAlertDialogToast("Please Check Your internet connection");
				}

			}
		});

		linearLayoutExport.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				Constants.filter="0";
				if(netCheckwithoutalert()==true){
					getDetails("export");
				}
				else{
					showAlertDialogToast("Please Check Your internet connection");
				}

			}
		});


		linearLayoutMyOrders.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				// TODO Auto-generated method stub

			}
		});

		linearLayoutProducts.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				/*moveto="product";
				getDetails();*/
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				if(netCheckwithoutalert()==true){
					getDetails("product");
				}
				else{
					showAlertDialogToast("Please Check Your internet connection");
				}
			}
		});
		linearLayoutClientVisit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				//if(netCheckwithoutalert()==true){
					getDetails("clientvisit");
				//}
				//else{
				//	showAlertDialogToast("Please Check Your internet connection");
				//}
			}
		});



		linearLayoutPayment.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				if(netCheckwithoutalert()==true){
					Intent io = new Intent(DealersOrderActivity.this,
							DealerPaymentActivity.class);
					startActivity(io);
					finish();
				}
				else{
					showAlertDialogToast("Please Check Your internet connection");
				}

			}
		});

		linearLayoutComplaint.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				//if(netCheckwithoutalert()==true){
					getDetails("postnotes");
				//}
				//else{
				//	showAlertDialogToast("Please Check Your internet connection");
				//}
			}
		});

		linearLayoutSignout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				// TODO Auto-generated method stub
				databaseHandler.delete();
				Intent io = new Intent(DealersOrderActivity.this,
						SigninActivity.class);
				startActivity(io);
				finish();
			}
		});
	}
	private void getDetails(final String paymentcheck) {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog= ProgressDialog.show(DealersOrderActivity.this, "",
						"Loading...", true, true);

			}

			@Override
			protected void onPostExecute(Void result) {

				try {

					PaymentStatus=(JsonAccountObject.getString("pymtstatus"));
					Log.e("PaymentStatus",PaymentStatus);


					if(PaymentStatus.equals("Pending")||PaymentStatus.equals(null) ||PaymentStatus.equals("")||PaymentStatus.equals("null")){
						showAlertDialogToast("Please make payment before place order");
					}else{
						Log.e("paymentcheck", paymentcheck);

						if(paymentcheck.equals("profile")){
							Intent io = new Intent(DealersOrderActivity.this,
									ProfileActivity.class);
							startActivity(io);
							finish();

						}else if(paymentcheck.equals("product")){
							Intent io = new Intent(DealersOrderActivity.this,
									ProductActivity.class);
							io.putExtra("Key","menuclick");
							startActivity(io);
							finish();

						}else if(paymentcheck.equals("postnotes")){
							Intent io = new Intent(DealersOrderActivity.this,
									MerchantComplaintActivity.class);
							startActivity(io);
							finish();

						}else if(paymentcheck.equals("export")){
							Intent to = new Intent(DealersOrderActivity.this,
									ExportActivity.class);
							startActivity(to);
							finish();

						}else if(paymentcheck.equals("clientvisit")){

								Intent io = new Intent(DealersOrderActivity.this,
										DealerClientVisit.class);
								startActivity(io);
								finish();

						}


					}

					dialog.dismiss();


				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
				}

			}

			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();


				JsonServiceHandler = new JsonServiceHandler(Utils.strGetProfileDetail+Constants.USER_ID, DealersOrderActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceDataGet();
				return null;
			}
		}.execute(new Void[]{});
	}

	private void myOrders() {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
		

			@Override
			protected void onPreExecute() {
				dialog= ProgressDialog.show(DealersOrderActivity.this, "",
						"Loading...", true, true);

			}

			@Override
			protected void onPostExecute(Void result) {

				try {

					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);

					if (strStatus.equals("true")) {
						
					
						
						JSONArray job1 = JsonAccountObject.getJSONArray("data");

						for (int i = 0; i < job1.length(); i++) {
							JSONObject job = new JSONObject();
							job=job1.getJSONObject(i);
							DealerOrderListDomain dod=new DealerOrderListDomain();
							dod.setOrderid(job.getString("ordid"));
							dod.setMuserid(job.getString("muserid"));
							dod.setFullname(job.getString("mname"));
							dod.setComName(job.getString("mcompanyname"));
							dod.setisread(job.getString("isread"));



							dod.setserialnonew(String.valueOf(i + 1));

							time=job.getString("orderdt");

							String inputPattern = "yyyy-MM-dd";
							    String outputPattern = "dd-MMM-yyyy";
							    SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
							    SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

							    String str = null;

							    try {
							    	dateStr = inputFormat.parse(time);
							        str = outputFormat.format(dateStr);
							        dod.setDate(str);
							        //Log.e("str", str);
							    } catch (ParseException e) {
							        e.printStackTrace();
							    }
							dod.setAddress(job.getString("ordid")+",");
							arraylistDealerOrderList.add(dod);
							arraylistSearchResults.add(dod);


						}

						setadap();
					}else{
						listViewOrders.setVisibility(View.GONE);
						textViewNodata.setVisibility(View.VISIBLE);
						textViewNodata.setText(strMsg);
						linearlayoutSearchIcon.setClickable(false);
						textViewAssetSearch.setVisibility(TextView.VISIBLE);
					}
					dialog.dismiss();
				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					//Log.e("ProductActivityException", e.toString());
				}
				
			}

			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();
				try {
					jsonObject.put("duserid",Constants.USER_ID);
					jsonObject.put("usertype",Constants.USER_TYPE);
				} catch (JSONException e) {
					e.printStackTrace();
				}


				JsonServiceHandler = new JsonServiceHandler(Utils.strMerchantOrderList,jsonObject.toString(), DealersOrderActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceData();
				return null;
			}
		}.execute(new Void[] {});
	}
	
	public  void setadap() {
		// TODO Auto-generated method stub
		
			 adapter=new DealerOrderListAdapter(DealersOrderActivity.this, R.layout.item_dealer_order_screen, arraylistDealerOrderList);
			listViewOrders.setVisibility(View.VISIBLE);
			listViewOrders.setAdapter(adapter);
			textViewNodata.setVisibility(View.GONE);
		
		
	}
	protected void toastDisplay(String msg) {
		Toast toast = Toast.makeText(DealersOrderActivity.this, msg,
				Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

	}
	public boolean netCheckwithoutalert() {
		// for network connection
		try {
			ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetwork = connectionManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			Object result = null;
			if (mWifi.isConnected() || mNetwork.isConnected()) {
				return true;
			}

			else if (result == null) {
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	public boolean netCheck() {
		// for network connection
		try {
			ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetwork = connectionManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			Object result = null;
			if (mWifi.isConnected() || mNetwork.isConnected()) {
				return true;
			}

			else if (result == null) {
				showAlertDialog(DealersOrderActivity.this,
						"No Internet Connection",
						"Please Check Your internet connection.", false);
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	public void showAlertDialog(Context context, String title, String message,
			Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
					}
				});
		alertDialog.show();
	}

	public void showAlertDialogToast( String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();

		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

	}
	public void onBackPressed() {
		exitAlret();
	}

	private void exitAlret() {
		AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
		localBuilder.setCancelable(false);
		localBuilder.setMessage("Do you want to Exit?");
		localBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {

						finish();

					}
				});
		localBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {
						paramDialogInterface.cancel();
					}
				});
		localBuilder.create().show();
	}
}
