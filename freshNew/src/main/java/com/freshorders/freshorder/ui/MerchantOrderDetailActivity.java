package com.freshorders.freshorder.ui;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.DealerOrderDetailAdapter;
import com.freshorders.freshorder.adapter.MerchantOrderDetailAdapter;
import com.freshorders.freshorder.adapter.MerchantOrderListAdapter;
import com.freshorders.freshorder.domain.DealerOrderDetailDomain;
import com.freshorders.freshorder.domain.MerchantOrderDetailDomain;
import com.freshorders.freshorder.domain.MerchantOrderDomainSelected;
import com.freshorders.freshorder.domain.MerchantOrderListDomain;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.MediaRouteButton;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MerchantOrderDetailActivity extends Activity {
	TextView menuIcon,textViewNodata;
	JSONObject JsonAccountObject = null;
	JSONArray JsonAccountArray = null;
	JsonServiceHandler JsonServiceHandler;
	public static ArrayList<MerchantOrderDetailDomain> arraylistMerchantOrderDetailList,arrayListSearchResults;
	public static ArrayList<MerchantOrderDomainSelected> arraylistMerchantOrderDetailListSelected,arraylisttemp,arralistUpdateQty;
	ListView listViewOrders;
	LinearLayout linearLayoutBack,linearlayoutSearchIcon,LinearLayoutMenu;
	Button buttonAddPlaceOrder;
	public static EditText editTextSearchField;
	public static TextView textViewHeader,textViewAssetSearch,textViewCross;
	public static String searchclick="0", strMsg = "";
	public static MerchantOrderDetailAdapter adapter;
	int index=1;
	int size=0;
	int j,k = 0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(1);
		setContentView(R.layout.merchant_my_order_detail);
		netCheck();
		menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
		listViewOrders = (ListView) findViewById(R.id.listViewOrders);
		//linearLayoutBack = (LinearLayout) findViewById(R.id.linearLayoutBack);
		LinearLayoutMenu = (LinearLayout) findViewById(R.id.LinearLayoutMenu);
		buttonAddPlaceOrder = (Button) findViewById(R.id.buttonAddPlaceOrder);
		linearlayoutSearchIcon = (LinearLayout) findViewById(R.id.linearlayoutSearchIcon);
		textViewHeader= (TextView) findViewById(R.id.textViewHeader);
		textViewAssetSearch= (TextView) findViewById(R.id.textViewAssetSearch);
		editTextSearchField = (EditText) findViewById(R.id.editTextSearchField);
		textViewNodata = (TextView) findViewById(R.id.textViewNodata);
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();
		textViewCross=(TextView) findViewById(R.id.textViewCross);

		Typeface font = Typeface.createFromAsset(getAssets(),
				"fontawesome-webfont.ttf");
		menuIcon.setTypeface(font);
		textViewAssetSearch.setTypeface(font);
		textViewCross.setTypeface(font);




		arralistUpdateQty=new ArrayList<MerchantOrderDomainSelected>();
		arraylisttemp=new ArrayList<MerchantOrderDomainSelected>();
		/*linearLayoutBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent to = new Intent(MerchantOrderDetailActivity.this, MerchantOrderActivity.class);
				startActivity(to);
			finish();
			}
		});*/
		if(Constants.checkproduct==0){
			myOrders();
		}else{
			setadap();
		}


		LinearLayoutMenu.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(LinearLayoutMenu.getWindowToken(), 0);
				Constants.adapterset=0;
				if(Constants.orderid!=""){
					Intent to = new Intent(MerchantOrderDetailActivity.this, MerchantSingleDetailActivity.class);
					startActivity(to);
					finish();
				}else{
					Intent to = new Intent(MerchantOrderDetailActivity.this, MerchantOrderActivity.class);
					startActivity(to);
					finish();
				}}
		});

		linearlayoutSearchIcon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(searchclick=="0"){
					textViewHeader.setVisibility(TextView.GONE);
					editTextSearchField.setVisibility(EditText.VISIBLE);
					editTextSearchField.setFocusable(true);
					editTextSearchField.requestFocus();
					textViewCross.setVisibility(TextView.VISIBLE);

					InputMethodManager iss = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					iss.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
					Log.e("Keyboard", "Show");
					searchclick="1";
				}else{
					textViewHeader.setVisibility(TextView.VISIBLE);
					editTextSearchField.setVisibility(EditText.GONE);
					textViewCross.setVisibility(TextView.GONE);
					InputMethodManager iss= (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					iss.hideSoftInputFromWindow(linearlayoutSearchIcon.getWindowToken(), 0);
					searchclick="0";
				}

			}
		});

		editTextSearchField.addTextChangedListener(new TextWatcher() {

			public void afterTextChanged(Editable s) {
				arraylistMerchantOrderDetailList = new ArrayList<MerchantOrderDetailDomain>();
				String searchText = editTextSearchField.getText().toString().toLowerCase(Locale.getDefault());
				for (MerchantOrderDetailDomain pd : arrayListSearchResults) {
					if (pd.getprodname().toLowerCase(Locale.getDefault()).contains(searchText) || pd.getprodcode().toLowerCase(Locale.getDefault()).contains(searchText)) {
						arraylistMerchantOrderDetailList.add(pd);
					}
				}
				MerchantOrderDetailActivity.this.runOnUiThread(new Runnable() {

					@Override
					public void run() {
						adapter = new MerchantOrderDetailAdapter(
								MerchantOrderDetailActivity.this,
								R.layout.item_order_details_new,
								arraylistMerchantOrderDetailList, netCheck());
						listViewOrders.setAdapter(adapter);
					}
				});
			}

			public void beforeTextChanged(CharSequence s, int start,
										  int count, int after) {
			}

			public void onTextChanged(CharSequence s, int start,
									  int before, int count) {
				textViewCross.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						editTextSearchField.setText("");
						if (editTextSearchField.equals("")) {
							InputMethodManager iss = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
							iss.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
						}
					}
				});

			}
		});

		buttonAddPlaceOrder.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Constants.checkproduct =1;
				size=MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.size();
				Log.e("size", String.valueOf(size));
				if(size==0){
					showAlertDialogToast("Atleast select one product");
					//toastDisplay("No data found");
				}else {
					Intent to = new Intent(MerchantOrderDetailActivity.this,
							MerchantOrderCheckoutActivity.class);
					startActivity(to);
					finish();
				}
			}
		});
	}

	private void myOrders() {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";


			@Override
			protected void onPreExecute() {
				dialog = ProgressDialog.show(MerchantOrderDetailActivity.this,
						"", "Loading...", true, true);

			}

			@Override
			protected void onPostExecute(Void result) {

				try {

					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);

					if (strStatus.equals("true")) {

						JSONArray jsonArr = JsonAccountObject
								.getJSONArray("data");
						arraylistMerchantOrderDetailListSelected=new ArrayList<MerchantOrderDomainSelected>();
						arraylistMerchantOrderDetailList = new ArrayList<MerchantOrderDetailDomain>();
						arrayListSearchResults = new ArrayList<MerchantOrderDetailDomain>();
						JSONObject job = new JSONObject();
						for (int i = 0; i < jsonArr.length(); i++) {

							job = jsonArr.getJSONObject(i);

							JSONArray jsonArr2 = job.getJSONArray("products");
							JSONObject job1;
							for (j = 0; j < jsonArr2.length(); j++) {
								try{
									MerchantOrderDetailDomain dod = new MerchantOrderDetailDomain();

									job1 = new JSONObject();
									job1 = jsonArr2.getJSONObject(j);
									Log.e("job1", String.valueOf(job1));
									dod.setDuserid(job.getJSONObject("D").getString("userid"));
									dod.setCompanyname(job.getJSONObject("D").getString("companyname"));
									dod.setusrdlrid(job1.getString("userid"));
									dod.setprodcode(job1.getString("prodcode"));
									dod.setprodid(job1.getString("prodid"));
									dod.setUOM(job1.getString("uom"));

									String[] date1 = job.getString("updateddt")
											.split("T");
									dod.setDate(date1[0]);
									dod.setqty("");
									dod.setDeliverymode("");
									dod.setDeliverydate("");
									dod.setDeliverytime("");
									dod.setFreeQty("");
									dod.setSelectedUOM(job1.getString("uom"));
									dod.setselectedfreeUOM(job1.getString("uom"));
									dod.setserialno(String.valueOf(j + k + 1));
									dod.setprodname(job1.getString("prodname"));
									dod.setprodtax("0");
									dod.setprodprice("0");
									dod.setUnitGram("");
									dod.setUnitPerUom("");
									dod.setSelecetdQtyUomUnit("");
									dod.setSelecetdFreeQtyUomUnit("");
									dod.setDeliverymode("");
									dod.setCurrStock("");
									arraylistMerchantOrderDetailList.add(dod);

									arrayListSearchResults.add(dod);
								} catch (JSONException e) {
									e.printStackTrace();
								}
							}
							Log.e("j value", String.valueOf(j));
							k=j;
							setadap();

						}
					}else{
						//toastDisplay(strMsg);
						listViewOrders.setVisibility(View.GONE);
						textViewNodata.setVisibility(View.VISIBLE);
						textViewNodata.setText("No Products");
						linearlayoutSearchIcon.setClickable(false);
						textViewAssetSearch.setVisibility(TextView.VISIBLE);
						buttonAddPlaceOrder.setVisibility(View.GONE);
					}

					dialog.dismiss();
				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					//Log.e("ProductActivityException", e.toString());
				}

			}

			@Override
			protected Void doInBackground(Void... params) {


				JSONObject jsonObject = new JSONObject();

				if(Constants.dealerid.equals("")){
					JsonServiceHandler = new JsonServiceHandler(
							Utils.strmerchantOderdetail+Constants.USER_ID+"&prodstatus=Active",
							MerchantOrderDetailActivity.this);
				}else{
					JsonServiceHandler = new JsonServiceHandler(
							Utils.strmerchantOderdetail+Constants.USER_ID +"&prodstatus=Active&filter[where][duserid]="+Constants.dealerid,
							MerchantOrderDetailActivity.this);
				}

				JsonAccountObject = JsonServiceHandler.ServiceDataGet();
				return null;
			}

		}.execute(new Void[] {});
	}

	public  void setadap() {
		// TODO Auto-generated method stub


		adapter = new MerchantOrderDetailAdapter(
				MerchantOrderDetailActivity.this,
				R.layout.item_order_details_new,
				arraylistMerchantOrderDetailList, netCheck());
		listViewOrders.setAdapter(adapter);


	}

	protected void toastDisplay(String msg) {
		Toast toast = Toast.makeText(MerchantOrderDetailActivity.this, msg,
				Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

	}

	public boolean netCheck() {
		// for network connection
		try {
			ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetwork = connectionManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			Object result = null;
			if (mWifi.isConnected() || mNetwork.isConnected()) {
				return true;
			}

			else if (result == null) {
				showAlertDialog(MerchantOrderDetailActivity.this,
						"No Internet Connection",
						"Please Check Your internet connection.", false);
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	public void showAlertDialog(Context context, String title, String message,
								Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
					}
				});
		alertDialog.show();
	}
	public void onBackPressed() {
		exitAlret();
	}

	private void exitAlret() {
		AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
		localBuilder.setCancelable(false);

		localBuilder.setMessage("Do you want to Exit?");
		localBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {

						finish();

					}
				});
		localBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {
						paramDialogInterface.cancel();
					}
				});
		localBuilder.create().show();
	}

	public void showAlertDialogToast( String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();

		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

	}
}
