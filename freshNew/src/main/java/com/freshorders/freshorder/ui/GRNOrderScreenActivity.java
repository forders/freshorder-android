package com.freshorders.freshorder.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.GRNOrderAdapter;
import com.freshorders.freshorder.aync.IUserDetailCallBack;
import com.freshorders.freshorder.aync.UserDetailTask;
import com.freshorders.freshorder.model.GRN;
import com.freshorders.freshorder.model.OrderDetail;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

public class GRNOrderScreenActivity extends Activity implements IUserDetailCallBack {

    private ListView grnOrderListview;
    public static final String GRN_LIST = "grnlist";
    public static final String GRN_CLICKED_POS = "grnclickedpos";
    private int clickedPos;
    private ArrayList<GRN> grnArrayList;
    private ProgressDialog mProgressDialog;
    private ArrayList<OrderDetail> orderDetails;
    private GRN grn;
    private static final String TAG = GRNOrderScreenActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.grn_order_screen_activity);
        getIntentExtra(getIntent().getExtras());
        TextView orderNo = (TextView)findViewById(R.id.orderNoTxtView);
        TextView orderDt = (TextView) findViewById(R.id.orderDtTxtView);
        TextView orderStatus = (TextView)findViewById(R.id.orderstatusTxtView);
        Button save = (Button)findViewById(R.id.saveBtn);
        Constants.startTime  = new SimpleDateFormat(
                "HH:mm:ss", Locale.ENGLISH).format(new java.util.Date());
        if(grn != null){
            orderNo.setText(grn.getOrdid());
            orderDt.setText(grn.getOrderdt());
            orderStatus.setText(grn.getOrdstatus());
        }
        grnOrderListview = (ListView) findViewById(R.id.grnOrderListView);
        final GRNOrderAdapter grnOrderAdapter = new GRNOrderAdapter(GRNOrderScreenActivity.this,
                R.layout.grn_order_list_view, orderDetails);
        grnOrderListview.setAdapter(grnOrderAdapter);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<OrderDetail> details = grnOrderAdapter.getUpdatedList();
                if(details == null)
                    return;
                show();
                final UserDetailTask userDetail = new UserDetailTask(Utils.strGrnstocks,
                        getGrnRequest(details).toString(), GRNOrderScreenActivity.this, GRNOrderScreenActivity.this);
                userDetail.execute();
                Log.d("postData", ""+ getGrnRequest(details).toString());
            }
        });
    }

    private void getIntentExtra(final Bundle bundle) {
        if (bundle != null) {
            clickedPos = bundle.getInt(GRN_CLICKED_POS);
            grnArrayList = bundle.getParcelableArrayList(GRN_LIST);
            if(grnArrayList != null && grnArrayList.size()>0){
                grn = grnArrayList.get(clickedPos);
                orderDetails = grnArrayList.get(clickedPos).getOrderDetailList();
            }
        }
    }

    private void show() {
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(GRNOrderScreenActivity.this, "",
                    "Sending Data wait ...", true, true);
        }
    }

    private void dismissDialog() {
        if (mProgressDialog != null &&
                mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private JSONObject getGrnRequest(final ArrayList<OrderDetail> list){
        JSONObject reqObj = null;
        try {
            if(list != null && list.size() > 0){
                reqObj = new JSONObject();
                final JSONArray ordDtlArray = new JSONArray();
                reqObj.put("suserid", grn.getSuserid());
                reqObj.put("distributorid", grn.getDistributorid());
                reqObj.put("starttime", Constants.startTime);
                reqObj.put("endime", new SimpleDateFormat(
                        "HH:mm:ss", Locale.ENGLISH).format(new java.util.Date()));
                reqObj.put("updateddt", new SimpleDateFormat(
                        "yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).format(new java.util.Date()));
                for (int i = 0; i < list.size(); i++){
                    final JSONObject object = new JSONObject();
                    OrderDetail orderDetail = list.get(i);
                    if(!TextUtils.isEmpty(orderDetail.getReceivedQty())){
                        object.put("orddtlid", orderDetail.getOrddtlid());
                        object.put("receivedqty", orderDetail.getReceivedQty());
                        object.put("prodid", orderDetail.getProdid());
                        object.put("uomid", orderDetail.getOrd_uom());
                        ordDtlArray.put(object);
                    }
                }
                reqObj.put("grndetails", ordDtlArray);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return reqObj;
    }

    @Override
    public void userResponse(JSONObject jsonObject) {
        dismissDialog();

        if (jsonObject == null) {
            showAlert("Something Went Wrong");
            return;
        }
        Log.d(TAG, "grnResponse in ......" + jsonObject.toString());
        if (jsonObject.optString("status").equalsIgnoreCase("true")) {
            final JSONArray dataArr = jsonObject.optJSONArray("data");
            final String msg = jsonObject.optString("message");
            showAlert(msg);
        } else if (jsonObject.optString("status").equalsIgnoreCase("false")) {
            final String msg = jsonObject.optString("message");
            showAlert(msg);
        }
    }

    public void showAlert( String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        GRNOrderScreenActivity.this.finish();
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();

        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }
}
