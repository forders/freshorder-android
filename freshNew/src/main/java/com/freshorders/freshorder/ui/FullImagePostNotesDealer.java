package com.freshorders.freshorder.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.Utils;
import com.squareup.picasso.Picasso;

import java.io.IOException;

/**
 * Created by ragul on 03/10/16.
 */
public class FullImagePostNotesDealer extends Activity {
    ImageView imagepostnote;
    Bitmap bitmap;
    LinearLayout linearLayoutBack;
    TextView textViewAssetMenu,textviewdeletemenu;
    String path,url;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.full_image_post_note);
        imagepostnote = (ImageView) findViewById(R.id.imagepostnote);

        linearLayoutBack = (LinearLayout) findViewById(R.id.linearLayoutBack);
        textViewAssetMenu = (TextView) findViewById(R.id.textViewAssetMenu);
        textviewdeletemenu = (TextView) findViewById(R.id.textviewdeletemenu);
        Typeface font = Typeface.createFromAsset(getAssets(),
                "fontawesome-webfont.ttf");
        textViewAssetMenu.setTypeface(font);
        textviewdeletemenu.setTypeface(font);
        Intent io = getIntent();
        url = io.getExtras().getString("url");


        Log.e("image url", url);

        textviewdeletemenu.setVisibility(TextView.GONE);
        Picasso.with(FullImagePostNotesDealer.this) //Context
                .load(url) //URL/FILE
                .fit()
                .placeholder(
                        R.drawable.loader)
                .centerCrop().into(imagepostnote);


        linearLayoutBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent to = new Intent(FullImagePostNotesDealer.this, ComplaintsDeleteActivity.class);

                startActivity(to);
                FullImagePostNotesDealer.this.finish();

            }
        });
    }

    public void showAlertDialogToast( String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent io=new Intent(FullImagePostNotesDealer.this,DealersComplaintActivity.class);
                startActivity(io);
                finish();
            }
        });
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    public void onBackPressed() {
        exitAlret();
    }

    private void exitAlret() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        localBuilder.setMessage("Do you want to Exit?");
        localBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {

                        finish();

                    }
                });
        localBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {
                        paramDialogInterface.cancel();
                    }
                });
        localBuilder.create().show();
    }
}
