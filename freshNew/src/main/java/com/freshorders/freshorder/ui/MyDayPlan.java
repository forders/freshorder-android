package com.freshorders.freshorder.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.accuracy.location.track.LocationTrackForegroundService;
import com.freshorders.freshorder.activity.DistanceCalActivity;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.receiver.StartTrackingBroadCast;
import com.freshorders.freshorder.receiver.StartTrackingBroadCastImmediate;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.toonline.migration.PendingDataMigrationActivity;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.GPSTrackerActivity;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.ServiceClass;
import com.freshorders.freshorder.utils.Utils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import java.util.Map;

import static com.freshorders.freshorder.service.PrefManager.KEY_IS_MY_DAY_PLAN_LEAVE;
import static com.freshorders.freshorder.service.PrefManager.KEY_MY_DAY_PLAN_LEAVE_DATE;
import static com.freshorders.freshorder.utils.Constants.BR_REQUEST_CODE_START_LIVE_TRACK;
import static com.freshorders.freshorder.utils.Constants.BR_REQUEST_CODE_START_LIVE_TRACK_By_My_DAY_PLAN;
import static com.freshorders.freshorder.utils.Constants.DATE_PATTERN_FOR_APP;
import static com.freshorders.freshorder.utils.Constants.MY_DAY_PLAN_LEAVE;

public class MyDayPlan extends Activity {
    private static String TAG = MyDayPlan.class.getSimpleName();

    private Location mLastLocation;
    private FusedLocationProviderClient mFusedLocationClient;
    private ProgressDialog pDialog;
    private long locationUpdateInterval = 1000;
    private static int gpsCounter = 0;


    LocationManager locationManager;
    boolean gps_enabled = false;
    boolean network_enabled = false;
    boolean passive_enabled = false;

    double myDayGeolat, myDayGeolon;

    TextView menuIcon, textViewAssetMenuProfile, textViewAssetMenuMyOrders,
            textViewAssetMenuMyDealers, textViewAssetMenuProducts,
            textViewAssetMenuPayment, textViewAssetMenuComplaint,textViewAssetMenuDistanceCalculation,
            textViewAssetMenuSignout,textViewAssetMenuCreateOrder,textViewAssetMenuExport,
            textViewAssetMenuClientVisit,textViewAssetMenuPaymentCollection,textViewAssetMenuRefresh,
            textViewAssetMenuStockEntry,textViewAssetMenuAddMerchant,textViewAssetMenuAcknowledge
            ,textViewAssetMenusurveyuser,textViewAssetMenudownloadscheme,textViewAssetMenuMyDayPlan,textViewAssetMenuMySales,
            textViewAssetMenuDistStock, textViewAssetMenuStockOnly, textViewAssetMenuMigration, textViewAssetMenuStockAudit;



    public int q= 0;
    int menuCliccked;

    LinearLayout linearLayoutMenuParent,linearLayoutProfile, linearLayoutMyOrders,
            linearLayoutMyDealers, linearLayoutProducts, linearLayoutPayment,
            linearLayoutComplaint, linearLayoutSignout,linearLayoutEdit,linearLayoutDetails,
            linearLayoutCreateOrder,linearLayoutExport,linearLayoutClientVisit,linearLayoutPaymentCollection,
            linearlayoutoverallayout,linearLayoutRefresh,linearLayoutDistanceCalculation,
            linearLayoutAcknowledge,linearLayoutStockEntry,linearLayoutAddMerchant
            ,linearLayoutsurveyuser,linearLayoutdownloadscheme,linearlayoutSearchIcon,linearLayoutMyDayPlan,linearlayoutroute
            ,linearlayoutdistributor,linearlayoutremarks,linearlayoutMySales,linearLayoutDistStock,
            linearLayoutStockOnly, linearLayoutMigration,
            linearLayoutStockAudit;

    private void showMenu(){

        MyApplication app = MyApplication.getInstance();

        if(!app.isCreateOrder()){
            linearLayoutCreateOrder.setVisibility(View.GONE);
        }
        if(!app.isProfile()){
            linearLayoutProfile.setVisibility(View.GONE);
        }
        if(!app.isMyDayPlan()){
            linearLayoutMyDayPlan.setVisibility(View.GONE);
        }
        if(!app.isMyOrders()){
            linearLayoutMyOrders.setVisibility(View.GONE);
        }
        if(!app.isAddMerchant()){
            linearLayoutAddMerchant.setVisibility(View.GONE);
        }
        if(!app.isMySales()){
            //linearLayoutMySales.setVisibility(View.GONE);
        }
        if(!app.isPostNotes()){
            linearLayoutComplaint.setVisibility(View.GONE);
        }
        if(!app.isMyClientVisit()){
            linearLayoutClientVisit.setVisibility(View.GONE);
        }
        if(!app.isAcknowledge()){
            linearLayoutAcknowledge.setVisibility(View.GONE);
        }
        if(!app.isPaymentCollection()){
            linearLayoutPaymentCollection.setVisibility(View.GONE);
        }
        if(!app.isPkdDataCapture()){
            linearLayoutStockEntry.setVisibility(View.GONE);
        }
        if(!app.isDistributorStock()){
            linearLayoutDistStock.setVisibility(View.GONE);
        }
        if(!app.isSurveyUser()){
            linearLayoutsurveyuser.setVisibility(View.GONE);
        }
        if(!app.isDownLoadScheme()){
            linearLayoutdownloadscheme.setVisibility(View.GONE);
        }
        if(!app.isDistanceCalculation()){
            linearLayoutDistanceCalculation.setVisibility(View.GONE);
        }
        if(!app.isRefresh()){
            linearLayoutRefresh.setVisibility(View.GONE);
        }
        if(!app.isLogout()){
            linearLayoutSignout.setVisibility(View.GONE);
        }
        if(!app.isClosingStock()){
            linearLayoutStockOnly.setVisibility(View.GONE);
        }
        if(!app.isClosingStockAudit()){
            linearLayoutStockAudit.setVisibility(View.GONE);
        }
        if(!app.isPendingData()){
            linearLayoutMigration.setVisibility(View.GONE);
        }
    }

    String moveto="NULL",PaymentStatus="NULL";
    JsonServiceHandler JsonServiceHandler ;
    JSONObject JsonAccountObject = null;

    KProgressHUD loader;
    EditText edtHQuat;

    String selectRoute = "";
    String selectDistributors = "";
    String selectWorkType = "";
   public static String selectremarks = "", userBeatModel="";

    String userid = "",SelectedDistId="",selectedbeatid="",selected_dealer="",currentdt="",worktype="",beatname="",distributorname="",remarks="",updateddt="";
    ;

    CardView cardview;
    DatabaseHandler databaseHandler;
    View btnSubmit;

    Button btnMakePlan, btnEditPlan;
    boolean isMyDayPlanPlaced = false;
    private String strId = "";

    @Override
    protected void onStart() {
        super.onStart();

        if (userid == null || userid.isEmpty()) {
            if (databaseHandler == null) {
                databaseHandler = new DatabaseHandler(getApplicationContext());
            }
            Cursor curs;
            curs = databaseHandler.getDetails();
            if (curs != null && curs.getCount() > 0) {
                curs.moveToFirst();
                userid = curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_id));
                if(Constants.USER_ID == null){
                    Constants.USER_ID = userid;
                }
                Log.e("MYDAY ", userid);
                curs.close();
            } else {
                showAlertDialogToast(getResources().getString(R.string.dealer_not_proper_load));
            }
        }
        if(selected_dealer == null || selected_dealer.isEmpty()) {
            Cursor cursor;
            cursor = databaseHandler.getdealer();

            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                selected_dealer = cursor.getString(cursor
                        .getColumnIndex(DatabaseHandler.KEY_Duserid));
                cursor.close();
            }else {
                showAlertDialogToast(getResources().getString(R.string.dealer_not_proper_load));
            }
        }

        if(userBeatModel == null || userBeatModel.isEmpty()) {
            Cursor cur1;
            cur1 = databaseHandler.getUserSetting();
            if (cur1 != null && cur1.getCount() > 0) {
                cur1.moveToFirst();
                userBeatModel = cur1.getString(cur1.getColumnIndex(DatabaseHandler.KEY_settingrefvalue));
                Log.e("userBeatModel", userBeatModel);
                cur1.close();
            }else {
                showAlertDialogToast(getResources().getString(R.string.dealer_not_proper_load));
            }
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);

        setContentView(R.layout.mydayplan);

        int permissionCheck1 = ContextCompat.checkSelfPermission(MyDayPlan.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionCheck1 != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    MyDayPlan.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.LOCATION_PERMISSION_REQUEST_CODE);
        } else {
            try {
                if (CheckGpsStatus()) {
                    startLocationUpdates();
                } else {
                    showSettingsAlert();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        loader = KProgressHUD.create(MyDayPlan.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setWindowColor(getResources().getColor(R.color.colorPrimaryDark))
                .setLabel("Please wait..")
                .setCancellable(false)
                .setAnimationSpeed(1)
                .setDimAmount(0.6f);

        btnMakePlan = (Button) findViewById(R.id.id_day_plan_make);
        btnMakePlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isMyDayPlanPlaced){
                    showAlertMSG("Plan Already Placed Use Edit Operation");
                }
            }
        });
        btnEditPlan = (Button) findViewById(R.id.id_day_plan_edit);
        btnEditPlan.setClickable(false);
        btnEditPlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isMyDayPlanPlaced){
                    layWorkType.setClickable(true);
                    linearlayoutremarks.setClickable(true);
                    layRoutes.setClickable(true);
                    layDistributors.setClickable(true);
                    edtHQuat.setEnabled(true);
                    edtHQuat.setFocusable(true);
                    edtHQuat.setCursorVisible(true);
                    edtHQuat.setFocusableInTouchMode(true);
                    cardview.setVisibility(View.VISIBLE);
                    gotoMain();////
                }else {
                    showAlertMSG("You Haven't Plan For Today Please Make Plan First");
                }
            }
        });

        edtHQuat = (EditText) findViewById(R.id.edtHQuat);
        edtHQuat.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // For whatever reason we need to request a soft keyboard.
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if(hasFocus)
                    if (imm != null) {
                        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                    }
                Log.v("DialogProblem", "Focus requested, " + (hasFocus?"has focus.":"doesn't have focus."));
            }
        });
        edtHQuat.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    hideSoftKeyboard();
                    edtHQuat.clearFocus();
                    edtHQuat.setCursorVisible(false);
                    Log.e(TAG, "....keyboard Hide");
                    return true;
                } else {
                    return false;
                }
            }
        });
        btnSubmit = findViewById(R.id.btnSubmit);
        cardview= (CardView) findViewById(R.id.cardview);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideSoftKeyboard();///////////////////////////////////////////
                if(myDayGeolat > 0 && myDayGeolon > 0) {
                    if (userBeatModel.equals("D")) {
                        CheckDistributorData();
                    } else {
                        checkData();
                    }
                }else {
                    locationUpdateWithDelay();
                }

            }
        });

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                hideSoftKeyboard();
                edtHQuat.clearFocus();
                edtHQuat.setCursorVisible(false);

            }
        }, 500);

        databaseHandler = new DatabaseHandler(getApplicationContext());
        JsonAccountObject = new JSONObject();
        Cursor cur;
        cur = databaseHandler.getDetails();
        cur.moveToFirst();
        userid =  cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_id));

        Cursor cursor;
        cursor = databaseHandler.getdealer();

        if(cursor.getCount()>0){
            cursor.moveToFirst();
            selected_dealer = cursor.getString(cursor
                    .getColumnIndex(DatabaseHandler.KEY_Duserid));
        }

        Cursor curPay;
        curPay = databaseHandler.getDetails();
        if(curPay != null && curPay.getCount() > 0){
            curPay.moveToFirst();
            if(isInternetOn()){
                PaymentStatus = curPay.getString(curPay.getColumnIndex(DatabaseHandler.KEY_paymentStatus));
            }
            curPay.close();
        }

        String date = "yyyy-MM-dd";

        Calendar c = Calendar.getInstance();
        System.out.println("Current time =&gt; "+c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        updateddt = df.format(c.getTime());
        SimpleDateFormat inputFormat = new SimpleDateFormat(date);
        currentdt =inputFormat.format(new Date());

        txtWorkType = (TextView) findViewById(R.id.txtWorkType);
        layWorkType = findViewById(R.id.layWorkType);

        txtDistributors = (TextView) findViewById(R.id.txtDistributors);
        layDistributors = findViewById(R.id.layDistributors);

        txtRoutes = (TextView) findViewById(R.id.txtRoutes);
        layRoutes = findViewById(R.id.layRoutes);

        getdaydetail();


        linearlayoutSearchIcon = (LinearLayout) findViewById(R.id.linearlayoutSearchIcon);
        linearlayoutoverallayout= (LinearLayout) findViewById(R.id.linearlayoutoverallayout);
        linearLayoutCreateOrder = (LinearLayout) findViewById(R.id.linearLayoutCreateOrder);
        linearLayoutProfile = (LinearLayout) findViewById(R.id.linearLayoutProfile);
        linearLayoutMyOrders = (LinearLayout) findViewById(R.id.linearLayoutMyOrders);
        linearLayoutMyDealers = (LinearLayout) findViewById(R.id.linearLayoutMyDealers);
        linearLayoutRefresh= (LinearLayout) findViewById(R.id.linearLayoutRefresh);
        textViewAssetMenuRefresh = (TextView) findViewById(R.id.textViewAssetMenuRefresh);
        linearLayoutPayment = (LinearLayout) findViewById(R.id.linearLayoutPayment);
        linearLayoutComplaint = (LinearLayout) findViewById(R.id.linearLayoutComplaint);
        linearLayoutSignout = (LinearLayout) findViewById(R.id.linearLayoutSignout);

        linearLayoutClientVisit= (LinearLayout) findViewById(R.id.linearLayoutClientVisit);
        linearLayoutPaymentCollection= (LinearLayout) findViewById(R.id.linearLayoutPaymentCollection);

        linearLayoutAcknowledge= (LinearLayout) findViewById(R.id.linearLayoutAcknowledge);
        linearLayoutStockEntry= (LinearLayout) findViewById(R.id.linearLayoutStockEntry);
        linearLayoutAddMerchant = (LinearLayout) findViewById(R.id.linearLayoutAddMerchant);

        linearLayoutdownloadscheme= (LinearLayout) findViewById(R.id.linearLayoutdownloadscheme);
        linearLayoutsurveyuser= (LinearLayout) findViewById(R.id.linearLayoutsurveyuser);
        linearLayoutMyDayPlan = (LinearLayout) findViewById(R.id.linearLayoutMyDayPlan);
        linearlayoutMySales = (LinearLayout) findViewById(R.id.linearLayoutMySales);
        linearLayoutDistanceCalculation= (LinearLayout) findViewById(R.id.linearLayoutDistanceCalculation);
        linearlayoutdistributor= (LinearLayout) findViewById(R.id.linearlayoutdistributor);
        linearlayoutroute= (LinearLayout) findViewById(R.id.linearlayoutroute);
        linearlayoutremarks= (LinearLayout) findViewById(R.id.linearlayoutremarks);
        linearLayoutMyDealers.setVisibility(View.GONE);

        menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
        textViewAssetMenuCreateOrder= (TextView) findViewById(R.id.textViewAssetMenuCreateOrder);
        textViewAssetMenuProfile = (TextView) findViewById(R.id.textViewAssetMenuProfile);
        textViewAssetMenuMyOrders = (TextView) findViewById(R.id.textViewAssetMenuMyOrders);
        textViewAssetMenuMyDealers = (TextView) findViewById(R.id.textViewAssetMenuMyDealers);

        textViewAssetMenuPayment = (TextView) findViewById(R.id.textViewAssetMenuPayment);
        textViewAssetMenuComplaint = (TextView) findViewById(R.id.textViewAssetMenuComplaint);
        textViewAssetMenuSignout = (TextView) findViewById(R.id.textViewAssetMenuSignout);
        textViewAssetMenuDistanceCalculation = (TextView) findViewById(R.id.textViewAssetMenuDistanceCalculation);
        textViewAssetMenuClientVisit= (TextView) findViewById(R.id.textViewAssetMenuClientVisit);
        textViewAssetMenuPaymentCollection= (TextView) findViewById(R.id.textViewAssetMenuPaymentCollection);

        textViewAssetMenuAddMerchant = (TextView) findViewById(R.id.textViewAssetMenuAddMerchant);
        textViewAssetMenuAcknowledge = (TextView) findViewById(R.id.textViewAssetMenuAcknowledge);
        textViewAssetMenuStockEntry = (TextView) findViewById(R.id.textViewAssetMenuStockEntry);

        textViewAssetMenudownloadscheme= (TextView) findViewById(R.id.textViewAssetMenudownloadscheme);
        textViewAssetMenusurveyuser= (TextView) findViewById(R.id.textViewAssetMenusurveyuser);
        textViewAssetMenuMyDayPlan= (TextView) findViewById(R.id.textViewAssetMenuMyDayPlan);
        textViewAssetMenuMySales= (TextView) findViewById(R.id.textViewAssetMenuMySales);

        linearLayoutMenuParent = (LinearLayout) findViewById(R.id.linearLayoutMenuParent);

        Typeface font = Typeface.createFromAsset(getAssets(),
                "fontawesome-webfont.ttf");

        menuIcon.setTypeface(font);
        textViewAssetMenuCreateOrder.setTypeface(font);
        textViewAssetMenuProfile.setTypeface(font);
        textViewAssetMenuMyOrders.setTypeface(font);
        textViewAssetMenuMyDealers.setTypeface(font);
        textViewAssetMenuRefresh.setTypeface(font);
        textViewAssetMenuPayment.setTypeface(font);
        textViewAssetMenuComplaint.setTypeface(font);
        textViewAssetMenuSignout.setTypeface(font);
        textViewAssetMenuDistanceCalculation.setTypeface(font);
        textViewAssetMenuClientVisit.setTypeface(font);
        textViewAssetMenuPaymentCollection.setTypeface(font);

        textViewAssetMenuAcknowledge.setTypeface(font);
        textViewAssetMenuStockEntry.setTypeface(font);
        textViewAssetMenuAddMerchant.setTypeface(font);

        textViewAssetMenusurveyuser.setTypeface(font);
        textViewAssetMenudownloadscheme.setTypeface(font);
        textViewAssetMenuMyDayPlan.setTypeface(font);
        textViewAssetMenuMySales.setTypeface(font);

        linearLayoutDistStock = (LinearLayout) findViewById(R.id.linearLayoutDistStock);
        textViewAssetMenuDistStock = (TextView) findViewById(R.id.textViewAssetMenuDistStock);
        textViewAssetMenuDistStock.setTypeface(font);

        textViewAssetMenuStockOnly = (TextView) findViewById(R.id.textViewAssetMenuStockOnly);
        textViewAssetMenuStockAudit = (TextView) findViewById(R.id.textViewAssetMenuStockAudit);
        textViewAssetMenuMigration = findViewById(R.id.textViewAssetMenuMigration);

        textViewAssetMenuStockOnly.setTypeface(font);
        textViewAssetMenuStockAudit.setTypeface(font);
        textViewAssetMenuMigration.setTypeface(font);

        linearLayoutStockOnly = (LinearLayout) findViewById(R.id.linearLayoutStockOnly);
        linearLayoutStockAudit = findViewById(R.id.linearLayoutStockAudit);
        linearLayoutMigration = (LinearLayout) findViewById(R.id.linearLayoutMigration);

        Cursor cur1;
        cur1 = databaseHandler.getUserSetting();
        cur1.moveToFirst();
         userBeatModel = cur1.getString(cur1.getColumnIndex(DatabaseHandler.KEY_settingrefvalue));
        Log.e("userBeatModel",userBeatModel);

        if(userBeatModel.equalsIgnoreCase("D")){
            linearlayoutdistributor.setVisibility(View.GONE);
            linearlayoutroute.setVisibility(View.GONE);
        }

        menuIcon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (menuCliccked == 0) {

                    linearLayoutMenuParent.setVisibility(View.VISIBLE);
                    menuCliccked = 1;
                } else {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                }


            }
        });

        linearLayoutMenuParent.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
            }
        });

        linearLayoutMyDayPlan.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
            }
        });

        linearLayoutProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto="profile";
                if( isInternetOn()){
                    getData();
                }else{
                    //showAlertDialogToast("Please Check Your internet connection");
                    toastDisplay("Please Check Your internet connection");
                }

            }
        });

        linearLayoutRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("Clicked", "Clicked");

                if(isInternetOn()){

                    final Dialog qtyDialog = new Dialog(MyDayPlan.this);
                    qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    qtyDialog.setContentView(R.layout.warning_dialog);

                    final Button buttonUpdateOk, buttonUpdateCancel;

                    final TextView textQtyValidate;

                    buttonUpdateOk = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateOk);
                    buttonUpdateCancel = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateCancel);

                    textQtyValidate = (TextView) qtyDialog
                            .findViewById(R.id.textQtyValidate);
                    qtyDialog.show();


                    buttonUpdateOk.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            databaseHandler.synTableDelete();
                            Constants.refreshscreen = "mydayplan";
                            Intent io = new Intent(MyDayPlan.this, RefreshActivity.class);
                            startActivity(io);
                            finish();
                            qtyDialog.dismiss();
                        }

                    });

                    buttonUpdateCancel
                            .setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub

                                    qtyDialog.dismiss();

                                }
                            });

                }else{
                   // showAlertDialogToast("Please Check Your internet connection");
                    toastDisplay("Please Check Your internet connection");
                }
            }

        });

        linearLayoutMyOrders.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Constants.orderstatus="Success";
                Constants.checkproduct = 0;
                linearLayoutMenuParent.setVisibility(View.GONE);
                moveto="myorder";
                if( isInternetOn()){
                    getData();
                }else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {

                        Intent io = new Intent(MyDayPlan.this,
                                SalesManOrderActivity.class);
                        io.putExtra("Key","menuclick");
                        Constants.setimage = null;
                        startActivity(io);
                        finish();

                        /*if (Constants.USER_TYPE.equals("D")) {
                            Intent io = new Intent(MyDayPlan.this,
                                    DealersOrderActivity.class);
                            io.putExtra("Key","menuclick");
                            startActivity(io);
                            finish();
                        } else if (Constants.USER_TYPE.equals("M")) {
                            Intent io = new Intent(MyDayPlan.this,
                                    MerchantOrderActivity.class);
                            io.putExtra("Key","menuclick");
                            Constants.setimage = null;
                            startActivity(io);
                            finish();
                        } else {
                            Intent io = new Intent(MyDayPlan.this,
                                    SalesManOrderActivity.class);
                            io.putExtra("Key","menuclick");
                            Constants.setimage = null;
                            startActivity(io);
                            finish();
                        } */
                    }
                }


            }
        });


        linearLayoutMyDealers.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                moveto="Mydealer";
                if( isInternetOn()){
                    getData();
                }else{
                    //showAlertDialogToast("Please Check Your internet connection");
                    toastDisplay("Please Check Your internet connection");
                }



            }
        });


        linearLayoutCreateOrder.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                MyApplication.getInstance().setTemplate8ForTemplate2(false);

                linearLayoutMenuParent.setVisibility(View.GONE);
                moveto="createorder";
                Constants.checkproduct=0;
                Constants.orderid="";
                if( isInternetOn()) {
                    getData();
                }else{
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment");
                    }else {
                        Intent io = new Intent(MyDayPlan.this, CreateOrderActivity.class);
                        startActivity(io);
                        finish();
                    }
                }


            }
        });


        linearLayoutComplaint.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto="postnotes";
                if( isInternetOn()){
                    getData();
                }else{
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment");
                    }else {

                        Intent io = new Intent(MyDayPlan.this,
                                MerchantComplaintActivity.class);
                        startActivity(io);
                        finish();
                        /*if (Constants.USER_TYPE.equals("D")) {
                            Intent io = new Intent(MyDayPlan.this,
                                    DealersComplaintActivity.class);
                            startActivity(io);
                            finish();

                        } else {
                            Intent io = new Intent(MyDayPlan.this,
                                    MerchantComplaintActivity.class);
                            startActivity(io);
                            finish();

                        }  */
                    }
                }
            }
        });

        linearLayoutClientVisit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto="clientvisit";
                if( isInternetOn()){
                    getData();
                }else{
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment");
                    }else {
                        Intent io = new Intent(MyDayPlan.this, SMClientVisitHistory.class);
                        startActivity(io);
                        finish();
                        /*if (Constants.USER_TYPE.equals("D")) {
                            Intent io = new Intent(MyDayPlan.this,
                                    DealerClientVisit.class);
                            startActivity(io);
                            finish();
                        } else {
                            Intent io = new Intent(MyDayPlan.this,
                                    SMClientVisitHistory.class);
                            startActivity(io);
                            finish();
                        }  */
                    }
                }
            }
        });
        linearLayoutPaymentCollection.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto="paymentcoll";
                if( isInternetOn()){
                    getData();
                }else{
                   // showAlertDialogToast("Please Check Your internet connection");
                    toastDisplay("Please Check Your internet connection");
                }

            }
        });


        linearLayoutStockEntry.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;

                if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                    showAlertDialogToast("Please make payment");
                } else {
                    Intent io = new Intent(MyDayPlan.this,
                            OutletStockEntry.class);
                    startActivity(io);
                    finish();
                }


            }
        });
        linearLayoutDistanceCalculation.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                Intent io = new Intent(MyDayPlan.this,
                        DistanceCalActivity.class);
                startActivity(io);
                finish();

            }
        });
        linearLayoutAcknowledge.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto = "acknowledge";
                if( isInternetOn()){
                    getData();
                }else{

                    //showAlertDialogToast("Please Check Your internet connection");
                    toastDisplay("Please Check Your internet connection");
                }

            }
        });

        linearLayoutAddMerchant.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto = "addmerchant";
                if( isInternetOn()){
                    getData();
                }else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent to = new Intent(MyDayPlan.this,
                                AddMerchantNew.class);
                        startActivity(to);
                        finish();
                    }
                }
            }
        });

        linearLayoutdownloadscheme.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(isInternetOn()){

                    final Dialog qtyDialog = new Dialog(MyDayPlan.this);
                    qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    qtyDialog.setContentView(R.layout.warning_dialog);

                    final Button buttonUpdateOk, buttonUpdateCancel;

                    final TextView textQtyValidate;

                    buttonUpdateOk = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateOk);
                    buttonUpdateCancel = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateCancel);

                    textQtyValidate = (TextView) qtyDialog
                            .findViewById(R.id.textQtyValidate);
                    qtyDialog.show();


                    buttonUpdateOk.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            databaseHandler.deletescheme();
                            Constants.downloadScheme = "mydayplan";
                            Intent io = new Intent(MyDayPlan.this, SchemeDownload.class);
                            startActivity(io);
                            finish();
                            qtyDialog.dismiss();
                        }

                    });

                    buttonUpdateCancel
                            .setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub

                                    qtyDialog.dismiss();

                                }
                            });

                }else{
                  //  showAlertDialogToast("Please Check Your internet connection");
                    toastDisplay("Please Check Your internet connection");
                }

            }
        });

        linearLayoutDistStock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto = "diststock";
                if (isInternetOn()) {
                    getData();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment");
                    } else {
                        Intent io = new Intent(MyDayPlan.this,
                                DistributrStockActivity.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });

        linearLayoutsurveyuser.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto = "surveyuser";
                if (isInternetOn()) {
                    getData();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent to = new Intent(MyDayPlan.this, SurveyMerchant.class);
                        startActivity(to);
                        finish();
                    }
                }

            }
        });

        linearlayoutMySales.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (isInternetOn()) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    moveto="mysales";
                    getData();
                }else {
                    toastDisplay("Please Check Your internet connection");
                }
            }
        });

        linearLayoutSignout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                // TODO Auto-generated method stub
                databaseHandler.delete();
                Intent io = new Intent(MyDayPlan.this, SigninActivity.class);
                startActivity(io);
                finish();
            }
        });

        linearLayoutMigration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Intent io = new Intent(MyDayPlan.this,
                        PendingDataMigrationActivity.class);
                startActivity(io);
            }
        });

        linearLayoutStockOnly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Constants.checkproduct = 0;
                Constants.orderid = "";
                Constants.SalesMerchant_Id = "";
                Constants.Merchantname = "";
                MyApplication.getInstance().setTemplate8ForTemplate2(true);
                if (isInternetOn()) {
                    Intent io = new Intent(MyDayPlan.this,
                            ClosingStockDashBoardActivity.class);
                    startActivity(io);
                    finish();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals("") || PaymentStatus.equalsIgnoreCase("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(MyDayPlan.this,
                                ClosingStockDashBoardActivity.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });

        linearLayoutStockAudit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().setTemplate8ForTemplate2(false);
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Constants.checkproduct = 0;
                Constants.orderid = "";
                Constants.SalesMerchant_Id = "";
                Constants.Merchantname = "";
                if (isInternetOn()) {
                    Intent io = new Intent(MyDayPlan.this,
                            ClosingStockAudit.class);
                    startActivity(io);
                    finish();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals("") || PaymentStatus.equalsIgnoreCase("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(MyDayPlan.this,
                                ClosingStockAudit.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });

        /////////////////
        showMenu();
        ///////////////

    }

    void gotoMain() {
        initView();
    }

    TextView txtWorkType, txtDistributors, txtRoutes;
    View layWorkType, layDistributors, layRoutes;
    final int ACTION_FOR_WORKTYPE = 1;
    final int ACTION_FOR_DISTRIBUTORS = 2;
    final int ACTION_FOR_ROUTES = 3;

    boolean WORK = false;
    boolean DISTRIBUTORS = false;
    boolean ROUTES = false;

    void initView() {

        selectRoute = "";
        selectDistributors = "";
        selectWorkType = "";
        selectremarks = "";

        itemRealList.clear();
        itemTempList.clear();


        layWorkType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WORK = true;
                DISTRIBUTORS = false;
                ROUTES = false;
                performAction(ACTION_FOR_WORKTYPE);
            }
        });
        layDistributors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WORK = false;
                DISTRIBUTORS = true;
                ROUTES = false;
                performAction(ACTION_FOR_DISTRIBUTORS);
            }
        });
        layRoutes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WORK = false;
                DISTRIBUTORS = false;
                ROUTES = true;
                performAction(ACTION_FOR_ROUTES);
            }
        });


    }

    String dialogtitle = "";

    void performAction(int Action) {
        hideSoftKeyboard();//////////////////////////////////////////////////
        itemRealList.clear();
        itemTempList.clear();
        switch (Action) {
            case ACTION_FOR_WORKTYPE:
                dialogtitle = "Select Work Type";
                if (isInternetOn()) {
                    getworktype();

                } else {
                    //showAlertDialogToast("No Internet Connection !!");
                    toastDisplay("Please Check Your internet connection");
                }

                break;
            case ACTION_FOR_DISTRIBUTORS:
                dialogtitle = "Select DISTRIBUTORS";
                if (isInternetOn()) {
                    getDistributors();
                } else {
                   // showAlertDialogToast("No Internet Connection !!");
                    toastDisplay("Please Check Your internet connection");

                }

                break;
            case ACTION_FOR_ROUTES:
                dialogtitle = "Select ROUTES";
                if (isInternetOn()) {
                    getBeat(selectDistributors);

                } else {
                   // showAlertDialogToast("No Internet Connection !!");
                    toastDisplay("Please Check Your internet connection");
                }
                break;
        }


    }

    AlertDialog selectingDialog = null;
    ListAdaptor mAdapterWork = null;
    private List<String> itemTempList = new ArrayList<>();
    private List<String> itemRealList = new ArrayList<>();
    RecyclerView recycler_view;
    View txtNoData;

    void selecting_dialog() {
        Log.e("adapter","adapter");
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
// ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.select_dialog, null);
        dialogBuilder.setView(dialogView);

        TextView txtTitle = (TextView) dialogView.findViewById(R.id.txtTitle);
        txtTitle.setText(dialogtitle);

        txtNoData = dialogView.findViewById(R.id.txtNoData);
        View btnBack = dialogView.findViewById(R.id.btnBack);
        txtNoData.setVisibility(View.GONE);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectingDialog.dismiss();
                hideSoftKeyboard();
            }
        });

        EditText edtWorkType = (EditText) dialogView.findViewById(R.id.edtWorkType);
        edtWorkType.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                filterWorkTypeList(editable.toString().toLowerCase());
            }
        });


        recycler_view = (RecyclerView) dialogView.findViewById(R.id.recycler_view);
        mAdapterWork = new ListAdaptor(itemTempList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        recycler_view.setAdapter(mAdapterWork);


        selectingDialog = dialogBuilder.create();
        selectingDialog.show();

    }

    void filterWorkTypeList(String input) {
        itemTempList.clear();
        int sizeOFList = itemRealList.size();
        for (int i = 0; i < sizeOFList; i++) {
            if (itemRealList.get(i).toLowerCase().contains(input)) {
                itemTempList.add(itemRealList.get(i));
            }
        }
        if (itemTempList.size() > 0) {
            recycler_view.setVisibility(View.VISIBLE);
            txtNoData.setVisibility(View.GONE);
        } else {
            recycler_view.setVisibility(View.GONE);
            txtNoData.setVisibility(View.VISIBLE);
        }
        mAdapterWork.notifyDataSetChanged();


    }


    public boolean isInternetOn() {
        ConnectivityManager connectivity = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;
    }

    void getdaydetail() {

        loader.show();
        RequestQueue queue = Volley.newRequestQueue(MyDayPlan.this);
        String url=Utils.strmydaydetail+selected_dealer+"&filter[where][suserid]="+Constants.USER_ID+"&filter[where][orderplndt]="+currentdt;

        Log.e("url",url);
        StringRequest stringRequest = new StringRequest(com.android.volley.Request.Method.GET, url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject responseObj = new JSONObject(response);
                            String status = responseObj.getString("status");

                            if(status.equals("true")){
                                JSONArray data = responseObj.getJSONArray("data");
                                Log.e(TAG, "****" + data.toString());
                                for(int i=0;i<data.length();i++){

                                    worktype= data.getJSONObject(0).getString("worktype");

                                    remarks= data.getJSONObject(0).getString("remarks");
                                    if (worktype.equalsIgnoreCase("Leave") || worktype.equalsIgnoreCase("Meeting")
                                            || worktype.equalsIgnoreCase("No Field Work") || worktype.equalsIgnoreCase("Holiday")) {
                                        linearlayoutroute.setVisibility(View.GONE);
                                        linearlayoutdistributor.setVisibility(View.GONE);
                                        //Kumaravel
                                        selectRoute = "";
                                        selectDistributors = "";
                                        txtDistributors.setText(""); txtRoutes.setText("");
                                    }else{
                                        if (userBeatModel.equals("C")){
                                            distributorname=  data.getJSONObject(0).getJSONObject("distributor").getString("fullname");
                                            beatname=  data.getJSONObject(0).getString("beatnm");
                                        }

                                    }

                                    if(data.getJSONObject(0).has("planid")){
                                        strId = data.getJSONObject(0).getString("planid");
                                    }
                                }

                                isMyDayPlanPlaced = true;
                                btnEditPlan.setClickable(true);

                                txtWorkType.setText(worktype);
                                layWorkType.setClickable(false);
                                linearlayoutremarks.setClickable(false);
                                edtHQuat.setEnabled(false);
                                edtHQuat.setFocusable(false);
                                edtHQuat.setText(remarks);
                                txtDistributors.setText(distributorname);
                                layRoutes.setClickable(false);
                                txtRoutes.setText(beatname);
                                layDistributors.setClickable(false);

                                cardview.setVisibility(View.GONE);
                            }else{
                                gotoMain();
                            }

                        } catch (Exception e) {
                        }
                        loader.dismiss();

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
               // showAlertDialogToast("Network Error");

                loader.dismiss();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                // the POST parameters:


                return params;
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(stringRequest);
    }

    void getworktype() {

        loader.show();
        RequestQueue queue = Volley.newRequestQueue(MyDayPlan.this);
        JSONObject jsonobj=new JSONObject();
        // Request a string response
        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST, Utils.strworktype,jsonobj,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            JSONArray data = response.getJSONArray("data");
                            int length = data.length();
                            for (int i = 0; i < length; i++) {
                                String worktype = data.getJSONObject(i).getString("type");
                                itemRealList.add(worktype);
                                itemTempList.add(worktype);
                            }
                            selecting_dialog();
                        } catch (Exception e) {
                            //showAlertDialogToast("Failed to get routes");
                           // Toast.makeText(MyDayPlan.this, "Failed to get routes.", Toast.LENGTH_SHORT).show();
                        }
                        loader.dismiss();

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
               // Toast.makeText(MyDayPlan.this, "Network Error", Toast.LENGTH_SHORT).show();
                loader.dismiss();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                // the POST parameters:


                return params;
            }
        };

// Add the request to the queue

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(stringRequest);

    }


    void getDistributors(){

        ////Kumaravel 01-08-2019
        if(isMyDayPlanPlaced){
            txtRoutes.setText("");
            beatname = "";
        }

        Cursor curs;
        curs = databaseHandler.getDistributor();
        Log.e("inside", "inside");
        itemRealList.clear();
        itemTempList.clear();
        if(curs!=null && curs.getCount() > 0) {
            if (curs.moveToFirst()) {

                do{
                    itemRealList.add(curs.getString(0));
                    itemTempList.add(curs.getString(0));
                } while (curs.moveToNext());
            }
        }
        selecting_dialog();
    } /*{

        loader.show();
        String completeUrl = "http://52.66.29.53:3000/api/distributors";
        RequestQueue queue = Volley.newRequestQueue(MyDayPlan.this);

        // Request a string response
        StringRequest stringRequest = new StringRequest(com.android.volley.Request.Method.GET, completeUrl,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            itemRealList.clear();
                            itemTempList.clear();
                            JSONObject responseObj = new JSONObject(response);
                            JSONObject data = responseObj.getJSONObject("data");
                            JSONArray distributors = data.getJSONArray("distributors");
                            int length = distributors.length();
                            for (int i = 0; i < length; i++) {
                                String distributor = distributors.getString(i);
                                itemRealList.add(distributor);
                                itemTempList.add(distributor);
                            }
                            selecting_dialog();
                        } catch (Exception e) {
                            Toast.makeText(MyDayPlan.this, "Failed to get routes.", Toast.LENGTH_SHORT).show();
                        }
                        loader.dismiss();

                    }
                }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error handling
                Toast.makeText(MyDayPlan.this, "Network Error", Toast.LENGTH_SHORT).show();
                loader.dismiss();

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                // the POST parameters:


                return params;
            }
        };

// Add the request to the queue

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(stringRequest);

    }*/

    private void getDistributorIdForEdit(){
        Cursor cursdistname;
        databaseHandler=new DatabaseHandler(getApplicationContext());
        //cursdistname = databaseHandler.getdistributorid(distributorname);
        cursdistname = databaseHandler.getdistributoridFromDisTable(distributorname);

        if(cursdistname!=null && cursdistname.getCount() > 0) {
            if (cursdistname.moveToLast()) {

                do{
                    SelectedDistId = cursdistname.getString(0);
                } while (cursdistname.moveToNext());

            }
        }
    }

    void CheckDistributorData(){
        selectremarks = edtHQuat.getText().toString();

        if(isMyDayPlanPlaced){
            checkDataForEdit();
        }

        if (selectWorkType.equalsIgnoreCase("")) {
            //showAlertDialogToast("Please select a work type");
            toastDisplay("Please select a work type");

        }/*else if (selectremarks.equalsIgnoreCase("")) {
            //showAlertDialogToast("Remarks value is not valid");
            toastDisplay("Remarks value is not valid");

        }*/else {
            if (isInternetOn()) {
                saveData();
            } else {

                toastDisplay("No Internet Connection !!");
            }
        }
    }

    private void checkDataForEdit(){

        if(strId.isEmpty()){
            showAlertMSG("Some Think Wrong");
            return;
        }

        selectWorkType = txtWorkType.getText().toString().trim();
        selectRoute = txtRoutes.getText().toString().trim();
        selectDistributors = txtDistributors.getText().toString().trim();

        if(!selectDistributors.isEmpty()){
            if(layDistributors.getVisibility() == View.VISIBLE) {
                getDistributorIdForEdit();
            }else {
                selectDistributors = "";
            }
        }

        if(!selectRoute.isEmpty()){
            if(layRoutes.getVisibility() == View.VISIBLE) {
                getbeatid(selectRoute);
            }else {
                selectRoute = "";
            }
        }


    }

    void checkData() {
        selectremarks = edtHQuat.getText().toString();

        if(isMyDayPlanPlaced){
            checkDataForEdit();
        }

        if (selectWorkType.equalsIgnoreCase("")) {
            //showAlertDialogToast("Please select a work type");
            toastDisplay("Please select a work type");

        }else {

                if (selectWorkType.equalsIgnoreCase("Leave") || selectWorkType.equalsIgnoreCase("Meeting")
                        || selectWorkType.equalsIgnoreCase("No Field Work") || selectWorkType.equalsIgnoreCase("Holiday")) {

                   // if (!selectremarks.equalsIgnoreCase("")) {
                        if (isInternetOn()) {
                            saveData();
                        } else {

                            toastDisplay("No Internet Connection !!");
                        }
                   /* }else{
                        toastDisplay("Remarks value is not valid");
                    }*/
                }else{
                    if (selectDistributors.equalsIgnoreCase("")) {
                        toastDisplay("Please select a distributor");

                    } else if (selectRoute.equalsIgnoreCase("")) {
                        toastDisplay("Please select a route");

                    } /*else if (selectremarks.equalsIgnoreCase("")) {

                        toastDisplay("Remarks value is not valid");
                    } */else {
                        if (isInternetOn()) {
                            saveData();
                        } else {

                            toastDisplay("No Internet Connection !!");
                        }
                    }
                }

        }

       /* if (selectWorkType.equalsIgnoreCase("")) {
            //showAlertDialogToast("Please select a work type");
            toastDisplay("Please select a work type");

        } else if (selectremarks.equalsIgnoreCase("")) {
            //showAlertDialogToast("Remarks value is not valid");
            toastDisplay("Remarks value is not valid");

        } else if (selectDistributors.equalsIgnoreCase("")) {
            if(userBeatModel.equals("C")){
                if(!selectWorkType.equalsIgnoreCase("Leave")||!selectWorkType.equalsIgnoreCase("Meeting")
                        ||!selectWorkType.equalsIgnoreCase("No Field Work")||!selectWorkType.equalsIgnoreCase("Holiday")){
                    //showAlertDialogToast("Please select a distributor");
                    toastDisplay("Please select a distributor");

                }
            }


        } else if (selectRoute.equalsIgnoreCase("")) {
            if(userBeatModel.equals("C")) {
                if (!selectWorkType.equalsIgnoreCase("Leave") || !selectWorkType.equalsIgnoreCase("Meeting")
                        || !selectWorkType.equalsIgnoreCase("No Field Work") || !selectWorkType.equalsIgnoreCase("Holiday")) {
                    //showAlertDialogToast("Please select a distributor");
                    toastDisplay("Please select a route");

                }
            }

        } else {
            if (isInternetOn()) {
                saveData();
            } else {

                toastDisplay("No Internet Connection !!");
            }
        }*/
    }

    private boolean isTrackingServiceRun(Context context){

        try {
            if (ServiceClass.isServiceRunningInForeground(context, LocationTrackForegroundService.class, TAG)) {
                return true;
            } else{
                PrefManager.setStatus(context, false);  /////////////////
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return PrefManager.getStatus(context);
        }
    }

    private void myDayPlanSettingForTrack(){
        ///////////////////////////////28-09-2019
        PrefManager pre = new PrefManager(this);
        DateTime curDate = new DateTime();
        DateTimeFormatter formatter = DateTimeFormat.forPattern(DATE_PATTERN_FOR_APP);
        String today = formatter.print(curDate);

        if(selectWorkType.equalsIgnoreCase(MY_DAY_PLAN_LEAVE)){

            pre.setBooleanDataByKey(KEY_IS_MY_DAY_PLAN_LEAVE, true);
            pre.setStringDataByKey(KEY_MY_DAY_PLAN_LEAVE_DATE, today);
            if(!isTrackingServiceRun(this)){
                new com.freshorders.freshorder.utils.Log(this).ee(TAG,"...Tracking Not Avail To Stop........");
            }else {
                PrefManager.setStatus(this,false);
                stopService(new Intent(this, LocationTrackForegroundService.class));
                new com.freshorders.freshorder.utils.Log(this).ee(TAG,"...Tracking Auto Stopped........");
            }
        }else {

            pre.setBooleanDataByKey(KEY_IS_MY_DAY_PLAN_LEAVE, false);
            pre.setStringDataByKey(KEY_MY_DAY_PLAN_LEAVE_DATE, today);

            if(!isTrackingServiceRun(this)) {
                AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                Intent intent = new Intent(this, StartTrackingBroadCastImmediate.class);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(this.getApplicationContext(), BR_REQUEST_CODE_START_LIVE_TRACK_By_My_DAY_PLAN, intent,
                        PendingIntent.FLAG_UPDATE_CURRENT);
                alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 1000, pendingIntent);  //AlarmManager.INTERVAL_FIFTEEN_MINUTES
            }

        }
        ////////////////////////////////
    }

    void saveData() {

        loader.show();
        JSONObject objData = new JSONObject();
        try {
            String url = "";
            objData.put("worktype", selectWorkType);
            objData.put("distid", SelectedDistId);
            objData.put("beatnm", selectRoute);
            objData.put("remarks", selectremarks);
            objData.put("id", selectedbeatid);
            objData.put("orderplndt", currentdt);
            objData.put("status", "Active");
            objData.put("updateddt", updateddt);
            objData.put("suserid", userid);
            objData.put("duserid",selected_dealer);

            objData.put("geolat",myDayGeolat);
            objData.put("geolong",myDayGeolon);

            Log.e("objData",objData.toString());

            if(isMyDayPlanPlaced){
                url = Utils.urlUpdateMyDayPlan + strId;  /// edit url (update URL)
                Log.e(TAG,"URL....Update"+url);
            }else {
                url = Utils.strmydayplan;
            }

            RequestQueue queue = Volley.newRequestQueue(MyDayPlan.this);
            JsonObjectRequest jsObjRequest = new JsonObjectRequest(Request.Method.POST, url, objData,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            System.out.println(response);
                            try {
                                String msg = "";
                                if(isMyDayPlanPlaced){
                                    msg = response.getString("message");
                                }else {
                                    String data = response.getString("data");
                                }
                                String success = response.getString("status");
                                if (success.equalsIgnoreCase("true")) {
                                    ///////////////////////////
                                    myDayPlanSettingForTrack();
                                    //////////////////////////
                                    if(isMyDayPlanPlaced){
                                        toastDisplay(msg);
                                    }else {
                                        toastDisplay("Saved Successfully");
                                    }
                                    edtHQuat.setText("");

                                    Intent io=new Intent(MyDayPlan.this,SalesManOrderActivity.class);
                                    startActivity(io);
                                    finish();
                               //   getdaydetail();
                                } else {

                                    toastDisplay("Failed to save DayPlane");
                                }
                            } catch (Exception e) {
                                Log.d("MYDAY_Error", e.getMessage()  );
                            }
                            loader.dismiss();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            toastDisplay("There is problem to save data");
                            Log.e(TAG, "...error" + error.getMessage());
                            loader.dismiss();

                        }
                    });
            jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(50000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(jsObjRequest);
        } catch (Exception e) {

           // Toast.makeText(MyDayPlan.this, "There is problem to save data.", Toast.LENGTH_LONG).show();

        }

    }

    public class ListAdaptor extends RecyclerView.Adapter<ListAdaptor.MyViewHolder> {

        private List<String> itemList;

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView tvItem;

            public MyViewHolder(View view) {
                super(view);
                tvItem = (TextView) view.findViewById(R.id.tvItem);
            }
        }


        public ListAdaptor(List<String> moviesList) {
            this.itemList = moviesList;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_list, parent, false);

            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
            holder.tvItem.setText(itemList.get(position));

            holder.tvItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (WORK == true) {
                        txtWorkType.setText(itemList.get(position));
                        selectWorkType = itemList.get(position);
                        if(selectWorkType.equalsIgnoreCase("Leave")||selectWorkType.equalsIgnoreCase("Meeting")
                        ||selectWorkType.equalsIgnoreCase("No Field Work")||selectWorkType.equalsIgnoreCase("Holiday"))
                        {
                            linearlayoutroute.setVisibility(View.GONE);
                            linearlayoutdistributor.setVisibility(View.GONE);
                            //Kumaravel
                            selectRoute = "";
                            selectDistributors = "";
                            txtDistributors.setText(""); txtRoutes.setText("");
                        }else{
                            if(userBeatModel.equals("C")){
                                linearlayoutroute.setVisibility(View.VISIBLE);
                                linearlayoutdistributor.setVisibility(View.VISIBLE);
                            }

                        }
                    } else if (DISTRIBUTORS == true) {
                        txtDistributors.setText(itemList.get(position));
                        selectDistributors = itemList.get(position);
                        distributorname = selectDistributors;///Kumaravel 01-07-2019

                    } else {
                        txtRoutes.setText(itemList.get(position));
                        selectRoute = itemList.get(position);
                        getbeatid(selectRoute);
                    }

                    selectingDialog.dismiss();

                    new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

                        @Override
                        public void run() {
                            // This method will be executed once the timer is over
                            // Start your app main activity
                            hideSoftKeyboard();
                        }
                    }, 200);

                }
            });
        }

        @Override
        public int getItemCount() {
            return itemList.size();
        }

    }
    private void getBeat(String distributorname) {
        //Kumaravel 01-08-2019
        if(isMyDayPlanPlaced){
            distributorname = txtDistributors.getText().toString();
            if(distributorname.isEmpty()){
                showAlertMSG("first select distributor");
                return;
            }
        }



        Cursor curs;
        curs = databaseHandler.getDistributorBeat(distributorname);

        itemRealList.clear();
        itemTempList.clear();

        Log.e("clear","clear");

        if(curs!=null && curs.getCount() > 0) {
            if (curs.moveToFirst()) {

                do{
                    itemRealList.add(curs.getString(0));
                    itemTempList.add(curs.getString(0));

                } while (curs.moveToNext());


            }
        }

        Cursor cursdistname;
        databaseHandler=new DatabaseHandler(getApplicationContext());
        //cursdistname = databaseHandler.getdistributorid(distributorname);  //
        cursdistname = databaseHandler.getdistributoridFromDisTable(distributorname); //Kumaravel 05-08-2019

        if(cursdistname!=null && cursdistname.getCount() > 0) {
            if (cursdistname.moveToLast()) {

                do{
                    SelectedDistId=cursdistname.getString(0);
                } while (cursdistname.moveToNext());

            }
        }
       selecting_dialog();
    }

    void getbeatid(String beatname){
        Cursor curs;
        curs = databaseHandler.getbeatid(beatname);
        itemRealList.clear();
        itemTempList.clear();
        if(curs!=null && curs.getCount() > 0) {
            if (curs.moveToLast()) {

                do{
                    selectedbeatid=curs.getString(0);
                } while (curs.moveToNext());
            }
        }
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
    public void showAlertDialogToast(String message) {

        android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                        InputMethodManager inn = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                        inn.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                    }
                });


        android.app.AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }
    private void getData() {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "";

            @Override
            protected void onPreExecute() {
                dialog= ProgressDialog.show(MyDayPlan.this, "",
                        "Loading...", true, true);

            }

            @Override
            protected void onPostExecute(Void result) {

                try {

                    PaymentStatus=(JsonAccountObject.getString("pymtstatus"));
                    Log.e("PaymentStatus",PaymentStatus);

                    if(PaymentStatus.equals("Pending")||PaymentStatus.equals(null) ||PaymentStatus.equals("")||PaymentStatus.equals("null")){
                        toastDisplay("Please make payment to place order");
                    }else {
                        if (moveto.equals("myorder")) {

                            Intent io = new Intent(MyDayPlan.this,
                                    SalesManOrderActivity.class);
                            startActivity(io);
                            finish();

                        }else if(moveto.equals("createorder")){
                            Intent io = new Intent(MyDayPlan.this,
                                    CreateOrderActivity.class);
                            startActivity(io);
                            finish();
                        }else if(moveto.equals("Mydealer")){
                            Intent io = new Intent(MyDayPlan.this,
                                    MyDealersActivity.class);
                            io.putExtra("Key","MenuClick");
                            startActivity(io);
                            finish();
                        }
                        else if(moveto.equals("profile")){
                            Intent io = new Intent(MyDayPlan.this,
                                    ProfileActivity.class);
                            startActivity(io);
                            finish();
                        }
                        else if(moveto.equals("postnotes")){
                            Intent io = new Intent(MyDayPlan.this,
                                    MerchantComplaintActivity.class);
                            startActivity(io);
                            finish();
                        }
                        else if(moveto.equals("clientvisit")){
                            Intent io = new Intent(MyDayPlan.this,
                                    SMClientVisitHistory.class);
                            startActivity(io);
                            finish();
                        }else if(moveto.equals("addmerchant")){
                            Intent io = new Intent(MyDayPlan.this, AddMerchantNew.class);
                            startActivity(io);
                            finish();
                        }
                        else if(moveto.equals("acknowledge")){
                            Intent io = new Intent(MyDayPlan.this, SalesmanAcknowledgeActivity.class);
                            startActivity(io);
                            finish();
                        }
                        else if(moveto.equals("paymentcoll")){
                            Intent io = new Intent(MyDayPlan.this, SalesmanPaymentCollectionActivity.class);
                            startActivity(io);
                            finish();
                        }

                        else if(moveto.equals("surveyuser")){
                            Intent io = new Intent(MyDayPlan.this, SurveyMerchant.class);
                            startActivity(io);
                            finish();
                        }
                        else if(moveto.equals("mysales")){
                            Intent io = new Intent(MyDayPlan.this, MySalesActivity.class);
                            startActivity(io);
                            finish();
                        } else if (moveto.equals("diststock")) {
                            Intent io = new Intent(MyDayPlan.this,
                                    DistributrStockActivity.class);
                            startActivity(io);
                            finish();
                        }

                    }

                    dialog.dismiss();

                }catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();


                JsonServiceHandler = new JsonServiceHandler(Utils.strGetProfileDetail+Constants.USER_ID, MyDayPlan.this);
                JsonAccountObject = JsonServiceHandler.ServiceDataGet();
                return null;
            }
        }.execute(new Void[]{});
    }
    public void onBackPressed() {
        exitAlret();
    }
    private void exitAlret() {
        android.app.AlertDialog.Builder localBuilder = new android.app.AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        localBuilder.setMessage("Do you want to Exit?");
        localBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {

                        finish();

                    }
                });
        localBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {
                        paramDialogInterface.cancel();
                    }
                });
        localBuilder.create().show();
    }
    protected void toastDisplay(String msg) {
        Toast toast = Toast.makeText(MyDayPlan.this, msg,
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();

    }

    public void showAlertMSG(String message) {

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setMessage(message);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        android.app.AlertDialog alert = builder.create();
        alert.show();

        Button buttonbackground = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    private boolean CheckGpsStatus(){
        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );
        return manager != null && manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    // Trigger new location updates at interval
    @SuppressLint("MissingPermission")
    protected void startLocationUpdates() {

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations, this can be null.
                        if (location != null) {
                            updateCurrentLocation(location);
                            mLastLocation = location;
                        }
                    }
                });
        try {
            locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            //    mLastLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            passive_enabled = locationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER);

            if (gps_enabled) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, locationUpdateInterval, 0,
                        locationListenerGps);
            }

            if (network_enabled) {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, locationUpdateInterval, 0,
                        locationListenerNetwork);
            }
            if (passive_enabled) {
                locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, locationUpdateInterval, 0,
                        locationListenerPassive);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    LocationListener locationListenerNetwork = new LocationListener() {
        public void onLocationChanged(Location location) {
            if (location != null) {
                mLastLocation = location;
                updateCurrentLocation(location);
            }
        }
        public void onProviderDisabled(String provider) { }
        public void onProviderEnabled(String provider) { }
        public void onStatusChanged(String provider, int status, Bundle extras) { }
    };

    LocationListener locationListenerGps = new LocationListener() {
        public void onLocationChanged(Location location) {
            if (location != null) {
                mLastLocation = location;
                updateCurrentLocation(location);
            }
        }
        public void onProviderDisabled(String provider) { }
        public void onProviderEnabled(String provider) { }
        public void onStatusChanged(String provider, int status, Bundle extras) { }
    };


    LocationListener locationListenerPassive = new LocationListener() {
        public void onLocationChanged(Location location) {
            if (location != null) {
                mLastLocation = location;
                updateCurrentLocation(location);
            }
        }
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) { }
        @Override
        public void onProviderEnabled(String provider) { }
        @Override
        public void onProviderDisabled(String provider) { }

    };

    public void showSettingsAlert() {
        android.app.AlertDialog.Builder alertDialog = new android.app.AlertDialog.Builder(MyDayPlan.this);

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.delete);

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, 2);

            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("inside", String.valueOf(resultCode));
        if (resultCode == 0) {
            switch (requestCode) {
                case 1:
                    Log.e("inside", "inside");
                    Intent io = new Intent(MyDayPlan.this, MyDayPlan.class);
                    startActivity(io);
                    finish();
                    break;
            }
        }

        if (requestCode == 2) {
            Intent intent = new Intent(this, GPSTrackerActivity.class);
            startActivityForResult(intent, 1);
        }
    }

    private void showD(String msg){
        if(pDialog == null){
            pDialog = ProgressDialog.show(MyDayPlan.this, "", msg, true, false);
        }else {
            if (!pDialog.isShowing()) {
                pDialog = ProgressDialog.show(MyDayPlan.this, "", msg, true, false);
            }
        }
    }

    private void hideD(){
        if(pDialog != null){
            if(pDialog.isShowing()){
                pDialog.dismiss();
            }
        }
    }

    synchronized private void updateCurrentLocation(Location mLastLocation){
        myDayGeolat = mLastLocation.getLatitude();
        myDayGeolon = mLastLocation.getLongitude();
    }

    private void locationUpdateWithDelay(){

        showD(Constants.GPS_LOCATION_FETCHING);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable(){
            @Override
            public void run(){
                gpsCounter ++ ;
                if(mLastLocation != null) {
                    updateCurrentLocation(mLastLocation);
                }else {
                    if(gpsCounter >= 2){
                        hideD();
                        if(CheckGpsStatus()){
                            showAlertMSG(getResources().getString(R.string.gps_accuracy_setting));
                            return;
                        }else {
                            showSettingsAlert();
                            return;
                        }
                    }else {
                        locationUpdateWithDelay();
                    }

                }
                hideD();
                if (userBeatModel.equals("D")) {
                    CheckDistributorData();
                } else {
                    checkData();
                }
            }
        }, 6000);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mFusedLocationClient == null) {
            startLocationUpdates();
        }
    }

}
