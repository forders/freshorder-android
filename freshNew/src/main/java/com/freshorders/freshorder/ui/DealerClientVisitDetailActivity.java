package com.freshorders.freshorder.ui;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.MyLocation;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


/**
 * Created by Pavithra on 20-10-2016.
 */
public class DealerClientVisitDetailActivity extends FragmentActivity implements OnMapReadyCallback {
    TextView textViewMerchant,textViewSalesman,textViewVisitDate,textViewNotes,textViewAssetMenu;
    LinearLayout linearLayoutBack;
    GoogleMap googleMap;
    public  static Location loc;
    LatLng point;
    private final int DEFAULT_ZOOM = 18;

    @SuppressLint("MissingPermission")
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.dealer_client_visit_detail_page);
        textViewMerchant = (TextView) findViewById(R.id.textViewMerchant);
        textViewSalesman = (TextView) findViewById(R.id.textViewSalesman);
        textViewVisitDate = (TextView) findViewById(R.id.textViewVisitDate);
        textViewNotes = (TextView) findViewById(R.id.textViewNotes);
        textViewAssetMenu = (TextView) findViewById(R.id.textViewAssetMenu);
        linearLayoutBack = (LinearLayout) findViewById(R.id.linearLayoutBack);

        Typeface font = Typeface.createFromAsset(getAssets(),
                "fontawesome-webfont.ttf");
        textViewAssetMenu.setTypeface(font);

        if(Constants.USER_TYPE.equals("D")){
            textViewMerchant.setText("Client : " + "\t" + DealerClientVisit.mname);
            textViewSalesman.setText("Salesman : " + "\t" + DealerClientVisit.sname);
            textViewVisitDate.setText("Visit Date : " + "\t" + DealerClientVisit.date);
            textViewNotes.setText("Notes : " + "\n" +"\t"+ DealerClientVisit.desc);

        }else{
            textViewMerchant.setText("Client : " + "\t" + SMClientVisitHistory.mname);
            textViewSalesman.setVisibility(View.GONE);
            textViewVisitDate.setText("Visit Date : " + "\t" + SMClientVisitHistory.date);
            textViewNotes.setText("Notes : " + "\n" +"\t"+ SMClientVisitHistory.desc);
        }

        linearLayoutBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Constants.USER_TYPE.equals("D")){
                    Constants.fromDate="";
                    Intent to = new Intent(DealerClientVisitDetailActivity.this, DealerClientVisit.class);
                    startActivity(to);
                    finish();
                }else{
                    Intent to = new Intent(DealerClientVisitDetailActivity.this, SMClientVisitHistory.class);
                    startActivity(to);
                    finish();
                }


            }
        });
        MyLocation.LocationResult locationResult = new MyLocation.LocationResult() {

            public void gotLocation(final Location location) {
                loc = location;
                System.out.println("Latitude: " + loc.getLatitude());

                System.out.println("Longitude: " + loc.getLongitude());
            }
        };


        MyLocation myLocation = new MyLocation();
        myLocation.getLocation(DealerClientVisitDetailActivity.this, locationResult);

        try {
            // LatLng object to store user input coordinates
            if(Constants.USER_TYPE.equals("D")){
                point = new LatLng(Double.parseDouble(DealerClientVisit.lat), Double.parseDouble(DealerClientVisit.lng));
                Log.e("POINT VALUES", String.valueOf(point));
                // Drawing the marker at the coordinates
                ////////drawMarker(point);
            }else{
                point = new LatLng(Double.parseDouble(SMClientVisitHistory.lat), Double.parseDouble(SMClientVisitHistory.lng));
                Log.e("POINT VALUES", String.valueOf(point));
                // Drawing the marker at the coordinates
                /////////////drawMarker(point);
            }


        } catch (NumberFormatException e) {
            Log.e("exception", e.getMessage());
        }


        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());

        // Showing status
        if (status != ConnectionResult.SUCCESS) { // Google Play Services are not available

            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, getParent(), requestCode);
            dialog.show();

        } else { // Google Play Services are available
            try {
                // Getting reference to the SupportMapFragment of activity_main.xml
                SupportMapFragment fm = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
                // Getting GoogleMap object from the fragment
                /////////googleMap = fm.getMap();/////////Kumaravel Changed this for GoogleLatestApi
                fm.getMapAsync(this);

            } catch (Exception e) {
                Log.e("exception", e.getMessage());
            }

        }
    }



    private void drawMarker(LatLng point){
        // Clears all the existing coordinates
        googleMap.clear();

        // Creating an instance of MarkerOptions
        MarkerOptions markerOptions = new MarkerOptions();

        // Setting latitude and longitude for the marker
        markerOptions.position(point);

        // Setting title for the InfoWindow
        markerOptions.title("Position");

        // Setting InfoWindow contents
        markerOptions.snippet("Latitude:" + point.latitude + ",Longitude" + point.longitude);

        // Adding marker on the Google Map
        googleMap.addMarker(markerOptions);

        // Moving CameraPosition to the user input coordinates
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(point,17.5f));
       // googleMap.animateCamera(CameraUpdateFactory.zoomTo(21.0f));

    }
    protected void toastDisplay(String msg) {
        Toast toast = Toast.makeText(DealerClientVisitDetailActivity.this, msg,
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();

    }
    public boolean netCheck() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            }

            else if (result == null) {
                showAlertDialog(DealerClientVisitDetailActivity.this,
                        "No Internet Connection",
                        "Please Check Your internet connection.", false);
                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
    public void showAlertDialog(Context context, String title, String message,
                                Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        alertDialog.show();
    }

    public void showAlertDialogToast( String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }
    public void onBackPressed() {
        exitAlret();
    }

    private void exitAlret() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        localBuilder.setMessage("Do you want to Exit?");
        localBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {

                        finish();

                    }
                });
        localBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {
                        paramDialogInterface.cancel();
                    }
                });
        localBuilder.create().show();
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMaps) {
        googleMap = googleMaps;
        // Enabling MyLocation Layer of Google Map
        googleMap.setMyLocationEnabled(true);

        //drawMarker(point);//////by Kumaravel
        markerLoadFirstTime();
    }

    private void markerLoadFirstTime(){

        LatLng selectedPlace = point;
        String title = "Position";
        String snippet = "Latitude:" + point.latitude + ",Longitude" + point.longitude;

        Marker positionMarker = googleMap.addMarker(new MarkerOptions()
                .title(title)
                .snippet(snippet)
                .position(selectedPlace)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                .draggable(false));
        positionMarker.setDraggable(false);
        positionMarker.showInfoWindow();
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(selectedPlace, DEFAULT_ZOOM));


    }
}
