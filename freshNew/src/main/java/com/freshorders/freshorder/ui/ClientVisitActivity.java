package com.freshorders.freshorder.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.MDealerCompDropdownDomain;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.GPSTracker;
import com.freshorders.freshorder.utils.GPSTrackerActivity;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;

/**
 * Created by Pavithra on 19-10-2016.
 */
public class ClientVisitActivity extends Activity {

    private static final String TAG = ClientVisitActivity.class.getSimpleName();
    private Location mLastLocation;
    private FusedLocationProviderClient mFusedLocationClient;
    LocationManager locationManager;
    boolean gps_enabled = false;
    boolean network_enabled = false;
    boolean passive_enabled = false;

    private long initialLocationTime;

    private ProgressDialog pDialog;
    //by Kumaravel

    private LocationRequest mLocationRequest;

    private long UPDATE_INTERVAL = 10 * 1000;  /* 10 secs */
    private long FASTEST_INTERVAL = 2000; /* 2 sec */


    TextView menuIcon, textViewAssetMenuProfile, textViewAssetMenuMyOrders,
            textViewAssetMenuMyDealers, textViewAssetMenuProducts,
            textViewAssetMenuPayment, textViewAssetMenuComplaint,
            textViewAssetMenuSignout, textViewAssetMenuCreateOrder, textViewAssetMenuClientVisit, textViewcross_1, textViewAssetMenuPaymentCollection;
    int menuCliccked;
    LinearLayout linearLayoutMenuParent, linearLayoutProfile, linearLayoutMyOrders,
            linearLayoutMyDealers, linearLayoutProducts, linearLayoutPayment,
            linearLayoutComplaint, linearLayoutSignout, linearLayoutCreateOrder, linearLayoutClientVisit, linearLayoutPaymentCollection;


    private void showMenu(){

        /////////////////////////////////
        menuIcon.setVisibility(View.GONE);
        linearLayoutMenuParent.setVisibility(View.GONE);

        /*
        MyApplication app = MyApplication.getInstance();

        if(!app.isCreateOrder()){
            linearLayoutCreateOrder.setVisibility(View.GONE);
        }
        if(!app.isProfile()){
            linearLayoutProfile.setVisibility(View.GONE);
        }
        if(!app.isMyDayPlan()){
            //linearLayoutMyDayPlan.setVisibility(View.GONE);
        }
        if(!app.isMyOrders()){
            linearLayoutMyOrders.setVisibility(View.GONE);
        }
        if(!app.isAddMerchant()){
            //linearLayoutAddMerchant.setVisibility(View.GONE);
        }
        if(!app.isMySales()){
            //linearLayoutMySales.setVisibility(View.GONE);
        }
        if(!app.isPostNotes()){
            linearLayoutComplaint.setVisibility(View.GONE);
        }
        if(!app.isMyClientVisit()){
            linearLayoutClientVisit.setVisibility(View.GONE);
        }
        if(!app.isAcknowledge()){
            //linearLayoutAcknowledge.setVisibility(View.GONE);
        }
        if(!app.isPaymentCollection()){
            linearLayoutPaymentCollection.setVisibility(View.GONE);
        }
        if(!app.isPkdDataCapture()){
            //linearLayoutStockEntry.setVisibility(View.GONE);
        }
        if(!app.isDistributorStock()){
            //linearLayoutDistStock.setVisibility(View.GONE);
        }
        if(!app.isSurveyUser()){
            //linearLayoutsurveyuser.setVisibility(View.GONE);
        }
        if(!app.isDownLoadScheme()){
            //linearLayoutdownloadscheme.setVisibility(View.GONE);
        }
        if(!app.isDistanceCalculation()){
            //linearLayoutDistanceCalculation.setVisibility(View.GONE);
        }
        if(!app.isRefresh()){
            //linearLayoutRefresh.setVisibility(View.GONE);
        }
        if(!app.isLogout()){
            linearLayoutSignout.setVisibility(View.GONE);
        }
        if(!app.isClosingStock()){
            //linearLayoutStockOnly.setVisibility(View.GONE);
        }
        if(!app.isClosingStockAudit()){
            //linearLayoutStockAudit.setVisibility(View.GONE);
        }
        if(!app.isPendingData()) {
            //linearLayoutMigration.setVisibility(View.GONE);
        }  */
    }


    JSONObject JsonAccountObject = null;
    JSONArray JsonAccountArray = null;
    DatabaseHandler databaseHandler;
    EditText EditTextViewDescription;
    AutoCompleteTextView autocompletemrchantlist;
    Button ButtonDate, ButtonTime, buttonSend, buttoncancel;
    JsonServiceHandler JsonServiceHandler;
    String moveto, PaymentStatus, date, time;
    public int monthnew, yearnew, daynew;
    public static Date dateStr = null;
    private int day, today;
    public static String fromdate, merchantlist, discription, muserId, am = "", uppenddate, rowid = "";
    private static int merchantPrimaryKey = 0;
    private int year, hr, sec, mins;
    DatePicker datePicker;
    ArrayList<MDealerCompDropdownDomain> arrayListDealerDomain, arrayListSearch, arrayListmerchant;
    ArrayList<String> arrayListDealer;
    public static int clientvisitmerchantselected = 0;
    SQLiteDatabase db;
    TimePicker timePicker;
    static final int TIME_DIALOG_ID = 999;
    StringBuilder time1;
    String dayOfTheWeek = "", beatMerchants, userBeatModel = "";
    GPSTracker gps;

    double clientvisitgeolat, clientvisitgeolog;

    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.salesman_client_visit);

        int permissionCheck1 = ContextCompat.checkSelfPermission(ClientVisitActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);

        if (permissionCheck1 != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    ClientVisitActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.LOCATION_PERMISSION_REQUEST_CODE);
        } else {
            /*Intent intent = new Intent(this, GPSTrackerActivity.class);
            startActivityForResult(intent,1);*/
            try {
                if (CheckGpsStatus()) {
                    startLocationUpdates();
                } else {
                    showSettingsAlert();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        databaseHandler = new DatabaseHandler(getApplicationContext());
        netCheck();
        db = getApplicationContext().openOrCreateDatabase("freshorders", 0, null);
        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();
        datePicker = (DatePicker) findViewById(R.id.datePicker1);
        autocompletemrchantlist = (AutoCompleteTextView) findViewById(R.id.autocompletemrchantlist);
        EditTextViewDescription = (EditText) findViewById(R.id.EditTextViewDescription);
        ButtonDate = (Button) findViewById(R.id.ButtonDate);
        ButtonTime = (Button) findViewById(R.id.ButtonTime);
        buttonSend = (Button) findViewById(R.id.buttonSend);
        buttoncancel = (Button) findViewById(R.id.buttoncancel);
        timePicker = (TimePicker) findViewById(R.id.timePicker);
        linearLayoutCreateOrder = (LinearLayout) findViewById(R.id.linearLayoutCreateOrder);
        linearLayoutProfile = (LinearLayout) findViewById(R.id.linearLayoutProfile);
        linearLayoutMyOrders = (LinearLayout) findViewById(R.id.linearLayoutMyOrders);
        linearLayoutMyDealers = (LinearLayout) findViewById(R.id.linearLayoutMyDealers);
        linearLayoutProducts = (LinearLayout) findViewById(R.id.linearLayoutProducts);
        linearLayoutPayment = (LinearLayout) findViewById(R.id.linearLayoutPayment);
        linearLayoutComplaint = (LinearLayout) findViewById(R.id.linearLayoutComplaint);
        linearLayoutClientVisit = (LinearLayout) findViewById(R.id.linearLayoutClientVisit);
        linearLayoutSignout = (LinearLayout) findViewById(R.id.linearLayoutSignout);
        linearLayoutPaymentCollection = (LinearLayout) findViewById(R.id.linearLayoutPaymentCollection);
        //gps = new GPSTracker(ClientVisitActivity.this);


        menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
        textViewAssetMenuCreateOrder = (TextView) findViewById(R.id.textViewAssetMenuCreateOrder);
        textViewAssetMenuProfile = (TextView) findViewById(R.id.textViewAssetMenuProfile);
        textViewAssetMenuMyOrders = (TextView) findViewById(R.id.textViewAssetMenuMyOrders);
        textViewAssetMenuMyDealers = (TextView) findViewById(R.id.textViewAssetMenuMyDealers);
        textViewAssetMenuProducts = (TextView) findViewById(R.id.textViewAssetMenuProducts);
        textViewAssetMenuPayment = (TextView) findViewById(R.id.textViewAssetMenuPayment);
        textViewAssetMenuComplaint = (TextView) findViewById(R.id.textViewAssetMenuComplaint);
        textViewAssetMenuSignout = (TextView) findViewById(R.id.textViewAssetMenuSignout);
        textViewAssetMenuClientVisit = (TextView) findViewById(R.id.textViewAssetMenuClientVisit);
        textViewcross_1 = (TextView) findViewById(R.id.textViewcross_1);
        textViewAssetMenuPaymentCollection = (TextView) findViewById(R.id.textViewAssetMenuPaymentCollection);

        linearLayoutMenuParent = (LinearLayout) findViewById(R.id.linearLayoutMenuParent);
        Typeface font = Typeface.createFromAsset(getAssets(),
                "fontawesome-webfont.ttf");
        menuIcon.setTypeface(font);
        textViewAssetMenuCreateOrder.setTypeface(font);
        textViewAssetMenuProfile.setTypeface(font);
        textViewAssetMenuMyOrders.setTypeface(font);
        textViewAssetMenuMyDealers.setTypeface(font);
        textViewAssetMenuProducts.setTypeface(font);
        textViewAssetMenuPayment.setTypeface(font);
        textViewAssetMenuComplaint.setTypeface(font);
        textViewAssetMenuSignout.setTypeface(font);
        textViewAssetMenuClientVisit.setTypeface(font);
        textViewAssetMenuPaymentCollection.setTypeface(font);
        textViewcross_1.setTypeface(font);
        showCurrentDateOnView();

        arrayListDealerDomain = new ArrayList<MDealerCompDropdownDomain>();
        arrayListmerchant = new ArrayList<MDealerCompDropdownDomain>();
        arrayListSearch = new ArrayList<MDealerCompDropdownDomain>();
        arrayListDealer = new ArrayList<String>();

        Cursor cur;
        cur = databaseHandler.getDetails();
        cur.moveToFirst();
        Constants.clientdealerid = cur.getString(cur
                .getColumnIndex(DatabaseHandler.KEY_clientdealid));

        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date d = new Date();
        dayOfTheWeek = sdf.format(d);
        Log.e("dateday", dayOfTheWeek);

        Cursor cur1;
        cur1 = databaseHandler.getUserSetting();
        cur1.moveToFirst();
        userBeatModel = cur1.getString(cur1.getColumnIndex(DatabaseHandler.KEY_settingrefvalue));
        Log.e("userBeatModel", userBeatModel);

        if (userBeatModel.equalsIgnoreCase("C")) {
            Log.e("userModel C", "userModel C");
            Cursor curr;
            curr = databaseHandler.getDetails();
            curr.moveToFirst();
            String selectedbeat = curr.getString(curr.getColumnIndex(DatabaseHandler.KEY_selectedBeat));
            String selectedDate = curr.getString(curr.getColumnIndex(DatabaseHandler.KEY_selectedDate));
            String todayDate = new SimpleDateFormat(
                    "yyyy-MM-dd").format(new java.util.Date());
            if (todayDate.equals(selectedDate) && !selectedbeat.equals("")) {
                getBeatMerchant(selectedbeat);

            } else {
                Constants.beatassignscreen = "ClientVisit";
                Intent to = new Intent(ClientVisitActivity.this, CompanyBeatActivity.class);
                startActivity(to);
                finish();
            }


        } else {
            Log.e("userModel D", "userModel D");
            offlineBeat();
        }


        autocompletemrchantlist.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                autocompletemrchantlist.showDropDown();

                textViewcross_1.setVisibility(View.VISIBLE);
                textViewcross_1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        autocompletemrchantlist.setText("");
                        Constants.clientvisitmerchname = "";
                        Constants.clientvisitmerchant_id = "";
                        clientvisitmerchantselected = 0;
                        if (autocompletemrchantlist.equals("")) {
                            InputMethodManager iaa = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            iaa.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                        }

                    }
                });

                return false;
            }
        });


        autocompletemrchantlist
                .setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {

                        String mnsme = autocompletemrchantlist.getText().toString();
                        for (MDealerCompDropdownDomain cd : arrayListmerchant) {
                            if (cd.getComName().equals(mnsme)) {
                                muserId = cd.getID();
                                merchantPrimaryKey = cd.getMerchantRowId();
                                Constants.clientvisitmerchant_id = muserId;
                                clientvisitmerchantselected = 1;
                                Constants.clientvisitmerchname = mnsme;

                                Log.e("Merchant", Constants.clientvisitmerchname);
                                Log.e("selected merchant Id", Constants.clientvisitmerchant_id);
                            }
                        }
                        if (clientvisitmerchantselected == 1) {
                            textViewcross_1.setVisibility(View.GONE);
                        }
                    }
                });

        EditTextViewDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textViewcross_1.setVisibility(View.GONE);
            }
        });
        menuIcon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Intent io = new Intent(ClientVisitActivity.this, SMClientVisitHistory.class);
                startActivity(io);
                finish();
               /* if (menuCliccked == 0) {
                    linearLayoutMenuParent.setVisibility(View.VISIBLE);
                    menuCliccked = 1;
                } else {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                }*/

            }
        });

        linearLayoutMenuParent.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);

                menuCliccked = 0;
            }
        });

        linearLayoutProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Intent io = new Intent(ClientVisitActivity.this, ProfileActivity.class);
                startActivity(io);
                finish();
            }
        });

        linearLayoutMyOrders.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Constants.checkproduct = 0;
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Constants.orderstatus = "Success";
                moveto = "myorder";
                if (netCheckWithoutAlert() == true) {
                    getData();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        switch (Constants.USER_TYPE) {
                            case "D": {
                                Intent io = new Intent(ClientVisitActivity.this,
                                        DealersOrderActivity.class);
                                io.putExtra("Key", "menuclick");
                                startActivity(io);
                                finish();
                                break;
                            }
                            case "M": {
                                Intent io = new Intent(ClientVisitActivity.this,
                                        MerchantOrderActivity.class);
                                io.putExtra("Key", "menuclick");
                                Constants.setimage = null;
                                startActivity(io);
                                finish();
                                break;
                            }
                            default: {
                                Intent io = new Intent(ClientVisitActivity.this,
                                        SalesManOrderActivity.class);
                                io.putExtra("Key", "menuclick");
                                Constants.setimage = null;
                                startActivity(io);
                                finish();
                                break;
                            }
                        }

                    }
                }
            }
        });

        linearLayoutMyDealers.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                moveto = "mydealer";
                if (netCheckWithoutAlert() == true) {
                    getData();
                } else {
                    Intent io = new Intent(ClientVisitActivity.this, MyDealersActivity.class);
                    io.putExtra("Key", "MenuClick");

                    startActivity(io);
                    finish();
                }
            }
        });

        linearLayoutProducts.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                moveto = "product";
                if (netCheckWithoutAlert() == true) {
                    getData();
                } else {
                    Intent io = new Intent(ClientVisitActivity.this, ProductActivity.class);
                    startActivity(io);

                    finish();
                }
            }
        });

        linearLayoutCreateOrder.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;

                MyApplication.getInstance().setTemplate8ForTemplate2(false);

                moveto = "createorder";
                if (netCheckWithoutAlert() == true) {
                    getData();
                } else {
                    Intent io = new Intent(ClientVisitActivity.this,
                            CreateOrderActivity.class);
                    startActivity(io);
                    finish();
                }
            }
        });

        linearLayoutPayment.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Intent io = new Intent(ClientVisitActivity.this, PaymentActivity.class);
                startActivity(io);
                finish();
            }
        });

        linearLayoutComplaint.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (Constants.USER_TYPE.equals("D")) {
                    Intent io = new Intent(ClientVisitActivity.this,
                            DealersComplaintActivity.class);
                    startActivity(io);
                    finish();
                } else {
                    Intent io = new Intent(ClientVisitActivity.this,
                            MerchantComplaintActivity.class);
                    startActivity(io);
                    finish();
                }
            }
        });
        linearLayoutClientVisit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;


            }
        });

        linearLayoutPaymentCollection.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;

                Intent io = new Intent(ClientVisitActivity.this,
                        SalesmanPaymentCollectionActivity.class);
                startActivity(io);
                finish();

            }
        });

        linearLayoutSignout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                // TODO Auto-generated method stub
                databaseHandler.delete();
                Intent io = new Intent(ClientVisitActivity.this, SigninActivity.class);

                startActivity(io);
                finish();
            }
        });
        ButtonTime.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                showDialog(TIME_DIALOG_ID);

            }

        });

        buttoncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent io = new Intent(ClientVisitActivity.this, SMClientVisitHistory.class);
                startActivity(io);
                finish();
            }
        });
       /* ButtonDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DatePickerDialog mdiDialog = new DatePickerDialog(
                        ClientVisitActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view,
                                                  int year, int month,
                                                  int dayOfMonth) {
                                Log.e("date1", year + "-" + (month)
                                        + "-" + day);
                                Log.e("date", year + "-" + (month+1)
                                        + "-" + dayOfMonth);
                                String time =year + "-" + (month+1)
                                        + "-" + dayOfMonth;

                                String monthLength =String.valueOf(month+1);
                                Log.e("date", monthLength);
                                if(monthLength.length()==1){
                                    monthLength = "0"+monthLength;
                                }
                                String dayLength =String.valueOf(dayOfMonth);
                                if(dayLength.length()==1){
                                    dayLength = "0"+dayLength;
                                }
                                fromdate = year + "-" + monthLength
                                        + "-" + dayLength;
                                monthnew=month;
                                yearnew=year;
                                daynew=dayOfMonth;
                                Log.e("date3", String.valueOf(month));
                                Log.e("date2", String.valueOf(monthnew));
                                String inputPattern = "yyyy-MM-dd";
                                String outputPattern = "dd-MMM-yyyy ";
                                SimpleDateFormat inputFormat = new SimpleDateFormat(
                                        inputPattern);
                                SimpleDateFormat outputFormat = new SimpleDateFormat(
                                        outputPattern);

                                String str = null;
                                try {
                                    dateStr = inputFormat.parse(time);
                                    str = outputFormat.format(dateStr);
                                    date=str;
                                    ButtonDate.setText(str);
                                    // Log.e("str", str);
                                } catch (Exception  e) {
                                    e.printStackTrace();
                                }
                            }
                        }, yearnew, monthnew, daynew);
                mdiDialog.show();
            }
        });*/
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyLocationUpdatedForClientVisit();
            }
        });

        /////////////////
        showMenu();
        ///////////////
    }

    private void  processClientVisitData(){
        // TODO Auto-generated method stub
        discription = EditTextViewDescription.getText().toString();
        date = ButtonDate.getText().toString();
        time = ButtonTime.getText().toString();
        Log.e("cos", Constants.setdiscription);
        merchantlist = autocompletemrchantlist.getText().toString();
        Log.e("cos", merchantlist);
        if (clientvisitmerchantselected == 1 && !merchantlist.equals(".") && !merchantlist.equals("")) {
            if (!discription.equals("")) {
                if (!date.equals("")) {
                    if (!time1.equals("")) {

                              /*  if (gps.canGetLocation() == true) {

                                    double latitude = gps.getLatitude();
                                    double longitude = gps.getLongitude();*/
                        uppenddate = date + " " + time1;
                        String status = "Completed";
                        Log.e("lat", String.valueOf(clientvisitgeolat));
                        Log.e("lng", String.valueOf(clientvisitgeolog));
                        Log.e("status", status);
                        Log.e("merchantlist", muserId);
                        Log.e("date", String.valueOf(uppenddate));
                        Log.e("suserid", String.valueOf(Constants.USER_ID));
                        Log.e("duserid", String.valueOf(Constants.clientdealerid));

                        String clientstatus = "Pending";
                        //databaseHandler.addclientvisit(Constants.clientdealerid,muserId, discription, uppenddate, clientvisitgeolat, clientvisitgeolog, status, Constants.USER_ID,clientstatus,Constants.clientvisitmerchname);

                        databaseHandler.addclientvisit(Constants.clientdealerid, muserId,
                                discription, uppenddate, clientvisitgeolat, clientvisitgeolog,
                                status, Constants.USER_ID, clientstatus, Constants.clientvisitmerchname, merchantPrimaryKey);


                        Cursor cur;
                        cur = databaseHandler.getclientvisit();
                        Log.e("count", String.valueOf(cur.getCount()));
                        Log.e("netcheck", String.valueOf(netCheck()));
                        if (netCheck1() == true) {
                            sendtosever();

                        } else {
                            showAlertDialogToast1("Saved successfully");
                            Constants.clientvisitmerchant_id = "";
                            clientvisitmerchantselected = 0;
                            Constants.clientvisitmerchname = "";
                            Intent to = new Intent(ClientVisitActivity.this, SMClientVisitHistory.class);
                            startActivity(to);
                            finish();
                        }


                    } else {
                        showAlertDialogToast("Time should not be empty");
                    }

                } else {
                    showAlertDialogToast("Date should not be empty");
                }
            } else {
                showAlertDialogToast("Description should not be empty");
            }
        } else {
            showAlertDialogToast("Traders should not be empty");
        }
    }


    private void showCurrentDateOnView() {
        // TODO Auto-generated method stub
        final Calendar c = Calendar.getInstance();
        yearnew = c.get(Calendar.YEAR);


        monthnew = c.get(Calendar.MONTH);
        daynew = c.get(Calendar.DAY_OF_MONTH);

        hr = c.get(Calendar.HOUR_OF_DAY);
        mins = c.get(Calendar.MINUTE);
        sec = c.get(Calendar.SECOND);
        if (c.get(Calendar.AM_PM) == Calendar.AM)
            am = "AM";
        else if (c.get(Calendar.AM_PM) == Calendar.PM)
            am = "PM";


        Log.e("ammmm", String.valueOf(am));
        String monthLength = String.valueOf(monthnew + 1);
        if (monthLength.length() == 1) {
            monthLength = "0" + monthLength;
        }
        String dayLength = String.valueOf(daynew);
        if (dayLength.length() == 1) {
            dayLength = "0" + dayLength;
        }

        Log.e("monthLength", monthLength);

        Log.e("dayLength", dayLength);
        // set current date into textview
        time1 = new StringBuilder().append(hr).append(":").append(mins).append(":").append(sec);
        ButtonTime.setText(new StringBuilder().append(hr).append(":").append(mins).append(" ").append(am));
        ButtonDate.setText(new StringBuilder()
                // Month is 0 based, just add 1
                .append(yearnew).append("-").append(monthLength).append("-").append(dayLength)
        );


        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd-MMM-yyyy ";
        SimpleDateFormat inputFormat = new SimpleDateFormat(
                inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(
                outputPattern);

        String str = null;
        try {
            dateStr = inputFormat.parse(time);
            str = outputFormat.format(dateStr);
            ButtonDate.setText(str);
            // Log.e("str", str);
        } catch (Exception e) {
            e.printStackTrace();
        }
        datePicker.init(year, monthnew, day, null);

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case TIME_DIALOG_ID:
                // set time picker as current time
                return new TimePickerDialog(this, timePickerListener, hr, mins, false);

        }
        return null;
    }

    private TimePickerDialog.OnTimeSetListener timePickerListener = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int selectedHour, int selectedMinute) {
            hr = selectedHour;
            mins = selectedMinute;

            // set current time into textview
            time1 = new StringBuilder().append(hr).append(":").append(mins);
            ButtonTime.setText(new StringBuilder().append(hr).append(":").append(mins).append(" ").append(am));

            // set current time into timepicker
            timePicker.setCurrentHour(hr);
            timePicker.setCurrentMinute(mins);

        }
    };

    public void getBeatMerchant(String beatname) {
        Cursor cursor;
        cursor = databaseHandler.getbeatMerchant(beatname);
        String merchants = "";
        int i = 0;
        if (cursor != null && cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {

                do {
                    if (i == 0) {
                        if (cursor.getString(0).equals("New User")) {
                            merchants = "'" + cursor.getString(0) + "'";
                        } else {
                            merchants = cursor.getString(0);
                        }

                        i++;
                    } else {
                        if (cursor.getString(0).equals("New User")) {
                            merchants = merchants + ", '" + cursor.getString(0) + "'";
                        } else {
                            merchants = merchants + "," + cursor.getString(0);
                        }

                    }


                } while (cursor.moveToNext());


            }
        }
        Log.e("merchants list", merchants);

        loadingMerchant(merchants);
    }


    public void sendtosever() {

        Cursor curs;
        db = getApplicationContext().openOrCreateDatabase("freshorders", 0, null);
        curs = databaseHandler.getclientvisit();
        Log.e("count", String.valueOf(curs.getCount()));

        if (curs != null && curs.getCount() > 0) {
            curs.moveToLast();
            final JSONArray resultDetail = new JSONArray();


            int totalColumn = curs.getColumnCount();
            final JSONObject rowObject = new JSONObject();

            for (int i = 0; i < totalColumn; i++) {
                if (curs.getColumnName(i) != null) {

                    try {

                        if (curs.getString(i) != null) {
                            Log.d("TAG_NAME", curs.getString(i));
                            rowObject.put(curs.getColumnName(i), curs.getString(i));
                        } else {
                            rowObject.put(curs.getColumnName(i), "");
                        }


                    } catch (Exception e) {
                        Log.d("TAG_NAME", e.getMessage());
                    }
                }


            }
            Log.e("sendordarray", rowObject.toString());
            curs.close();

            //pushing values to mstr DB
            new AsyncTask<Void, Void, Void>() {
                ProgressDialog dialog;
                String strStatus = "";
                String strMsg = "";

                @Override
                protected void onPreExecute() {
                    dialog = ProgressDialog.show(ClientVisitActivity.this, "",
                            "Loading...", true, false);
                }

                @Override
                protected void onPostExecute(Void result) {
                    try {
                        if(dialog != null){
                            dialog.dismiss();
                        }
                        if(JsonAccountObject != null) {
                            strStatus = JsonAccountObject.getString("status");
                            Log.e("return status", strStatus);
                            strMsg = JsonAccountObject.getString("message");
                            Log.e("return message", strMsg);


                            if (strStatus.equals("true")) {
                                //showAlertDialogToast1(strMsg);
                                db.execSQL("UPDATE clientvisit SET clientstatus = 'Success' WHERE id =" + rowid);
                                Cursor cur;
                                db = getApplicationContext().openOrCreateDatabase("freshorders", 0, null);
                                cur = db.rawQuery("SELECT * FROM clientvisit  WHERE  clientstatus = 'Success'", null);
                                Log.e("HeaderCountcompleted", String.valueOf(cur.getCount()));
                                showAlertSuccess("Client visit added successfully ");

                            }
                        }else {
                            showAlertDialogToast("No value from server");
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        showAlertDialogToast("Some think went wrong");
                    }
                }

                @Override
                protected Void doInBackground(Void... params) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        rowid = rowObject.getString("id");
                        Log.e("id", rowid);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        return null;
                    }

                    JsonServiceHandler = new JsonServiceHandler(Utils.strclientvisitsales, rowObject.toString(),
                            ClientVisitActivity.this);
                    JsonAccountObject = JsonServiceHandler.ServiceData();
                    return null;

                }
            }.execute(new Void[]{});


        }
    }


    private void getData() {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "";

            @Override
            protected void onPreExecute() {
                dialog = ProgressDialog.show(ClientVisitActivity.this, "",
                        "Loading...", true, true);

            }

            @Override
            protected void onPostExecute(Void result) {

                try {

                    PaymentStatus = (JsonAccountObject.getString("pymtstatus"));
                    Log.e("PaymentStatus", PaymentStatus);

                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment to place order");
                    } else {
                        if (moveto.equals("createorder")) {
                            Intent io = new Intent(ClientVisitActivity.this,
                                    CreateOrderActivity.class);
                            startActivity(io);
                            finish();
                        } else if (moveto.equals("myorder")) {
                            Intent io = new Intent(ClientVisitActivity.this,
                                    SalesManOrderActivity.class);
                            startActivity(io);
                            finish();

                        } else if (moveto.equals("product")) {
                            Intent io = new Intent(ClientVisitActivity.this, ProductActivity.class);
                            startActivity(io);
                            finish();
                        } else if (moveto.equals("mydealer")) {
                            Intent io = new Intent(ClientVisitActivity.this, MyDealersActivity.class);
                            io.putExtra("Key", "MenuClick");
                            startActivity(io);
                            finish();
                        }
                    }

                    dialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();


                JsonServiceHandler = new JsonServiceHandler(Utils.strGetProfileDetail + Constants.USER_ID, ClientVisitActivity.this);
                JsonAccountObject = JsonServiceHandler.ServiceDataGet();
                return null;
            }
        }.execute(new Void[]{});
    }

    public void offlineBeat() {
        Cursor curs;
        curs = databaseHandler.getbeatwithMerchant(dayOfTheWeek);
        Log.e("beatCount", String.valueOf(curs.getCount()));

        int i = 0;

        if (curs != null && curs.getCount() > 0) {
            if (curs.moveToFirst()) {

                do {
                    Log.e("beat inside", "beat inside");
                    if (i == 0) {
                        if (curs.getString(0).equals("New User")) {
                            beatMerchants = "'" + curs.getString(0) + "'";
                        } else {
                            beatMerchants = curs.getString(0);
                        }


                        i++;
                    } else {
                        if (curs.getString(0).equals("New User")) {
                            beatMerchants = beatMerchants + "," + "'" + curs.getString(0) + "'";
                        } else {
                            beatMerchants = beatMerchants + "," + curs.getString(0);
                        }


                    }

                    Log.e("beatMerchants", beatMerchants);


                } while (curs.moveToNext());


            }
            loadingMerchant(beatMerchants);
        } else {
            showAlertDialogToast("No Merchants available for this day");
        }

    }


    private void loadingMerchant(String beatMerchants) {


        Cursor curs;
        curs = databaseHandler.getmerchantoncurrentday(beatMerchants);
        Log.e("merchantCount", String.valueOf(curs.getCount()));

        if (curs != null && curs.getCount() > 0) {
            if (curs.moveToFirst()) {

                do {
                    Log.e("inside", "inside");
                    MDealerCompDropdownDomain cd = new MDealerCompDropdownDomain();
                    cd.setComName(curs.getString(1));
                    cd.setID(String.valueOf(curs.getString(2)));
                    cd.setName(curs.getString(3));
                    cd.setMerchantRowId(curs.getInt(0));//Kumaravel for new merchant
                    Log.e("companyname", curs.getString(1));
                    Log.e("mid", curs.getString(2));
                    Log.e("fullname", curs.getString(3));
                    arrayListDealerDomain.add(cd);
                    arrayListSearch.add(cd);
                    arrayListDealer.add(curs.getString(1));
                    arrayListmerchant.add(cd);

                } while (curs.moveToNext());
            }
            Log.e("outside", "outside");
        } else {

        }
        Log.e("size", String.valueOf(arrayListDealerDomain.size()));

        Log.e("size of array", String.valueOf(arrayListDealer.size()));
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                ClientVisitActivity.this, R.layout.textview, R.id.textView,
                arrayListDealer);
        autocompletemrchantlist.setAdapter(adapter);

       /* if(!Constants.clientvisitmerchname.equals("")){
            autocompletemrchantlist.setText(Constants.clientvisitmerchname);
            clientvisitmerchantselected=1;
        }*/
        Log.e("arraylist", Constants.clientvisitmerchname);
    }


    private void Export() {
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "";

            @Override
            protected void onPreExecute() {
                dialog = ProgressDialog.show(ClientVisitActivity.this, "In Progress",
                        "Validating...", true, true);
            }

            @Override
            protected void onPostExecute(Void result) {
                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {

                    } else {

                        showAlertDialogToast(strMsg);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Log.e("loginActivityException", e.toString());
                }

                dialog.dismiss();
            }

            @Override
            protected Void doInBackground(Void... params) {
                dialog.show();

                JSONObject jsonObject = new JSONObject();
                try {

                    jsonObject.put("userid", merchantlist);
                    jsonObject.put("usertype", date);
                    jsonObject.put("fromdt", discription);
                    jsonObject.put("userid", Constants.USER_ID);


                    jsonObject.put("userid", Constants.USER_ID);
                    jsonObject.put("userid", Constants.USER_ID);

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                JsonServiceHandler = new JsonServiceHandler(
                        Utils.strExport, jsonObject.toString(),
                        ClientVisitActivity.this);
                JsonAccountObject = JsonServiceHandler.ServiceData();
                return null;
            }
        }.execute(new Void[]{});

    }

    @TargetApi(23)
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permission, @NonNull int[] grantResults) {

        Log.e("requestcode", String.valueOf(requestCode));
        Log.e("size", String.valueOf(grantResults.length));

        if (requestCode == Constants.LOCATION_PERMISSION_REQUEST_CODE) {
            Log.e("requestcode", String.valueOf(grantResults[0]));
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent io = new Intent(ClientVisitActivity.this, ClientVisitActivity.class);
                startActivity(io);
                finish();
            } else {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.LOCATION_PERMISSION_REQUEST_CODE);
            }


        }
    }


    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ClientVisitActivity.this);

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.delete);

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, 2);

            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("inside", String.valueOf(resultCode));
        if (resultCode == 0) {
            switch (requestCode) {
                case 1:
                    Log.e("inside", "inside");
                    Intent io = new Intent(ClientVisitActivity.this, ClientVisitActivity.class);
                    startActivity(io);
                    finish();
                    break;
            }
        }

        if (requestCode == 1) {
            Bundle extras = data.getExtras();
            clientvisitgeolat = extras.getDouble("Latitude");
            clientvisitgeolog = extras.getDouble("Longitude");


        }

        if (requestCode == 2) {
            Intent intent = new Intent(this, GPSTrackerActivity.class);
            startActivityForResult(intent, 1);
        }

    }

    public boolean netCheckWithoutAlert() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            } else if (result == null) {

                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public void showAlertDialog(Context context, String title, String message,
                                Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        alertDialog.show();
    }

    public boolean netCheck1() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            } else if (result == null) {

                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public boolean netCheck() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            } else if (result == null) {

                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public void showAlertSuccess(String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setCancelable(false);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        muserId = "";
                        EditTextViewDescription.setText("");
                        autocompletemrchantlist.setText("");

                        dialog.cancel();
                        Intent to = new Intent(ClientVisitActivity.this, SMClientVisitHistory.class);
                        startActivity(to);
                        finish();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    public void showAlertDialogToast(String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    public void showAlertDialogToast1(String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        muserId = "";
                        EditTextViewDescription.setText("");
                        autocompletemrchantlist.setText("");
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    public void onBackPressed() {
        exitAlret();
    }

    private void exitAlret() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        localBuilder.setMessage("Do you want to Exit?");
        localBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {

                        finish();

                    }
                });
        localBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {
                        paramDialogInterface.cancel();
                    }
                });
        localBuilder.create().show();
    }

    private boolean CheckGpsStatus(){
        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );
        return manager != null && manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    // Trigger new location updates at interval
    @SuppressLint("MissingPermission")
    protected void startLocationUpdates() {

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations, this can be null.
                        if (location != null) {
                            updateCurrentLocation(location);
                            mLastLocation = location;
                            initialLocationTime = location.getTime();
                        }
                    }
                });
        try {
            locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        //    mLastLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            passive_enabled = locationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER);

            if (gps_enabled) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0,
                        locationListenerGps);
            }

            if (network_enabled) {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 0,
                        locationListenerNetwork);
            }
            if (passive_enabled) {
                locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 5000, 0,
                        locationListenerPassive);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    LocationListener locationListenerNetwork = new LocationListener() {
        public void onLocationChanged(Location location) {
            if (location != null) {
                mLastLocation = location;
                updateCurrentLocation(location);
            }
        }
        public void onProviderDisabled(String provider) { }
        public void onProviderEnabled(String provider) { }
        public void onStatusChanged(String provider, int status, Bundle extras) { }
    };

    LocationListener locationListenerGps = new LocationListener() {
        public void onLocationChanged(Location location) {
            if (location != null) {
                mLastLocation = location;
                updateCurrentLocation(location);
            }
        }
        public void onProviderDisabled(String provider) { }
        public void onProviderEnabled(String provider) { }
        public void onStatusChanged(String provider, int status, Bundle extras) { }
    };


    LocationListener locationListenerPassive = new LocationListener() {
        public void onLocationChanged(Location location) {
            if (location != null) {
                mLastLocation = location;
                updateCurrentLocation(location);
            }
        }
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) { }
        @Override
        public void onProviderEnabled(String provider) { }
        @Override
        public void onProviderDisabled(String provider) { }

    };

    private void showD(String msg){
        if(pDialog == null){
            pDialog = ProgressDialog.show(ClientVisitActivity.this, "", msg, true, false);
        }else {
            if (!pDialog.isShowing()) {
                pDialog = ProgressDialog.show(ClientVisitActivity.this, "", msg, true, false);
            }
        }
    }

    private void hideD(){
        if(pDialog != null){
            if(pDialog.isShowing()){
                pDialog.dismiss();
            }
        }
    }

    private void verifyLocationUpdatedForClientVisit() {
        if (clientvisitgeolat > 0 && clientvisitgeolog > 0) {
            Log.e(TAG, "In Complete................." + mLastLocation);
            if (mLastLocation != null) {
                updateCurrentLocation(mLastLocation);
            }
            processClientVisitData();
        } else {
            showD(Constants.GPS_LOCATION_FETCHING);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    updateDelayLocation(mLastLocation);
                    processClientVisitData();
                }
            }, 7000);
        }
    }

    private void updateDelayLocation(Location mLastLocation){
        if(mLastLocation != null) {
            clientvisitgeolat = mLastLocation.getLatitude();
            clientvisitgeolog = mLastLocation.getLongitude();
            hideD();
        }else {
            updateLocationByDelay();
        }
    }

    synchronized private void updateCurrentLocation(Location mLastLocation){
        if(mLastLocation != null) {
            clientvisitgeolat = mLastLocation.getLatitude();
            clientvisitgeolog = mLastLocation.getLongitude();
        }
    }

    private void updateLocationByDelay(){
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        hideD();
                        updateCurrentLocation(location);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        hideD();
                        showAlertDialogToast("Please ensure your mobile GPS enabled or working properly");
                    }
                });
    }

        /*
        try {
            mFusedLocationClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                    if (task.isSuccessful() && task.getResult() != null) {
                        Log.e(TAG, "In Complete.....");
                        Location mLastLocation = task.getResult();
                        updateCurrentLocation(mLastLocation);
                        if(initialLocationTime == mLastLocation.getTime()){
                            Log.e(TAG, "In Complete.....Time Matching");
                            try {
                                Thread.sleep(3000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            verifyLocationUpdatedForClientVisit();
                        }else {
                            hideD();
                            processClientVisitData();
                        }
                    } else {
                        hideD();
                        Log.e(TAG, "Failed to get location.");
                        Toast.makeText(ClientVisitActivity.this, "Failed to get location. Re-Try", Toast.LENGTH_LONG).show();
                    }
                }
            });
        } catch (SecurityException unlikely) {
            Log.e(TAG, "Lost location permission." + unlikely);
        }  */

}
