package com.freshorders.freshorder.ui;

import java.util.ArrayList;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;

import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.GridAdapter;
import com.freshorders.freshorder.domain.GalleryDomain;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class GalleryActivity extends Activity {
	
	ViewPager pager;
	JSONObject JsonAccountObject;
	JSONObject JsonObject;
	JSONArray JsonAccountArray;
    public JsonServiceHandler JsonServiceHandler;
    public  ArrayList<GalleryDomain> arraylistPdoductImages;
   String images[];
    LinearLayout linearLayoutViewPager,linearLayoutButton;
	TextView textViewBack,textViewGalleryHeading;
	public  Context context;
	
	public  Integer gridposition;
	  public  String time;
	public  GalleryDomain gposition;
	GridView gridView;
	ArrayList<String> gridArray = new ArrayList<String>();
	 GridAdapter pa;

	
	 LinearLayout linearlayoutAboutUs,linearlayoutClients,linearlayoutWarranty,
	 linearlayoutProduct,linearlayoutGallery,linearlayoutNews,linearlayoutCareers,linearLayoutContactUs,
	 linearLayoutHome,linearLayoutHomeImages,linearLayoutCredentialImages,linearLayoutBack,
	 linearLayoutAboutUsLeft,linearLayoutClientsLeft,linearLayoutProductLeft,linearLayoutNewsLeft,
	 linearLayoutWarrantyLeft,linearLayoutGalleryLeft,linearLayoutCareersLeft,linearLayoutContactUsLeft,linearLayoutHomeBack;
	 
	 Date dateStr = null;
	 RelativeLayout relativeLayoutMenu,relativeLayoutHome;
	 
	 TextView textViewHomeIcon,textViewClientsIcon,textViewProductIcon,textViewNewsIcon,textViewWarrantyIcon,
	 textViewGalleryIcon,textViewCareerIcon,textViewContactIcon,textViewAboutIcon,textViewCreDentialHeading,
	 textViewDashboardHeading,textViewCredential,textViewHome,textViewAboutUs,
	 textViewClient,textViewProduct,textViewNews,textViewWarranty,textViewGallery,textViewCareers,textViewContact;
	 
	 public static int menuState = 1;
	  Animation animOpen, animClose;
	  
	  
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.viewpager_gallery);
		netCheck();
		context = GalleryActivity.this;
		//pager=(ViewPager)findViewById(R.id.pager);
		linearLayoutViewPager= (LinearLayout) findViewById(R.id.linearLayoutViewPager);
		linearLayoutButton= (LinearLayout) findViewById(R.id.linearLayoutButton);
		 linearLayoutBack = (LinearLayout) findViewById(R.id.linearLayoutBack);
		 gridView = (GridView) findViewById(R.id.gridView1);
			

	        Typeface font = Typeface.createFromAsset( getAssets(), "fontawesome-webfont.ttf" );
	        textViewBack= (TextView) findViewById(R.id.textViewBack);
	        textViewGalleryHeading= (TextView) findViewById(R.id.textViewGalleryHeading);

	        textViewBack.setTypeface(font);
	
	        linearlayoutAboutUs= (LinearLayout) findViewById(R.id.linearlayoutAboutUs);
			 linearlayoutClients= (LinearLayout) findViewById(R.id.linearlayoutClients);
			 linearlayoutWarranty= (LinearLayout) findViewById(R.id.linearlayoutWarranty);
			 linearlayoutProduct= (LinearLayout) findViewById(R.id.linearlayoutProduct);
			 linearlayoutGallery= (LinearLayout) findViewById(R.id.linearlayoutGallery);
			 linearlayoutNews= (LinearLayout) findViewById(R.id.linearlayoutNews);
			 linearlayoutCareers= (LinearLayout) findViewById(R.id.linearlayoutCareers);
			 linearLayoutContactUs= (LinearLayout) findViewById(R.id.linearLayoutContactUs);
			 linearLayoutHome= (LinearLayout) findViewById(R.id.linearLayoutHome);
			 
			 linearLayoutAboutUsLeft= (LinearLayout) findViewById(R.id.linearLayoutAboutUsLeft);
			 linearLayoutClientsLeft= (LinearLayout) findViewById(R.id.linearLayoutClientsLeft);
			 linearLayoutProductLeft= (LinearLayout) findViewById(R.id.linearLayoutProductLeft);
			 linearLayoutNewsLeft= (LinearLayout) findViewById(R.id.linearLayoutNewsLeft);
			 linearLayoutWarrantyLeft= (LinearLayout) findViewById(R.id.linearLayoutWarrantyLeft); 
			 linearLayoutGalleryLeft= (LinearLayout) findViewById(R.id.linearLayoutGalleryLeft);
			 linearLayoutCareersLeft= (LinearLayout) findViewById(R.id.linearLayoutCareersLeft);
			 linearLayoutContactUsLeft= (LinearLayout) findViewById(R.id.linearLayoutContactUsLeft);
			 linearLayoutHomeBack= (LinearLayout) findViewById(R.id.linearLayoutHomeBack);
			 
			 relativeLayoutMenu= (RelativeLayout) findViewById(R.id.relativeLayoutMenu);
			 relativeLayoutHome= (RelativeLayout) findViewById(R.id.relativeLayoutHome);
			 
			 textViewHomeIcon = (TextView) findViewById(R.id.textViewHomeIcon);
			 textViewClientsIcon= (TextView) findViewById(R.id.textViewClientsIcon);
			 textViewProductIcon= (TextView) findViewById(R.id.textViewProductIcon);
			 textViewNewsIcon= (TextView) findViewById(R.id.textViewNewsIcon);
			 textViewWarrantyIcon= (TextView) findViewById(R.id.textViewWarrantyIcon);
			 textViewGalleryIcon= (TextView) findViewById(R.id.textViewGalleryIcon);
			 textViewCareerIcon= (TextView) findViewById(R.id.textViewCareerIcon);
			 textViewContactIcon= (TextView) findViewById(R.id.textViewContactIcon);
			 textViewAboutIcon= (TextView) findViewById(R.id.textViewAboutIcon);
			  textViewHome= (TextView) findViewById(R.id.textViewHome);
			  textViewAboutUs= (TextView) findViewById(R.id.textViewAboutUs);
			  textViewClient= (TextView) findViewById(R.id.textViewClient);
			  textViewProduct= (TextView) findViewById(R.id.textViewProduct);
			  textViewNews= (TextView) findViewById(R.id.textViewNews);
			  textViewWarranty= (TextView) findViewById(R.id.textViewWarranty);
			  textViewGallery= (TextView) findViewById(R.id.textViewGallery);
			  textViewCareers= (TextView) findViewById(R.id.textViewCareers);
			  textViewContact= (TextView) findViewById(R.id.textViewContact);
			  


	        
		JsonObject = new JSONObject();
		JsonAccountArray = new JSONArray();
		
		Gallery();
		
		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				/*gridposition = position;
				Intent intent = new Intent(GalleryActivity.this, GalleryDetailActivity.class);
				intent.putExtra("gridposition",String.valueOf(gridposition));
				intent.putExtra("count",String.valueOf(arraylistPdoductImages.size()));
				startActivity(intent);
				GalleryActivity.this.finish();*/
			}
		});
		

	}
	
	 


	  private void closeAnim() {
	        relativeLayoutMenu.startAnimation(animClose);
	        menuState = 1;
	        animClose.setAnimationListener(new Animation.AnimationListener() {

	            @Override
	            public void onAnimationStart(Animation animation) {
	                // TODO Auto-generated method stub
	                runOnUiThread(new Runnable() {

	                    @Override
	                    public void run() {
	                        Color Color = null;
	                       // linearlayoutMenuSpace.setBackgroundColor(Color.parseColor("#ffffff"));
	                    }

	                });
	            }

	            @Override
	            public void onAnimationRepeat(Animation animation) {
	                // TODO Auto-generated method stub

	            }

	            @Override
	            public void onAnimationEnd(Animation animation) {
	                runOnUiThread(new Runnable() {

	                    @Override
	                    public void run() {
	                        relativeLayoutMenu.setVisibility(View.GONE);
	                        linearLayoutHomeBack.setAlpha(1F);
	                    }

	                });


	            }
	        });
	    }
	  
	  private void openAnim() {
	      
	            
	        relativeLayoutMenu.setVisibility(View.VISIBLE);
	        relativeLayoutMenu.startAnimation(animOpen);
	        animOpen.setAnimationListener(new Animation.AnimationListener() {
	            @Override
	            public void onAnimationStart(Animation animation) {

	            }

	            @Override
	            public void onAnimationEnd(Animation animation) {
	                runOnUiThread(new Runnable() {

	                    @Override
	                    public void run() {
	                       // linearlayoutMenuSpace.setBackgroundColor(Color.parseColor("#ffffff"));
	                    	linearLayoutHomeBack.setAlpha(0.5F);
	                    }

	                });
	            }

	            @Override
	            public void onAnimationRepeat(Animation animation) {

	            }
	        });
	        menuState = 0;

	    }
	  
	public void Gallery() {
		// TODO Auto-generated method stub
new AsyncTask<Void, Void, Void>() {
	ProgressDialog dialog;
	String strStatus = "";
	String strMsg = "";

	@Override
	protected void onPreExecute() {
		dialog= ProgressDialog.show(GalleryActivity.this, "Loaging Image",
				"Loading...", true, true);

	}

			@Override
			protected void onPostExecute(Void result) {
				
				try {
					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					
					if (strStatus.equals("true")) {
					JSONArray jobArray =JsonAccountObject.getJSONArray("data"); 
					arraylistPdoductImages=new ArrayList<GalleryDomain>();
					for (  int i = 0; i < jobArray.length(); i++) {
						JSONObject job=new JSONObject();
						job=jobArray.getJSONObject(i);
						GalleryDomain apd=new GalleryDomain();
						try {
						
						/*apd.setNewsId((job.getString("news_id")));
						apd.setGalleryTitle((job.getString("news_title")));
						apd.setGalleryContent((job.getString("news_desc")));
						apd.setStatus((job.getString("status")));
						time =job.getString("last_updated_dt");*/
						
						/*String[] date=job.getString("last_updated_dt").split("T");
						for (int k = 0; k < date.length; k++) {
							
							
							 String inputPattern = "yyyy-MM-dd";
							    String outputPattern = "dd-MMM-yyyy";
							    SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
							    SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

							  
							    String str = null;

							    try {
							    	dateStr = inputFormat.parse(date[k]);
							        str = outputFormat.format(dateStr);
							        apd.setUpdatedDate(str);
							        Log.e("str", str);
							    } catch (ParseException e) {
							        e.printStackTrace();
							    }
							
						         		break;
					         
								}*/
						//apd.setUpdateBy((job.getString("last_updated_by")));
						
						try {
							 images=job.getString("ordimage").split(",");
								
								for(int j=0; j<images.length; j++){
									images[j]=Utils.strImageUrl+images[j];
									images[j] =images[j].replaceAll(" ", "%20");
									Log.e("imgPath",images[j]);
									apd.setImages(images[j]);
									arraylistPdoductImages.add(apd);
								}
						} catch (Exception e) {
							// TODO: handle exception
						}
					
						
						

						
						} catch (Exception e) {
							// TODO: handle exception
						}
						
						
					
					
					}
					
					if(strStatus.equals("true")){
						
						
						linearLayoutViewPager.setVisibility(LinearLayout.VISIBLE);
						linearLayoutButton.setVisibility(LinearLayout.GONE);
						pa=new GridAdapter(GalleryActivity.this, R.layout.gallery_item_screen, arraylistPdoductImages);
						gridView.setAdapter(pa);

						
					}else{
					linearLayoutViewPager.setVisibility(LinearLayout.GONE);
					linearLayoutButton.setVisibility(LinearLayout.VISIBLE);
					}
					
					Log.e("arraylistPdoductImages",String.valueOf(arraylistPdoductImages.size()));
					}
					
				} catch (JSONException e) {
					linearLayoutViewPager.setVisibility(LinearLayout.GONE);
					linearLayoutButton.setVisibility(LinearLayout.VISIBLE);
					e.printStackTrace();
				} catch (Exception e) {
					Log.e("listException",e.toString());
				}
		
				
				dialog.dismiss();
			}

			@Override
			protected Void doInBackground(Void... params) {
				JSONObject jsonObject = new JSONObject();


				JsonServiceHandler = new JsonServiceHandler(Utils.strMerSingleOderdetail+MerchantOrderActivity.orderId+Utils.strMerSingleOderdetailAdditon+MerchantOrderActivity.duserid, GalleryActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceDataGet();
				return null;
			}
		}.execute(new Void[] {});
	}

	@SuppressWarnings("unused")
	public boolean netCheck() {
		// for network connection
		try {
			ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetwork = connectionManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			Object result = null;
			if (mWifi.isConnected() || mNetwork.isConnected()) {
				return true;
			}

			else if (result == null) {
				showAlertDialog(GalleryActivity.this, "No Internet Connection",
						"Please Check Your internet connection.", false);
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	// for network connection messg
		public void showAlertDialog(Context context, String title, String message,
				Boolean status) {
			AlertDialog alertDialog = new AlertDialog.Builder(context).create();
			alertDialog.setTitle(title);
			alertDialog.setMessage(message);
			alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
			alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
						}
					});
			alertDialog.show();
		}

		public void onBackPressed() {
			exitAlret();
		}


	    private void exitAlret() {
	        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
	        localBuilder.setCancelable(false);
	        localBuilder.setMessage("Do you want to Exit?");
	        localBuilder.setPositiveButton("Yes",
	                new DialogInterface.OnClickListener() {
	                    public void onClick(DialogInterface paramDialogInterface,
	                                        int paramInt) {

	                        finish();

	                    }
	                });
	        localBuilder.setNegativeButton("No",
	                new DialogInterface.OnClickListener() {
	                    public void onClick(DialogInterface paramDialogInterface,
	                                        int paramInt) {
	                        paramDialogInterface.cancel();
	                    }
	                });
	        localBuilder.create().show();
	    }

}
