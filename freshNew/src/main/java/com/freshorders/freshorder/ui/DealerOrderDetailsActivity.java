package com.freshorders.freshorder.ui;

import java.util.Arrays;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.DealerImageAdapter;
import com.freshorders.freshorder.adapter.DealerOrderDetailAdapter;
import com.freshorders.freshorder.domain.DealerAddProductDomain;
import com.freshorders.freshorder.domain.DealerOrderDetailDomain;
import com.freshorders.freshorder.domain.ProductUomDomain;
import com.freshorders.freshorder.domain.ViewPagerDomain;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class DealerOrderDetailsActivity extends Activity {
	
	TextView textViewName,textViewAddress,textViewDate,menuIcon,textViewdelord;
	LinearLayout linearLayoutBack,linearLayoutButton,linearLayoutHeading;
	JSONObject JsonAccountObject = null;
	JSONArray JsonAccountArray = null;
	JsonServiceHandler JsonServiceHandler ;

	public static ArrayList<DealerAddProductDomain> arraylistMProductListSelected;
	public static ArrayList<ProductUomDomain> arraylistproductUOM;
	//public static  ArrayList<ViewPagerDomain> arraylistPdoductImages;
	ListView listViewOrders;
	  Date dateStr = null;
	  public static String time;
	  Button buttonUpdate,buttonAddNewProducts,buttonUpdateImage;
	  public static String[] orderdtid, orderid,prodid,qty,date,status,freeQty,flag,paymentType,qtysplit,prodImage
			  ,removedimage,read,
	deliverydate,deliverytime,deliverymode,freeqtyUomUnit,price,ptax,unitgram,qtyUomUnit;
	public static int ProdImageCount,imageset=0;
	GridView gridview;
	Gallery galleryViewcamera;
	public static String[] mThumbIds=null,image=null,imagesetarray=null;
	public static List<String> list;
	public static int imageTotal;
	public   String images="";
	LinearLayout layoutgalleryViewcamera;
	public static Double approximateTotal=0.0,pval=0.0,defaultuomTotal=0.0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(1);
		setContentView(R.layout.dealer_new_single_screen);
		
		netCheck();
		textViewName = (TextView) findViewById(R.id.textViewName);
		textViewAddress = (TextView) findViewById(R.id.textViewAddress);
		textViewDate = (TextView) findViewById(R.id.textViewDate);
		textViewdelord= (TextView) findViewById(R.id.textViewdelord);
		menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
		listViewOrders = (ListView) findViewById(R.id.listViewOrders);
		buttonUpdate = (Button) findViewById(R.id.buttonUpdate);
		buttonAddNewProducts=(Button) findViewById(R.id.buttonAddNewProducts);
		linearLayoutBack = (LinearLayout) findViewById(R.id.linearLayoutBack);
		linearLayoutButton= (LinearLayout) findViewById(R.id.linearLayoutButton);
		linearLayoutHeading= (LinearLayout) findViewById(R.id.linearLayoutHeading);
		layoutgalleryViewcamera= (LinearLayout) findViewById(R.id.layoutgalleryViewcamera);
		buttonUpdateImage= (Button) findViewById(R.id.buttonUpdateImage);
		galleryViewcamera= (Gallery) findViewById(R.id.galleryViewcamera);
		galleryViewcamera.setSelection(1);
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);

		ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) galleryViewcamera.getLayoutParams();
		mlp.setMargins(-500, 0, 0, 0);
		galleryViewcamera.setSpacing(15);

		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();
		gridview = (GridView) findViewById(R.id.gridview);
		arraylistMProductListSelected=new ArrayList<DealerAddProductDomain>();
		arraylistproductUOM=new ArrayList<ProductUomDomain>();
		Typeface font = Typeface.createFromAsset(getAssets(),
				"fontawesome-webfont.ttf");
		menuIcon.setTypeface(font);
		textViewdelord.setTypeface(font);
		textViewName.setText(DealersOrderActivity.fullname);
		textViewAddress.setText("Order Id : " + DealersOrderActivity.orderId+",");

		textViewDate.setText(DealersOrderActivity.date);

		if(Constants.adapterset==1){
			Log.e("size",String.valueOf(DealersOrderActivity.arraylistDealerOrderDetailList.size()));
			mThumbIds = list.toArray(new String[0]);
			setViewPagerAdapter();
		}else{
			DealersOrderActivity.arraylistDealerOrderDetailList.clear();
			myOrders();
		}

		linearLayoutBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Constants.adapterset=0;
				Intent to = new Intent(DealerOrderDetailsActivity.this, DealersOrderActivity.class);
				startActivity(to);
				finish();
			}
		});
		
		buttonUpdate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				saveOrder();
				
			}
		});
		buttonAddNewProducts.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Constants.orderid=DealersOrderActivity.orderId;
				Constants.checkproduct=0;
				Intent io=new Intent(DealerOrderDetailsActivity.this,DealerAddNewProduct.class);
				startActivity(io);
				finish();
			}
		});
		buttonUpdateImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				for(int i=0;i<list.size();i++){
					String [] image=list.get(i).split("/");
					Log.e("list",image[image.length-1]);
					if(i==0){
						images=images+image[image.length-1];
					}else{
						images=images+","+image[image.length-1];
					}

				}

				updateImage();

			}
		});
		galleryViewcamera.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int posit, long l) {
				Intent i0 = new Intent(getApplicationContext(), SM_FullGalleryView.class);
				i0.putExtra("id", posit);
				startActivity(i0);
				DealerOrderDetailsActivity.this.finish();
			}
		});
		gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				Intent i = new Intent(getApplicationContext(), DealerFullImage.class);
				i.putExtra("id", position);
				startActivity(i);
				DealerOrderDetailsActivity.this.finish();
			}
		});
		textViewdelord.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				showAlertDialogToastdelorder();
			}
		});
	}
	public void deleteorders() {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog1;
			String strStatus = "";
			String strMsg = "";
			@Override
			protected void onPreExecute() {
				dialog1 = ProgressDialog.show(DealerOrderDetailsActivity.this, "",
						"Loading...", true, true);
				dialog1.setCancelable(false);
			}

			@Override
			protected void onPostExecute(Void result) {

				try{

					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);

					if (strStatus.equals("true")) {
						Constants.adapterset=0;
						showAlertDialogToast1(strMsg);
					}else{
						showAlertDialogToast1("Not Deleted");

					}
					dialog1.dismiss();
				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {

				}

			}

			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();


				try {
					jsonObject.put("ordstatus","Cancelled");
				} catch (JSONException e) {
					e.printStackTrace();
				}


				JsonServiceHandler = new JsonServiceHandler(
						Utils.strdeleteorder +DealersOrderActivity.orderId+"&[where][duserid]="+Constants.USER_ID ,jsonObject.toString(),
						DealerOrderDetailsActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceData();
				return null;
			}
		}.execute(new Void[] {});
	}
	public void updateImage(){
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog= ProgressDialog.show(DealerOrderDetailsActivity.this, "",
						"Loading...", true, true);
			}

			@Override
			protected void onPostExecute(Void result) {

				try {

					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);
					showAlertDialogToastupdateimage(strMsg);

				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {

				}
				dialog.dismiss();
			}

			@Override
			protected Void doInBackground(Void... params) {
				JSONObject jsonObject = new JSONObject();
				try {
					JSONArray jsonArray = new JSONArray();
					JSONArray jsonArray1 = new JSONArray();
					JSONObject jsonObject1,jsonObject2;
					//mThumbIds= new String[MerchantOrderActivity.arraylistPdoductImages.size()];
					orderdtid = new String[DealersOrderActivity.arraylistDealerOrderDetailList.size()];
					orderid = new String[DealersOrderActivity.arraylistDealerOrderDetailList.size()];
					removedimage= new String[DealersOrderActivity.removedImage.size()];

					for (int i = 0; i < DealersOrderActivity.arraylistDealerOrderDetailList.size(); i++) {
						orderdtid[i] =DealersOrderActivity.arraylistDealerOrderDetailList.get(i).getorddtlid();
						orderid[i] =  DealersOrderActivity.arraylistDealerOrderDetailList.get(i).getOrderid();

					}

					for(int j=0;j< DealersOrderActivity.removedImage.size();j++){
						removedimage[j] =DealersOrderActivity.removedImage.get(j);
					}

					for (int i = 0; i < DealersOrderActivity.arraylistDealerOrderDetailList.size(); i++) {
						jsonObject1 = new JSONObject();
						jsonObject2 = new JSONObject();
						try {
							jsonObject1.put("orddtlid", orderdtid[i]);
							jsonObject1.put("ordid", orderid[i]);
							jsonObject1.put("ordimage", images);
							jsonObject1.put("prodid", "0");
							Log.e("ordimage", images);


						/*jsonObject1.put("deletelist", MerchantOrderActivity.removedImage);
						Log.e("deletelist",  MerchantOrderActivity.removedImage.toString());*/

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						jsonArray.put(jsonObject1);
					}


					for(int q=0;q<DealersOrderActivity.removedImage.size();q++){
						jsonArray1 .put(DealersOrderActivity.removedImage.get(q));
					}

					jsonObject.put("orderdtls", jsonArray);
					jsonObject.put("deletelist", jsonArray1);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				JsonServiceHandler = new JsonServiceHandler(Utils.strsavedealerOderdetail,jsonObject.toString(), DealerOrderDetailsActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceData();
				return null;
			}
		}.execute(new Void[] {});
	}

	private void myOrders() {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog= ProgressDialog.show(DealerOrderDetailsActivity.this, "Loading",
						"Loading...", true, true);

			}

			@Override
			protected void onPostExecute(Void result) {

				try {

					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);

					if (strStatus.equals("true")) {
						JSONArray job1 = JsonAccountObject.getJSONArray("data");
						imagesetarray = new String[0];
						DealersOrderActivity.arraylistimage.clear();
						approximateTotal=0.0;
						for (int i = 0; i < job1.length(); i++) {
							JSONObject job = new JSONObject();
							job = job1.getJSONObject(i);
							DealerOrderDetailDomain dod = new DealerOrderDetailDomain();
							ViewPagerDomain vP = new ViewPagerDomain();
							Constants.orderid = job.getString("ordid");
							dod.setorddtlid(job.getString("orddtlid"));
							dod.setprodid(job.getString("prodid"));

							if(job.getString("ordimage").equals("null")) {

								if(imageset==0) {
									dod.setordimage("0");
									imagesetarray = new String[0];
								}

							}else{

								imagesetarray=job.getString("ordimage").split(",");
								for (int k = 0; k < imagesetarray.length; k++) {
									imagesetarray[k] = Utils.strImageUrl + imagesetarray[k];
									dod.setordimage(imagesetarray[k]);
									DealersOrderActivity.arraylistimage.add(dod);
								}
								imageset=1;
							}
                            read=job.getString("isread").split(",");
						/*	String[] date1 = job.getString("deliverydt").split("T");
							time = date1[0];

							String inputPattern = "yyyy-MM-dd";
							String outputPattern = "dd-MMM-yyyy";
							SimpleDateFormat inputFormat = new SimpleDateFormat(
									inputPattern);
							SimpleDateFormat outputFormat = new SimpleDateFormat(
									outputPattern);

							String str = null;
*/
							try {
								/*dateStr = inputFormat.parse(time);
								str = outputFormat.format(dateStr);
								dod.setDate(str);*/

							if (job.getString("prodid").equals("0")) {

								mThumbIds = job.getString("ordimage").split(",");
								imageTotal = mThumbIds.length;

								for (int k = 0; k < mThumbIds.length; k++) {

									mThumbIds[k] = Utils.strImageUrl + mThumbIds[k];
									vP.setImage(mThumbIds[k]);
									DealersOrderActivity.arraylistPdoductImages.add(vP);

								}

								list = new ArrayList<String>(Arrays.asList(mThumbIds));
								Log.e("prodArray", String.valueOf(mThumbIds.length));

							} else {
								dod.setFlag(job.getString("isfree"));
								Log.e("flag", dod.getFlag());
								Constants.serialno = job.getString("ordslno");
								dod.setProdcode(job.getJSONObject("product").getString("prodcode"));
								dod.setproductname(job.getJSONObject("product").getString("prodname"));
								if(dod.getFlag().equals("NF")){
									dod.setqty(job.getString("qty"));
									dod.setFreeQty("0");
								}else{
									dod.setFreeQty(job.getString("qty"));
									dod.setqty("0");
								}

								dod.setordstatus(job.getString("ordstatus"));
								dod.setpaymentType("C");
								dod.setUOM(job.getJSONObject("product").getString("uom"));
                                dod.setSelcetedUOM(job.getString("ord_uom"));
								dod.setUnitGram(job.getJSONObject("product").getString("unitsgm"));
								dod.setUnitPerUom(job.getJSONObject("product").getString("unitperuom"));
								dod.setDeliverymode(job.getString("deliverytype"));
								dod.setProductPrice(job.getJSONObject("product").getString("prodprice"));
								dod.setProductTax(job.getJSONObject("product").getString("prodtax"));


								Log.e("approximateTotal", String.valueOf(approximateTotal));
								Log.e("flag", job.getString("isfree"));

								if ( job.getString("isfree").equals("NF")){
									approximateTotal = approximateTotal+Double.parseDouble(job.getString("pvalue"));
								}

								if(job.getString("deliverydt").equals("")||job.getString("deliverydt").equals(null)){
									dod.setDeliverydate("");

								}else{
									dod.setDeliverydate(job.getString("deliverydt"));

								}
								if(job.getString("deliverytime").equals("")||job.getString("deliverytime").equals(null)){
									dod.setDeliverytime("");
								}else{
									dod.setDeliverytime(job.getString("deliverytime"));
								}

								//getting MultipleUOM for each product
								JSONArray uom=job.getJSONObject("product").getJSONArray("u");
								Log.e("JSONArray", String.valueOf(uom.length()));
								for(int j=0;j<uom.length();j++){
									ProductUomDomain UOM = new ProductUomDomain();
									JSONObject jobuom = new JSONObject();
									jobuom=uom.getJSONObject(j);
									UOM.setProdid(jobuom.getString("prodid"));
									UOM.setUomdesc(jobuom.getString("uomname"));
									UOM.setUomid(jobuom.getString("uomid"));
									UOM.setUomvalue(jobuom.getString("uomvalue"));
									UOM.setFlag(job.getString("isfree"));
									Log.e("uomname",jobuom.getString("uomname"));
									arraylistproductUOM.add(UOM);
								}


								if(dod.getUOM().equals(dod.getSelcetedUOM())){
									dod.setSelecetdQtyUomUnit(dod.getUnitPerUom());
									dod.setSelecetdFreeQtyUomUnit(dod.getUnitPerUom());
								}else{
									for(ProductUomDomain fpd : DealerOrderDetailsActivity.arraylistproductUOM){
										Log.e("flag",dod.getFlag());
										if(fpd.getUomdesc().equals(dod.getSelcetedUOM())){
											Log.e("SelecetdQty",fpd.getUomvalue());
											Log.e("Selecetdprodid",fpd.getProdid());
											Log.e("Selecetduom",fpd.getUomdesc());
											if(dod.getFlag().equals("NF")){
												dod.setSelecetdQtyUomUnit(fpd.getUomvalue());
											}else{
												dod.setSelecetdFreeQtyUomUnit(fpd.getUomvalue());
											}
										}
									}
								}
							}

								DealersOrderActivity.arraylistDealerOrderDetailList.add(dod);
						}
							catch(Exception e){

						}
					}
						Log.e("approximateTotal", String.valueOf(approximateTotal));
						Constants.adapterset=1;
						setViewPagerAdapter();
						isread();
					}else{
						showAlertDialogToast3(strMsg);

					}
					dialog.dismiss();
				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {

				}

			}

			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();
				JsonServiceHandler = new JsonServiceHandler(Utils.strdealerOderdetail+DealersOrderActivity.orderId+Utils.strMerSingleOderdetailAdditon+Constants.USER_ID, DealerOrderDetailsActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceDataGet();
				return null;
			}
		}.execute(new Void[] {});
	}
	public void setViewPagerAdapter() {

		if (DealersOrderActivity.arraylistPdoductImages.size() == 0) {

			linearLayoutButton.setVisibility(LinearLayout.GONE);
			linearLayoutHeading.setVisibility(LinearLayout.VISIBLE);

			Log.e("arraysize", String.valueOf(imagesetarray.length));
			Log.e("arraysize1", String.valueOf(DealersOrderActivity.arraylistimage.size()));
			if(DealersOrderActivity.arraylistimage.size()==0)
			{
                    layoutgalleryViewcamera.setVisibility(View.GONE);
			}else{

				Constants.sendimageproduct=1;
				galleryViewcamera.setAdapter(new DealerImageAdapter(this));

			}
			Log.e("arraylist", String.valueOf(DealersOrderActivity.arraylistDealerOrderDetailList.size()));
			DealerOrderDetailAdapter adapter = new DealerOrderDetailAdapter(DealerOrderDetailsActivity.this, R.layout.item_dealer_order_detail, DealersOrderActivity.arraylistDealerOrderDetailList);
			listViewOrders.setAdapter(adapter);
		} else {

			Constants.sendimageproduct=0;
			gridview.setAdapter(new DealerImageAdapter(this));
			linearLayoutButton.setVisibility(LinearLayout.VISIBLE);
			linearLayoutHeading.setVisibility(LinearLayout.GONE);

		}
	}

	public void isread() {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";
			@Override
			protected void onPreExecute() {
				/*dialog = ProgressDialog.show(MerchantSingleDetailActivity.this, "",
						"Loading...", true, true);*/

			}

			@Override
			protected void onPostExecute(Void result) {

				try{

					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);


				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {

				}

			}

			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();
				int j=0;
				String newread = null;
				try {
					Log.e("readarraylenght", String.valueOf(read.length));
					for(int i=0;i<read.length;i++){

						if(read[i].equals(Constants.USER_ID)){
							Log.e("readarray", read[i]);

							List<String> list = new ArrayList<String>(Arrays.asList(read));
							list.remove(read[i]);
							read = list.toArray(new String[0]);
						}
					}

					Log.e("arraylenghtout", String.valueOf(read.length));
					for(int a=0;a<read.length;a++) {

						if (j == 0) {

							newread =read[a];
							a++;
						}else{

							newread=newread+","+read[a];

						}

					}

					if(read.length==0){
						jsonObject.put("isread","null");
					}else {

						jsonObject.put("isread", newread);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}

				Log.e("orderidisread",Constants.orderid);
				JsonServiceHandler = new JsonServiceHandler(
						Utils.strisread +Constants.orderid +"&[where][duserid]="+Constants.USER_ID,jsonObject.toString(),
						DealerOrderDetailsActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceData();
				return null;
			}
		}.execute(new Void[] {});
	}

	private void saveOrder() {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog= ProgressDialog.show(DealerOrderDetailsActivity.this, "",
						"Loading...", true, true);
			}

			@Override
			protected void onPostExecute(Void result) {

				try {

					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);
					saveApproximateTotal();
					//showAlertDialogToastupdate(strMsg);
     //toastDisplay(strMsg);

				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					//Log.e("ProductActivityException", e.toString());
				}
				dialog.dismiss();
			}

			@Override
			protected Void doInBackground(Void... params) {
				JSONObject jsonObject = new JSONObject();
				try {
				JSONArray jsonArray = new JSONArray();
				JSONObject jsonObject1;
					approximateTotal=0.0;
				orderdtid = new String[DealersOrderActivity.arraylistDealerOrderDetailList.size()];
				orderid = new String[DealersOrderActivity.arraylistDealerOrderDetailList.size()];
				prodid = new String[DealersOrderActivity.arraylistDealerOrderDetailList.size()];
				qty = new String[DealersOrderActivity.arraylistDealerOrderDetailList.size()];
				date = new String[DealersOrderActivity.arraylistDealerOrderDetailList.size()];
				status = new String[DealersOrderActivity.arraylistDealerOrderDetailList.size()];
				freeQty	 = new String[DealersOrderActivity.arraylistDealerOrderDetailList.size()];
					flag= new String[DealersOrderActivity.arraylistDealerOrderDetailList.size()];
					deliverydate= new String[DealersOrderActivity.arraylistDealerOrderDetailList.size()];
					deliverymode= new String[DealersOrderActivity.arraylistDealerOrderDetailList.size()];
					deliverytime= new String[DealersOrderActivity.arraylistDealerOrderDetailList.size()];
					price = new String[DealersOrderActivity.arraylistDealerOrderDetailList.size()];
					ptax = new String[DealersOrderActivity.arraylistDealerOrderDetailList.size()];
					unitgram= new String[DealersOrderActivity.arraylistDealerOrderDetailList.size()];
					qtyUomUnit= new String[DealersOrderActivity.arraylistDealerOrderDetailList.size()];
					freeqtyUomUnit= new String[DealersOrderActivity.arraylistDealerOrderDetailList.size()];
					//paymentType= new String[arraylistDealerOrderDetailList.size()];

				for (int i = 0; i < DealersOrderActivity.arraylistDealerOrderDetailList.size(); i++) {
					orderdtid[i] =DealersOrderActivity.arraylistDealerOrderDetailList.get(i).getorddtlid();
					orderid[i] =  DealersOrderActivity.arraylistDealerOrderDetailList.get(i).getOrderid();
					prodid[i] =DealersOrderActivity.arraylistDealerOrderDetailList.get(i).getprodid();
					flag[i]=DealersOrderActivity.arraylistDealerOrderDetailList.get(i).getFlag();
					date[i] =DealersOrderActivity.arraylistDealerOrderDetailList.get(i).getDate();
					status[i]=DealersOrderActivity.arraylistDealerOrderDetailList.get(i).getordstatus();
					freeQty[i]=DealersOrderActivity.arraylistDealerOrderDetailList.get(i).getFreeQty();
					qty[i]=DealersOrderActivity.arraylistDealerOrderDetailList.get(i).getqty();
					deliverymode[i]=DealersOrderActivity.arraylistDealerOrderDetailList.get(i).getDeliverymode();
					deliverydate[i]=DealersOrderActivity.arraylistDealerOrderDetailList.get(i).getDeliverydate();
					deliverytime[i]=DealersOrderActivity.arraylistDealerOrderDetailList.get(i).getDeliverytime();
					price[i]=DealersOrderActivity.arraylistDealerOrderDetailList.get(i).getProductPrice();
					ptax[i]=DealersOrderActivity.arraylistDealerOrderDetailList.get(i).getProductTax();
					unitgram[i]=DealersOrderActivity.arraylistDealerOrderDetailList.get(i).getUnitGram();
					qtyUomUnit[i]=DealersOrderActivity.arraylistDealerOrderDetailList.get(i).getSelectedQtyUomUnit();
					freeqtyUomUnit[i]=DealersOrderActivity.arraylistDealerOrderDetailList.get(i).getSelectedFreeQtyUomUnit();
					//paymentType[i]=arraylistDealerOrderDetailList.get(i).getPaymentType();
					Log.e("flag", String.valueOf(flag[i]));
					Log.e("fqty", String.valueOf(freeQty[i]));
					Log.e("qty", String.valueOf(qty[i]));
				}
					Log.e("size", String.valueOf(DealersOrderActivity.arraylistDealerOrderDetailList.size()));
				for (int i = 0; i < DealersOrderActivity.arraylistDealerOrderDetailList.size(); i++) {
					jsonObject1 = new JSONObject();
					try {

						double frqty = Float.parseFloat(freeQty[i]);
						double qtty = Float.parseFloat(qty[i]);
						Log.e("approximateTotalmain", String.valueOf(approximateTotal));

						Log.e("fqty", String.valueOf(frqty));
						Log.e("qty", String.valueOf(qtty));
						if(frqty>0){
							jsonObject1.put("qty", freeQty[i]);
							Double totalunit=   (frqty) *(Double.parseDouble(freeqtyUomUnit[i]));
							Double pval = (totalunit*(Double.parseDouble(price[i]) + Double.parseDouble(ptax[i])));

							Log.e("pval",String.valueOf(pval));
							//Double defaultuomTotal =( (frqty) *(Double.parseDouble(unitgram[i]))*(Double.parseDouble(freeqtyUomUnit[i])))/1000;
							Double defaultuomTotal =( (frqty) *(Double.parseDouble(freeqtyUomUnit[i])));
							Log.e("defaultuomTotalfrqty123", String.valueOf(( (frqty) *(Double.parseDouble(freeqtyUomUnit[i])))));
							// apprvalue=apprvalue+pval;
							Log.e("approximateTotalfree", String.valueOf(approximateTotal));
							Log.e("defaultuomTotal",String.valueOf(defaultuomTotal));
							jsonObject1.put("pvalue", pval);
							jsonObject1.put("cqty", defaultuomTotal);
						}

						if(qtty>0){
							jsonObject1.put("qty", qty[i]);
							Double totalunit=   (qtty) *(Double.parseDouble(qtyUomUnit[i]));
							Double updtpval = (totalunit*(Double.parseDouble(price[i]) + Double.parseDouble(ptax[i])));
						//	Double defaultuomTotal =( (qtty) *(Double.parseDouble(unitgram[i]))*(Double.parseDouble(qtyUomUnit[i])))/1000;
							Double defaultuomTotal =( (qtty) *(Double.parseDouble(freeqtyUomUnit[i])));
							Log.e("defaultuomTotalqtty123", String.valueOf(( (qtty) *(Double.parseDouble(freeqtyUomUnit[i])))));
							jsonObject1.put("pvalue", updtpval);
							jsonObject1.put("cqty", defaultuomTotal);

							Log.e("qty","qty");
							approximateTotal = approximateTotal + updtpval;
							Log.e("approximateTotal", String.valueOf(approximateTotal));
						}


						jsonObject1.put("orddtlid", orderdtid[i]);
						jsonObject1.put("ordid", orderid[i]);
						jsonObject1.put("prodid",prodid[i]);
						jsonObject1.put("deliverydt",date[i]);
						jsonObject1.put("ordstatus", status[i]);
						jsonObject1.put("duserid",Constants.USER_ID);

						jsonObject1.put("deliverydt",deliverydate[i]);
						jsonObject1.put("deliverytype", deliverymode[i]);
						jsonObject1.put("deliverytime", deliverytime[i]);
						Log.e("approximate", String.valueOf(approximateTotal));
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					jsonArray.put(jsonObject1);
				}

					jsonObject.put("orderdtls", jsonArray);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				JsonServiceHandler = new JsonServiceHandler(Utils.strsavedealerOderdetail,jsonObject.toString(), DealerOrderDetailsActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceData();
				return null;
			}
		}.execute(new Void[] {});
	}
	private void saveApproximateTotal() {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog= ProgressDialog.show(DealerOrderDetailsActivity.this, "",
						"Loading...", true, true);
			}

			@Override
			protected void onPostExecute(Void result) {

				try {

					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);

					showAlertDialogToastupdate(strMsg);

				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					//Log.e("ProductActivityException", e.toString());
				}
				dialog.dismiss();
			}

			@Override
			protected Void doInBackground(Void... params) {
				JSONObject jsonObject = new JSONObject();
				try {

					jsonObject.put("aprxordval", approximateTotal);


				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				JsonServiceHandler = new JsonServiceHandler(Utils.strUpdateOrderHeader+DealersOrderActivity.orderId,jsonObject.toString(), DealerOrderDetailsActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceData();


				return null;
			}
		}.execute(new Void[]{});
	}

	protected void toastDisplay(String msg) {
		Toast toast = Toast.makeText(DealerOrderDetailsActivity.this, msg,
				Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

	}
	public boolean netCheck() {
		// for network connection
		try {
			ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetwork = connectionManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			Object result = null;
			if (mWifi.isConnected() || mNetwork.isConnected()) {
				return true;
			}

			else if (result == null) {
				showAlertDialog(DealerOrderDetailsActivity.this,
						"No Internet Connection",
						"Please Check Your internet connection.", false);
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	public void showAlertDialog(Context context, String title, String message,
			Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
					}
				});
		alertDialog.show();
	}
	public void showAlertDialogToast3( String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();

		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

	}
	public void showAlertDialogToastupdate( String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						Intent io = new Intent(DealerOrderDetailsActivity.this, DealerOrderDetailsActivity.class);
						Constants.adapterset = 0;
						startActivity(io);
						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();

		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

	}
	public void showAlertDialogToastupdateimage( String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						Intent io = new Intent(DealerOrderDetailsActivity.this, DealerOrderDetailsActivity.class);
						Constants.adapterset = 1;
						startActivity(io);
						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();

		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

	}
	public void showAlertDialogToast1( String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						Intent io = new Intent(DealerOrderDetailsActivity.this, DealersOrderActivity.class);
						startActivity(io);
						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();

		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));
	}
	public void showAlertDialogToastdelorder() {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage("Do you want to cancel the order?");
		builder1.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						deleteorders();
						dialog.cancel();
					}
				});
		builder1.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();

	}
	public void onBackPressed() {
		exitAlret();
	}

	private void exitAlret() {
		AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
		localBuilder.setCancelable(false);
		localBuilder.setMessage("Do you want to Exit?");
		localBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {

						finish();

					}
				});
		localBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {
						paramDialogInterface.cancel();
					}
				});
		localBuilder.create().show();
	}

}
