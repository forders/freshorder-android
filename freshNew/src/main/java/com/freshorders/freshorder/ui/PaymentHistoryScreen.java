package com.freshorders.freshorder.ui;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.DealerOrderListAdapter;
import com.freshorders.freshorder.adapter.PaymentHistoryAdapter;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.DealerOrderListDomain;
import com.freshorders.freshorder.domain.PaymentHistoryDomain;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandlerForPayment;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class PaymentHistoryScreen extends Activity {

    TextView menuIcon, textViewAssetMenuProfile, textViewAssetMenuMyOrders,
            textViewAssetMenuMyDealers, textViewAssetMenuProducts,
            textViewAssetMenuPayment, textViewAssetMenuComplaint,
            textViewAssetMenuSignout,textViewAssetMenuCreateOrder,textViewAssetMenuExport,textViewAssetMenuSClientVisit;

    ListView listViewDetails;
    int menuCliccked;
    LinearLayout linearLayoutMenuParent, linearLayoutProfile,
            linearLayoutMyOrders, linearLayoutMyDealers, linearLayoutProducts,
            linearLayoutPayment, linearLayoutComplaint, linearLayoutSignout
            ,linearLayoutCreateOrder,linearLayoutExport,linearLayoutClientVisit;
    JSONObject JsonAccountObject = null;
    JSONArray JsonAccountArray = null;
    DatabaseHandler databaseHandler;
    ArrayList<PaymentHistoryDomain> arraylistPaymentHistoryList;
    JsonServiceHandler JsonServiceHandler1;
    public static String time,searchclick="0", strMsg = "";
    Date dateStr = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.payment_history_screen);
        netCheck();
        databaseHandler = new DatabaseHandler(getApplicationContext());
        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();

        Cursor cur;
        cur = databaseHandler.getDetails();
        cur.moveToFirst();
        Log.e("Insertion Check",
                cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_username)));
        Log.e("Insertion Check",
                cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_password)));
        Log.e("Insertion Check",
                cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_usertype)));

        myPayment();

        linearLayoutCreateOrder = (LinearLayout) findViewById(R.id.linearLayoutCreateOrder);
        listViewDetails = (ListView) findViewById(R.id.listViewPaymentHistory);
        linearLayoutProfile = (LinearLayout) findViewById(R.id.linearLayoutProfile);
        linearLayoutMyOrders = (LinearLayout) findViewById(R.id.linearLayoutMyOrders);
        linearLayoutMyDealers = (LinearLayout) findViewById(R.id.linearLayoutMyDealers);
        linearLayoutProducts = (LinearLayout) findViewById(R.id.linearLayoutProducts);
        linearLayoutPayment = (LinearLayout) findViewById(R.id.linearLayoutPayment);
        linearLayoutComplaint = (LinearLayout) findViewById(R.id.linearLayoutComplaint);
        linearLayoutExport= (LinearLayout) findViewById(R.id.linearLayoutExport);
        linearLayoutClientVisit= (LinearLayout) findViewById(R.id.linearLayoutClientVisit);
        linearLayoutSignout = (LinearLayout) findViewById(R.id.linearLayoutSignout);
        if (Constants.USER_TYPE.equals("D")) {
            linearLayoutMyDealers.setVisibility(View.GONE);
            linearLayoutProducts.setVisibility(View.VISIBLE);
            linearLayoutCreateOrder.setVisibility(View.GONE);
        } else if (Constants.USER_TYPE.equals("M")){
            linearLayoutMyDealers.setVisibility(View.VISIBLE);
            linearLayoutProducts.setVisibility(View.GONE);
            linearLayoutCreateOrder.setVisibility(View.GONE);
        }else {
            linearLayoutMyDealers.setVisibility(View.VISIBLE);
            linearLayoutProducts.setVisibility(View.GONE);
            linearLayoutCreateOrder.setVisibility(View.VISIBLE);
        }
        menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
        textViewAssetMenuCreateOrder= (TextView) findViewById(R.id.textViewAssetMenuCreateOrder);
        textViewAssetMenuProfile = (TextView) findViewById(R.id.textViewAssetMenuProfile);
        textViewAssetMenuMyOrders = (TextView) findViewById(R.id.textViewAssetMenuMyOrders);
        textViewAssetMenuMyDealers = (TextView) findViewById(R.id.textViewAssetMenuMyDealers);
        textViewAssetMenuProducts = (TextView) findViewById(R.id.textViewAssetMenuProducts);
        textViewAssetMenuPayment = (TextView) findViewById(R.id.textViewAssetMenuPayment);
        textViewAssetMenuComplaint = (TextView) findViewById(R.id.textViewAssetMenuComplaint);
        textViewAssetMenuSignout = (TextView) findViewById(R.id.textViewAssetMenuSignout);
        textViewAssetMenuExport= (TextView) findViewById(R.id.textViewAssetMenuExport);
        linearLayoutMenuParent = (LinearLayout) findViewById(R.id.linearLayoutMenuParent);
        textViewAssetMenuSClientVisit= (TextView) findViewById(R.id.textViewAssetMenuSClientVisit);

        Typeface font = Typeface.createFromAsset(getAssets(),
                "fontawesome-webfont.ttf");
        menuIcon.setTypeface(font);
        textViewAssetMenuCreateOrder.setTypeface(font);
        textViewAssetMenuProfile.setTypeface(font);
        textViewAssetMenuMyOrders.setTypeface(font);
        textViewAssetMenuMyDealers.setTypeface(font);
        textViewAssetMenuProducts.setTypeface(font);
        textViewAssetMenuPayment.setTypeface(font);
        textViewAssetMenuComplaint.setTypeface(font);
        textViewAssetMenuSignout.setTypeface(font);
        textViewAssetMenuSClientVisit.setTypeface(font);
        textViewAssetMenuExport.setTypeface(font);


        menuIcon.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (menuCliccked == 0) {
                    linearLayoutMenuParent.setVisibility(View.VISIBLE);
                    menuCliccked = 1;
                } else {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                }

            }
        });

        linearLayoutMenuParent.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
            }
        });

        linearLayoutProfile.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Intent io = new Intent(PaymentHistoryScreen.this,
                        ProfileActivity.class);
                startActivity(io);
                finish();

            }
        });

        linearLayoutMyOrders.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                // TODO Auto-generated method stub
                if (Constants.USER_TYPE.equals("D")) {
                    Intent io = new Intent(PaymentHistoryScreen.this,
                            DealersOrderActivity.class);
                    startActivity(io);
                    finish();
                } else if (Constants.USER_TYPE.equals("M")){
                    Intent io = new Intent(PaymentHistoryScreen.this,
                            MerchantOrderActivity.class);
                    startActivity(io);
                    finish();
                }else {
                    Intent io = new Intent(PaymentHistoryScreen.this,
                            SalesManOrderActivity.class);
                    startActivity(io);
                    finish();
                }

            }
        });

        linearLayoutMyDealers.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                // TODO Auto-generated method stub
                Intent io = new Intent(PaymentHistoryScreen.this,
                        MyDealersActivity.class);
                startActivity(io);
                finish();

            }
        });

        linearLayoutProducts.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                // TODO Auto-generated method stub
                Intent io = new Intent(PaymentHistoryScreen.this,
                        ProductActivity.class);
                startActivity(io);
                finish();

            }
        });

        linearLayoutCreateOrder.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                // TODO Auto-generated method stub
                Intent io = new Intent(PaymentHistoryScreen.this,
                        CreateOrderActivity.class);
                startActivity(io);
                finish();

            }
        });

        linearLayoutPayment.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Intent io = new Intent(PaymentHistoryScreen.this,
                        DealerPaymentActivity.class);
                startActivity(io);
                finish();
            }
        });

        linearLayoutComplaint.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (Constants.USER_TYPE.equals("D")) {
                    Intent io = new Intent(PaymentHistoryScreen.this,
                            DealersComplaintActivity.class);
                    startActivity(io);
                    finish();
                } else {
                    Intent io = new Intent(PaymentHistoryScreen.this,
                            MerchantComplaintActivity.class);
                    startActivity(io);
                    finish();
                }

            }
        });

        linearLayoutSignout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                // TODO Auto-generated method stub
                databaseHandler.delete();
                Intent io = new Intent(PaymentHistoryScreen.this,
                        SigninActivity.class);
                startActivity(io);
                finish();
            }
        });
        linearLayoutClientVisit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Intent io = new Intent(PaymentHistoryScreen.this,
                        DealerClientVisit.class);
                startActivity(io);
                finish();

            }
        });

        linearLayoutExport.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Intent io = new Intent(PaymentHistoryScreen.this,
                        ExportActivity.class);
                startActivity(io);
                finish();

            }
        });


    }

    private void myPayment() {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "";



            @Override
            protected void onPreExecute() {
                dialog= ProgressDialog.show(PaymentHistoryScreen.this, "",
                        "Loading...", true, true);

            }

            @Override
            protected void onPostExecute(Void result) {

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {

                        JSONArray job1 = JsonAccountObject.getJSONArray("data");
                        arraylistPaymentHistoryList=new ArrayList<PaymentHistoryDomain>();

                        for (int i = 0; i < job1.length(); i++) {
                            JSONObject job = new JSONObject();
                            job=job1.getJSONObject(i);
                            PaymentHistoryDomain dod=new PaymentHistoryDomain();

                            if(job.getString("pymtref").equals("null")||job.getString("pymtref").equals(null)||job.getString("pymtref").equals("")){
                                dod.setPaymentId("");

                            }else {
                                dod.setPaymentId(job.getString("pymtref"));

                            }
                            dod.setStatus(job.getString("pymtstatus"));

                            time=job.getString("pymtdate");

                            String inputPattern = "yyyy-MM-dd HH:mm:ss";
                            String outputPattern = "dd-MMM-yyyy hh:mm aa";
                            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
                            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

                            String str = null;

                            try {
                                dateStr = inputFormat.parse(time);
                                str = outputFormat.format(dateStr);
                                dod.setDate(str);
                                //Log.e("str", str);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            arraylistPaymentHistoryList.add(dod);


                        }
                        PaymentHistoryAdapter  adapter=new PaymentHistoryAdapter(PaymentHistoryScreen.this, R.layout.item_dealer_order_screen, arraylistPaymentHistoryList);
                        listViewDetails.setVisibility(View.VISIBLE);
                        listViewDetails.setAdapter(adapter);

                    }else{

                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Log.e("ProductActivityException", e.toString());
                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();


                JsonServiceHandler1 = new JsonServiceHandler(Utils.strPaymentHistory+Constants.USER_ID, PaymentHistoryScreen.this);
                JsonAccountObject = JsonServiceHandler1.ServiceDataGet();
                return null;
            }
        }.execute(new Void[] {});
    }


    public boolean netCheck() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            }

            else if (result == null) {
                showAlertDialog(PaymentHistoryScreen.this, "No Internet Connection",
                        "Please Check Your internet connection.", false);
                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public void showAlertDialog(Context context, String title, String message,
                                Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        alertDialog.show();
    }


    public void showAlertDialogToast( String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }
    public void onBackPressed() {
        exitAlret();
    }

    private void exitAlret() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        localBuilder.setMessage("Do you want to Exit?");
        localBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {

                        finish();

                    }
                });
        localBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {
                        paramDialogInterface.cancel();
                    }
                });
        localBuilder.create().show();
    }
}
