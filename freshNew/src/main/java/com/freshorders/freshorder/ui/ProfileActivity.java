package com.freshorders.freshorder.ui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.activity.DistanceCalActivity;
import com.freshorders.freshorder.adapter.AddDealerListAdapter;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.AddDealerListDomain;
import com.freshorders.freshorder.domain.AddEodDomain;
import com.freshorders.freshorder.toonline.migration.PendingDataMigrationActivity;
import com.freshorders.freshorder.utils.CircularImageView;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.DocUploadFileToServer;
import com.freshorders.freshorder.utils.DocUploadFileToServer1;
import com.freshorders.freshorder.utils.ImageLoader;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

import eu.janmuller.android.simplecropimage.CropImage;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import static java.lang.String.valueOf;


public class ProfileActivity extends Activity {

	TextView menuIcon, textViewAssetMenuProfile, textViewAssetMenuMyOrders,
			textViewAssetMenuMyDealers, textViewAssetMenuProducts,textViewSalesmanData,
			textViewAssetMenuPayment, textViewAssetMenuComplaint,
			textViewAssetMenuSignout, textViewAssetMenuCreateOrder, textViewAssetMenuExport,
			textViewAssetMenuClientVisit, textViewAssetMenuPaymentCollection, textViewClient, textViewAssetMenuRefresh,textViewAssetMenuDistanceCalculation,
			textViewAssetMenuStockEntry, textViewAssetMenuAddMerchant, textViewAssetMenuAcknowledge,
			textViewAssetMenusurveyuser, textViewAssetMenudownloadscheme, textViewAssetMenuMyDayPlan, textViewAssetMenuMySales,
			textViewAssetMenuDistStock, textViewAssetMenuStockOnly, textViewAssetMenuMigration, textViewAssetMenuStockAudit;

	int menuCliccked;
	LinearLayout linearLayoutMenuParent, linearLayoutProfile, linearLayoutMyOrders,
			linearLayoutMyDealers, linearLayoutProducts, linearLayoutPayment,
			linearLayoutComplaint, linearLayoutSignout, linearLayoutEdit, linearLayoutDetails,
			linearLayoutCreateOrder, linearLayoutdump3, linearLayoutdump2, linearLayoutdump1, linearLayoutdump4, linearLayoutExport,
			linearLayoutClientVisit, linearLayoutPaymentCollection, linearLayoutRefresh,linearLayoutDistanceCalculation,
			linearLayoutAcknowledge, linearLayoutStockEntry, linearLayoutAddMerchant, linearLayoutsurveyuser, linearLayoutdownloadscheme, linearLayoutMyDayPlan, linearLayoutMySales,
			linearLayoutDistStock, linearLayoutStockOnly, linearLayoutMigration, linearLayoutStockAudit;

	private void showMenu(){

		MyApplication app = MyApplication.getInstance();

		if(!app.isCreateOrder()){
			linearLayoutCreateOrder.setVisibility(View.GONE);
		}
		if(!app.isProfile()){
			linearLayoutProfile.setVisibility(View.GONE);
		}
		if(!app.isMyDayPlan()){
			linearLayoutMyDayPlan.setVisibility(View.GONE);
		}
		if(!app.isMyOrders()){
			linearLayoutMyOrders.setVisibility(View.GONE);
		}
		if(!app.isAddMerchant()){
			linearLayoutAddMerchant.setVisibility(View.GONE);
		}
		if(!app.isMySales()){
			linearLayoutMySales.setVisibility(View.GONE);
		}
		if(!app.isPostNotes()){
			linearLayoutComplaint.setVisibility(View.GONE);
		}
		if(!app.isMyClientVisit()){
			linearLayoutClientVisit.setVisibility(View.GONE);
		}
		if(!app.isAcknowledge()){
			linearLayoutAcknowledge.setVisibility(View.GONE);
		}
		if(!app.isPaymentCollection()){
			linearLayoutPaymentCollection.setVisibility(View.GONE);
		}
		if(!app.isPkdDataCapture()){
			linearLayoutStockEntry.setVisibility(View.GONE);
		}
		if(!app.isDistributorStock()){
			linearLayoutDistStock.setVisibility(View.GONE);
		}
		if(!app.isSurveyUser()){
			linearLayoutsurveyuser.setVisibility(View.GONE);
		}
		if(!app.isDownLoadScheme()){
			linearLayoutdownloadscheme.setVisibility(View.GONE);
		}
		if(!app.isDistanceCalculation()){
			linearLayoutDistanceCalculation.setVisibility(View.GONE);
		}
		if(!app.isRefresh()){
			linearLayoutRefresh.setVisibility(View.GONE);
		}
		if(!app.isLogout()){
			linearLayoutSignout.setVisibility(View.GONE);
		}
		if(!app.isClosingStock()){
			linearLayoutStockOnly.setVisibility(View.GONE);
		}
		if(!app.isClosingStockAudit()){
			linearLayoutStockAudit.setVisibility(View.GONE);
		}
		if(!app.isPendingData()){
			linearLayoutMigration.setVisibility(View.GONE);
		}
	}

	CircularImageView CircularImageView;
	File imageFilePath1;
	String imagePath1 = "", fullname, companyName, email, address, payment;
	EditText EditTextViewName, EditTextViewCompanyName, EditTextViewMobile, EditTextViewEmail, EditTextViewAddress, textViewName, textViewCompanyName, textViewMobile, textViewAddress,
			textViewEmail, textViewPayment;
	Spinner spinnerPayment;
	Button buttonSave, buttonEdit,buttonChangePassword;
	String settingid,industryid,refkey,data_type,remarks,refvalue,userid,updateddt,status;
	JSONObject JsonAccountObject;
//
// 	JSONObject jsonSettings=null;
	JSONArray JsonAccountArray;
	DatabaseHandler databaseHandler;
	JsonServiceHandler JsonServiceHandler;
	String mobno;
	ImageLoader imageloader;
	public static String PaymentStatus = "NULL", moveto = "NULL";

	EditText edittextOldPassword;
	EditText edittextNewPassword;
	EditText edittextConfirmPassword;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(1);
		setContentView(R.layout.profile_new_activity);
		databaseHandler = new DatabaseHandler(getApplicationContext());
		Cursor curs;
		curs = databaseHandler.getdealer();
		curs.moveToFirst();
		Constants.DUSER_ID = curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_Duserid));
		databaseHandler = new DatabaseHandler(getApplicationContext());
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();
		imageloader = new ImageLoader(getApplicationContext());
		//SalesmanDataLink();
		Cursor cur;
		cur = databaseHandler.getDetails();
		cur.moveToFirst();
		Log.e("Insertion Check",
				cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_username)));
		Log.e("Insertion Check",
				cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_password)));
		Log.e("Insertion Check",
				cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_usertype)));

		if(netCheckwithoutAlert()){
			PaymentStatus = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_paymentStatus));
		}

		linearLayoutEdit = (LinearLayout) findViewById(R.id.linearLayoutEdit);
		linearLayoutDetails = (LinearLayout) findViewById(R.id.linearLayoutDetails);
		linearLayoutCreateOrder = (LinearLayout) findViewById(R.id.linearLayoutCreateOrder);
		linearLayoutProfile = (LinearLayout) findViewById(R.id.linearLayoutProfile);
		linearLayoutMyOrders = (LinearLayout) findViewById(R.id.linearLayoutMyOrders);
		linearLayoutMyDealers = (LinearLayout) findViewById(R.id.linearLayoutMyDealers);
		linearLayoutProducts = (LinearLayout) findViewById(R.id.linearLayoutProducts);
		linearLayoutPayment = (LinearLayout) findViewById(R.id.linearLayoutPayment);
		linearLayoutComplaint = (LinearLayout) findViewById(R.id.linearLayoutComplaint);
		linearLayoutSignout = (LinearLayout) findViewById(R.id.linearLayoutSignout);
		linearLayoutExport = (LinearLayout) findViewById(R.id.linearLayoutExport);
		linearLayoutClientVisit = (LinearLayout) findViewById(R.id.linearLayoutClientVisit);
		linearLayoutPaymentCollection = (LinearLayout) findViewById(R.id.linearLayoutPaymentCollection);
		linearLayoutRefresh = (LinearLayout) findViewById(R.id.linearLayoutRefresh);
		linearLayoutdump1 = (LinearLayout) findViewById(R.id.linearLayoutdump1);
		linearLayoutdump2 = (LinearLayout) findViewById(R.id.linearLayoutdump2);
		linearLayoutdump3 = (LinearLayout) findViewById(R.id.linearLayoutdump3);
		linearLayoutdump4 = (LinearLayout) findViewById(R.id.linearLayoutdump4);

		linearLayoutAcknowledge = (LinearLayout) findViewById(R.id.linearLayoutAcknowledge);
		linearLayoutStockEntry = (LinearLayout) findViewById(R.id.linearLayoutStockEntry);
		linearLayoutAddMerchant = (LinearLayout) findViewById(R.id.linearLayoutAddMerchant);
		linearLayoutDistanceCalculation=(LinearLayout) findViewById(R.id.linearLayoutDistanceCalculation);
		linearLayoutdownloadscheme = (LinearLayout) findViewById(R.id.linearLayoutdownloadscheme);
		linearLayoutsurveyuser = (LinearLayout) findViewById(R.id.linearLayoutsurveyuser);
		linearLayoutMyDayPlan = (LinearLayout) findViewById(R.id.linearLayoutMyDayPlan);
		linearLayoutMySales = (LinearLayout) findViewById(R.id.linearLayoutMySales);

		textViewAssetMenuRefresh = (TextView) findViewById(R.id.textViewAssetMenuRefresh);

		linearLayoutStockOnly = (LinearLayout) findViewById(R.id.linearLayoutStockOnly);
		linearLayoutStockAudit = findViewById(R.id.linearLayoutStockAudit);
		linearLayoutMigration = (LinearLayout) findViewById(R.id.linearLayoutMigration);

		if (Constants.USER_TYPE.equals("D")) {
			linearLayoutPaymentCollection.setVisibility(View.GONE);
			linearLayoutMyDealers.setVisibility(View.GONE);
			linearLayoutProducts.setVisibility(View.VISIBLE);
			linearLayoutExport.setVisibility(View.VISIBLE);
			linearLayoutCreateOrder.setVisibility(View.GONE);
			linearLayoutRefresh.setVisibility(View.GONE);
			linearLayoutdump2.setVisibility(View.VISIBLE);
			linearLayoutAcknowledge.setVisibility(View.GONE);
			linearLayoutAddMerchant.setVisibility(View.GONE);
			linearLayoutStockEntry.setVisibility(View.GONE);
			linearLayoutdownloadscheme.setVisibility(View.GONE);
			linearLayoutsurveyuser.setVisibility(View.GONE);
			linearLayoutDistanceCalculation.setVisibility(View.GONE);
			linearLayoutMySales.setVisibility(View.GONE);
		} else if (Constants.USER_TYPE.equals("M")) {
			linearLayoutPaymentCollection.setVisibility(View.GONE);
			linearLayoutClientVisit.setVisibility(View.GONE);
			linearLayoutMyDealers.setVisibility(View.VISIBLE);
			linearLayoutProducts.setVisibility(View.GONE);
			linearLayoutExport.setVisibility(View.GONE);
			linearLayoutCreateOrder.setVisibility(View.GONE);
			linearLayoutRefresh.setVisibility(View.GONE);
			linearLayoutAcknowledge.setVisibility(View.GONE);
			linearLayoutAddMerchant.setVisibility(View.GONE);
			linearLayoutStockEntry.setVisibility(View.GONE);
			linearLayoutdump2.setVisibility(View.VISIBLE);
			linearLayoutdump3.setVisibility(View.VISIBLE);
			linearLayoutdump1.setVisibility(View.VISIBLE);
			linearLayoutdump4.setVisibility(View.VISIBLE);
			linearLayoutDistanceCalculation.setVisibility(View.GONE);
			linearLayoutdownloadscheme.setVisibility(View.GONE);
			linearLayoutsurveyuser.setVisibility(View.GONE);
			linearLayoutMySales.setVisibility(View.GONE);
		} else {
			linearLayoutMyDealers.setVisibility(View.VISIBLE);
			linearLayoutProducts.setVisibility(View.GONE);
			linearLayoutExport.setVisibility(View.GONE);
			linearLayoutCreateOrder.setVisibility(View.VISIBLE);
			linearLayoutPayment.setVisibility(View.GONE);
			linearLayoutRefresh.setVisibility(View.VISIBLE);
			linearLayoutDistanceCalculation.setVisibility(View.VISIBLE);
			linearLayoutMyDealers.setVisibility(View.GONE);
			linearLayoutMyDayPlan.setVisibility(View.VISIBLE);

		}
		menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
		textViewAssetMenuCreateOrder = (TextView) findViewById(R.id.textViewAssetMenuCreateOrder);
		textViewAssetMenuProfile = (TextView) findViewById(R.id.textViewAssetMenuProfile);
		textViewAssetMenuMyOrders = (TextView) findViewById(R.id.textViewAssetMenuMyOrders);
		textViewAssetMenuMyDealers = (TextView) findViewById(R.id.textViewAssetMenuMyDealers);
		textViewAssetMenuProducts = (TextView) findViewById(R.id.textViewAssetMenuProducts);
		textViewAssetMenuPayment = (TextView) findViewById(R.id.textViewAssetMenuPayment);
		textViewAssetMenuComplaint = (TextView) findViewById(R.id.textViewAssetMenuComplaint);
		textViewAssetMenuSignout = (TextView) findViewById(R.id.textViewAssetMenuSignout);
		textViewAssetMenuExport = (TextView) findViewById(R.id.textViewAssetMenuExport);
		textViewAssetMenuClientVisit = (TextView) findViewById(R.id.textViewAssetMenuClientVisit);
		textViewAssetMenuPaymentCollection = (TextView) findViewById(R.id.textViewAssetMenuPaymentCollection);
		textViewClient = (TextView) findViewById(R.id.textViewClient);
        textViewAssetMenuDistanceCalculation= (TextView) findViewById(R.id.textViewAssetMenuDistanceCalculation);
		textViewAssetMenuAddMerchant = (TextView) findViewById(R.id.textViewAssetMenuAddMerchant);
		textViewAssetMenuAcknowledge = (TextView) findViewById(R.id.textViewAssetMenuAcknowledge);
		textViewAssetMenuStockEntry = (TextView) findViewById(R.id.textViewAssetMenuStockEntry);

		textViewAssetMenudownloadscheme = (TextView) findViewById(R.id.textViewAssetMenudownloadscheme);
		textViewAssetMenusurveyuser = (TextView) findViewById(R.id.textViewAssetMenusurveyuser);
		textViewAssetMenuMyDayPlan = (TextView) findViewById(R.id.textViewAssetMenuMyDayPlan);
		textViewAssetMenuMySales = (TextView) findViewById(R.id.textViewAssetMenuMySales);


		textViewName = (EditText) findViewById(R.id.textViewName);
		textViewCompanyName = (EditText) findViewById(R.id.textViewCompanyName);
		textViewMobile = (EditText) findViewById(R.id.textViewMobile);
		textViewAddress = (EditText) findViewById(R.id.textViewAddress);
		textViewEmail = (EditText) findViewById(R.id.textViewEmail);
		textViewPayment = (EditText) findViewById(R.id.textViewPayment);

		linearLayoutMenuParent = (LinearLayout) findViewById(R.id.linearLayoutMenuParent);
		Typeface font = Typeface.createFromAsset(getAssets(),
				"fontawesome-webfont.ttf");
		menuIcon.setTypeface(font);
		textViewAssetMenuCreateOrder.setTypeface(font);
		textViewAssetMenuProfile.setTypeface(font);
		textViewAssetMenuMyOrders.setTypeface(font);
		textViewAssetMenuMyDealers.setTypeface(font);
		textViewAssetMenuProducts.setTypeface(font);
		textViewAssetMenuPayment.setTypeface(font);
		textViewAssetMenuComplaint.setTypeface(font);
		textViewAssetMenuSignout.setTypeface(font);
		textViewAssetMenuExport.setTypeface(font);
		textViewAssetMenuClientVisit.setTypeface(font);
		textViewAssetMenuPaymentCollection.setTypeface(font);
		textViewAssetMenuRefresh.setTypeface(font);
		textViewAssetMenuAcknowledge.setTypeface(font);
		textViewAssetMenuStockEntry.setTypeface(font);
		textViewAssetMenuAddMerchant.setTypeface(font);
		textViewAssetMenusurveyuser.setTypeface(font);
		textViewAssetMenudownloadscheme.setTypeface(font);
		textViewAssetMenuMyDayPlan.setTypeface(font);
		textViewAssetMenuMySales.setTypeface(font);
		textViewAssetMenuDistanceCalculation.setTypeface(font);

		linearLayoutDistStock = (LinearLayout) findViewById(R.id.linearLayoutDistStock);
		textViewAssetMenuDistStock = (TextView) findViewById(R.id.textViewAssetMenuDistStock);
		textViewAssetMenuDistStock.setTypeface(font);

		textViewAssetMenuStockOnly = (TextView) findViewById(R.id.textViewAssetMenuStockOnly);
		textViewAssetMenuStockAudit = (TextView) findViewById(R.id.textViewAssetMenuStockAudit);
		textViewAssetMenuMigration = findViewById(R.id.textViewAssetMenuMigration);

        textViewAssetMenuStockOnly.setTypeface(font);
        textViewAssetMenuStockAudit.setTypeface(font);
        textViewAssetMenuMigration.setTypeface(font);



		databaseHandler = new DatabaseHandler(getApplicationContext());
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();

		Cursor cur1;
		cur1 = databaseHandler.getDetails();
		cur1.moveToFirst();
		mobno = cur1.getString(cur1.getColumnIndex(DatabaseHandler.KEY_username));

		CircularImageView = (CircularImageView) findViewById(R.id.imgProfile);
		imageFilePath1 = new File(Environment.getExternalStorageDirectory(),
				"Profile.jpg");
		spinnerPayment = (Spinner) findViewById(R.id.spinnerPayment);
		EditTextViewName = (EditText) findViewById(R.id.EditTextViewName);
		EditTextViewCompanyName = (EditText) findViewById(R.id.EditTextViewCompanyName);
		EditTextViewMobile = (EditText) findViewById(R.id.EditTextViewMobile);
		EditTextViewEmail = (EditText) findViewById(R.id.EditTextViewEmail);
		EditTextViewAddress = (EditText) findViewById(R.id.EditTextViewAddress);
		buttonSave = (Button) findViewById(R.id.buttonSave);
		buttonEdit = (Button) findViewById(R.id.buttonEdit);
		textViewSalesmanData = (TextView) findViewById(R.id.textViewSalesmanData);
		buttonChangePassword = (Button) findViewById(R.id.buttonChangePassword);


		EditTextViewMobile.setText(mobno);

		if (Constants.USER_TYPE.equals("D")) {
			textViewClient.setText("Client Visit");
		} else {
			textViewClient.setText("My Client Visit");
		}

		if (netCheck() == true) {

			getDetails();

		} else {

			buttonEdit.setVisibility(Button.GONE);
		}

			String cursettings;
			cursettings = databaseHandler.getSalesmanDataUserSetting("SCUSFIELD");
			if(cursettings!=null){
			if (cursettings.equalsIgnoreCase("Y")) {
				textViewSalesmanData.setVisibility(View.VISIBLE);
			}

		}


		buttonChangePassword.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				openDialog();

			}
		});




		buttonEdit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutEdit.setVisibility(View.VISIBLE);
				linearLayoutDetails.setVisibility(View.GONE);


			}
		});


		buttonSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				fullname = EditTextViewName.getText()
						.toString();
				companyName = EditTextViewCompanyName.getText()
						.toString();
				email = EditTextViewEmail.getText()
						.toString();
				address = EditTextViewAddress.getText()
						.toString();
				payment = spinnerPayment.getSelectedItem()
						.toString();

				Log.e(payment, payment);


				if (!fullname.isEmpty() && !fullname.startsWith(" ")) {

					if (!companyName.isEmpty() && !companyName.startsWith(" ")) {

						if (!email.isEmpty() && !email.startsWith(" ") && email.contains("@") && email.contains(".")) {
							if (!address.isEmpty() && !address.startsWith(" ")) {


								sendData();

							} else {

								showAlertDialogToast("Please Enter the valid  address");
								//toastDisplay("Please Enter the valid  address");
							}

						} else {
							showAlertDialogToast("Please Enter the valid email ");
							//toastDisplay("Please Enter the valid email address");
						}
					} else {
						showAlertDialogToast("Please Enter the company name");
						//toastDisplay("Please Enter the company name");
					}
				} else {
					showAlertDialogToast("Please Enter the fullname");
					//toastDisplay("Please Enter the fullname");
				}

			}


		});

		String[] items = new String[]{"Monthly", "Quaterly", "Half Yearly", "Yearly"};
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items);
		spinnerPayment.setAdapter(adapter);
		CircularImageView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(Intent.ACTION_PICK);
				i.setType("image/*");
				startActivityForResult(i, 100);

			}
		});

		menuIcon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (menuCliccked == 0) {
					linearLayoutMenuParent.setVisibility(View.VISIBLE);
					menuCliccked = 1;
				} else {
					linearLayoutMenuParent.setVisibility(View.GONE);
					menuCliccked = 0;
				}

			}
		});

		linearLayoutMenuParent.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
			}
		});


		linearLayoutDistanceCalculation.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

					linearLayoutMenuParent.setVisibility(View.GONE);
					menuCliccked = 0;
					Constants.Merchantname = "";
					Constants.SalesMerchant_Id = "";
					Intent to = new Intent(ProfileActivity.this,
							DistanceCalActivity.class);
					startActivity(to);

			}
		});
		textViewSalesmanData.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				Intent io = new Intent(ProfileActivity.this, AddSalesmanDataActivity.class);
				startActivity(io);
				finish();
			}
		});

		linearLayoutExport.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				Constants.filter = "0";
				if (netCheckwithoutAlert() == true) {
					getDetail("export");
				} else {

					showAlertDialogToast("Please Check Your internet connection");
				}

			}
		});

		linearLayoutRefresh.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				Log.e("Clicked", "Clicked");

				if (netCheck()) {

					final Dialog qtyDialog = new Dialog(ProfileActivity.this);
					qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					qtyDialog.setContentView(R.layout.warning_dialog);

					final Button buttonUpdateOk, buttonUpdateCancel;

					final TextView textQtyValidate;

					buttonUpdateOk = (Button) qtyDialog
							.findViewById(R.id.buttonUpdateOk);
					buttonUpdateCancel = (Button) qtyDialog
							.findViewById(R.id.buttonUpdateCancel);

					textQtyValidate = (TextView) qtyDialog
							.findViewById(R.id.textQtyValidate);
					qtyDialog.show();


					buttonUpdateOk.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							databaseHandler.synTableDelete();
							Constants.refreshscreen = "profile";
							Intent io = new Intent(ProfileActivity.this, com.freshorders.freshorder.ui.RefreshActivity.class);
							startActivity(io);
							finish();
							qtyDialog.dismiss();
						}

					});

					buttonUpdateCancel
							.setOnClickListener(new View.OnClickListener() {

								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub

									qtyDialog.dismiss();

								}
							});

				}else{
					showAlertDialogToast("Please Check Your internet connection");
				}
			}

		});


		linearLayoutMyOrders.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				if(netCheck()==true) {

					getDetail("myorder");
				}else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment before place order");
					} else {

						Intent io = new Intent(ProfileActivity.this,
								SalesManOrderActivity.class);
						io.putExtra("Key", "menuclick");
						Constants.setimage = null;
						startActivity(io);
						finish();
						/*switch (Constants.USER_TYPE) {
							case "D": {
								Intent io = new Intent(ProfileActivity.this,
										DealersOrderActivity.class);
								io.putExtra("Key", "menuclick");
								startActivity(io);
								finish();
								break;
							}
							case "M": {
								Intent io = new Intent(ProfileActivity.this,
										MerchantOrderActivity.class);
								io.putExtra("Key", "menuclick");
								Constants.setimage = null;
								startActivity(io);
								finish();
								break;
							}
							default: {
								Intent io = new Intent(ProfileActivity.this,
										SalesManOrderActivity.class);
								io.putExtra("Key", "menuclick");
								Constants.setimage = null;
								startActivity(io);
								finish();
								break;
							}
						} */

					}
				}
			}
		});


		linearLayoutMyDealers.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);

				if (netCheckwithoutAlert() == true) {
					getDetail("Mydealer");
				} else {

					showAlertDialogToast("Please Check Your internet connection");
				}

			}
		});

		linearLayoutProducts.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);

				if (netCheckwithoutAlert() == true) {
					getDetail("product");
				} else {

					showAlertDialogToast("Please Check Your internet connection");
				}
			}
		});

		linearLayoutCreateOrder.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				MyApplication.getInstance().setTemplate8ForTemplate2(false);

				linearLayoutMenuParent.setVisibility(View.GONE);
				Constants.checkproduct = 0;
				Constants.orderid = "";
				if (netCheckwithoutAlert()) {
					getDetail("createorder");
				}else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment to place order");
					} else {
						Intent io = new Intent(ProfileActivity.this, CreateOrderActivity.class);
						startActivity(io);
						finish();
					}
				}
			}
		});

		linearLayoutPayment.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				if (netCheckwithoutAlert() == true) {
					if (Constants.USER_TYPE.equals("D")) {
						Intent io = new Intent(ProfileActivity.this, DealerPaymentActivity.class);
						startActivity(io);
						finish();
					} else {
						Intent io = new Intent(ProfileActivity.this, PaymentActivity.class);
						startActivity(io);
						finish();
					}
				} else {

					showAlertDialogToast("Please Check Your internet connection");
				}


			}
		});

		linearLayoutComplaint.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				if (netCheckwithoutAlert()) {
					getDetail("postnotes");
				}else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment");
					} else {
						Intent io = new Intent(ProfileActivity.this,
								MerchantComplaintActivity.class);
						startActivity(io);
						finish();
						/*if (Constants.USER_TYPE.equals("D")) {
							Intent io = new Intent(ProfileActivity.this,
									DealersComplaintActivity.class);
							startActivity(io);
							finish();

						} else {
							Intent io = new Intent(ProfileActivity.this,
									MerchantComplaintActivity.class);
							startActivity(io);
							finish();

						}  */
					}
				}
			}
		});

		linearLayoutClientVisit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				if (netCheckwithoutAlert()) {
					getDetail("clientvisit");
				}else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment");
					} else {
						Intent io = new Intent(ProfileActivity.this,
								SMClientVisitHistory.class);
						startActivity(io);
						finish();
						/*if (Constants.USER_TYPE.equals("D")) {
							Intent io = new Intent(ProfileActivity.this,
									DealerClientVisit.class);
							startActivity(io);
							finish();
						} else {
							Intent io = new Intent(ProfileActivity.this,
									SMClientVisitHistory.class);
							startActivity(io);
							finish();
						} */
					}
				}
			}
		});
		linearLayoutPaymentCollection.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				if (netCheckwithoutAlert() == true) {
					getDetail("paymentcoll");
				} else {

					showAlertDialogToast("Please Check Your internet connection");
				}
			}
		});

		linearLayoutStockEntry.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				moveto = "stockentry";
				if (netCheckwithoutAlert() == true) {
					getDetail("stockentry");
				} else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment");
					}else {
						Intent io = new Intent(ProfileActivity.this,
								OutletStockEntry.class);
						startActivity(io);
						finish();
					}
				}


			}
		});

		linearLayoutDistStock.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				moveto = "diststock";
				if (netCheckwithoutAlert() == true) {
					getDetail("diststock");
				} else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment");
					} else {
						Intent io = new Intent(ProfileActivity.this,
								DistributrStockActivity.class);
						startActivity(io);
						finish();
					}
				}

			}
		});

		linearLayoutAcknowledge.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				moveto = "acknowledge";
				if (netCheckwithoutAlert() == true) {
					getDetail("acknowledge");
				} else {

					showAlertDialogToast("Please Check Your internet connection");
				}

			}
		});

		linearLayoutAddMerchant.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (netCheck() == true) {
					linearLayoutMenuParent.setVisibility(View.GONE);
					menuCliccked = 0;

					getDetail("addmerchant");
				} else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment before place order");
					} else {
						Intent to = new Intent(ProfileActivity.this,
								AddMerchantNew.class);
						startActivity(to);
						finish();
					}
				}

			}
		});
		linearLayoutdownloadscheme.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (netCheck()) {

					final Dialog qtyDialog = new Dialog(ProfileActivity.this);
					qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					qtyDialog.setContentView(R.layout.warning_dialog);

					final Button buttonUpdateOk, buttonUpdateCancel;

					final TextView textQtyValidate;

					buttonUpdateOk = (Button) qtyDialog
							.findViewById(R.id.buttonUpdateOk);
					buttonUpdateCancel = (Button) qtyDialog
							.findViewById(R.id.buttonUpdateCancel);

					textQtyValidate = (TextView) qtyDialog
							.findViewById(R.id.textQtyValidate);
					qtyDialog.show();


					buttonUpdateOk.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							databaseHandler.deletescheme();
							Constants.downloadScheme = "profile";
							Intent io = new Intent(ProfileActivity.this, SchemeDownload.class);
							startActivity(io);
							finish();
							qtyDialog.dismiss();
						}

					});

					buttonUpdateCancel
							.setOnClickListener(new View.OnClickListener() {

								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub

									qtyDialog.dismiss();

								}
							});

				} else {
					showAlertDialogToast("Please Check Your internet connection");
				}
			}
		});
		linearLayoutsurveyuser.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				if (netCheckwithoutAlert()) {
					getDetail("surveyuser");
				} else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment before place order");
					} else {
						Intent to = new Intent(ProfileActivity.this, SurveyMerchant.class);
						startActivity(to);
						finish();
					}
				}

			}
		});

		linearLayoutMyDayPlan.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Constants.orderstatus = "Success";
				Constants.checkproduct = 0;
				linearLayoutMenuParent.setVisibility(View.GONE);

				if (netCheckwithoutAlert() == true) {
					getDetail("mydayplan");
				} else {

					showAlertDialogToast("Please Check Your internet connection");
				}
			}
		});

		linearLayoutMySales.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Constants.orderstatus = "Success";
				Constants.checkproduct = 0;
				linearLayoutMenuParent.setVisibility(View.GONE);

				if (netCheckwithoutAlert() == true) {
					getDetail("mysales");
				} else {

					showAlertDialogToast("Please Check Your internet connection");
				}
			}
		});

		linearLayoutSignout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				// TODO Auto-generated method stub
				databaseHandler.delete();
				Intent io = new Intent(ProfileActivity.this, SigninActivity.class);
				startActivity(io);
				finish();
			}
		});

		linearLayoutMigration.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				Intent io = new Intent(ProfileActivity.this,
						PendingDataMigrationActivity.class);
				startActivity(io);
			}
		});

		linearLayoutStockOnly.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				Constants.checkproduct = 0;
				Constants.orderid = "";
				Constants.SalesMerchant_Id = "";
				Constants.Merchantname = "";
				MyApplication.getInstance().setTemplate8ForTemplate2(true);
				if (netCheck()) {
					Intent io = new Intent(ProfileActivity.this,
							ClosingStockDashBoardActivity.class);
					startActivity(io);
					finish();
				} else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals("") || PaymentStatus.equalsIgnoreCase("null")) {
						showAlertDialogToast("Please make payment before place order");
					} else {
						Intent io = new Intent(ProfileActivity.this,
								ClosingStockDashBoardActivity.class);
						startActivity(io);
						finish();
					}
				}
			}
		});

		linearLayoutStockAudit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				MyApplication.getInstance().setTemplate8ForTemplate2(false);
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				Constants.checkproduct = 0;
				Constants.orderid = "";
				Constants.SalesMerchant_Id = "";
				Constants.Merchantname = "";
				if (netCheck()) {
					Intent io = new Intent(ProfileActivity.this,
							ClosingStockAudit.class);
					startActivity(io);
					finish();
				} else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals("") || PaymentStatus.equalsIgnoreCase("null")) {
						showAlertDialogToast("Please make payment before place order");
					} else {
						Intent io = new Intent(ProfileActivity.this,
								ClosingStockAudit.class);
						startActivity(io);
						finish();
					}
				}
			}
		});

		/////////////
		showMenu();
		///////////

	}

    private void showSoftInput(View view) {
        view.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    private void changepassword(final String newpassword,final String oldpassword,
						final String confirmpassword) {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog= ProgressDialog.show(ProfileActivity.this, "",
						"Loading...", true, true);


			}

			@Override
			protected void onPostExecute(Void result) {


				try {
					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {
                        edittextConfirmPassword.setText(null);
						edittextOldPassword.setText(null);
						edittextNewPassword.setText(null);
                        showSoftInput(edittextOldPassword);
						showAlertDialogToast(strMsg);
                    }
                    else {
                        showAlertDialogToast(strMsg);
                    }

                    dialog.dismiss();

				} catch (JSONException e) {
					e.printStackTrace();
				}

			}

			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();


				try {

                    jsonObject.put("userid",Constants.USER_ID);
					jsonObject.put("newpassword", newpassword);
					jsonObject.put("oldpassword", oldpassword);
					jsonObject.put("confirmpassword", confirmpassword);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


				JsonServiceHandler = new JsonServiceHandler(Utils.strchangepasswordUrl,jsonObject.toString(), ProfileActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceData();
				return null;
			}
		}.execute(new Void[] {});

	}



	public void openDialog(){

		final Dialog dialog;
		dialog=new Dialog(this);
		dialog.setContentView(R.layout.change_password_activity);
		edittextOldPassword=(EditText)dialog.findViewById(R.id.edittextOldPassword);
		edittextNewPassword=(EditText)dialog.findViewById(R.id.edittextNewPassword);
		edittextConfirmPassword=(EditText)dialog.findViewById(R.id.edittextConfirmPassword);
		Button buttonPasswordCancel=(Button)dialog.findViewById(R.id.buttonPasswordCancel);
		Button buttonPasswordUpdate=(Button)dialog.findViewById(R.id.buttonPasswordUpdate);
		buttonPasswordCancel.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		buttonPasswordUpdate.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				String oldpassword = edittextOldPassword.getText().toString();
				String newpassword = edittextNewPassword.getText().toString();
				String confirmpassword = edittextConfirmPassword.getText().toString();

				if (!oldpassword.isEmpty() && !oldpassword.startsWith(" "))
				{
					if (!newpassword.isEmpty() && !newpassword.startsWith(" "))
					{
						if (!confirmpassword.isEmpty() && !confirmpassword.startsWith(" ")) {
							if (confirmpassword.equals(newpassword)) {
								changepassword(newpassword, oldpassword, confirmpassword);
								dialog.dismiss();
							} else {
								showAlertDialogToast("Password does not match");
								//			EditTextViewMobile.setText("");

							}

						}
						else{
							showAlertDialogToast("Confirm Password should not be empty");
							//			EditTextViewMobile.setText("");
						}
					} else {
						showAlertDialogToast("New Password should not be empty");
						//		EditTextViewPassword.setText("");
					}

				} else {
					showAlertDialogToast("Old Password should not be empty");
					//		EditTextViewPassword.setText("");
				}

			}
		});
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		dialog.show();

	}

	private void getDetail(final String moveto) {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog = ProgressDialog.show(ProfileActivity.this, "",
						"Loading...", true, true);

			}

			@Override
			protected void onPostExecute(Void result) {

				try {

					PaymentStatus = (JsonAccountObject.getString("pymtstatus"));
					Log.e("PaymentStatus", PaymentStatus);

					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment to place order");
					} else {
						if (moveto.equals("product")) {

							Intent to = new Intent(ProfileActivity.this,
									ProductActivity.class);
							startActivity(to);
							finish();

						} else if (moveto.equals("export")) {


							Intent to = new Intent(ProfileActivity.this,
									ExportActivity.class);

							startActivity(to);
							finish();

						} else if (moveto.equals("myorder")) {

							if (Constants.USER_TYPE.equals("D")) {
								Intent io = new Intent(ProfileActivity.this,
										DealersOrderActivity.class);
								startActivity(io);
								finish();

							} else if (Constants.USER_TYPE.equals("M")) {
								Intent io = new Intent(ProfileActivity.this,
										MerchantOrderActivity.class);
								startActivity(io);
								finish();

							} else {
								Intent io = new Intent(ProfileActivity.this,
										SalesManOrderActivity.class);
								startActivity(io);
								finish();

							}

						} else if (moveto.equals("createorder")) {
							Intent io = new Intent(ProfileActivity.this,
									CreateOrderActivity.class);
							startActivity(io);
							finish();

						} else if (moveto.equals("Mydealer")) {
							Intent io = new Intent(ProfileActivity.this,
									MyDealersActivity.class);
							io.putExtra("Key", "MenuClick");
							startActivity(io);
							finish();

						} else if (moveto.equals("postnotes")) {
							if (Constants.USER_TYPE.equals("D")) {
								Intent io = new Intent(ProfileActivity.this,
										DealersComplaintActivity.class);
								startActivity(io);
								finish();

							} else {
								Intent io = new Intent(ProfileActivity.this,
										MerchantComplaintActivity.class);
								startActivity(io);
								finish();

							}

						} else if (moveto.equals("createorder")) {

							Intent io = new Intent(ProfileActivity.this,
									CreateOrderActivity.class);
							startActivity(io);
							finish();

						} else if (moveto.equals("clientvisit")) {

							if (Constants.USER_TYPE.equals("D")) {
								Intent io = new Intent(ProfileActivity.this,
										DealerClientVisit.class);
								startActivity(io);
								finish();
							} else {
								Intent io = new Intent(ProfileActivity.this,
										SMClientVisitHistory.class);
								startActivity(io);
								finish();
							}
						} else if (moveto.equals("paymentcoll")) {

							Intent io = new Intent(ProfileActivity.this,
									SalesmanPaymentCollectionActivity.class);
							startActivity(io);
							finish();

						} else if (moveto.equals("addmerchant")) {

							Intent io = new Intent(ProfileActivity.this,
									AddMerchantNew.class);
							startActivity(io);
							finish();

						} else if (moveto.equals("acknowledge")) {

							Intent io = new Intent(ProfileActivity.this,
									SalesmanAcknowledgeActivity.class);
							startActivity(io);
							finish();

						} else if (moveto.equals("stockentry")) {

							Intent io = new Intent(ProfileActivity.this,
									OutletStockEntry.class);
							startActivity(io);
							finish();

						} else if (moveto.equals("surveyuser")) {
							Intent to = new Intent(ProfileActivity.this,
									SurveyMerchant.class);
							startActivity(to);
							finish();
						} else if (moveto.equals("mydayplan")) {
							Intent to = new Intent(ProfileActivity.this,
									MyDayPlan.class);
							startActivity(to);
							finish();
						} else if (moveto.equals("mysales")) {
							Intent to = new Intent(ProfileActivity.this,
									MySalesActivity.class);
							startActivity(to);
							finish();
						} else if (moveto.equals("diststock")) {
							Intent io = new Intent(ProfileActivity.this,
									DistributrStockActivity.class);
							startActivity(io);
							finish();
						}

					}

					dialog.dismiss();

				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
				}

			}

			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();


				JsonServiceHandler = new JsonServiceHandler(Utils.strGetProfileDetail + Constants.USER_ID, ProfileActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceDataGet();
				return null;
			}
		}.execute(new Void[]{});
	}

	private void getDetails() {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog = ProgressDialog.show(ProfileActivity.this, "",
						"Loading...", true, true);

			}

			@Override
			protected void onPostExecute(Void result) {

				try {


					EditTextViewName.setText(JsonAccountObject.getString("fullname"));
					EditTextViewCompanyName.setText(JsonAccountObject.getString("companyname"));

					EditTextViewEmail.setText(JsonAccountObject.getString("emailid"));
					EditTextViewAddress.setText(JsonAccountObject.getString("address"));
					try {
						String imageURL = Utils.strProfilePicPrefix + JsonAccountObject.getString("profileimg");
						imageloader.DisplayImage(imageURL, CircularImageView);


					} catch (Exception e) {
						// TODO Auto-generated catch block
						Log.e("ProductListAdapter", "exception1 " + e);
					}


					textViewName.setText(JsonAccountObject.getString("fullname"));
					textViewCompanyName.setText(JsonAccountObject.getString("companyname"));
					textViewMobile.setText(JsonAccountObject.getString("mobileno"));
					textViewAddress.setText(JsonAccountObject.getString("address"));
					textViewEmail.setText(JsonAccountObject.getString("emailid"));
					textViewPayment.setText(JsonAccountObject.getString("pymttenure"));
					ContentValues contentValue = new ContentValues();
					contentValue.put(DatabaseHandler.KEY_username, mobno);
					contentValue.put(DatabaseHandler.KEY_name, JsonAccountObject.getString("fullname"));
					contentValue.put(DatabaseHandler.KEY_payment, JsonAccountObject.getString("pymttenure"));
					databaseHandler.updateUserDetails(contentValue);
					Constants.PAYMENT = JsonAccountObject.getString("pymttenure");

					dialog.dismiss();
				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					//Log.e("ProductActivityException", e.toString());
				}

			}

			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();


				JsonServiceHandler = new JsonServiceHandler(Utils.strGetProfileDetail + Constants.USER_ID, ProfileActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceDataGet();
				return null;
			}
		}.execute(new Void[]{});
	}
	private void sendData() {
		// TODO Auto-generated method stub
		SendDetails(fullname, address, email, companyName, payment);//,jobi
	}

	private void SendDetails(final String fullname2, final String address2, final String email2,
							 final String companyName2, final String payment2) {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog = ProgressDialog.show(ProfileActivity.this, "In Progress",
						"Validating...", true, true);
			}

			@Override
			protected void onPostExecute(Void result) {
				try {
					// JsonAccountArray =
					// JsonAccountObject.getJSONArray("response");
					// for (int i = 0; i < JsonAccountArray.length(); i++) {
					// JSONObject job=new JSONObject();
					// job=JsonAccountArray.getJSONObject(0);
					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);

					if (strStatus.equals("true")) {
						showAlertDialogToast(strMsg);
						//toastDisplay("" + strMsg);
						ContentValues contentValue = new ContentValues();
						contentValue.put(DatabaseHandler.KEY_username, mobno);
						contentValue.put(DatabaseHandler.KEY_name, fullname2);
						databaseHandler.updateUserDetails(contentValue);

						getDetails();
						linearLayoutEdit.setVisibility(View.GONE);
						linearLayoutDetails.setVisibility(View.VISIBLE);
						
						
						
					/*	if (Constants.USER_TYPE.equals("D")) {
							Intent intent=new Intent(ProfileActivity.this,DealersOrderActivity.class);
							
							startActivity(intent);
							overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
							finish();
						} else {
							Intent intent=new Intent(ProfileActivity.this,MerchantOrderActivity.class);
							
							startActivity(intent);
							overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
							finish();
							
						}  */

					} else {
						showAlertDialogToast(strMsg);
						//toastDisplay(strMsg);

					}

				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					Log.e("loginActivityException", e.toString());
				}

				dialog.dismiss();
			}

			@Override
			protected Void doInBackground(Void... params) {
				dialog.show();

				JSONObject jsonObject = new JSONObject();
				try {

					jsonObject.put("fullname", fullname2);
					jsonObject.put("address", address2);
					jsonObject.put("emailid", email2);
					jsonObject.put("companyname", companyName2);
					jsonObject.put("pymttenure", payment2);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				JsonServiceHandler = new JsonServiceHandler(
						Utils.strAddProfileDetail + Constants.USER_ID, jsonObject.toString(),
						ProfileActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceData();
				return null;
			}
		}.execute(new Void[]{});
	}

	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == 100) { // image
				imageDecode(imageFilePath1, data, 1);
				Log.e("data", "" + data);
			} else if (requestCode == 111) {
				String path = data.getStringExtra(CropImage.IMAGE_PATH);
				if (path == null) {
					return;
				}
				imagePath1 = path;
				Bitmap b = BitmapFactory.decodeFile(imageFilePath1.getPath());
				CircularImageView.setImageBitmap(b);

				DocUploadFileToServer ufs = new DocUploadFileToServer(ProfileActivity.this,
						imagePath1, Constants.USER_ID);//,jobi
				ufs.execute();
				// imageViewAdded1.setBackground(null);
				try {
					CircularImageView.setBackground(null);
				} catch (NoSuchMethodError e) {
					try {
						CircularImageView.setBackgroundDrawable(null);
					} catch (NoSuchMethodError e1) {
						e1.printStackTrace();
					}
				}

			}
		}
	}

	public void imageDecode(File iPath, Intent data, int i) {

		try {
			if (iPath.exists()) {
				iPath.delete();
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		try {
			 Uri selectedImage = data.getData();
			 Log.e("imageDecode", selectedImage.toString());
			 InputStream inputStream = getApplicationContext()
					.getContentResolver().openInputStream(data.getData());
			 FileOutputStream fileOutputStream = new FileOutputStream(iPath);
			 copyStream(inputStream, fileOutputStream);
			 fileOutputStream.close();
			 inputStream.close();
			 Intent intent = new Intent(ProfileActivity.this,
					CropImage.class);
			 intent.putExtra(CropImage.IMAGE_PATH, iPath.getPath());
			 intent.putExtra(CropImage.SCALE, true);
			 intent.putExtra(CropImage.ASPECT_X, 1);
			 intent.putExtra(CropImage.ASPECT_Y, 1);
			 intent.putExtra(CropImage.OUTPUT_X, 600);
			 intent.putExtra(CropImage.OUTPUT_Y, 600);
			 if (i == 1) {
				startActivityForResult(intent, 111);
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void copyStream(InputStream input, OutputStream output)
			throws IOException {

		byte[] buffer = new byte[1024];
		int bytesRead;
		while ((bytesRead = input.read(buffer)) != -1) {
			output.write(buffer, 0, bytesRead);
		}
	}

	protected void toastDisplay(String msg) {
		Toast toast = Toast.makeText(ProfileActivity.this, msg,
				Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

	}

	public boolean netCheck() {
		// for network connection
		try {
			ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetwork = connectionManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			Object result = null;
			if (mWifi.isConnected() || mNetwork.isConnected()) {
				return true;
			} else if (result == null) {
				showAlertDialog(ProfileActivity.this,
						"No Internet Connection",
						"Please Check Your internet connection.", false);
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public boolean netCheckwithoutAlert() {
		// for network connection
		try {
			ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetwork = connectionManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			Object result = null;
			if (mWifi.isConnected() || mNetwork.isConnected()) {
				return true;
			} else if (result == null) {

				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public void showAlertDialog(Context context, String title, String message,
								Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
					}
				});
		alertDialog.show();
	}

	public void showAlertDialogToast(String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
					}
				});


		AlertDialog alert11 = builder1.create();

		alert11.show();

		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

	}

	public void onBackPressed() {
		exitAlret();
	}

	private void exitAlret() {
		AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
		localBuilder.setCancelable(false);
		localBuilder.setMessage("Do you want to Exit?");
		localBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {

						finish();

					}
				});
		localBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {
						paramDialogInterface.cancel();
					}
				});
		localBuilder.create().show();
	}


}







