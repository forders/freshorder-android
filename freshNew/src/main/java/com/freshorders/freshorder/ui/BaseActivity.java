package com.freshorders.freshorder.ui;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.helper.UIHelper;
import com.freshorders.freshorder.receiver.NetworkConnectReceiver;
import com.freshorders.freshorder.service.MigrationResetBroadCast;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.service.ServiceClass;
import com.freshorders.freshorder.service.ServiceForegroundMigration;
import com.freshorders.freshorder.toonline.BendingOrderMigrateToOnline;
import com.freshorders.freshorder.toonline.Migration;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.Dialogs;
import com.freshorders.freshorder.utils.NetworkUtil;
import com.freshorders.freshorder.utils.Utils;

import static com.freshorders.freshorder.toonline.Migration.KEY_IS_PKD_MIGRATION_START;
import static com.freshorders.freshorder.ui.SalesManOrderActivity.showAlertDialogToast;

public class BaseActivity extends Activity implements NetworkConnectReceiver.ConnectivityReceiverListener
        , UIHelper.IDBHelperProgressDialog {

    private ProgressDialog dialog;
    //Kumaravel
    private ProgressDialog pDialog;
    private static boolean isMigDialogShow = false;

    private static String TAG = BaseActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyApplication.getInstance().setConnectivityListener(this);
        MyApplication.activityResumed();
        if (Utils.isNetWorkAvailable(this)) {
            syncData();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        MyApplication.activityPaused();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if(isConnected){
            syncData();
        }
    }

    private void syncData() {
        Context mContext = this.getApplicationContext();
        if (MyApplication.isActivityVisible()) {
            //TODO start sync
            final UIHelper helper = new UIHelper(this, this);
            helper.execute();
            boolean isMigrationSet = new PrefManager(this.getApplicationContext()).getBooleanDataByKey(PrefManager.KEY_IS_MIGRATION_SET);
            if (!isMigrationSet) {
                pendingOrderMigration();
                /////startReset(this.getApplicationContext());////////////
            }

            if(!ServiceClass.isServiceRunning(ServiceForegroundMigration.class, mContext)) {
                Log.e(TAG, ".....Foreground Migration Not Started.....");
            }else {
                Log.e(TAG, ".....Already Foreground Migration Working.....");
            }

            ///// Migration Work Start here///////////////////
            Migration migration = new Migration(mContext);
            boolean isNeedMigration = migration.isNeedForegroundMigration();
            if (isNeedMigration) {
                if(NetworkUtil.getConnectivityStatus(mContext) == 1 ||
                        NetworkUtil.getConnectivityStatus(mContext) == 2) {
                    if(!migration.isMigrationServiceWorking()) {
                        boolean isFullMig = new PrefManager(mContext).getBooleanDataByKey(PrefManager.KEY_IS_FULL_MIGRATION_SET);
                        if(!isFullMig) {
                            if(!isMigDialogShow) {
                                isMigDialogShow = true;
                                startPendingDataMove(migration, mContext);
                                ///migration.startMigrationByAlarmManager(mContext); ///////////////////////////////////////////////////
                            }
                        }else {
                            Log.e(TAG, "Migration Already Working");
                        }
                    }else {
                        Log.e(TAG, "Migration Alarm Service Already Working");
                    }
                }else {
                    Log.e(TAG, "Migration Alarm Service No Mobile Data Found");
                }
            }else {
                Log.e(TAG, "Migration Alarm Service Not Required");
            }

            /*if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M){
                if(!ServiceClass.isServiceRunning(ServiceForegroundMigration.class, mContext)) {
                    boolean isNeedMigration = migration.isNeedForegroundMigration();
                    if (isNeedMigration) {
                        mContext.startService(new Intent(mContext, ServiceForegroundMigration.class));
                    }
                }else {
                    Log.e(TAG, ".....Already Foreground Migration Working.....");
                }
            }else {
                if(!ServiceClass.isServiceRunning(ServiceBackgroundMigration.class, mContext)) {
                    mContext.startService(new Intent(mContext, ServiceBackgroundMigration.class));
                }else {
                    Log.e(TAG, ".....Already Background Migration Working.....");
                }
                startMigrationByAlarmManager(mContext);
            }   */
        }
    }


    private synchronized void startPendingDataMove(final Migration migration, final Context mContext){
        new Dialogs(this).showDialogOKCancel(
                "Uploading of Data.",
                "Would you like to move data from mobile to cloud now?",
                "Yes start now",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        isMigDialogShow = false;
                        dialog.dismiss();
                        migration.startMigrationByAlarmManager(mContext); ///////////////////////////////////////////////////
                    }
                },
                "Not now, later",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        isMigDialogShow = false;
                        dialog.dismiss();
                    }
                },
                false
        );
    }



    // Kumaravel //this function is use for move pending order to server
    private synchronized void pendingOrderMigration(){
        try{
            DatabaseHandler databaseHandler = new DatabaseHandler(this.getApplicationContext());
            Cursor curOrder = databaseHandler.isOrderHeaderForNewUser();
            Cursor curs = databaseHandler.getorderheader();
            if(curs.getCount() > 0){
                if(curOrder != null && curOrder.getCount()>0){
                    if((curs.getCount() - curOrder.getCount()) <= 0) {
                        return; // prevent order to move with mew user
                    }
                }
                Log.e("DataMigration","started.............");
                // Do Migration
                showD("");
                new BendingOrderMigrateToOnline(this.getApplicationContext(), new BendingOrderMigrateToOnline.ServerMigrationListener() {
                    @Override
                    public void onMigrationFinished(boolean isSuccess, String statusMSG, boolean isException, final String excMSG) {
                        hideD();

                        new PrefManager(BaseActivity.this.getApplicationContext()).setBooleanDataByKey(PrefManager.KEY_IS_MIGRATION_SET, false);

                        if (MyApplication.isActivityVisible()) {
                            if (isSuccess) {
                                Toast.makeText(MyApplication.getInstance().getApplicationContext(), Constants.MIGRATION_SUCCESS, Toast.LENGTH_LONG).show();
                                BaseActivity.this.runOnUiThread(new Runnable() { // this is used to reload current activity for order correct status
                                    @Override
                                    public void run() {
                                        Intent intent = getIntent();
                                        finish();
                                        startActivity(intent);
                                    }
                                });
                            } else {
                                Handler handler = new Handler(Looper.getMainLooper());
                                handler.postDelayed(new Runnable() {
                                    public void run() {
                                        showAlertDialogToast(Constants.MIGRATION_FAILED + excMSG);
                                    }
                                }, 50);
                            }
                        }
                        /*Runnable runnable = new Runnable() {
                            public void run() {
                                System.out.println("New Client Start Migration !!");
                                startNewClientMigration();///////////
                            }
                        };
                        ScheduledExecutorService service = Executors
                                .newSingleThreadScheduledExecutor();
                        service.schedule(runnable, 7, TimeUnit.SECONDS);  */ // hide By Kumaravel Because All Migration Start As Service
                    }
                }).migration();
            }else {
                Log.e("Migration","NO Order Migrate");
            }
        }catch (Exception e){
            e.printStackTrace();
            hideD();
        }
    }

    public void startReset(Context context){

        Intent intent = new Intent(context, MigrationResetBroadCast.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 234324243, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if (alarmManager != null) {
            alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + (4 * 60 * 1000), pendingIntent);
            Toast.makeText(context, "Migration Reset Started",Toast.LENGTH_LONG).show();
        }else {
            Toast.makeText(context, "Migration Reset Started Failed",Toast.LENGTH_LONG).show();
        }

    }

    private void postNoteMigration(){
        try{
            new Migration(BaseActivity.this.getApplicationContext()).doPostNoteMigration();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void PKDMigration(){
        try {
            boolean isMigrationStarted = new PrefManager(BaseActivity.this.getApplicationContext()).getBooleanDataByKey(KEY_IS_PKD_MIGRATION_START);
            if(!isMigrationStarted){
                DatabaseHandler databaseHandler = new DatabaseHandler(this.getApplicationContext());
                Cursor cursor = databaseHandler.getPKDPending();
                if(cursor != null) {
                    if (cursor.getCount() > 0) {
                        new Migration(BaseActivity.this.getApplicationContext()).doPKDMigration();
                    }else {
                        Log.e(TAG, "No PKD Data Found To Migrate");
                    }
                }
            }else {
                Log.e(TAG, "PKD Migration Already Running");
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG, "PKD Migration Start failed Or Some think Wrong");
        }
    }





    private void showD(String msg){
        pDialog = ProgressDialog.show(this, "",
                Constants.DATA_MIGRATION_TO_SERVER, true, false);
    }

    private void hideD(){
        if(pDialog != null){
            if(pDialog.isShowing()){
                pDialog.dismiss();
            }
        }
    }


    @Override
    public void showDialog(String  msg) {
        if(dialog == null){
            dialog = ProgressDialog.show(this, "",
                    msg, true, true);
        }
    }

    @Override
    public void closeDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }
}
