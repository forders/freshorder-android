package com.freshorders.freshorder.ui;

import android.app.AlertDialog;

import androidx.lifecycle.ProcessLifecycleOwner;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;

import com.freshorders.freshorder.adapter.ClosingStockAuditTemp;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.MenuItemCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.asyntask.ClosingStockAuditAsyncTask;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.MDealerCompDropdownDomain;
import com.freshorders.freshorder.model.StockAuditDisplayModel;
import com.freshorders.freshorder.popup.PopupClosingStockAudit;
import com.freshorders.freshorder.utils.ApplicationObserver;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Log;
import com.freshorders.freshorder.utils.Utils;

import org.json.JSONException;
import org.json.JSONStringer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import static android.widget.LinearLayout.VERTICAL;

public class ClosingStockAudit extends AppCompatActivity {

    public interface PopupCloseI{
        void dismiss();
    }


    private static String TAG = ClosingStockAudit.class.getSimpleName();
    private Context mContext;
    private SwipeRefreshLayout swipeContainer;
    PopupClosingStockAudit popup;

    private String M_USER_ID = "";

    SwipeRefreshLayout.OnRefreshListener swipeRefreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            Log.i(TAG, "onRefresh called from SwipeRefreshLayout");
            // This method performs the actual data-refresh operation.
            // The method calls setRefreshing(false) when it's finished.
            //loadData();
            swipeContainer.setRefreshing(false);
        }
    };

    private RelativeLayout root;

    ArrayList<MDealerCompDropdownDomain> arrayListDealerDomain, arrayListSearch, arrayListmerchant;
    ArrayList<String> arrayListDealer;

    private DrawerLayout mDrawerLayout;
    private NavigationView navigationView;
    private Toolbar toolbar;
    private LinearLayout lineHeaderProgress ;
    public static int navItemIndex = 0;
    private int home_page;

    private RecyclerView mRecyclerView;
    ////////////////////private ClosingStockAuditAdapter adapter;
    private ClosingStockAuditTemp adapter;

    DatabaseHandler databaseHandler;

    private Spinner spinUser;
    private Button btnSubmit, btnClear;

    List<String> listMerchant;
    ArrayAdapter<String> adapterSpin;
    String[] arrSpinUser = {
            "Salesman1",
            "Salesman2",
            "Salesman3",
            "Salesman4"
    };
    List<StockAuditDisplayModel> stockAuditList;
    LinkedHashMap<Integer,Integer> enteredList;
    private LinkedHashMap<Integer,Integer> enteredStockList;//////////////need to remove only temp
    LinkedHashMap<Integer,StockAuditDisplayModel> enteredListPair;

    public static String strMsg = "", PaymentStatus = "null", Orderno;
    public static String orderno, pushstatus, productstatus, offlineorderid, followup = "No", endtime,
            startTime, beatMerchants, userBeatModel = "";

    String dayOfTheWeek = "";

    private void show(){
        lineHeaderProgress.setVisibility(View.VISIBLE);
    }

    private void hide(){
        lineHeaderProgress.setVisibility(View.GONE);
    }


    View.OnClickListener confirmListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(enteredList.size() > 0){
                popup = new PopupClosingStockAudit(root,
                        mContext,
                        stockAuditList,
                        enteredList,
                        enteredStockList,
                        enteredListPair, new PopupCloseI() {
                    @Override
                    public void dismiss() {
                        refreshRVData();
                    }
                });
                popup.showPopup();
            }else {
                showAlertDialogToast("Please enter stock value");
            }
        }
    };

    View.OnClickListener clearListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            enteredList.clear();
            enteredListPair.clear();
            enteredStockList.clear();
            refreshRVData();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_closing_stock_audit);
        mContext = ClosingStockAudit.this;
        stockAuditList = new ArrayList<>();
        enteredList = new LinkedHashMap<>();
        enteredStockList = new LinkedHashMap<>();//////////////need to remove only temp
        enteredListPair = new LinkedHashMap<>();

        if(databaseHandler == null){
            databaseHandler = new DatabaseHandler(getApplicationContext());
        }
        // register observer
        ProcessLifecycleOwner.get().getLifecycle().addObserver(new ApplicationObserver());

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setHomeAsUpIndicator(R.drawable.ic_menu_green_24dp);
            ab.setDisplayHomeAsUpEnabled(true);
        }

        root = findViewById(R.id.frame_container_attendance);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        lineHeaderProgress = findViewById(R.id.lineHeaderProgress_home);
        spinUser = findViewById(R.id.id_audit_spin);
        btnClear = findViewById(R.id.id_audit_btn_clear);
        btnSubmit = findViewById(R.id.id_audit_btn_submit);
        btnSubmit.setOnClickListener(confirmListener);
        btnClear.setOnClickListener(clearListener);

        loadUserDetail();

        // initializing navigation menu
        setUpNavigationView();
        setRecyclerView();

        listMerchant = new ArrayList<>(Arrays.asList(arrSpinUser));
        adapterSpin = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, listMerchant);
        spinUser.setAdapter(adapterSpin);
        spinUser.setOnItemSelectedListener(new MerchantSelectedListener());
        //loadData();

        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(swipeRefreshListener);

            // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


    }

    private void loadUserDetail() {
        final Cursor cur;
        cur = databaseHandler.getDetails();
        if(cur != null && cur.getCount() > 0) {
            cur.moveToFirst();
            PaymentStatus = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_paymentStatus));
            Constants.USER_TYPE = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_usertype));
            Constants.USER_ID = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_id));
            Constants.USER_FULLNAME = cur.getString(cur
                    .getColumnIndex(DatabaseHandler.KEY_name));
            Constants.USER_TYPE = "S";
            cur.close();
        }else {
            new Log(mContext).ee(TAG,".......................................databaseHandler.getDetails() return null");
            showAlertDialogToast("Data not properly loaded. Please re-login");
            return;
        }
    }

    private void setRecyclerView(){
        mRecyclerView = findViewById(R.id.id_RV_closing_stock_audit);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        //Add Divider
        DividerItemDecoration itemDecor = new DividerItemDecoration(mRecyclerView.getContext(), VERTICAL);
        mRecyclerView.addItemDecoration(itemDecor);
        //creating recyclerView adapter
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        ////adapter = new ClosingStockAuditAdapter(stockAuditList, enteredList, enteredListPair);  ///comment for temp should un comment when needs
        adapter = new ClosingStockAuditTemp(stockAuditList, enteredList,enteredStockList, enteredListPair);
        mRecyclerView.setAdapter(adapter);
    }

    private void loadData(){
        show1();
        JSONStringer jsonStringer = null;
        try {
            /*jsonStringer=new JSONStringer().object().key("logTime").value("")
                    .key("datas")
                    .array()
                    .object().key("dat1").value("1")
                    .key("dat2").value("3")
                    .key("dat3").value("5")
                    .key("dat4").value("5")
                    .endObject()
                    .endArray()
                    .endObject(); */


            if(!(M_USER_ID != null && M_USER_ID.length() > 0)){
                showAlertDialogToast("Please re-login....");
            }

            jsonStringer = new JSONStringer().object().key("where")
                    .object()
                    .key("auditqty").value(null)
                    /*.key("suserid").value("2680")*/  .key("suserid").value(Constants.USER_ID)
                    .key("muserid").value(M_USER_ID)
                    .endObject()
                    .key("include")
                    .array()
                    .value("product")
                    .endArray()
                    .endObject();




            android.util.Log.e(TAG,"Formed json object......." + jsonStringer);

            /*

            StockAuditViewModel model = ViewModelProviders.of(this).get(StockAuditViewModel.class);

            /*model.getStockList(jsonStringer).observe(this, new Observer<List<StockAuditDisplayModel>>() {
                @Override
                public void onChanged(@Nullable List<StockAuditDisplayModel> stockList) {
                    adapter = new ClosingStockAuditAdapter(stockList);
                    mRecyclerView.setAdapter(adapter);
                }
            });  */ /*
            model.getStockList(jsonStringer.toString(), getApplicationContext(), swipeContainer).observe(this, new Observer<List<StockAuditDisplayModel>>() {
                @Override
                public void onChanged(@Nullable List<StockAuditDisplayModel> stockList) {
                    hide1();
                    if(stockList != null) {
                        android.util.Log.e(TAG, "........" + stockList.size());
                        stockAuditList.addAll(stockList);
                        adapter.notifyDataSetChanged();
                    }else {
                        android.util.Log.e(TAG, "........................");
                        showAlertDialogToast("First import data in WEB ");
                    }
                    //adapter = new ClosingStockAuditAdapter(stockList);
                    //mRecyclerView.setAdapter(adapter);
                }
            });   */

            loadStockItems(jsonStringer.toString(), mContext);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadStockItems(final String jsonStringer, final Context mContext){

        JsonServiceHandler jsonServiceHandler = new JsonServiceHandler(Utils.strClosingStockAudit, jsonStringer, mContext);
        ClosingStockAuditAsyncTask obj = new ClosingStockAuditAsyncTask(jsonServiceHandler, new ClosingStockAuditAsyncTask.GetAuditCompleteListener() {
            @Override
            public void onSuccess(List<StockAuditDisplayModel> object) {
                new Log(mContext).ee("ViewModel","......SUCCESS");
                //stockAuditList.setValue(object);
                stockAuditList.addAll(object);
                adapter.notifyDataSetChanged();
                swipeContainer.setRefreshing(false);
            }

            @Override
            public void onFailed(String msg) {
                new Log(mContext).ee("ViewModel","......FAILED : " + Utils.strClosingStockAudit + jsonStringer);
                //stockAuditList.setValue(null);
                swipeContainer.setRefreshing(false);
                showAlertDialogToast("First import data in WEB ");
            }

            @Override
            public void onException(String msg) {
                new Log(mContext).ee("ViewModel","......EXCEPTION : " + Utils.strClosingStockAudit + jsonStringer);
                //stockAuditList.setValue(null);
                swipeContainer.setRefreshing(false);
                showAlertDialogToast("First import data in WEB ");
            }
        });
        obj.execute();
    }

    private void show1(){
        if( swipeContainer != null) {
            swipeContainer.setRefreshing(true);
        }else {
            Log.e("not",".........................showing");
        }
    }

    private void hide1(){
        if( swipeContainer != null)
            swipeContainer.setRefreshing(false);
    }


    @Override
    protected void onStart() {
        super.onStart();

        if(databaseHandler == null){
            databaseHandler = new DatabaseHandler(getApplicationContext());
        }
        if(ApplicationObserver.fromBackground){
            ApplicationObserver.fromBackground = false;
            android.util.Log.e(TAG," App From Background...........................");
        }else {
            android.util.Log.e(TAG,"Initial Start Method called..........................");
        }

        loadInitialSetup();
        //loadData();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_search, menu);

        MenuItem search = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        search(searchView);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    private void search(SearchView searchView) {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                adapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
    }


    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.nav_home_page:
                        navItemIndex = 0;
                        //startActivity(new Intent(MainActivity.this, HomeNavigationActivity.class));
                        mDrawerLayout.closeDrawers();
                        homePage();
                        return true;
                    case R.id.nav_logout_page:
                        navItemIndex = 0;
                        //startActivity(new Intent(MainActivity.this, HomeNavigationActivity.class));
                        mDrawerLayout.closeDrawers();
                        logoutPage();
                        return true;
                    /*case R.id.nav_profile_page:
                        navItemIndex = 1;
                        //CURRENT_TAG = TAG_PHOTOS;
                        //startActivity(new Intent(MainActivity.this, WorkAssignmentActivity.class));
                        //CURRENT_TAG = TAG_MOVIES;
                        mDrawerLayout.closeDrawers();
                        return true;
                    case R.id.nav_previous_test_page:
                        navItemIndex = 2;
                        //CURRENT_TAG = TAG_PHOTOS;
                        //startActivity(new Intent(MainActivity.this, WorkAssignmentEditActivity.class));
                        //CURRENT_TAG = TAG_MOVIES;
                        mDrawerLayout.closeDrawers();
                        return true;
                    case R.id.nav_notification_page:
                        navItemIndex = 3;
                        //startActivity(new Intent(MainActivity.this, AddOutPersonActivity.class));
                        //CURRENT_TAG = TAG_MOVIES;
                        mDrawerLayout.closeDrawers();
                        return true;
                    case R.id.nav_membership_page:
                        navItemIndex = 4;
                        //startActivity(new Intent(MainActivity.this, EditOutPersonActivity.class));
                        //CURRENT_TAG = TAG_MOVIES;
                        mDrawerLayout.closeDrawers();
                        return true;
                    case R.id.nav_settings_page:
                        navItemIndex = 4;
                        //startActivity(new Intent(MainActivity.this, SettingActivity.class));
                        //CURRENT_TAG = TAG_MOVIES;
                        mDrawerLayout.closeDrawers();
                        return true;
                    case R.id.nav_feedback_page:
                        navItemIndex = 5;
                        //startActivity(new Intent(MainActivity.this, FeedBackActivity.class));
                        //CURRENT_TAG = TAG_MOVIES;
                        mDrawerLayout.closeDrawers();
                        return true;
                    case R.id.nav_about_us_page:
                        // launch new intent instead of loading fragment
                        //startActivity(new Intent(MainActivity.this, AboutUsActivity.class));
                        mDrawerLayout.closeDrawers();
                        return true;
                    case R.id.nav_privacy_policy_page:
                        // launch new intent instead of loading fragment
                        //startActivity(new Intent(MainActivity.this, ServerActivity.class));
                        mDrawerLayout.closeDrawers();
                        return true;  */
                    default:
                        navItemIndex = 0;
                }
                return true;
            }
        });

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        ///////////////////////////////////////////mDrawerLayout.addDrawerListener(actionBarDrawerToggle);/////////
        mDrawerLayout.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    private void logoutPage(){
        databaseHandler.delete();
        Intent io = new Intent(ClosingStockAudit.this, SigninActivity.class);
        startActivity(io);
        finish();
    }

    private void homePage(){
        Intent io = new Intent(ClosingStockAudit.this, SalesManOrderActivity.class);
        startActivity(io);
        finish();
    }

    private void loadInitialSetup(){

        arrayListDealer = new ArrayList<String>();
        arrayListDealerDomain = new ArrayList<MDealerCompDropdownDomain>();
        arrayListmerchant = new ArrayList<MDealerCompDropdownDomain>();
        arrayListSearch = new ArrayList<MDealerCompDropdownDomain>();
        //arraylistmerchantdropdown = new ArrayList<MerchantDropdownDomain>();
        //searchresult = new ArrayList<MerchantDropdownDomain>();

        final Cursor cur;
        cur = databaseHandler.getDetails();
        if(cur != null && cur.getCount() > 0) {
            cur.moveToFirst();
            PaymentStatus = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_paymentStatus));
            Constants.USER_TYPE = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_usertype));
            Constants.USER_ID = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_id));
            Constants.USER_FULLNAME = cur.getString(cur
                    .getColumnIndex(DatabaseHandler.KEY_name));
            Constants.USER_TYPE = "S";
            cur.close();
        }else {
            new Log(mContext).ee(TAG,".......................................databaseHandler.getDetails() return null");
            showAlertDialogToast("Data not properly loaded. Please re-login");
            return;
        }

        Cursor cur1;
        cur1 = databaseHandler.getUserSetting();
        if(cur1 != null && cur1.getCount() > 0) {
            cur1.moveToFirst();
            android.util.Log.e("curCount", ".........................." + cur.getCount());
            userBeatModel = cur1.getString(cur1.getColumnIndex(DatabaseHandler.KEY_settingrefvalue));
            android.util.Log.e("userBeatModel", userBeatModel);
            cur1.close();
        }else {
            new Log(mContext).ee(TAG,".......................................databaseHandler.getUserSetting() return null or Zero");
            showAlertDialogToast("Data not properly loaded. Please re-login");
            return;
        }

        if (userBeatModel.equalsIgnoreCase("C")) {
            android.util.Log.e("userModel C", "userModel C");
            Cursor curr;
            curr = databaseHandler.getDetails();
            curr.moveToFirst();
            String selectedbeat = curr.getString(curr.getColumnIndex(DatabaseHandler.KEY_selectedBeat));
            String selectedDate = curr.getString(curr.getColumnIndex(DatabaseHandler.KEY_selectedDate));
            String todayDate = new SimpleDateFormat(
                    "yyyy-MM-dd").format(new java.util.Date());
            if (todayDate.equals(selectedDate) && !selectedbeat.equals("")) {
                getBeatMerchant(selectedbeat);

            } else {
                Constants.beatassignscreen = "CreateOrder";
                Intent to = new Intent(ClosingStockAudit.this, CompanyBeatActivity.class);
                startActivity(to);
                finish();
            }

        } else {
            android.util.Log.e("userModel D", "userModel D");
            offlineBeat();
        }
    }

    public void offlineBeat() {

        SimpleDateFormat sdf = new SimpleDateFormat("EEEE", Locale.getDefault());
        Date d = new Date();
        dayOfTheWeek = sdf.format(d);
        android.util.Log.e("dateday", dayOfTheWeek);

        Cursor curs;
        curs = databaseHandler.getbeatwithMerchant(dayOfTheWeek);

        int i = 0;

        if (curs != null && curs.getCount() > 0) {
            android.util.Log.e("beatCount", String.valueOf(curs.getCount()));
            if (curs.moveToFirst()) {

                do {
                    android.util.Log.e("beat inside", "beat inside");
                    if (i == 0) {
                        if (curs.getString(0).equals("New User")) {
                            beatMerchants = "'" + curs.getString(0) + "'";
                        } else {
                            beatMerchants = curs.getString(0);
                        }
                        i++;
                    } else {
                        if (curs.getString(0).equals("New User")) {
                            beatMerchants = beatMerchants + "," + "'" + curs.getString(0) + "'";
                        } else {
                            beatMerchants = beatMerchants + "," + curs.getString(0);
                        }
                    }

                    android.util.Log.e("beatMerchants", beatMerchants);


                } while (curs.moveToNext());


            }
            offlinemerchant(beatMerchants);
        } else {
            showAlertDialogToast("No Merchants available for this day");
        }

    }

    public void getBeatMerchant(String beatname) {
        Cursor cursor;
        cursor = databaseHandler.getbeatMerchant(beatname);
        android.util.Log.e("beatname", beatname);
        android.util.Log.e("beatname count", String.valueOf(cursor.getCount()));
        String merchants = "";
        int i = 0;
        if (cursor != null && cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {

                do {
                    if (i == 0) {
                        if (cursor.getString(0).equals("New User")) {
                            merchants = "'" + cursor.getString(0) + "'";
                        } else {
                            merchants = cursor.getString(0);
                        }
                        i++;
                    } else {
                        if (cursor.getString(0).equals("New User")) {
                            merchants = merchants + ", '" + cursor.getString(0) + "'";
                        } else {
                            merchants = merchants + "," + cursor.getString(0);
                        }
                    }

                    android.util.Log.e("merchants", merchants);
                } while (cursor.moveToNext());


            }
        }

        offlinemerchant(merchants);
    }

    public void offlinemerchant(String beatMerchants) {


        android.util.Log.e("string merchant", beatMerchants);
        Cursor curs;
        curs = databaseHandler.getmerchantoncurrentday(beatMerchants);
        if (curs != null && curs.getCount() > 0) {
            android.util.Log.e("merchantCount", String.valueOf(curs.getCount()));
            if (curs.moveToFirst()) {

                do {
                    android.util.Log.e("inside", "inside");
                    MDealerCompDropdownDomain cd = new MDealerCompDropdownDomain();
                    cd.setComName(curs.getString(1));
                    cd.setID(curs.getString(2));
                    cd.setName(curs.getString(3));
                    cd.setOfflineMerchantId(curs.getString(curs.getColumnIndex("oflnordid")));  ///By Kumaravel
                    android.util.Log.e("companyname", curs.getString(1));
                    android.util.Log.e("mid", curs.getString(2));
                    android.util.Log.e("fullname", curs.getString(3));
                    android.util.Log.e("OfflineMerchantId", curs.getString(curs.getColumnIndex("oflnordid")));
                    arrayListDealerDomain.add(cd);
                    arrayListSearch.add(cd);
                    arrayListDealer.add(curs.getString(1));
                    arrayListmerchant.add(cd);

                } while (curs.moveToNext());


            }

        } else {
            showAlertDialogToast("No Products Found");
            return;
        }
        android.util.Log.e("size", String.valueOf(arrayListDealerDomain.size()));
            /*
        android.util.Log.e("size of array", String.valueOf(arrayListDealer.size()));
        ArrayAdapter<String> adapter = new
                ArrayAdapter<String>(ClosingStockAudit.this, R.layout.textview, R.id.textView, arrayListDealer); */

        M_USER_ID = arrayListmerchant.get(0).getID(); //// Only one user for this type customer


        adapterSpin.clear();
        adapterSpin.addAll(arrayListDealer);
        adapterSpin.notifyDataSetChanged();

        //autocompleteTextTraders.setAdapter(adapter);
        //autocompleteTextTraders.setText(Constants.Merchantname);
        spinUser.setSelection(0);
    }

    class MerchantSelectedListener implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            /*selectedSalesmanIndex = pos;
            if(listOfSalesman.size() > 0) {
                selectedSalesman = listOfSalesman.get(selectedSalesmanIndex);
                selectedSalesmanId = listSalesmanId.get(selectedSalesmanIndex);
                Toast.makeText(mContext, selectedSalesman, Toast.LENGTH_LONG).show();
            }  */
            if(arrayListmerchant != null && arrayListmerchant.size() > 0) {
                swipeContainer.setRefreshing(true);
                M_USER_ID = arrayListmerchant.get(pos).getID();
                stockAuditList.clear();
                adapter.notifyDataSetChanged();
                loadData();
            }else {
                showAlertDialogToast("No merchant avail please re-login");
                return;
            }
        }
        public void onNothingSelected(AdapterView<?> parent) {
            // Dummy
        }
    }


    public void showAlertDialogToast(String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
        builder1.setCancelable(false);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    @Override
    public void onBackPressed() {
        if(popup != null){
            if(popup.isPopupVisible()){
                popup.close();
                refreshRVData();
            }
        }
    }

    public void refreshRVData(){
        Log.e("reload","..................Called");
        List<StockAuditDisplayModel> stockAuditListTemp = new ArrayList<>(stockAuditList);
        stockAuditList.clear();
        adapter.notifyDataSetChanged();
        stockAuditList.addAll(stockAuditListTemp);
        adapter.notifyDataSetChanged();
    }

}
