package com.freshorders.freshorder.ui;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.AddDealerListAdapter;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.AddDealerListDomain;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class DealerUpdateProductActivity extends Activity {

	Spinner dropdown;
	TextView textViewAssetBack;
	EditText EditTextViewProductCode, EditTextViewProductName;
	Button buttonSave,buttonImport;
	String productCode, productID, status, productName;
	JSONObject JsonAccountObject = null;
	JSONArray JsonAccountArray = null;
	DatabaseHandler databaseHandler;
	JsonServiceHandler JsonServiceHandler;
	String prodId,prodName,prodCode,prodStatus;
	int dpPosition;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(1);
		setContentView(R.layout.dealer_add_product_screen);
		netCheck();
		dropdown = (Spinner) findViewById(R.id.spinnerStatus);
		textViewAssetBack = (TextView) findViewById(R.id.textViewAssetBack);
		EditTextViewProductCode = (EditText) findViewById(R.id.EditTextViewProductCode);
		EditTextViewProductName = (EditText) findViewById(R.id.EditTextViewProductName);
		buttonSave = (Button) findViewById(R.id.buttonSave);
		buttonImport = (Button) findViewById(R.id.buttonImport);
		Typeface font = Typeface.createFromAsset(getAssets(),
				"fontawesome-webfont.ttf");
		textViewAssetBack.setTypeface(font);

		String[] items = new String[] { "Active", "Inactive" };
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_dropdown_item, items);
		dropdown.setAdapter(adapter);
		
		prodId=getIntent().getExtras().getString("prodId");
		prodName=getIntent().getExtras().getString("prodName");
		prodCode=getIntent().getExtras().getString("prodCode");
		prodStatus=getIntent().getExtras().getString("prodStatus");
		
		Log.e("prodStatus", prodStatus);
		if(prodStatus.equals("Active")){
			dpPosition=0;
		}else{
			dpPosition=1;
		}
		
		EditTextViewProductCode.setText(prodCode);
		EditTextViewProductName.setText(prodName);
		dropdown.setSelection(dpPosition);
		
		textViewAssetBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(DealerUpdateProductActivity.this,
						ProductActivity.class);
				startActivity(intent);
			    finish();  
			}

		});
		buttonImport.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(DealerUpdateProductActivity.this,
						ImportActivity.class);
				startActivity(intent);
			    finish();  
			}

		});
		
		buttonSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				productCode = EditTextViewProductCode.getText().toString();
				productName = EditTextViewProductName.getText().toString();
				status = dropdown.getSelectedItem().toString();
				if (!productCode.isEmpty() && !productCode.startsWith(" ")) {
					if (!productName.isEmpty() && !productName.startsWith(" ")) {

						SaveProduct();
						Log.e("en", "en");

					} else {
						showAlertDialogToast("productName should not be empty");
						//toastDisplay("productName should not be empty");

					}
				} else {
					showAlertDialogToast("productCode should not be empty");
					//toastDisplay("productCode should not be empty");

				}


			}

		});
	}

	private void SaveProduct() {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog = ProgressDialog.show(DealerUpdateProductActivity.this, "",
						"Loading...", true, true);

			}

			@Override
			protected void onPostExecute(Void result) {

				try {

					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);

					if (strStatus.equals("true")) {
						showAlertDialogToast(strMsg);
						//toastDisplay(strMsg);

					} else {
						showAlertDialogToast(strMsg);
						//toastDisplay(strMsg);

					}
					dialog.dismiss();
				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					Log.e("ProductActivityException", e.toString());
				}

			}

			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();

				try {
					jsonObject.put("userid", Constants.USER_ID);
					jsonObject.put("prodcode", productCode);
					jsonObject.put("prodname", productName);
					jsonObject.put("prodstatus",status);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				JsonServiceHandler = new JsonServiceHandler(
						Utils.strDealerUpdateProduct+prodId,jsonObject.toString(),
						DealerUpdateProductActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceData();
				return null;
			}
		}.execute(new Void[] {});

	}

	protected void toastDisplay(String msg) {
		Toast toast = Toast.makeText(DealerUpdateProductActivity.this, msg,
				Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

	}
	/*@Override
	public void onBackPressed()
	{
		Intent intent = new Intent(DealerUpdateProductActivity.this,
				ProductActivity.class);
		startActivity(intent);
	    finish();  
	}*/
	public boolean netCheck() {
		// for network connection
		try {
			ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetwork = connectionManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			Object result = null;
			if (mWifi.isConnected() || mNetwork.isConnected()) {
				return true;
			}

			else if (result == null) {
				showAlertDialog(DealerUpdateProductActivity.this,
						"No Internet Connection",
						"Please Check Your internet connection.", false);
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	public void showAlertDialog(Context context, String title, String message,
			Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
					}
				});
		alertDialog.show();
	}

	public void showAlertDialogToast( String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();

		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

	}

	public void onBackPressed() {
		exitAlret();
	}

	private void exitAlret() {
		AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
		localBuilder.setCancelable(false);
		localBuilder.setMessage("Do you want to Exit?");
		localBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {

						finish();

					}
				});
		localBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {
						paramDialogInterface.cancel();
					}
				});
		localBuilder.create().show();
	}
}
