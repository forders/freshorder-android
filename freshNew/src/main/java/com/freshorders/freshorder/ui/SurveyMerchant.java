package com.freshorders.freshorder.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.EditText;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.activity.DistanceCalActivity;
import com.freshorders.freshorder.activity.SurveyPopupActivity;
import com.freshorders.freshorder.adapter.OutleyStockEntryAdapter;
import com.freshorders.freshorder.adapter.SurveyAdapter;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.MDealerCompDropdownDomain;
import com.freshorders.freshorder.domain.OutletStockEntryDomain;
import com.freshorders.freshorder.domain.SpinnerDomain;
import com.freshorders.freshorder.toonline.migration.PendingDataMigrationActivity;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SurveyMerchant extends Activity implements SurveyAdapter.ListAdapterListener {

    TextView menuIcon, textViewAssetMenuProfile, textViewAssetMenuMyOrders,
            textViewAssetMenuMyDealers, textViewAssetMenuProducts,
            textViewAssetMenuPayment, textViewAssetMenuComplaint,textViewAssetMenuDistanceCalculation,
            textViewAssetMenuSignout, textViewAssetMenuCreateOrder, textViewAssetMenuExport,
            textViewAssetMenuClientVisit, textViewAssetMenuPaymentCollection, textViewAssetMenuRefresh,
            textViewAssetMenuStockEntry, textViewAssetMenuAddMerchant, textViewAssetMenuAcknowledge, textViewAssetMenusurveyuser, textViewAssetMenudownloadscheme, textViewAssetMenuMyDayPlan, textViewAssetMenuMySales,
            textViewAssetMenuDistStock, textViewAssetMenuStockOnly, textViewAssetMenuMigration, textViewAssetMenuStockAudit;

    int menuCliccked;

    LinearLayout linearLayoutMenuParent, linearLayoutProfile, linearLayoutMyOrders,
            linearLayoutMyDealers, linearLayoutProducts, linearLayoutPayment,linearLayoutDistanceCalculation,
            linearLayoutComplaint, linearLayoutSignout, linearLayoutEdit, linearLayoutDetails,
            linearLayoutCreateOrder, linearLayoutExport, linearLayoutClientVisit, linearLayoutPaymentCollection,
            linearlayoutoverallayout, linearLayoutRefresh,
            linearLayoutAcknowledge, linearLayoutStockEntry, linearLayoutAddMerchant, linearlayoutsurvey,
            linearLayoutsurveyuser, linearLayoutdownloadscheme, linearLayoutMyDayPlan, linearLayoutMySales,
            linearLayoutDistStock, linearLayoutStockOnly, linearLayoutMigration, linearLayoutStockAudit;

    private void showMenu(){

        MyApplication app = MyApplication.getInstance();

        if(!app.isCreateOrder()){
            linearLayoutCreateOrder.setVisibility(View.GONE);
        }
        if(!app.isProfile()){
            linearLayoutProfile.setVisibility(View.GONE);
        }
        if(!app.isMyDayPlan()){
            linearLayoutMyDayPlan.setVisibility(View.GONE);
        }
        if(!app.isMyOrders()){
            linearLayoutMyOrders.setVisibility(View.GONE);
        }
        if(!app.isAddMerchant()){
            linearLayoutAddMerchant.setVisibility(View.GONE);
        }
        if(!app.isMySales()){
            linearLayoutMySales.setVisibility(View.GONE);
        }
        if(!app.isPostNotes()){
            linearLayoutComplaint.setVisibility(View.GONE);
        }
        if(!app.isMyClientVisit()){
            linearLayoutClientVisit.setVisibility(View.GONE);
        }
        if(!app.isAcknowledge()){
            linearLayoutAcknowledge.setVisibility(View.GONE);
        }
        if(!app.isPaymentCollection()){
            linearLayoutPaymentCollection.setVisibility(View.GONE);
        }
        if(!app.isPkdDataCapture()){
            linearLayoutStockEntry.setVisibility(View.GONE);
        }
        if(!app.isDistributorStock()){
            linearLayoutDistStock.setVisibility(View.GONE);
        }
        if(!app.isSurveyUser()){
            linearLayoutsurveyuser.setVisibility(View.GONE);
        }
        if(!app.isDownLoadScheme()){
            linearLayoutdownloadscheme.setVisibility(View.GONE);
        }
        if(!app.isDistanceCalculation()){
            linearLayoutDistanceCalculation.setVisibility(View.GONE);
        }
        if(!app.isRefresh()){
            linearLayoutRefresh.setVisibility(View.GONE);
        }
        if(!app.isLogout()){
            linearLayoutSignout.setVisibility(View.GONE);
        }
        if(!app.isClosingStock()){
            linearLayoutStockOnly.setVisibility(View.GONE);
        }
        if(!app.isClosingStockAudit()){
            linearLayoutStockAudit.setVisibility(View.GONE);
        }
        if(!app.isPendingData()){
            linearLayoutMigration.setVisibility(View.GONE);
        }
    }

    public Dialog qtyDialog;
    AutoCompleteTextView autocompletemrchantlist;
    public static TextView textViewcross_1, textviewnodata, textViewQuestion;

    Button buttonpay;

    JSONObject JsonSurvyObject = null;
    JSONArray JsonAccountArray = null;
    public static DatabaseHandler databaseHandler;
    JsonServiceHandler JsonServiceHandler;
    ArrayList<MDealerCompDropdownDomain> arrayListDealerDomain, arrayListSearch, arrayListmerchant;
    ArrayList<String> arrayListDealer;
    public static int smpymntmerchantselected = 0;
    String muserId = "", merchantmobileno = "";
    OutleyStockEntryAdapter adapter;
    public static String PaymentStatus = "NULL", moveto = "NULL", dealername, time = "", dayOfTheWeek, beatMerchants;
    static ArrayList<OutletStockEntryDomain> arraylistDealerOrderList;
    Date dateStr;
    public static String companyname, mname, fullname, date, address, orderId, morderId, onlineorderno,
            Paymentdate = "NULL", salesuserid = "NULL", userBeatModel = "";
    public static OutletStockEntryDomain domainSelected;

    Button buttonsave;

    private RadioGroup radioGroup;
    private RadioButton radioButton;
    SQLiteDatabase db;

    View viewArCustomComponents[]=null;
    LinearLayout ll;
    LinearLayout .LayoutParams lp;
    String customValue="",customID="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.survey_merchant);


        databaseHandler = new DatabaseHandler(getApplicationContext());
        JsonSurvyObject = new JSONObject();
        JsonAccountArray = new JSONArray();

        Cursor curPay;
        curPay = databaseHandler.getDetails();
        if(curPay != null && curPay.getCount() > 0){
            curPay.moveToFirst();
            PaymentStatus = curPay.getString(curPay.getColumnIndex(DatabaseHandler.KEY_paymentStatus));
            curPay.close();
        }

        ///Kumaravel /// For Offline Count
        TextView tvFailedCount = (TextView) findViewById(R.id.id_survey_failed_count);
        TextView tvOfflineCount = (TextView) findViewById(R.id.id_survey_offline_count);
        String strFailedCount = Constants.FAILED_COUNT;
        String strOfflineCount = Constants.OFFLINE_COUNT;

        Button btnViewHistory = findViewById(R.id.id_btn_view_survey);
        btnViewHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent io = new Intent(SurveyMerchant.this, SurveyPopupActivity.class);
                startActivity(io);
            }
        });

        Cursor cur_count = databaseHandler.getPendingSurvey();
        if(cur_count != null && cur_count.getCount() > 0){
            tvOfflineCount.setText((strOfflineCount+cur_count.getCount()));
            cur_count.close();
        }else {
            tvOfflineCount.setText((strOfflineCount+0));
        }
        Cursor cur_failed_count = databaseHandler.getFailedSurvey();
        if(cur_failed_count != null && cur_failed_count.getCount() > 0){
            tvFailedCount.setText((strFailedCount+cur_failed_count.getCount()));
            cur_failed_count.close();
        }else {
            tvFailedCount.setText((strFailedCount+0));
        }

        ll=(LinearLayout)findViewById(R.id.linearLayoutAdnlSurvey);
        lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        autocompletemrchantlist = (AutoCompleteTextView) findViewById(R.id.autocompletemrchantlist);

        textViewcross_1 = (TextView) findViewById(R.id.textViewcross_1);


//radio button declaration

        textviewnodata = (TextView) findViewById(R.id.textViewnodata);
        textViewQuestion = (TextView) findViewById(R.id.textViewQuestion);

        buttonsave = (Button) findViewById(R.id.buttonsave);
        linearlayoutsurvey = (LinearLayout) findViewById(R.id.linearlayoutsurvey);
        db = getApplicationContext().openOrCreateDatabase("freshorders", 0, null);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        databaseHandler = new DatabaseHandler(getApplicationContext());

        //validate if the user comes from add new user screen

        if (!AddMerchantNew.newmerchantcmpnyname.equals("null")) {
            Constants.paymentmerchname = AddMerchantNew.newmerchantcmpnyname;
            autocompletemrchantlist.setText(Constants.paymentmerchname);
            linearlayoutsurvey.setVisibility(View.VISIBLE);
            textviewnodata.setVisibility(View.GONE);
            dealernameset();

        } else {
            //normal flow for system userautocompletemrchantlist.setText("");
        }


        arrayListDealer = new ArrayList<String>();
        arrayListDealerDomain = new ArrayList<MDealerCompDropdownDomain>();
        arrayListmerchant = new ArrayList<MDealerCompDropdownDomain>();
        arrayListSearch = new ArrayList<MDealerCompDropdownDomain>();

        linearlayoutoverallayout = (LinearLayout) findViewById(R.id.linearlayoutoverallayout);
        linearLayoutCreateOrder = (LinearLayout) findViewById(R.id.linearLayoutCreateOrder);
        linearLayoutProfile = (LinearLayout) findViewById(R.id.linearLayoutProfile);
        linearLayoutMyOrders = (LinearLayout) findViewById(R.id.linearLayoutMyOrders);
        linearLayoutMyDealers = (LinearLayout) findViewById(R.id.linearLayoutMyDealers);
        linearLayoutRefresh = (LinearLayout) findViewById(R.id.linearLayoutRefresh);
        textViewAssetMenuRefresh = (TextView) findViewById(R.id.textViewAssetMenuRefresh);
        linearLayoutPayment = (LinearLayout) findViewById(R.id.linearLayoutPayment);
        linearLayoutComplaint = (LinearLayout) findViewById(R.id.linearLayoutComplaint);
        linearLayoutSignout = (LinearLayout) findViewById(R.id.linearLayoutSignout);
        linearLayoutDistanceCalculation= (LinearLayout) findViewById(R.id.linearLayoutDistanceCalculation);
        linearLayoutClientVisit = (LinearLayout) findViewById(R.id.linearLayoutClientVisit);
        linearLayoutPaymentCollection = (LinearLayout) findViewById(R.id.linearLayoutPaymentCollection);

        linearLayoutAcknowledge = (LinearLayout) findViewById(R.id.linearLayoutAcknowledge);
        linearLayoutStockEntry = (LinearLayout) findViewById(R.id.linearLayoutStockEntry);
        linearLayoutAddMerchant = (LinearLayout) findViewById(R.id.linearLayoutAddMerchant);

        linearLayoutdownloadscheme = (LinearLayout) findViewById(R.id.linearLayoutdownloadscheme);
        linearLayoutsurveyuser = (LinearLayout) findViewById(R.id.linearLayoutsurveyuser);
        linearLayoutMyDayPlan = (LinearLayout) findViewById(R.id.linearLayoutMyDayPlan);
        linearLayoutMySales = (LinearLayout) findViewById(R.id.linearLayoutMySales);

        linearLayoutStockOnly = (LinearLayout) findViewById(R.id.linearLayoutStockOnly);
        linearLayoutStockAudit = findViewById(R.id.linearLayoutStockAudit);
        linearLayoutMigration = (LinearLayout) findViewById(R.id.linearLayoutMigration);

        menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
        textViewAssetMenuCreateOrder = (TextView) findViewById(R.id.textViewAssetMenuCreateOrder);
        textViewAssetMenuProfile = (TextView) findViewById(R.id.textViewAssetMenuProfile);
        textViewAssetMenuMyOrders = (TextView) findViewById(R.id.textViewAssetMenuMyOrders);
        textViewAssetMenuMyDealers = (TextView) findViewById(R.id.textViewAssetMenuMyDealers);

        textViewAssetMenuPayment = (TextView) findViewById(R.id.textViewAssetMenuPayment);
        textViewAssetMenuComplaint = (TextView) findViewById(R.id.textViewAssetMenuComplaint);
        textViewAssetMenuSignout = (TextView) findViewById(R.id.textViewAssetMenuSignout);
        textViewAssetMenuDistanceCalculation=(TextView) findViewById(R.id.textViewAssetMenuDistanceCalculation);
        textViewAssetMenuClientVisit = (TextView) findViewById(R.id.textViewAssetMenuClientVisit);
        textViewAssetMenuPaymentCollection = (TextView) findViewById(R.id.textViewAssetMenuPaymentCollection);

        textViewAssetMenuAddMerchant = (TextView) findViewById(R.id.textViewAssetMenuAddMerchant);
        textViewAssetMenuAcknowledge = (TextView) findViewById(R.id.textViewAssetMenuAcknowledge);
        textViewAssetMenuStockEntry = (TextView) findViewById(R.id.textViewAssetMenuStockEntry);

        textViewAssetMenudownloadscheme = (TextView) findViewById(R.id.textViewAssetMenudownloadscheme);
        textViewAssetMenusurveyuser = (TextView) findViewById(R.id.textViewAssetMenusurveyuser);
        textViewAssetMenuMyDayPlan = (TextView) findViewById(R.id.textViewAssetMenuMyDayPlan);
        textViewAssetMenuMySales = (TextView) findViewById(R.id.textViewAssetMenuMySales);


        linearLayoutMenuParent = (LinearLayout) findViewById(R.id.linearLayoutMenuParent);


        Typeface font = Typeface.createFromAsset(getAssets(),
                "fontawesome-webfont.ttf");

        textViewcross_1.setTypeface(font);
        menuIcon.setTypeface(font);
        textViewAssetMenuCreateOrder.setTypeface(font);
        textViewAssetMenuProfile.setTypeface(font);
        textViewAssetMenuMyOrders.setTypeface(font);
        textViewAssetMenuMyDealers.setTypeface(font);
        textViewAssetMenuRefresh.setTypeface(font);
        textViewAssetMenuPayment.setTypeface(font);
        textViewAssetMenuComplaint.setTypeface(font);
        textViewAssetMenuSignout.setTypeface(font);
        textViewAssetMenuDistanceCalculation.setTypeface(font);
        textViewAssetMenuClientVisit.setTypeface(font);
        textViewAssetMenuPaymentCollection.setTypeface(font);

        textViewAssetMenuAcknowledge.setTypeface(font);
        textViewAssetMenuStockEntry.setTypeface(font);
        textViewAssetMenuAddMerchant.setTypeface(font);

        textViewAssetMenusurveyuser.setTypeface(font);
        textViewAssetMenudownloadscheme.setTypeface(font);
        textViewAssetMenuMyDayPlan.setTypeface(font);
        textViewAssetMenuMySales.setTypeface(font);

        linearLayoutDistStock = (LinearLayout) findViewById(R.id.linearLayoutDistStock);
        textViewAssetMenuDistStock = (TextView) findViewById(R.id.textViewAssetMenuDistStock);
        textViewAssetMenuDistStock.setTypeface(font);

        textViewAssetMenuStockOnly = (TextView) findViewById(R.id.textViewAssetMenuStockOnly);
        textViewAssetMenuStockAudit = (TextView) findViewById(R.id.textViewAssetMenuStockAudit);
        textViewAssetMenuMigration = findViewById(R.id.textViewAssetMenuMigration);

        textViewAssetMenuStockOnly.setTypeface(font);
        textViewAssetMenuStockAudit.setTypeface(font);
        textViewAssetMenuMigration.setTypeface(font);

        linearLayoutMyDealers.setVisibility(View.GONE);

        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date d = new Date();
        dayOfTheWeek = sdf.format(d);
        Log.e("dateday", dayOfTheWeek);

        Cursor cur1;
        cur1 = databaseHandler.getUserSetting();
        cur1.moveToFirst();
        userBeatModel = cur1.getString(cur1.getColumnIndex(DatabaseHandler.KEY_settingrefvalue));
        Log.e("userBeatModel", userBeatModel);

        if (userBeatModel.equalsIgnoreCase("C")) {
            Log.e("userModel C", "userModel C");
            Cursor curr;
            curr = databaseHandler.getDetails();
            curr.moveToFirst();
            String selectedbeat = curr.getString(curr.getColumnIndex(DatabaseHandler.KEY_selectedBeat));
            String selectedDate = curr.getString(curr.getColumnIndex(DatabaseHandler.KEY_selectedDate));
            String todayDate = new SimpleDateFormat(
                    "yyyy-MM-dd").format(new java.util.Date());
            if (todayDate.equals(selectedDate) && !selectedbeat.equals("")) {
                getBeatMerchant(selectedbeat);

            } else {
                Constants.beatassignscreen = "SurveyMerchant";
                Intent to = new Intent(SurveyMerchant.this, CompanyBeatActivity.class);
                startActivity(to);
                finish();
            }

        } else {
            Log.e("userModel D", "userModel D");
            offlineBeat();
        }

        if( Constants.jsonArCustomFieldsMerchant ==null)
        {
            try {
                    String strMerchantCustomFields = databaseHandler.getConstantValue("MERCHANTCUSTOMFIELDS");

                    if (strMerchantCustomFields == null ||  strMerchantCustomFields == "") {

                        if (netCheck() == true) {
                            String strUrl = Utils.strCustomFieldMerchantURL + Constants.DUSER_ID;
                            Log.e("strUrlelse SmOact", strUrl);
                            //loadCustomFields(strUrl);
                        }
                        else {
                            toastDisplay("Please Check Your internet connection");
                        }
                    }

            }catch (Exception ex)
            {
                ex.printStackTrace();
            }
        }
        else {
            loadUIComponents(Constants.jsonArCustomFieldsMerchant);
        }

        autocompletemrchantlist.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                autocompletemrchantlist.showDropDown();

                Constants.startTime  = new SimpleDateFormat(
                        "HH:mm:ss").format(new java.util.Date());

                textViewcross_1.setVisibility(View.VISIBLE);

                textViewcross_1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        autocompletemrchantlist.setText("");
                        Constants.paymentmerchname = "";
                        Constants.paymentmerchant_id = "";
                        smpymntmerchantselected = 0;

                        if (autocompletemrchantlist.getText().equals("")) {
                            InputMethodManager iaa = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            iaa.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                        }

                    }
                });

                return false;
            }
        });
        autocompletemrchantlist
                .setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {

                        String mnsme = autocompletemrchantlist.getText().toString();

                        for (MDealerCompDropdownDomain cd : arrayListmerchant) {

                            if (cd.getComName().equals(mnsme)) {
                                muserId = cd.getID();
                                Constants.MERCHANT_PRIMARY_KEY_FOR_SURVEY = cd.getMerchantRowId();//Kumaravel for new Merchant SURVEY
                                merchantmobileno = cd.getmobileno();
                                Constants.paymentmerchant_id = muserId;
                                smpymntmerchantselected = 1;

                                if (smpymntmerchantselected == 1) {

                                    textViewcross_1.setVisibility(View.GONE);
                                    InputMethodManager iaa = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                    iaa.hideSoftInputFromWindow(autocompletemrchantlist.getWindowToken(), 0);
                                }
                                if (!autocompletemrchantlist.getText().equals("")) {
                                    linearlayoutsurvey.setVisibility(View.VISIBLE);
                                    textviewnodata.setVisibility(View.GONE);
                                    dealernameset();
                                }

                                Constants.paymentmerchname = mnsme;

                            }
                        }


                    }
                });


        buttonsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (autocompletemrchantlist.getText().toString().equals(" ")) {

                    showAlertDialogToast("Please select merchant");

                } else {
                    Boolean showalert = false;

//get selected radio button

                    int selectedId = radioGroup.getCheckedRadioButtonId();
                    radioButton = (RadioButton) findViewById(selectedId);

                    if (String.valueOf(radioButton.getText()).equals("Yes")) {

//yes scenario update merchant table yes in survey column

                        db.execSQL("UPDATE merchanttable SET survey = 'YES' WHERE companyname =" + "'" + Constants.paymentmerchname + "'");

                        Cursor cur;
                        cur = databaseHandler.getmerchant();
                        Log.e("out if Cursor :",""+cur);
                        if (cur != null && cur.getCount() > 0) {
                            cur.moveToFirst();
                            while (cur.isAfterLast() == false) {
                                Log.e("Cursor :",""+cur);
                                Log.e("Cursor value:",cur.getString(1));
                                Log.e("Cursor value11:",cur.getString(13));
                                if (cur.getString(1).equals(Constants.paymentmerchname) && cur.getString(13).equals("YES")) {
                                    showalert = true;
                                }
                                cur.moveToNext();
                            }

                            cur.close();
                        }
                    } else {

                        //No scenario update merchant table no in survey column

                        db.execSQL("UPDATE merchanttable SET survey = 'NO' WHERE companyname =" + "'" + Constants.paymentmerchname + "'");

                        Cursor cur;
                        cur = databaseHandler.getmerchant();

                        if (cur != null && cur.getCount() > 0) {
                            cur.moveToFirst();
                            while (cur.isAfterLast() == false) {

                                if (cur.getString(1).equals(Constants.paymentmerchname) && cur.getString(13).equals("NO")) {

                                    showalert = true;

                                }
                                cur.moveToNext();
                            }

                            cur.close();
                        }
                    }
                    if (Constants.jsonArCustomFieldsMerchant != null) {
                        if(netCheck()){
                            pushSurvey();
                        }else{
                            ContentValues cv = new ContentValues();
                            cv.put(DatabaseHandler.KEY_SURVEY_URL, Utils.strSaveSurvey);
                            cv.put(DatabaseHandler.KEY_SURVEY_SYNC_DATA, storeSurvey());
                            cv.put(DatabaseHandler.KEY_SURVEY_FAILED_REASON, Constants.EMPTY);
                            cv.put(DatabaseHandler.KEY_SURVEY_MIGRATION_STATUS, 1);
                            cv.put(DatabaseHandler.KEY_SURVEY_MERCHANT_PRIMARY_KEY, Constants.MERCHANT_PRIMARY_KEY_FOR_SURVEY);
                            databaseHandler.loadSurvey(cv);
                            showAlertDialogToast1("Saved Successfully");
                            navigateToCreateOrder();
                        }
                    } else {
                        if (showalert)
                            showAlertDialogToast1("Saved successfully");
                    }

                }
            }
        });

        linearlayoutoverallayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textViewcross_1.setVisibility(View.GONE);
            }
        });

        menuIcon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (menuCliccked == 0) {
                    linearLayoutMenuParent.setVisibility(View.VISIBLE);
                    menuCliccked = 1;
                } else {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                }

            }
        });

        linearLayoutMenuParent.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
            }
        });

        linearLayoutProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto = "profile";
                if (netCheckwithoutAlert() == true) {
                    getData();
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });

        linearLayoutRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("Clicked", "Clicked");

                if (netCheck()) {

                    final Dialog qtyDialog = new Dialog(SurveyMerchant.this);
                    qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    qtyDialog.setContentView(R.layout.warning_dialog);

                    final Button buttonUpdateOk, buttonUpdateCancel;

                    final TextView textQtyValidate;

                    buttonUpdateOk = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateOk);
                    buttonUpdateCancel = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateCancel);

                    textQtyValidate = (TextView) qtyDialog
                            .findViewById(R.id.textQtyValidate);
                    qtyDialog.show();


                    buttonUpdateOk.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            databaseHandler.synTableDelete();
                            Constants.refreshscreen = "stockentry";
                            Intent io = new Intent(SurveyMerchant.this, RefreshActivity.class);
                            startActivity(io);
                            finish();
                            qtyDialog.dismiss();
                        }

                    });

                    buttonUpdateCancel
                            .setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub

                                    qtyDialog.dismiss();

                                }
                            });

                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }
            }

        });

        linearLayoutMyOrders.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Constants.orderstatus = "Success";
                Constants.checkproduct = 0;
                linearLayoutMenuParent.setVisibility(View.GONE);
                moveto = "myorder";
                if (netCheckwithoutAlert() == true) {
                    getData();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(SurveyMerchant.this,
                                SalesManOrderActivity.class);
                        startActivity(io);
                        finish();
                    }
                }

            }
        });

        linearLayoutMyDealers.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                moveto = "Mydealer";
                if (netCheckwithoutAlert() == true) {
                    getData();
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }


            }
        });


        linearLayoutCreateOrder.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                MyApplication.getInstance().setTemplate8ForTemplate2(false);

                linearLayoutMenuParent.setVisibility(View.GONE);
                moveto = "createorder";
                Constants.checkproduct = 0;
                Constants.orderid = "";
                if (netCheckwithoutAlert() == true) {
                    getData();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(SurveyMerchant.this,
                                CreateOrderActivity.class);
                        startActivity(io);
                        finish();
                    }
                }


            }
        });


        linearLayoutComplaint.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto = "postnotes";
                if (netCheckwithoutAlert() == true) {
                    getData();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(SurveyMerchant.this,
                                MerchantComplaintActivity.class);
                        startActivity(io);
                        finish();
                    }
                }

            }
        });

        linearLayoutClientVisit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto = "clientvisit";
                if (netCheckwithoutAlert() == true) {

                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(SurveyMerchant.this,
                                ClientVisitActivity.class);
                        startActivity(io);
                        finish();
                    }
                }


            }
        });
        linearLayoutPaymentCollection.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto = "paymentcoll";
                if (netCheckwithoutAlert() == true) {
                    getData();
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });


        linearLayoutStockEntry.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto = "stockentry";
                if (netCheckwithoutAlert() == true) {
                    getData();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(SurveyMerchant.this,
                                OutletStockEntry.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });
        linearLayoutDistStock.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto = "diststock";
                if (netCheckwithoutAlert() == true) {
                    getData();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment");
                    } else {
                        Intent io = new Intent(SurveyMerchant.this,
                                DistributrStockActivity.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });
        linearLayoutAcknowledge.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto = "acknowledge";
                if (netCheckwithoutAlert() == true) {
                    getData();
                } else {

                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });
        linearLayoutDistanceCalculation.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto = "acknowledge";

                  Intent into=new Intent(SurveyMerchant.this, DistanceCalActivity.class);
                    startActivity(into);

            }
        });
        linearLayoutAddMerchant.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto = "addmerchant";
                if (netCheckwithoutAlert() == true) {
                    getData();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(SurveyMerchant.this,
                                AddMerchantNew.class);
                        startActivity(io);
                        finish();
                    }
                }

            }
        });
        linearLayoutdownloadscheme.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck()) {

                    final Dialog qtyDialog = new Dialog(SurveyMerchant.this);
                    qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    qtyDialog.setContentView(R.layout.warning_dialog);

                    final Button buttonUpdateOk, buttonUpdateCancel;

                    final TextView textQtyValidate;

                    buttonUpdateOk = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateOk);
                    buttonUpdateCancel = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateCancel);

                    textQtyValidate = (TextView) qtyDialog
                            .findViewById(R.id.textQtyValidate);
                    qtyDialog.show();


                    buttonUpdateOk.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            databaseHandler.deletescheme();
                            Constants.downloadScheme = "surveyuser";
                            Intent io = new Intent(SurveyMerchant.this, SchemeDownload.class);
                            startActivity(io);
                            finish();
                            qtyDialog.dismiss();
                        }

                    });

                    buttonUpdateCancel
                            .setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub

                                    qtyDialog.dismiss();

                                }
                            });

                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });
        linearLayoutsurveyuser.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;

            }
        });

        linearLayoutMyDayPlan.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto = "mydayplan";
                if (netCheckwithoutAlert() == true) {
                    getData();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(SurveyMerchant.this,
                                MyDayPlan.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });

        linearLayoutMySales.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto = "mysales";
                if (netCheckwithoutAlert() == true) {
                    getData();
                } else {

                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });


        linearLayoutSignout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                // TODO Auto-generated method stub
                databaseHandler.delete();
                Intent io = new Intent(SurveyMerchant.this, SigninActivity.class);
                startActivity(io);
                finish();
            }
        });

        linearLayoutMigration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Intent io = new Intent(SurveyMerchant.this,
                        PendingDataMigrationActivity.class);
                startActivity(io);
            }
        });

        linearLayoutStockOnly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Constants.checkproduct = 0;
                Constants.orderid = "";
                Constants.SalesMerchant_Id = "";
                Constants.Merchantname = "";
                MyApplication.getInstance().setTemplate8ForTemplate2(true);
                if (netCheck()) {
                    Intent io = new Intent(SurveyMerchant.this,
                            ClosingStockDashBoardActivity.class);
                    startActivity(io);
                    finish();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals("") || PaymentStatus.equalsIgnoreCase("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(SurveyMerchant.this,
                                ClosingStockDashBoardActivity.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });

        linearLayoutStockAudit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().setTemplate8ForTemplate2(false);
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Constants.checkproduct = 0;
                Constants.orderid = "";
                Constants.SalesMerchant_Id = "";
                Constants.Merchantname = "";
                if (netCheck()) {
                    Intent io = new Intent(SurveyMerchant.this,
                            ClosingStockAudit.class);
                    startActivity(io);
                    finish();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals("") || PaymentStatus.equalsIgnoreCase("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(SurveyMerchant.this,
                                ClosingStockAudit.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });

        ////////////
        showMenu();
        ////////////

    }

    public void getBeatMerchant(String beatname) {
        Cursor cursor;
        cursor = databaseHandler.getbeatMerchant(beatname);
        String merchants = "";
        int i = 0;
        if (cursor != null && cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {

                do {
                    if (i == 0) {
                        if (cursor.getString(0).equals("New User")) {
                            merchants = "'" + cursor.getString(0) + "'";
                        } else {
                            merchants = cursor.getString(0);
                        }

                        i++;
                    } else {
                        if (cursor.getString(0).equals("New User")) {
                            merchants = merchants + ", '" + cursor.getString(0) + "'";
                        } else {
                            merchants = merchants + "," + cursor.getString(0);
                        }

                    }


                } while (cursor.moveToNext());


            }
        }
        Log.e("merchants list", merchants);
        offlinemerchant(merchants);
    }

    private void getData() {
        if (moveto.equals("myorder")) {

            Intent io = new Intent(SurveyMerchant.this,
                    SalesManOrderActivity.class);
            startActivity(io);
            finish();

        } else if (moveto.equals("createorder")) {
            Intent io = new Intent(SurveyMerchant.this,
                    CreateOrderActivity.class);
            startActivity(io);
            finish();
        } else if (moveto.equals("Mydealer")) {
            Intent io = new Intent(SurveyMerchant.this,
                    MyDealersActivity.class);
            io.putExtra("Key", "MenuClick");
            startActivity(io);
            finish();
        } else if (moveto.equals("profile")) {
            Intent io = new Intent(SurveyMerchant.this,
                    ProfileActivity.class);
            startActivity(io);
            finish();
        } else if (moveto.equals("postnotes")) {
            Intent io = new Intent(SurveyMerchant.this,
                    MerchantComplaintActivity.class);
            startActivity(io);
            finish();
        } else if (moveto.equals("clientvisit")) {
            Intent io = new Intent(SurveyMerchant.this,
                    SMClientVisitHistory.class);
            startActivity(io);
            finish();
        } else if (moveto.equals("addmerchant")) {
            Intent io = new Intent(SurveyMerchant.this, AddMerchantNew.class);
            startActivity(io);
            finish();
        } else if (moveto.equals("acknowledge")) {
            Intent io = new Intent(SurveyMerchant.this, SalesmanAcknowledgeActivity.class);
            startActivity(io);
            finish();
        } else if (moveto.equals("paymentcoll")) {
            Intent io = new Intent(SurveyMerchant.this, SalesmanPaymentCollectionActivity.class);
            startActivity(io);
            finish();
        } else if (moveto.equals("stockentry")) {
            Intent io = new Intent(SurveyMerchant.this, OutletStockEntry.class);
            startActivity(io);
            finish();
        } else if (moveto.equals("mydayplan")) {
            Intent io = new Intent(SurveyMerchant.this, MyDayPlan.class);
            startActivity(io);
            finish();
        } else if (moveto.equals("mysales")) {
            Intent io = new Intent(SurveyMerchant.this, MySalesActivity.class);
            startActivity(io);
            finish();
        } else if (moveto.equals("diststock")) {
            Intent io = new Intent(SurveyMerchant.this,
                    DistributrStockActivity.class);
            startActivity(io);
            finish();
        }
    }

    public void offlineBeat() {
        Cursor curs;
        curs = databaseHandler.getbeatwithMerchant(dayOfTheWeek);
        Log.e("beatCount", String.valueOf(curs.getCount()));

        int i = 0;

        if (curs != null && curs.getCount() > 0) {
            if (curs.moveToFirst()) {

                do {
                    Log.e("beat inside", "beat inside");
                    if (i == 0) {
                        if (curs.getString(0).equals("New User")) {
                            beatMerchants = "'" + curs.getString(0) + "'";
                        } else {
                            beatMerchants = curs.getString(0);
                        }


                        i++;
                    } else {
                        if (curs.getString(0).equals("New User")) {
                            beatMerchants = beatMerchants + "," + "'" + curs.getString(0) + "'";
                        } else {
                            beatMerchants = beatMerchants + "," + curs.getString(0);
                        }


                    }

                    Log.e("beatMerchants", beatMerchants);


                } while (curs.moveToNext());


            }
            offlinemerchant(beatMerchants);
        } else {
            showAlertDialogToast("No Merchants available for this day");
        }

    }

    public void offlinemerchant(String beatMerchants) {


        Cursor curs;
        curs = databaseHandler.getmerchantoncurrentday(beatMerchants);
        Log.e("merchantCount", String.valueOf(curs.getCount()));

        if (curs != null && curs.getCount() > 0) {
            if (curs.moveToFirst()) {

                do {
                    Log.e("inside", "inside");
                    MDealerCompDropdownDomain cd = new MDealerCompDropdownDomain();
                    cd.setComName(curs.getString(1));
                    cd.setID(String.valueOf(curs.getString(2)));
                    cd.setName(curs.getString(3));
                    cd.setmobileno(curs.getString(4));
                    cd.setMerchantRowId(curs.getInt(0));//Kumaravel for new merchant
                    arrayListDealerDomain.add(cd);
                    arrayListSearch.add(cd);
                    arrayListDealer.add(curs.getString(1));
                    arrayListmerchant.add(cd);

                } while (curs.moveToNext());


            }
            Log.e("outside", "outside");
        } else {

        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                SurveyMerchant.this, R.layout.textview, R.id.textView,
                arrayListDealer);

        autocompletemrchantlist.setAdapter(adapter);

    }

    // get dealer name from dealer table for showing in survey question
    public static void dealernameset() {
        Cursor curs;
        curs = databaseHandler.getdealer();
        Log.e("dealerCount", String.valueOf(curs.getCount()));

        if (curs != null && curs.getCount() > 0) {
            if (curs.moveToFirst()) {

                String dealername = curs.getString(1);

                Log.e("dealername", dealername);
                textViewQuestion.setText("Are you using " + dealername + " products ?");


            }
            Log.e("outside", "outside");
        }
    }

    public boolean netCheckwithoutAlert() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            } else if (result == null) {

                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    private void navigateToCreateOrder(){
        AddMerchantNew.newmerchantcmpnyname = "null";

        Intent io = new Intent(SurveyMerchant.this, CreateOrderActivity.class);
        Constants.checkproduct = 0;
        startActivity(io);
        finish();
    }

    public void showAlertDialogToast1(String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                       navigateToCreateOrder();

                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    public void showAlertDialogToast(String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                        InputMethodManager inn = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                        inn.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    public boolean netCheck() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            } else if (result == null) {
               /* showAlertDialog(SalesManOrderActivity.this,
                        "No Internet Connection",
                        "Please Check Your internet connection.", false);*/
                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void onClickButton(List<SpinnerDomain> list) {
        Log.e("SurMerchant", "onClickButton: "+ " listener");
        for (SpinnerDomain data : list) {
            if(data.isSelected()) {
                Log.e("SurMerchant", "onClickButton: " + data.getTitle());
            }
        }
    }

/*
    private void loadCustomFields(final String url) {
        RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("url",url);
                try{
                    JSONObject jsonObject= null;
                    try {
                        jsonObject = new JSONObject(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if(jsonObject.getString("status").equalsIgnoreCase("true")){
                        Constants.jsonArCustomFieldsMerchant=jsonObject.getJSONArray("data");
                        databaseHandler.addConstant("MERCHANTCUSTOMFIELDS",""+Constants.jsonArCustomFieldsMerchant);
                        loadUIComponents(Constants.jsonArCustomFieldsMerchant);
                    }

                }catch (JSONException e){e.printStackTrace();}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }
*/

    public void loadUIComponents(JSONArray jsonArCustomComponents)
    {
        try {

            viewArCustomComponents = new View[jsonArCustomComponents.length()];

            for(int i=0;i<jsonArCustomComponents.length();i++)
            {
                setCustomViewComponent(viewArCustomComponents[i],jsonArCustomComponents.getJSONObject(i));
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

    }


    public  void setCustomViewComponent(View v,JSONObject jsonCustomField)
    {
        try {

            if(jsonCustomField.getString("uicomptype").equalsIgnoreCase("inputtext")){
                TextView avtTitle=new TextView(this);
                LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );
                p.setMargins(60, 0, 10, 0);
                avtTitle.setText(jsonCustomField.getString("reflabel"));
                avtTitle.setLayoutParams(p);
                ll.addView(avtTitle);
                EditText et = new EditText(this);
                ll.addView(et);
            }

            if(jsonCustomField.getString("uicomptype").equalsIgnoreCase("dropdown")){
                final Spinner spinner = new Spinner(this);
                String[] strArSpinnerItems = (jsonCustomField.getString("reflabel")+","+jsonCustomField.getString("refvalue")).split(",");
                ArrayAdapter<String> adapterSpinnerCategory = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, strArSpinnerItems);
                spinner.setAdapter(adapterSpinnerCategory);
                v=spinner;
                ll.addView(v);
                customValue = spinner.getSelectedItem()
                        .toString();

            }

            if(jsonCustomField.getString("uicomptype").equalsIgnoreCase("switch")){
                RadioGroup radioGroup = new RadioGroup(this);
                LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.FILL_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );
                v=radioGroup;
                TextView tvRGTitle=new TextView(this);
                tvRGTitle.setText(jsonCustomField.getString("reflabel"));
                ll.addView(tvRGTitle);
                ll.addView(v, p);
                String strRefValues=jsonCustomField.getString("refvalue");
                String strArRefValues[]=strRefValues.split(",");
                RadioButton radioButtonView[]=new RadioButton[strArRefValues.length];
                if(strArRefValues!=null && strArRefValues.length>0)
                {
                    for(int rbc=0;rbc<strArRefValues.length;rbc++)
                    {
                        radioButtonView[rbc] = new RadioButton(this);
                        radioButtonView[rbc].setText(strArRefValues[rbc]);
                        //radioButtonView.setOnClickListener(this);
                        radioGroup.addView(radioButtonView[rbc], p);
                    }
                }
            }

            if(jsonCustomField.getString("uicomptype").equalsIgnoreCase("multiselect")){

                Spinner spinner = new Spinner(this);
                spinner.setPrompt(jsonCustomField.getString("reflabel"));
                String[] strArSpinnerCategory = (jsonCustomField.getString("reflabel")+","+jsonCustomField.getString("refvalue")).split(",");
                v=spinner;
                ll.addView(v);
                ArrayList<SpinnerDomain> listVOs4 = new ArrayList<>();
                for (int i = 0; i < strArSpinnerCategory.length; i++) {
                    SpinnerDomain spinnerdomain = new SpinnerDomain();
                    spinnerdomain.setTitle(strArSpinnerCategory[i]);
                    spinnerdomain.setSelected(false);
                    listVOs4.add(spinnerdomain);

                }
                SurveyAdapter adapter = new SurveyAdapter(SurveyMerchant.this, 0,
                        listVOs4,this);
                spinner.setAdapter(adapter);



            }


        }catch (Exception ex)
        {
            ex.printStackTrace();
        }

    }

    protected void toastDisplay(String msg) {
        Toast toast = Toast.makeText(SurveyMerchant.this, msg,
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();

    }

    //build json with data and store in db
    private String storeSurvey() {
        final JSONObject jsonSurveyObject = new JSONObject();
        final JSONArray jsonArCustomDataValues = new JSONArray();
        JSONArray jsonArCustomData;
        View v;
        try {
            jsonArCustomData = Constants.jsonArCustomFieldsMerchant;
            int childId = 0;
            for (int i = 0; i < jsonArCustomData.length(); i++) {
                JSONObject jsonObject = new JSONObject();
                JSONObject jsonCF = jsonArCustomData.getJSONObject(i);
                String strCFId = jsonCF.getString("custmfldid");
                jsonObject.put("custmfldid", "" + strCFId);
                String strCustomValue = null;
                if (jsonCF.getString("uicomptype").equalsIgnoreCase("text")) {
                    v = ll.getChildAt(childId++);
                    strCustomValue = "" + ((EditText) v).getText();
                }
                if(jsonCF.getString("uicomptype").equalsIgnoreCase("inputtext")){
                    ll.getChildAt(childId++);
                    v = ll.getChildAt(childId++);
                    strCustomValue = "" + ((EditText) v).getText();
                }

                if (jsonCF.getString("uicomptype").equalsIgnoreCase("dropdown")) {
                    v = ll.getChildAt(childId++);
                    Spinner ms = (Spinner) v;
                    strCustomValue = "" + ms.getSelectedItem();
                }
                if (jsonCF.getString("uicomptype").equalsIgnoreCase("multiselect")) {
                    v = ll.getChildAt(childId++);
                    strCustomValue = "" + ((SurveyAdapter) ((Spinner) v).getAdapter()).getSelectedValuesString();
                }
                if (jsonCF.getString("uicomptype").equalsIgnoreCase("switch")) {
                    v = ll.getChildAt(++childId);
                    RadioGroup rg = (RadioGroup) v;
                    int intVal = rg.getCheckedRadioButtonId();
                    for (int z = 0; z < rg.getChildCount(); z++) {
                        RadioButton rb = (RadioButton) rg.getChildAt(z);
                        if (rb.isChecked()) {
                            strCustomValue = "" + rb.getText();
                        }
                    }
                    childId++;
                }

                jsonObject.put("custmfldvalue", strCustomValue);
                jsonArCustomDataValues.put(jsonObject);

            }
            jsonSurveyObject.put("muserid", muserId);
            jsonSurveyObject.put("suserid", Constants.USER_ID);
            jsonSurveyObject.put("starttime", Constants.startTime);
            jsonSurveyObject.put("endime", new SimpleDateFormat(
                    "HH:mm:ss").format(new java.util.Date()));
            jsonSurveyObject.put("duserid", Constants.DUSER_ID);
            jsonSurveyObject.put("customdata", jsonArCustomDataValues);
            jsonSurveyObject.put("surveydate", new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss").format(new java.util.Date()));

            Log.i("SurveyJSON", jsonSurveyObject.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonSurveyObject.toString();
    }

    private void pushSurvey() {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "", strOrderId = "";

            @Override
            protected void onPreExecute() {
                dialog = ProgressDialog.show(SurveyMerchant.this, "",
                        "Saving...", true, true);
            }

            @Override
            protected void onPostExecute(Void result) {

                try {

                    strStatus = JsonSurvyObject.getString("status");
                    Log.e("return status", strStatus);
                    if (strStatus.equals("true")) {
                        showAlertDialogToast1("Saved Successfully");

                    } else {
                        showAlertDialogToast("Failed! Please Contact Support");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    //Log.e("ProductActivityException", e.toString());
                }
                dialog.dismiss();
            }

            @Override
            protected Void doInBackground(Void... params) {
                JsonServiceHandler = new JsonServiceHandler(Utils.strSaveSurvey, storeSurvey(), SurveyMerchant.this);
                JsonSurvyObject = JsonServiceHandler.ServiceData();
                return null;
            }
        }.execute(new Void[]{});
    }
}
