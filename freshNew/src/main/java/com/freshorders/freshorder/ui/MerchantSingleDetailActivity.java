package com.freshorders.freshorder.ui;

import java.util.Arrays;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.DealerOrderDetailAdapter;
import com.freshorders.freshorder.adapter.ImageAdapter;
import com.freshorders.freshorder.domain.DealerOrderDetailDomain;
import com.freshorders.freshorder.domain.ViewPagerDomain;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MerchantSingleDetailActivity extends Activity {
	TextView textViewName,textViewAddress,textViewDate,menuIcon,textViewdelord;
	LinearLayout linearLayoutBack,linearLayoutButton,linearLayoutHeading;
	JSONObject JsonAccountObject = null;
	JSONArray JsonAccountArray = null;
	JsonServiceHandler JsonServiceHandler ;
	ListView listViewOrders;
	  Date dateStr = null;
	  public static String time;
	  Button buttonUpdate,buttonAddNewProducts,buttonUpdateImage;
	  public static String[] orderdtid, orderid,prodid,qty,date,status,freeqty,prodImage,removedimage,read,orderuom,currstock;
	public static int ProdImageCount,CountInt=1;
	GridView gridview;
	public static String[] mThumbIds=null;
	public static List<String> list;
	public static int imageTotal;
	public   String images="",orderidisread="";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(1);
		setContentView(R.layout.merchant_order_detail_screen);
		netCheck();
		textViewName = (TextView) findViewById(R.id.textViewName);
		textViewAddress = (TextView) findViewById(R.id.textViewAddress);
		textViewDate = (TextView) findViewById(R.id.textViewDate);
		textViewdelord = (TextView) findViewById(R.id.textViewdelord);
		menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
		listViewOrders = (ListView) findViewById(R.id.listViewOrders);
		buttonUpdate = (Button) findViewById(R.id.buttonUpdate);
		buttonAddNewProducts = (Button) findViewById(R.id.buttonAddNewProducts);
		buttonUpdateImage= (Button) findViewById(R.id.buttonUpdateImage);
		linearLayoutBack = (LinearLayout) findViewById(R.id.linearLayoutBack);
		linearLayoutButton= (LinearLayout) findViewById(R.id.linearLayoutButton);
		linearLayoutHeading= (LinearLayout) findViewById(R.id.linearLayoutHeading);
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();
		gridview = (GridView) findViewById(R.id.gridview);

		Typeface font = Typeface.createFromAsset(getAssets(),
				"fontawesome-webfont.ttf");
		menuIcon.setTypeface(font);
		textViewdelord.setTypeface(font);
		textViewName.setText(MerchantOrderActivity.fullname);
		textViewAddress.setText("Order Id:" + MerchantOrderActivity.orderId + ",");
		textViewDate.setText(MerchantOrderActivity.date);

		if(Constants.adapterset==1){
			Log.e("size",String.valueOf(MerchantOrderActivity.arraylistDealerOrderDetailList.size()));
			mThumbIds = list.toArray(new String[0]);
			setViewPagerAdapter();
		}else{
			MerchantOrderActivity.arraylistDealerOrderDetailList.clear();
			myOrders();
		}

		linearLayoutBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Constants.adapterset=0;
				Intent io = new Intent(MerchantSingleDetailActivity.this, MerchantOrderActivity.class);
				startActivity(io);
				finish();
			}
		});
		
		buttonUpdate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub


					saveOrder();

				
			}
		});
		buttonUpdateImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				for(int i=0;i<list.size();i++){
					String [] image=list.get(i).split("/");
					Log.e("list",image[image.length-1]);
					if(i==0){
						images=images+image[image.length-1];
					}else{
						images=images+","+image[image.length-1];
					}

				}

				updateImage();

			}
		});

		buttonAddNewProducts.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Constants.orderid = MerchantOrderActivity.orderId;
				Constants.checkproduct = 0;
				Intent io = new Intent(MerchantSingleDetailActivity.this, MerchantOrderDetailActivity.class);
				startActivity(io);
				finish();


			}
		});


		gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
				Intent i = new Intent(getApplicationContext(), FullImageActivity.class);
				i.putExtra("id", position);
				startActivity(i);
				MerchantSingleDetailActivity.this.finish();
			}
		});
       textViewdelord.setOnClickListener(new OnClickListener() {
	  @Override
	 public void onClick(View v) {
		//deleteorders();
		showAlertDialogToastdelorder();
	 }
     });
	 }
	public void deleteorders() {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog1;
			String strStatus = "";
			String strMsg = "";
			@Override
			protected void onPreExecute() {
				dialog1 = ProgressDialog.show(MerchantSingleDetailActivity.this, "",
						"Loading...", true, true);
                dialog1.setCancelable(false);
			}

			@Override
			protected void onPostExecute(Void result) {

				try{

					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);

					if (strStatus.equals("true")) {
						showAlertDialogToast1("Order deleted Successfully");
					}else{
						showAlertDialogToast1("Not Deleted");

					}
					dialog1.dismiss();
				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {

				}

			}

			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();


				try {
					jsonObject.put("ordstatus","Cancelled");
				} catch (JSONException e) {
					e.printStackTrace();
				}


				JsonServiceHandler = new JsonServiceHandler(
						Utils.strdeleteorder +MerchantOrderActivity.orderId ,jsonObject.toString(),
						MerchantSingleDetailActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceData();
				return null;
			}
		}.execute(new Void[] {});
	}
    public void updateImage(){
	new AsyncTask<Void, Void, Void>() {
		ProgressDialog dialog;
		String strStatus = "";
		String strMsg = "";

		@Override
		protected void onPreExecute() {
			dialog= ProgressDialog.show(MerchantSingleDetailActivity.this, "",
					"Loading...", true, true);
		}

		@Override
		protected void onPostExecute(Void result) {

			try {

				strStatus = JsonAccountObject.getString("status");
				Log.e("return status", strStatus);
				strMsg = JsonAccountObject.getString("message");
				Log.e("return message", strMsg);
				showAlertDialogToastupdateimage(strMsg);

			} catch (JSONException e) {
				e.printStackTrace();
			} catch (Exception e) {

			}
			dialog.dismiss();
		}

		@Override
		protected Void doInBackground(Void... params) {
			JSONObject jsonObject = new JSONObject();
			try {
				JSONArray jsonArray = new JSONArray();
				JSONArray jsonArray1 = new JSONArray();
				JSONObject jsonObject1,jsonObject2;
				//mThumbIds= new String[MerchantOrderActivity.arraylistPdoductImages.size()];
				orderdtid = new String[MerchantOrderActivity.arraylistDealerOrderDetailList.size()];
				orderid = new String[MerchantOrderActivity.arraylistDealerOrderDetailList.size()];
				removedimage= new String[MerchantOrderActivity.removedImage.size()];

				for (int i = 0; i < MerchantOrderActivity.arraylistDealerOrderDetailList.size(); i++) {
					orderdtid[i] =MerchantOrderActivity.arraylistDealerOrderDetailList.get(i).getorddtlid();
					orderid[i] =  MerchantOrderActivity.arraylistDealerOrderDetailList.get(i).getOrderid();

				}

				for(int j=0;j< MerchantOrderActivity.removedImage.size();j++){
					removedimage[j] =MerchantOrderActivity.removedImage.get(j);
				}

				for (int i = 0; i < MerchantOrderActivity.arraylistDealerOrderDetailList.size(); i++) {
					jsonObject1 = new JSONObject();
					jsonObject2 = new JSONObject();
					try {
						jsonObject1.put("orddtlid", orderdtid[i]);
						jsonObject1.put("ordid", orderid[i]);
						jsonObject1.put("ordimage", images);
						jsonObject1.put("prodid", "0");
						Log.e("ordimage", images);


						/*jsonObject1.put("deletelist", MerchantOrderActivity.removedImage);
						Log.e("deletelist",  MerchantOrderActivity.removedImage.toString());*/

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					jsonArray.put(jsonObject1);
				}


				for(int q=0;q<MerchantOrderActivity.removedImage.size();q++){
					jsonArray1.put(MerchantOrderActivity.removedImage.get(q));
				}

				jsonObject.put("orderdtls", jsonArray);
				jsonObject.put("deletelist", jsonArray1);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			JsonServiceHandler = new JsonServiceHandler(Utils.strsavedealerOderdetail,jsonObject.toString(), MerchantSingleDetailActivity.this);
			JsonAccountObject = JsonServiceHandler.ServiceData();
			return null;
		}
	}.execute(new Void[] {});
}


	private void myOrders() {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog= ProgressDialog.show(MerchantSingleDetailActivity.this,"",
						"Loading...", true, true);

			}

			@Override
			protected void onPostExecute(Void result) {

				try {

					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);

					if (strStatus.equals("true")) {
						
						JSONArray job1 = JsonAccountObject.getJSONArray("data");

						for (int i = 0; i < job1.length(); i++) {

							try{

							JSONObject job = new JSONObject();
							job=job1.getJSONObject(i);
								Log.e("job", String.valueOf((job)));
							DealerOrderDetailDomain dod=new DealerOrderDetailDomain();
								ViewPagerDomain vP= new ViewPagerDomain();
								read=new String[3];
							dod.setOrderid(job.getString("ordid"));
							orderidisread=job.getString("ordid");
							dod.setorddtlid(job.getString("orddtlid"));
                            dod.setisread(job.getString("isread"));
							dod.setprodid(job.getString("prodid"));
								 read=job.getString("isread").split(",");
								String[] date1=job.getString("deliverydt").split("T");
								time = date1[0];
								String inputPattern = "yyyy-MM-dd";
								String outputPattern = "dd-MMM-yyyy";
								SimpleDateFormat inputFormat = new SimpleDateFormat(
										inputPattern);
								SimpleDateFormat outputFormat = new SimpleDateFormat(
										outputPattern);
								String str = null;

								try {
									dateStr = inputFormat.parse(time);
									str = outputFormat.format(dateStr);
									dod.setDate(str);
									//vP.setDate(str);

								} catch (ParseException e) {
									e.printStackTrace();
								}

								if(job.getString("prodid").equals("0")){
									mThumbIds=job.getString("ordimage").split(",");
									imageTotal=mThumbIds.length;
										for(int k=0;k<mThumbIds.length;k++){
											mThumbIds[k]= Utils.strImageUrl+mThumbIds[k];
											Log.e("image",mThumbIds[k]);
											vP.setImage(mThumbIds[k]);
											MerchantOrderActivity.arraylistPdoductImages.add(vP);
										}
								 list = new ArrayList<String>(Arrays.asList(mThumbIds));
										Log.e("prodArray", String.valueOf(mThumbIds.length));


								}else{

							dod.setFlag(job.getString("isfree"));

							Constants.serialno=job.getString("ordslno");

							        dod.setProdcode(job.getJSONObject("product").getString("prodcode"));
									dod.setproductname(job.getJSONObject("product").getString("prodname"));
                                    dod.setFreeQty("0");
									dod.setqty(job.getString("qty"));
									dod.setProdprice(job.getString("prodPrice"));
									dod.setUOM(job.getJSONObject("product").getString("uom"));
									dod.setSelecetdQtyUomUnit("1");
									dod.setSelecetdFreeQtyUomUnit("1");
									dod.setSelcetedUOM(job.getJSONObject("product").getString("uom"));
									dod.setSelectedFreeUom(job.getJSONObject("product").getString("uom"));
									dod.setDeliverymode("");
									dod.setDeliverytime("");
									dod.setDeliverydate("");
									dod.setProdprice("");
									dod.setordstatus(job.getString("ordstatus"));
									dod.setCurrStock(job.getString("currstock"));

								}
								MerchantOrderActivity.arraylistDealerOrderDetailList.add(dod);


							}catch (Exception e){

							}

						}
						Constants.adapterset=1;
						setViewPagerAdapter();
						isread();
					}else{
						showAlertDialogToast(strMsg);

					
					}
					dialog.dismiss();
				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {

				}

			}

			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();
				

				JsonServiceHandler = new JsonServiceHandler(Utils.strMerSingleOderdetail+MerchantOrderActivity.orderId+Utils.strMerSingleOderdetailAdditon+MerchantOrderActivity.duserid, MerchantSingleDetailActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceDataGet();
				return null;
			}
		}.execute(new Void[] {});
	}


	public void setViewPagerAdapter(){

		if(MerchantOrderActivity.arraylistPdoductImages.size()==0){
			linearLayoutButton.setVisibility(LinearLayout.GONE);
			linearLayoutHeading.setVisibility(LinearLayout.VISIBLE);

			DealerOrderDetailAdapter adapter=new DealerOrderDetailAdapter(MerchantSingleDetailActivity.this, R.layout.item_dealer_order_detail, MerchantOrderActivity.arraylistDealerOrderDetailList);
			listViewOrders.setAdapter(adapter);
		}else{
			gridview.setAdapter(new ImageAdapter(this));
			linearLayoutButton.setVisibility(LinearLayout.VISIBLE);
			linearLayoutHeading.setVisibility(LinearLayout.GONE);

		}


	}
	public void isread() {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";
			@Override
			protected void onPreExecute() {
				/*dialog = ProgressDialog.show(MerchantSingleDetailActivity.this, "",
						"Loading...", true, true);*/

			}

			@Override
			protected void onPostExecute(Void result) {

     try{

	strStatus = JsonAccountObject.getString("status");
	Log.e("return status", strStatus);
	strMsg = JsonAccountObject.getString("message");
	Log.e("return message", strMsg);


      } catch (JSONException e) {
	  e.printStackTrace();
      } catch (Exception e) {

        }

			}

			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();
                int j=0;
				String newread = null;
				try {
					Log.e("readarraylenght", String.valueOf(read.length));
                    for(int i=0;i<read.length;i++){

					if(read[i].equals(Constants.USER_ID)){
						Log.e("readarray", read[i]);

						List<String> list = new ArrayList<String>(Arrays.asList(read));
						list.remove(read[i]);
						read = list.toArray(new String[0]);

					}
                    }

					Log.e("arraylenghtout", String.valueOf(read.length));
					for(int a=0;a<read.length;a++) {

						if (j == 0) {

							newread =read[a];

							j++;
						}else{

							newread=newread+","+read[a];

						}

					}


                 if(read.length==0){
	               jsonObject.put("isread","null");
                 }else{
	              jsonObject.put("isread",newread);
                 }


				} catch (JSONException e) {
					e.printStackTrace();
				}

                Log.e("orderidisread",orderidisread);

				for(int m=0;m<MerchantOrderActivity.arraylistDealerOrderDetailList.size();m++){

				}
				JsonServiceHandler = new JsonServiceHandler(
						Utils.strisread +orderidisread+"&[where][duserid]="+MerchantOrderActivity.duserid ,jsonObject.toString(),
						MerchantSingleDetailActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceData();
				return null;
			}
		}.execute(new Void[] {});
	}
	private void saveOrder() {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog= ProgressDialog.show(MerchantSingleDetailActivity.this, "",
						"Loading...", true, true);
			}

			@Override
			protected void onPostExecute(Void result) {

				try {

					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);
					showAlertDialogToastupdate(strMsg);
     //toastDisplay(strMsg);
     //toastDisplay(strMsg);

				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					//Log.e("ProductActivityException", e.toString());
				}
				dialog.dismiss();
			}

			@Override
			protected Void doInBackground(Void... params) {
				JSONObject jsonObject = new JSONObject();
				try {
				JSONArray jsonArray = new JSONArray();
				JSONObject jsonObject1;

				orderdtid = new String[MerchantOrderActivity.arraylistDealerOrderDetailList.size()];
				orderid = new String[MerchantOrderActivity.arraylistDealerOrderDetailList.size()];
				prodid = new String[MerchantOrderActivity.arraylistDealerOrderDetailList.size()];
				qty = new String[MerchantOrderActivity.arraylistDealerOrderDetailList.size()];
				date = new String[MerchantOrderActivity.arraylistDealerOrderDetailList.size()];
				status = new String[MerchantOrderActivity.arraylistDealerOrderDetailList.size()];
					freeqty = new String[MerchantOrderActivity.arraylistDealerOrderDetailList.size()];
					currstock = new String[MerchantOrderActivity.arraylistDealerOrderDetailList.size()];

					orderuom = new String[MerchantOrderActivity.arraylistDealerOrderDetailList.size()];
			    	for (int i = 0; i < MerchantOrderActivity.arraylistDealerOrderDetailList.size(); i++) {
					orderdtid[i] =MerchantOrderActivity.arraylistDealerOrderDetailList.get(i).getorddtlid();
					orderid[i] =  MerchantOrderActivity.arraylistDealerOrderDetailList.get(i).getOrderid();
					prodid[i] =MerchantOrderActivity.arraylistDealerOrderDetailList.get(i).getprodid();
					qty[i]=MerchantOrderActivity.arraylistDealerOrderDetailList.get(i).getqty();
					date[i] =MerchantOrderActivity.arraylistDealerOrderDetailList.get(i).getDate();
					status[i]=MerchantOrderActivity.arraylistDealerOrderDetailList.get(i).getordstatus();
					freeqty[i]=MerchantOrderActivity.arraylistDealerOrderDetailList.get(i).getFreeQty();
					orderuom[i]=MerchantOrderActivity.arraylistDealerOrderDetailList.get(i).getUOM();
					currstock[i]=MerchantOrderActivity.arraylistDealerOrderDetailList.get(i).getCurrStock();

					//freeqty[i]=arraylistDealerOrderDetailList.get(i).get();

				}
					/*int serialno=Integer.parseInt(Constants.serialno);
					serialno=serialno+1;*/
					for (int i = 0; i < MerchantOrderActivity.arraylistDealerOrderDetailList.size(); i++) {
					jsonObject1 = new JSONObject();
					try {
						Log.e("qty", String.valueOf(MerchantOrderActivity.arraylistDealerOrderDetailList.size()));

						double frqty = Float.parseFloat(freeqty[i]);
						double qtty = Float.parseFloat(qty[i]);

						if(frqty>0){
							jsonObject1.put("qty", freeqty[i]);
							jsonObject1.put("cqty", qty[i]);
						}

						if(qtty>0){
							jsonObject1.put("qty", qty[i]);
							jsonObject1.put("cqty", qty[i]);
						}
						jsonObject1.put("orddtlid", orderdtid[i]);
						jsonObject1.put("ordid", orderid[i]);
						jsonObject1.put("prodid",prodid[i]);
						jsonObject1.put("deliverydt",date[i]);
						jsonObject1.put("ordstatus", status[i]);
						//jsonObject1.put("fqty", freeqty[i]);
						jsonObject1.put("duserid", Constants.dealerid);
						//jsonObject1.put("ordslno",serialno);
						//jsonObject1.put("duserid", Constants.USER_ID);
						jsonObject1.put("ord_uom",orderuom[i]);
						jsonObject1.put("default_uom",orderuom[i]);
						jsonObject1.put("currstock",currstock[i]);

					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					jsonArray.put(jsonObject1);
				}
			
					jsonObject.put("orderdtls", jsonArray);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				JsonServiceHandler = new JsonServiceHandler(Utils.strsavedealerOderdetail,jsonObject.toString(), MerchantSingleDetailActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceData();
				return null;
			}
		}.execute(new Void[] {});
	}
	
	protected void toastDisplay(String msg) {
		Toast toast = Toast.makeText(MerchantSingleDetailActivity.this, msg,
				Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

	}
	public boolean netCheck() {
		// for network connection
		try {
			ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetwork = connectionManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			Object result = null;
			if (mWifi.isConnected() || mNetwork.isConnected()) {
				return true;
			}

			else if (result == null) {
				showAlertDialog(MerchantSingleDetailActivity.this,
						"No Internet Connection",
						"Please Check Your internet connection.", false);
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	public void showAlertDialog(Context context, String title, String message,
			Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
					}
				});
		alertDialog.show();
	}
	public void showAlertDialogToast1( String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						Intent io = new Intent(MerchantSingleDetailActivity.this, MerchantOrderActivity.class);

						startActivity(io);
						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();

		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

	}
	public void showAlertDialogToastupdateimage( String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						Intent io = new Intent(MerchantSingleDetailActivity.this, MerchantSingleDetailActivity.class);
						Constants.adapterset=1;
						startActivity(io);
						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();

		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

	}
	public void showAlertDialogToast( String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						Intent io=new Intent(MerchantSingleDetailActivity.this,MerchantSingleDetailActivity.class);
						Constants.adapterset=0;
						startActivity(io);
						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();

		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

	}
	public void showAlertDialogToastupdate( String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						Intent io=new Intent(MerchantSingleDetailActivity.this,MerchantSingleDetailActivity.class);
           Constants.adapterset=0;
						startActivity(io);
						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();

		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

	}
	public void showAlertDialogToastdelorder() {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage("Do you want to cancel the order?");
		builder1.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						deleteorders();
						dialog.cancel();
					}
				});
		builder1.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();


	}
	public void onBackPressed() {
		exitAlret();
	}

	private void exitAlret() {
		AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
		localBuilder.setCancelable(false);
		localBuilder.setMessage("Do you want to Exit?");
		localBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {

						finish();

					}
				});
		localBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {
						paramDialogInterface.cancel();
					}
				});
		localBuilder.create().show();
	}
}
