package com.freshorders.freshorder.ui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.utils.CircularImageView;
import com.freshorders.freshorder.utils.Constants;
import  com.freshorders.freshorder.utils.DocUploadFileToServer;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

import eu.janmuller.android.simplecropimage.CropImage;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class ProfileActivityInitial extends Activity {

	CircularImageView CircularImageView;
	File imageFilePath1;
	String imagePath1="",fullname,companyName,email,address,payment;
	EditText EditTextViewName,EditTextViewCompanyName,EditTextViewMobile,EditTextViewEmail
	,EditTextViewAddress;
	Spinner spinnerPayment;
	Button buttonSave;
	JSONObject JsonAccountObject ;
	JSONArray JsonAccountArray ;
	DatabaseHandler databaseHandler;
	JsonServiceHandler JsonServiceHandler;
	String mobno;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(1);
		setContentView(R.layout.initial_profile_screen);
		netCheck();
		databaseHandler = new DatabaseHandler(getApplicationContext());
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();

		Cursor cur;
		cur = databaseHandler.getDetails();
		cur.moveToFirst();
		mobno=cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_username));

		CircularImageView = (CircularImageView) findViewById(R.id.imgProfile);
		imageFilePath1 = new File(Environment.getExternalStorageDirectory(),
				"Profile.jpg");
		spinnerPayment = (Spinner)findViewById(R.id.spinnerPayment);
		EditTextViewName = (EditText)findViewById(R.id.EditTextViewName);
		EditTextViewCompanyName = (EditText)findViewById(R.id.EditTextViewCompanyName);
		EditTextViewMobile = (EditText)findViewById(R.id.EditTextViewMobile);
		EditTextViewEmail = (EditText)findViewById(R.id.EditTextViewEmail);
		EditTextViewAddress = (EditText)findViewById(R.id.EditTextViewAddress);
		buttonSave=(Button)findViewById(R.id.buttonSave);
		
		EditTextViewMobile.setText(mobno);
		
		buttonSave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				fullname = EditTextViewName.getText()
						.toString();	
				companyName = EditTextViewCompanyName.getText()
						.toString();
				email = EditTextViewEmail.getText()
						.toString();
				address = EditTextViewAddress.getText()
						.toString();
				payment = spinnerPayment.getSelectedItem()
						.toString();
				
				Log.e(payment,payment);
				
		
		if (!fullname.isEmpty() && !fullname.startsWith(" ")) {
				
			if (!companyName.isEmpty() && !companyName.startsWith(" ")) {
				
				if (!email.isEmpty() && !email.startsWith(" ")&& email.contains("@") && email.contains(".")) {
					if (!address.isEmpty() && !address.startsWith(" ")) {
					
					
							sendData();
							if(!imagePath1.isEmpty()  ){
						}else{
								//showAlertDialogToast("Please select image file");
							//toastDisplay("Please select image file");
						}
					}else{
						showAlertDialogToast("Please Enter the valid  address");
					}

				}else{
					showAlertDialogToast("Please Enter the valid email address");
				}
			}else{
				showAlertDialogToast("Please Enter the company name");
			}
		}else{
			showAlertDialogToast("Please select image file");

		}
		
			}

			
		});
		
		String[] items = new String[]{"Monthly", "Quaterly","Half Yearly","Yearly"};
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items);
		spinnerPayment.setAdapter(adapter);
		CircularImageView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(Intent.ACTION_PICK);
				i.setType("image/*");
				startActivityForResult(i, 100);

			}
		});

	}
	private void sendData() {
		// TODO Auto-generated method stub
	SendDetails(fullname, address, email, companyName, payment);//,jobi
	}
	private void SendDetails(final String fullname2,final String address2,final String email2,
			final String companyName2,final String payment2) {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog = ProgressDialog.show(ProfileActivityInitial.this, "In Progress",
						"Validating...", true, true);
			}

			@Override
			protected void onPostExecute(Void result) {
				try {
					// JsonAccountArray =
					// JsonAccountObject.getJSONArray("response");
					// for (int i = 0; i < JsonAccountArray.length(); i++) {
					// JSONObject job=new JSONObject();
					// job=JsonAccountArray.getJSONObject(0);
					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);

					if (strStatus.equals("true")) {
						showAlertDialogToast("" + strMsg);
						ContentValues contentValue = new ContentValues();
						contentValue.put(DatabaseHandler.KEY_username, mobno);
						contentValue.put(DatabaseHandler.KEY_name, fullname2);
						contentValue.put(DatabaseHandler.KEY_payment, payment2);
						databaseHandler.updateUserDetails(contentValue);
						
						Constants.PAYMENT=payment2;


						Cursor cur;
						cur = databaseHandler.getDetails();
						cur.moveToFirst();

						String PaymentStatus=cur.getString(cur
								.getColumnIndex(DatabaseHandler.KEY_paymentStatus));

						Log.e("PaymentStatus", PaymentStatus);

						if(PaymentStatus.equals("Pending")) {
							if (Constants.USER_TYPE.equals("D")) {
								Intent intent = new Intent(ProfileActivityInitial.this, DealerPaymentActivity.class);

								startActivity(intent);
								overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
								finish();
							} else if (Constants.USER_TYPE.equals("M")){
								Intent intent = new Intent(ProfileActivityInitial.this, PaymentActivity.class);

								startActivity(intent);
								overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
								finish();
						}else{
                                Intent intent = new Intent(ProfileActivityInitial.this, SalesManOrderActivity.class);

                                startActivity(intent);
                                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                                finish();
                            }
						}else {

							if (Constants.USER_TYPE.equals("D")) {
								Intent intent = new Intent(ProfileActivityInitial.this, DealersOrderActivity.class);

								startActivity(intent);
								overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
								finish();
							} else if (Constants.USER_TYPE.equals("M")) {
								Intent intent = new Intent(ProfileActivityInitial.this, MerchantOrderActivity.class);

								startActivity(intent);
								overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
								finish();

							} else {
								Intent intent = new Intent(ProfileActivityInitial.this, SalesManOrderActivity.class);

								startActivity(intent);
								overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
								finish();
							}
						}

					} else {
						showAlertDialogToast(strMsg);
						
					}

				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					Log.e("loginActivityException", e.toString());
				}

				dialog.dismiss();
			}

			@Override
			protected Void doInBackground(Void... params) {
				dialog.show();

				JSONObject jsonObject = new JSONObject();
				try {
					
				jsonObject.put("fullname", fullname2);
				jsonObject.put("address", address2);
				jsonObject.put("emailid", email2);
				jsonObject.put("companyname", companyName2);
				jsonObject.put("pymttenure", payment2);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				JsonServiceHandler = new JsonServiceHandler(
						Utils.strAddProfileDetail+Constants.USER_ID, jsonObject.toString(),
						ProfileActivityInitial.this);
				JsonAccountObject = JsonServiceHandler.ServiceData();
				return null;
			}
		}.execute(new Void[]{});
	}
	@SuppressWarnings("deprecation")
	@SuppressLint("NewApi")
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == 100) { // image
				imageDecode(imageFilePath1, data, 1);
				Log.e("data",""+data);
			} else if (requestCode == 111) {
				String path = data.getStringExtra(CropImage.IMAGE_PATH);
				if (path == null) {
					return;
				}
				imagePath1 = path;
				Bitmap b = BitmapFactory.decodeFile(imageFilePath1.getPath());
				CircularImageView.setImageBitmap(b);
				DocUploadFileToServer ufs = new DocUploadFileToServer(ProfileActivityInitial.this,
						imagePath1,Constants.USER_ID);//,jobi
				ufs.execute();
				// imageViewAdded1.setBackground(null);
				try {
					CircularImageView.setBackground(null);
				} catch (NoSuchMethodError e) {
					try {
						CircularImageView.setBackgroundDrawable(null);
					} catch (NoSuchMethodError e1) {
						e1.printStackTrace();
					}
				}

			}
		}
	}

	public void imageDecode(File iPath, Intent data, int i) {

		try {
			if (iPath.exists()) {
				iPath.delete();
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		try {
			Uri selectedImage = data.getData();
			Log.e("imageDecode", selectedImage.toString());
			InputStream inputStream = getApplicationContext()
					.getContentResolver().openInputStream(data.getData());
			FileOutputStream fileOutputStream = new FileOutputStream(iPath);
			copyStream(inputStream, fileOutputStream);
			fileOutputStream.close();
			inputStream.close();
			Intent intent = new Intent(ProfileActivityInitial.this,
					CropImage.class);
			intent.putExtra(CropImage.IMAGE_PATH, iPath.getPath());
			intent.putExtra(CropImage.SCALE, true);
			intent.putExtra(CropImage.ASPECT_X, 1);
			intent.putExtra(CropImage.ASPECT_Y, 1);
			intent.putExtra(CropImage.OUTPUT_X, 600);
			intent.putExtra(CropImage.OUTPUT_Y, 600);
			if (i == 1) {
				startActivityForResult(intent, 111);

			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void copyStream(InputStream input, OutputStream output)
			throws IOException {

		byte[] buffer = new byte[1024];
		int bytesRead;
		while ((bytesRead = input.read(buffer)) != -1) {
			output.write(buffer, 0, bytesRead);
		}
	}
	
	protected void toastDisplay(String msg) {
		Toast toast = Toast.makeText(ProfileActivityInitial.this, msg,
				Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

	}
	public boolean netCheck() {
		// for network connection
		try {
			ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetwork = connectionManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			Object result = null;
			if (mWifi.isConnected() || mNetwork.isConnected()) {
				return true;
			}

			else if (result == null) {
				showAlertDialog(ProfileActivityInitial.this,
						"No Internet Connection",
						"Please Check Your internet connection.", false);
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	public void showAlertDialog(Context context, String title, String message,
			Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
					}
				});
		alertDialog.show();
	}

	public void showAlertDialogToast( String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();

		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

	}
	public void onBackPressed() {
		exitAlret();
	}

	private void exitAlret() {
		AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
		localBuilder.setCancelable(false);
		localBuilder.setMessage("Do you want to Exit?");
		localBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {

						finish();

					}
				});
		localBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {
						paramDialogInterface.cancel();
					}
				});
		localBuilder.create().show();
	}
}
