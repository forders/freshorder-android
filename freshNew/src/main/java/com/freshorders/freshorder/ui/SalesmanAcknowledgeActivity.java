package com.freshorders.freshorder.ui;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.text.format.Time;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.activity.DistanceCalActivity;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.toonline.migration.PendingDataMigrationActivity;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.CsvFileUploadToServer;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.SalesmanLocation;
import com.freshorders.freshorder.utils.Utils;
import com.opencsv.CSVWriter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.Format;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class SalesmanAcknowledgeActivity extends Activity {

	TextView menuIcon, textViewAssetMenuProfile, textViewAssetMenuMyOrders,
			textViewAssetMenuMyDealers, textViewAssetMenuProducts,
			textViewAssetMenuPayment, textViewAssetMenuComplaint,
			textViewAssetMenuSignout,textViewAssetMenuCreateOrder,textViewAssetMenuClientVisit,
			textViewAssetMenuPaymentCollection,textViewAssetMenuRefresh,textViewAssetMenuDistanceCalculation,
			textViewAssetMenuStockEntry,textViewAssetMenuAddMerchant,textViewAssetMenuAcknowledge
			,textViewAssetMenusurveyuser,textViewAssetMenudownloadscheme,textViewAssetMenuMyDayPlan,textViewAssetMenuMySales,
			textViewAssetMenuDistStock, textViewAssetMenuStockOnly, textViewAssetMenuMigration, textViewAssetMenuStockAudit;
	private static int serialNoCounter = 0;
	int menuCliccked;
	public static BroadcastReceiver locationtrack;

	LinearLayout linearLayoutMenuParent,linearLayoutProfile, linearLayoutMyOrders,
			linearLayoutMyDealers, linearLayoutProducts, linearLayoutPayment,
			linearLayoutComplaint, linearLayoutSignout,linearLayoutCreateOrder,linearLayoutClientVisit,
            linearLayoutPaymentCollection,linearLayoutRefresh,linearlayoutdump1,linearLayoutDistanceCalculation,
			linearLayoutAcknowledge,linearLayoutStockEntry,linearLayoutAddMerchant,linearLayoutsurveyuser,linearLayoutdownloadscheme
			,linearLayoutMyDayPlan,linearLayoutMySales,linearLayoutDistStock, linearLayoutStockOnly, linearLayoutMigration, linearLayoutStockAudit;

	private void showMenu(){

		MyApplication app = MyApplication.getInstance();

		if(!app.isCreateOrder()){
			linearLayoutCreateOrder.setVisibility(View.GONE);
		}
		if(!app.isProfile()){
			linearLayoutProfile.setVisibility(View.GONE);
		}
		if(!app.isMyDayPlan()){
			linearLayoutMyDayPlan.setVisibility(View.GONE);
		}
		if(!app.isMyOrders()){
			linearLayoutMyOrders.setVisibility(View.GONE);
		}
		if(!app.isAddMerchant()){
			linearLayoutAddMerchant.setVisibility(View.GONE);
		}
		if(!app.isMySales()){
			linearLayoutMySales.setVisibility(View.GONE);
		}
		if(!app.isPostNotes()){
			linearLayoutComplaint.setVisibility(View.GONE);
		}
		if(!app.isMyClientVisit()){
			linearLayoutClientVisit.setVisibility(View.GONE);
		}
		if(!app.isAcknowledge()){
			linearLayoutAcknowledge.setVisibility(View.GONE);
		}
		if(!app.isPaymentCollection()){
			linearLayoutPaymentCollection.setVisibility(View.GONE);
		}
		if(!app.isPkdDataCapture()){
			linearLayoutStockEntry.setVisibility(View.GONE);
		}
		if(!app.isDistributorStock()){
			linearLayoutDistStock.setVisibility(View.GONE);
		}
		if(!app.isSurveyUser()){
			linearLayoutsurveyuser.setVisibility(View.GONE);
		}
		if(!app.isDownLoadScheme()){
			linearLayoutdownloadscheme.setVisibility(View.GONE);
		}
		if(!app.isDistanceCalculation()){
			linearLayoutDistanceCalculation.setVisibility(View.GONE);
		}
		if(!app.isRefresh()){
			linearLayoutRefresh.setVisibility(View.GONE);
		}
		if(!app.isLogout()){
			linearLayoutSignout.setVisibility(View.GONE);
		}
		if(!app.isClosingStock()){
			linearLayoutStockOnly.setVisibility(View.GONE);
		}
		if(!app.isClosingStockAudit()){
			linearLayoutStockAudit.setVisibility(View.GONE);
		}
		if(!app.isPendingData()){
			linearLayoutMigration.setVisibility(View.GONE);
		}
	}

	@Override
	protected void onStart() {
		super.onStart();

		if(Constants.USER_ID == null || Constants.USER_ID.isEmpty()){
			if(databaseHandler == null){
				databaseHandler = new DatabaseHandler(getApplicationContext());
			}
			Cursor cur;
			cur = databaseHandler.getDetails();
			if(cur != null && cur.getCount() > 0){
				Constants.USER_ID = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_id));
				cur.close();
			}
		}
	}

	JSONObject JsonAccountObject = null;
	JSONArray JsonAccountArray = null;
	JsonServiceHandler JsonServiceHandler ;

	DatabaseHandler databaseHandler;
	public static TextView textViewUploadedFile;
	public static Button buttonSend,buttonGenerateCSV,buttonShare;
	public static EditText EditTextViewDescription,textviewdesignation,textviewtrader;
	String Description,DuserID,traders,todate,totime,designation;
	public int monthnew,newmnth,newyear,yearnew,newday,daynew;
	public int year,hr,sec,mins;
	public static Date dateStr = null;
	public static String moveto="NULL",PaymentStatus="NULL";
    CSVWriter mWriter;
	BufferedWriter bw;
	public static String csvpath ="";

	public StringBuilder datebuilder;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(1);

		setContentView(R.layout.salesman_acknowledge_screen);


		databaseHandler = new DatabaseHandler(getApplicationContext());
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();
        locationtrack = new SalesmanLocation();

		Cursor cur;
		cur = databaseHandler.getDetails();
		cur.moveToFirst();
		showCurrentDateOnView();
		getachnowledgedetail();
		Log.e("Insertion Check",
				cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_username)));
		Log.e("Insertion Check",
				cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_password)));
		Log.e("Insertion Check",
				cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_usertype)));
		PaymentStatus = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_paymentStatus));
		/////Kumaravel 22-07-2019
		Constants.USER_ID = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_id));


		buttonSend = (Button) findViewById(R.id.buttonSend);
		buttonGenerateCSV = (Button) findViewById(R.id.buttonGenerateCSV);
		buttonShare= (Button) findViewById(R.id.buttonShare);


		textviewtrader = (EditText) findViewById(R.id.textviewtrader);
        textViewUploadedFile= (TextView) findViewById(R.id.textViewUploadedFile);
		textviewdesignation= (EditText) findViewById(R.id.textviewdesignation);
		EditTextViewDescription = (EditText) findViewById(R.id.EditTextViewDescription);

		linearLayoutCreateOrder = (LinearLayout) findViewById(R.id.linearLayoutCreateOrder);
		linearLayoutProfile = (LinearLayout) findViewById(R.id.linearLayoutProfile);
		linearLayoutMyOrders = (LinearLayout) findViewById(R.id.linearLayoutMyOrders);
		linearLayoutMyDealers = (LinearLayout) findViewById(R.id.linearLayoutMyDealers);
		linearLayoutProducts = (LinearLayout) findViewById(R.id.linearLayoutProducts);
		linearLayoutPayment = (LinearLayout) findViewById(R.id.linearLayoutPayment);
		linearLayoutComplaint = (LinearLayout) findViewById(R.id.linearLayoutComplaint);
		linearLayoutClientVisit = (LinearLayout) findViewById(R.id.linearLayoutClientVisit);
		linearLayoutSignout = (LinearLayout) findViewById(R.id.linearLayoutSignout);
		linearLayoutPaymentCollection = (LinearLayout) findViewById(R.id.linearLayoutPaymentCollection);

		linearLayoutRefresh = (LinearLayout) findViewById(R.id.linearLayoutRefresh);
		textViewAssetMenuRefresh = (TextView) findViewById(R.id.textViewAssetMenuRefresh);
		linearlayoutdump1 = (LinearLayout) findViewById(R.id.linearlayoutdump1);

		linearLayoutAcknowledge= (LinearLayout) findViewById(R.id.linearLayoutAcknowledge);
		linearLayoutStockEntry= (LinearLayout) findViewById(R.id.linearLayoutStockEntry);
		linearLayoutAddMerchant = (LinearLayout) findViewById(R.id.linearLayoutAddMerchant);
		linearLayoutDistanceCalculation= (LinearLayout) findViewById(R.id.linearLayoutDistanceCalculation);
		linearLayoutdownloadscheme= (LinearLayout) findViewById(R.id.linearLayoutdownloadscheme);
		linearLayoutsurveyuser= (LinearLayout) findViewById(R.id.linearLayoutsurveyuser);
		linearLayoutMyDayPlan = (LinearLayout) findViewById(R.id.linearLayoutMyDayPlan);
		linearLayoutMySales = (LinearLayout) findViewById(R.id.linearLayoutMySales);

        textViewUploadedFile.setText("");
        textViewUploadedFile.setVisibility(TextView.GONE);

		linearLayoutStockOnly = (LinearLayout) findViewById(R.id.linearLayoutStockOnly);
		linearLayoutStockAudit = findViewById(R.id.linearLayoutStockAudit);
		linearLayoutMigration = (LinearLayout) findViewById(R.id.linearLayoutMigration);



		if (Constants.USER_TYPE.equals("D")) {
			linearLayoutMyDealers.setVisibility(View.GONE);
			linearLayoutProducts.setVisibility(View.VISIBLE);
			linearLayoutCreateOrder.setVisibility(View.GONE);
			linearLayoutPaymentCollection.setVisibility(View.GONE);
			linearLayoutDistanceCalculation.setVisibility(View.GONE);
			linearLayoutRefresh.setVisibility(View.GONE);
		} else if (Constants.USER_TYPE.equals("M")) {
			linearLayoutMyDealers.setVisibility(View.VISIBLE);
			linearLayoutProducts.setVisibility(View.GONE);
			linearLayoutCreateOrder.setVisibility(View.GONE);
			linearLayoutPaymentCollection.setVisibility(View.GONE);
			linearLayoutRefresh.setVisibility(View.GONE);
			linearLayoutDistanceCalculation.setVisibility(View.GONE);
			linearlayoutdump1.setVisibility(View.VISIBLE);
		} else {
			linearLayoutPayment.setVisibility(LinearLayout.GONE);
			linearLayoutMyDealers.setVisibility(View.GONE);
			linearLayoutProducts.setVisibility(View.GONE);
			linearLayoutCreateOrder.setVisibility(View.VISIBLE);
			linearLayoutDistanceCalculation.setVisibility(View.VISIBLE);
			linearLayoutRefresh.setVisibility(View.VISIBLE);
		}
		buttonGenerateCSV.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				int permissionCheck1 = ContextCompat.checkSelfPermission(SalesmanAcknowledgeActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE);

				if (permissionCheck1 != PackageManager.PERMISSION_GRANTED) {
					ActivityCompat.requestPermissions(
							SalesmanAcknowledgeActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constants.READ_EXTERNAL_LOCATION);
				} else {
					exportChartCsv();
				}
			}
		});

		buttonShare.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
			Intent io=new Intent (SalesmanAcknowledgeActivity.this,EODActivity.class);
				startActivity(io);
				finish();
			}
		});
		buttonSend.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Cursor cur;
				cur = databaseHandler.getdealer();
				Log.e("dealercount", String.valueOf(cur.getCount()));


				if (cur != null && cur.getCount() > 0) {
					if (cur.moveToLast()) {

						DuserID = cur.getString(3);
						Log.e("DuserID", DuserID);
					}
				}

				Description = EditTextViewDescription.getText().toString().trim();
				traders = textviewtrader.getText().toString().trim();
				designation = textviewdesignation.getText().toString().trim();

				if (!traders.startsWith(".") && !traders.isEmpty()) {
					Log.e("textview", String.valueOf(textViewUploadedFile.getText()));
					if (!textViewUploadedFile.getText().equals("")) {

						Log.e("suserid", Constants.USER_ID);
						Log.e("duserid", DuserID);

						Log.e("traders", traders);
						Log.e("particular", Description);
						Log.e("designation", designation);

						CsvFileUploadToServer ufs = new CsvFileUploadToServer(SalesmanAcknowledgeActivity.this,
								csvpath, Constants.USER_ID, DuserID, todate, totime, traders, designation, Description);
						ufs.execute();


					} else {
						showAlertDialogToast("Please upload CSV file");
					}
				} else {
					showAlertDialogToast("Name should not be empty");
				}


			}


		});

		menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
		textViewAssetMenuCreateOrder = (TextView) findViewById(R.id.textViewAssetMenuCreateOrder);
		textViewAssetMenuProfile = (TextView) findViewById(R.id.textViewAssetMenuProfile);
		textViewAssetMenuMyOrders = (TextView) findViewById(R.id.textViewAssetMenuMyOrders);
		textViewAssetMenuMyDealers = (TextView) findViewById(R.id.textViewAssetMenuMyDealers);
		textViewAssetMenuProducts = (TextView) findViewById(R.id.textViewAssetMenuProducts);
		textViewAssetMenuPayment = (TextView) findViewById(R.id.textViewAssetMenuPayment);
		textViewAssetMenuComplaint = (TextView) findViewById(R.id.textViewAssetMenuComplaint);
		textViewAssetMenuSignout = (TextView) findViewById(R.id.textViewAssetMenuSignout);
		textViewAssetMenuClientVisit = (TextView) findViewById(R.id.textViewAssetMenuClientVisit);
		textViewAssetMenuPaymentCollection = (TextView) findViewById(R.id.textViewAssetMenuPaymentCollection);
		linearLayoutMenuParent = (LinearLayout) findViewById(R.id.linearLayoutMenuParent);
		textViewAssetMenuDistanceCalculation= (TextView) findViewById(R.id.textViewAssetMenuDistanceCalculation);
		textViewAssetMenuAddMerchant = (TextView) findViewById(R.id.textViewAssetMenuAddMerchant);
		textViewAssetMenuAcknowledge = (TextView) findViewById(R.id.textViewAssetMenuAcknowledge);
		textViewAssetMenuStockEntry = (TextView) findViewById(R.id.textViewAssetMenuStockEntry);

		textViewAssetMenudownloadscheme= (TextView) findViewById(R.id.textViewAssetMenudownloadscheme);
		textViewAssetMenusurveyuser= (TextView) findViewById(R.id.textViewAssetMenusurveyuser);
		textViewAssetMenuMyDayPlan= (TextView) findViewById(R.id.textViewAssetMenuMyDayPlan);
		textViewAssetMenuMySales= (TextView) findViewById(R.id.textViewAssetMenuMySales);

		Typeface font = Typeface.createFromAsset(getAssets(),
				"fontawesome-webfont.ttf");
		menuIcon.setTypeface(font);
		textViewAssetMenuCreateOrder.setTypeface(font);
		textViewAssetMenuProfile.setTypeface(font);
		textViewAssetMenuMyOrders.setTypeface(font);
		textViewAssetMenuMyDealers.setTypeface(font);
		textViewAssetMenuProducts.setTypeface(font);
		textViewAssetMenuPayment.setTypeface(font);
		textViewAssetMenuComplaint.setTypeface(font);
		textViewAssetMenuSignout.setTypeface(font);
		textViewAssetMenuClientVisit.setTypeface(font);
		textViewAssetMenuPaymentCollection.setTypeface(font);
		textViewAssetMenuRefresh.setTypeface(font);
		textViewAssetMenuDistanceCalculation.setTypeface(font);
		textViewAssetMenuAcknowledge.setTypeface(font);
		textViewAssetMenuStockEntry.setTypeface(font);
		textViewAssetMenuAddMerchant.setTypeface(font);

		textViewAssetMenusurveyuser.setTypeface(font);
		textViewAssetMenudownloadscheme.setTypeface(font);
		textViewAssetMenuMyDayPlan.setTypeface(font);
		textViewAssetMenuMySales.setTypeface(font);

		linearLayoutDistStock = (LinearLayout) findViewById(R.id.linearLayoutDistStock);
		textViewAssetMenuDistStock = (TextView) findViewById(R.id.textViewAssetMenuDistStock);
		textViewAssetMenuDistStock.setTypeface(font);

		textViewAssetMenuStockOnly = (TextView) findViewById(R.id.textViewAssetMenuStockOnly);
		textViewAssetMenuStockAudit = (TextView) findViewById(R.id.textViewAssetMenuStockAudit);
		textViewAssetMenuMigration = findViewById(R.id.textViewAssetMenuMigration);

		textViewAssetMenuStockOnly.setTypeface(font);
		textViewAssetMenuStockAudit.setTypeface(font);
		textViewAssetMenuMigration.setTypeface(font);

		menuIcon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (menuCliccked == 0) {
					linearLayoutMenuParent.setVisibility(View.VISIBLE);
					menuCliccked = 1;
				} else {
					linearLayoutMenuParent.setVisibility(View.GONE);
					menuCliccked = 0;
				}

			}
		});

		linearLayoutMenuParent.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);

				menuCliccked = 0;
			}
		});

		linearLayoutProfile.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				moveto = "profile";
				if (netCheckWithoutAlert() == true) {
					getData();
				} else {
					showAlertDialogToast("Please Check Your internet connection");
				}


			}
		});
		linearLayoutDistanceCalculation.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				//moveto = "profile";

					Intent into=new Intent(SalesmanAcknowledgeActivity.this, DistanceCalActivity.class);
					startActivity(into);



			}
		});
		linearLayoutMyOrders.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				Constants.orderstatus = "Success";
				moveto = "myorder";
				if (netCheckWithoutAlert()) {
					getData();
				} else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment before place order");
					} else {

						Intent io = new Intent(SalesmanAcknowledgeActivity.this,
								SalesManOrderActivity.class);
						io.putExtra("Key", "menuclick");
						Constants.setimage = null;
						startActivity(io);
						finish();
					}
				}


			}
		});

		linearLayoutRefresh.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				Log.e("Clicked", "Clicked");

				if (netCheck()) {

					final Dialog qtyDialog = new Dialog(SalesmanAcknowledgeActivity.this);
					qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					qtyDialog.setContentView(R.layout.warning_dialog);

					final Button buttonUpdateOk, buttonUpdateCancel;

					final TextView textQtyValidate;

					buttonUpdateOk = (Button) qtyDialog
							.findViewById(R.id.buttonUpdateOk);
					buttonUpdateCancel = (Button) qtyDialog
							.findViewById(R.id.buttonUpdateCancel);

					textQtyValidate = (TextView) qtyDialog
							.findViewById(R.id.textQtyValidate);
					qtyDialog.show();


					buttonUpdateOk.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							databaseHandler.synTableDelete();
							Constants.refreshscreen = "acknowledge";
							Intent io = new Intent(SalesmanAcknowledgeActivity.this, RefreshActivity.class);
							startActivity(io);
							finish();
							qtyDialog.dismiss();
						}

					});

					buttonUpdateCancel
							.setOnClickListener(new OnClickListener() {

								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub

									qtyDialog.dismiss();

								}
							});

				}
			}

		});


		linearLayoutMyDealers.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				moveto = "mydealer";
				if (netCheckWithoutAlert() == true) {
					getData();
				} else {
					showAlertDialogToast("Please Check Your internet connection");
				}


			}
		});

		linearLayoutProducts.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				moveto = "product";

				if (netCheckWithoutAlert() == true) {
					getData();
				} else {
					showAlertDialogToast("Please Check Your internet connection");
				}


			}
		});

		linearLayoutCreateOrder.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				MyApplication.getInstance().setTemplate8ForTemplate2(false);

				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				moveto = "createorder";
				Constants.checkproduct = 0;
				Constants.orderid = "";

				if (netCheckWithoutAlert()) {
					getData();
				} else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment to place order");
					} else {
						Intent io = new Intent(SalesmanAcknowledgeActivity.this, CreateOrderActivity.class);
						startActivity(io);
						finish();
					}
				}


			}
		});

		linearLayoutPayment.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				moveto = "payment";

				if (netCheckWithoutAlert() == true) {
					getData();
				} else {
					showAlertDialogToast("Please Check Your internet connection");
				}


			}
		});

		linearLayoutComplaint.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				moveto = "complaint";
				if (netCheckWithoutAlert() == true) {
					getData();
				} else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment");
					} else {
						Intent io = new Intent(SalesmanAcknowledgeActivity.this,
								MerchantComplaintActivity.class);
						startActivity(io);
						finish();
					}
				}

			}
		});

		linearLayoutClientVisit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				moveto = "clientvisit";
				if (netCheckWithoutAlert() == true) {
					getData();
				} else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment");
					} else {
						Intent io = new Intent(SalesmanAcknowledgeActivity.this,
								SMClientVisitHistory.class);
						startActivity(io);
						finish();
						/*if (Constants.USER_TYPE.equals("D")) {
							Intent io = new Intent(SalesmanAcknowledgeActivity.this,
									DealerClientVisit.class);
							startActivity(io);
							finish();
						} else {
							Intent io = new Intent(SalesmanAcknowledgeActivity.this,
									SMClientVisitHistory.class);
							startActivity(io);
							finish();
						} */
					}
				}


			}
		});

		linearLayoutPaymentCollection.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				moveto = "paymentcoll";
				if (netCheckWithoutAlert() == true) {
					getData();
				} else {
					showAlertDialogToast("Please Check Your internet connection");
				}


			}
		});

		linearLayoutStockEntry.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				moveto = "stockentry";
				if( netCheckWithoutAlert()==true){
					getData();
				}else{
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment");
					}else {
						Intent io = new Intent(SalesmanAcknowledgeActivity.this,
								OutletStockEntry.class);
						startActivity(io);
						finish();
					}
				}
			}
		});

		linearLayoutDistStock.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				moveto = "diststock";
				if( netCheckWithoutAlert()==true){
					getData();
				}else{
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment");
					} else {
						Intent io = new Intent(SalesmanAcknowledgeActivity.this,
								DistributrStockActivity.class);
						startActivity(io);
						finish();
					}
				}
			}
		});

		linearLayoutAcknowledge.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;

			}
		});

		linearLayoutAddMerchant.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				moveto = "addmerchant";
				if( netCheckWithoutAlert()==true){
					getData();
				}else{
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment before place order");
					} else {
						Intent to = new Intent(SalesmanAcknowledgeActivity.this,
								AddMerchantNew.class);
						startActivity(to);
						finish();
					}
				}

			}
		});
		linearLayoutdownloadscheme.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(netCheck()){

					final Dialog qtyDialog = new Dialog(SalesmanAcknowledgeActivity.this);
					qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					qtyDialog.setContentView(R.layout.warning_dialog);

					final Button buttonUpdateOk, buttonUpdateCancel;

					final TextView textQtyValidate;

					buttonUpdateOk = (Button) qtyDialog
							.findViewById(R.id.buttonUpdateOk);
					buttonUpdateCancel = (Button) qtyDialog
							.findViewById(R.id.buttonUpdateCancel);

					textQtyValidate = (TextView) qtyDialog
							.findViewById(R.id.textQtyValidate);
					qtyDialog.show();


					buttonUpdateOk.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							databaseHandler.deletescheme();
							Constants.downloadScheme = "acknowledge";
							Intent io = new Intent(SalesmanAcknowledgeActivity.this, SchemeDownload.class);
							startActivity(io);
							finish();
							qtyDialog.dismiss();
						}

					});

					buttonUpdateCancel
							.setOnClickListener(new View.OnClickListener() {

								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub

									qtyDialog.dismiss();

								}
							});

				}else{
					showAlertDialogToast("Please Check Your internet connection");
				}
			}
		});
		linearLayoutsurveyuser.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (netCheckWithoutAlert() == true) {
					linearLayoutMenuParent.setVisibility(View.GONE);
					menuCliccked = 0;
					moveto="surveyuser";
					getData();
				}else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment before place order");
					} else {
						Intent to = new Intent(SalesmanAcknowledgeActivity.this, SurveyMerchant.class);
						startActivity(to);
						finish();
					}
				}

              /*  Intent to = new Intent(SalesManOrderActivity.this,
                        SalesmanAcknowledgeActivity.class);
                startActivity(to);
                finish();*/

			}
		});

		linearLayoutMyDayPlan.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (netCheckWithoutAlert() == true) {
					linearLayoutMenuParent.setVisibility(View.GONE);
					menuCliccked = 0;
					moveto="mydayplan";
					getData();
				}else {
					showAlertDialogToast("Please Check Your internet connection");
				}
			}
		});

		linearLayoutMySales.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (netCheckWithoutAlert() == true) {
					linearLayoutMenuParent.setVisibility(View.GONE);
					menuCliccked = 0;
					moveto="mysales";
					getData();
				}else {
					showAlertDialogToast("Please Check Your internet connection");
				}
			}
		});

		linearLayoutSignout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				// TODO Auto-generated method stub
				databaseHandler.delete();
				Intent io = new Intent(SalesmanAcknowledgeActivity.this, SigninActivity.class);
				startActivity(io);
				finish();
			}
		});

		linearLayoutMigration.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				Intent io = new Intent(SalesmanAcknowledgeActivity.this,
						PendingDataMigrationActivity.class);
				startActivity(io);
			}
		});

		linearLayoutStockOnly.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				Constants.checkproduct = 0;
				Constants.orderid = "";
				Constants.SalesMerchant_Id = "";
				Constants.Merchantname = "";
				MyApplication.getInstance().setTemplate8ForTemplate2(true);
				if (netCheck()) {
					Intent io = new Intent(SalesmanAcknowledgeActivity.this,
							ClosingStockDashBoardActivity.class);
					startActivity(io);
					finish();
				} else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals("") || PaymentStatus.equalsIgnoreCase("null")) {
						showAlertDialogToast("Please make payment before place order");
					} else {
						Intent io = new Intent(SalesmanAcknowledgeActivity.this,
								ClosingStockDashBoardActivity.class);
						startActivity(io);
						finish();
					}
				}
			}
		});

		linearLayoutStockAudit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				MyApplication.getInstance().setTemplate8ForTemplate2(false);
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				Constants.checkproduct = 0;
				Constants.orderid = "";
				Constants.SalesMerchant_Id = "";
				Constants.Merchantname = "";
				if (netCheck()) {
					Intent io = new Intent(SalesmanAcknowledgeActivity.this,
							ClosingStockAudit.class);
					startActivity(io);
					finish();
				} else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals("") || PaymentStatus.equalsIgnoreCase("null")) {
						showAlertDialogToast("Please make payment before place order");
					} else {
						Intent io = new Intent(SalesmanAcknowledgeActivity.this,
								ClosingStockAudit.class);
						startActivity(io);
						finish();
					}
				}
			}
		});

		//////////////
		showMenu();
		//////////////
	}


	@TargetApi(23)
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permission, @NonNull int[] grantResults) {

		Log.e("requestcode", String.valueOf(requestCode));
		Log.e("size", String.valueOf(grantResults.length));

		if (requestCode == Constants.LOCATION_PERMISSION_REQUEST_CODE) {
			for (int i=0;i<permission.length;i++){
				Log.e("requestcode", String.valueOf(grantResults[0] ));
				if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
					exportChartCsv();
				} else {
					requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, Constants.READ_EXTERNAL_LOCATION);
				}
			}
		}
	}

	private String getMerchantAddress(String muserid){
		String address = "";
		Cursor curMerchant = databaseHandler.getMerchantAddressByUserId(muserid);
		if (curMerchant != null && curMerchant.getCount() > 0) {
			curMerchant.moveToFirst();
			address = curMerchant.getString(curMerchant.getColumnIndex("address"));
			curMerchant.close();
		}
		return address;
	}

    private void exportChartCsv() {
        try {

			buttonGenerateCSV.setVisibility(View.INVISIBLE);

			serialNoCounter = 0;

            JSONArray resultSet = new JSONArray();
            String date = "yyyy-MM-dd";
            String datewithtime = "yyyy-MM-dd HH:mm:ss";
            SimpleDateFormat inputFormat = new SimpleDateFormat(date);
            String dt =inputFormat.format(new Date());
            SimpleDateFormat inputFormat1 = new SimpleDateFormat(datewithtime);
            String dtwithtm =inputFormat1.format(new Date());
            String status ="Success";

            Cursor cur;
            cur = databaseHandler.getDetails();
            cur.moveToFirst();
            Log.e("userId", cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_id)));
            String userId = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_id));
            String beat = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_selectedBeat));

            boolean isCompanyModel = false;
            String login_model = databaseHandler.getSalesmanDataUserSetting("BEAT_TYPE");
            if(login_model.equalsIgnoreCase("C")){
            	isCompanyModel = true;
            	Log.e("Acknowlodge","................Company Model");
			}

            String files = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/"+userId+"_"+dtwithtm+".csv";
			bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(files, true), StandardCharsets.UTF_8));
			mWriter = new CSVWriter(bw);
			//mWriter = new CSVWriter(new FileWriter(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath() + "/"+userId+"_"+dtwithtm+".csv"));
			String[] mExportChartHeaders;
			if(isCompanyModel){
				mExportChartHeaders = new String[]{
						"S.No",
						"Order Date",
						"Distributor Name",
						"Beat",
						"Store Name",
						"Address",
						"Product Name",
						"Quantity",
						"Sales Value",


				};
			}else {
				mExportChartHeaders = new String[]{
						"S.No",
						"Order Date",
						"Store Name",
						"Address",
						"Product Name",
						"Quantity",
						"Sales Value"
				};
			}
            mWriter.writeNext(mExportChartHeaders);


            Cursor curs;
            curs = databaseHandler.getSuccessheader(status, dt);
            if(curs != null && cur.getCount() > 0) {
				Log.e("HeaderCount", String.valueOf(curs.getCount()));
				curs.moveToFirst();
				while (curs.isAfterLast() == false) {

					int totalColumn = curs.getColumnCount();
					JSONObject rowObject = new JSONObject();

					//////////////////Kumaravel 29-06-2019
					String approxVal = curs.getString(curs.getColumnIndex("aprxordval"));
					if(approxVal != null && !approxVal.isEmpty()){
						try {
							float val = Float.parseFloat(approxVal);
							if(val > 0){
								Log.e("aprxordval", " .....................::: " + approxVal);
							}else {
								curs.moveToNext();
								continue;
							}
						}catch (Exception e){
							curs.moveToNext();
							continue;
						}
					}else {
						curs.moveToNext();
						continue;
					}
					////////////////////////////// end modification..............

					for (int i = 0; i < totalColumn; i++) {
						if (curs.getColumnName(i) != null) {
							try {

								if (curs.getString(i) != null) {
									if (!curs.getString(i).equals("New User")) {
										Log.d("TAG_NAME", curs.getString(i));
										rowObject.put(curs.getColumnName(i), curs.getString(i));
									}else if(curs.getString(i).equals("New User")){  ///////////// this special case handled for merchant address
										if(curs.getColumnName(i).equalsIgnoreCase("muserid")){
											Log.d("TAG_NAME", curs.getString(i));
											rowObject.put(curs.getColumnName(i), curs.getString(i));
										}
									}

								} else {
									if (!curs.getColumnName(i).equals("muserid")) {
										rowObject.put(curs.getColumnName(i), "");
									}
								}
							} catch (Exception e) {
								Log.d("TAG_NAME", e.getMessage());
							}
						}

					}
					// curs.moveToNext();

					JSONArray resultDetail = new JSONArray();


					Cursor cursor;
					cursor = databaseHandler.getorderdetailForAcknowledge(rowObject.getString("oflnordid"));
					///////////////////////
					if(!(cursor != null && cursor.getCount() > 0)){
						curs.moveToNext();
						continue;
					}
					//////////////////////
					Log.e("DetailCount", String.valueOf(cursor.getCount()));
					cursor.moveToFirst();
					while (cursor.isAfterLast() == false) {

						Log.e("columnCount", String.valueOf(cursor.getColumnCount()));
						JSONObject detailObject = new JSONObject();

						for (int i = 0; i < 21; i++) {
							if (cursor.getColumnName(i) != null) {

								try {

									if (cursor.getString(i) != null) {
										Log.d("TAG_NAME", cursor.getString(i));
										detailObject.put(cursor.getColumnName(i), cursor.getString(i));
									} else {
										detailObject.put(cursor.getColumnName(i), "");
									}
								} catch (Exception e) {
									Log.d("TAG_NAME", e.getMessage());
								}
							}

						}

						resultDetail.put(detailObject);
						cursor.moveToNext();
					}

					rowObject.put("orderdtls", resultDetail);
					resultSet.put(rowObject);
					curs.moveToNext();
				}
				curs.close();
				Log.e("Savearray", resultSet.toString());
			}else {
            	//////////////////////////////Kumaravel [nambi told no need to show qty-> 0 value]
				showAlertDialogToast("No data avail to export");
				return;
			}

            ///////////////Kumaravel 29-06-2019
            if(resultSet.length() <= 0){
				showAlertDialogToast("No data avail to export");
				return;
			}


            for (int i = 0; i < resultSet.length(); i++) {
                JSONObject orderheader = (JSONObject) resultSet.get(i);
                JSONArray innerJsonArray =  (JSONArray) orderheader.getJSONArray("orderdtls");

                ///////by Kumaravel for add new header address
				String address = "";
				String muserid = orderheader.getString("muserid");
				if(muserid.isEmpty()){
					address = "";
				}else if(muserid.equalsIgnoreCase("New User")){
					String orderId = orderheader.getString("oflnordno");
					if(orderId != null && !orderId.isEmpty()){
						Cursor cur_map = databaseHandler.getMORMerchantIdByOrderId(orderId);
						if(cur_map != null && cur_map.getCount() > 0){
							cur_map.moveToFirst();
							muserid = cur_map.getString(cur_map.getColumnIndex("merchant_id"));
							address = getMerchantAddress(muserid);
							cur_map.close();
						}
					}
				}else {
					address = getMerchantAddress(muserid);
				}

				if(isCompanyModel){

					String distributor = new PrefManager(SalesmanAcknowledgeActivity.this)
							.getStringDataByKey(PrefManager.KEY_DISTRIBUTOR_NAME);

					for (int j = 0; j < innerJsonArray.length(); j++) {
						JSONObject orderDetail = (JSONObject) innerJsonArray.get(j);
						String[] stringArray = new String[9];   //////////////////by
						Log.d("orderDetail", orderDetail.toString());
						serialNoCounter ++;

						stringArray[0] = String.valueOf(serialNoCounter);
						stringArray[1] = getDate(orderheader.getString("orderdt"));
						stringArray[2] = distributor;
						stringArray[3] = beat;
						stringArray[4] = (String) orderheader.getString("mname");
						stringArray[5] = address;
						stringArray[6] = (String) orderDetail.getString("productname");
						stringArray[7] = (String) orderDetail.getString("qty");
						stringArray[8] = getFormattedAmounts(orderDetail.getString("pvalue"));

						Log.d("date..", stringArray[1]);
						Log.d("amount..", stringArray[8]);
						Log.d("stringArray", stringArray.toString());
						mWriter.writeNext(stringArray);
					}
				}else {

					for (int j = 0; j < innerJsonArray.length(); j++) {
						JSONObject orderDetail = (JSONObject) innerJsonArray.get(j);
						String[] stringArray = new String[7];   //////////////////by
						Log.d("orderDetail", orderDetail.toString());
						serialNoCounter ++;

						stringArray[0] = String.valueOf(serialNoCounter);
						stringArray[1] = getDate(orderheader.getString("orderdt"));
						stringArray[2] = (String) orderheader.getString("mname");
						stringArray[3] = address;
						stringArray[4] = (String) orderDetail.getString("productname");
						stringArray[5] = (String) orderDetail.getString("qty");
						stringArray[6] = getFormattedAmounts(orderDetail.getString("pvalue"));

						Log.d("stringArray", stringArray.toString());
						mWriter.writeNext(stringArray);
					}
				}
            }

            mWriter.close();
			curs.close();

			File path = Environment.getExternalStoragePublicDirectory(
					Environment.DIRECTORY_DOWNLOADS);
			File file = new File(path, userId+"_"+dtwithtm+".csv");

			try {
				path.mkdirs();

				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
				showAlertDialogToast("Something wrong : "+ e.getMessage());
				buttonGenerateCSV.setVisibility(View.VISIBLE);
				return;
			}

			showAlertDialogToast("Data exported successfully");
			buttonGenerateCSV.setVisibility(View.VISIBLE);

            textViewUploadedFile.setText(userId + "_" + dtwithtm + ".csv");

            textViewUploadedFile.setVisibility(TextView.VISIBLE);

			csvpath= String.valueOf(file);


		} catch (Exception e) {
            e.printStackTrace();
			showAlertDialogToast("Something wrong : "+ e.getMessage());
			buttonGenerateCSV.setVisibility(View.VISIBLE);
        }

    }

	private void getData() {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog= ProgressDialog.show(SalesmanAcknowledgeActivity.this, "",
						"Loading...", true, true);

			}

			@Override
			protected void onPostExecute(Void result) {

				try {

					PaymentStatus=(JsonAccountObject.getString("pymtstatus"));
					Log.e("PaymentStatus",PaymentStatus);

					if(PaymentStatus.equals("Pending")||PaymentStatus.equals(null) ||PaymentStatus.equals("")||PaymentStatus.equals("null")){
						showAlertDialogToast("Please make payment to place order");
					}else {
						if(moveto.equals("createorder")){
							Intent io = new Intent(SalesmanAcknowledgeActivity.this,
									CreateOrderActivity.class);
							startActivity(io);
							finish();
						}

						else if (moveto.equals("myorder")){
							if (Constants.USER_TYPE.equals("D")) {
								Intent io = new Intent(SalesmanAcknowledgeActivity.this,
										DealersOrderActivity.class);
								startActivity(io);
								finish();
							} else if (Constants.USER_TYPE.equals("M")) {
								Intent io = new Intent(SalesmanAcknowledgeActivity.this,
										MerchantOrderActivity.class);
								Constants.setimage=null;
								startActivity(io);
								finish();
							} else {
								Intent io = new Intent(SalesmanAcknowledgeActivity.this,
										SalesManOrderActivity.class);
								Constants.setimage=null;
								startActivity(io);
								finish();
							}
						}else if(moveto.equals("product")){
							Intent io = new Intent(SalesmanAcknowledgeActivity.this, ProductActivity.class);
							startActivity(io);
							Constants.setimage=null;
							finish();
						}else if(moveto.equals("mydealer")){
							Intent io = new Intent(SalesmanAcknowledgeActivity.this, MyDealersActivity.class);
							io.putExtra("Key","MenuClick");
							Constants.setimage=null;
							startActivity(io);
							finish();
						}
						else if(moveto.equals("profile")){
							Intent io = new Intent(SalesmanAcknowledgeActivity.this, ProfileActivity.class);
							startActivity(io);
							finish();
						}
						else if(moveto.equals("clientvisit")){
							Intent io = new Intent(SalesmanAcknowledgeActivity.this, SMClientVisitHistory.class);
							startActivity(io);
							finish();
						}
						else if(moveto.equals("paymentcoll")){
							Intent io = new Intent(SalesmanAcknowledgeActivity.this, SalesmanPaymentCollectionActivity.class);
							startActivity(io);
                          		finish();
						}else if(moveto.equals("payment")){
                            Intent io = new Intent(SalesmanAcknowledgeActivity.this, PaymentActivity.class);
                            startActivity(io);
                            finish();
                        }else if(moveto.equals("complaint")){
							Intent io = new Intent(SalesmanAcknowledgeActivity.this, MerchantComplaintActivity.class);
							startActivity(io);
							finish();
						}
						else if(moveto.equals("addmerchant")){
							Intent io = new Intent(SalesmanAcknowledgeActivity.this, AddMerchantNew.class);
							startActivity(io);
							finish();
						}
						else if(moveto.equals("stockentry")){
							Intent io = new Intent(SalesmanAcknowledgeActivity.this, OutletStockEntry.class);
							startActivity(io);
							finish();
						}
						else if(moveto.equals("surveyuser")){
							Intent to = new Intent(SalesmanAcknowledgeActivity.this,
									SurveyMerchant.class);
							startActivity(to);
							finish();
						}
						else if(moveto.equals("downloadscheme")){
							Intent to = new Intent(SalesmanAcknowledgeActivity.this,
									SchemeDownload.class);
							startActivity(to);
							finish();
						}
						else if(moveto.equals("mydayplan")){
							Intent to = new Intent(SalesmanAcknowledgeActivity.this,
									MyDayPlan.class);
							startActivity(to);
							finish();
						}
						else if(moveto.equals("mysales")){
							Intent to = new Intent(SalesmanAcknowledgeActivity.this,
									MySalesActivity.class);
							startActivity(to);
							finish();
						} else if (moveto.equals("diststock")) {
							Intent io = new Intent(SalesmanAcknowledgeActivity.this,
									DistributrStockActivity.class);
							startActivity(io);
							finish();
						}
					}

					dialog.dismiss();

				}catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
				}

			}

			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();


				JsonServiceHandler = new JsonServiceHandler(Utils.strGetProfileDetail+Constants.USER_ID, SalesmanAcknowledgeActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceDataGet();
				return null;
			}
		}.execute(new Void[]{});
	}

	private void showCurrentDateOnView() {
		// TODO Auto-generated method stub


		final Calendar c = Calendar.getInstance();
		yearnew = c.get(Calendar.YEAR);
		newyear= c.get(Calendar.YEAR);
		newmnth = c.get(Calendar.MONTH);
		monthnew=c.get(Calendar.MONTH);
		daynew = c.get(Calendar.DAY_OF_MONTH);
		newday=c.get(Calendar.DAY_OF_MONTH);
		hr= c.get(Calendar.HOUR_OF_DAY);
		mins= c.get(Calendar.MINUTE);
		sec= c.get(Calendar.SECOND);

		String monthLength =String.valueOf(newmnth+1);
		if(monthLength.length()==1){
			monthLength = "0"+monthLength;
		}
		String dayLength =String.valueOf(daynew);
		if(dayLength.length()==1){
			dayLength = "0"+dayLength;
		}

		Log.e("monthLength",monthLength);

		Log.e("dayLength", dayLength);


		 datebuilder=(new StringBuilder().append(yearnew).append("-").append(monthLength).append("-").append(dayLength));

		 Time today = new Time(Time.getCurrentTimezone());

		today.setToNow();

		String newtime=today.format("%k:%M:%S");



		Log.e("date", String.valueOf(datebuilder));
		Log.e("time", String.valueOf(newtime));

		String date=  datebuilder.toString();
		String time=  newtime.toString();

		String inputPattern = "yyyy-MM-dd";
		String outputPattern = "dd-MM-yyyy ";
		SimpleDateFormat inputFormat = new SimpleDateFormat(
				inputPattern);
		SimpleDateFormat outputFormat = new SimpleDateFormat(
				outputPattern);

		String str = null;
		try {
			dateStr = inputFormat.parse(date);
			str = outputFormat.format(dateStr);


		} catch (Exception  e) {
			e.printStackTrace();
		}
		todate=date;
		totime=time;


	}
	private void getachnowledgedetail() {
		// TODO Auto-generated method stub
		
	
	new AsyncTask<Void, Void, Void>() {
		ProgressDialog dialog;
		String strStatus = "";
		String strMsg = "";

		@Override
		protected void onPreExecute() {
			dialog= ProgressDialog.show(SalesmanAcknowledgeActivity.this, "",
					"Loading...", true, true);

		}

		@Override
		protected void onPostExecute(Void result) {

			try {

				strStatus = JsonAccountObject.getString("status");
				Log.e("return status", strStatus);
				strMsg = JsonAccountObject.getString("message");
				Log.e("return message", strMsg);

				if (strStatus.equals("true")) {
					buttonShare.setVisibility(View.VISIBLE);

				}else{
					buttonShare.setVisibility(View.GONE);

				}
				dialog.dismiss();
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (Exception e) {

			}

		}

		@Override
		protected Void doInBackground(Void... params) {

			JSONObject jsonObject = new JSONObject();
			try {

				jsonObject.put("suserid", Constants.USER_ID);
				jsonObject.put("usertype", Constants.USER_TYPE);
				jsonObject.put("eoddt", datebuilder);
			
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			

			JsonServiceHandler = new JsonServiceHandler(Utils.stracknowledge,jsonObject.toString(), SalesmanAcknowledgeActivity.this);
			JsonAccountObject = JsonServiceHandler.ServiceData();
			return null;
		}
	}.execute(new Void[] {});
}

protected void toastDisplay(String msg) {
	Toast toast = Toast.makeText(SalesmanAcknowledgeActivity.this, msg,
			Toast.LENGTH_SHORT);
	toast.setGravity(Gravity.CENTER, 0, 0);
	toast.show();

}
public boolean netCheck() {
	// for network connection
	try {
		ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo mWifi = connManager
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

		ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo mNetwork = connectionManager
				.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

		Object result = null;
		if (mWifi.isConnected() || mNetwork.isConnected()) {
			return true;
		}

		else if (result == null) {
			showAlertDialog(SalesmanAcknowledgeActivity.this,
					"No Internet Connection",
					"Please Check Your internet connection.", false);
			return false;
		}
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return false;
}

	public boolean netCheckWithoutAlert() {
		// for network connection
		try {
			ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetwork = connectionManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			Object result = null;
			if (mWifi.isConnected() || mNetwork.isConnected()) {
				return true;
			}

			else if (result == null) {

				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
public void showAlertDialog(Context context, String title, String message,
		Boolean status) {
	AlertDialog alertDialog = new AlertDialog.Builder(context).create();
	alertDialog.setTitle(title);
	alertDialog.setMessage(message);
	alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
	alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
			new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
				}
			});
	alertDialog.show();
}

	public void showAlertDialogToast( String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();

		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

	}


	public void onBackPressed() {
		exitAlret();
	}

	private void exitAlret() {
		AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
		localBuilder.setCancelable(false);
		localBuilder.setMessage("Do you want to Exit?");
		localBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {

						finish();

					}
				});
		localBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {
						paramDialogInterface.cancel();
					}
				});
		localBuilder.create().show();
	}

	private String getFormattedAmount(String amounts){
		if(amounts != null && !amounts.isEmpty()) {
			int amount = Integer.parseInt(amounts);
			return NumberFormat.getNumberInstance(Locale.US).format(amount);
		}
		return "";
	}

	private String getFormattedAmt(String amounts){

		if(amounts != null && !amounts.isEmpty()) {
			Format format = NumberFormat.getCurrencyInstance(new Locale("en", "in"));
			String str =  (format.format(new BigDecimal(amounts)));
			return  str.replaceAll("\\s+","");
		}
		return "";

	}

    private String getFormattedAmounts(String amounts){

        try{
			if(amounts == null || amounts.isEmpty()) {
				return "";
			}
			NumberFormat nf = NumberFormat.getCurrencyInstance(new Locale("en", "in"));
			DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat) nf).getDecimalFormatSymbols();
			decimalFormatSymbols.setCurrencySymbol("Rs.");
			((DecimalFormat) nf).setDecimalFormatSymbols(decimalFormatSymbols);
			String str = (nf.format(new BigDecimal(amounts)).trim());
			return  str.replaceAll("\\s+","");

		}catch (Exception e){
        	e.printStackTrace();
        	return "";
		}
    }

	private String dateStringToString(String date){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
		try{
			Date newDate = getDateFromApp(date);
			if(newDate != null){
				sdf.applyPattern("yyyy-MM-dd hh:mm aa");
				return sdf.format(newDate);
			}else {
				return "";
			}
		}catch (Exception e){
			return "";
		}
	}

	private Date getDateFromApp(String date){
		SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
		try {
			if (date != null && !date.isEmpty()) {
				return input.parse(date);
			}
		}catch (ParseException e){
			e.printStackTrace();
			return null;
		}
		return null;
	}

	private String getDate(String date){
		SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
		try {
			if (date != null && !date.isEmpty()) {
				Date outDate =  input.parse(date);
				if(outDate != null){
					sdf.applyPattern("yyyy-MM-dd");
					return sdf.format(outDate);
				}else {
					return "";
				}
			}
		}catch (ParseException e){
			e.printStackTrace();
			return "";
		}
		return "";
	}



}
