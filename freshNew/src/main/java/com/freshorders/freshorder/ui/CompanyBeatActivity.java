package com.freshorders.freshorder.ui;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.activity.DistanceCalActivity;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.MerchantOrderDetailDomain;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Pavithra on 3/3/2017.
 */

public class CompanyBeatActivity extends Activity {
    Button buttonview;
    public static JSONObject JsonAccountObject = null;
    public static JSONArray JsonAccountArray = null;
    public static DatabaseHandler databaseHandler;
    public static JsonServiceHandler JsonServiceHandler ;
    ArrayList<String> arraylistDistributor;
    ArrayList<String> arraylistBeat;
   public  AutoCompleteTextView AutoCompleteDistributor,autocompleteBeat;
    String userid;

    //menu objects

    TextView menuIcon, textViewAssetMenuProfile, textViewAssetMenuMyOrders,
            textViewAssetMenuMyDealers, textViewAssetMenuProducts,
            textViewAssetMenuPayment, textViewAssetMenuComplaint,textViewAssetMenuDistanceCalculation,
            textViewAssetMenuSignout,textViewAssetMenuClientVisit,textViewAssetMenuPaymentCollection,
            textViewNodata,textViewAssetMenuCreateOrder,textViewcross_1,textViewAssetMenuRefresh
            ,textViewAssetMenuStockEntry,textViewAssetMenuAddMerchant,textViewAssetMenuAcknowledge
            ,textViewAssetMenusurveyuser,textViewAssetMenudownloadscheme,textViewcross_2,textViewAssetMenuMyDayPlan,textViewAssetMenuMySales,
            textViewAssetMenuDistStock;

    public static int menuCliccked;

    LinearLayout linearLayoutMenuParent, linearLayoutProfile,linearLayoutDistanceCalculation,
            linearLayoutMyOrders, linearLayoutMyDealers, linearLayoutProducts,
            linearLayoutPayment,linearLayoutPaymentCollection, linearLayoutComplaint, linearLayoutSignout,
            linearlayoutSearchIcon,linearLayoutCreateOrder,linearLayoutClientVisit,linearLayoutRefresh,
            linearLayoutAcknowledge,linearLayoutStockEntry,linearLayoutAddMerchant,
            linearLayoutsurveyuser,linearLayoutdownloadscheme,linearLayoutMyDayPlan,linearLayoutMySales,
            linearLayoutDistStock;

    String PaymentStatus="null",selectedDate="",selectedbeatname="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.company_beat_activity);

        databaseHandler = new DatabaseHandler(getApplicationContext());

        Cursor curs;
        curs = databaseHandler.getDetails();
        curs.moveToFirst();
        Log.e("Insertion Check",
                curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_username)));
        Log.e("Insertion Check",
                curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_password)));
        Log.e("KEY_paymentStatus",
                curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_paymentStatus)));
        PaymentStatus =curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_paymentStatus));
        Constants.USER_TYPE=curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_usertype));
        userid =  curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_id));

        Constants.USER_TYPE="S";

        buttonview = (Button) findViewById(R.id.buttonview);
        AutoCompleteDistributor =(AutoCompleteTextView) findViewById(R.id.AutoCompleteDistributor);
        autocompleteBeat =(AutoCompleteTextView) findViewById(R.id.autocompleteBeat);
        textViewcross_1=(TextView) findViewById(R.id.textViewcross_1);
        textViewcross_2=(TextView) findViewById(R.id.textViewcross_2);
        linearLayoutRefresh= (LinearLayout) findViewById(R.id.linearLayoutRefresh);


        linearLayoutMyDealers = (LinearLayout) findViewById(R.id.linearLayoutMyDealers);
        linearLayoutProducts = (LinearLayout) findViewById(R.id.linearLayoutProducts);
        linearLayoutPayment = (LinearLayout) findViewById(R.id.linearLayoutPayment);

        linearLayoutComplaint = (LinearLayout) findViewById(R.id.linearLayoutComplaint);
        linearLayoutSignout = (LinearLayout) findViewById(R.id.linearLayoutSignout);
        linearLayoutClientVisit= (LinearLayout) findViewById(R.id.linearLayoutClientVisit);
        linearLayoutPaymentCollection= (LinearLayout) findViewById(R.id.linearLayoutPaymentCollection);
        linearLayoutCreateOrder = (LinearLayout) findViewById(R.id.linearLayoutCreateOrder);
        linearLayoutProfile = (LinearLayout) findViewById(R.id.linearLayoutProfile);
        linearLayoutMyOrders = (LinearLayout) findViewById(R.id.linearLayoutMyOrders);
        linearLayoutAcknowledge= (LinearLayout) findViewById(R.id.linearLayoutAcknowledge);
        linearLayoutStockEntry= (LinearLayout) findViewById(R.id.linearLayoutStockEntry);
        linearLayoutAddMerchant = (LinearLayout) findViewById(R.id.linearLayoutAddMerchant);
        linearLayoutdownloadscheme= (LinearLayout) findViewById(R.id.linearLayoutdownloadscheme);
        linearLayoutsurveyuser= (LinearLayout) findViewById(R.id.linearLayoutsurveyuser);
        linearLayoutMyDayPlan = (LinearLayout) findViewById(R.id.linearLayoutMyDayPlan);
        linearLayoutMySales = (LinearLayout) findViewById(R.id.linearLayoutMySales);
        linearLayoutDistanceCalculation= (LinearLayout) findViewById(R.id.linearLayoutDistanceCalculation);
        menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);

        textViewAssetMenuMyDealers = (TextView) findViewById(R.id.textViewAssetMenuMyDealers);
        textViewAssetMenuProducts = (TextView) findViewById(R.id.textViewAssetMenuProducts);
        textViewAssetMenuPayment = (TextView) findViewById(R.id.textViewAssetMenuPayment);

        textViewAssetMenuComplaint = (TextView) findViewById(R.id.textViewAssetMenuComplaint);
        textViewAssetMenuSignout = (TextView) findViewById(R.id.textViewAssetMenuSignout);
        textViewAssetMenuClientVisit = (TextView) findViewById(R.id.textViewAssetMenuClientVisit);
        textViewAssetMenuAddMerchant = (TextView) findViewById(R.id.textViewAssetMenuAddMerchant);
        textViewAssetMenuAcknowledge = (TextView) findViewById(R.id.textViewAssetMenuAcknowledge);
        textViewAssetMenuStockEntry = (TextView) findViewById(R.id.textViewAssetMenuStockEntry);
        textViewAssetMenudownloadscheme= (TextView) findViewById(R.id.textViewAssetMenudownloadscheme);
        textViewAssetMenusurveyuser= (TextView) findViewById(R.id.textViewAssetMenusurveyuser);
        textViewAssetMenuPaymentCollection= (TextView) findViewById(R.id.textViewAssetMenuPaymentCollection);
        textViewAssetMenuRefresh = (TextView) findViewById(R.id.textViewAssetMenuRefresh);
        textViewAssetMenuCreateOrder= (TextView) findViewById(R.id.textViewAssetMenuCreateOrder);
        textViewAssetMenuProfile = (TextView) findViewById(R.id.textViewAssetMenuProfile);
        textViewAssetMenuMyOrders = (TextView) findViewById(R.id.textViewAssetMenuMyOrders);
        textViewAssetMenuMyDayPlan= (TextView) findViewById(R.id.textViewAssetMenuMyDayPlan);
        textViewAssetMenuMySales= (TextView) findViewById(R.id.textViewAssetMenuMySales);
        textViewAssetMenuDistanceCalculation= (TextView) findViewById(R.id.textViewAssetMenuDistanceCalculation);

        linearLayoutMenuParent = (LinearLayout) findViewById(R.id.linearLayoutMenuParent);
        Typeface font = Typeface.createFromAsset(getAssets(),
                "fontawesome-webfont.ttf");
        menuIcon.setTypeface(font);
        textViewAssetMenuCreateOrder.setTypeface(font);
        textViewAssetMenuProfile.setTypeface(font);
        textViewAssetMenuMyOrders.setTypeface(font);
        textViewAssetMenuMyDealers.setTypeface(font);
        textViewAssetMenuProducts.setTypeface(font);
        textViewAssetMenuPayment.setTypeface(font);
        textViewAssetMenuComplaint.setTypeface(font);
        textViewAssetMenuSignout.setTypeface(font);
        textViewAssetMenuClientVisit.setTypeface(font);
        textViewAssetMenuPaymentCollection.setTypeface(font);
        textViewAssetMenuRefresh.setTypeface(font);
        textViewAssetMenuDistanceCalculation.setTypeface(font);
        textViewAssetMenuAcknowledge.setTypeface(font);
        textViewAssetMenuStockEntry.setTypeface(font);
        textViewAssetMenuAddMerchant.setTypeface(font);
        textViewAssetMenusurveyuser.setTypeface(font);
        textViewAssetMenudownloadscheme.setTypeface(font);
        textViewcross_1.setTypeface(font);
        textViewcross_2.setTypeface(font);
        textViewAssetMenuMyDayPlan.setTypeface(font);
        textViewAssetMenuMySales.setTypeface(font);

        linearLayoutDistStock = (LinearLayout) findViewById(R.id.linearLayoutDistStock);
        textViewAssetMenuDistStock = (TextView) findViewById(R.id.textViewAssetMenuDistStock);
        textViewAssetMenuDistStock.setTypeface(font);

        databaseHandler = new DatabaseHandler(getApplicationContext());
        Cursor cur;
        cur = databaseHandler.getDetails();
        cur.moveToFirst();
        userid =  cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_id));

        getDistributor();


        buttonview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              String distrbutorname =  AutoCompleteDistributor.getText().toString();
                if(!distrbutorname.equals("")){
                    if(!selectedbeatname.equals("")){
                        //by Kumaravel 13-07-2019
                        new PrefManager(CompanyBeatActivity.this)
                                .setStringDataByKey(PrefManager.KEY_DISTRIBUTOR_NAME, distrbutorname);
                        getDistProducts();
                    }else{
                        showAlertDialogToast("Please Select Beat");
                    }
                }else{
                    showAlertDialogToast("Please Select Distributor");
                }



            }
        });
        autocompleteBeat.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(AutoCompleteDistributor.getText().toString().equals("")){
                       showAlertDialogToast("Please select distributor before selecting beat");
                }else{
                    Log.e("Selected Distributor",AutoCompleteDistributor.getText().toString());

                    // Added by SK for pushing distributorname
                    Constants.distributorname = AutoCompleteDistributor.getText().toString();
                    // End of SK Changes


                    autocompleteBeat.showDropDown();
                    textViewcross_2.setVisibility(View.GONE);

                    textViewcross_2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            AutoCompleteDistributor.setText("");

                            if (AutoCompleteDistributor.getText().equals("")) {
                                InputMethodManager iaa = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                iaa.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                            }

                        }
                    });

                }


                return false;
            }
        });


        AutoCompleteDistributor.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.e("size", String.valueOf(arraylistDistributor.size()));
                System.out.println( arraylistDistributor);

                if(arraylistDistributor.size()!=0){
                    AutoCompleteDistributor.showDropDown();
                    textViewcross_1.setVisibility(View.GONE);

                    textViewcross_1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            AutoCompleteDistributor.setText("");

                            if (AutoCompleteDistributor.getText().equals("")) {
                                InputMethodManager iaa = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                iaa.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                            }

                        }
                    });
                }else{
                    showAlertDialogToast("No Distributor");
                }

                return false;
            }
        });


        autocompleteBeat
                .setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {

                         selectedbeatname = autocompleteBeat.getText().toString();
            Log.e("Selceted beat",selectedbeatname);

                    }
                });


        AutoCompleteDistributor
                .setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {

                        String selectedDistributor = AutoCompleteDistributor.getText().toString();
                        Log.e("Selceted distributor",selectedDistributor);
                        getBeat(selectedDistributor);
                    }
                });
        menuIcon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (menuCliccked == 0) {
                    linearLayoutMenuParent.setVisibility(View.VISIBLE);
                    InputMethodManager iss = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    iss.hideSoftInputFromWindow(menuIcon.getWindowToken(), 0);
                    menuCliccked = 1;
                } else {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    InputMethodManager iss = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    iss.hideSoftInputFromWindow(menuIcon.getWindowToken(), 0);
                    menuCliccked = 0;
                }

            }
        });


        linearLayoutMenuParent.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);

                menuCliccked = 0;
            }
        });

        linearLayoutCreateOrder.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
            }
        });

        linearLayoutProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub

                if(netCheckwithoutAlert()==true){
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Constants.Merchantname="";
                    Constants.SalesMerchant_Id="";
                    getDetails("profile");
                }else{
                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });
        linearLayoutDistanceCalculation.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub


                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Constants.Merchantname="";
                    Constants.SalesMerchant_Id="";
                   Intent into =new Intent(CompanyBeatActivity.this, DistanceCalActivity.class);
                    startActivity(into);


            }
        });
        linearLayoutMyOrders.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(netCheckwithoutAlert()==true) {
                    Constants.orderstatus = "Success";
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Constants.Merchantname="";
                    Constants.SalesMerchant_Id="";
                    getDetails("myorders");
                }else{
                    if(PaymentStatus.equals("Pending")||PaymentStatus.equals(null) ||PaymentStatus.equals("")||PaymentStatus.equals("null")){
                        showAlertDialogToast("Please make payment before place order");
                    }else{
                        Intent io = new Intent(CompanyBeatActivity.this,
                                SalesManOrderActivity.class);
                        startActivity(io);
                        finish();
                    }
                }


            }
        });

        linearLayoutRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("Clicked", "Clicked");

                if(netCheckwithoutAlert()){

                    final Dialog qtyDialog = new Dialog(CompanyBeatActivity.this);
                    qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    qtyDialog.setContentView(R.layout.warning_dialog);

                    final Button buttonUpdateOk, buttonUpdateCancel;

                    final TextView textQtyValidate;

                    buttonUpdateOk = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateOk);
                    buttonUpdateCancel = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateCancel);

                    textQtyValidate = (TextView) qtyDialog
                            .findViewById(R.id.textQtyValidate);
                    qtyDialog.show();


                    buttonUpdateOk.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            databaseHandler.synTableDelete();
                            Constants.refreshscreen="createorder";
                            Intent io = new Intent(CompanyBeatActivity.this, RefreshActivity.class);
                            startActivity(io);
                            finish();
                            qtyDialog.dismiss();
                        }

                    });

                    buttonUpdateCancel
                            .setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub

                                    qtyDialog.dismiss();

                                }
                            });

                }else{
                    showAlertDialogToast("Please Check Your internet connection");
                }
            }

        });


        linearLayoutMyDealers.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(netCheckwithoutAlert()==true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Constants.Merchantname="";
                    Constants.SalesMerchant_Id="";
                    getDetails("mydealer");
                }else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(CompanyBeatActivity.this,
                                MyDealersActivity.class);
                        io.putExtra("Key","menuclick");
                        startActivity(io);
                        finish();

                    }
                }

            }
        });




        linearLayoutComplaint.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheckwithoutAlert() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Constants.Merchantname="";
                    Constants.SalesMerchant_Id="";
                    getDetails("postnotes");
                }else {
                    showAlertDialogToast("Please Check Your internet connection");
                }
            }
        });
        linearLayoutClientVisit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheckwithoutAlert() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Constants.Merchantname="";
                    Constants.SalesMerchant_Id="";

                    // TODO Auto-generated method stub
                    getDetails("clientvisit");
                }else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    }else{
                        Intent to = new Intent(CompanyBeatActivity.this,
                                SMClientVisitHistory.class);
                        startActivity(to);
                        finish();
                    }
                }

            }
        });

        linearLayoutPaymentCollection.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheckwithoutAlert() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Constants.Merchantname="";
                    Constants.SalesMerchant_Id="";
                    getDetails("paymentcoll");
                }else {
                    showAlertDialogToast("Please Check Your internet connection");
                }


            }
        });

        linearLayoutAcknowledge.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheckwithoutAlert() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Constants.Merchantname="";
                    Constants.SalesMerchant_Id="";
                    getDetails("acknowledge");
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }


            }
        });
        linearLayoutStockEntry.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheckwithoutAlert() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Constants.Merchantname="";
                    Constants.SalesMerchant_Id="";
                    getDetails("stockentry");
                }else {
                    showAlertDialogToast("Please Check Your internet connection");
                }


            }
        });

        linearLayoutDistStock.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //if (netCheckwithoutAlert() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Constants.Merchantname="";
                    Constants.SalesMerchant_Id="";
                    getDetails("diststock");
               // }else {
               //     showAlertDialogToast("Please Check Your internet connection");
               // }


            }
        });

        linearLayoutAddMerchant.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(netCheckwithoutAlert()==true) {
                    Constants.orderstatus = "Success";
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Constants.Merchantname="";
                    Constants.SalesMerchant_Id="";
                    getDetails("addmerchant");
                }else{
                    if(PaymentStatus.equals("Pending")||PaymentStatus.equals(null) ||PaymentStatus.equals("")||PaymentStatus.equals("null")){
                        showAlertDialogToast("Please make payment before place order");
                    }else{
                        Intent io = new Intent(CompanyBeatActivity.this,
                                AddMerchantNew.class);
                        startActivity(io);
                        finish();
                    }
                }


            }
        });
        linearLayoutdownloadscheme.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(netCheckwithoutAlert()){

                    final Dialog qtyDialog = new Dialog(CompanyBeatActivity.this);
                    qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    qtyDialog.setContentView(R.layout.warning_dialog);

                    final Button buttonUpdateOk, buttonUpdateCancel;

                    final TextView textQtyValidate;

                    buttonUpdateOk = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateOk);
                    buttonUpdateCancel = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateCancel);

                    textQtyValidate = (TextView) qtyDialog
                            .findViewById(R.id.textQtyValidate);
                    qtyDialog.show();


                    buttonUpdateOk.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            databaseHandler.deletescheme();
                            Constants.downloadScheme = "createorder";
                            Intent io = new Intent(CompanyBeatActivity.this, SchemeDownload.class);
                            startActivity(io);
                            finish();
                            qtyDialog.dismiss();
                        }

                    });

                    buttonUpdateCancel
                            .setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub

                                    qtyDialog.dismiss();

                                }
                            });

                }else{
                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });
        linearLayoutsurveyuser.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //if (netCheckwithoutAlert() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    getDetails("surveyuser");
                //}else {
                //    showAlertDialogToast("Please Check Your internet connection");
                //}

              /*  Intent to = new Intent(SalesManOrderActivity.this,
                        SalesmanAcknowledgeActivity.class);
                startActivity(to);
                finish();*/

            }
        });

        linearLayoutMyDayPlan.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheckwithoutAlert() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    getDetails("mydayplan");
                }else {
                    showAlertDialogToast("Please Check Your internet connection");
                }
            }
        });

        linearLayoutMySales.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheckwithoutAlert() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    getDetails("mysales");
                }else {
                    showAlertDialogToast("Please Check Your internet connection");
                }
            }
        });

        linearLayoutSignout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                // TODO Auto-generated method stub
                Constants.Merchantname="";
                Constants.SalesMerchant_Id="";
                databaseHandler.delete();
                Intent io = new Intent(CompanyBeatActivity.this,
                        SigninActivity.class);
                startActivity(io);
                finish();
            }
        });
    }

    public void getDistProducts()
    {
        new AsyncTask<Void, Void, Void>() {
            String strStatus = "", strMsg;
            ProgressDialog dialog;

            @Override
            protected void onPreExecute() {
                dialog= ProgressDialog.show(CompanyBeatActivity.this, "",
                        "Distributor Stock Loading...", true, true);
            }
            @Override
            protected void onPostExecute(Void result) {
                try {
                    Log.e("onPostExecute","dist products");
                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("returnmessage", strMsg);
                    if(strStatus.equalsIgnoreCase("false")){
                        //textView2.setText("No Products available");
                    }
                    else {
                        databaseHandler.insertStockEntry(JsonAccountObject.toString());
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getBeatMerchant(selectedbeatname);
                if(dialog.isShowing()){
                    dialog.dismiss();
                }
            }


            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("distributorname", Constants.distributorname);

                    Log.e("Products list",jsonObject.toString());

                    JsonServiceHandler = new JsonServiceHandler(Utils.strDistriProd, jsonObject.toString(), CompanyBeatActivity.this);
                    JsonAccountObject = JsonServiceHandler.ServiceData();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute(new Void[]{});
    }

    public void getBeatMerchant(String beatname){
        Cursor cursor;
        cursor = databaseHandler.getbeatMerchant(beatname);
        Log.e("beatname",beatname);
        Log.e("beatname count", String.valueOf(cursor.getCount()));
        String merchants="";
        int i=0;
        if(cursor!=null && cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {

                do{
                    if(i==0){
                        if(cursor.getString(0).equals("New User")){
                            merchants = "'"+cursor.getString(0)+"'";
                        }else{
                            merchants = cursor.getString(0);
                        }

                        i++;
                    }else{
                        if(cursor.getString(0).equals("New User")){
                            merchants = merchants+", '"+cursor.getString(0)+"'";
                        }else{
                            merchants = merchants+","+cursor.getString(0);
                        }
                    }

                    Log.e("merchants",merchants);
                } while (cursor.moveToNext());


            }
        }

        offlinemerchant(merchants);
    }
    public void offlinemerchant(String beatMerchants){


        Log.e("string merchant",beatMerchants);
        Cursor curs;
        curs = databaseHandler.getmerchantoncurrentday(beatMerchants);
        Log.e("merchantCount", String.valueOf(curs.getCount()));

if(curs.getCount()!=0){
    selectedDate = new SimpleDateFormat(
            "yyyy-MM-dd").format(new Date());
    databaseHandler.updateSelectedBeat(selectedbeatname,userid,selectedDate);
    Cursor curs1;
    curs1 = databaseHandler.getDetails();
    curs1.moveToFirst();
    String updatedbeat =  curs1.getString(curs1.getColumnIndex(DatabaseHandler.KEY_selectedBeat));
    Log.e("updated beat",updatedbeat);
    String updatedDate =  curs1.getString(curs1.getColumnIndex(DatabaseHandler.KEY_selectedDate));
    Log.e("updated Date",updatedDate);
moveto();
}else{
    showAlertDialogToast("No merchants available");
}


    }

public void moveto(){
    if(Constants.beatassignscreen.equals("OutletEntry")){
        Intent to = new Intent(CompanyBeatActivity.this,
                OutletStockEntry.class);
        startActivity(to);
        finish();

    }else if(Constants.beatassignscreen.equals("AddMerchant")){
        Intent to = new Intent(CompanyBeatActivity.this,
                AddMerchantNew.class);
        startActivity(to);
        finish();
    }else if(Constants.beatassignscreen.equals("ClientVisit")){
        Intent to = new Intent(CompanyBeatActivity.this,
                ClientVisitActivity.class);
        startActivity(to);
        finish();
    }else if(Constants.beatassignscreen.equals("SurveyMerchant")){
        Intent to = new Intent(CompanyBeatActivity.this,
                SurveyMerchant.class);
        startActivity(to);
        finish();
    }else if(Constants.beatassignscreen.equals("PaymentCollection")){
        Intent to = new Intent(CompanyBeatActivity.this,
                SalesmanPaymentCollectionActivity.class);
        startActivity(to);
        finish();
    }else if(Constants.beatassignscreen.equals("CreateOrder")){
        Intent to = new Intent(CompanyBeatActivity.this,
                CreateOrderActivity.class);
        startActivity(to);
        finish();
    }else if(Constants.beatassignscreen.equals("DistStock")){
        Intent to = new Intent(CompanyBeatActivity.this,
                DistributrStockActivity.class);
        startActivity(to);
        finish();
    }else{
        Intent to = new Intent(CompanyBeatActivity.this,
                CreateOrderActivity.class);
        startActivity(to);
        finish();
    }
}
    private void getDetails(final String paymentcheck) {
        if(paymentcheck.equals("profile")){
            Intent io = new Intent(CompanyBeatActivity.this,
                    ProfileActivity.class);
            startActivity(io);
            finish();
        }
        else if(paymentcheck.equals("myorders")){
            Intent io = new Intent(CompanyBeatActivity.this,
                    SalesManOrderActivity.class);
            startActivity(io);
            finish();

        }else if(paymentcheck.equals("mydealer")){
            Intent io = new Intent(CompanyBeatActivity.this,
                    MyDealersActivity.class);
            io.putExtra("Key","menuclick");
            startActivity(io);
            finish();

        }else if(paymentcheck.equals("postnotes")){
            Intent io = new Intent(CompanyBeatActivity.this,
                    MerchantComplaintActivity.class);
            startActivity(io);
            finish();

        } else if(paymentcheck.equals("placeorder")){
            Constants.imagesetcheckout=1;
            Intent to = new Intent(CompanyBeatActivity.this,
                    SalesManOrderCheckoutActivity.class);
            startActivity(to);
            finish();
        }
        else if(paymentcheck.equals("clientvisit")){
            Intent to = new Intent(CompanyBeatActivity.this,
                    SMClientVisitHistory.class);
            startActivity(to);
            finish();
        }
        else if(paymentcheck.equals("paymentcoll")){
            Intent to = new Intent(CompanyBeatActivity.this,
                    SalesmanPaymentCollectionActivity.class);
            startActivity(to);
            finish();
        } else if(paymentcheck.equals("addmerchant")){
            Intent to = new Intent(CompanyBeatActivity.this,
                    AddMerchantNew.class);
            startActivity(to);
            finish();
        }
        else if(paymentcheck.equals("acknowledge")){
            Intent to = new Intent(CompanyBeatActivity.this,
                    SalesmanAcknowledgeActivity.class);
            startActivity(to);
            finish();
        }
        else if(paymentcheck.equals("stockentry")){
            Intent to = new Intent(CompanyBeatActivity.this,
                    OutletStockEntry.class);
            startActivity(to);
            finish();
        }  else if(paymentcheck.equals("surveyuser")){
            Intent to = new Intent(CompanyBeatActivity.this,
                    SurveyMerchant.class);
            startActivity(to);
            finish();
        }
        else if(paymentcheck.equals("mydayplan")){
            Intent to = new Intent(CompanyBeatActivity.this,
                    MyDayPlan.class);
            startActivity(to);
            finish();
        }
        else if(paymentcheck.equals("mysales")){
            Intent to = new Intent(CompanyBeatActivity.this,
                    MySalesActivity.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("diststock")) {
            Intent io = new Intent(CompanyBeatActivity.this,
                    DistributrStockActivity.class);
            startActivity(io);
            finish();
        }
    }

    private void getDistributor() {
        // TODO Auto-generated method stub


        Cursor curs;
        curs = databaseHandler.getDistributor();
        Log.e("merchantCount", String.valueOf(curs.getCount()));
        arraylistDistributor = new ArrayList<String>();

        if(curs!=null && curs.getCount() > 0) {
            if (curs.moveToFirst()) {

                do{
                    Log.e("inside", "inside");
                    arraylistDistributor.add( curs.getString(0));


                } while (curs.moveToNext());


            }
            Log.e("outside", "outside");
        }
     Log.e("Distributor count",String.valueOf(arraylistDistributor.size()));
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                CompanyBeatActivity.this, R.layout.textview, R.id.textView,
                arraylistDistributor);

        AutoCompleteDistributor.setAdapter(adapter);

    }

    private void getBeat(String distributorname) {
        // TODO Auto-generated method stub


        Cursor curs;
        curs = databaseHandler.getDistributorBeat(distributorname);
        Log.e("merchantCount", String.valueOf(curs.getCount()));
        arraylistBeat = new ArrayList<String>();

        if(curs!=null && curs.getCount() > 0) {
            if (curs.moveToFirst()) {

                do{
                    Log.e("inside", "inside");
                    arraylistBeat.add( curs.getString(0));


                } while (curs.moveToNext());


            }
            Log.e("outside", "outside");
        }
        Log.e("Distributor count",String.valueOf(arraylistBeat.size()));
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                CompanyBeatActivity.this, R.layout.textview, R.id.textView,
                arraylistBeat);

        autocompleteBeat.setAdapter(adapter);

    }

    public boolean netCheck() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            }

            else if (result == null) {
                showAlertDialogToast(
                        "No Internet Connection, Please Check Your internet connection.");
                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
    public void showAlertDialogToast(String message) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setMessage(message);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert = builder.create();
        alert.show();

        Button buttonbackground = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    public void onBackPressed() {
        exitAlret();
    }

    private void exitAlret() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        localBuilder.setMessage("Do you want to Exit?");
        localBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {

                        finish();

                    }
                });
        localBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {
                        paramDialogInterface.cancel();
                    }
                });
        localBuilder.create().show();
    }
    public boolean netCheckwithoutAlert() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            }

            else if (result == null) {

                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    }

