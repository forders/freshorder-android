package com.freshorders.freshorder.ui;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.CameraGridAdapter;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.UploadFileToServer;
import com.freshorders.freshorder.utils.Utils;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CameraPhotoCapture extends Activity {

    final static int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1;
    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    JSONObject JsonAccountObject = null;
    JSONArray JsonAccountArray = null;
    JsonServiceHandler JsonServiceHandler;
    public  int i=0;
    Uri imageUri                      = null;
    static TextView imageDetails      = null;
    public  static ImageView imageView1  = null,imageView2 =null,imageView3=null,imageView4=null,imageView5=null,imageView6=null,
            imageView7=null,imageView8=null,imageView9=null,imageView10=null,imageView11=null,imageView12=null;
    CameraPhotoCapture CameraActivity = null;
    Button confirmOrder;
    Bitmap mBitmap1=null ,mBitmap2=null,mBitmap3=null,mBitmap4=null,mBitmap5=null,mBitmap6=null,mBitmap7=null,mBitmap8=null,mBitmap9=null,mBitmap10=null
            ,mBitmap11=null,mBitmap12=null;
    public  static String CapturedImageDetails,photoPath;
    public  static  String imageId;
    public  static String Path="",path1="",path2="",path3="",path4="",path5="",path6="",path7="",path8="",path9=""
            ,path10="",path11="",path12="",muserid="";
    public  static String  inputFormat;
    LinearLayout linearLayoutBack;
    TextView textViewAssetMenu,textViewNodata;

    Context context;
    public  static GridView gridView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.cameragridnew);
        CameraActivity = this;
        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();
        textViewNodata = (TextView) findViewById(R.id.textViewNodata);
        textViewAssetMenu = (TextView)findViewById(R.id.textViewAssetMenu);
        imageDetails = (TextView) findViewById(R.id.imageDetails);
        linearLayoutBack = (LinearLayout)findViewById(R.id.linearLayoutBack);
        Typeface font = Typeface.createFromAsset(getAssets(),
                "fontawesome-webfont.ttf");
        textViewAssetMenu.setTypeface(font);
        context=CameraPhotoCapture.this;


       /* imageView1 = (ImageView) findViewById(R.id.imageView1);
        imageView2= (ImageView) findViewById(R.id.imageView2);
        imageView3 = (ImageView) findViewById(R.id.imageView3);
        imageView4= (ImageView) findViewById(R.id.imageView4);
        imageView5= (ImageView) findViewById(R.id.imageView5);
        imageView6= (ImageView) findViewById(R.id.imageView6);
        imageView7= (ImageView) findViewById(R.id.imageView7);
        imageView8= (ImageView) findViewById(R.id.imageView8);
        imageView9= (ImageView) findViewById(R.id.imageView9);
        imageView10= (ImageView) findViewById(R.id.imageView10);
        imageView11= (ImageView) findViewById(R.id.imageView11);
        imageView12= (ImageView) findViewById(R.id.imageView12);
        imageView1.setImageResource(android.R.color.transparent);
        imageView2.setImageResource(android.R.color.transparent);*/
        gridView=(GridView) findViewById(R.id.gridview);

        confirmOrder = (Button) findViewById(R.id.confirmOrder);
        final Button photo = (Button) findViewById(R.id.photo);

        if(Constants.adapterset==1){
            textViewNodata.setVisibility(TextView.GONE);
            gridView.setVisibility(View.VISIBLE);
            setadp();
        }
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e("inside", "inside");
                Intent io = new Intent(CameraPhotoCapture.this, ViewFullImage.class);
                Log.e("position", String.valueOf(position));
                io.putExtra("position", position);
                startActivity(io);
                finish();
            }
        });

        photo.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {

              /*  if(checkAndRequestPermissions()) {
                    // carry on the normal flow, as the case of  permissions  granted.
                    callMethod();
                }*/

              int permissionCheck = ContextCompat.checkSelfPermission(CameraPhotoCapture.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(
                            CameraPhotoCapture.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constants.WRITE_EXTERNAL_STORAGE);

                } else {
                    callMethod();
                }

            }

        });

        linearLayoutBack.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                // Log.e("imageIDsize",String.valueOf(arraylistimageID.size()));

                Intent to = new Intent(CameraPhotoCapture.this, MerchantOrderActivity.class);
                Constants.adapterset=0;
                startActivity(to);
                finish();
            }
        });

        confirmOrder.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Constants.adapterset=0;
                Log.e("arraylistimagepath", String.valueOf(MerchantOrderActivity.arrayListimagebitmap.size()));
                if(MerchantOrderActivity.arrayListimagebitmap.size()==0){
                    showAlertDialogToast("Please take picture to place order");
                }else{
                    Constants.Imageduserid="";
                    Intent to = new Intent (CameraPhotoCapture.this, MyDealersActivity.class);
                    to.putExtra("Key","ImageOrder");
                    startActivity(to);
                    finish();
                }



                //saveOrder();


            }
        });

    }
    private  boolean checkAndRequestPermissions() {
        int permissionSendMessage = ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {

         case  Constants.WRITE_EXTERNAL_STORAGE:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    callMethod();
                }
                break;

            default:
                break;

          /*  case 2909: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e("Permission", "Granted");
                    callMethod();
                } else {
                    Log.e("Permission", "Denied");

                }
                return;
            }*/
        }
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }



    public static void deleteFileFromMediaStore(final ContentResolver contentResolver, final File file) {
        String canonicalPath;
        try {
            canonicalPath = file.getCanonicalPath();
        } catch (IOException e) {
            canonicalPath = file.getAbsolutePath();
        }
        final Uri uri = MediaStore.Files.getContentUri("external");
        final int result = contentResolver.delete(uri,
                MediaStore.Files.FileColumns.DATA + "=?", new String[]{canonicalPath});
        if (result == 0) {
            final String absolutePath = file.getAbsolutePath();
            if (!absolutePath.equals(canonicalPath)) {
                contentResolver.delete(uri,
                        MediaStore.Files.FileColumns.DATA + "=?", new String[]{absolutePath});
            }
        }
    }
    private void  callMethod(){

     String fileName = "Camera_Example.jpg";

        ContentValues values = new ContentValues();

        values.put(MediaStore.Images.Media.TITLE, fileName);

        values.put(MediaStore.Images.Media.DESCRIPTION,"Image capture by camera");

        imageUri = getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        Log.e("values", String.valueOf(values));

        Intent intent = new Intent( MediaStore.ACTION_IMAGE_CAPTURE );

        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);

        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);

        startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);


    }

    private void saveOrder() {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "",strOrderId;

            @Override
            protected void onPreExecute() {
                dialog= ProgressDialog.show(CameraPhotoCapture.this, "",
                        "Loading...", true, true);
            }


            @Override
            protected void onPostExecute(Void result) {

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);
                    if(strStatus.equals("true")){
                        strOrderId = JsonAccountObject.getString("data");
                        Log.e("strOrderId", strOrderId);

                        UploadFileToServer ufs = new UploadFileToServer(
                                CameraPhotoCapture.this, path1,path2,path3,path4, path5,path6,path7,path8,path9,path10,path11,path12,strOrderId);
                        ufs.execute();
                       /* Intent to = new Intent (CameraPhotoCapture.this, MerchantOrderConfirmation.class);
                        to.putExtra("strOrderId",strOrderId);
                        startActivity(to);
                        finish();*/


                    }else{
                        showAlertDialogToast(strMsg);
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    //Log.e("ProductActivityException", e.toString());
                }
                dialog.dismiss();
            }

            @Override
            protected Void doInBackground(Void... params) {
                JSONObject jsonObject = new JSONObject();
                try {
                    JSONArray jsonArray = new JSONArray();
                    JSONObject jsonObject1,jsonObject2;

                    String  inputFormat= new SimpleDateFormat(
                            "yyyy-MM-dd HH:mm:ss").format(new java.util.Date());


                    jsonObject1 = new JSONObject();
                    jsonObject2 = new JSONObject();
                    try {


                        jsonObject2.put("image", "Y");
                        jsonObject2.put("duserid", "194");
                        jsonObject2.put("ordslno", "0");
                        jsonObject2.put("ordstatus", "Pending");
                        jsonObject2.put("deliverydt",inputFormat);
                        jsonArray.put(jsonObject2);
                        jsonObject.put("orderdtls", jsonArray);




                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }






                    jsonObject.put("muserid", Constants.USER_ID);
                    jsonObject.put("usertype", Constants.USER_TYPE);
                    jsonObject.put("orderdt",inputFormat);


                    if(Constants.PaymentType.equals("Cash")){
                        jsonObject.put("iscash","C");
                    }else if(Constants.PaymentType.equals("Credit")){
                        jsonObject.put("iscash", "CR");
                    }

                    Log.e("payment type", Constants.PaymentType);

                    jsonObject.put("orderdtls", jsonArray);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                JsonServiceHandler = new JsonServiceHandler(Utils.strsavemerchantOderdetail,jsonObject.toString(), CameraPhotoCapture.this);
                JsonAccountObject = JsonServiceHandler.ServiceData();
                return null;
            }
        }.execute(new Void[]{});
    }

    public void showAlertDialogToast( String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }
    @Override
    protected void onActivityResult( int requestCode, int resultCode, Intent data)
    {

      /*  if (resultCode == Activity.RESULT_OK)
        {
            try
            {
                photoPath = getPath(imageUri);
                MerchantOrderActivity.arraylistimagepath.add(photoPath);
                Log.e("photoPath",photoPath);


               // Bitmap b = decodeUri(imageUri);
                //your_image_view.setImageBitmap(b);
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }*/
 if ( requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {

     Log.e("data",String.valueOf(data));

            if ( resultCode == RESULT_OK ) { //&& data !=null

               /*********** Load Captured Image And Data Start ****************/
                Log.e("imageUri",String.valueOf(imageUri));
                imageId = convertImageUriToFile( imageUri,CameraActivity);


                //  Create and excecute AsyncTask to load capture image

                new LoadImagesFromSDCard().execute(""+imageId);

                //*********** Load Captured Image And Data End ****************/


            } else if ( resultCode == RESULT_CANCELED) {

                // Toast.makeText(this, " Picture was not taken ", Toast.LENGTH_SHORT).show();
            } else {

                // Toast.makeText(this, " Picture was not taken ", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private String getPath(Uri selectedImaeUri)
    {
        String[] projection = { MediaStore.Images.Media.DATA };

        Cursor cursor = managedQuery(selectedImaeUri, projection, null, null,
                null);

        if (cursor != null)
        {
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

            return cursor.getString(columnIndex);
        }

        return selectedImaeUri.getPath();
    }

    /************ Convert Image Uri path to physical path **************/

    public static String convertImageUriToFile ( Uri imageUri, Activity activity )  {

        Cursor cursor = null;
        int imageID = 0;

        try {

            /*********** Which columns values want to get *******/
            String [] proj={
                    MediaStore.Images.Media.DATA,
                    MediaStore.Images.Media._ID,
                    MediaStore.Images.Thumbnails._ID,
                    MediaStore.Images.ImageColumns.ORIENTATION
            };
            Log.e("proj", String.valueOf(proj));

            cursor = activity.managedQuery(

                    imageUri,         //  Get data for specific image URI
                    proj,             //  Which columns to return
                    null,             //  WHERE clause; which rows to return (all rows)
                    null,             //  WHERE clause selection arguments (none)
                    null              //  Order-by clause (ascending by name)

            );

            //  Get Query Data

            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media._ID);
            int columnIndexThumb = cursor.getColumnIndexOrThrow(MediaStore.Images.Thumbnails._ID);
            int file_ColumnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

            //int orientation_ColumnIndex = cursor.
            //    getColumnIndexOrThrow(MediaStore.Images.ImageColumns.ORIENTATION);

            int size = cursor.getCount();
            Log.e("size", String.valueOf(size));

            /*******  If size is 0, there are no images on the SD Card. *****/

            if (size == 0) {


                imageDetails.setText("No Image");
            }
            else
            {

                int thumbID = 0;
                if (cursor.moveToFirst()) {

                    /**************** Captured image details ************/

                    /*****  Used to show image on view in LoadImagesFromSDCard class ******/
                    imageID     = cursor.getInt(columnIndex);

                    thumbID     = cursor.getInt(columnIndexThumb);

                    Path = cursor.getString(file_ColumnIndex);

                    //String orientation =  cursor.getString(orientation_ColumnIndex);

                    CapturedImageDetails = " CapturedImageDetails : \n\n"
                            +" ImageID :"+imageID+"\n"
                            +" ThumbID :"+thumbID+"\n"
                            +" Path :"+Path+"\n";

MerchantOrderActivity.arraylistimagepath.add(Path);

                    Log.e("CapturedImageDetails",CapturedImageDetails);
                    // Show Captured Image detail on activity
                    imageDetails.setText( CapturedImageDetails );

                }
            }
        } finally {
            if (cursor != null) {
                //  cursor.close();
            }
        }

        // Return Captured Image ImageID ( By this ImageID Image will load from sdcard )

        return ""+imageID;
    }


    /**
     * Async task for loading the images from the SD card.
     *
     * @author Android Example
     *
     */

    // Class with extends AsyncTask class

    public class LoadImagesFromSDCard  extends AsyncTask<String, Void, Void> {

        private ProgressDialog Dialog = new ProgressDialog(CameraPhotoCapture.this);



        protected void onPreExecute() {
            /****** NOTE: You can call UI Element here. *****/
            textViewNodata.setVisibility(TextView.GONE);
            gridView.setVisibility(GridView.VISIBLE);
            // Progress Dialog
            Dialog.setMessage(" Loading image from Sdcard..");
            Dialog.show();
        }


        // Call after onPreExecute method
        protected Void doInBackground(String... urls) {

            Bitmap bitmap = null;
            Bitmap newBitmap = null;
            Uri uri = null;


          try {

                /**  Uri.withAppendedPath Method Description
                 * Parameters
                 *    baseUri  Uri to append path segment to
                 *    pathSegment  encoded path segment to append
                 * Returns
                 *    a new Uri based on baseUri with the given segment appended to the path
                 */


              uri = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "" + urls[0]);

               /**************  Decode an input stream into a bitmap. *********/
                bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(uri));

                if (bitmap != null) {

                   /********* Creates a new bitmap, scaled from an existing bitmap. ***********/

                    newBitmap = Bitmap.createScaledBitmap(bitmap, 170, 170, true);

                    bitmap.recycle();

                    mBitmap1 = newBitmap;
                    path1 =Path;


                }
            } catch (IOException e) {
                // Error fetching image, try to recover

             /********* Cancel execution of this task. **********/
                cancel(true);
            }

            return null;
        }


        protected void onPostExecute(Void unused) {

            // NOTE: You can call UI Element here.


            MerchantOrderActivity.arrayListimagebitmap.add(mBitmap1);
            Log.e("pathload", String.valueOf(Path));



            Log.e("pathsize", String.valueOf(MerchantOrderActivity.arraylistimagepath.size()));
            setadp();
            // Close progress dialog
            Dialog.dismiss();

            // Set Image to ImageView
            Log.e("showImg", "showImg");

        }

    }
    public void setadp(){
        gridView.setAdapter(new CameraGridAdapter(this));
    }

    public boolean netCheck() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                //setofflinDealer();
                return true;
            } else if (result == null) {
                //setofflinDealer();
						/*showAlertDialog(MyDealersActivity.this,
								"No Internet Connection",
								"Please Check Your internet connection.", false);*/
                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public void showAlertDialog(Context context, String title, String message,
                                Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        alertDialog.show();
    }



    public void onBackPressed() {
        exitAlret();
    }

    private void exitAlret() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        localBuilder.setMessage("Do you want to Exit?");
        localBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {

                        finish();

                    }
                });
        localBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {
                        paramDialogInterface.cancel();
                    }
                });
        localBuilder.create().show();
    }
}
