package com.freshorders.freshorder.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.OrderAcceptanceDetailAdapter;
import com.freshorders.freshorder.aync.IUserDetailCallBack;
import com.freshorders.freshorder.aync.UserDetailTask;
import com.freshorders.freshorder.model.GRN;
import com.freshorders.freshorder.model.OrderDetail;
import com.freshorders.freshorder.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.freshorders.freshorder.ui.GRNOrderScreenActivity.GRN_CLICKED_POS;
import static com.freshorders.freshorder.ui.GRNOrderScreenActivity.GRN_LIST;

public class OrderAcceptanceDetailScreenActivity extends Activity implements IUserDetailCallBack {

    private ListView orderAcceptanceDetailListview;
    private int clickedPos;
    private ArrayList<GRN> grnArrayList;
    private ArrayList<OrderDetail> orderDetails;
    private GRN grn;
    private ProgressDialog mProgressDialog;
    private static final String TAG = OrderAcceptanceDetailScreenActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.order_acceptance_detail_screen_activity);
        getIntentExtra(getIntent().getExtras());
        TextView distrbName = (TextView)findViewById(R.id.distrNameTxtView);
        TextView value = (TextView) findViewById(R.id.valTxtView);
        TextView orderNo = (TextView)findViewById(R.id.orderNoTxtView);
        TextView orderDt = (TextView)findViewById(R.id.orderdtTxtView);
        Button saveBtn = (Button)findViewById(R.id.saveBtn);
        if(grn != null){
            distrbName.setText(grn.getDistributorName());
            orderDt.setText(grn.getOrderdt());
            value.setText(grn.getAprxordval());
            orderNo.setText(grn.getOrdid());
        }
        orderAcceptanceDetailListview = (ListView) findViewById(R.id.orderAcceptanceDetailListView);
        final OrderAcceptanceDetailAdapter orderAcceptanceDetailAdapter = new OrderAcceptanceDetailAdapter(OrderAcceptanceDetailScreenActivity.this,
                R.layout.order_acceptance_detail_list_view, orderDetails);
        orderAcceptanceDetailListview.setAdapter(orderAcceptanceDetailAdapter);

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<OrderDetail> details = orderAcceptanceDetailAdapter.getUpdatedList();
                if(details == null)
                    return;
                show();
                final UserDetailTask userDetail = new UserDetailTask(Utils.strBulkOrderAcceptance,
                        getOrderAcceptanceRequest(details).toString(), OrderAcceptanceDetailScreenActivity.this, OrderAcceptanceDetailScreenActivity.this);
                userDetail.execute();

                Log.d("postData", ""+ getOrderAcceptanceRequest(details).toString());

            }
        });
    }

    private void getIntentExtra(final Bundle bundle) {
        if (bundle != null) {
            clickedPos = bundle.getInt(GRN_CLICKED_POS);
            grnArrayList = bundle.getParcelableArrayList(GRN_LIST);
            if(grnArrayList != null && grnArrayList.size()>0){
                grn = grnArrayList.get(clickedPos);
                orderDetails = grnArrayList.get(clickedPos).getOrderDetailList();
            }
        }
    }

    private JSONObject getOrderAcceptanceRequest(final ArrayList<OrderDetail> list){
        JSONObject reqObj = null;
        try {
            if(list != null && list.size() > 0){
                reqObj = new JSONObject();
                final JSONArray ordDtlArray = new JSONArray();
                reqObj.put("isacceptance", "Y");
                for (int i = 0; i < list.size(); i++){
                    final JSONObject object = new JSONObject();
                    OrderDetail orderDetail = list.get(i);
                    if(orderDetail.isAccepted()){
                        object.put("orddtlid", orderDetail.getOrddtlid());
                        object.put("acceptedyn", "Y");
                        ordDtlArray.put(object);
                    }
                }
                reqObj.put("orderdtls", ordDtlArray);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return reqObj;
    }

    private void show() {
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(OrderAcceptanceDetailScreenActivity.this, "",
                    "Sending Data wait ...", true, true);
        }
    }

    private void dismissDialog() {
        if (mProgressDialog != null &&
                mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void userResponse(JSONObject jsonObject) {
        dismissDialog();

        if (jsonObject == null) {
            showAlert("Something Went Wrong");
            return;
        }
        Log.d(TAG, "grnResponse in ......" + jsonObject.toString());
        if (jsonObject.optString("status").equalsIgnoreCase("true")) {
            final JSONArray dataArr = jsonObject.optJSONArray("data");
            final String msg = jsonObject.optString("message");
            showAlert(msg);
        } else if (jsonObject.optString("status").equalsIgnoreCase("false")) {
            final String msg = jsonObject.optString("message");
            showAlert(msg);
        }
    }

    public void showAlert( String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        OrderAcceptanceDetailScreenActivity.this.finish();
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();

        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }
}
