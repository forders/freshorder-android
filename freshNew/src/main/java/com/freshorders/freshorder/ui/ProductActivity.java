package com.freshorders.freshorder.ui;

import java.util.ArrayList;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.DealerProductListAdapter;
import com.freshorders.freshorder.adapter.MerchantOrderListAdapter;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.DealerProductDomain;
import com.freshorders.freshorder.domain.MerchantOrderListDomain;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ProductActivity extends Activity {

	TextView menuIcon, textViewAssetMenuProfile, textViewAssetMenuMyOrders,
			textViewAssetMenuMyDealers, textViewAssetMenuProducts,
			textViewAssetMenuPayment, textViewAssetMenuComplaint,
			textViewAssetMenuSignout,textViewAssetMenuSClientVisit,textViewHeader,textViewAssetSearch,textViewAssetMenuCreateOrder,textViewNodata,textViewAssetMenuExport,textViewCross;
	int menuCliccked;
	LinearLayout linearLayoutMenuParent, linearLayoutProfile,
			linearLayoutMyOrders, linearLayoutMyDealers, linearLayoutProducts,
			linearLayoutPayment, linearLayoutComplaint, linearLayoutSignout,linearlayoutSearchIcon,linearLayoutCreateOrder,linearLayoutExport,linearLayoutClientVisit;
	JSONObject JsonAccountObject = null;
	JSONArray JsonAccountArray = null;
	DatabaseHandler databaseHandler;
	ListView listViewProductList;
	Button buttonAddProduct;
	JsonServiceHandler JsonServiceHandler;
	ArrayList<DealerProductDomain> arraylistDealerProductList,arrayListSearchResults;
	public static Typeface font;
	 EditText editTextSearchField;
	 public static String searchclick="0", strMsg = "",PaymentStatus="NULL";
	 public static DealerProductListAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(1);
		setContentView(R.layout.products_screen);
		netCheck();
		databaseHandler = new DatabaseHandler(getApplicationContext());
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();
		myProducts();
		Cursor cur;
		cur = databaseHandler.getDetails();
		cur.moveToFirst();
		Log.e("Insertion Check",
				cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_username)));
		Log.e("Insertion Check",
				cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_password)));
		Log.e("Insertion Check",
				cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_usertype)));
		listViewProductList = (ListView) findViewById(R.id.listViewProductList);
		buttonAddProduct = (Button) findViewById(R.id.buttonAddProduct);
		linearLayoutProfile = (LinearLayout) findViewById(R.id.linearLayoutProfile);
		linearLayoutCreateOrder = (LinearLayout) findViewById(R.id.linearLayoutCreateOrder);
		linearLayoutMyOrders = (LinearLayout) findViewById(R.id.linearLayoutMyOrders);
		linearLayoutMyDealers = (LinearLayout) findViewById(R.id.linearLayoutMyDealers);
		linearLayoutProducts = (LinearLayout) findViewById(R.id.linearLayoutProducts);
		linearLayoutPayment = (LinearLayout) findViewById(R.id.linearLayoutPayment);
		linearLayoutComplaint = (LinearLayout) findViewById(R.id.linearLayoutComplaint);
		linearLayoutSignout = (LinearLayout) findViewById(R.id.linearLayoutSignout);



		textViewNodata = (TextView) findViewById(R.id.textViewNodata);
		textViewAssetMenuExport = (TextView) findViewById(R.id.textViewAssetMenuExport);
		linearLayoutExport= (LinearLayout) findViewById(R.id.linearLayoutExport);
		linearlayoutSearchIcon = (LinearLayout) findViewById(R.id.linearlayoutSearchIcon);
		linearLayoutClientVisit = (LinearLayout) findViewById(R.id.linearLayoutClientVisit);
		textViewHeader= (TextView) findViewById(R.id.textViewHeader);
		textViewAssetSearch= (TextView) findViewById(R.id.textViewAssetSearch);
		editTextSearchField = (EditText) findViewById(R.id.editTextSearchField);
		textViewCross=(TextView) findViewById(R.id.textViewCross);


		buttonAddProduct.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ProductActivity.this,
						DealerAddProductActivity.class);

				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_left);
			}
		});

		listViewProductList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(ProductActivity.this,
						DealerUpdateProductActivity.class);
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(listViewProductList.getWindowToken(), 0);
				intent.putExtra("prodId", arraylistDealerProductList.get(position).getProductID());
				intent.putExtra("prodName", arraylistDealerProductList.get(position).getProductName());
				intent.putExtra("prodCode", arraylistDealerProductList.get(position).getProductCode());
				intent.putExtra("prodStatus", arraylistDealerProductList.get(position).getStatus());
			
				startActivity(intent);
				finish();
				overridePendingTransition(R.anim.slide_in_left,
						R.anim.slide_out_left);
			}
		});

		if (Constants.USER_TYPE.equals("D")) {
			linearLayoutMyDealers.setVisibility(View.GONE);
			linearLayoutProducts.setVisibility(View.VISIBLE);
			linearLayoutCreateOrder.setVisibility(View.GONE);
			linearLayoutExport.setVisibility(View.VISIBLE);
		} else if (Constants.USER_TYPE.equals("M")){
			linearLayoutMyDealers.setVisibility(View.VISIBLE);
			linearLayoutProducts.setVisibility(View.GONE);
			linearLayoutCreateOrder.setVisibility(View.GONE);
			linearLayoutExport.setVisibility(View.GONE);
		}else {
			linearLayoutMyDealers.setVisibility(View.VISIBLE);
			linearLayoutProducts.setVisibility(View.GONE);
			linearLayoutCreateOrder.setVisibility(View.VISIBLE);
			linearLayoutExport.setVisibility(View.GONE);
		}

		menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
		textViewAssetMenuCreateOrder= (TextView) findViewById(R.id.textViewAssetMenuCreateOrder);
		textViewAssetMenuProfile = (TextView) findViewById(R.id.textViewAssetMenuProfile);
		textViewAssetMenuMyOrders = (TextView) findViewById(R.id.textViewAssetMenuMyOrders);
		textViewAssetMenuMyDealers = (TextView) findViewById(R.id.textViewAssetMenuMyDealers);
		textViewAssetMenuProducts = (TextView) findViewById(R.id.textViewAssetMenuProducts);
		textViewAssetMenuPayment = (TextView) findViewById(R.id.textViewAssetMenuPayment);
		textViewAssetMenuComplaint = (TextView) findViewById(R.id.textViewAssetMenuComplaint);
		textViewAssetMenuSignout = (TextView) findViewById(R.id.textViewAssetMenuSignout);
		textViewAssetMenuSClientVisit = (TextView) findViewById(R.id.textViewAssetMenuSClientVisit);

		linearLayoutMenuParent = (LinearLayout) findViewById(R.id.linearLayoutMenuParent);
		font = Typeface.createFromAsset(getAssets(),
				"fontawesome-webfont.ttf");
		menuIcon.setTypeface(font);
		textViewAssetMenuCreateOrder.setTypeface(font);
		textViewAssetSearch.setTypeface(font);
		textViewAssetMenuProfile.setTypeface(font);
		textViewAssetMenuMyOrders.setTypeface(font);
		textViewAssetMenuMyDealers.setTypeface(font);
		textViewAssetMenuProducts.setTypeface(font);
		textViewAssetMenuPayment.setTypeface(font);
		textViewAssetMenuComplaint.setTypeface(font);
		textViewAssetMenuSignout.setTypeface(font);
		textViewAssetMenuExport.setTypeface(font);
		textViewCross.setTypeface(font);
		textViewAssetMenuSClientVisit.setTypeface(font);

	linearlayoutSearchIcon.setOnClickListener(new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if (searchclick == "0") {
				textViewHeader.setVisibility(TextView.GONE);
				editTextSearchField.setVisibility(EditText.VISIBLE);
				editTextSearchField.setFocusable(true);
				editTextSearchField.requestFocus();

				linearLayoutMenuParent.setVisibility(View.GONE);
				textViewCross.setVisibility(TextView.VISIBLE);
				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
				Log.e("Keyboard", "Show");
				searchclick = "1";
			} else {
				textViewHeader.setVisibility(TextView.VISIBLE);
				editTextSearchField.setVisibility(EditText.GONE);
				textViewCross.setVisibility(TextView.GONE);

				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(linearlayoutSearchIcon.getWindowToken(), 0);
				searchclick = "0";
			}

		}
	});
		
		editTextSearchField.addTextChangedListener(new TextWatcher() {

			public void afterTextChanged(Editable s) {
				arraylistDealerProductList = new ArrayList<DealerProductDomain>();
				String searchText = editTextSearchField.getText().toString().toLowerCase(Locale.getDefault());
				for (DealerProductDomain pd : arrayListSearchResults) {
					if (pd.getProductName().toLowerCase(Locale.getDefault()).contains(searchText) || pd.getProductCode().toLowerCase(Locale.getDefault()).contains(searchText)) {
						arraylistDealerProductList.add(pd);
					}
				}
				ProductActivity.this.runOnUiThread(new Runnable() {

					@Override
					public void run() {
						adapter = new DealerProductListAdapter(
								ProductActivity.this,
								R.layout.item_dealer_product_screen,
								arraylistDealerProductList);
						listViewProductList.setAdapter(adapter);
					}
				});

			}

			public void beforeTextChanged(CharSequence s, int start,
										  int count, int after) {
			}

			public void onTextChanged(CharSequence s, int start,
									  int before, int count) {
				textViewCross.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						editTextSearchField.setText("");
					}
				});
			}
		});

	
		
		menuIcon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (menuCliccked == 0) {
					linearLayoutMenuParent.setVisibility(View.VISIBLE);
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(menuIcon.getWindowToken(), 0);
					editTextSearchField.setVisibility(EditText.GONE);
					textViewHeader.setVisibility(TextView.VISIBLE);
					textViewCross.setVisibility(TextView.GONE);
					menuCliccked = 1;
				} else {
					linearLayoutMenuParent.setVisibility(View.GONE);
					textViewHeader.setVisibility(TextView.VISIBLE);
					editTextSearchField.setVisibility(EditText.GONE);
					textViewCross.setVisibility(TextView.GONE);
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(linearlayoutSearchIcon.getWindowToken(), 0);
					menuCliccked = 0;
				}

			}
		});

		linearLayoutMenuParent.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
			}
		});

		linearLayoutProfile.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				if(netCheckWithoutAlert()==true){
					getDetails("profile");
				}else{
					showAlertDialogToast("Please Check Your internet connection");
				}
			}
		});

		linearLayoutMyOrders.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				// TODO Auto-generated method stub
				if(netCheckWithoutAlert()==true){
					getDetails("myorders");
				}else{
					showAlertDialogToast("Please Check Your internet connection");
				}

			}
		});


		linearLayoutProducts.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				// TODO Auto-generated method stub


			}
		});

		linearLayoutCreateOrder.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				// TODO Auto-generated method stub
				Intent io = new Intent(ProductActivity.this,
						CreateOrderActivity.class);
				startActivity(io);
				finish();

			}
		});

		linearLayoutPayment.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				if(netCheckWithoutAlert()==true){
					Intent io = new Intent(ProductActivity.this,
							DealerPaymentActivity.class);
					startActivity(io);
					finish();
				}else{
					showAlertDialogToast("Please Check Your internet connection");
				}


			}
		});

		linearLayoutComplaint.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				//if(netCheckWithoutAlert()==true){
					getDetails("postnotes");
				//}else{
				//	showAlertDialogToast("Please Check Your internet connection");
				//}

			}
		});
		linearLayoutClientVisit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				//if(netCheckWithoutAlert()==true){
					getDetails("clientvisit");
				//}else{
				//	showAlertDialogToast("Please Check Your internet connection");
				//}

			}
		});

		linearLayoutExport.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				Constants.filter="0";
				if(netCheckWithoutAlert()==true){
					getDetails("export");
				}else{
					showAlertDialogToast("Please Check Your internet connection");
				}

			}
		});
		linearLayoutSignout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				// TODO Auto-generated method stub
				databaseHandler.delete();
				Intent io = new Intent(ProductActivity.this,
						SigninActivity.class);
				startActivity(io);
				finish();
			}
		});


	}
	private void getDetails(final String paymentcheck) {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog= ProgressDialog.show(ProductActivity.this, "",
						"Loading...", true, true);

			}

			@Override
			protected void onPostExecute(Void result) {

				try {

					PaymentStatus=(JsonAccountObject.getString("pymtstatus"));
					Log.e("PaymentStatus",PaymentStatus);


					if(PaymentStatus.equals("Pending")||PaymentStatus.equals(null) ||PaymentStatus.equals("")||PaymentStatus.equals("null")){
						showAlertDialogToast("Please make payment before place order");
					}else{
						Log.e("paymentcheck", paymentcheck);

						if(paymentcheck.equals("profile")){
							Intent io = new Intent(ProductActivity.this,
									ProfileActivity.class);
							startActivity(io);
							finish();

						}else if(paymentcheck.equals("postnotes")){
							Intent io = new Intent(ProductActivity.this,
									DealersComplaintActivity.class);
							startActivity(io);
							finish();

						}else if(paymentcheck.equals("export")){
							Intent io = new Intent(ProductActivity.this,
									ExportActivity.class);
							startActivity(io);
							finish();

						}else if(paymentcheck.equals("myorders")){
							Intent to = new Intent(ProductActivity.this,
									DealersOrderActivity.class);
							startActivity(to);
							finish();

						}else if(paymentcheck.equals("clientvisit")){

							Intent io = new Intent(ProductActivity.this,
									DealerClientVisit.class);
							startActivity(io);
							finish();

						}

					}

					dialog.dismiss();


				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
				}

			}

			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();


				JsonServiceHandler = new JsonServiceHandler(Utils.strGetProfileDetail+Constants.USER_ID, ProductActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceDataGet();
				return null;
			}
		}.execute(new Void[]{});
	}
	private void myProducts() {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			

			@Override
			protected void onPreExecute() {
				dialog = ProgressDialog.show(ProductActivity.this, "",
						"Loading...", true, true);

			}

			@Override
			protected void onPostExecute(Void result) {

				try {

					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);

					if (strStatus.equals("true")) {

						JSONArray job1 = JsonAccountObject.getJSONArray("data");
						arraylistDealerProductList = new ArrayList<DealerProductDomain>();
						arrayListSearchResults = new ArrayList<DealerProductDomain>();
						for (int i = 0; i < job1.length(); i++) {
							JSONObject job = new JSONObject();
							job = job1.getJSONObject(i);
							DealerProductDomain dod = new DealerProductDomain();
							dod.setProductID(job.getString("prodid"));
							dod.setProductName(job.getString("prodname"));
							dod.setStatus(job.getString("prodstatus"));
							dod.setProductCode(job.getString("prodcode"));
							dod.setserialno(String.valueOf(i+1));
							arraylistDealerProductList.add(dod);
							arrayListSearchResults.add(dod);
						}
						 setadap();
					}else{
						//toastDisplay(strMsg);

						listViewProductList.setVisibility(View.GONE);
						textViewNodata.setVisibility(View.VISIBLE);
						textViewNodata.setText(strMsg);
						linearlayoutSearchIcon.setClickable(false);
						textViewAssetSearch.setVisibility(TextView.VISIBLE);
					}
						
					
					dialog.dismiss();
				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					//Log.e("ProductActivityException", e.toString());
				}
			
			}

			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();

				JsonServiceHandler = new JsonServiceHandler(
						Utils.strDealerGetProduct + Constants.USER_ID,
						ProductActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceDataGet();
				return null;
			}
		}.execute(new Void[] {});
	}
	
	public  void setadap() {
		// TODO Auto-generated method stub
		
		
			 adapter = new DealerProductListAdapter(
					ProductActivity.this,
					R.layout.item_dealer_product_screen,
					arraylistDealerProductList);
			listViewProductList.setAdapter(adapter);
		
		
	}

	protected void toastDisplay(String msg) {
		Toast toast = Toast.makeText(ProductActivity.this, msg,
				Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

	}

	public boolean netCheckWithoutAlert() {
		// for network connection
		try {
			ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetwork = connectionManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			Object result = null;
			if (mWifi.isConnected() || mNetwork.isConnected()) {
				return true;
			}

			else if (result == null) {

				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	public boolean netCheck() {
		// for network connection
		try {
			ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetwork = connectionManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			Object result = null;
			if (mWifi.isConnected() || mNetwork.isConnected()) {
				return true;
			}

			else if (result == null) {
				showAlertDialog(ProductActivity.this,
						"No Internet Connection",
						"Please Check Your internet connection.", false);
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	public void showAlertDialog(Context context, String title, String message,
			Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
					}
				});
		alertDialog.show();
	}

	public void showAlertDialogToast( String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();

		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

	}
	public void onBackPressed() {
		exitAlret();
	}

	private void exitAlret() {
		AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
		localBuilder.setCancelable(false);
		localBuilder.setMessage("Do you want to Exit?");
		localBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {

						finish();

					}
				});
		localBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {
						paramDialogInterface.cancel();
					}
				});
		localBuilder.create().show();
	}
}
