package com.freshorders.freshorder.ui;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.utils.ImageLoader;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



/**
 * Created by ragul on 07/07/16.
 */
public class ComplaintsDeleteActivity extends Activity {

    TextView textViewComplaints,textViewAssetMenu,textimageheader;
    Button buttonComplaintDelete;
    LinearLayout linearLayoutBack;
    JsonServiceHandler JsonServiceHandler ;
    JSONObject JsonAccountObject = null;
    JSONArray JsonAccountArray = null;
    public static String strStatus = "";
Bitmap mBitmap1=null;
    ImageLoader imageLoader;

ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.complaint_delete_screen);
        netCheck();
        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();

        buttonComplaintDelete = (Button)findViewById(R.id.buttonComplaintDelete);
        linearLayoutBack = (LinearLayout)findViewById(R.id.linearLayoutBack);
        textViewComplaints= (TextView)findViewById(R.id.textViewComplaintContent);
        textViewAssetMenu= (TextView)findViewById(R.id.textViewAssetMenu);

        imageView=(ImageView) findViewById(R.id.imageView);
        textViewComplaints.setText(DealersComplaintActivity.complaintContent);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent io = new Intent(ComplaintsDeleteActivity.this, FullImagePostNotesDealer.class);
                io.putExtra("url", Utils.strcomplainturl + DealersComplaintActivity.imagepost);
                startActivity(io);
                finish();
            }
        });

        Log.e("deletepage", DealersComplaintActivity.imagepost);
        if(DealersComplaintActivity.imagepost.equals("null")){
            Log.e("NULL","NULL");
            imageView.setVisibility(ImageView.GONE);
        }
        else{
            Log.e("inside", DealersComplaintActivity.imagepost);
            imageView.setVisibility(ImageView.VISIBLE);

            Picasso.with(ComplaintsDeleteActivity.this) //Context
                    .load(Utils.strcomplainturl+DealersComplaintActivity.imagepost) //URL/FILE
                    .fit()
                    .placeholder(
                            R.drawable.loader)
                    .centerCrop().into(imageView);



        }

        Typeface font = Typeface.createFromAsset(getAssets(),
                "fontawesome-webfont.ttf");
        textViewAssetMenu.setTypeface(font);

        linearLayoutBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent to = new Intent(ComplaintsDeleteActivity.this, DealersComplaintActivity.class);
                startActivity(to);
                finish();
            }
        });

        buttonComplaintDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Delete();
            }
        });

    }

    private void Delete() {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;

            String strMsg = "";

            @Override
            protected void onPreExecute() {
                dialog= ProgressDialog.show(ComplaintsDeleteActivity.this, "",
                        "Loading...", true, true);

            }

            @Override
            protected void onPostExecute(Void result) {

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {

                        showAlertDialogToast(strMsg);
                    }else{

                        showAlertDialogToast(strMsg);


                    }

                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Log.e("ProductActivityException", e.toString());
                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("status", "deleted");
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


                JsonServiceHandler = new JsonServiceHandler(Utils.strDeleteComplaint+DealersComplaintActivity.cmpID,jsonObject.toString(), ComplaintsDeleteActivity.this);
                JsonAccountObject = JsonServiceHandler.ServiceData();
                return null;
            }
        }.execute(new Void[]{});
    }

    public void showAlertDialogToast( String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
if(strStatus.equals("true")){
    Intent to = new Intent(ComplaintsDeleteActivity.this, DealersComplaintActivity.class);
    startActivity(to);
    finish();

}

                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }


    public void onBackPressed() {
        exitAlret();
    }

    private void exitAlret() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        localBuilder.setMessage("Do you want to Exit?");
        localBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {

                        finish();

                    }
                });
        localBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {
                        paramDialogInterface.cancel();
                    }
                });
        localBuilder.create().show();
    }

    public boolean netCheck() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            }

            else if (result == null) {
                showAlertDialog(ComplaintsDeleteActivity.this,
                        "No Internet Connection",
                        "Please Check Your internet connection.", false);
                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
    public void showAlertDialog(Context context, String title, String message,
                                Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        alertDialog.show();
    }

}
