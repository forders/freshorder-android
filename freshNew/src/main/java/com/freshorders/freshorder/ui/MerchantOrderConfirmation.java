package com.freshorders.freshorder.ui;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.utils.Constants;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MerchantOrderConfirmation extends Activity{
	Button buttonPendingOrder,buttonNewOrder;
	TextView menuIcon,textViewOrderId;
	DatabaseHandler databaseHandler;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(1);
		setContentView(R.layout.order_cnfmtn_screen);
		databaseHandler = new DatabaseHandler(getApplicationContext());
		menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
		textViewOrderId= (TextView) findViewById(R.id.textViewOrderId);
		buttonNewOrder=(Button)findViewById(R.id.buttonNewOrder);
		buttonPendingOrder=(Button)findViewById(R.id.buttonPendingOrder);
		netCheck();
		Cursor cur;
		cur = databaseHandler.getDetails();
		cur.moveToFirst();
		Constants.USER_TYPE=cur.getString(cur
				.getColumnIndex(DatabaseHandler.KEY_usertype));
        Log.e("Confirmation User Type", cur.getString(cur
                .getColumnIndex(DatabaseHandler.KEY_usertype)));

		Typeface font = Typeface.createFromAsset(getAssets(),
				"fontawesome-webfont.ttf");
		//menuIcon.setTypeface(font);
		Intent intent = getIntent();
		Cursor curs;
		curs=databaseHandler.getSuccessheader("Success", "2016-10-14");
		Log.e("headercountcomplaint", String.valueOf(curs.getCount()));
		String orderId =intent.getStringExtra("strOrderId");
		textViewOrderId .setText("Order Id : "+orderId);

		
	/*menuIcon.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Constants.checkproduct =0;
				if(Constants.USER_TYPE.equals("M")){
					Intent to = new Intent (MerchantOrderConfirmation.this, MerchantOrderActivity.class);
					startActivity(to);
					finish();
				}else{
					Intent to = new Intent (MerchantOrderConfirmation.this, CreateOrderActivity.class);
					startActivity(to);
					finish();

				}
			}
		});*/
		buttonNewOrder.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Constants.checkproduct=0;
				// TODO Auto-generated method stub
				if(Constants.USER_TYPE.equals("M")){
					if(Constants.Imageduserid.equals("")){
						Intent to = new Intent (MerchantOrderConfirmation.this, MerchantOrderDetailActivity.class);
						startActivity(to);
						finish();
					}else{
						Intent to = new Intent (MerchantOrderConfirmation.this, CameraPhotoCapture.class);
						startActivity(to);
						finish();
						Constants.Imageduserid="";
					}


				}else{
					//Kumaravel // for closing stock menu default merchant selection avoid
					/////Constants.checkproduct = 0;
					Constants.orderid = "";
					Constants.SalesMerchant_Id = "";
					Constants.Merchantname = "";
					Intent to = new Intent (MerchantOrderConfirmation.this, CreateOrderActivity.class);

					startActivity(to);


					finish();

				}
			}
		});
		buttonPendingOrder.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Constants.Merchantname ="";
				Constants.SalesMerchant_Id="";
				CreateOrderActivity.merchantselected=0;
				// TODO Auto-generated method stub

					if(Constants.USER_TYPE.equals("M")){
						Intent to = new Intent (MerchantOrderConfirmation.this, MerchantOrderActivity.class);
						startActivity(to);
						finish();
						Constants.Imageduserid="";
					}else{
                        if(netCheck()==true){
                            Constants.orderstatus ="Success";
                        }else if(netCheck()==false){
                            Constants.orderstatus ="Pending";
                        }
                        CreateOrderActivity.merchantselected=0;
                        Intent to = new Intent (MerchantOrderConfirmation.this, SalesManOrderActivity.class);
                        startActivity(to);
                        finish();
                    }


				
			}
		});
	}
	public boolean netCheck() {
		// for network connection
		try {
			ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetwork = connectionManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			Object result = null;
			if (mWifi.isConnected() || mNetwork.isConnected()) {
				return true;
			}

			else if (result == null) {
				/*showAlertDialog(MerchantOrderConfirmation.this,
						"No Internet Connection",
						"Please Check Your internet connection.", false);*/
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	public void showAlertDialog(Context context, String title, String message,
			Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
					}
				});
		alertDialog.show();
	}

	public void showAlertDialogToast( String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();

		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

	}
	public void onBackPressed() {
		exitAlret();
	}

	private void exitAlret() {
		AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
		localBuilder.setCancelable(false);
		localBuilder.setMessage("Do you want to Exit?");
		localBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {

						finish();

					}
				});
		localBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {
						paramDialogInterface.cancel();
					}
				});
		localBuilder.create().show();
	}
}
