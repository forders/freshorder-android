package com.freshorders.freshorder.ui;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.AddNewProductAdapter;
import com.freshorders.freshorder.adapter.MerchantOrderListAdapter;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.DealerAddProductDomain;
import com.freshorders.freshorder.domain.DealerProductDomain;
import com.freshorders.freshorder.domain.MerchantOrderDomainSelected;
import com.freshorders.freshorder.domain.MerchantOrderListDomain;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class DealerAddNewProduct extends Activity {

    TextView menuIcon, textViewAssetMenuProfile, textViewAssetMenuMyOrders,
            textViewAssetMenuMyDealers, textViewAssetMenuProducts,
            textViewAssetMenuPayment, textViewAssetMenuComplaint,
            textViewAssetMenuSignout,textViewHeader,textViewAssetSearch,textViewAssetMenuCreateOrder,textViewNodata,textViewAssetMenuExport,textViewCross,textViewAssetMenu;
    int menuCliccked;
    /*LinearLayout linearLayoutMenuParent, linearLayoutProfile,
            linearLayoutMyOrders, linearLayoutMyDealers, linearLayoutProducts,
            linearLayoutPayment, linearLayoutComplaint, linearLayoutSignout,linearlayoutSearchIcon,linearLayoutCreateOrder,linearLayoutExport;*/
    LinearLayout linearlayoutSearchIcon;
    JSONObject JsonAccountObject = null;
    JSONArray JsonAccountArray = null;
    DatabaseHandler databaseHandler;
    ListView listViewProductList;
    Button buttonAddProduct;
    JsonServiceHandler JsonServiceHandler;
    ArrayList<DealerAddProductDomain> arraylistDealerProductList,arrayListSearchResults;
    String[]userid,prodid,qty,freeQty,deliverydate,deliverytime,deliverymode,uom,unitgram,unitperuom,price,tax;

    public static Typeface font;
    EditText editTextSearchField;
    public static String searchclick="0", strMsg = "",deliverydt;
    int size=0;
    public static AddNewProductAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.dealer_new_prod_add);

        databaseHandler = new DatabaseHandler(getApplicationContext());
        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();

        if(netCheck()==true){
            myProducts();
        }

        Cursor cur;
        cur = databaseHandler.getDetails();
        cur.moveToFirst();
        Log.e("Insertion Check",
                cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_username)));
        Log.e("Insertion Check",
                cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_password)));
        Log.e("Insertion Check",
                cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_usertype)));

        listViewProductList = (ListView) findViewById(R.id.listViewProductList);
        buttonAddProduct = (Button) findViewById(R.id.buttonAddProduct);
        textViewNodata = (TextView) findViewById(R.id.textViewNodata);
        textViewAssetMenuExport = (TextView) findViewById(R.id.textViewAssetMenuExport);
        linearlayoutSearchIcon = (LinearLayout) findViewById(R.id.linearlayoutSearchIcon);
        textViewHeader= (TextView) findViewById(R.id.textViewHeader);
        textViewAssetSearch= (TextView) findViewById(R.id.textViewAssetSearch);
        editTextSearchField = (EditText) findViewById(R.id.editTextSearchField);
        textViewAssetMenu= (TextView) findViewById(R.id.textViewAssetMenu);
        textViewCross=(TextView) findViewById(R.id.textViewCross);


        buttonAddProduct.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                try {

                    Constants.adapterset=0;
                size=DealerOrderDetailsActivity.arraylistMProductListSelected.size();
                Log.e("size", String.valueOf(size));
                if(size==0){
                    showAlertDialogToast("Atleast select one product");
                    //toastDisplay("No data found");
                }else{

                     deliverydt = new SimpleDateFormat(
                            "yyyy-MM-dd HH:mm:ss").format(new java.util.Date());
                    Log.e("inputFormat",deliverydt);

                    updateorder();
                    /*Intent to = new Intent(DealerAddNewProduct.this,
                            DealerOrderDetailsActivity.class);
                    startActivity(to);
                    finish();*/
                }
                }catch (Exception e) {
                    //Log.e("ProductActivityException", e.toString());
                }
            }
        });



        textViewAssetMenu=(TextView) findViewById(R.id.textViewAssetMenu);

        font = Typeface.createFromAsset(getAssets(),
                "fontawesome-webfont.ttf");

        textViewAssetSearch.setTypeface(font);
        textViewCross.setTypeface(font);
        textViewAssetMenu.setTypeface(font);
        linearlayoutSearchIcon.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (searchclick == "0") {
                    textViewHeader.setVisibility(TextView.GONE);
                    editTextSearchField.setVisibility(EditText.VISIBLE);
                    editTextSearchField.setFocusable(true);
                    editTextSearchField.requestFocus();

                    //linearLayoutMenuParent.setVisibility(View.GONE);
                    textViewCross.setVisibility(TextView.VISIBLE);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                    Log.e("Keyboard", "Show");
                    searchclick = "1";
                } else {
                    textViewHeader.setVisibility(TextView.VISIBLE);
                    editTextSearchField.setVisibility(EditText.GONE);
                    textViewCross.setVisibility(TextView.GONE);

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(linearlayoutSearchIcon.getWindowToken(), 0);
                    searchclick = "0";
                }

            }
        });

        editTextSearchField.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                arraylistDealerProductList = new ArrayList<DealerAddProductDomain>();
                String searchText = editTextSearchField.getText().toString().toLowerCase(Locale.getDefault());
                for (DealerAddProductDomain pd : arrayListSearchResults) {
                    if (pd.getProductName().toLowerCase(Locale.getDefault()).contains(searchText) || pd.getProductCode().toLowerCase(Locale.getDefault()).contains(searchText)) {
                        arraylistDealerProductList.add(pd);
                    }
                }
                DealerAddNewProduct.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        adapter = new AddNewProductAdapter(
                                DealerAddNewProduct.this,
                                R.layout.item_add_new_product_dealer_new,
                                arraylistDealerProductList);
                        listViewProductList.setAdapter(adapter);
                    }
                });

            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                textViewCross.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        editTextSearchField.setText("");
                    }
                });
            }
        });




        textViewAssetMenu.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.adapterset=0;
                Intent io =new Intent(DealerAddNewProduct.this,DealerOrderDetailsActivity.class) ;
                startActivity(io);
                finish();
            }
        });


    }
    public void updateorder(){
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "";

            @Override
            protected void onPreExecute() {
                dialog= ProgressDialog.show(DealerAddNewProduct.this, "",
                        "Loading...", true, true);
            }

            @Override
            protected void onPostExecute(Void result) {

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);
                    saveApproximateTotal();
                    /*showAlertDialogToast(strMsg);
                    if(strStatus.equals("true")){
                        Intent to = new Intent(DealerAddNewProduct.this,
                                DealerOrderDetailsActivity.class);
                        startActivity(to);
                        finish();
                    }else{
                        showAlertDialogToast(strMsg);
                    }*/

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    //Log.e("ProductActivityException", e.toString());
                }
                dialog.dismiss();
            }

            @Override
            protected Void doInBackground(Void... params) {
                JSONObject jsonObject = new JSONObject();
                // try {
                JSONArray jsonArray = new JSONArray();
                JSONObject jsonObject1;
                JSONObject jsonObject2;

                userid = new String[DealerOrderDetailsActivity.arraylistMProductListSelected.size()];
                prodid = new String[DealerOrderDetailsActivity.arraylistMProductListSelected.size()];
                qty = new String[DealerOrderDetailsActivity.arraylistMProductListSelected.size()];
                freeQty= new String[DealerOrderDetailsActivity.arraylistMProductListSelected.size()];
                deliverydate= new String[DealerOrderDetailsActivity.arraylistMProductListSelected.size()];
                deliverytime= new String[DealerOrderDetailsActivity.arraylistMProductListSelected.size()];
                deliverymode= new String[DealerOrderDetailsActivity.arraylistMProductListSelected.size()];
                unitgram= new String[DealerOrderDetailsActivity.arraylistMProductListSelected.size()];
                unitperuom= new String[DealerOrderDetailsActivity.arraylistMProductListSelected.size()];
                uom= new String[DealerOrderDetailsActivity.arraylistMProductListSelected.size()];
                price= new String[DealerOrderDetailsActivity.arraylistMProductListSelected.size()];
                tax= new String[DealerOrderDetailsActivity.arraylistMProductListSelected.size()];
                //index=0;
                //paymentType= new String[MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.size()];

                for (int i = 0; i < DealerOrderDetailsActivity.arraylistMProductListSelected.size(); i++) {
                    userid[i] =DealerOrderDetailsActivity.arraylistMProductListSelected.get(i).getProductCode();
                    prodid[i] =DealerOrderDetailsActivity.arraylistMProductListSelected.get(i).getProductID();
                    qty[i]=DealerOrderDetailsActivity.arraylistMProductListSelected.get(i).getDqty();
                    freeQty[i]=DealerOrderDetailsActivity.arraylistMProductListSelected.get(i).getDfqty();
                    deliverytime[i]=DealerOrderDetailsActivity.arraylistMProductListSelected.get(i).getDeliverytime();
                    deliverymode[i]=DealerOrderDetailsActivity.arraylistMProductListSelected.get(i).getDeliverymode();
                    deliverydate[i]=DealerOrderDetailsActivity.arraylistMProductListSelected.get(i).getDeliverydate();
                    unitgram[i]=DealerOrderDetailsActivity.arraylistMProductListSelected.get(i).getUnitGram();
                    unitperuom[i]=DealerOrderDetailsActivity.arraylistMProductListSelected.get(i).getUnitPerUom();
                    uom[i]=DealerOrderDetailsActivity.arraylistMProductListSelected.get(i).getUom();
                    price[i]=DealerOrderDetailsActivity.arraylistMProductListSelected.get(i).getPrice();
                    tax[i]=DealerOrderDetailsActivity.arraylistMProductListSelected.get(i).getTax();

                    //index[i]=MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.get(i);
                }

                for (int i = 0; i < DealerOrderDetailsActivity.arraylistMProductListSelected.size(); i++) {
                    jsonObject1 = new JSONObject();
                    jsonObject2 = new JSONObject();
                    try {

                        Log.e("fqty",freeQty[i]);
                        Log.e("qty",qty[i]);
                        if(qty[i].equals("")||qty[i].equals(".")){
                            qty[i]="0";
                        }
                        if(freeQty[i].equals("")||freeQty[i].equals(".")){
                            freeQty[i]="0";
                        }
                        double frqty = Float.parseFloat(freeQty[i]);
                        double qtty = Float.parseFloat(qty[i]);
                        //int frqty =Integer.parseInt(freeQty[i]);

                        int serialno=Integer.parseInt(Constants.serialno);
                        serialno=serialno+1;
                        Constants.serialno=String.valueOf(serialno);
                        String deldate="";
                        if(!deliverydate[i].equals("")){
                            SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
                            Date date = null;
                            try {
                                date = fmt.parse(deliverydate[i]);
                            } catch (java.text.ParseException e) {
                                e.printStackTrace();
                            }

                            SimpleDateFormat fmtOut = new SimpleDateFormat("yyyy-MM-dd");
                            deldate= fmtOut.format(date);
                            Log.e("deliverydate", deldate);
                        }
                        if(frqty>0){
                            String flag = "F";
                            Double pvalue = Double.parseDouble(freeQty[i])*Double.parseDouble(unitperuom[i])*(Double.parseDouble(price[i])+ (Double.parseDouble(price[i])*(Double.parseDouble(tax[i])/100)));
                           // Double cqty = (Double.parseDouble(freeQty[i])*Double.parseDouble(unitperuom[i])*Double.parseDouble(unitgram[i]))/1000;
                            Double cqty =( Double.parseDouble(freeQty[i]) *Double.parseDouble(unitperuom[i]));
                            Log.e("cqtyfreeQty[i]", String.valueOf(( Double.parseDouble(freeQty[i]) *Double.parseDouble(unitperuom[i]))));
                            Log.e("Freeqty", freeQty[i]);
                            jsonObject1.put("qty", freeQty[i]);
                            jsonObject1.put("isfree", flag);
                            jsonObject1.put("ordid",Constants.orderid);
                            jsonObject1.put("prodid",prodid[i]);
                            jsonObject1.put("ordstatus", "Pending");
                            jsonObject1.put("duserid", Constants.USER_ID);
                            jsonObject1.put("ordslno", serialno);
                            jsonObject1.put("prate", price[i]);
                            jsonObject1.put("ptax", tax[i]);
                            jsonObject1.put("pvalue", pvalue);
                            jsonObject1.put("default_uom", uom[i]);
                            jsonObject1.put("ord_uom", uom[i]);
                            jsonObject1.put("cqty", cqty);
                            jsonObject1.put("deliverydt", deldate);
                            jsonObject1.put("deliverytime", deliverytime[i]);
                            jsonObject1.put("deliverytype", deliverymode[i]);

                            jsonArray.put(jsonObject1);
                            jsonObject.put("orderdtls", jsonArray);

                         //   DealerOrderDetailsActivity.approximateTotal = DealerOrderDetailsActivity.approximateTotal+pvalue;
                        }

                        if(qtty>0){
                            String flag= "NF";

                            Log.e("quantity", qty[i]);
                            Log.e("unitperuom", unitperuom[i]);
                            Log.e("price", price[i]);
                            Log.e("tax", tax[i]);
                            Double pvalue = Double.parseDouble(qty[i])*Double.parseDouble(unitperuom[i])*(Double.parseDouble(price[i])+ (Double.parseDouble(price[i])*(Double.parseDouble(tax[i])/100)));
                           //Double cqty = (Double.parseDouble(qty[i])*Double.parseDouble(unitperuom[i])*Double.parseDouble(unitgram[i]))/1000;
                            Double cqty =( Double.parseDouble(qty[i]) *Double.parseDouble(unitperuom[i]));
                            Log.e("cqtyqty[i]", String.valueOf(( Double.parseDouble(qty[i]) *Double.parseDouble(unitperuom[i]))));
                            Log.e("pvalue",String.valueOf(pvalue));
                            Log.e("cqty", String.valueOf(cqty));
                            Log.e("quantity", qty[i]);
                            jsonObject2.put("qty", qty[i]);
                            jsonObject2.put("isfree", flag);
                            jsonObject2.put("ordid",Constants.orderid);
                            jsonObject2.put("prodid",prodid[i]);
                            jsonObject2.put("ordstatus", "Pending");
                            jsonObject2.put("duserid", Constants.USER_ID);
                            jsonObject2.put("ordslno", serialno);
                            jsonObject2.put("prate", price[i]);
                            jsonObject2.put("ptax", tax[i]);
                            jsonObject2.put("pvalue", pvalue);
                            jsonObject2.put("default_uom", uom[i]);
                            jsonObject2.put("ord_uom", uom[i]);
                            jsonObject2.put("cqty", cqty);
                            jsonObject2.put("deliverydt",deldate);
                            jsonObject2.put("deliverytime", deliverytime[i]);
                            jsonObject2.put("deliverytype", deliverymode[i]);

                            jsonArray.put(jsonObject2);
                            jsonObject.put("orderdtls", jsonArray);

                            DealerOrderDetailsActivity.approximateTotal = DealerOrderDetailsActivity.approximateTotal+pvalue;
                        }



                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    //jsonArray.put(jsonObject1);
                }


               /* } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }*/
                JsonServiceHandler = new JsonServiceHandler(Utils.strsavedealerOderdetail,jsonObject.toString(), DealerAddNewProduct.this);
                JsonAccountObject = JsonServiceHandler.ServiceData();
                return null;
            }
        }.execute(new Void[] {});
    }

    private void saveApproximateTotal() {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "";

            @Override
            protected void onPreExecute() {
                dialog= ProgressDialog.show(DealerAddNewProduct.this, "",
                        "Loading...", true, true);
            }

            @Override
            protected void onPostExecute(Void result) {

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);
                showAlertDialogToast(strMsg);
                    if(strStatus.equals("true")){
                        Intent to = new Intent(DealerAddNewProduct.this,
                                DealerOrderDetailsActivity.class);
                        startActivity(to);
                        finish();
                    }

                    //showAlertDialogToast(strMsg);

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    //Log.e("ProductActivityException", e.toString());
                }
                dialog.dismiss();
            }

            @Override
            protected Void doInBackground(Void... params) {
                JSONObject jsonObject = new JSONObject();
                try {

                    jsonObject.put("aprxordval", DealerOrderDetailsActivity.approximateTotal);


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                JsonServiceHandler = new JsonServiceHandler(Utils.strUpdateOrderHeader+Constants.orderid,jsonObject.toString(), DealerAddNewProduct.this);
                JsonAccountObject = JsonServiceHandler.ServiceData();


                return null;
            }
        }.execute(new Void[]{});
    }

    private void myProducts() {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";


            @Override
            protected void onPreExecute() {
                dialog = ProgressDialog.show(DealerAddNewProduct.this, "",
                        "Loading...", true, true);

            }

            @Override
            protected void onPostExecute(Void result) {

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {

                        JSONArray job1 = JsonAccountObject.getJSONArray("data");
                        arraylistDealerProductList = new ArrayList<DealerAddProductDomain>();
                        arrayListSearchResults = new ArrayList<DealerAddProductDomain>();
                        for (int i = 0; i < job1.length(); i++) {
                            try{
                            JSONObject job = new JSONObject();
                            job = job1.getJSONObject(i);
                            DealerAddProductDomain dod = new DealerAddProductDomain();
                            dod.setProductID(job.getString("prodid"));
                            dod.setProductName(job.getString("prodname"));
                            dod.setStatus(job.getString("prodstatus"));
                            dod.setProductCode(job.getString("prodcode"));
                                dod.setDeliverytime("");
                                dod.setDeliverydate("");
                                dod.setDeliverymode("");
                            dod.setDqty("");
                            dod.setDfqty("");
                                dod.setUnitGram(job.getString("unitsgm"));
                                dod.setUnitPerUom(job.getString("unitperuom"));
                                dod.setUom(job.getString("uom"));
                                dod.setPrice(job.getString("prodprice"));
                                dod.setTax(job.getString("prodtax"));
                                dod.setserialno(String.valueOf(i+1));
                            arraylistDealerProductList.add(dod);
                            arrayListSearchResults.add(dod);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        setadap();
                    }else{
                        //toastDisplay(strMsg);

                        listViewProductList.setVisibility(View.GONE);
                        textViewNodata.setVisibility(View.VISIBLE);
                        textViewNodata.setText(strMsg);
                        linearlayoutSearchIcon.setClickable(false);
                        textViewAssetSearch.setVisibility(TextView.VISIBLE);
                    }


                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    //Log.e("ProductActivityException", e.toString());
                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();

                JsonServiceHandler = new JsonServiceHandler(
                        Utils.strDealerProduct + Constants.USER_ID,
                        DealerAddNewProduct.this);
                JsonAccountObject = JsonServiceHandler.ServiceDataGet();
                return null;
            }
        }.execute(new Void[] {});
    }

    public  void setadap() {
        // TODO Auto-generated method stub


        adapter = new AddNewProductAdapter(
                DealerAddNewProduct.this,
                R.layout.item_add_new_product_dealer_new,
               arraylistDealerProductList);
        listViewProductList.setAdapter(adapter);


    }

    protected void toastDisplay(String msg) {
        Toast toast = Toast.makeText(DealerAddNewProduct.this, msg,
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();

    }
    public boolean netCheck() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            }

            else if (result == null) {
                showAlertDialog(DealerAddNewProduct.this,
                        "No Internet Connection",
                        "Please Check Your internet connection.", false);
                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
    public void showAlertDialog(Context context, String title, String message,
                                Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        alertDialog.show();
    }

    public void showAlertDialogToast( String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }
    public void onBackPressed() {
        exitAlret();
    }

    private void exitAlret() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        localBuilder.setMessage("Do you want to Exit?");
        localBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {

                        finish();

                    }
                });
        localBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {
                        paramDialogInterface.cancel();
                    }
                });
        localBuilder.create().show();
    }
}
