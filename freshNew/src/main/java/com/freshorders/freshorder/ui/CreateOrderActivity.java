package com.freshorders.freshorder.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.activity.DistanceCalActivity;
import com.freshorders.freshorder.adapter.MerchantDropDownAdapter;
import com.freshorders.freshorder.adapter.MerchantOrderDetailAdapter;
import com.freshorders.freshorder.adapter.MerchantOrderListAdapter;
import com.freshorders.freshorder.adapter.Template10MerchantOrderDetailAdapter;
import com.freshorders.freshorder.adapter.Template1MerchantOrderDetailAdapter;
import com.freshorders.freshorder.adapter.Template2MerchantOrderDetailAdapter;
import com.freshorders.freshorder.adapter.Template3MerchantOrderDetailAdapter;
import com.freshorders.freshorder.adapter.Template4MerchantOrderDetailAdapter;
import com.freshorders.freshorder.adapter.Template6MilkOrderDetailAdapter;
import com.freshorders.freshorder.adapter.Template8StockOnlyAdapter;
import com.freshorders.freshorder.adapter.Template9MerchantOrderDetailAdapter;
import com.freshorders.freshorder.aync.IUserDetailCallBack;
import com.freshorders.freshorder.aync.OnlineAsyncTaskAction;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.MDealerCompDropdownDomain;
import com.freshorders.freshorder.domain.MerchantDropdownDomain;
import com.freshorders.freshorder.domain.MerchantOrderDetailDomain;
import com.freshorders.freshorder.domain.MerchantOrderDomainSelected;
import com.freshorders.freshorder.domain.MerchantOrderListDomain;
import com.freshorders.freshorder.popup.Template6QuantityEditAndSavePopup;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.toonline.migration.PendingDataMigrationActivity;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.GPSTracker;
import com.freshorders.freshorder.utils.GPSTrackerActivity;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.MerchantOrderSelectedConvert;
import com.freshorders.freshorder.utils.MerchantOrderSelectedTemplate3Convert;
import com.freshorders.freshorder.utils.MerchantOrderSelectedTemplate4Convert;
import com.freshorders.freshorder.utils.MerchantOrderSelectedTemplate6Convert;
import com.freshorders.freshorder.utils.MerchantOrderSelectedTemplate9Convert;
import com.freshorders.freshorder.utils.Utils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import static com.freshorders.freshorder.MyApplication.previousSelected;
import static com.freshorders.freshorder.MyApplication.productIdPositionMap;
import static com.freshorders.freshorder.utils.Constants.COMPANY_STOCK;
import static com.freshorders.freshorder.utils.Constants.CURRENT_MILLIS;
import static com.freshorders.freshorder.utils.Constants.CURRENT_STOCK_NOT_AVAIL;
import static com.freshorders.freshorder.utils.Constants.DISTRIBUTOR_STOCK;
import static com.freshorders.freshorder.utils.Constants.MILK_TEMPLATE_NO;
import static com.freshorders.freshorder.utils.Constants.TEMPLATE10_PAIR_MISSING_MESSAGE;
import static com.freshorders.freshorder.utils.Constants.TEMPLATE1_PAIR_MISSING_MESSAGE;
import static com.freshorders.freshorder.utils.Constants.TEMPLATE1_PAIR_MISSING_TITLE;
import static com.freshorders.freshorder.utils.Constants.TEMPLATE3_PAIR_MISSING_MESSAGE;

public class CreateOrderActivity extends Activity implements IUserDetailCallBack {


    private Location mLastLocation;
    private FusedLocationProviderClient mFusedLocationClient;
    private ProgressDialog pDialog;

    private static int gpsCounter = 0;


    LocationManager locationManager;
    boolean gps_enabled = false;
    boolean network_enabled = false;
    boolean passive_enabled = false;


    private LinearLayout llProgressBar;
    private static boolean isStockMenuActive = false;

    TextView menuIcon, textViewAssetMenuProfile, textViewAssetMenuMyOrders,
            textViewAssetMenuMyDealers, textViewAssetMenuProducts,
            textViewAssetMenuPayment, textViewAssetMenuComplaint,textViewAssetMenuDistanceCalculation,
            textViewAssetMenuSignout, textViewAssetMenuClientVisit, textViewAssetMenuPaymentCollection,
            textViewNodata, textViewAssetMenuCreateOrder, textViewcross_1, textViewAssetMenuRefresh, textViewAssetMenuStockEntry, textViewAssetMenuAddMerchant, textViewAssetMenuAcknowledge, textViewAssetMenusurveyuser, textViewAssetMenudownloadscheme, textViewAssetMenuMyDayPlan, textViewAssetMenuMySales,
            textViewAssetMenuDistStock, textViewAssetMenuStockOnly, textViewAddress,
            textViewAssetMenuMigration, textViewAssetMenuStockAudit;

    ListView listViewProducts;

    public static int menuCliccked, selectedPosition;


    LinearLayout linearLayoutMenuParent, linearLayoutProfile,
            linearLayoutMyOrders, linearLayoutMyDealers, linearLayoutProducts,
            linearLayoutPayment, linearLayoutPaymentCollection, linearLayoutComplaint, linearLayoutSignout,
            linearlayoutSearchIcon, linearLayoutCreateOrder, linearLayoutClientVisit, linearLayoutRefresh,
            linearLayoutAcknowledge, linearLayoutStockEntry, linearLayoutAddMerchant,linearLayoutDistanceCalculation,
            linearLayoutsurveyuser, linearLayoutdownloadscheme, linearLayoutMyDayPlan, linearLayoutMySales,
            linearLayoutDistStock, linearLayoutStockOnly, linearLayoutIndicatorStock, linearLayoutIndicatorOrder,
            linearLayoutMigration, linearLayoutStockAudit;

    private void showMenu(){

        MyApplication app = MyApplication.getInstance();

        if(!app.isCreateOrder()){
            linearLayoutCreateOrder.setVisibility(View.GONE);
        }
        if(!app.isProfile()){
            linearLayoutProfile.setVisibility(View.GONE);
        }
        if(!app.isMyDayPlan()){
            linearLayoutMyDayPlan.setVisibility(View.GONE);
        }
        if(!app.isMyOrders()){
            linearLayoutMyOrders.setVisibility(View.GONE);
        }
        if(!app.isAddMerchant()){
            linearLayoutAddMerchant.setVisibility(View.GONE);
        }
        if(!app.isMySales()){
            linearLayoutMySales.setVisibility(View.GONE);
        }
        if(!app.isPostNotes()){
            linearLayoutComplaint.setVisibility(View.GONE);
        }
        if(!app.isMyClientVisit()){
            linearLayoutClientVisit.setVisibility(View.GONE);
        }
        if(!app.isAcknowledge()){
            linearLayoutAcknowledge.setVisibility(View.GONE);
        }
        if(!app.isPaymentCollection()){
            linearLayoutPaymentCollection.setVisibility(View.GONE);
        }
        if(!app.isPkdDataCapture()){
            linearLayoutStockEntry.setVisibility(View.GONE);
        }
        if(!app.isDistributorStock()){
            linearLayoutDistStock.setVisibility(View.GONE);
        }
        if(!app.isSurveyUser()){
            linearLayoutsurveyuser.setVisibility(View.GONE);
        }
        if(!app.isDownLoadScheme()){
            linearLayoutdownloadscheme.setVisibility(View.GONE);
        }
        if(!app.isDistanceCalculation()){
            linearLayoutDistanceCalculation.setVisibility(View.GONE);
        }
        if(!app.isRefresh()){
            linearLayoutRefresh.setVisibility(View.GONE);
        }
        if(!app.isLogout()){
            linearLayoutSignout.setVisibility(View.GONE);
        }
        if(!app.isClosingStock()){
            linearLayoutStockOnly.setVisibility(View.GONE);
        }
        if(!app.isClosingStockAudit()){
            linearLayoutStockAudit.setVisibility(View.GONE);
        }
        if(!app.isPendingData()){
            linearLayoutMigration.setVisibility(View.GONE);
        }
    }

    JSONObject JsonAccountObject = null;
    JSONArray JsonAccountArray = null;
    DatabaseHandler databaseHandler;
    JsonServiceHandler JsonServiceHandler;

    public static ArrayList<MerchantOrderDetailDomain> arraylistMerchantOrderDetailList, arrayListSearchResults, previousLoadedOrderDetailList;
    public static ArrayList<MerchantOrderDomainSelected> arraylistMerchantOrderDetailListSelected, arraylisttemp;

    // by Kumaravel for Template2
    public static HashMap<String, String[]> itemListUOM = new HashMap<>();
    public static List<Integer> UOMCount = new ArrayList<>();

    List<String> array = new ArrayList<String>();
    public static String companyname, fullname, date, address, orderId, selected_dealer, OfflineOrderNo;
    public static String Duserid = "NULL", compname = "NULL", userid = "NULL", prodcode = "NULL", prodid = "NULL", prodname = "NULL", merchantRowid, beatrowid;
    Date dateStr = null;
    public int q = 0;
    public static String time, searchclick = "0", strMsg = "", PaymentStatus = "null", Orderno;
    public static EditText editTextSearchField;
    public static TextView textViewHeader, textViewAssetSearch, textViewCross;
    static ArrayList<MerchantOrderListDomain> arraylistMerchadntOrderList;
    ArrayList<MerchantOrderListDomain> arraylistSearcResultsList;
    public static MerchantOrderListAdapter adapter1;
    public static MerchantOrderDomainSelected domainSelected;
    int size = 0;
    public static AutoCompleteTextView autocompleteTextTraders;
    ArrayList<MDealerCompDropdownDomain> arrayListDealerDomain, arrayListSearch, arrayListmerchant;
    ArrayList<MerchantDropdownDomain> arraylistmerchantdropdown, searchresult;
    ArrayList<String> arrayListDealer;
    String muserId;
    Button buttonAddPlaceOrder, ButtonNoOrder, ButtonLastOrder;
    public static MerchantOrderDetailAdapter adapter;
    public static Template1MerchantOrderDetailAdapter template1Adapter;
    public static Template2MerchantOrderDetailAdapter template2Adapter;
    public static Template3MerchantOrderDetailAdapter template3Adapter;
    public static Template4MerchantOrderDetailAdapter template4Adapter;
    public static Template6MilkOrderDetailAdapter template6Adapter;
    public static Template8StockOnlyAdapter template8Adapter;
    public static Template9MerchantOrderDetailAdapter template9Adapter;
    public static Template10MerchantOrderDetailAdapter template10Adapter;
    public static int merchantselected = 0;
    public static MerchantDropDownAdapter dropdown;
    double createordergeolat, createordergeolog;
    public static String orderno, pushstatus, productstatus, offlineorderid, followup = "No", endtime,
            startTime, beatMerchants, userBeatModel = "";
    public SQLiteDatabase db;

    String dayOfTheWeek = "";
    GPSTracker gps;
    public String TAG = "CreateOrderActivity";
    private boolean isLastOrdered=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.create_order_screen);
        // By Kumaravel for ProgressBar Showing
        llProgressBar = (LinearLayout) findViewById(R.id.create_order_progress);

        int permissionCheck1 = ContextCompat.checkSelfPermission(CreateOrderActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionCheck1 != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    CreateOrderActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.LOCATION_PERMISSION_REQUEST_CODE);
        } else {
            try {
                if (CheckGpsStatus()) {
                    startLocationUpdates();
                } else {
                    showSettingsAlert();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        netCheck();
        databaseHandler = new DatabaseHandler(getApplicationContext());
        //////////////gps = new GPSTracker(CreateOrderActivity.this);
        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();
        final Cursor cur;
        cur = databaseHandler.getDetails();
        cur.moveToFirst();
        Log.e("Insertion Check",
                cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_username)));
        Log.e("Insertion Check",
                cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_password)));
        Log.e("KEY_paymentStatus",
                cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_paymentStatus)));
        PaymentStatus = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_paymentStatus));
        Constants.USER_TYPE = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_usertype));
        Constants.USER_ID = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_id));
        Constants.USER_TYPE = "S";


        Cursor cur1;
        cur1 = databaseHandler.getUserSetting();
        cur1.moveToFirst();
        Log.e("curCount",".........................."+cur.getCount());
        userBeatModel = cur1.getString(cur1.getColumnIndex(DatabaseHandler.KEY_settingrefvalue));
        Log.e("userBeatModel", userBeatModel);
        textViewAddress = findViewById(R.id.id_create_order_TV_address);
        textViewAddress.setText(Constants.EMPTY);
        //Kumaravel
        Log.e("Constants.TEMPLATE_NO","..................................................."+Constants.TEMPLATE_NO);

        linearLayoutRefresh = (LinearLayout) findViewById(R.id.linearLayoutRefresh);
        textViewAssetMenuRefresh = (TextView) findViewById(R.id.textViewAssetMenuRefresh);


        linearLayoutCreateOrder = (LinearLayout) findViewById(R.id.linearLayoutCreateOrder);
        listViewProducts = (ListView) findViewById(R.id.listViewProducts);
        linearLayoutProfile = (LinearLayout) findViewById(R.id.linearLayoutProfile);
        linearLayoutMyOrders = (LinearLayout) findViewById(R.id.linearLayoutMyOrders);
        linearLayoutMyDealers = (LinearLayout) findViewById(R.id.linearLayoutMyDealers);
        linearLayoutProducts = (LinearLayout) findViewById(R.id.linearLayoutProducts);
        linearLayoutPayment = (LinearLayout) findViewById(R.id.linearLayoutPayment);
        linearLayoutComplaint = (LinearLayout) findViewById(R.id.linearLayoutComplaint);
        linearLayoutSignout = (LinearLayout) findViewById(R.id.linearLayoutSignout);
        linearlayoutSearchIcon = (LinearLayout) findViewById(R.id.linearlayoutSearchIcon);
        linearLayoutClientVisit = (LinearLayout) findViewById(R.id.linearLayoutClientVisit);
        linearLayoutPaymentCollection = (LinearLayout) findViewById(R.id.linearLayoutPaymentCollection);
        linearLayoutDistanceCalculation = (LinearLayout) findViewById(R.id.linearLayoutDistanceCalculation);
        linearLayoutAcknowledge = (LinearLayout) findViewById(R.id.linearLayoutAcknowledge);
        linearLayoutStockEntry = (LinearLayout) findViewById(R.id.linearLayoutStockEntry);
        linearLayoutAddMerchant = (LinearLayout) findViewById(R.id.linearLayoutAddMerchant);

        linearLayoutdownloadscheme = (LinearLayout) findViewById(R.id.linearLayoutdownloadscheme);
        linearLayoutsurveyuser = (LinearLayout) findViewById(R.id.linearLayoutsurveyuser);
        linearLayoutMyDayPlan = (LinearLayout) findViewById(R.id.linearLayoutMyDayPlan);
        linearLayoutMySales = (LinearLayout) findViewById(R.id.linearLayoutMySales);

        linearLayoutStockOnly = (LinearLayout) findViewById(R.id.linearLayoutStockOnly);
        linearLayoutIndicatorStock = findViewById(R.id.id_menu_indicator_stock);
        linearLayoutIndicatorOrder = findViewById(R.id.id_menu_indicator_order);


        linearLayoutStockAudit = findViewById(R.id.linearLayoutStockAudit);
        linearLayoutMigration = (LinearLayout) findViewById(R.id.linearLayoutMigration);

        isStockMenuActive = MyApplication.getInstance().isTemplate8ForTemplate2(); // backward handle

        if(isStockMenuActive){
            linearLayoutStockOnly.setBackgroundResource(R.color.app_menuhg_color);
            linearLayoutCreateOrder.setBackgroundResource(R.color.app_menubg_color);

            linearLayoutIndicatorStock.setVisibility(View.VISIBLE);
            linearLayoutIndicatorOrder.setVisibility(View.INVISIBLE);

        }else {
            linearLayoutIndicatorOrder.setVisibility(View.VISIBLE);
            //linearLayoutIndicatorStock.setVisibility(View.INVISIBLE);
            //linearLayoutStockOnly.setBackgroundResource(R.color.app_menubg_color);
            linearLayoutStockOnly.setVisibility(View.GONE);
            linearLayoutCreateOrder.setBackgroundResource(R.color.app_menuhg_color);
        }

        /*linearLayoutStockOnly.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("Template2","Stock......................................");
                isStockMenuActive = true;
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (Constants.SalesMerchant_Id != null &&
                        !Constants.SalesMerchant_Id.isEmpty() &&
                        merchantselected == 1) {
                    //loadTemplate8();
                } else {
                    showAlertDialogToast("Please select merchant");
                }

            }
        });  */


        buttonAddPlaceOrder = (Button) findViewById(R.id.buttonAddPlaceOrder);
        ButtonNoOrder = (Button) findViewById(R.id.ButtonNoOrder);
      //  ButtonLastOrder = (Button) findViewById(R.id.ButtonLastOrder);
        textViewNodata = (TextView) findViewById(R.id.textViewNodata);
        arraylisttemp = new ArrayList<MerchantOrderDomainSelected>();
        arrayListDealer = new ArrayList<String>();
        arrayListDealerDomain = new ArrayList<MDealerCompDropdownDomain>();
        arrayListmerchant = new ArrayList<MDealerCompDropdownDomain>();
        arrayListSearch = new ArrayList<MDealerCompDropdownDomain>();
        arraylistmerchantdropdown = new ArrayList<MerchantDropdownDomain>();
        searchresult = new ArrayList<MerchantDropdownDomain>();

       /* if (Constants.USER_TYPE.equals("D")) {
            linearLayoutMyDealers.setVisibility(View.GONE);
            linearLayoutProducts.setVisibility(View.VISIBLE);
            linearLayoutCreateOrder.setVisibility(View.GONE);
        } else if (Constants.USER_TYPE.equals("M")){
            linearLayoutMyDealers.setVisibility(View.VISIBLE);
            linearLayoutProducts.setVisibility(View.GONE);
            linearLayoutCreateOrder.setVisibility(View.GONE);
        }else {*/
        linearLayoutMyDealers.setVisibility(View.GONE);
        linearLayoutProducts.setVisibility(View.GONE);
        linearLayoutCreateOrder.setVisibility(View.VISIBLE);
        //}
        /*
        int permissionCheck = ContextCompat.checkSelfPermission(CreateOrderActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    CreateOrderActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.LOCATION_PERMISSION_REQUEST_CODE);
        } else {
            try {
                getcurrentlocation();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }  */

        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date d = new Date();
        dayOfTheWeek = sdf.format(d);
        Log.e("dateday", dayOfTheWeek);

        autocompleteTextTraders = (AutoCompleteTextView) findViewById(R.id.autocompleteTextTraders);

        if (userBeatModel.equalsIgnoreCase("C")) {
            Log.e("userModel C", "userModel C");
            Cursor curr;
            curr = databaseHandler.getDetails();
            curr.moveToFirst();
            String selectedbeat = curr.getString(curr.getColumnIndex(DatabaseHandler.KEY_selectedBeat));
            String selectedDate = curr.getString(curr.getColumnIndex(DatabaseHandler.KEY_selectedDate));
            String todayDate = new SimpleDateFormat(
                    "yyyy-MM-dd").format(new java.util.Date());
            if (todayDate.equals(selectedDate) && !selectedbeat.equals("")) {
                getBeatMerchant(selectedbeat);

            } else {
                Constants.beatassignscreen = "CreateOrder";
                Intent to = new Intent(CreateOrderActivity.this, CompanyBeatActivity.class);
                startActivity(to);
                finish();
            }

            } else {
            Log.e("userModel D", "userModel D");
            offlineBeat();
        }
        Log.e("FromBack","...................size" + previousSelected.size());

        autocompleteTextTraders.setThreshold(1);
        // merchantadpset();

        autocompleteTextTraders.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                autocompleteTextTraders.showDropDown();
                textViewHeader.setVisibility(View.VISIBLE);
                textViewcross_1.setVisibility(View.VISIBLE);
                editTextSearchField.setVisibility(View.GONE);
                textViewCross.setVisibility(View.GONE);
                textViewcross_1.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        autocompleteTextTraders.setText("");
                        Constants.Merchantname = "";
                        Constants.SalesMerchant_Id = "";
                        merchantselected = 0;
                        textViewAddress.setText("");
                        if (autocompleteTextTraders.equals("")) {
                            InputMethodManager iaa = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            iaa.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                        }

                    }
                });

                return false;
            }
        });
        autocompleteTextTraders
                .setOnItemClickListener(new OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        boolean isMerchantChanged = false;
                        boolean isFirstSelect = false;
                        String mnsme = autocompleteTextTraders.getText().toString();
                        if(!Constants.Merchantname.isEmpty()){
                            if(!Constants.Merchantname.equals(mnsme)){
                                MyApplication.previousSelected.clear();// need to clear for new merchant
                                isMerchantChanged = true;
                            }
                        }else {
                            isFirstSelect = true;
                        }

                        startTime = new SimpleDateFormat(
                                "HH:mm:ss").format(new java.util.Date());
                        Constants.startTime = startTime;
                        Log.e("datestart", Constants.startTime);

                        for (MDealerCompDropdownDomain cd : arrayListmerchant) {
                            if (cd.getComName().equals(mnsme)) {
                                muserId = cd.getID();

                                Constants.SalesMerchant_Id = muserId;
                                merchantselected = 1;
                                Constants.OFFLINE_MERCHANT_ID = cd.getOfflineMerchantId();///////this is for new merchant order to online

                                if (merchantselected == 1) {
                                    InputMethodManager iaa = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                    iaa.hideSoftInputFromWindow(linearlayoutSearchIcon.getWindowToken(), 0);

                                    textViewcross_1.setVisibility(View.GONE);

                                }

                                Constants.Merchantname = mnsme;
                                Log.e("Merchant", Constants.Merchantname);
                                Log.e("selected merchant Id", Constants.SalesMerchant_Id);

                                textViewAddress.setText(cd.getaddress());
                              /*  Cursor curs;
                                curs = databaseHandler.getSetingProduct(Constants.SalesMerchant_Id);
                                Log.e("seleted Products", String.valueOf(curs.getCount()));
                                String newproducts="";
                                if(curs!=null && curs.getCount() > 0) {

                                    if (curs.moveToLast()) {
                                         newproducts = curs.getString(1);
                                        Log.e("newproducts", newproducts);
                                        loadingSettingProducts(newproducts);
                                    }
                                }else{
                                    //showAlertDialogToast("No Products Available");
                                    loadingOfflineProducts();
                                }
*/
                            }
                        }

                        // Added By Kumaravel for Template2
                        if(isMerchantChanged || isFirstSelect) {

                            MyApplication.previousSelectedForSearch.clear();

                            if(MyApplication.getInstance().isTemplate8ForTemplate2()){
                                loadTemplate8();
                            }else {

                                if(MyApplication.getInstance().isTemplate1()) {
                                    CreateOrderActivity.this.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            template1Adapter = new Template1MerchantOrderDetailAdapter(
                                                    CreateOrderActivity.this,
                                                    R.layout.adapter_template1,
                                                    arraylistMerchantOrderDetailList, netCheck());
                                            listViewProducts.setAdapter(template1Adapter);
                                        }
                                    });
                                }

                                if (MyApplication.getInstance().isTemplate2()) {
                                    Log.e("TouchListener", ".......isTemplate2....." + arraylistMerchantOrderDetailList.size());
                                    if (Constants.SalesMerchant_Id != null &&
                                            !Constants.SalesMerchant_Id.isEmpty() &&
                                            merchantselected == 1) {
                                            template2Load();
                                    } else {
                                        Log.e("TouchListener", "............" + arraylistMerchantOrderDetailList.size());
                                    }
                                }
                                if (MyApplication.getInstance().isTemplate3()) {
                                    Log.e("TouchListener", ".....isTemplate3......." + arraylistMerchantOrderDetailList.size());
                                    if (Constants.SalesMerchant_Id != null &&
                                            !Constants.SalesMerchant_Id.isEmpty() &&
                                            merchantselected == 1) {
                                        template3Load();
                                    } else {
                                        Log.e("TouchListener", "............" + arraylistMerchantOrderDetailList.size());
                                    }
                                }

                                if (MyApplication.getInstance().isTemplate4()) {
                                    Log.e("TouchListener", ".....isTemplate3......." + arraylistMerchantOrderDetailList.size());
                                    if (Constants.SalesMerchant_Id != null &&
                                            !Constants.SalesMerchant_Id.isEmpty() &&
                                            merchantselected == 1) {
                                        template4Load();
                                    } else {
                                        Log.e("TouchListener", "............" + arraylistMerchantOrderDetailList.size());
                                    }
                                }

                                if (MyApplication.getInstance().isTemplate9()) {
                                    Log.e("TouchListener", ".....isTemplate9......." + arraylistMerchantOrderDetailList.size());
                                    if (Constants.SalesMerchant_Id != null &&
                                            !Constants.SalesMerchant_Id.isEmpty() &&
                                            merchantselected == 1) {
                                        template9Load();
                                    } else {
                                        Log.e("TouchListener", "............" + arraylistMerchantOrderDetailList.size());
                                    }
                                }

                                if (MyApplication.getInstance().isTemplate10()) {
                                    Log.e("TouchListener", ".....isTemplate10......." + arraylistMerchantOrderDetailList.size());
                                    if (Constants.SalesMerchant_Id != null &&
                                            !Constants.SalesMerchant_Id.isEmpty() &&
                                            merchantselected == 1) {
                                        template10Load();
                                    } else {
                                        Log.e("TouchListener", "............" + arraylistMerchantOrderDetailList.size());
                                    }
                                }
                                /*
                                if (MyApplication.getInstance().isTemplate8()) {
                                    Log.e("TouchListener", ".....isTemplate8......." + arraylistMerchantOrderDetailList.size());
                                    if (Constants.SalesMerchant_Id != null &&
                                            !Constants.SalesMerchant_Id.isEmpty() &&
                                            merchantselected == 1) {
                                        template8Adapter = new Template8StockOnlyAdapter(
                                                CreateOrderActivity.this,
                                                R.layout.adapter_template8_stock_only,
                                                arraylistMerchantOrderDetailList, netCheck());
                                        listViewProducts.setAdapter(template8Adapter);
                                    } else {
                                        Log.e("TouchListener", "............" + arraylistMerchantOrderDetailList.size());
                                    }
                                }  */
                            }

                        }else {
                            Log.e("Merchant","...............NotChanged");
                        }

                    }
                });

        menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
        textViewAssetSearch = (TextView) findViewById(R.id.textViewAssetSearch);
        textViewHeader = (TextView) findViewById(R.id.textViewHeader);

        textViewAssetMenuCreateOrder = (TextView) findViewById(R.id.textViewAssetMenuCreateOrder);
        textViewAssetMenuProfile = (TextView) findViewById(R.id.textViewAssetMenuProfile);
        textViewAssetMenuMyOrders = (TextView) findViewById(R.id.textViewAssetMenuMyOrders);
        textViewAssetMenuMyDealers = (TextView) findViewById(R.id.textViewAssetMenuMyDealers);
        textViewAssetMenuProducts = (TextView) findViewById(R.id.textViewAssetMenuProducts);
        textViewAssetMenuPayment = (TextView) findViewById(R.id.textViewAssetMenuPayment);
        textViewAssetMenuComplaint = (TextView) findViewById(R.id.textViewAssetMenuComplaint);
        textViewAssetMenuSignout = (TextView) findViewById(R.id.textViewAssetMenuSignout);
        textViewAssetMenuClientVisit = (TextView) findViewById(R.id.textViewAssetMenuClientVisit);
        textViewAssetMenuAddMerchant = (TextView) findViewById(R.id.textViewAssetMenuAddMerchant);
        textViewAssetMenuAcknowledge = (TextView) findViewById(R.id.textViewAssetMenuAcknowledge);
        textViewAssetMenuStockEntry = (TextView) findViewById(R.id.textViewAssetMenuStockEntry);
        textViewAssetMenudownloadscheme = (TextView) findViewById(R.id.textViewAssetMenudownloadscheme);
        textViewAssetMenusurveyuser = (TextView) findViewById(R.id.textViewAssetMenusurveyuser);
        textViewAssetMenuMyDayPlan = (TextView) findViewById(R.id.textViewAssetMenuMyDayPlan);
        textViewAssetMenuMySales = (TextView) findViewById(R.id.textViewAssetMenuMySales);
        textViewAssetMenuPaymentCollection = (TextView) findViewById(R.id.textViewAssetMenuPaymentCollection);
        textViewAssetMenuDistanceCalculation = (TextView) findViewById(R.id.textViewAssetMenuDistanceCalculation);
        linearLayoutMenuParent = (LinearLayout) findViewById(R.id.linearLayoutMenuParent);

        textViewCross = (TextView) findViewById(R.id.textViewCross);
        textViewcross_1 = (TextView) findViewById(R.id.textViewcross_1);
        editTextSearchField = (EditText) findViewById(R.id.editTextSearchField);

        Typeface font = Typeface.createFromAsset(getAssets(),
                "fontawesome-webfont.ttf");
        menuIcon.setTypeface(font);
        textViewAssetMenuCreateOrder.setTypeface(font);
        textViewAssetSearch.setTypeface(font);
        textViewAssetMenuProfile.setTypeface(font);
        textViewAssetMenuMyOrders.setTypeface(font);
        textViewAssetMenuMyDealers.setTypeface(font);
        textViewAssetMenuProducts.setTypeface(font);
        textViewAssetMenuPayment.setTypeface(font);
        textViewAssetMenuComplaint.setTypeface(font);
        textViewAssetMenuSignout.setTypeface(font);
        textViewAssetMenuClientVisit.setTypeface(font);
        textViewCross.setTypeface(font);
        textViewcross_1.setTypeface(font);
        textViewAssetMenuPaymentCollection.setTypeface(font);
        textViewAssetMenuRefresh.setTypeface(font);
        textViewAssetMenuDistanceCalculation.setTypeface(font);
        textViewAssetMenuAcknowledge.setTypeface(font);
        textViewAssetMenuStockEntry.setTypeface(font);
        textViewAssetMenuAddMerchant.setTypeface(font);
        textViewAssetMenusurveyuser.setTypeface(font);
        textViewAssetMenudownloadscheme.setTypeface(font);
        textViewAssetMenuMyDayPlan.setTypeface(font);
        textViewAssetMenuMySales.setTypeface(font);

        linearLayoutDistStock = (LinearLayout) findViewById(R.id.linearLayoutDistStock);
        textViewAssetMenuDistStock = (TextView) findViewById(R.id.textViewAssetMenuDistStock);
        textViewAssetMenuDistStock.setTypeface(font);
        textViewAssetMenuStockOnly = findViewById(R.id.textViewAssetMenuStockOnly);
        textViewAssetMenuStockOnly.setTypeface(font);

        textViewAssetMenuStockAudit = (TextView) findViewById(R.id.textViewAssetMenuStockAudit);
        textViewAssetMenuMigration = findViewById(R.id.textViewAssetMenuMigration);

        textViewAssetMenuStockAudit.setTypeface(font);
        textViewAssetMenuMigration.setTypeface(font);

        if(MyApplication.getInstance().isTemplate8ForTemplate2()){
            ButtonNoOrder.setVisibility(View.GONE);
            textViewHeader.setText(getResources().getString(R.string.closing_stock));
        }
        if(MyApplication.getInstance().isTemplate10() || MyApplication.getInstance().isTemplate9()) {
            ButtonNoOrder.setVisibility(View.GONE);
        }
        ButtonNoOrder.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!Constants.Merchantname.equals("")) {
                    if(mLastLocation != null) {
                        updateCurrentLocation(mLastLocation);
                        followup();
                    }else {
                        noOrderLocationUpdateWithDelay();
                    }
                } else {
                    showAlertDialogToast("Please select merchant");
                }


            }
        });
      /*  ButtonLastOrder.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Constants.checkproduct = 1;
                if (merchantselected == 1) {
                    int offlineID = 1;
                    Log.e("CreateOrderActivty", "ButtonLastOrder: " + muserId);
                    Cursor curs = databaseHandler.getOfflineOrderID(muserId);
                    if (curs != null && curs.getCount() > 0) {
                        if (curs.moveToLast()) {
                            offlineID = curs.getInt(0);
                        }
                        curs.close();
                    }
                    getOrderDetails(offlineID);

                } else {
                    showAlertDialogToast("Please choose the Merchant");
                }

            }
        });*/
        buttonAddPlaceOrder.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                /*Check the last order */
                Constants.checkproduct = 1;
                Date curDate = new Date();
                try{
                    CURRENT_MILLIS = curDate.getTime();
                }catch (Exception e){
                    e.printStackTrace();
                    CURRENT_MILLIS = 100;///////
                }

                if (merchantselected == 1) {
                    //Kumaravel some circumstance Merchant Name Not Appear In Order So I Verify Here Also
                    if(Constants.Merchantname.isEmpty()){
                        Constants.Merchantname = autocompleteTextTraders.getText().toString();
                        Log.e("MerchantName","...............How Happen This...");
                    }
                    if(Constants.Merchantname.isEmpty()){
                        showAlertDialogToast("Merchant Name Not Able To Pick");
                        return;
                    }
                    //kumaravel
                    // this all method should under merchant == 1
                    if(isStockMenuActive) {
                        MyApplication.getInstance().setTemplate8ForTemplate2(true);
                        CreateOrderActivity.arraylistMerchantOrderDetailListSelected.clear();
                        CreateOrderActivity.arraylistMerchantOrderDetailListSelected.
                                addAll(new MerchantOrderSelectedConvert(getApplicationContext(), template8Adapter.getChangedUOM(), true)
                                        .getMerchantSelectedFromTemplate8(template8Adapter.getSelectedMerchantOrders(), CreateOrderActivity.arraylistMerchantOrderDetailList));
                    }else {
                        if (MyApplication.getInstance().isTemplate1()) {

                            if (!template1Adapter.stock_quantity_pairCheck()) {
                                showDialog(CreateOrderActivity.this, TEMPLATE1_PAIR_MISSING_TITLE, TEMPLATE1_PAIR_MISSING_MESSAGE);
                                return;
                            }

                            CreateOrderActivity.arraylistMerchantOrderDetailListSelected.clear();
                            CreateOrderActivity.arraylistMerchantOrderDetailListSelected.
                                    addAll(new MerchantOrderSelectedConvert(getApplicationContext())
                                            .getMerchantSelectedFromTemplate1(template1Adapter.getSelectedMerchantOrders(), CreateOrderActivity.arraylistMerchantOrderDetailList));
                        }

                        if (MyApplication.getInstance().isTemplate2()) {
                            /// This Template2 using some common template1 functions

                            if (!template2Adapter.stock_quantity_pairCheck()) {
                                showDialog(CreateOrderActivity.this, TEMPLATE1_PAIR_MISSING_TITLE, TEMPLATE1_PAIR_MISSING_MESSAGE);
                                return;
                            }
                            CreateOrderActivity.arraylistMerchantOrderDetailListSelected.clear();
                            CreateOrderActivity.arraylistMerchantOrderDetailListSelected.
                                    addAll(new MerchantOrderSelectedConvert(getApplicationContext(), template2Adapter.getChangedUOM(), true)
                                            .getMerchantSelectedFromTemplate1(template2Adapter.getSelectedMerchantOrders(), CreateOrderActivity.arraylistMerchantOrderDetailList));
                        }

                        if (MyApplication.getInstance().isTemplate3()) {
                            /// This Template2 using some common template1 functions
                            if (!template3Adapter.price_quantity_pairCheck()) {
                                showDialog(CreateOrderActivity.this, TEMPLATE1_PAIR_MISSING_TITLE, TEMPLATE3_PAIR_MISSING_MESSAGE);
                                return;
                            }

                            CreateOrderActivity.arraylistMerchantOrderDetailListSelected.clear();
                            CreateOrderActivity.arraylistMerchantOrderDetailListSelected.
                                    addAll(new MerchantOrderSelectedTemplate3Convert(getApplicationContext(), template3Adapter.getChangedUOM(), true)
                                            .getMerchantSelectedFromTemplate3(template3Adapter.getSelectedMerchantOrders(), CreateOrderActivity.arraylistMerchantOrderDetailList));
                        }

                        if (MyApplication.getInstance().isTemplate4()) {
                            /// This Template2 using some common template1 functions
                            if (!template4Adapter.price_quantity_pairCheck()) {
                                showDialog(CreateOrderActivity.this, TEMPLATE1_PAIR_MISSING_TITLE, TEMPLATE3_PAIR_MISSING_MESSAGE);
                                return;
                            }

                            CreateOrderActivity.arraylistMerchantOrderDetailListSelected.clear();
                            CreateOrderActivity.arraylistMerchantOrderDetailListSelected.
                                    addAll(new MerchantOrderSelectedTemplate4Convert(getApplicationContext(), template4Adapter.getChangedUOM(), true)
                                            .getMerchantSelectedFromTemplate4(template4Adapter.getSelectedMerchantOrders(), CreateOrderActivity.arraylistMerchantOrderDetailList));
                        }

                        if (MyApplication.getInstance().isTemplate9()) {

                            CreateOrderActivity.arraylistMerchantOrderDetailListSelected.clear();
                            CreateOrderActivity.arraylistMerchantOrderDetailListSelected.
                                    addAll(new MerchantOrderSelectedTemplate9Convert(getApplicationContext(), template9Adapter.getChangedUOM(), true)
                                            .getMerchantSelectedFromTemplate9(template9Adapter.getSelectedMerchantOrders(), CreateOrderActivity.arraylistMerchantOrderDetailList));
                        }

                        if (MyApplication.getInstance().isTemplate10()) {
                            /// This Template2 using some common template1 functions
                            if (!template10Adapter.price_quantity_pairCheck()) {
                                showDialog(CreateOrderActivity.this, TEMPLATE1_PAIR_MISSING_TITLE, TEMPLATE10_PAIR_MISSING_MESSAGE);
                                return;
                            }

                            CreateOrderActivity.arraylistMerchantOrderDetailListSelected.clear();
                            CreateOrderActivity.arraylistMerchantOrderDetailListSelected.
                                    addAll(new MerchantOrderSelectedTemplate3Convert(getApplicationContext(), template10Adapter.getChangedUOM(), true)
                                            .getMerchantSelectedFromTemplate3(template10Adapter.getSelectedMerchantOrders(), CreateOrderActivity.arraylistMerchantOrderDetailList));
                        }


                        if (Constants.TEMPLATE_NO.equals(MILK_TEMPLATE_NO)) {
                            if (!template6Adapter.requirementCheck()) {
                                showDialog(CreateOrderActivity.this, TEMPLATE1_PAIR_MISSING_TITLE, TEMPLATE3_PAIR_MISSING_MESSAGE);
                                return;
                            }
                            CreateOrderActivity.arraylistMerchantOrderDetailListSelected.clear();
                            CreateOrderActivity.arraylistMerchantOrderDetailListSelected.
                                    addAll(new MerchantOrderSelectedTemplate6Convert(getApplicationContext()).getMerchantSelectedFromTemplate6
                                            (template6Adapter.getSelectedMerchantOrders(), CreateOrderActivity.arraylistMerchantOrderDetailList));
                        }
                    }


                        size = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size();
                        Log.e("size", String.valueOf(size));
                        if (size == 0) {
                            showAlertDialogToast("Atleast select one product");

                        } else {
                            if (netCheck()) {
                                if(!Constants.TEMPLATE_NO.equals(MILK_TEMPLATE_NO)) {
                                    Log.e("placeorder", ".........................................." + "netConnected");
                                    getDetails("placeorder");
                                }else {
                                    showDairyPopup();
                                }
                            } else {
                                Log.e("placeorder",".........................................."+ "notConnected");
                                if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                                    showAlertDialogToast("Please make payment before place order");
                                } else {
                                    if(!Constants.TEMPLATE_NO.equals(MILK_TEMPLATE_NO)) {
                                        Constants.imagesetcheckout = 1;
                                        Intent to = new Intent(CreateOrderActivity.this,
                                                SalesManOrderCheckoutActivity.class);
                                        to.putExtra("MERCHANT_NAME",Constants.Merchantname);
                                        to.putExtra(Constants.EXTRA_LOCATION, mLastLocation);
                                        startActivity(to);
                                        finish();
                                    }else {
                                        showDairyPopup();
                                    }
                                }
                            }

                        }
                } else {
                    showAlertDialogToast("Please choose the Merchant");
                }

            }
        });



        linearlayoutSearchIcon.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub
                if (searchclick == "0") {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    textViewHeader.setVisibility(TextView.GONE);
                    editTextSearchField.setVisibility(EditText.VISIBLE);
                    editTextSearchField.setFocusable(true);
                    textViewcross_1.setVisibility(View.GONE);
                    editTextSearchField.requestFocus();
                    textViewCross.setVisibility(TextView.VISIBLE);
                    InputMethodManager iaa = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    iaa.hideSoftInputFromWindow(linearlayoutSearchIcon.getWindowToken(), 0);
                    InputMethodManager iss = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    iss.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                    Log.e("Keyboard", "Show");
                    searchclick = "1";
                } else {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    textViewHeader.setVisibility(TextView.VISIBLE);
                    editTextSearchField.setVisibility(EditText.GONE);
                    textViewCross.setVisibility(TextView.GONE);
                    textViewcross_1.setVisibility(View.GONE);
                    /*InputMethodManager iss = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    iss.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);*/
                    InputMethodManager iss = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    iss.hideSoftInputFromWindow(linearlayoutSearchIcon.getWindowToken(), 0);
                    searchclick = "0";
                }

            }
        });

        editTextSearchField.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

                arraylistMerchantOrderDetailList = new ArrayList<MerchantOrderDetailDomain>();
                String searchText = editTextSearchField.getText().toString().toLowerCase(Locale.getDefault());
                /*if(editTextSearchField.getText().toString().equals("")){
                    merchantselected=0;
                }*/

                for (MerchantOrderDetailDomain pd : arrayListSearchResults) {
                    if (pd.getprodname().toLowerCase(Locale.getDefault()).contains(searchText) || pd.getprodcode().toLowerCase(Locale.getDefault()).contains(searchText)) {

                        arraylistMerchantOrderDetailList.add(pd);
                    }
                }

                ///////////////////
                /////arraylistMerchantOrderDetailList.addAll(CreateOrderActivity.arraylistMerchantOrderDetailListSelected);

                ////////////////////

                if(!MyApplication.getInstance().isTemplate8ForTemplate2()) {

                    if (MyApplication.getInstance().isTemplate5()) {
                        CreateOrderActivity.this.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                adapter = new MerchantOrderDetailAdapter(
                                        CreateOrderActivity.this,
                                        R.layout.item_order_details_new,
                                        arraylistMerchantOrderDetailList, netCheck());
                                listViewProducts.setAdapter(adapter);
                            }
                        });
                    }


                    //// Kumaravel Start Here On 13-12-2018

                    if (MyApplication.getInstance().isTemplate1()) {

                        CreateOrderActivity.arraylistMerchantOrderDetailListSelected.clear();
                        CreateOrderActivity.arraylistMerchantOrderDetailListSelected.
                                addAll(new MerchantOrderSelectedConvert(getApplicationContext()).
                                        getMerchantSelectedFromTemplate2ForSearch(template1Adapter.getSelectedMerchantOrders(),
                                                CreateOrderActivity.arraylistMerchantOrderDetailList,
                                                CreateOrderActivity.arrayListSearchResults));

                        CreateOrderActivity.this.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                template1Adapter = new Template1MerchantOrderDetailAdapter(
                                        CreateOrderActivity.this,
                                        R.layout.adapter_template1,
                                        arraylistMerchantOrderDetailList, netCheck());
                                listViewProducts.setAdapter(template1Adapter);
                            }
                        });
                    }

                    if (MyApplication.getInstance().isTemplate6()) {

                        CreateOrderActivity.this.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                template6Adapter = new Template6MilkOrderDetailAdapter(
                                        CreateOrderActivity.this,
                                        R.layout.adapter_template6,
                                        arraylistMerchantOrderDetailList, netCheck());
                                listViewProducts.setAdapter(template6Adapter);
                            }
                        });
                    }

                    // Template2 Showing
                    if (MyApplication.getInstance().isTemplate2()) {
                        if (!Constants.SalesMerchant_Id.isEmpty() &&
                                Constants.SalesMerchant_Id != null &&
                                merchantselected == 1) {

                            CreateOrderActivity.arraylistMerchantOrderDetailListSelected.clear();
                            CreateOrderActivity.arraylistMerchantOrderDetailListSelected.
                                    addAll(new MerchantOrderSelectedConvert(getApplicationContext(),
                                            template2Adapter.getChangedUOM(), true).
                                            getMerchantSelectedFromTemplate2ForSearch(template2Adapter.getSelectedMerchantOrders(),
                                                    CreateOrderActivity.arraylistMerchantOrderDetailList,
                                                    CreateOrderActivity.arrayListSearchResults));


                            template2Load();
                        } else {
                            showAlertDialogToast("Please choose the Merchant");
                            return;
                        }
                    }

                    if (MyApplication.getInstance().isTemplate3()) {
                        if (!Constants.SalesMerchant_Id.isEmpty() &&
                                Constants.SalesMerchant_Id != null &&
                                merchantselected == 1) {
                            /////IS_CLEAR_PREVIOUS = false;
                            //if(template3Adapter.getSelectedMerchantOrders().size() > 0 ) {
                            Log.e("previousCount",".........................previousLoadedOrderDetailList............"+
                                    CreateOrderActivity.previousLoadedOrderDetailList.size());
                            CreateOrderActivity.arraylistMerchantOrderDetailListSelected.clear();
                            CreateOrderActivity.arraylistMerchantOrderDetailListSelected.
                                    addAll(new MerchantOrderSelectedTemplate3Convert(getApplicationContext(),
                                            template3Adapter.getChangedUOM(), true).
                                            getMerchantSelectedFromTemplate3ForSearch(template3Adapter.getSelectedMerchantOrders(),
                                                    CreateOrderActivity.arraylistMerchantOrderDetailList,
                                                    CreateOrderActivity.arrayListSearchResults));

                            //}
                            template3Load();
                            //CreateOrderActivity.previousLoadedOrderDetailList.addAll(CreateOrderActivity.arraylistMerchantOrderDetailList);
                        } else {
                            showAlertDialogToast("Please choose the Merchant");
                            return;
                        }
                    }
                    if (MyApplication.getInstance().isTemplate4()) {
                        if (!Constants.SalesMerchant_Id.isEmpty() &&
                                Constants.SalesMerchant_Id != null &&
                                merchantselected == 1) {

                            Log.e("previousCount",".........................previousLoadedOrderDetailList............"
                                    +CreateOrderActivity.previousLoadedOrderDetailList.size());
                            CreateOrderActivity.arraylistMerchantOrderDetailListSelected.clear();
                            CreateOrderActivity.arraylistMerchantOrderDetailListSelected.
                                    addAll(new MerchantOrderSelectedTemplate4Convert(getApplicationContext(),
                                            template4Adapter.getChangedUOM(), true).
                                            getMerchantSelectedFromTemplate4ForSearch(template4Adapter.getSelectedMerchantOrders(),
                                                    CreateOrderActivity.arraylistMerchantOrderDetailList,
                                                    CreateOrderActivity.arrayListSearchResults));

                            template4Load();
                        } else {
                            showAlertDialogToast("Please choose the Merchant");
                            return;
                        }
                    }

                    if (MyApplication.getInstance().isTemplate9()) {
                        if (!Constants.SalesMerchant_Id.isEmpty() &&
                                Constants.SalesMerchant_Id != null &&
                                merchantselected == 1) {

                            Log.e("previousCount",".........................previousLoadedOrderDetailList............"
                                    +CreateOrderActivity.previousLoadedOrderDetailList.size());
                            CreateOrderActivity.arraylistMerchantOrderDetailListSelected.clear();
                            CreateOrderActivity.arraylistMerchantOrderDetailListSelected.
                                    addAll(new MerchantOrderSelectedTemplate9Convert(getApplicationContext(),
                                            template9Adapter.getChangedUOM(), true).
                                            getMerchantSelectedFromTemplate9ForSearch(template9Adapter.getSelectedMerchantOrders(),
                                                    CreateOrderActivity.arraylistMerchantOrderDetailList,
                                                    CreateOrderActivity.arrayListSearchResults));

                            template9Load();
                        } else {
                            showAlertDialogToast("Please choose the Merchant");
                            return;
                        }
                    }

                    if (MyApplication.getInstance().isTemplate10()) {
                        if (!Constants.SalesMerchant_Id.isEmpty() &&
                                Constants.SalesMerchant_Id != null &&
                                merchantselected == 1) {
                            Log.e("previousCount",".........................previousLoadedOrderDetailList............"+
                                    CreateOrderActivity.previousLoadedOrderDetailList.size());
                            CreateOrderActivity.arraylistMerchantOrderDetailListSelected.clear();
                            CreateOrderActivity.arraylistMerchantOrderDetailListSelected.
                                    addAll(new MerchantOrderSelectedTemplate3Convert(getApplicationContext(),
                                            template10Adapter.getChangedUOM(), true).
                                            getMerchantSelectedFromTemplate3ForSearch(template10Adapter.getSelectedMerchantOrders(),
                                                    CreateOrderActivity.arraylistMerchantOrderDetailList,
                                                    CreateOrderActivity.arrayListSearchResults));

                            //}
                            template10Load();
                            //CreateOrderActivity.previousLoadedOrderDetailList.addAll(CreateOrderActivity.arraylistMerchantOrderDetailList);
                        } else {
                            showAlertDialogToast("Please choose the Merchant");
                            return;
                        }
                    }
                    /*
                    if (MyApplication.getInstance().isTemplate8()) {
                        CreateOrderActivity.this.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                template8Adapter = new Template8StockOnlyAdapter(
                                        CreateOrderActivity.this,
                                        R.layout.adapter_template8_stock_only,
                                        arraylistMerchantOrderDetailList, netCheck());
                                listViewProducts.setAdapter(template8Adapter);
                            }
                        });
                    }  */
                }else {

                    CreateOrderActivity.arraylistMerchantOrderDetailListSelected.clear();
                    CreateOrderActivity.arraylistMerchantOrderDetailListSelected.
                            addAll(new MerchantOrderSelectedConvert(getApplicationContext(),
                                    template8Adapter.getChangedUOM(),
                                    true)
                                    .getMerchantSelectedFromTemplate2ForSearch(template8Adapter.getSelectedMerchantOrders(),
                                            CreateOrderActivity.arraylistMerchantOrderDetailList,
                                            CreateOrderActivity.arrayListSearchResults));

                    loadTemplate8();
                }

            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {

            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                textViewCross.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        editTextSearchField.setText("");

                        if (editTextSearchField.equals("")) {
                            InputMethodManager iss = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            iss.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                        }
                    }
                });
            }
        });

        menuIcon.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (menuCliccked == 0) {
                    linearLayoutMenuParent.setVisibility(View.VISIBLE);

                    editTextSearchField.setVisibility(EditText.GONE);
                    textViewHeader.setVisibility(TextView.VISIBLE);
                    textViewCross.setVisibility(TextView.GONE);
                    textViewcross_1.setVisibility(View.GONE);
                    InputMethodManager iss = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    iss.hideSoftInputFromWindow(menuIcon.getWindowToken(), 0);
                    menuCliccked = 1;
                } else {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    textViewHeader.setVisibility(TextView.VISIBLE);
                    editTextSearchField.setVisibility(EditText.GONE);
                    textViewCross.setVisibility(TextView.GONE);
                    InputMethodManager iss = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    iss.hideSoftInputFromWindow(menuIcon.getWindowToken(), 0);
                    menuCliccked = 0;
                }

            }
        });


        linearLayoutMenuParent.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);

                menuCliccked = 0;
            }
        });

        linearLayoutCreateOrder.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;

                    MyApplication.getInstance().setTemplate8ForTemplate2(false);
                    isStockMenuActive = false;
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    merchantselected = 0;
                    getDetails("Createorder");

            }
        });

        linearLayoutProfile.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub

                if (netCheck() == true) {
                    MyApplication.getInstance().setTemplate8ForTemplate2(false);
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    merchantselected = 0;
                    getDetails("profile");
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });

        linearLayoutMyOrders.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;

                if (netCheck() == true) {
                    MyApplication.getInstance().setTemplate8ForTemplate2(false);
                    Constants.orderstatus = "Success";
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    merchantselected = 0;
                    getDetails("myorders");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Constants.orderstatus = "Success";
                        Constants.Merchantname = "";
                        Constants.SalesMerchant_Id = "";
                        merchantselected = 0;
                        MyApplication.getInstance().setTemplate8ForTemplate2(false);

                        Intent io = new Intent(CreateOrderActivity.this,
                                SalesManOrderActivity.class);
                        io.putExtra("Key","menuclick");
                        Constants.setimage = null;
                        startActivity(io);
                        finish();
                        /*if (Constants.USER_TYPE.equals("D")) {
                            Intent io = new Intent(CreateOrderActivity.this,
                                    DealersOrderActivity.class);
                            io.putExtra("Key","menuclick");
                            startActivity(io);
                            finish();
                        } else if (Constants.USER_TYPE.equals("M")) {
                            Intent io = new Intent(CreateOrderActivity.this,
                                    MerchantOrderActivity.class);
                            io.putExtra("Key","menuclick");
                            Constants.setimage = null;
                            startActivity(io);
                            finish();
                        } else {
                            Intent io = new Intent(CreateOrderActivity.this,
                                    SalesManOrderActivity.class);
                            io.putExtra("Key","menuclick");
                            Constants.setimage = null;
                            startActivity(io);
                            finish();
                        }  */
                    }
                }


            }
        });

        linearLayoutRefresh.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("Clicked", "Clicked");

                if (netCheck()) {

                    final Dialog qtyDialog = new Dialog(CreateOrderActivity.this);
                    qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    qtyDialog.setContentView(R.layout.warning_dialog);

                    final Button buttonUpdateOk, buttonUpdateCancel;

                    final TextView textQtyValidate;

                    buttonUpdateOk = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateOk);
                    buttonUpdateCancel = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateCancel);

                    textQtyValidate = (TextView) qtyDialog
                            .findViewById(R.id.textQtyValidate);
                    qtyDialog.show();


                    buttonUpdateOk.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            MyApplication.getInstance().setTemplate8ForTemplate2(false);
                            databaseHandler.synTableDelete();
                            Constants.refreshscreen = "createorder";
                            Intent io = new Intent(CreateOrderActivity.this, RefreshActivity.class);
                            startActivity(io);
                            finish();
                            qtyDialog.dismiss();
                        }

                    });

                    buttonUpdateCancel
                            .setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub

                                    qtyDialog.dismiss();

                                }
                            });

                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }
            }

        });


        linearLayoutMyDealers.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheck()) {
                    MyApplication.getInstance().setTemplate8ForTemplate2(false);
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    merchantselected = 0;
                    getDetails("mydealer");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Constants.Merchantname = "";
                        Constants.SalesMerchant_Id = "";
                        merchantselected = 0;
                        MyApplication.getInstance().setTemplate8ForTemplate2(false);

                        Intent io = new Intent(CreateOrderActivity.this,
                                MyDealersActivity.class);
                        io.putExtra("Key", "menuclick");
                        startActivity(io);
                        finish();

                    }
                }

            }
        });


        linearLayoutComplaint.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheck() == true) {
                    MyApplication.getInstance().setTemplate8ForTemplate2(false);
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    merchantselected = 0;
                    getDetails("postnotes");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        MyApplication.getInstance().setTemplate8ForTemplate2(false);
                        Constants.Merchantname = "";
                        Constants.SalesMerchant_Id = "";
                        merchantselected = 0;
                        Intent io = new Intent(CreateOrderActivity.this,
                                MerchantComplaintActivity.class);
                        startActivity(io);
                        finish();
                        /*if (Constants.USER_TYPE.equals("D")) {
                            Intent io = new Intent(CreateOrderActivity.this,
                                    DealersComplaintActivity.class);
                            startActivity(io);
                            finish();

                        } else {
                            Intent io = new Intent(CreateOrderActivity.this,
                                    MerchantComplaintActivity.class);
                            startActivity(io);
                            finish();

                        }  */
                    }
                }

            }
        });
        linearLayoutDistanceCalculation.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                    MyApplication.getInstance().setTemplate8ForTemplate2(false);
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    merchantselected = 0;
                    MyApplication.getInstance().setTemplate8ForTemplate2(false);
                    Intent to = new Intent(CreateOrderActivity.this,
                            DistanceCalActivity.class);
                    startActivity(to);

            }
        });
        linearLayoutClientVisit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;

                if (netCheck() == true) {
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    MyApplication.getInstance().setTemplate8ForTemplate2(false);
                    merchantselected = 0;
                    getDetails("clientvisit");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Constants.Merchantname = "";
                        Constants.SalesMerchant_Id = "";
                        merchantselected = 0;
                        MyApplication.getInstance().setTemplate8ForTemplate2(false);
                        Intent io = new Intent(CreateOrderActivity.this,
                                SMClientVisitHistory.class);
                        startActivity(io);
                        finish();
                        /*if (Constants.USER_TYPE.equals("D")) {
                            Intent io = new Intent(CreateOrderActivity.this,
                                    DealerClientVisit.class);
                            startActivity(io);
                            finish();
                        } else {
                            Intent io = new Intent(CreateOrderActivity.this,
                                    SMClientVisitHistory.class);
                            startActivity(io);
                            finish();
                        }  */
                    }
                }

            }
        });

        linearLayoutPaymentCollection.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck() == true) {
                    MyApplication.getInstance().setTemplate8ForTemplate2(false);
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    merchantselected = 0;
                    getDetails("paymentcoll");
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }


            }
        });

        linearLayoutAcknowledge.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck() == true) {
                    MyApplication.getInstance().setTemplate8ForTemplate2(false);
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    merchantselected = 0;
                    getDetails("acknowledge");
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }


            }
        });
        linearLayoutStockEntry.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;

                if (netCheck() == true) {
                    MyApplication.getInstance().setTemplate8ForTemplate2(false);
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    merchantselected = 0;
                    getDetails("stockentry");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment");
                    }else {
                        MyApplication.getInstance().setTemplate8ForTemplate2(false);
                        Constants.Merchantname = "";
                        Constants.SalesMerchant_Id = "";
                        merchantselected = 0;
                        Intent io = new Intent(CreateOrderActivity.this,
                                OutletStockEntry.class);
                        startActivity(io);
                        finish();
                    }
                }


            }
        });

        linearLayoutDistStock.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;

               if (netCheck() == true) {
                   MyApplication.getInstance().setTemplate8ForTemplate2(false);
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    merchantselected = 0;
                    getDetails("diststock");
                } else {
                   if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                       showAlertDialogToast("Please make payment");
                   } else {
                       MyApplication.getInstance().setTemplate8ForTemplate2(false);
                       Constants.Merchantname = "";
                       Constants.SalesMerchant_Id = "";
                       merchantselected = 0;
                       Intent io = new Intent(CreateOrderActivity.this,
                               DistributrStockActivity.class);
                       startActivity(io);
                       finish();
                   }
                }


            }
        });

        linearLayoutAddMerchant.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;

                if (netCheck() == true) {
                    MyApplication.getInstance().setTemplate8ForTemplate2(false);
                    Constants.orderstatus = "Success";
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    merchantselected = 0;
                    getDetails("addmerchant");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        MyApplication.getInstance().setTemplate8ForTemplate2(false);
                        Constants.orderstatus = "Success";
                        Constants.Merchantname = "";
                        Constants.SalesMerchant_Id = "";
                        merchantselected = 0;

                        Intent io = new Intent(CreateOrderActivity.this,
                                AddMerchantNew.class);
                        startActivity(io);
                        finish();
                    }
                }


            }
        });
        linearLayoutdownloadscheme.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck()) {

                    final Dialog qtyDialog = new Dialog(CreateOrderActivity.this);
                    qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    qtyDialog.setContentView(R.layout.warning_dialog);

                    final Button buttonUpdateOk, buttonUpdateCancel;

                    final TextView textQtyValidate;

                    buttonUpdateOk = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateOk);
                    buttonUpdateCancel = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateCancel);

                    textQtyValidate = (TextView) qtyDialog
                            .findViewById(R.id.textQtyValidate);
                    qtyDialog.show();


                    buttonUpdateOk.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            databaseHandler.deletescheme();
                            Constants.downloadScheme = "createorder";
                            MyApplication.getInstance().setTemplate8ForTemplate2(false);
                            Intent io = new Intent(CreateOrderActivity.this, SchemeDownload.class);
                            startActivity(io);
                            finish();
                            qtyDialog.dismiss();
                        }

                    });

                    buttonUpdateCancel
                            .setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub

                                    qtyDialog.dismiss();

                                }
                            });

                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });
        linearLayoutsurveyuser.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;

                if (netCheck()) {
                    MyApplication.getInstance().setTemplate8ForTemplate2(false);
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    merchantselected = 0;
                    getDetails("surveyuser");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        MyApplication.getInstance().setTemplate8ForTemplate2(false);
                        Constants.Merchantname = "";
                        Constants.SalesMerchant_Id = "";
                        merchantselected = 0;

                        Intent to = new Intent(CreateOrderActivity.this, SurveyMerchant.class);
                        startActivity(to);
                        finish();
                    }
                }


            }
        });
        linearLayoutMyDayPlan.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                MyApplication.getInstance().setTemplate8ForTemplate2(false);
                if (netCheck()) {
                    getDetails("mydayplan");
                } else {
                    Intent into = new Intent(CreateOrderActivity.this,
                            MyDayPlan.class);
                    startActivity(into);
                    finish();
                }



            }
        });



        linearLayoutMySales.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck() == true) {
                    MyApplication.getInstance().setTemplate8ForTemplate2(false);
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    getDetails("mysales");
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }

              /*  Intent to = new Intent(SalesManOrderActivity.this,
                        SalesmanAcknowledgeActivity.class);
                startActivity(to);
                finish();*/

            }
        });

        linearLayoutSignout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                // TODO Auto-generated method stub
                Constants.Merchantname = "";
                Constants.SalesMerchant_Id = "";
                merchantselected = 0;
               // merchantselected=0;
                MyApplication.getInstance().setTemplate8ForTemplate2(false);
                databaseHandler.delete();
                Intent io = new Intent(CreateOrderActivity.this,
                        SigninActivity.class);
                startActivity(io);
                finish();
            }
        });

        linearLayoutMigration.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                // TODO Auto-generated method stub
                Constants.Merchantname = "";
                Constants.SalesMerchant_Id = "";
                merchantselected = 0;
                Intent io = new Intent(CreateOrderActivity.this,
                        PendingDataMigrationActivity.class);
                startActivity(io);
            }
        });

        linearLayoutStockOnly.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Constants.checkproduct = 0;
                Constants.orderid = "";
                Constants.SalesMerchant_Id = "";
                Constants.Merchantname = "";

                MyApplication.getInstance().setTemplate8ForTemplate2(true);
                if (netCheck()) {
                    Intent io = new Intent(CreateOrderActivity.this,
                            ClosingStockDashBoardActivity.class);
                    startActivity(io);
                    finish();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals("") || PaymentStatus.equalsIgnoreCase("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(CreateOrderActivity.this,
                                ClosingStockDashBoardActivity.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });

        linearLayoutStockAudit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().setTemplate8ForTemplate2(false);
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Constants.checkproduct = 0;
                Constants.orderid = "";
                Constants.SalesMerchant_Id = "";
                Constants.Merchantname = "";
                if (netCheck()) {
                    Intent io = new Intent(CreateOrderActivity.this,
                            ClosingStockAudit.class);
                    startActivity(io);
                    finish();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals("") || PaymentStatus.equalsIgnoreCase("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(CreateOrderActivity.this,
                                ClosingStockAudit.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });

        /////////////////////////
        showMenu();
        ///////////////////////
    }

    public void getcurrentlocation() throws IOException {

        if (gps.canGetLocation() == true) {

               /* createordergeolat = gps.getLatitude();
                    createordergeolog = gps.getLongitude();

                    		Log.e("lat", String.valueOf(createordergeolat));
            			Log.e("log", String.valueOf(createordergeolog));
*/

            Intent intent = new Intent(this, GPSTrackerActivity.class);
            startActivityForResult(intent, 1);

        } else {
            showSettingsAlert();

        }

    }

    public void getBeatMerchant(String beatname) {
        Cursor cursor;
        cursor = databaseHandler.getbeatMerchant(beatname);
        Log.e("beatname", beatname);
        Log.e("beatname count", String.valueOf(cursor.getCount()));
        String merchants = "";
        int i = 0;
        if (cursor != null && cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {

                do {
                    if (i == 0) {
                        if (cursor.getString(0).equals("New User")) {
                            merchants = "'" + cursor.getString(0) + "'";
                        } else {
                            merchants = cursor.getString(0);
                        }
                        i++;
                    } else {
                        if (cursor.getString(0).equals("New User")) {
                            merchants = merchants + ", '" + cursor.getString(0) + "'";
                        } else {
                            merchants = merchants + "," + cursor.getString(0);
                        }
                    }

                    Log.e("merchants", merchants);
                } while (cursor.moveToNext());


            }
        }

        offlinemerchant(merchants);
    }


    @TargetApi(23)
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permission, @NonNull int[] grantResults) {

        Log.e("requestcode", String.valueOf(requestCode));
        Log.e("size", String.valueOf(grantResults.length));

        if (requestCode == Constants.LOCATION_PERMISSION_REQUEST_CODE) {
            Log.e("requestcode", String.valueOf(grantResults[0]));
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent io = new Intent(CreateOrderActivity.this, CreateOrderActivity.class);
                startActivity(io);
                finish();
            } else {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.LOCATION_PERMISSION_REQUEST_CODE);
            }


        }
    }

    public void followup() {
        final Dialog qtyDialog = new Dialog(CreateOrderActivity.this);
        qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        qtyDialog.setContentView(R.layout.followup_activity);

        final Button buttonUpdateOk, buttonUpdateCancel;
        final RadioGroup radioGroupAnswer;
        final EditText editTextDate, editTextRemarks;
        final RadioButton radioButtoYes, radioButtonNo;
        final LinearLayout linearLayoutEdit;
        buttonUpdateOk = (Button) qtyDialog
                .findViewById(R.id.buttonUpdateOk);
        buttonUpdateCancel = (Button) qtyDialog
                .findViewById(R.id.buttonUpdateCancel);
        radioGroupAnswer = (RadioGroup) qtyDialog.findViewById(R.id.radioGroupAnswer);
        radioButtoYes = (RadioButton) qtyDialog.findViewById(R.id.radioButtoYes);
        radioButtonNo = (RadioButton) qtyDialog.findViewById(R.id.radioButtonNo);
        editTextDate = (EditText) qtyDialog.findViewById(R.id.editTextDate);
        editTextRemarks = (EditText) qtyDialog.findViewById(R.id.editTextRemarks);
        linearLayoutEdit = (LinearLayout) qtyDialog.findViewById(R.id.linearLayoutEdit);

        qtyDialog.show();

        radioButtoYes.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                radioButtoYes.setChecked(true);
                radioButtonNo.setChecked(false);
                followup = "Yes";
                Calendar cal = Calendar.getInstance();
                Intent intent = new Intent(Intent.ACTION_EDIT);
                intent.setType("vnd.android.cursor.item/event");
                intent.putExtra("beginTime", cal.getTimeInMillis());
                intent.putExtra("allDay", true);
                intent.putExtra("endTime", cal.getTimeInMillis() + 60 * 60 * 1000);
                intent.putExtra("title", "");
                intent.putExtra("description", "");
                startActivity(intent);
                Log.e("afterfollowup", "followup");


            }
        });

        radioButtonNo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                radioButtonNo.setChecked(true);
                radioButtoYes.setChecked(false);
                followup = "No";

            }
        });

        buttonUpdateOk.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                qtyDialog.dismiss();
                endtime = new SimpleDateFormat(
                        "HH:mm:ss").format(new java.util.Date());
                Log.e("endtime", endtime);
                saveOfflineHeader();


            }

        });

        buttonUpdateCancel
                .setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub

                        qtyDialog.dismiss();

                    }
                });

    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(CreateOrderActivity.this);

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.delete);

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, 2);

            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("inside", String.valueOf(resultCode));
        if (resultCode == 0) {
            switch (requestCode) {
                case 1:
                    Log.e("inside", "inside");
                    Intent io = new Intent(CreateOrderActivity.this, CreateOrderActivity.class);
                    startActivity(io);
                    finish();
                    break;
            }
        }
        if (requestCode == 1) {
            try {
                Bundle extras = data.getExtras();
                createordergeolog = extras.getDouble("Longitude");
                Log.e("createordergeolog", String.valueOf(createordergeolog));
                createordergeolat = extras.getDouble("Latitude");
                Log.e("createordergeolat", String.valueOf(createordergeolat));
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        if (requestCode == 2) {
            Intent intent = new Intent(this, GPSTrackerActivity.class);
            startActivityForResult(intent, 1);
        }


    }

    public void saveOfflineHeader() {

        String inputFormat1 = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss").format(new java.util.Date());
        Log.e("inputFormat", inputFormat1);


        String inputPattern = "ddMMyyhhmmss";

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        String dt = inputFormat.format(new java.util.Date());


        Log.e("dt", dt);
        Log.e("geolat", String.valueOf(createordergeolat));
        Log.e("geolat", String.valueOf(createordergeolat));
        Log.e("Constants.USER_ID", String.valueOf(Constants.USER_ID));
        Log.e("Constants.startTime", Constants.startTime);
        Log.e("Constants.endtime", endtime);


        databaseHandler.addorderheader(Constants.USER_ID, Constants.SalesMerchant_Id, Constants.Merchantname, "", "No Order", inputFormat1, "0",
                Constants.USER_ID + dt, "", createordergeolat, createordergeolog, Constants.startTime, endtime, followup);


        Cursor curs;
        curs = databaseHandler.getNoOrders();
        Log.e("orderHeaderCount", String.valueOf(curs.getCount()));

        if (curs != null && curs.getCount() > 0) {

            if (curs.moveToLast()) {
                orderno = curs.getString(0);
                pushstatus = curs.getString(4);
                String date = curs.getString(5);
                Log.e("onlineorderno", curs.getString(6));
                Log.e("orderno", orderno);
                Log.e("Hdate", inputFormat1);


                saveofflineDetail(orderno);
            }
        }

    }

    public void saveofflineDetail(String orderno) {
        Log.e("orde number", String.valueOf(orderno));
        productstatus = "No Order";

        String inputFormat = new SimpleDateFormat(
                "yyyy-MM-dd").format(new java.util.Date());
        db = getApplicationContext().openOrCreateDatabase("freshorders", 0, null);
        Cursor curs1;
        curs1 = db.rawQuery("SELECT * from dealertable", null);
        Log.e("dEALER", String.valueOf(curs1.getCount()));
        if (curs1.getCount() > 0) {
            curs1.moveToFirst();
            selected_dealer = curs1.getString(curs1
                    .getColumnIndex(DatabaseHandler.KEY_Duserid));
        }

        databaseHandler.addorderdetail(orderno, selected_dealer, "", "0", "1", "", productstatus, "", "", "",
                inputFormat, "", "", 0.0, "", "", "null", "null", "null", "", "", "", 0.0, "", "", "");

        Cursor curs;
        curs = databaseHandler.getorderdetail(orderno);
        curs = databaseHandler.getorderdetail(orderno);

        Log.e("detailcount", String.valueOf(curs.getCount()));

        if (netCheck() == true) {
            convertjson();

        } else {

            showAlertDialogToast1("No Order Successfully placed");///Kumaravel Changed
        }

    }

    public void convertjson() {
        final JSONArray resultSet = new JSONArray();


        Cursor curs;
        curs = databaseHandler.getNoOrders();
        Log.e("HeaderCount", String.valueOf(curs.getCount()));
        curs.moveToFirst();
        while (curs.isAfterLast() == false) {

            int totalColumn = curs.getColumnCount();
            JSONObject rowObject = new JSONObject();

            for (int i = 0; i < totalColumn; i++) {
                if (curs.getColumnName(i) != null) {

                    try {

                        if (curs.getString(i) != null) {
                            if (!curs.getString(i).equals("New User")) {
                                Log.d("TAG_NAME", curs.getString(i));
                                rowObject.put(curs.getColumnName(i), curs.getString(i));
                            }

                        } else {
                            if (!curs.getColumnName(i).equals("muserid")) {
                                rowObject.put(curs.getColumnName(i), "");
                            }

                        }
                    } catch (Exception e) {
                        Log.d("TAG_NAME", e.getMessage());
                    }
                }

            }

            try {
                //Kumaravel 13-07-2019
                if(Constants.distributorname == null || Constants.distributorname.isEmpty()){
                    Constants.distributorname = new PrefManager(CreateOrderActivity.this).getStringDataByKey(PrefManager.KEY_DISTRIBUTOR_NAME);
                }
                rowObject.put("distributorname",Constants.distributorname);
            } catch (Exception e) {
                Log.d("Exception in Dist. name", e.getMessage());
            }


            try {
                db = getApplicationContext().openOrCreateDatabase("freshorders", 0, null);

                Cursor curs1;
                curs1 = db.rawQuery("SELECT * from dealertable", null);
                Log.e("dEALER", String.valueOf(curs1.getCount()));
                if (curs1.getCount() > 0) {
                    curs1.moveToFirst();
                    selected_dealer = curs1.getString(curs1
                            .getColumnIndex(DatabaseHandler.KEY_Duserid));
                }

                Cursor cursor2;
                Constants.Merchantname = rowObject.getString("mname");

                cursor2 = db.rawQuery("SELECT * from merchanttable where companyname =" + "'" + Constants.Merchantname + "'", null);
                Log.e("HeaderCountCanled", String.valueOf(cursor2.getCount()));

                cursor2.moveToFirst();
                while (cursor2.isAfterLast() == false) {

                    int Columncount = cursor2.getColumnCount();
                    Log.e("merchantColumn", String.valueOf(cursor2.getColumnCount()));
                    JSONObject merchantdetail = new JSONObject();
                    for (int j = 0; j < Columncount; j++) {
                        if (cursor2.getColumnName(j) != null) {
                            try {

                                if (cursor2.getString(j) != null) {
                                    if (!cursor2.getString(j).equals("New User")) {

                                        merchantdetail.put(cursor2.getColumnName(j), cursor2.getString(j));

                                    }
                                } else {
                                    if (!cursor2.getColumnName(j).equals("userid")) {
                                        merchantdetail.put(cursor2.getColumnName(j), "");
                                    }

                                }


                            } catch (Exception e) {
                                Log.d("Merchant_Exception", e.getMessage());
                            }
                        }

                    }
                    merchantdetail.put("usertype", "M");
                    merchantdetail.put("duserid", selected_dealer);
                    rowObject.put("merchant", merchantdetail);
                    cursor2.moveToNext();
                }

                cursor2.close();

                Log.e("Detailarray", rowObject.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }


            Log.e("jsonarray", rowObject.toString());

            JSONArray resultDetail = new JSONArray();
            try {

                Cursor cursor;
                cursor = databaseHandler.getorderdetail(rowObject.getString("oflnordid"));
                Log.e("DetailCount", String.valueOf(cursor.getCount()));
                cursor.moveToFirst();
                while (cursor.isAfterLast() == false) {

                    int Columncount = cursor.getColumnCount();
                    Log.e("columnCount", String.valueOf(cursor.getColumnCount()));
                    JSONObject detailObject = new JSONObject();

                    for (int i = 0; i < 21; i++) // SK TO CHECK
                    {
                        if (cursor.getColumnName(i) != null) {

                            try {

                                if (cursor.getString(i) != null) {
                                    Log.d("TAG_NAME", cursor.getString(i));
                                    detailObject.put(cursor.getColumnName(i), cursor.getString(i));
                                } else {
                                    detailObject.put(cursor.getColumnName(i), "");
                                }
                            } catch (Exception e) {
                                Log.d("TAG_NAME", e.getMessage());
                            }
                        }

                    }

                    resultDetail.put(detailObject);
                    cursor.moveToNext();
                }

                cursor.close();

                Log.e("Detailarray", resultDetail.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                rowObject.put("orderdtls", resultDetail);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            resultSet.put(rowObject);
            curs.moveToNext();
        }
        curs.close();


        Log.e("SaveArray", resultSet.toString());


        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "", strOrderId = "";
            JSONObject jsonObject = new JSONObject();

            @Override
            protected void onPreExecute() {
                dialog = ProgressDialog.show(CreateOrderActivity.this, "",
                        "Loading...", true, true);
            }

            @Override
            protected void onPostExecute(Void result) {

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);

                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {
                        strOrderId = JsonAccountObject.getJSONObject("data").getString("ordid");
                        Log.e("strOrderId", strOrderId);
                        String muserid = JsonAccountObject.getJSONObject("data").getString("muserid");
                        Log.e("muserid", muserid);

                        String updatedFlag = jsonObject.getJSONObject("merchant").getString("cflag");

                        Log.e("updatedFlag", updatedFlag);
                        ContentValues contentValues1 = new ContentValues();
                        contentValues1.put(DatabaseHandler.KEY_Mcompanyname, jsonObject.getString("mname"));
                        contentValues1.put(DatabaseHandler.KEY_Muserid, muserid);
                        contentValues1.put(DatabaseHandler.KEY_MFlag, updatedFlag);
                        databaseHandler.updateMerchant(contentValues1);
                        Log.e("updateMerchant", String.valueOf(contentValues1));
                        Cursor curs1;
                        curs1 = databaseHandler.getmerchantDetails(muserid);
                        Log.e("Outlet/Merchant Count", String.valueOf(curs1.getCount()));

                        if (curs1 != null && curs1.getCount() > 0) {

                            if (curs1.moveToFirst()) {
                                Log.e("merchant name", curs1.getString(0));
                                Log.e("merchant id", curs1.getString(1));
                                merchantRowid = curs1.getString(0);
                            }
                        }
                        databaseHandler.updateBeatMercahant(muserid, merchantRowid);
                        Cursor curs2;
                        curs2 = databaseHandler.getbeat();
                        Log.e("countbeat ", String.valueOf(curs2.getCount()));

                        beatupdate();
                        CreateOrderActivity.arraylistMerchantOrderDetailListSelected.clear();

                        db.execSQL("DELETE FROM oredrdetail where ( ordstatus = 'No Order' ) AND ( oflnordid = " + OfflineOrderNo + " ) ");
                        db.execSQL("DELETE FROM orderheader where ( pushstatus = 'No Order' ) AND ( oflnordid = " + OfflineOrderNo + " ) ");
                        Cursor curs;
                        curs = databaseHandler.getNoOrders();
                        Log.e("Exist Count", String.valueOf(curs.getCount()));


                        //showAlertDialogToast1(strMsg);
                        showAlertDialogToast1("No Order Successfully placed");


                    }
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    merchantselected = 0;

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {

                }

                dialog.dismiss();
            }

            @Override
            protected Void doInBackground(Void... params) {

                try {

                    for (int k = 0; k < resultSet.length(); k++) {
                        jsonObject = resultSet.getJSONObject(k);
                        Log.e("jsonObject11111", String.valueOf(jsonObject));
                        OfflineOrderNo = jsonObject.getString("oflnordid");
                        JsonServiceHandler = new JsonServiceHandler(Utils.strsavemerchantOderdetail, jsonObject.toString(), CreateOrderActivity.this);
                        JsonAccountObject = JsonServiceHandler.ServiceData();

                    }
                } catch (JSONException e) {

                    e.printStackTrace();
                }
                return null;

            }
        }.execute(new Void[]{});


    }

    public void beatupdate() {

        Cursor cursor;
        cursor = db.rawQuery("SELECT * from beat where flag ='Inactive'", null);
        Log.e("beatCount", String.valueOf(cursor.getCount()));
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            final JSONArray beatarray = new JSONArray();
            while (cursor.isAfterLast() == false) {

                int Columncount = cursor.getColumnCount();
                JSONObject beatobject = new JSONObject();


                for (int i = 0; i < 15; i++) {
                    if (cursor.getColumnName(i) != null) {

                        try {

                            if (cursor.getString(i) != null) {
                                Log.d("TAG_NAME", cursor.getString(i));
                                beatobject.put(cursor.getColumnName(i), cursor.getString(i));
                            } else {
                                beatobject.put(cursor.getColumnName(i), "");

                            }
                        } catch (Exception e) {
                            Log.d("TAG_NAME", e.getMessage());
                        }
                    }

                }


                beatarray.put(beatobject);

                cursor.moveToNext();
            }

            cursor.close();

            Log.e("Detailarray", beatarray.toString());

            new AsyncTask<Void, Void, Void>() {
                String strStatus = "";
                String strMsg = "";

                @Override
                protected void onPreExecute() {

                }

                @Override
                protected void onPostExecute(Void result) {

                }

                @Override
                protected Void doInBackground(Void... params) {
                    JSONObject jsonObject = new JSONObject();
                    try {

                        for (int k = 0; k < beatarray.length(); k++) {
                            jsonObject = beatarray.getJSONObject(k);
                            beatrowid = jsonObject.getString("rowid");
                            JsonServiceHandler = new JsonServiceHandler(Utils.strsavebeat, jsonObject.toString(), getApplicationContext());
                            JsonAccountObject = JsonServiceHandler.ServiceData();

                            try {

                                strStatus = JsonAccountObject.getString("status");
                                Log.e("return status", strStatus);
                                strMsg = JsonAccountObject.getString("message");
                                Log.e("return message", strMsg);

                                if (strStatus.equals("true")) {

                                    db.execSQL("UPDATE beat SET flag = 'Active' WHERE rowid =" + "'" + beatrowid + "'");
                                    if (k == (beatarray.length() - 1)) {

                                    }
                                } else {

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (Exception e) {

                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return null;

                }
            }.execute(new Void[]{});

        } else {

        }
    }

    private void getDetails(final String paymentcheck) {
        boolean isClosing = false;
        isClosing = MyApplication.getInstance().isTemplate8ForTemplate2();
        MyApplication.getInstance().setTemplate8ForTemplate2(false);
        if (paymentcheck.equals("profile")) {
            Intent io = new Intent(CreateOrderActivity.this,
                    ProfileActivity.class);
            startActivity(io);
            finish();

        } else if (paymentcheck.equals("myorders")) {
            Intent io = new Intent(CreateOrderActivity.this,
                    SalesManOrderActivity.class);
            startActivity(io);
            finish();

        } else if (paymentcheck.equals("mydealer")) {
            Intent io = new Intent(CreateOrderActivity.this,
                    MyDealersActivity.class);
            io.putExtra("Key", "menuclick");
            startActivity(io);
            finish();

        } else if (paymentcheck.equals("postnotes")) {
            Intent io = new Intent(CreateOrderActivity.this,
                    MerchantComplaintActivity.class);
            startActivity(io);
            finish();

        } else if (paymentcheck.equals("placeorder")) {
            if(isClosing){
                MyApplication.getInstance().setTemplate8ForTemplate2(true);
            }
            Constants.imagesetcheckout = 1;
            Intent to = new Intent(CreateOrderActivity.this,
                    SalesManOrderCheckoutActivity.class);
            to.putExtra("MERCHANT_NAME",Constants.Merchantname);
            to.putExtra(Constants.EXTRA_LOCATION, mLastLocation);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("clientvisit")) {
            Intent to = new Intent(CreateOrderActivity.this,
                    SMClientVisitHistory.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("paymentcoll")) {
            Intent to = new Intent(CreateOrderActivity.this,
                    SalesmanPaymentCollectionActivity.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("addmerchant")) {
            Intent to = new Intent(CreateOrderActivity.this,
                    AddMerchantNew.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("acknowledge")) {
            Intent to = new Intent(CreateOrderActivity.this,
                    SalesmanAcknowledgeActivity.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("stockentry")) {
            Intent to = new Intent(CreateOrderActivity.this,
                    OutletStockEntry.class);
            startActivity(to);
            finish();
        }
        else if (paymentcheck.equals("Distance Calculator")) {
            Intent to = new Intent(CreateOrderActivity.this,
                    DistanceCalActivity.class);
            startActivity(to);
            finish();
        }
        else if (paymentcheck.equals("mydayplan")) {
            Intent to = new Intent(CreateOrderActivity.this,
                    MyDayPlan.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("mysales")) {
            Intent to = new Intent(CreateOrderActivity.this,
                    MySalesActivity.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("diststock")) {
            Intent io = new Intent(CreateOrderActivity.this,
                    DistributrStockActivity.class);
            startActivity(io);
            finish();
        }else if(paymentcheck.equals("Createorder")){
            Intent to = new Intent(CreateOrderActivity.this,
                    CreateOrderActivity.class);
            startActivity(to);
            finish();
        }
    }

    public void loadingOfflineProducts() {
        arraylistMerchantOrderDetailListSelected = new ArrayList<MerchantOrderDomainSelected>();
        arraylistMerchantOrderDetailList = new ArrayList<MerchantOrderDetailDomain>();
        arrayListSearchResults = new ArrayList<MerchantOrderDetailDomain>();

        previousLoadedOrderDetailList = new ArrayList<>();

        productIdPositionMap = new LinkedHashMap<>();

        Cursor curs;
        curs = databaseHandler.getProduct();
        //curs.moveToFirst();
        Log.e("productCount", String.valueOf(curs.getCount()));
        Log.e("Columncount", String.valueOf(curs.getColumnCount()));

        if (curs != null && curs.getCount() > 0) {
            if (curs.moveToFirst()) {

                do {
                    Log.e("inside", "inside");
                    MerchantOrderDetailDomain dod = new MerchantOrderDetailDomain();
                    dod.setDuserid(String.valueOf(curs.getInt(1))); //(curs.getColumnIndex(DatabaseHandler.KEY_userid)))
                    dod.setCompanyname(curs.getString(2)); //(curs.getColumnIndex(DatabaseHandler.KEY_companyname))
                    dod.setprodcode(curs.getString(3)); //(curs.getColumnIndex(DatabaseHandler.KEY_prodcode))
                    dod.setprodid(curs.getString(4)); //String.valueOf(curs.getInt(curs.getColumnIndex(DatabaseHandler.KEY_prodid)))
                    dod.setprodname(curs.getString(5)); //curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_prodname))
                    dod.setqty(curs.getString(6)); //curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_quantity))
                    dod.setFreeQty(curs.getString(7)); //curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_freeqty))
                    dod.setDate(curs.getString(8)); //curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_updateddt))
                    //dod.setFreeQty(curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_currentdate)));
                    dod.setprodprice(curs.getString(10));
                    dod.setprodtax(curs.getString(11));
                    dod.setserialno(String.valueOf(q + 1));
                    dod.setBatchno(curs.getString(12));
                    dod.setExpdate(curs.getString(14));
                    dod.setMgfDate(curs.getString(13));
                    dod.setUOM(curs.getString(15));
                    dod.setSelectedUOM(curs.getString(15));
                    dod.setselectedfreeUOM("");
                    dod.setUnitGram(curs.getString(17));
                    dod.setUnitPerUom(curs.getString(18));
                    dod.setProdimage(curs.getString(19));
                    dod.setSelecetdQtyUomUnit(curs.getString(18));
                    dod.setSelecetdFreeQtyUomUnit(curs.getString(18));
                    dod.setDeliverymode("");
                    dod.setCurrStock(curs.getString(20));

                    Log.e("id", curs.getString(0));
                    Log.e("product name", curs.getString(5));
                    Log.e("price", curs.getString(10));
                    Log.e("tax", curs.getString(11));
                    Log.e("unitgram", curs.getString(17));
                    Log.e("unitperUOM", curs.getString(18));

                    arraylistMerchantOrderDetailList.add(dod);
                    arrayListSearchResults.add(dod);
                    previousLoadedOrderDetailList.add(dod);

                    productIdPositionMap.put(curs.getString(4), q); /// for mapping purpose

                    q++;
                } while (curs.moveToNext());


            }
            Log.e("outside", "outside");
        }
        Log.e("size", String.valueOf(arraylistMerchantOrderDetailList.size()));


        setadap();
    }


    public void loadingSettingProducts(String prodid) {
        arraylistMerchantOrderDetailListSelected = new ArrayList<MerchantOrderDomainSelected>();
        arraylistMerchantOrderDetailList = new ArrayList<MerchantOrderDetailDomain>();
        arrayListSearchResults = new ArrayList<MerchantOrderDetailDomain>();
        previousLoadedOrderDetailList = new ArrayList<>();

        Cursor curs;
        curs = databaseHandler.getFilterProduct(prodid);
        //curs.moveToFirst();
        Log.e("SelectedproductCount", String.valueOf(curs.getCount()));

        if (curs != null && curs.getCount() > 0) {
            if (curs.moveToFirst()) {

                do {
                    Log.e("inside", "inside");
                    MerchantOrderDetailDomain dod = new MerchantOrderDetailDomain();
                    dod.setDuserid(String.valueOf(curs.getInt(1))); //(curs.getColumnIndex(DatabaseHandler.KEY_userid)))
                    dod.setCompanyname(curs.getString(2)); //(curs.getColumnIndex(DatabaseHandler.KEY_companyname))
                    dod.setprodcode(curs.getString(3)); //(curs.getColumnIndex(DatabaseHandler.KEY_prodcode))
                    dod.setprodid(curs.getString(4)); //String.valueOf(curs.getInt(curs.getColumnIndex(DatabaseHandler.KEY_prodid)))
                    dod.setprodname(curs.getString(5)); //curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_prodname))
                    dod.setqty(curs.getString(6)); //curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_quantity))
                    dod.setFreeQty(curs.getString(7)); //curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_freeqty))
                    dod.setDate(curs.getString(8)); //curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_updateddt))
                    //dod.setFreeQty(curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_currentdate)));
                    dod.setprodprice(curs.getString(10));
                    dod.setprodtax(curs.getString(11));
                    dod.setserialno(String.valueOf(q + 1));
                    dod.setBatchno(curs.getString(12));
                    dod.setExpdate(curs.getString(14));
                    dod.setMgfDate(curs.getString(13));
                    dod.setUOM(curs.getString(15));

                    Log.e("id", curs.getString(0));
                    Log.e("userid", curs.getString(1));
                    Log.e("cmpnyname", curs.getString(2));
                    Log.e("productcode", curs.getString(3));
                    Log.e("product id", curs.getString(4));
                    Log.e("product name", curs.getString(5));
                    Log.e("qty", curs.getString(6));
                    Log.e("fqty", curs.getString(7));
                    Log.e("date", curs.getString(8));
                    Log.e("price", curs.getString(10));
                    Log.e("tax", curs.getString(11));

                    arraylistMerchantOrderDetailList.add(dod);
                    arrayListSearchResults.add(dod);
                    previousLoadedOrderDetailList.add(dod);
                    q++;
                } while (curs.moveToNext());


            }
            Log.e("outside", "outside");
        }
        Log.e("size", String.valueOf(arraylistMerchantOrderDetailList.size()));


        setadap();
    }

    private void loadingMerchant() {
        new AsyncTask<Void, Void, Void>() {
            String cityName;
            ProgressDialog dialog;

            @Override
            protected void onPreExecute() {
                dialog = ProgressDialog.show(CreateOrderActivity.this,
                        "", "Loading...", true, true);
            }

            @Override
            protected void onPostExecute(Void result) {
                try {
                    MDealerCompDropdownDomain cd;

                    JSONArray JsonArray = JsonAccountObject.getJSONArray("data");
                    for (int i = 0; i < JsonArray.length(); i++) {
                        cd = new MDealerCompDropdownDomain();
                        JsonAccountObject = JsonArray.getJSONObject(i);
                        cd.setName(JsonAccountObject.getString("fullname"));
                        cd.setComName(JsonAccountObject.getString("companyname"));
                        cd.setID(JsonAccountObject.getString("userid"));
                        arrayListDealerDomain.add(cd);
                        arrayListSearch.add(cd);
                        arrayListDealer.add(JsonAccountObject.getString("companyname"));
                        arrayListmerchant.add(cd);
                        dialog.dismiss();
                    }

                } catch (JSONException e) {
                    Log.e("JSONException", e.toString());

                } catch (Exception e) {
                    Log.e("Exception", e.toString());

                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                        CreateOrderActivity.this, R.layout.textview, R.id.textView,
                        arrayListDealer);
                autocompleteTextTraders.setAdapter(adapter);
                autocompleteTextTraders.setText(Constants.Merchantname);
                if (!Constants.Merchantname.equals("")) {
                    merchantselected = 1;
                }


                Log.e("arraylist", Constants.Merchantname);
                //myOrders();
            }

            @Override
            protected Void doInBackground(Void... params) {
                JSONObject jsonObject = new JSONObject();

                try {
                    jsonObject.put("userid", Constants.USER_ID);
                    jsonObject.put("usertype", Constants.USER_TYPE);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                JsonServiceHandler = new JsonServiceHandler(Utils.strGetMerchantList, jsonObject.toString(),
                        CreateOrderActivity.this);
                JsonAccountObject = JsonServiceHandler.ServiceData();
                return null;
            }
        }.execute(new Void[]{});
    }


    private void myOrders() {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog1;
            String strStatus = "";


            @Override
            protected void onPreExecute() {
                dialog1 = ProgressDialog.show(CreateOrderActivity.this,
                        "", "Loading...", true, true);

            }

            @Override
            protected void onPostExecute(Void result) {

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {

                        JSONArray jsonArr = JsonAccountObject
                                .getJSONArray("data");
                        arraylistMerchantOrderDetailList = new ArrayList<MerchantOrderDetailDomain>();
                        arrayListSearchResults = new ArrayList<MerchantOrderDetailDomain>();
                        JSONObject job;
                        for (int i = 0; i < jsonArr.length(); i++) {
                            job = new JSONObject();
                            job = jsonArr.getJSONObject(i);

                            JSONArray jsonArr2 = job.getJSONArray("products");
                            JSONObject job1;


                            for (int j = 0; j < jsonArr2.length(); j++) {
                                MerchantOrderDetailDomain dod = new MerchantOrderDetailDomain();
                                job1 = new JSONObject();
                                job1 = jsonArr2.getJSONObject(j);
                                try {
                                    dod.setDuserid(job.getJSONObject("D").getString("userid"));
                                    dod.setCompanyname(job.getJSONObject("D").getString("companyname"));
                                    dod.setusrdlrid(job1.getString("userid"));
                                    dod.setprodcode(job1.getString("prodcode"));
                                    dod.setprodid(job1.getString("prodid"));
                                    String[] date1 = job1.getString("updateddt")
                                            .split("T");
                                    dod.setDate(date1[0]);
                                    dod.setqty("");
                                    dod.setFreeQty("");
                                    dod.setDeliverymode("");
                                    dod.setprodname(job1.getString("prodname"));
                                   // dod.setprodprice(job1.getString("prodprice"));
                                    dod.setprodprice("");

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                arraylistMerchantOrderDetailList.add(dod);
                                arrayListSearchResults.add(dod);

                                Log.e("size", String.valueOf(arraylistMerchantOrderDetailList.size()));
                                Log.e("inside", "inside");
                                dialog1.dismiss();

                            }
                            if (String.valueOf(arraylistMerchantOrderDetailList.size()).equals("0")) {
                                listViewProducts.setVisibility(View.GONE);
                                textViewNodata.setVisibility(View.VISIBLE);
                                textViewNodata.setText("No product(s) found for this dealer");
                                linearlayoutSearchIcon.setClickable(false);
                                textViewAssetSearch.setVisibility(TextView.VISIBLE);
                                dialog1.dismiss();
                                Log.e("size", String.valueOf(arraylistMerchantOrderDetailList.size()));
                            }


                        }


                    } else {
                        //toastDisplay(strMsg);
                        listViewProducts.setVisibility(View.GONE);
                        textViewNodata.setVisibility(View.VISIBLE);
                        textViewNodata.setText(strMsg);
                        linearlayoutSearchIcon.setClickable(false);
                        textViewAssetSearch.setVisibility(TextView.VISIBLE);
                        dialog1.dismiss();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {

                }

            }

            @Override
            protected Void doInBackground(Void... params) {


                JSONObject jsonObject = new JSONObject();

                JsonServiceHandler = new JsonServiceHandler(
                        Utils.strmerchantOderdetail + Constants.USER_ID,
                        CreateOrderActivity.this);
                JsonAccountObject = JsonServiceHandler.ServiceDataGet();
                return null;

            }
        }.execute(new Void[]{});
    }

    // by Kumaravel 18-12-2018 for template2
    public boolean loadingOfflineUOM(){
        Cursor cur = null;
        try{
            int size = arraylistMerchantOrderDetailList.size();
            DatabaseHandler databaseHandler = new DatabaseHandler(this);
            for(int j = 0; j < size; j++){
                MerchantOrderDetailDomain item = arraylistMerchantOrderDetailList.get(j);
                Log.e("UOM",",,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`"+item.getUOM());
                String prodiduom = item.getprodid();
                cur = databaseHandler.getUOMforproduct(prodiduom);
                UOMCount.add(cur.getCount());
                Log.e("multipleUomCount","UOMCount.."+UOMCount);
                String[] array = new String[cur.getCount()];
                cur.moveToFirst();
                //8-9-2017
                for(int i=0;i<cur.getCount();i++)
                {
                    //array[0] = item.getUOM();
                    array[i] = cur.getString(cur.getColumnIndex("uomname"));
                    Log.e("array[i+1]",array[i]);
                    cur.moveToNext();
                }
                itemListUOM.put(prodiduom,array);
            }
            return true;
        }catch (Exception e){
            MyApplication.getInstance().setErrorMSg(e.getLocalizedMessage());
            e.printStackTrace();
            return false;
        }finally {
            if(cur != null) {
                cur.close();
            }
        }
    }
    public void showChooseMerchant(){
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                showAlertDialogToast("Please choose the Merchant");
            }
        }, 1000);
    }

    public void setadap() {
        // TODO Auto-generated method stub

        if(MyApplication.getInstance().isTemplate8ForTemplate2()){
            loadTemplate8();
        }else {

            switch (Constants.TEMPLATE_NO) {
                case Constants.TEMPLATE1: {
                    template1Adapter = new Template1MerchantOrderDetailAdapter(
                            CreateOrderActivity.this,
                            R.layout.adapter_template1,
                            arraylistMerchantOrderDetailList, netCheck());
                    listViewProducts.setAdapter(template1Adapter);
                }
                break;
                case Constants.TEMPLATE2: {
                    if (!Constants.SalesMerchant_Id.isEmpty() &&
                            Constants.SalesMerchant_Id != null &&
                            merchantselected == 1) {
                        if (isStockMenuActive) {
                            loadTemplate8();
                        } else {
                            template2Load();
                        }

                    } else {
                        showChooseMerchant();
                        return;
                    }
                }
                break;
                case Constants.TEMPLATE3: {
                    if (!Constants.SalesMerchant_Id.isEmpty() &&
                            Constants.SalesMerchant_Id != null &&
                            merchantselected == 1) {
                        template3Load();
                    } else {
                        template3Load();
                        ///showChooseMerchant();
                        return;
                    }
                }
                break;
                case Constants.TEMPLATE4: {
                    if (!Constants.SalesMerchant_Id.isEmpty() &&
                            Constants.SalesMerchant_Id != null &&
                            merchantselected == 1) {
                        template4Load();
                    } else {
                        template4Load();
                        ////showChooseMerchant();
                        return;

                    }
                }
                break;
                case Constants.TEMPLATE5: {
                    adapter = new MerchantOrderDetailAdapter(
                            CreateOrderActivity.this,
                            R.layout.item_order_details_new,
                            arraylistMerchantOrderDetailList, netCheck());
                    listViewProducts.setAdapter(adapter);
                }
                break;
                case Constants.TEMPLATE6: {
                    template6Adapter = new Template6MilkOrderDetailAdapter(
                            CreateOrderActivity.this,
                            R.layout.adapter_template6,
                            arraylistMerchantOrderDetailList, netCheck());
                    listViewProducts.setAdapter(template6Adapter);
                }
                break;
                case Constants.TEMPLATE8: {
                    template8Adapter = new Template8StockOnlyAdapter(
                            CreateOrderActivity.this,
                            R.layout.adapter_template8_stock_only,
                            arraylistMerchantOrderDetailList, netCheck());
                    listViewProducts.setAdapter(template8Adapter);
                }
                break;

                case Constants.TEMPLATE9: {
                    if (!Constants.SalesMerchant_Id.isEmpty() &&
                            Constants.SalesMerchant_Id != null &&
                            merchantselected == 1) {
                        template9Load();
                    } else {
                        template9Load();
                        ////showChooseMerchant();
                        return;
                    }
                }
                break;

                case Constants.TEMPLATE10: {
                    if (!Constants.SalesMerchant_Id.isEmpty() &&
                            Constants.SalesMerchant_Id != null &&
                            merchantselected == 1) {
                        template10Load();
                    } else {
                        template10Load();
                        ///showChooseMerchant();
                        return;
                    }
                }
                break;

                default:
                    break;
            }
        }
        Log.e("arraylist", String.valueOf(arraylistMerchantOrderDetailList.size()));
    }

    private void loadTemplate8(){
        isStockMenuActive = true;

        showLoadingInfo();///////
        if (loadingOfflineUOM()) {
            CreateOrderActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    template8Adapter = new Template8StockOnlyAdapter(
                            CreateOrderActivity.this,
                            R.layout.adapter_template8_stock_only,
                            arraylistMerchantOrderDetailList, netCheck());
                    listViewProducts.setAdapter(template8Adapter);
                }
            });
            hideLoadingInfo();
        }else {
            hideLoadingInfo();
            showDialog(this, "Error", "Some Technical Error");
        }
    }

    public void template2Load(){
        try {
            showLoadingInfo();///////
            if (loadingOfflineUOM()) {
                if(netCheck()) {
                    new OnlineAsyncTaskAction(this).getStock(userBeatModel, arraylistMerchantOrderDetailList,
                            new OnlineAsyncTaskAction.OnlineStockCompletedListener() {
                                @Override
                                public void onStockFetchFinished(boolean result, String errorMSG) {
                                    hideLoadingInfo();
                                    if (result) {
                                        template2Adapter = new Template2MerchantOrderDetailAdapter(
                                                CreateOrderActivity.this,
                                                R.layout.adapter_template2,
                                                arraylistMerchantOrderDetailList, netCheck());
                                        listViewProducts.setAdapter(template2Adapter);
                                    } else {
                                        showDialog(CreateOrderActivity.this, "Info", errorMSG);
                                    }
                                }
                            });

                }else {
                    for(int i = 0; i < arraylistMerchantOrderDetailList.size(); i++) {
                        MerchantOrderDetailDomain item = arraylistMerchantOrderDetailList.get(i);
                        item.setStockPosition(DISTRIBUTOR_STOCK +
                                CURRENT_STOCK_NOT_AVAIL + "\n" + COMPANY_STOCK + CURRENT_STOCK_NOT_AVAIL);
                        arraylistMerchantOrderDetailList.set(i, item);
                    }
                    template2Adapter = new Template2MerchantOrderDetailAdapter(
                            CreateOrderActivity.this,
                            R.layout.adapter_template2,
                            arraylistMerchantOrderDetailList, netCheck());
                    listViewProducts.setAdapter(template2Adapter);

                    hideLoadingInfo();
                }
            } else {
                hideLoadingInfo();
                showDialog(this, "Error", "Some Technical Error");
            }
        } catch (Exception e) {
            e.printStackTrace();
            hideLoadingInfo();
        }
    }

    public void template3Load(){
        /// No need to display stocks;
        try {
            showLoadingInfo();///////
            if (loadingOfflineUOM()) {
                hideLoadingInfo();
                template3Adapter = new Template3MerchantOrderDetailAdapter(
                        CreateOrderActivity.this,
                        R.layout.adapter_template3,
                        arraylistMerchantOrderDetailList, netCheck());
                listViewProducts.setAdapter(template3Adapter);
            } else {
                hideLoadingInfo();
                showDialog(this, "INFO", "Some Technical Fault");
            }
        }catch (Exception e){
            hideLoadingInfo();
            e.printStackTrace();
            showDialog(this, "INFO", "Some Technical Fault :" + e.getMessage());
        }
        /*  // don't delete this code suppose change any design may use
        try {
            showLoadingInfo();///////
            if (loadingOfflineUOM()) {
                if(netCheck()) {
                    new OnlineAsyncTaskAction(this).getStock(userBeatModel, arraylistMerchantOrderDetailList,
                            new OnlineAsyncTaskAction.OnlineStockCompletedListener() {
                                @Override
                                public void onStockFetchFinished(boolean result, String errorMSG) {
                                    hideLoadingInfo();
                                    if (result) {
                                        template3Adapter = new Template3MerchantOrderDetailAdapter(
                                                CreateOrderActivity.this,
                                                R.layout.adapter_template3,
                                                arraylistMerchantOrderDetailList, netCheck());
                                        listViewProducts.setAdapter(template3Adapter);
                                    } else {
                                        showDialog(CreateOrderActivity.this, "Info", errorMSG);
                                    }
                                }
                            });

                }else {
                    for(int i = 0; i < arraylistMerchantOrderDetailList.size(); i++) {
                        MerchantOrderDetailDomain item = arraylistMerchantOrderDetailList.get(i);
                        item.setStockPosition(DISTRIBUTOR_STOCK +
                                CURRENT_STOCK_NOT_AVAIL + "\n" + COMPANY_STOCK + CURRENT_STOCK_NOT_AVAIL);
                        arraylistMerchantOrderDetailList.set(i, item);
                    }
                    template3Adapter = new Template3MerchantOrderDetailAdapter(
                            CreateOrderActivity.this,
                            R.layout.adapter_template3,
                            arraylistMerchantOrderDetailList, netCheck());
                    listViewProducts.setAdapter(template3Adapter);

                    hideLoadingInfo();
                }
            } else {
                hideLoadingInfo();
                showDialog(this, "Error", "Some Technical Error");
            }
        } catch (Exception e) {
            e.printStackTrace();
            hideLoadingInfo();
        }  */
    }

    public void template4Load() {
        /// No need to display stocks;
        try {
            showLoadingInfo();///////
            if (loadingOfflineUOM()) {
                hideLoadingInfo();
                template4Adapter = new Template4MerchantOrderDetailAdapter(
                        CreateOrderActivity.this,
                        R.layout.adapter_template4,
                        arraylistMerchantOrderDetailList, netCheck());
                listViewProducts.setAdapter(template4Adapter);
            } else {
                hideLoadingInfo();
                showDialog(this, "INFO", "Some Technical Fault");
            }
        } catch (Exception e) {
            hideLoadingInfo();
            showDialog(this, "INFO", "Some Technical Fault :" + e.getMessage());
        }
    }

    public void template9Load() {
        /// No need to display stocks;
        try {
            showLoadingInfo();///////
            if (loadingOfflineUOM()) {
                hideLoadingInfo();
                template9Adapter = new Template9MerchantOrderDetailAdapter(
                        CreateOrderActivity.this,
                        R.layout.adapter_template9,
                        arraylistMerchantOrderDetailList, netCheck());
                listViewProducts.setAdapter(template9Adapter);
            } else {
                hideLoadingInfo();
                showDialog(this, "INFO", "Some Technical Fault");
            }
        } catch (Exception e) {
            hideLoadingInfo();
            showDialog(this, "INFO", "Some Technical Fault :" + e.getMessage());
        }
    }

    public void template10Load() {
        try {
            showLoadingInfo();///////
            if (loadingOfflineUOM()) {
                hideLoadingInfo();
                template10Adapter = new Template10MerchantOrderDetailAdapter(
                        CreateOrderActivity.this,
                        R.layout.adapter_template10,
                        arraylistMerchantOrderDetailList, netCheck());
                listViewProducts.setAdapter(template10Adapter);
            } else {
                hideLoadingInfo();
                showDialog(this, "INFO", "Some Technical Fault");
            }
        }catch (Exception e){
            hideLoadingInfo();
            e.printStackTrace();
            showDialog(this, "INFO", "Some Technical Fault :" + e.getMessage());
    }
    }



    public void offlineBeat() {
        Cursor curs;
        curs = databaseHandler.getbeatwithMerchant(dayOfTheWeek);
        Log.e("beatCount", String.valueOf(curs.getCount()));

        int i = 0;

        if (curs != null && curs.getCount() > 0) {
            if (curs.moveToFirst()) {

                do {
                    Log.e("beat inside", "beat inside");
                    if (i == 0) {
                        if (curs.getString(0).equals("New User")) {
                            beatMerchants = "'" + curs.getString(0) + "'";
                        } else {
                            beatMerchants = curs.getString(0);
                        }
                        i++;
                    } else {
                        if (curs.getString(0).equals("New User")) {
                            beatMerchants = beatMerchants + "," + "'" + curs.getString(0) + "'";
                        } else {
                            beatMerchants = beatMerchants + "," + curs.getString(0);
                        }
                    }

                    Log.e("beatMerchants", beatMerchants);


                } while (curs.moveToNext());


            }
            offlinemerchant(beatMerchants);
        } else {
            showAlertDialogToast("No Merchants available for this day");
        }

    }

    public void offlinemerchant(String beatMerchants) {


        Log.e("string merchant", beatMerchants);
        Cursor curs;
        curs = databaseHandler.getmerchantoncurrentday(beatMerchants);
        Log.e("merchantCount", String.valueOf(curs.getCount()));

        if (curs != null && curs.getCount() > 0) {
            if (curs.moveToFirst()) {

                do {
                    String address = "",city = "",pin = "",fullAdd ="";
                    Log.e("inside", "inside");
                    MDealerCompDropdownDomain cd = new MDealerCompDropdownDomain();
                    cd.setComName(curs.getString(1));
                    cd.setID(curs.getString(2));
                    cd.setName(curs.getString(3));
                    cd.setOfflineMerchantId(curs.getString(curs.getColumnIndex("oflnordid")));  ///By Kumaravel


                    address = curs.getString(curs.getColumnIndex("address"));
                    city = curs.getString(curs.getColumnIndex("city"));
                    pin = curs.getString(curs.getColumnIndex("pincode"));

                    fullAdd = "Address : ";
                    if(address != null && !address.isEmpty()){
                        fullAdd = fullAdd + address;
                    }
                    if(city != null && !city.isEmpty()){
                        fullAdd = fullAdd + "  " + city;
                    }
                    if(pin != null && !pin.isEmpty()){
                        fullAdd = fullAdd +  "  " + pin;
                    }
                    if(fullAdd.isEmpty()){
                        fullAdd = "Address not avail";
                    }
                    cd.setaddress(fullAdd);
                    Log.e("Address", fullAdd);

                    Log.e("companyname", curs.getString(1));
                    Log.e("mid", curs.getString(2));
                    Log.e("fullname", curs.getString(3));
                    Log.e("OfflineMerchantId", curs.getString(curs.getColumnIndex("oflnordid")));
                    arrayListDealerDomain.add(cd);
                    arrayListSearch.add(cd);
                    arrayListDealer.add(curs.getString(1));
                    arrayListmerchant.add(cd);

                } while (curs.moveToNext());


            }

        } else {
            textViewNodata.setVisibility(View.VISIBLE);
            textViewNodata.setVisibility(View.VISIBLE);
            textViewNodata.setText("No Products Found");
        }
        Log.e("size", String.valueOf(arrayListDealerDomain.size()));

        Log.e("size of array", String.valueOf(arrayListDealer.size()));
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                CreateOrderActivity.this, R.layout.textview, R.id.textView,
                arrayListDealer);
        autocompleteTextTraders.setAdapter(adapter);
        autocompleteTextTraders.setText(Constants.Merchantname);


        if (!Constants.Merchantname.equals("")) {
            autocompleteTextTraders.setText(Constants.Merchantname);
            merchantselected = 1;
            //merchantadpset();
        }

        if (Constants.checkproduct == 0) {
            loadingOfflineProducts();
        } else {
            setadap();
        }

    }

    public void getOrderDetails(int offlineID) {
        databaseHandler = new DatabaseHandler(getApplicationContext());
        Cursor cursororderdetail;
        cursororderdetail = databaseHandler.getorderdetail(String.valueOf(offlineID));
        int lstOrderCount = cursororderdetail.getCount();
        Log.e("", "getOrderDetails: " + cursororderdetail.getCount());
        Log.e(TAG, "getOrderDetails: " + "NOt NULL" + arraylistMerchantOrderDetailList.size());
        if (lstOrderCount > 0) {
            cursororderdetail.moveToFirst();

            while (!cursororderdetail.isAfterLast()) {
                try {
                    int arraycount=arraylistMerchantOrderDetailList.size();
                    for(int i=0;i<arraycount;i++)
                    {
                        String arryprodCode =  arraylistMerchantOrderDetailList.get(i).getprodcode().toLowerCase(Locale.getDefault());
                        Log.e(TAG, "ProdCode: " + arryprodCode);
                        String prodcode = cursororderdetail.getString(9).toLowerCase(Locale.getDefault());
                        Log.e(TAG, "Cursor Prod code: " + prodcode);
                        if (arryprodCode.equals(prodcode)) {
                            arraylistMerchantOrderDetailList.get(i).setqty(cursororderdetail.getString(cursororderdetail.getColumnIndex(DatabaseHandler.KEY_qty)));
                            Log.e(TAG, "Finally GOT IT: " + "Finally");
                        }
                    }

                }catch (ConcurrentModificationException e)
                {
                    e.getMessage();
                }
                cursororderdetail.moveToNext();
            }
            cursororderdetail.close();
            /* Notify the Changes in the Same ArrayList */
            adapter.notifyDataSetChanged();

                          /*   int  arraysize=arraylistMerchantOrderDetailList.size();
            Log.e("oflnordid", String.valueOf(cursororderdetail.getInt(1)));
            Log.e("duserid", String.valueOf(cursororderdetail.getInt(2)));
            Log.e("Dcompany", cursororderdetail.getString(3));
            Log.e("prodid", cursororderdetail.getString(4));
            Log.e("isfree", cursororderdetail.getString(6));
            Log.e("ordstatus", cursororderdetail.getString(7));
            Log.e("ordslno", cursororderdetail.getString(8));
            Log.e("productcode", cursororderdetail.getString(9));
            Log.e("updateddt", cursororderdetail.getString(11));
            Log.e("deliverydt", cursororderdetail.getString(12));
            Log.e("prate", cursororderdetail.getString(13));
            Log.e("ptax", cursororderdetail.getString(14));
            Log.e("pvalue", cursororderdetail.getString(15));
            Log.e("isread", cursororderdetail.getString(16));
            Log.e("stock", cursororderdetail.getString(17));
            Log.e("batchno", cursororderdetail.getString(18));
            Log.e("manufdt", cursororderdetail.getString(19));
            Log.e("expirydt", cursororderdetail.getString(20));
            Log.e("ordimage", cursororderdetail.getString(21));
            Log.e("default_uom", cursororderdetail.getString(22));
            Log.e("ord_uom", cursororderdetail.getString(23));
            Log.e("cqty", cursororderdetail.getString(24));
            Log.e("deliverytime", cursororderdetail.getString(25));
            Log.e("deliverytime", cursororderdetail.getString(26));*/

        }else {
            Log.e(TAG, "get the Last ORDER FROM API: "+"" );
        }
    }

    /*  public void merchantadpset(){
          dropdown =new MerchantDropDownAdapter(CreateOrderActivity.this, R.layout.item_screen_merchant_dropdown,
                  arraylistmerchantdropdown);

          autocompleteTextTraders.setAdapter(dropdown);
      }*/
    protected void toastDisplay(String msg) {
        Toast toast = Toast.makeText(CreateOrderActivity.this, msg,
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();

    }

    public boolean netCheck() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {

                return true;
            } else if (result == null) {

             /*showAlertDialog(CreateOrderActivity.this,
                        "No Internet Connection",
                        "Please Check Your internet connection.", false);

Log.e("offline","offline");*/
                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public void showAlertDialog(Context context, String title, String message,
                                Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        alertDialog.show();
    }

    public void showAlertDialogToast(String message) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setMessage(message);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert = builder.create();
        alert.show();

        Button buttonbackground = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    public void showAlertDialogToast1(String message) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setMessage(message);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent to = new Intent(CreateOrderActivity.this, CreateOrderActivity.class);
                        startActivity(to);
                        CreateOrderActivity.this.finish();
                        dialog.cancel();
                    }
                });


        AlertDialog alert = builder.create();
        alert.show();

        Button buttonbackground = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    public void onBackPressed() {
        exitAlret();
    }

    @Override
    public void userResponse(JSONObject jsonObject) {

    }

    private void exitAlret() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        localBuilder.setMessage("Do you want to Exit?");
        localBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {

                        finish();

                    }
                });
        localBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {
                        paramDialogInterface.cancel();
                    }
                });
        localBuilder.create().show();

    }

    /// by Kumaravel
    public void showDialog(Context mContext, String title, String message){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        // Setting Dialog Title
        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.tick);
        // Setting Negative "NO" Button
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // User pressed No button. Write Logic Here
                //Toast.makeText(mContext, "You clicked on NO", Toast.LENGTH_SHORT).show();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }


    // By Kumaravel 21-12-2018
    private void showLoadingInfo(){
        if(llProgressBar != null){
            llProgressBar.setVisibility(View.VISIBLE);
        }
    }

    private void hideLoadingInfo(){
        if(llProgressBar != null){
            if(llProgressBar.getVisibility() == View.VISIBLE)
            llProgressBar.setVisibility(View.GONE);
        }
    }

    private void showDairyPopup(){
        Log.e("MILK_POPUP","..............welcome");
        final Template6QuantityEditAndSavePopup popupMilk =
                new Template6QuantityEditAndSavePopup(
                        CreateOrderActivity.this,
                        findViewById(R.id.listViewProducts),
                        template6Adapter.getPairCheck(),
                        template6Adapter.getSelectedMerchantOrders(),
                        CreateOrderActivity.arraylistMerchantOrderDetailList,
                        new Template6QuantityEditAndSavePopup.ParentListener() {
                            @Override
                            public void onPopupDismiss() {
                                CreateOrderActivity.this.runOnUiThread(new Runnable() { // this is used to reload current activity for order correct status
                                    @Override
                                    public void run() {
                                        Intent intent = getIntent();
                                        finish();
                                        startActivity(intent);
                                    }
                                });

                            }
                        });

        new Handler().postDelayed(new Runnable() {
            public void run() {
                popupMilk.showPopup();
            }
        }, 50);
    }


    private boolean CheckGpsStatus(){
        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );
        return manager != null && manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    // Trigger new location updates at interval
    @SuppressLint("MissingPermission")
    protected void startLocationUpdates() {

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations, this can be null.
                        if (location != null) {
                            updateCurrentLocation(location);
                            mLastLocation = location;
                        }
                    }
                });
        try {
            locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            //    mLastLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            passive_enabled = locationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER);

            if (gps_enabled) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0,
                        locationListenerGps);
            }

            if (network_enabled) {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0,
                        locationListenerNetwork);
            }
            if (passive_enabled) {
                locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 1000, 0,
                        locationListenerPassive);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    LocationListener locationListenerNetwork = new LocationListener() {
        public void onLocationChanged(Location location) {
            if (location != null) {
                mLastLocation = location;
                updateCurrentLocation(location);
            }
        }
        public void onProviderDisabled(String provider) { }
        public void onProviderEnabled(String provider) { }
        public void onStatusChanged(String provider, int status, Bundle extras) { }
    };

    LocationListener locationListenerGps = new LocationListener() {
        public void onLocationChanged(Location location) {
            if (location != null) {
                mLastLocation = location;
                updateCurrentLocation(location);
            }
        }
        public void onProviderDisabled(String provider) { }
        public void onProviderEnabled(String provider) { }
        public void onStatusChanged(String provider, int status, Bundle extras) { }
    };


    LocationListener locationListenerPassive = new LocationListener() {
        public void onLocationChanged(Location location) {
            if (location != null) {
                mLastLocation = location;
                updateCurrentLocation(location);
            }
        }
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) { }
        @Override
        public void onProviderEnabled(String provider) { }
        @Override
        public void onProviderDisabled(String provider) { }

    };

    synchronized private void updateCurrentLocation(Location mLastLocation){
        createordergeolat = mLastLocation.getLatitude();
        createordergeolog = mLastLocation.getLongitude();
    }

    private void showD(String msg){
        if(pDialog == null){
            pDialog = ProgressDialog.show(CreateOrderActivity.this, "", msg, true, false);
        }else {
            if (!pDialog.isShowing()) {
                pDialog = ProgressDialog.show(CreateOrderActivity.this, "", msg, true, false);
            }
        }
    }

    private void hideD(){
        if(pDialog != null){
            if(pDialog.isShowing()){
                pDialog.dismiss();
            }
        }
    }

    private void noOrderLocationUpdateWithDelay(){

        showD(getResources().getString(R.string.no_order_gps));
        Handler handler = new Handler();
        handler.postDelayed(new Runnable(){
            @Override
            public void run(){
                gpsCounter ++ ;
                if(mLastLocation != null) {
                    updateCurrentLocation(mLastLocation);
                }else {
                    if(gpsCounter >= 2){
                        hideD();
                        if(CheckGpsStatus()){
                            showAlertDialogToast(getResources().getString(R.string.gps_accuracy_setting));
                            return;
                        }else {
                            showSettingsAlert();
                            return;
                        }
                    }else {
                        noOrderLocationUpdateWithDelay();
                    }
                }
                hideD();
                followup();
            }
        }, 6000);
    }




    @Override
    protected void onResume() {
        super.onResume();

        if(mFusedLocationClient == null) {
            startLocationUpdates();
        }

        if(databaseHandler == null) {
            databaseHandler = new DatabaseHandler(getApplicationContext());
        }
        Cursor cur;

        cur = databaseHandler.getDetails();
        if(cur != null && cur.getCount() > 0) {
            cur.moveToFirst();
            PaymentStatus = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_paymentStatus));
            Constants.USER_TYPE = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_usertype));
            Constants.USER_ID = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_id));
            Constants.USER_TYPE = "S";
            cur.close();
        }

        Cursor cur1;
        if(userBeatModel == null || userBeatModel.isEmpty()) {
            cur1 = databaseHandler.getUserSetting();
            if(cur1 != null && cur1.getCount() > 0) {
                cur1.moveToFirst();
                Log.e("curCount", ".........................." + cur1.getCount());
                userBeatModel = cur1.getString(cur1.getColumnIndex(DatabaseHandler.KEY_settingrefvalue));
                cur1.close();
            }
        }

    }

    /// this for

    private void setListViewPreventException(ListView listView){

        listView.setRecyclerListener(new ListView.RecyclerListener() {
            @Override
            public void onMovedToScrapHeap(View view) {
                if ( view.hasFocus()){
                    view.clearFocus(); //we can put it inside the second if as well, but it makes sense to do it to all scraped views
                    //Optional: also hide keyboard in that case
                    if ( view instanceof EditText) {
                        InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                }
            }
        });
    }

}
