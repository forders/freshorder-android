package com.freshorders.freshorder.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.GrnAdapter;
import com.freshorders.freshorder.aync.IUserDetailCallBack;
import com.freshorders.freshorder.aync.UserDetailTask;
import com.freshorders.freshorder.model.GRN;
import com.freshorders.freshorder.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class GRNActivity extends Activity implements IUserDetailCallBack {

    private ListView grnListView;
    private TextView grnDataNotFound;
    private ProgressDialog mProgressDialog;
    private static final String TAG = GRNActivity.class.getSimpleName();
    private ArrayList<GRN> grnList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.grn_activity);
        grnListView = (ListView) findViewById(R.id.grnListView);
        grnDataNotFound = (TextView) findViewById(R.id.grnDataNotFoundTxtView);
        getGrnOrder();
    }

    private void show() {
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(GRNActivity.this, "",
                    "Initializing the order ...", true, true);
        }
    }

    private void dismissDialog() {
        if (mProgressDialog != null &&
                mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    public void getGrnOrder() {
        show();
        Log.d(TAG, "getGrnOrder in ......");
        final UserDetailTask userDetail = new UserDetailTask(Utils.strGrnOrder,
                null, GRNActivity.this, this);
        userDetail.execute();
    }

    @Override
    public void userResponse(JSONObject jsonObject) {
        dismissDialog();

        if (jsonObject == null) {
            return;
        }
        Log.d(TAG, "grnResponse in ......" + jsonObject.toString());
        if (jsonObject.optString("status").equalsIgnoreCase("true")) {
            final JSONArray dataArr = jsonObject.optJSONArray("data");
            if (dataArr == null) {
                final String msg = jsonObject.optString("message");
                setAlert(msg);
                return;
            }
            grnList = Utils.getOrderList(dataArr);
            setGrnAdapter();

        } else if (jsonObject.optString("status").equalsIgnoreCase("false")) {
            final String msg = jsonObject.optString("message");
            setAlert(msg);
        }
    }

    private void setAlert(final String msg) {
        grnDataNotFound.setVisibility(View.VISIBLE);
        grnDataNotFound.setText(msg);
        setListVisibility(View.GONE);
    }

    private void setGrnAdapter() {
        grnDataNotFound.setVisibility(View.GONE);
        setListVisibility(View.VISIBLE);
        GrnAdapter grnAdapter = new GrnAdapter(GRNActivity.this, R.layout.grn_list_view, grnList);
        grnListView.setAdapter(grnAdapter);
        grnListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                navigateToNxtScreen(i);
            }
        });
    }

    private void setListVisibility(final int mode) {
        grnListView.setVisibility(mode);
    }

    private void navigateToNxtScreen(final int listPos) {
        Log.d("navigateToNxtScreen", "clickPos" + listPos);
        Log.d(TAG, "grnlist Size" + grnList.size());
        Intent intent = new Intent(GRNActivity.this,
                GRNOrderScreenActivity.class);
        final Bundle b = new Bundle();
        b.putParcelableArrayList(GRNOrderScreenActivity.GRN_LIST, grnList);
        intent.putExtra(GRNOrderScreenActivity.GRN_CLICKED_POS, listPos);
        intent.putExtras(b);
        startActivity(intent);
    }
}
