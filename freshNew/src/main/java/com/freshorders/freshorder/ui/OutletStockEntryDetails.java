package com.freshorders.freshorder.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.freshorders.freshorder.R;

import com.freshorders.freshorder.adapter.OutletStockDetailAdapter;
import com.freshorders.freshorder.db.DatabaseHandler;

import com.freshorders.freshorder.domain.OutletStockEntryDetailDomain;
import com.freshorders.freshorder.popup.PopupOutletStockEntryDatePicker;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class OutletStockEntryDetails extends Activity {

	TextView textViewName,textViewAddress,textViewDate,menuIcon;
	LinearLayout linearLayoutBack;
	JSONObject JsonAccountObject = null;
	JSONArray JsonAccountArray = null;
	JsonServiceHandler JsonServiceHandler ;
	public static ArrayList<OutletStockEntryDetailDomain> arraylistDealerOrderDetailList;
	Date dateStr = null,manufdate=null,expirydate=null;
	public static String time;
	Button buttonUpdate,buttonCancel;
	DatabaseHandler databaseHandler;
	public static String[] orderdtid, orderid,prodid,qty,date,status,freeQty,duserid,flag,stock,batchno,expdate,manufdt;
	ArrayList<String> Orderdtlist ;
	SQLiteDatabase db;
	OutletStockDetailAdapter adapter;

	EditText editText9,editText8,editText7,editText6,editText5,editText4,editText3,editText2,editText1;
	String mfgno1,batchno1,stock1,mfgno2,batchno2,stock2,mfgno3,batchno3,stock3,outformatdate1="",outformatdate2="",outformatdate3="";
	DatePicker datePicker;
	public int monthnew,newmnth,mnth3,year3,newyear,yearnew,day3,newday,daynew;
	private int year,hr,sec,mins,day;
	public static String date1,date2,date3,updatedate;
	JSONObject jsonObject1,jsonObject2,jsonObject3;
	int validate=0,validate2=0,validate3=0;
	JSONArray jsonArray;

	private String inputPattern = "dd-MM-yyyy";
	private SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.getDefault());
	private String outputDatePattern = "yyyy-MM-dd";
	SimpleDateFormat outputFormat = new SimpleDateFormat(outputDatePattern, Locale.getDefault());


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(1);
		setContentView(R.layout.outlet_stock_entry_detail);
		Constants.startTime  = new SimpleDateFormat(
				"HH:mm:ss").format(new java.util.Date());

		databaseHandler = new DatabaseHandler(getApplicationContext());
		netCheck();

		db=getApplicationContext().openOrCreateDatabase("freshorders", 0, null);
		textViewName = (TextView) findViewById(R.id.textViewName);
		textViewAddress = (TextView) findViewById(R.id.textViewAddress);
		textViewDate = (TextView) findViewById(R.id.textViewDate);
		datePicker= (DatePicker) findViewById(R.id.datePicker1);
		editText1=(EditText) findViewById(R.id.editText1);
		editText2=(EditText) findViewById(R.id.editText2);
		editText3=(EditText) findViewById(R.id.editText3);
		editText4=(EditText) findViewById(R.id.editText4);
		editText5=(EditText) findViewById(R.id.editText5);
		editText6=(EditText) findViewById(R.id.editText6);
		editText7=(EditText) findViewById(R.id.editText7);
		editText8=(EditText) findViewById(R.id.editText8);
		editText9=(EditText) findViewById(R.id.editText9);
		menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
		buttonUpdate = (Button) findViewById(R.id.buttonUpdate);
		buttonCancel = (Button) findViewById(R.id.buttonCancel);
		jsonArray = new JSONArray();
		Orderdtlist =new  ArrayList<String>();
		linearLayoutBack = (LinearLayout) findViewById(R.id.linearLayoutBack);
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		 updatedate=dateFormat.format(date);

		Log.e("updateddate",updatedate);
		showCurrentDateOnView();
		Typeface font = Typeface.createFromAsset(getAssets(),
				"fontawesome-webfont.ttf");
		menuIcon.setTypeface(font);

		textViewName.setText(OutletStockEntry.companyname+ "  ["
				+ OutletStockEntry.fullname + "]");

		textViewAddress.setText(OutletStockEntry.date +"  [" + OutletStockEntry.address + "]");

		buttonCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent to = new Intent(OutletStockEntryDetails.this, OutletStockEntry.class);
				Constants.setorder=1;
				startActivity(to);
				finish();
			}
		});


		buttonUpdate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				mfgno1=editText1.getText().toString();
				mfgno2=editText4.getText().toString();
				mfgno3=editText7.getText().toString();
				stock1=editText2.getText().toString();
				stock2=editText5.getText().toString();
				stock3=editText8.getText().toString();
				batchno1=editText3.getText().toString();
				batchno2=editText6.getText().toString();
				batchno3=editText9.getText().toString();


				if(!mfgno1.equals("")||!stock1.equals("")){

					validate=1;

					if(mfgno1.equals("")){
						showAlertDialogToast("Please select manufacture date in row 1");
					}else if(stock1.equals("")){
						showAlertDialogToast("Please enter quantity in row 1");
					}else{

						try {

							jsonObject1 = new JSONObject();
							jsonObject1.put("manufdt", outformatdate1);
							jsonObject1.put("batchno", batchno1);
							jsonObject1.put("qty", stock1);
							jsonObject1.put("suserid", Constants.USER_ID);
							jsonObject1.put("muserid", OutletStockEntry.merchantid);
							jsonObject1.put("prodid", OutletStockEntry.prodid);
							jsonObject1.put("remark","");
							jsonObject1.put("updateddt",updatedate);
							jsonObject1.put("updatedby","ADMIN");


							jsonArray.put(jsonObject1);

							} catch (JSONException e) {
								e.printStackTrace();
							}
							validate2();
						}

				}else{
					validate=0;
					validate2();
				}


			}
		});

		editText1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				hideKeyBoard();//////
				PopupOutletStockEntryDatePicker datePopup = new
						PopupOutletStockEntryDatePicker(
								OutletStockEntryDetails.this, editText1,
						new PopupOutletStockEntryDatePicker.PopupListener() {
							@Override
							public void onPopupClosed() {
								String mfgDate1 = editText1.getText().toString().trim();
								Log.e("editText1" , "..............." + mfgDate1);
								try {
									if (mfgDate1.length() > 0) {
                                        dateStr = inputFormat.parse(mfgDate1);
										outformatdate1 = outputFormat.format(dateStr);
									}
								}catch (Exception e){
									e.printStackTrace();
								}
							}
						});
				datePopup.showPopup();
			}
		});

		editText4.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				hideKeyBoard();////////
				PopupOutletStockEntryDatePicker datePopup = new
						PopupOutletStockEntryDatePicker(
						OutletStockEntryDetails.this, editText4,
						new PopupOutletStockEntryDatePicker.PopupListener() {
							@Override
							public void onPopupClosed() {
								String mfgDate2 = editText4.getText().toString().trim();
								try {
									if (mfgDate2.length() > 0) {
                                        dateStr = inputFormat.parse(mfgDate2);
										outformatdate2 = outputFormat.format(dateStr);
									}
								}catch (Exception e){
									e.printStackTrace();
								}
							}
						});
				datePopup.showPopup();
			}
		});

		editText7.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				hideKeyBoard();///////
				PopupOutletStockEntryDatePicker datePopup = new
						PopupOutletStockEntryDatePicker(
						OutletStockEntryDetails.this, editText7,
						new PopupOutletStockEntryDatePicker.PopupListener() {
							@Override
							public void onPopupClosed() {
								String mfgDate3 = editText7.getText().toString().trim();
								try {
									if (mfgDate3.length() > 0) {
                                        dateStr = inputFormat.parse(mfgDate3);
										outformatdate3 = outputFormat.format(dateStr);
									}
								}catch (Exception e){
									e.printStackTrace();
								}
							}
						});
				datePopup.showPopup();
			}
		});

		/*editText1.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				DatePickerDialog mdiDialog = new DatePickerDialog(
						OutletStockEntryDetails.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT,
						new DatePickerDialog.OnDateSetListener() {
							@Override
							public void onDateSet(DatePicker view,
												  int year, int month,
												  int dayOfMonth) {
								Log.e("date1", year + "-" + (month)
										+ "-" + day);

								Log.e("date", year + "-" + (month+1)
										+ "-" + dayOfMonth);
								String time =year + "-" + (month+1)
										+ "-" + dayOfMonth;

								String monthLength =String.valueOf(month+1);
								Log.e("date", monthLength);
								if(monthLength.length()==1){
									monthLength = "0"+monthLength;
								}
								String dayLength =String.valueOf(dayOfMonth);
								if(dayLength.length()==1){
									dayLength = "0"+dayLength;
								}
								date1 = dayLength + "-" + monthLength
										+ "-" + year;

								Log.e("date1",date1);

								monthnew=month;
								yearnew=year;
								daynew=dayOfMonth;

								editText1.setText(date1);

								String inputPattern = "dd-MM-yyyy";
								String outputPattern = "yyyy-MM-dd";

								SimpleDateFormat inputFormat = new SimpleDateFormat(
										inputPattern);
								SimpleDateFormat outputFormat = new SimpleDateFormat(
										outputPattern);

								String str = null;
								try {
									dateStr = inputFormat.parse(date1);
									outformatdate1 = outputFormat.format(dateStr);

									// Log.e("str", str);
								} catch (Exception  e) {
									e.printStackTrace();
								}

							}
						}, yearnew, monthnew, daynew);
				mdiDialog.show();
			}
		});

		editText4.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				DatePickerDialog mdiDialog = new DatePickerDialog(
						OutletStockEntryDetails.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT,
						new DatePickerDialog.OnDateSetListener() {
							@Override
							public void onDateSet(DatePicker view,
												  int year, int month,
												  int dayOfMonth) {
								// Toast.makeText(getContext(),year+
								// " "+monthOfYear+" "+dayOfMonth,Toast.LENGTH_LONG).show();
								Log.e("date", year + "-" + (month+1)
										+ "-" + dayOfMonth);
								String time =year + "-" + (month+1)
										+ "-" + dayOfMonth;

								String monthLength =String.valueOf(month+1);
								if(monthLength.length()==1){
									monthLength = "0"+monthLength;
								}
								String dayLength =String.valueOf(dayOfMonth);
								if(dayLength.length()==1){
									dayLength = "0"+dayLength;
								}

								date2 = dayLength + "-" + monthLength
										+ "-" + year;

								newmnth=month;
								newyear=year;
								newday=dayOfMonth;
								editText4.setText(date2);

								String inputPattern = "dd-MM-yyyy";
								String outputPattern = "yyyy-MM-dd";

								SimpleDateFormat inputFormat = new SimpleDateFormat(
										inputPattern);
								SimpleDateFormat outputFormat = new SimpleDateFormat(
										outputPattern);

								String str = null;
								try {
									dateStr = inputFormat.parse(date2);
									outformatdate2 = outputFormat.format(dateStr);

									// Log.e("str", str);
								} catch (Exception  e) {
									e.printStackTrace();
								}

							}
						}, newyear, newmnth, newday);
				mdiDialog.show();
			}
		});

		editText7.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				DatePickerDialog mdiDialog = new DatePickerDialog(
						OutletStockEntryDetails.this,AlertDialog.THEME_DEVICE_DEFAULT_LIGHT,
						new DatePickerDialog.OnDateSetListener() {
							@Override
							public void onDateSet(DatePicker view,
												  int year, int month,
												  int dayOfMonth) {

								String time =year + "-" + (month+1)
										+ "-" + dayOfMonth;

								String monthLength =String.valueOf(month+1);
								Log.e("date", monthLength);
								if(monthLength.length()==1){
									monthLength = "0"+monthLength;
								}
								String dayLength =String.valueOf(dayOfMonth);
								if(dayLength.length()==1){
									dayLength = "0"+dayLength;
								}
								date3 = dayLength + "-" + monthLength
										+ "-" + year;

								Log.e("date1",date3);

								mnth3=month;
								year3=year;
								day3=dayOfMonth;

								editText7.setText(date3);

								String inputPattern = "dd-MM-yyyy";
								String outputPattern = "yyyy-MM-dd";

								SimpleDateFormat inputFormat = new SimpleDateFormat(
										inputPattern);
								SimpleDateFormat outputFormat = new SimpleDateFormat(
										outputPattern);

								String str = null;
								try {
									dateStr = inputFormat.parse(date3);
									outformatdate3 = outputFormat.format(dateStr);

									// Log.e("str", str);
								} catch (Exception  e) {
									e.printStackTrace();
								}

							}
						}, year3, mnth3, day3);
				mdiDialog.show();
			}
		});  */
	}

	private void showCurrentDateOnView() {
		// TODO Auto-generated method stub


		final Calendar c = Calendar.getInstance();
		yearnew = c.get(Calendar.YEAR);
		newyear= c.get(Calendar.YEAR);
		year3= c.get(Calendar.YEAR);
		newmnth = c.get(Calendar.MONTH);
		monthnew=c.get(Calendar.MONTH);
		mnth3=c.get(Calendar.MONTH);
		daynew = c.get(Calendar.DAY_OF_MONTH);
		day3 = c.get(Calendar.DAY_OF_MONTH);
		newday=c.get(Calendar.DAY_OF_MONTH);
		hr= c.get(Calendar.HOUR_OF_DAY);
		mins= c.get(Calendar.MINUTE);
		sec= c.get(Calendar.SECOND);

		String monthLength =String.valueOf(newmnth+1);
		if(monthLength.length()==1){
			monthLength = "0"+monthLength;
		}
		String dayLength =String.valueOf(daynew);
		if(dayLength.length()==1){
			dayLength = "0"+dayLength;
		}

		Log.e("monthLength",monthLength);

		Log.e("dayLength",dayLength);

		String inputPattern = "yyyy-MM-dd";
		String outputPattern = "dd-MMM-yyyy ";
		SimpleDateFormat inputFormat = new SimpleDateFormat(
				inputPattern);
		SimpleDateFormat outputFormat = new SimpleDateFormat(
				outputPattern);

		String str = null;
		try {
			dateStr = inputFormat.parse(time);
			str = outputFormat.format(dateStr);

		} catch (Exception  e) {
			e.printStackTrace();
		}


		datePicker.init(year, newmnth, day, null);

	}

	protected void validate2() {
		if(!mfgno2.equals("")||!stock2.equals("")){

			validate2=1;

				if(mfgno2.equals("")){
					showAlertDialogToast("Please select manufacture date in row 2");
				}else if(stock2.equals("")){
					showAlertDialogToast("Please enter quantity in row 2");
				}else{
					try {

						jsonObject2 = new JSONObject();
						jsonObject2.put("manufdt", outformatdate2);
						jsonObject2.put("batchno", batchno2);
						jsonObject2.put("qty", stock2);
						jsonObject2.put("suserid", Constants.USER_ID);
						jsonObject2.put("muserid", OutletStockEntry.merchantid);
						jsonObject2.put("prodid", OutletStockEntry.prodid);
						jsonObject2.put("remark","TESR");
						jsonObject2.put("updateddt",updatedate);
						jsonObject2.put("updatedby","ADMIN");

						jsonArray.put(jsonObject2);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					validate3();
				}
		}else{
			validate2=0;
			validate3();
		}

	}

	protected void validate3() {
		if(!mfgno3.equals("")||!stock3.equals("")){
			validate3=1;

				if(mfgno3.equals("")){
					showAlertDialogToast("Please select manufacture date in row 3");
				}else if(stock3.equals("")){
					showAlertDialogToast("Please enter quantity in row 3");
				}else{
					try {
						jsonObject3 = new JSONObject();
						jsonObject3.put("manufdt", outformatdate3);
						jsonObject3.put("batchno", batchno3);
						jsonObject3.put("qty", stock3);
						jsonObject3.put("suserid", Constants.USER_ID);
						jsonObject3.put("muserid", OutletStockEntry.merchantid);
						jsonObject3.put("prodid", OutletStockEntry.prodid);
						jsonObject3.put("remark","");
						jsonObject3.put("updateddt",updatedate);
						jsonObject3.put("updatedby","ADMIN");

						jsonArray.put(jsonObject3);
						if(netCheck()) {
							saveOrder();
						}else {
							try {
								ContentValues cv = new ContentValues();
								cv.put(DatabaseHandler.KEY_PKD_URL, Utils.strstocks);
								cv.put(DatabaseHandler.KEY_PKD_SYNC_DATA, insertStockEntry());
								cv.put(DatabaseHandler.KEY_PKD_FAILED_REASON, Constants.EMPTY);
								cv.put(DatabaseHandler.KEY_PKD_MIGRATION_STATUS, 1);
								cv.put(DatabaseHandler.KEY_PKD_MERCHANT_PRIMARY_KEY, Constants.MERCHANT_PRIMARY_KEY_FOR_PKD);
								databaseHandler.loadPKD(cv);
								showAlertDialogToast1("Saved Successfully");
								navigateToPrevious();
							}catch (Exception e){
								e.printStackTrace();
								showAlertDialogToast1("UnExpected Failed Occur May Technical Assist Need");
							}
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}

		}else{
			validate3=0;
			if(validate==0&&validate2==0&&validate3==0){
				showAlertDialogToast("Please enter atleast one entry");
			}else{
				if(netCheck()) {
					saveOrder();
				}else {
					try {
						ContentValues cv = new ContentValues();
						cv.put(DatabaseHandler.KEY_PKD_URL, Utils.strstocks);
						cv.put(DatabaseHandler.KEY_PKD_SYNC_DATA, insertStockEntry());
						cv.put(DatabaseHandler.KEY_PKD_FAILED_REASON, Constants.EMPTY);
						cv.put(DatabaseHandler.KEY_PKD_MIGRATION_STATUS, 1);
						cv.put(DatabaseHandler.KEY_PKD_MERCHANT_PRIMARY_KEY, Constants.MERCHANT_PRIMARY_KEY_FOR_PKD);
						databaseHandler.loadPKD(cv);
						showAlertDialogToast1("Saved Successfully");
						navigateToPrevious();
					}catch (Exception e){
						e.printStackTrace();
						showAlertDialogToast1("UnExpected Failed Occur May Technical Assist Need");
					}
				}
			}
		}

	}

	private String insertStockEntry(){
		JSONObject jsonObject = new JSONObject();

		try {
			jsonObject.put("stockarr",jsonArray);
			jsonObject.put("starttime",Constants.startTime);
			jsonObject.put("endtime",new SimpleDateFormat(
					"HH:mm:ss").format(new java.util.Date()));

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObject.toString();
	}

	private	 void saveOrder() {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog= ProgressDialog.show(OutletStockEntryDetails.this, "",
						"Loading...", true, true);
			}

			@Override
			protected void onPostExecute(Void result) {

		try {

					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);

					if(strStatus.equals("true")){
						showAlertDialogToast1(strMsg);
					}else{
						showAlertDialogToast1("Not updated successfully");
					}



				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					//Log.e("ProductActivityException", e.toString());
				}
				dialog.dismiss();
			}

			@Override
			protected Void doInBackground(Void... params) {

				JsonServiceHandler = new JsonServiceHandler(Utils.strstocks,insertStockEntry(), OutletStockEntryDetails.this);
				JsonAccountObject = JsonServiceHandler.ServiceData();


				return null;
			}
		}.execute(new Void[]{});
	}





	protected void toastDisplay(String msg) {
		Toast toast = Toast.makeText(OutletStockEntryDetails.this, msg,
				Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

	}
	public boolean netCheck() {
		// for network connection
		try {
			ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetwork = connectionManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			Object result = null;
			if (mWifi.isConnected() || mNetwork.isConnected()) {
				return true;
			}

			else if (result == null) {

				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	public void showAlertDialog(Context context, String title, String message,
								Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
					}
				});
		alertDialog.show();
	}

	public void showAlertDialogToast( String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();
		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));


	}

	private void navigateToPrevious(){
		Constants.setorder=1;
		Intent io=new Intent(OutletStockEntryDetails.this,OutletStockEntry.class);
		startActivity(io);
		finish();
	}
	public void showAlertDialogToast1( String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(false);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						navigateToPrevious();
						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();
		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));


	}




	public void onBackPressed() {
		exitAlret();
	}

	private void exitAlret() {
		AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
		localBuilder.setCancelable(false);
		localBuilder.setMessage("Do you want to Exit?");
		localBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {

						finish();

					}
				});
		localBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {
						paramDialogInterface.cancel();
					}
				});
		localBuilder.create().show();
	}

	private void hideKeyBoard(){
		// Check if no view has focus:
		View view = this.getCurrentFocus();
		if (view != null) {
			InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			if (imm != null) {
				imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
			}
		}
	}

}
