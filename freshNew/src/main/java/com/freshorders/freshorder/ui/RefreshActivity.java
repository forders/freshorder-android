package com.freshorders.freshorder.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.MDealerCompDropdownDomain;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Pavithra on 10-11-2016.
 */
public class RefreshActivity extends Activity {

    public static String moveto, PaymentStatus = "", salesuserid, Paymentdate, UDID, Duserid, Dfullname, Dcompanyname,
            Daddress, DealerCount = "", prodid, prodname, prodcode, userid, qty, fqty, pdate, pcompanyname, prodprice,
            prodtax, ProductCount = "", MerchantCount = "", stockiest,route,Mcmpnyname, Muserid, Mfullname, merchantmobileno,
            mfgdate, expdate, batchno, mflag, memail, maddress, uom, SchemeCount = "", selected_dealer,
            beatcount = "", ProductSetting = "", currentDate, userbeattype = "", userSetting = "", clientDealerid = "",pan_no,strStockiest,strState,strRoute,customdata;
    public static Context context;
    public static JSONObject JsonAccountObject = null;
    public static JSONArray JsonAccountArray = null;
    public static DatabaseHandler databaseHandler;
    public static JsonServiceHandler JsonServiceHandler;
    String uomid, uomprodid, uomduserid, uomname, uomvalue, uomstatus, uomupdateddt,isdefault;
    int count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.refresh_activity);
        netCheck();
        databaseHandler = new DatabaseHandler(getApplicationContext());
        Cursor cur;
        cur = databaseHandler.getDetails();
        cur.moveToFirst();
        clientDealerid = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_clientdealid));

        currentDate = new SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date());
        context = RefreshActivity.this;
        databaseHandler = new DatabaseHandler(getApplicationContext());
        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();
        moveto = "sync";
        getData();

    }

    public void getData() {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "";

            @Override
            protected void onPreExecute() {
                dialog = ProgressDialog.show(context, "",
                        "Loading...", true, true);

            }

            @Override
            protected void onPostExecute(Void result) {

                try {

                    PaymentStatus = (JsonAccountObject.getString("pymtstatus"));

                    salesuserid = JsonAccountObject.getString("userid");
                    Log.e("PaymentStatus", PaymentStatus);

                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment to place order");
                    } else {
                        if (moveto.equals("sync")) {
                            Paymentdate = "hiiii";
                            ContentValues contentValues = new ContentValues();
                            contentValues.put(DatabaseHandler.KEY_id, salesuserid);
                            contentValues.put(DatabaseHandler.KEY_paymentdate, Paymentdate);
                            databaseHandler.updatesignintable(contentValues);
                            Cursor curs;
                            curs = databaseHandler.getDetails();
                            Log.e("countsignin", String.valueOf(curs.getCount()));
                            if (curs != null && curs.getCount() > 0) {
                                if (curs.moveToFirst()) {

                                    Log.e("nextpayment", String.valueOf(curs.getString(8)));
                                }
                            }
                            offlineDealers();
                        }

                    }


                    dialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();


                JsonServiceHandler = new JsonServiceHandler(Utils.strGetProfileDetail + Constants.USER_ID, context);
                JsonAccountObject = JsonServiceHandler.ServiceDataGet();
                return null;
            }
        }.execute(new Void[]{});
    }


    public void offlineDealers() {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            String strStatus = "", strMsg;

            ProgressDialog dialog;

            @Override
            protected void onPreExecute() {
                dialog = ProgressDialog.show(context, "",
                        "Initializing Dealer table ...", true, true);
                dialog.setCancelable(false);
            }


            @Override
            protected void onPostExecute(Void result) {

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {


                        JSONArray job1 = JsonAccountObject.getJSONArray("data");


                        for (int i = 0; i < job1.length(); i++) {

                            JSONObject job = new JSONObject();
                            job = job1.getJSONObject(i);
                            UDID = job.getString("usrdlrid");
                            Duserid = job.getJSONObject("D").getString("userid");
                            Dfullname = job.getJSONObject("D").getString("fullname");
                            Dcompanyname = job.getJSONObject("D").getString("companyname");
                            Daddress = job.getJSONObject("D").getString("address");

                            String inputFormat = new SimpleDateFormat(
                                    "yyyy-MM-dd HH:mm:ss").format(new Date());
                            Log.e("inputFormat", inputFormat);

                            databaseHandler.adddealer(Dcompanyname, UDID, Duserid, Dfullname, Daddress, inputFormat);


                        }
                        Cursor cur;
                        cur = databaseHandler.getdealer();
                        Log.e("dealercount", String.valueOf(cur.getCount()));
                        DealerCount = String.valueOf(cur.getCount());

                        if (cur != null && cur.getCount() > 0) {
                            if (cur.moveToLast()) {

                                Log.e("Ddate", String.valueOf(cur.getString(6)));
                            }
                        }
                        dialog.dismiss();
                        OfflineProducts();


                    } else {
                        dialog.dismiss();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {

                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();


                JsonServiceHandler = new JsonServiceHandler(Utils.strofflineDealerList + Constants.USER_ID, context);
                JsonAccountObject = JsonServiceHandler.ServiceDataGet();
                return null;
            }
        }.execute(new Void[]{});

    }

    private void DistributorLoad() {
        Log.e("DistributorLoad","DistributorLoad");
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "";

            @Override
            protected void onPreExecute() {
                dialog = ProgressDialog.show(RefreshActivity.this, "",
                        "Distributor Loading...", true, false);

            }

            @Override
            protected void onPostExecute(Void result) {

                try {
                    if(JsonAccountObject != null) {
                        strStatus = JsonAccountObject.getString("status");
                        Log.e("return status", strStatus);
                        strMsg = JsonAccountObject.getString("message");
                        Log.e("return message", strMsg);
                        if (strStatus.equals("true")) {
                            JSONArray distributorArray = JsonAccountObject.getJSONArray("data");
                            if(distributorArray.length() > 0){
                                List<ContentValues> listCV = new ArrayList<>();
                                for(int i = 0; i < distributorArray.length(); i++){
                                    ContentValues cv = new ContentValues();
                                    JSONObject obj = distributorArray.getJSONObject(i);
                                    String distributorId = obj.getString("distributorid");
                                    String distributorName = obj.getString("distributorname");
                                    String mobileNo = obj.getString("mobileno");
                                    cv.put(DatabaseHandler.KEY_DISTRIBUTOR_ID, distributorId);
                                    cv.put(DatabaseHandler.KEY_DISTRIBUTOR_NAME, distributorName);
                                    cv.put(DatabaseHandler.KEY_DISTRIBUTOR_MOBILE, mobileNo);
                                    listCV.add(cv);
                                }
                                databaseHandler.loadDistributor(listCV);
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(dialog != null) {
                        dialog.dismiss();
                        offlineProductSetting();///////////////// 05-08-2019 Kumaravel
                    }
                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();
                try {
                    String suserId = "";
                    if(databaseHandler == null){
                        databaseHandler = new DatabaseHandler(RefreshActivity.this);
                    }
                    Cursor cur = databaseHandler.getDetails();
                    if(cur != null && cur.getCount() > 0){
                        cur.moveToFirst();
                        suserId = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_id));
                        cur.close();
                    }


                    jsonObject.put("suserid",suserId);
                    Log.e("suserid-----",suserId);

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                JsonServiceHandler = new JsonServiceHandler(Utils.strDistributor, jsonObject.toString(),RefreshActivity.this);
                JsonAccountObject = JsonServiceHandler.ServiceData();
                return null;
            }
        }.execute(new Void[]{});
    }

    public void productuom() {
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "", strMsg = "";

            @Override
            protected void onPreExecute() {

                dialog = ProgressDialog.show(context, "",
                        "Initializing Products UOM table ...", true, true);
                dialog.setCancelable(false);
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
                if (!dialog.isShowing()) {
                    ///offlineProductSetting();
                    ///////////////////////////////////05-08-2019 Kumaravel
                    if(databaseHandler == null){
                        databaseHandler = new DatabaseHandler(RefreshActivity.this);
                    }
                    String type = databaseHandler.getSalesmanDataUserSetting("BEAT_TYPE");
                    if(!type.isEmpty()){
                        Log.e("DistributorLoad",".....................type " + type);
                        if(type.equalsIgnoreCase("C")){
                            DistributorLoad();
                        }else {
                            offlineProductSetting();
                        }
                    }
                    ////////////////////////////////////////////

                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JsonServiceHandler = new JsonServiceHandler(Utils.strproductuom + selected_dealer, context);
                JsonAccountObject = JsonServiceHandler.ServiceDataGet();

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {

                        JSONArray job1 = JsonAccountObject.getJSONArray("data");
                        count = job1.length();

                        for (int i = 0; i < job1.length(); i++) {

                            JSONObject job = new JSONObject();

                            try {
                                job = job1.getJSONObject(i);

                                uomid = job.getString("uomid");
                                uomprodid = job.getString("prodid");
                                uomduserid = job.getString("duserid");
                                uomname = job.getString("uomname");
                                uomupdateddt = job.getString("updateddt");
                                uomstatus = job.getString("status");
                                uomvalue = job.getString("uomvalue");
                                isdefault = job.getString("isdefault");
                                Log.e("isdefault",isdefault);

                                databaseHandler.addProductUOM(uomid, uomprodid, uomduserid, uomname, uomvalue, uomupdateddt, uomstatus,isdefault);


                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (Exception e) {

                            }

                        }
                        Cursor cur;
                        cur = databaseHandler.getProductUOM();
                        // ProductCount= String.valueOf(cur.getCount());
                        Log.e("productuomCount", String.valueOf(cur.getCount()));

                    } else {

                    }

                    dialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {

                }

                return null;

            }

        }.execute(new Void[]{});

    }

    public void offlineProductSetting() {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            String strStatus = "", strMsg = "";

            ProgressDialog dialog;

            @Override
            protected void onPreExecute() {
                dialog = ProgressDialog.show(context, "",
                        "Initializing Product Setting table ...", true, true);
                dialog.setCancelable(false);
            }


            @Override
            protected void onPostExecute(Void result) {

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {


                        JSONArray job1 = JsonAccountObject.getJSONArray("data");


                        for (int i = 0; i < job1.length(); i++) {

                            JSONObject job = new JSONObject();
                            job = job1.getJSONObject(i);
                            String prodid = job.getString("prodid");
                            String refkey = job.getString("refkey");
                            String refvalue = job.getString("refvalue");
                            String muserid = job.getString("muserid");
                            String status = job.getString("status");
                            String updateddate = job.getString("updateddt");

                            databaseHandler.addProductStting(prodid, refkey, refvalue, muserid, status, updateddate, "");

                        }
                        Cursor cur;
                        cur = databaseHandler.getProductSetting();
                        Log.e("ProductSetting", String.valueOf(cur.getCount()));
                        ProductSetting = String.valueOf(cur.getCount());


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {

                }
                dialog.dismiss();
                if (ProductCount.equals("null") || ProductCount.isEmpty()) {
                    ProductCount = "0";
                }
                if (MerchantCount.equals("null") || MerchantCount.isEmpty()) {
                    MerchantCount = "0";
                }
                if (SchemeCount.equals("null") || SchemeCount.isEmpty()) {
                    SchemeCount = "0";
                }
                if (beatcount.equals("null") || beatcount.isEmpty()) {
                    beatcount = "0";
                }
                showAlertDialogToast("Stockiest/Dealer Count :" + DealerCount + "\n" + "Product Count :" + ProductCount + "\n" + "Outlet/Merchant Count :" + MerchantCount + "\n" + "Scheme Count :" + SchemeCount);

                Constants.syn = 1;

                dialog.dismiss();
            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();


                JsonServiceHandler = new JsonServiceHandler(Utils.getProductSetting, context);
                JsonAccountObject = JsonServiceHandler.ServiceDataGet();
                return null;
            }
        }.execute(new Void[]{});

    }

    public void OfflineProducts() {
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "", strMsg;

            @Override
            protected void onPreExecute() {

                dialog = ProgressDialog.show(context, "",
                        "Initializing Products table ...", true, true);
                dialog.setCancelable(false);


            }


            @Override
            protected void onPostExecute(Void result) {

                super.onPostExecute(result);
                if (!dialog.isShowing()) {
                    offlineMerchant();
                }
            }


            @Override
            protected Void doInBackground(Void... params) {

                //Do something...
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("userid", Constants.USER_ID);
                    jsonObject.put("status", "Active");

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                JsonServiceHandler = new JsonServiceHandler(Utils.strofflineproduct, jsonObject.toString(), context);
                JsonAccountObject = JsonServiceHandler.ServiceData();

                try {


                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {


                        JSONArray job1 = JsonAccountObject.getJSONArray("data");
                        count = job1.length();
                        int i;
                        for (i = 0; i < job1.length(); i++) {

                            JSONObject job = new JSONObject();
                            try {
                                job = job1.getJSONObject(i);
                                prodid = job.getString("prodid");
                                prodname = job.getString("prodname");
                                prodcode = job.getString("prodcode");
                                userid = job.getString("userid");
                                qty = "";
                                fqty = "";
                                pdate = job.getString("updateddt");
                                pcompanyname = job.getString("companyname");
                                prodprice = job.getString("prodprice");
                                prodtax = job.getString("prodtax");
                                mfgdate = "";
                                expdate = "";
                                batchno = "";
                                uom = job.getString("uom");
                                String unitgram = job.getString("unitsgm");
                                String unitperuom = job.getString("unitperuom");
                                String prodimage=job.getString("prodimage");

                                if (uom.equals(null) || uom.isEmpty() || uom.equals("null")) {
                                    uom = "";
                                }


                                if (prodprice.equals(null) || prodprice.isEmpty() || prodprice.equals("null")) {
                                    prodprice = "0";
                                }

                                if (prodtax.equals(null) || prodtax.isEmpty() || prodtax.equals("null")) {
                                    prodtax = "0";
                                }
                                //currentdate="2016-07-04";
                                String inputFormat = new SimpleDateFormat(
                                        "yyyy-MM-dd HH:mm:ss").format(new Date());

                                databaseHandler.addProd(userid, pcompanyname, prodcode, prodid, prodname, qty, fqty, pdate, inputFormat,
                                        prodprice, prodtax, batchno, mfgdate, expdate, uom, "0", unitgram, unitperuom,prodimage,"");


                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (Exception e) {

                            }


                        }
                        Cursor cur;
                        cur = databaseHandler.getProduct();
                        ProductCount = String.valueOf(cur.getCount());
                        Log.e("productCount", String.valueOf(cur.getCount()));

                        if (i == count) {
                            dialog.dismiss();

                        }


                    } else {
                        dialog.dismiss();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {

                }

                return null;

            }

        }.execute(new Void[]{});

    }

    public void offlineMerchant() {
        new AsyncTask<Void, Void, Void>() {
            String strStatus = "", strMsg;
            ProgressDialog dialog;
            String city;
            @Override

            protected void onPreExecute() {
                super.onPreExecute();
                dialog = ProgressDialog.show(context, "",
                        "Initializing Merchant table ...", true, true);
                dialog.setCancelable(false);
            }

            @Override
            protected void onPostExecute(Void result) {

                super.onPostExecute(result);
                if (!dialog.isShowing()) {
                    offlineUserSetting();
                }

            }

            @Override
            protected Void doInBackground(Void... params) {
                JSONObject jsonObject = new JSONObject();

                try {
                    jsonObject.put("userid", Constants.USER_ID);
                    jsonObject.put("usertype", Constants.USER_TYPE);


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                JsonServiceHandler = new JsonServiceHandler(Utils.strGetMerchantList, jsonObject.toString(), context);
                JsonAccountObject = JsonServiceHandler.ServiceData();
                try {
                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);
                    if (strStatus.equals("true")) {
                        MDealerCompDropdownDomain cd;

                        JSONArray JsonArray = JsonAccountObject.getJSONArray("data");
                        int merchntcount = JsonArray.length();
                        int i;

                        for (i = 0; i < JsonArray.length(); i++) {
                            cd = new MDealerCompDropdownDomain();
                            JsonAccountObject = JsonArray.getJSONObject(i);
                            Mfullname = JsonAccountObject.getString("fullname");
                            Mcmpnyname = JsonAccountObject.getString("companyname");
                            Muserid = JsonAccountObject.getString("userid");
                            merchantmobileno = JsonAccountObject.getString("mobileno");
                            mflag = JsonAccountObject.getString("cflag");
                            memail = JsonAccountObject.getString("emailid");
                            maddress = JsonAccountObject.getString("address");
                            city = JsonAccountObject.getString("city");
                            strStockiest=JsonAccountObject.getString("stockiest");
                            strRoute=JsonAccountObject.getString("route");
                            String date[]=JsonAccountObject.getString("updateddt").split("\\s+");
                            Log.e("newdate",date[0]);
                            // String day=JsonAccountObject.getString("address");

                            databaseHandler.addmerchant(Mcmpnyname, Muserid, Mfullname, merchantmobileno, mflag, memail, maddress, city, "", "", "", "Tuesday", date[0],pan_no,customdata,strStockiest,strRoute,strState);


                        }


                        Cursor cur;
                        cur = databaseHandler.getmerchant();
                        Log.e("merchantcount", String.valueOf(cur.getCount()));
                        MerchantCount = String.valueOf(cur.getCount());

                        if (i == merchntcount) {
                            dialog.dismiss();
                        }
                    } else {
                        dialog.dismiss();

                    }

                } catch (JSONException e) {
                    Log.e("JSONException", e.toString());

                } catch (Exception e) {
                    Log.e("Exception", e.toString());


                }

                return null;
            }
        }.execute(new Void[]{});
    }

    public void offlineUserSetting() {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            String strStatus = "", strMsg = "";

            ProgressDialog dialog;

            @Override
            protected void onPreExecute() {
                dialog = ProgressDialog.show(context, "",
                        "Initializing User Setting table ...", true, true);
                dialog.setCancelable(false);
            }


            @Override
            protected void onPostExecute(Void result) {

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {


                        JSONArray job1 = JsonAccountObject.getJSONArray("data");


                        for (int i = 0; i < job1.length(); i++) {

                            JSONObject job = new JSONObject();
                            job = job1.getJSONObject(i);
                            String userid = job.getString("userid");
                            String refkey = job.getString("refkey");
                            String refvalue = job.getString("refvalue");
                            userbeattype = job.getString("refvalue");
                            String remarks = job.getString("remarks");
                            String status = job.getString("status");
                            Log.e("status", "status");
                            String updateddate = job.getString("updateddt");

                            databaseHandler.addUserSetting(userid, refkey, refvalue, updateddate, status, remarks);
                        }
                        Cursor cur;
                        cur = databaseHandler.getUserSetting();
                        Log.e("UserSetting", String.valueOf(cur.getCount()));
                        userSetting = String.valueOf(cur.getCount());
                        cur.moveToFirst();
                        if (cur != null && cur.getCount() > 0) {
                            if (cur.moveToLast()) {

                                Log.e("SettingModel", String.valueOf(cur.getString(3)));
                            }
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {

                }
                dialog.dismiss();
                if (userbeattype.equalsIgnoreCase("C")) {
                    offlineCompanyModelBeat();
                } else {
                    offlineDistributorModelBeat();
                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();


                JsonServiceHandler = new JsonServiceHandler(Utils.getUserSetting + clientDealerid, context);
                JsonAccountObject = JsonServiceHandler.ServiceDataGet();
                return null;
            }
        }.execute(new Void[]{});

    }

    public void offlineDistributorModelBeat() {
        new AsyncTask<Void, Void, Void>() {
            String strStatus = "", strMsg = "";

            ProgressDialog dialog;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialog = ProgressDialog.show(context, "",
                        "Initializing Beat table ...", true, true);
                dialog.setCancelable(false);
            }


            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
                if (!dialog.isShowing()) {
                    Cursor cursor;
                    cursor = databaseHandler.getdealer();
                    Log.e("count", String.valueOf(cursor.getCount()));
                    if (cursor.getCount() > 0) {
                        cursor.moveToFirst();
                        selected_dealer = cursor.getString(cursor
                                .getColumnIndex(DatabaseHandler.KEY_Duserid));
                    }

                    offlineScheme(selected_dealer);

                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JsonServiceHandler = new JsonServiceHandler(Utils.strbeat + Constants.USER_ID + "&filter[where][status]=Active", context);
                JsonAccountObject = JsonServiceHandler.ServiceDataGet();

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);
                    int i;
                    if (strStatus.equals("true")) {


                        JSONArray job1 = JsonAccountObject.getJSONArray("data");

                        int beatcount1 = job1.length();
                        for (i = 0; i < job1.length(); i++) {

                            JSONObject job = new JSONObject();
                            job = job1.getJSONObject(i);
                            String beatid = job.getString("beatid");
                            String muserid = job.getString("muserid");
                            String suserid = job.getString("suserid");
                            String duserid = job.getString("duserid");
                            String day_mon = job.getString("day_mon");
                            String day_tue = job.getString("day_tue");
                            String day_wed = job.getString("day_wed");
                            String day_thu = job.getString("day_thu");
                            String day_fri = job.getString("day_fri");
                            String day_sat = job.getString("day_sat");
                            String day_sun = job.getString("day_sun");
                            String status = job.getString("status");
                            String beattype = "D"; //job.getString("beattype")
                            String beatname = ""; //job.getString("beatname")


                            String inputFormat1 = new SimpleDateFormat(
                                    "yyyy-MM-dd HH:mm:ss").format(new Date());
                            Log.e("inputFormat", inputFormat1);

                            databaseHandler.addDistributorModelbeat(beatid, muserid, suserid, duserid, day_mon, day_tue,
                                    day_wed, day_thu, day_fri, day_sat, day_sun, status, inputFormat1, inputFormat1, "", "", beattype, beatname);


                        }
                        Cursor cur;
                        cur = databaseHandler.getbeat();
                        Log.e("addDistributorModelbeat", String.valueOf(cur.getCount()));
                        beatcount = String.valueOf(cur.getCount());


                        if (i == beatcount1) {

                            dialog.dismiss();
                        }

                    } else {
                        dialog.dismiss();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {

                }

                return null;
            }
        }.execute(new Void[]{});
    }

    public void offlineCompanyModelBeat() {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            String strStatus = "", strMsg = "";

            ProgressDialog dialog;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialog = ProgressDialog.show(context, "",
                        "Initializing Beat table ...", true, true);
                dialog.setCancelable(false);
            }


            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
                if (!dialog.isShowing()) {
                    Cursor cursor;
                    cursor = databaseHandler.getdealer();
                    Log.e("count", String.valueOf(cursor.getCount()));
                    if (cursor.getCount() > 0) {
                        cursor.moveToFirst();
                        selected_dealer = cursor.getString(cursor
                                .getColumnIndex(DatabaseHandler.KEY_Duserid));
                    }

                    offlineScheme(selected_dealer);

                }

            }

            @Override
            protected Void doInBackground(Void... params) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("suserid", Constants.USER_ID);


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                JsonServiceHandler = new JsonServiceHandler(Utils.getdistributor, jsonObject.toString(), context);
                JsonAccountObject = JsonServiceHandler.ServiceData();

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);
                    int i;
                    if (strStatus.equals("true")) {


                        JSONArray job1 = JsonAccountObject.getJSONArray("data");

                        int beatcount1 = job1.length();
                        for (i = 0; i < job1.length(); i++) {

                            JSONObject job = new JSONObject();
                            Log.e("job------", String.valueOf(job));
                            job = job1.getJSONObject(i);
                            Log.e("job1-------", String.valueOf(job1));
                            String beatid = job.getString("id");
                            Log.e("beatid", beatid);
                            String muserid = ""; //job.getString("muserid")
                            String suserid = Constants.USER_ID;
                            Log.e("suserid", suserid);//job.getString("suserid")
                            String duserid = job.getString("duserid");
                            Log.e("duserid", duserid);
                            String day_mon = job.getString("day_mon");
                            Log.e("day_mon", day_mon);
                            String day_tue = job.getString("day_tue");
                            Log.e("day_tue", day_tue);
                            String day_wed = job.getString("day_wed");
                            Log.e("day_wed", day_wed);
                            String day_thu = job.getString("day_thu");
                            Log.e("day_thu", day_thu);
                            String day_fri = job.getString("day_fri");
                            Log.e("day_fri", day_fri);
                            String day_sat = job.getString("day_sat");
                            Log.e("day_sat", day_sat);
                            String day_sun = job.getString("day_sun");
                            Log.e("day_sun", day_sun);
                            String status = job.getString("status");
                            Log.e("status", status);
                            String beattype = job.getString("beattype");
                            Log.e("beattype", beattype);
                            String beatname = job.getString("beatnm");
                            Log.e("beatname", beatname);
                            String fullname = job.getString("fullname");
                            Log.e("fullname", fullname);
                            String usertype = job.getString("usertype");
                            Log.e("usertype", usertype);
                            String companyname = job.getString("companyname");
                            Log.e("companyname", companyname);

                            String inputFormat1 = new SimpleDateFormat(
                                    "yyyy-MM-dd HH:mm:ss").format(new Date());
                            Log.e("inputFormat", inputFormat1);

                            databaseHandler.addCompanyModelbeat(beatid, muserid, suserid, duserid, day_mon, day_tue,
                                    day_wed, day_thu, day_fri, day_sat, day_sun, status, inputFormat1, inputFormat1, "", "", beattype, beatname, fullname, usertype, companyname);
                            offlineCompanyBeatMerchant(beatid, beatname);


                        }
                        Cursor cur;
                        cur = databaseHandler.getbeat();
                        Log.e("addCompanyModelbeat", String.valueOf(cur.getCount()));
                        beatcount = String.valueOf(cur.getCount());


                        if (i == beatcount1) {

                            dialog.dismiss();
                        }

                    } else {
                        dialog.dismiss();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {

                }

                return null;
            }
        }.execute(new Void[]{});

    }

    public void offlineCompanyBeatMerchant(final String beatid, final String beatname) {
        new AsyncTask<Void, Void, Void>() {
            String strStatus = "", strMsg = "";

            ProgressDialog dialog;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

            }


            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);

            }

            @Override
            protected Void doInBackground(Void... params) {
                Cursor cursor;
                cursor = databaseHandler.getdealer();
                Log.e("count", String.valueOf(cursor.getCount()));
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    selected_dealer = cursor.getString(cursor
                            .getColumnIndex(DatabaseHandler.KEY_Duserid));
                }
                JsonServiceHandler = new JsonServiceHandler(Utils.getCompanyBeatMerchant + selected_dealer + "&filter[where][id]=" + beatid, context);
                JsonAccountObject = JsonServiceHandler.ServiceDataGet();

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);
                    int i;
                    if (strStatus.equals("true")) {

                        String muserid = "";
                        JSONArray job1 = JsonAccountObject.getJSONArray("data");

                        for (i = 0; i < job1.length(); i++) {

                            JSONObject job = new JSONObject();
                            job = job1.getJSONObject(i);

                            if (i == 0) {
                                muserid = job.getString("muserid");
                            } else {
                                muserid = muserid + "," + job.getString("muserid");
                            }


                        }
                        Log.e("muserid", muserid);
                        databaseHandler.updateCompanyBeatMerchant(muserid, beatname);
                        Cursor cur;
                        cur = databaseHandler.getUpdatedBeat(muserid, beatname);
                        Log.e("addCompanyModelbeat", String.valueOf(cur.getCount()));
                        beatcount = String.valueOf(cur.getCount());

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {

                }

                return null;
            }
        }.execute(new Void[]{});

    }

    public void offlineScheme(final String selected_dealer) {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            String strStatus = "", strMsg = "";

            ProgressDialog dialog;

            @Override
            protected void onPreExecute() {
                dialog = ProgressDialog.show(context, "",
                        "Initializing Scheme table ...", true, true);
                dialog.setCancelable(false);
            }


            @Override
            protected void onPostExecute(Void result) {

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {


                        JSONArray job1 = JsonAccountObject.getJSONArray("data");


                        for (int i = 0; i < job1.length(); i++) {

                            JSONObject job = new JSONObject();
                            job = job1.getJSONObject(i);
                            String schmeduserid = job.getString("duserid");
                            String schemeprodid = job.getString("prodid");
                            String schemestartdate = job.getString("start_date");
                            String schemeenddate = job.getString("end_date");
                            String schemetype = job.getString("schemetype");
                            String schemediscount = job.getString("discount");
                            String schemefreeProdid = job.getString("free_prodid");
                            String schemefreeQty = job.getString("free_qty");
                            String schemeremark = job.getString("remarks");
                            String schemestatus = job.getString("status");

                            databaseHandler.addSchemes(schemeprodid, schmeduserid, schemestartdate, schemeenddate, schemediscount, schemetype,
                                    schemefreeProdid, schemefreeQty, schemestatus, schemeremark);


                        }


                        dialog.dismiss();

                    } else {
                        dialog.dismiss();

                    }

                    Cursor cur;
                    cur = databaseHandler.getSchemeCount();
                    Log.e("SchemeCount", String.valueOf(cur.getCount()));
                    SchemeCount = String.valueOf(cur.getCount());
                    productuom();
                    // offlineProductSetting();


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {

                }

            }

            @Override
            protected Void doInBackground(Void... params) {
                JSONObject jsonObject = new JSONObject();

                try {
                    jsonObject.put("duserid", selected_dealer);


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                JsonServiceHandler = new JsonServiceHandler(Utils.strgetScheme, jsonObject.toString(), context);
                JsonAccountObject = JsonServiceHandler.ServiceData();
                return null;
            }
        }.execute(new Void[]{});

    }

    public void offlinebeat() {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            String strStatus = "", strMsg = "";

            ProgressDialog dialog;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialog = ProgressDialog.show(context, "",
                        "Initializing Beat table ...", true, true);
                dialog.setCancelable(false);
            }


            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
                if (!dialog.isShowing()) {
                    Cursor cursor;
                    cursor = databaseHandler.getdealer();
                    Log.e("count", String.valueOf(cursor.getCount()));
                    if (cursor.getCount() > 0) {
                        cursor.moveToFirst();
                        selected_dealer = cursor.getString(cursor
                                .getColumnIndex(DatabaseHandler.KEY_Duserid));
                    }

                    offlineScheme(selected_dealer);

                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JsonServiceHandler = new JsonServiceHandler(Utils.strbeat + Constants.USER_ID + "&filter[where][status]=Active", context);
                JsonAccountObject = JsonServiceHandler.ServiceDataGet();

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);
                    int i;
                    if (strStatus.equals("true")) {


                        JSONArray job1 = JsonAccountObject.getJSONArray("data");

                        int beatcount1 = job1.length();
                        for (i = 0; i < job1.length(); i++) {

                            JSONObject job = new JSONObject();
                            job = job1.getJSONObject(i);
                            String beatid = job.getString("beatid");
                            String muserid = job.getString("muserid");
                            String suserid = job.getString("suserid");
                            String duserid = job.getString("duserid");
                            String day_mon = job.getString("day_mon");
                            String day_tue = job.getString("day_tue");
                            String day_wed = job.getString("day_wed");
                            String day_thu = job.getString("day_thu");
                            String day_fri = job.getString("day_fri");
                            String day_sat = job.getString("day_sat");
                            String day_sun = job.getString("day_sun");
                            String status = job.getString("status");
                            String beattype = job.getString("beattype");
                            String beatname = job.getString("beatname");
                            String fullname = job.getString("fullname");
                            String usertype = job.getString("usertype");
                            String companyname = job.getString("companyname");

                            String inputFormat1 = new SimpleDateFormat(
                                    "yyyy-MM-dd HH:mm:ss").format(new Date());
                            Log.e("inputFormat", inputFormat1);

                            databaseHandler.addCompanyModelbeat(beatid, muserid, suserid, duserid, day_mon, day_tue,
                                    day_wed, day_thu, day_fri, day_sat, day_sun, status, inputFormat1, inputFormat1, "", "", beattype, beatname, fullname, usertype, companyname);


                        }
                        Cursor cur;
                        cur = databaseHandler.getbeat();
                        Log.e("beatcount", String.valueOf(cur.getCount()));
                        beatcount = String.valueOf(cur.getCount());


                        if (i == beatcount1) {

                            dialog.dismiss();
                        }

                    } else {
                        dialog.dismiss();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {

                }

                return null;
            }
        }.execute(new Void[]{});

    }

    public void showAlertDialogToast(String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        if (Constants.refreshscreen.equals("createorder")) {
                            Intent to = new Intent(context,
                                    CreateOrderActivity.class);
                            context.startActivity(to);
                            finish();
                        } else if (Constants.refreshscreen.equals("mydealer")) {
                            Intent io = new Intent(context,
                                    MyDealersActivity.class);
                            io.putExtra("Key", "menuclick");
                            startActivity(io);
                            finish();
                        } else if (Constants.refreshscreen.equals("postnotes")) {
                            Intent io = new Intent(RefreshActivity.this,
                                    MerchantComplaintActivity.class);
                            startActivity(io);
                            finish();
                        } else if (Constants.refreshscreen.equals("clientvisit")) {
                            Intent to = new Intent(RefreshActivity.this,
                                    SMClientVisitHistory.class);
                            startActivity(to);
                            finish();
                        } else if (Constants.refreshscreen.equals("paymentcollection")) {
                            Intent to = new Intent(RefreshActivity.this,
                                    SalesmanPaymentCollectionActivity.class);
                            startActivity(to);
                            finish();
                        } else if (Constants.refreshscreen.equals("profile")) {
                            Intent io = new Intent(RefreshActivity.this,
                                    ProfileActivity.class);
                            startActivity(io);
                            finish();

                        } else if (Constants.refreshscreen.equals("myorder")) {
                            Intent io = new Intent(RefreshActivity.this,
                                    SalesManOrderActivity.class);
                            startActivity(io);
                            finish();

                        } else if (Constants.refreshscreen.equals("addmerchant")) {
                            Intent io = new Intent(RefreshActivity.this,
                                    AddMerchantNew.class);
                            startActivity(io);
                            finish();

                        } else if (Constants.refreshscreen.equals("stockentry")) {
                            Intent io = new Intent(RefreshActivity.this,
                                    OutletStockEntry.class);
                            startActivity(io);
                            finish();

                        } else if (Constants.refreshscreen.equals("acknowledge")) {
                            Intent io = new Intent(RefreshActivity.this,
                                    SalesmanAcknowledgeActivity.class);
                            startActivity(io);
                            finish();

                        } else if (Constants.refreshscreen.equals("mydayplan")) {
                            Intent io = new Intent(RefreshActivity.this,
                                    MyDayPlan.class);
                            startActivity(io);
                            finish();

                        }
                        else if (Constants.refreshscreen.equals("mysales")) {
                            Intent io = new Intent(RefreshActivity.this,
                                    MySalesActivity.class);
                            startActivity(io);
                            finish();

                        }
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    public boolean netCheck() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            } else if (result == null) {
                showAlertDialog(RefreshActivity.this,
                        "No Internet Connection",
                        "Please Check Your internet connection.", false);
                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public void showAlertDialog(Context context, String title, String message,
                                Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        alertDialog.show();
    }
}
