package com.freshorders.freshorder.ui;





import com.freshorders.freshorder.R;
import android.app.Activity;
import android.app.ProgressDialog;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;


public class webViewActivityForPayment extends Activity  {
	
	static WebView webView;
	LinearLayout linearLayoutActivityBack, linearLayoutMoreBottomButton, linearLayoutShare;
	View progressBarActivityLoading;
	ImageView imageViewWebViewClose;
	ProgressBar progressBarWebView;
	 String url ;
	//Progress bar declerations
			ProgressDialog progressBar;
			private int progressBarStatus = 0;
			private Handler progressBarHandler = new Handler();
			private long fileSize = 0;
			
	public webViewActivityForPayment(){}
	
//	@Override
//	  public void setUserVisibleHint(boolean isVisibleToUser) {
//	    super.setUserVisibleHint(isVisibleToUser);
//	    if (isVisibleToUser) { }
//	    else { 
//	    	webView.stopLoading();
//	    	TabsFragment.progressBarLoading.setVisibility(View.GONE);
//			TabsFragment.imageViewSync.setVisibility(View.VISIBLE);
//	    }
//	  }
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		webViewActivityForPayment.this.requestWindowFeature(1);
		setContentView(R.layout.activity_webview);
		  webView = (WebView) findViewById(R.id.webViewActivity);
		  imageViewWebViewClose=(ImageView)findViewById(R.id.imageViewBack) ;
		  WebSettings settings = webView.getSettings();
		  settings.setJavaScriptEnabled(true);
		  settings.setBuiltInZoomControls(true);
		  settings.setSupportZoom(true);
		  settings.setLoadWithOverviewMode(true);
		  settings.setUseWideViewPort(true);
		  settings.setDomStorageEnabled(true);
		 
		  
		  
			// prepare for a progress bar dialog
		//	progressBar = new ProgressDialog(webViewActivity.this);
		//	progressBar.setCancelable(true);
		//	progressBar.setMessage("File downloading ...");
		//	progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		 /* progressBarWebView.setProgress(0);
		  progressBarWebView.setMax(100);
		  progressBarWebView.setVisibility(View.VISIBLE);

			//reset progress bar status
			progressBarStatus = 0;
				
			//reset filesize
			fileSize = 0;
				
			new Thread(new Runnable() {
			  public void run() {
				while (progressBarStatus < 100) {

				  // process some tasks
				  progressBarStatus = doSomeTasks();

				  // your computer is too fast, sleep 1 second
				  try {
					Thread.sleep(1000);
				  } catch (InterruptedException e) {
					e.printStackTrace();
				  }

				  // Update the progress bar
				  progressBarHandler.post(new Runnable() {
					public void run() {
						progressBarWebView.setProgress(progressBarStatus);
					}
				  });
				}

				// ok, file is downloaded,
				if (progressBarStatus >= 100) {

					// sleep 2 seconds, so that you can see the 100%
					try {
						Thread.sleep(20000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					// close the progress bar dialog
					//progressBar.dismiss();
					 progressBarWebView.setVisibility(View.GONE);
				}
			  }
		       }).start();*/
		  
		  CountDownTimer ct = new CountDownTimer(8000, 1000) {

				@Override
				public void onTick(long millisUntilFinished) {
					// TODO Auto-generated method stub
			//		 progressBarWebView.setVisibility(View.VISIBLE);
				//	 progressBarWebView.setClickable(true);
				}

				@Override
				public void onFinish() {
					//Validation for intro screen...
				//	 progressBarWebView.setVisibility(View.GONE);

				}
			};
			ct.start();
		  
		  
	
		     url =getIntent().getStringExtra("longURL");
		    Log.e("url", url);
	        
        	startWebView(url);
        	
        
        	imageViewWebViewClose.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					finish();
				}
			});
        	
    }
	
	
	private void startWebView(String paramString) {
		webView.setWebViewClient(new WebViewClient() {
			//ProgressDialog progressDialog;
			//int i = 0;
			@Override
			public void onLoadResource(WebView paramWebView, String paramString) {
		//		progressBarActivityLoading.setVisibility(View.VISIBLE);
				if(paramString!=url){
					Log.e("11","11");
				}
				Log.e("onLoadResource",paramString);
				
			}
			@Override
			public void onPageFinished(WebView paramWebView, String paramString) {
		//		progressBarActivityLoading.setVisibility(View.GONE);
				Log.e("onPageFinished",paramString);
if(paramString!=url){
	Log.e("225","225");
				}
				// close the progress bar dialog
				//progressBar.dismiss();
			}
			@Override
			public boolean shouldOverrideUrlLoading(WebView paramWebView,
					String paramString) {
				paramWebView.loadUrl(paramString);
				Log.e("shouldOverrideUrlLoading",paramString);
				return super.shouldOverrideUrlLoading(paramWebView, paramString);
			}
			
			
		  @Override
		  public void onReceivedSslError (WebView view, SslErrorHandler handler, SslError error) {
			  super.onReceivedSslError(view, handler, error);
			  handler.proceed();
			  Log.e("onReceivedSslError",error.toString());
			  }
		});
		webView.loadUrl(paramString);
	}
	
	
	
	@Override
	public void onBackPressed() {
		 if(webView.canGoBack()){
             webView.goBack();
         }else{
             finish();
         }
	}
	
	// file download simulator... a really simple
				public int doSomeTasks() {

					while (fileSize <= 1000000) {

						fileSize++;

						if (fileSize == 100000) {
							return 10;
						} else if (fileSize == 200000) {
							return 20;
						} else if (fileSize == 300000) {
							return 30;
						}
						// ...add your own

					}

					return 100;

				}
	
}

