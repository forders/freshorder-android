package com.freshorders.freshorder.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.utils.Constants;

import java.io.File;

/**
 * Created by ragul on 01/09/16.
 */
public class ViewFullImage extends Activity  {
    ProgressDialog dialog;

    ImageView img;
    Bitmap bitmap;
    LinearLayout linearLayoutBack;
    TextView textViewAssetMenu,textviewdeletemenu;
    String path;
    int pathnew;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.fullimageview);

        linearLayoutBack =(LinearLayout) findViewById(R.id.linearLayoutBack);
        textViewAssetMenu =(TextView) findViewById(R.id.textViewAssetMenu);
        textviewdeletemenu=(TextView) findViewById(R.id.textviewdeletemenu);
        Typeface font = Typeface.createFromAsset(getAssets(),
                "fontawesome-webfont.ttf");
        textViewAssetMenu.setTypeface(font);
        textviewdeletemenu.setTypeface(font);
        dialog = ProgressDialog.show(ViewFullImage.this, "",
                "Loading...", true, true);
        dialog.setCancelable(false);
        Bundle extras = getIntent().getExtras();
         pathnew = extras.getInt("position");

        img = (ImageView) findViewById(R.id.image);

        DownloadImage(pathnew);
        textviewdeletemenu.setVisibility(View.VISIBLE);

        textviewdeletemenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MerchantOrderActivity.arrayListimagebitmap.remove(pathnew);
                MerchantOrderActivity.arraylistimagepath.remove(pathnew);
                Constants.adapterset=1;
                Intent to = new Intent(ViewFullImage.this,CameraPhotoCapture.class);
                startActivity(to);
                ViewFullImage.this.finish();
            }
        });
        linearLayoutBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.adapterset=1;
                Intent to = new Intent(ViewFullImage.this,CameraPhotoCapture.class);
                startActivity(to);
                ViewFullImage.this.finish();

            }
        });

    }
    public void DownloadImage(Integer pathnew){
     File imgFile = new File(MerchantOrderActivity.arraylistimagepath.get(pathnew));
        bitmap= BitmapFactory.decodeFile(imgFile.getAbsolutePath());
        BitmapDrawable d = new BitmapDrawable(getResources(), bitmap);
        img.setImageDrawable(d);

        dialog.dismiss();
        //img.setImageBitmap(MerchantOrderActivity.arrayListimagebitmap.get(pathnew));
    }


    public void onBackPressed() {
        exitAlret();
    }

    private void exitAlret() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        localBuilder.setMessage("Do you want to Exit?");
        localBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {

                        finish();

                    }
                });
        localBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {
                        paramDialogInterface.cancel();
                    }
                });
        localBuilder.create().show();
    }
}
