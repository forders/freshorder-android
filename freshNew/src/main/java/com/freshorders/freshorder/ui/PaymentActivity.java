package com.freshorders.freshorder.ui;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandlerForPayment;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;
import com.instamojo.android.Instamojo;
import com.instamojo.android.activities.PaymentDetailsActivity;
import com.instamojo.android.callbacks.OrderRequestCallBack;
import com.instamojo.android.models.Errors;
import com.instamojo.android.models.Order;
import com.instamojo.android.network.Request;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telecom.Call;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.security.auth.callback.Callback;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Response;

public class PaymentActivity extends Activity {

	TextView menuIcon, textViewAssetMenuProfile, textViewAssetMenuMyOrders,
			textViewAssetMenuMyDealers, textViewAssetMenuProducts,
			textViewAssetMenuPayment, textViewAssetMenuComplaint,
			textViewAssetMenuSignout,textViewAssetMenuCreateOrder,textViewPaymentStatus,textViewAssetMenuExport,textViewAssetMenuClientVisit;
	Date lastdateStr1 = null,nextdateStr1=null;
	EditText EditTextViewMobile,EditTextViewAmount;
	int menuCliccked;
	LinearLayout linearLayoutMenuParent, linearLayoutProfile,
			linearLayoutMyOrders, linearLayoutMyDealers, linearLayoutProducts,
			linearLayoutPayment,linearLayoutCreateOrder, linearLayoutComplaint,
			linearLayoutSignout,linearLayoutExport,linearLayoutclientVisit;
	JSONObject JsonAccountObject = null;
	JSONArray JsonAccountArray = null;
	DatabaseHandler databaseHandler;
	static JsonServiceHandlerForPayment JsonServiceHandler;
	JsonServiceHandler JsonServiceHandler1;
	JsonServiceHandler JsonServiceHandler2;
	Button buttonPay,buttonhistory;
	String longURL,paymentId,paymentAmount;
	private ProgressDialog dialog;
	private String currentEnv = "https://api.instamojo.com/";
	private String accessToken = null,PaymentOrderId,PaymentTransactionId,Paymenttrcn_mojo_id,pymtstatus;
	public static String UsrName,UsrEmail,UsrMobile,lastPayment,nextPayment,PaymentStatus="null",moveto="NULL";


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Instamojo.initialize(this);
		requestWindowFeature(1);
		setContentView(R.layout.payment_screen);

		databaseHandler = new DatabaseHandler(getApplicationContext());
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();
		textViewPaymentStatus = (TextView) findViewById(R.id.textViewPaymentStatus);


		dialog = new ProgressDialog(this);
		dialog.setIndeterminate(true);
		dialog.setMessage("please wait...");
		dialog.setCancelable(false);

		Cursor cur;
		cur = databaseHandler.getDetails();
		cur.moveToFirst();

		Constants.PAYMENT=cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_payment));
		Log.e("Insertion Check",
				cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_username)));
		Log.e("Insertion Check",
				cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_password)));
		Log.e("Insertion Check",
				cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_usertype)));
		String payments=cur.getString(cur
				.getColumnIndex(DatabaseHandler.KEY_paymentStatus));


		EditTextViewMobile= (EditText) findViewById(R.id.EditTextViewMobile);
		EditTextViewAmount= (EditText) findViewById(R.id.EditTextViewAmount);
		if(netCheck()==true){
			getDetails();
		}

		buttonhistory= (Button) findViewById(R.id.buttonhistory);
		buttonPay= (Button) findViewById(R.id.buttonPay);
		linearLayoutCreateOrder = (LinearLayout) findViewById(R.id.linearLayoutCreateOrder);
		linearLayoutProfile = (LinearLayout) findViewById(R.id.linearLayoutProfile);
		linearLayoutMyOrders = (LinearLayout) findViewById(R.id.linearLayoutMyOrders);
		linearLayoutMyDealers = (LinearLayout) findViewById(R.id.linearLayoutMyDealers);
		linearLayoutProducts = (LinearLayout) findViewById(R.id.linearLayoutProducts);
		linearLayoutPayment = (LinearLayout) findViewById(R.id.linearLayoutPayment);
		linearLayoutComplaint = (LinearLayout) findViewById(R.id.linearLayoutComplaint);
		linearLayoutSignout = (LinearLayout) findViewById(R.id.linearLayoutSignout);
		linearLayoutExport= (LinearLayout) findViewById(R.id.linearLayoutExport);
		linearLayoutclientVisit= (LinearLayout) findViewById(R.id.linearLayoutclientVisit);
		if (Constants.USER_TYPE.equals("D")) {
			linearLayoutMyDealers.setVisibility(View.GONE);
			linearLayoutProducts.setVisibility(View.VISIBLE);
			linearLayoutExport.setVisibility(View.VISIBLE);
			linearLayoutCreateOrder.setVisibility(View.GONE);
		} else if (Constants.USER_TYPE.equals("M")){
			linearLayoutMyDealers.setVisibility(View.VISIBLE);
			linearLayoutProducts.setVisibility(View.GONE);
			linearLayoutCreateOrder.setVisibility(View.GONE);
			linearLayoutExport.setVisibility(View.GONE);
			linearLayoutclientVisit.setVisibility(View.GONE);

		}else {
			linearLayoutMyDealers.setVisibility(View.VISIBLE);
			linearLayoutProducts.setVisibility(View.GONE);
			linearLayoutCreateOrder.setVisibility(View.VISIBLE);
			linearLayoutExport.setVisibility(View.GONE);
		}

		menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
		textViewAssetMenuCreateOrder= (TextView) findViewById(R.id.textViewAssetMenuCreateOrder);
		textViewAssetMenuProfile = (TextView) findViewById(R.id.textViewAssetMenuProfile);
		textViewAssetMenuMyOrders = (TextView) findViewById(R.id.textViewAssetMenuMyOrders);
		textViewAssetMenuMyDealers = (TextView) findViewById(R.id.textViewAssetMenuMyDealers);
		textViewAssetMenuProducts = (TextView) findViewById(R.id.textViewAssetMenuProducts);
		textViewAssetMenuPayment = (TextView) findViewById(R.id.textViewAssetMenuPayment);
		textViewAssetMenuComplaint = (TextView) findViewById(R.id.textViewAssetMenuComplaint);
		textViewAssetMenuSignout = (TextView) findViewById(R.id.textViewAssetMenuSignout);
		textViewAssetMenuExport = (TextView) findViewById(R.id.textViewAssetMenuExport);
		textViewAssetMenuClientVisit= (TextView) findViewById(R.id.textViewAssetMenuClientVisit);
		linearLayoutMenuParent = (LinearLayout) findViewById(R.id.linearLayoutMenuParent);
		Typeface font = Typeface.createFromAsset(getAssets(),
				"fontawesome-webfont.ttf");
		menuIcon.setTypeface(font);
		textViewAssetMenuCreateOrder.setTypeface(font);
		textViewAssetMenuProfile.setTypeface(font);
		textViewAssetMenuMyOrders.setTypeface(font);
		textViewAssetMenuMyDealers.setTypeface(font);
		textViewAssetMenuProducts.setTypeface(font);
		textViewAssetMenuPayment.setTypeface(font);
		textViewAssetMenuComplaint.setTypeface(font);
		textViewAssetMenuSignout.setTypeface(font);
		textViewAssetMenuExport.setTypeface(font);
		textViewAssetMenuClientVisit.setTypeface(font);

		if(cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_usertype)).equals("M")){

			linearLayoutclientVisit.setVisibility(View.GONE);
		}

		buttonhistory.setOnClickListener(new OnClickListener() {
	@Override
	public void onClick(View v) {

		Intent to = new Intent(PaymentActivity.this,
				AppPaymentHistoryActivity.class);
		startActivity(to);
		finish();
	}
});

		buttonPay.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub


				paymentAmount = EditTextViewAmount.getText().toString();
				OnlinePayment();

				//Instamojo.setBaseUrl("https://test.instamojo.com/");
				Instamojo.setBaseUrl("https://api.instamojo.com/");
			}
		});
		menuIcon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (menuCliccked == 0) {
					linearLayoutMenuParent.setVisibility(View.VISIBLE);
					menuCliccked = 1;
				} else {
					linearLayoutMenuParent.setVisibility(View.GONE);
					menuCliccked = 0;
				}

			}
		});

		linearLayoutExport.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				Constants.filter="0";
				if(netCheckwithoutAlert()==true){
					getDetail("export");
				}else{
					showAlertDialogToast("Please Check Your internet connection");
				}


			}
		});

		linearLayoutMenuParent.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
			}
		});

		linearLayoutProfile.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;

				if(netCheckwithoutAlert()==true){
					getDetail("profile");
				}else{
					showAlertDialogToast("Please Check Your internet connection");
				}
			}
		});

		linearLayoutMyOrders.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Constants.checkproduct = 0;
				Constants.orderstatus="Success";
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				// TODO Auto-generated method stub

				//if(netCheckwithoutAlert()==true){
					getDetail("myorders");
				//}else{
				//	showAlertDialogToast("Please Check Your internet connection");
				//}


			}
		});

		linearLayoutMyDealers.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;

				if(netCheckwithoutAlert()==true){
					getDetail("Mydealer");
				}else{
					showAlertDialogToast("Please Check Your internet connection");
				}

			}
		});

		linearLayoutProducts.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;

				if(netCheckwithoutAlert()==true){
					getDetail("products");
				}else{
					showAlertDialogToast("Please Check Your internet connection");
				}


			}
		});

		linearLayoutPayment.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				if(netCheckwithoutAlert()==true){

					Intent io = new Intent(PaymentActivity.this,
							PaymentActivity.class);
					startActivity(io);
					finish();
				}else{
					showAlertDialogToast("Please Check Your internet connection");
				}


			}
		});

		linearLayoutCreateOrder.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;

				Constants.checkproduct=0;
				Constants.orderid="";
				//if(netCheckwithoutAlert()==true){
					getDetail("Createorder");
				//}else{
				//	showAlertDialogToast("Please Check Your internet connection");
				//}

			}
		});

		linearLayoutComplaint.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;

				//if(netCheckwithoutAlert()==true){
					getDetail("postnotes");
				//}else{
				//	showAlertDialogToast("Please Check Your internet connection");
				//}

			}
		});

		linearLayoutclientVisit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				//if(netCheckwithoutAlert()==true){
					getDetail("clientvisit");
				//}else{
				//	showAlertDialogToast("Please Check Your internet connection");
				//}
			}
		});

		linearLayoutSignout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				// TODO Auto-generated method stub
				databaseHandler.delete();
				Intent io = new Intent(PaymentActivity.this,
						SigninActivity.class);
				startActivity(io);
				finish();
			}
		});
	}


	private void createOrder() {


		//Create the Order
		Order order = new Order(accessToken,paymentId,UsrName,UsrEmail,UsrMobile,paymentAmount,"Application Fee");

		//Validate the Order
		if (!order.isValid()) {
			//oops order validation failed. Pinpoint the issue(s).

			if (!order.isValidName()) {
				Log.e("name","invalid");
			}

			if (!order.isValidEmail()) {
				Log.e("isValidEmail","invalid");
			}

			if (!order.isValidPhone()) {
				Log.e("isValidPhone","invalid");
			}

			if (!order.isValidAmount()) {
				Log.e("isValidAmount","invalid");
			}

			if (!order.isValidDescription()) {
				Log.e("isValidDescription","invalid");
			}

			if (!order.isValidTransactionID()) {
				Log.e("isValidTransactionID","invalid");
			}

			if (!order.isValidRedirectURL()) {
				Log.e("isValidRedirectURL","invalid");
			}

			return;
		}

		//Validation is successful. Proceed

		Request request = new Request(order, new OrderRequestCallBack() {
			@Override
			public void onFinish(final Order order, final Exception error) {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {

						if (error != null) {
							if (error instanceof Errors.ConnectionError) {
								showToast("No internet connection");
							} else if (error instanceof Errors.ServerError) {
								showToast("Server Error. Try again");
							} else if (error instanceof Errors.AuthenticationError) {
								showToast("Access token is invalid or expired. Please Update the token!!");
							} else if (error instanceof Errors.ValidationError) {
								// Cast object to validation to pinpoint the issue
								Errors.ValidationError validationError = (Errors.ValidationError) error;

								if (!validationError.isValidTransactionID()) {

									return;
								}

								if (!validationError.isValidRedirectURL()) {

									return;
								}

								if (!validationError.isValidPhone()) {

									return;
								}

								if (!validationError.isValidEmail()) {

									return;
								}

								if (!validationError.isValidAmount()) {

									return;
								}

								if (!validationError.isValidName()) {

									return;
								}
							} else {
								showToast(error.getMessage());
							}
							return;
						}

						startPreCreatedUI(order);
					}
				});
			}
		});

		request.execute();
	}

	private void startPreCreatedUI(Order order) {
		//Using Pre created UI
		Intent intent = new Intent(getBaseContext(), PaymentDetailsActivity.class);
		intent.putExtra(com.instamojo.android.helpers.Constants.ORDER, order);
		startActivityForResult(intent, com.instamojo.android.helpers.Constants.REQUEST_CODE);


	}

	public void Moveto(){
		Intent to = new Intent(PaymentActivity.this, MyDealersActivity.class);
		to.putExtra("Key","MenuClick");
		startActivity(to);
		finish();
	}

	private void showToast(final String message) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(getBaseContext(), message, Toast.LENGTH_LONG).show();
			}
		});
	}

	public boolean netCheck() {
		// for network connection
		try {
			ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetwork = connectionManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			Object result = null;
			if (mWifi.isConnected() || mNetwork.isConnected()) {
				return true;
			}

			else if (result == null) {
				showAlertDialog(PaymentActivity.this, "No Internet Connection",
						"Please Check Your internet connection.", false);
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	public boolean netCheckwithoutAlert() {
		// for network connection
		try {
			ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetwork = connectionManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			Object result = null;
			if (mWifi.isConnected() || mNetwork.isConnected()) {
				return true;
			}

			else if (result == null) {

				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	private void getDetails() {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog= ProgressDialog.show(PaymentActivity.this, "",
						"Loading...", true, true);

			}

			@Override
			protected void onPostExecute(Void result) {

				try {


					lastPayment =(JsonAccountObject.getString("lastpymtdt"));
					nextPayment =(JsonAccountObject.getString("nextpymtdt"));
					UsrName=(JsonAccountObject.getString("fullname"));
					UsrMobile=(JsonAccountObject.getString("mobileno"));
					UsrEmail=(JsonAccountObject.getString("emailid"));
					PaymentStatus=(JsonAccountObject.getString("pymtstatus"));
					Log.e("PaymentStatus",PaymentStatus);
					Log.e("lastPayment",lastPayment);
					Log.e("nextPayment",nextPayment);

					String datelast[] =lastPayment.split("T");
					String datenxt[] =nextPayment.split("T");

					Log.e("datelast",datelast[0]);
					Log.e("datenxt",datenxt[0]);


					String inputPattern = "yyyy-MM-dd";
					String outputPattern = "dd-MMM-yyyy ";
					SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
					SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

					String lastdatestr = "",nextdatestr="";

					try {
						lastdateStr1 = inputFormat.parse(datelast[0]);
						lastdatestr = outputFormat.format(lastdateStr1);
						Log.e("lastdatestr",lastdatestr);

						nextdateStr1 = inputFormat.parse(datenxt[0]);
						nextdatestr = outputFormat.format(nextdateStr1);
						Log.e("nextdatestr",nextdatestr);


						//Log.e("str", str);
					} catch (Exception e) {
						e.printStackTrace();
					}
					if(PaymentStatus.equals("Pending")||PaymentStatus.equals(null) ||PaymentStatus.equals("")||PaymentStatus.equals("null")){
						textViewPaymentStatus.setText("Please make payment here to activate your subscription");
					}else{
						textViewPaymentStatus.setText("Last Payment date   :   " + lastdatestr + "\n"+ "Next Payment date  :   " +nextdatestr );
					}

					dialog.dismiss();




					getAmount();
				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					Log.e("ProductActivityException", e.toString());
				}

			}

			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();


				JsonServiceHandler1 = new JsonServiceHandler(Utils.strGetProfileDetail+Constants.USER_ID, PaymentActivity.this);
				JsonAccountObject = JsonServiceHandler1.ServiceDataGet();
				return null;
			}
		}.execute(new Void[]{});
	}


	public void showAlertDialog(Context context, String title, String message,
			Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
					}
				});
		alertDialog.show();
	}


	private void OnlinePayment() {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";
			@Override
			protected void onPreExecute() {
				dialog = ProgressDialog.show(PaymentActivity.this, "In Progress",
						"Validating...", true, true);
			}

			@Override
			protected void onPostExecute(Void result) {
				try
				{
					strStatus = JsonAccountObject.getString("status");
					strMsg = JsonAccountObject.getString("message");

					if(strStatus=="true"){

						paymentId= JsonAccountObject.getString("data");



						getAccessToken();
					//		startActivity(io);
							//MainFragmentActivity.buttonMenuFeedback.setText(ac);

					}else{
showToast(strMsg);
					}

				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
				//	Log.e("CategoryActivityException",e.toString());
				}

				dialog.dismiss();
			}

			@Override
			protected Void doInBackground(Void... params) {
				dialog.show();
				JSONObject jsonObject = new JSONObject();
				try {

					jsonObject.put("pymtstatus", "Pending");
					jsonObject.put("userid", Constants.USER_ID);
					jsonObject.put("pymtamount", paymentAmount);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				JsonServiceHandler1 = new JsonServiceHandler(Utils.strInitiatePayment,jsonObject.toString(),
						PaymentActivity.this);
				JsonAccountObject = JsonServiceHandler1.ServiceData();
				return null;
			}
		}.execute(new Void[]{});

	}
	private void  getDetail(final String moveto) {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog= ProgressDialog.show(PaymentActivity.this, "",
						"Loading...", true, true);

			}

			@Override
			protected void onPostExecute(Void result) {

				try {

					PaymentStatus=(JsonAccountObject.getString("pymtstatus"));
					Log.e("PaymentStatus",PaymentStatus);

					if(PaymentStatus.equals("Pending")||PaymentStatus.equals(null) ||PaymentStatus.equals("")||PaymentStatus.equals("null")){
						showAlertDialogToast("Please make payment to place order");
					}else {
						if (moveto.equals("products")) {
							Intent to = new Intent(PaymentActivity.this,
									ProductActivity.class);
							startActivity(to);
							finish();

						} else if (moveto.equals("export")) {

							Intent to = new Intent(PaymentActivity.this,
									ExportActivity.class);
							startActivity(to);
							finish();

						} else if (moveto.equals("myorders")) {
							if (Constants.USER_TYPE.equals("D")) {
								Intent io = new Intent(PaymentActivity.this,
										DealersOrderActivity.class);
								startActivity(io);
								finish();

							} else if (Constants.USER_TYPE.equals("M")){
								Intent io = new Intent(PaymentActivity.this,
										MerchantOrderActivity.class);
								startActivity(io);
								finish();

							}else
							{
								Intent io = new Intent(PaymentActivity.this,
										SalesManOrderActivity.class);
								startActivity(io);
								finish();
							}
						}else if(moveto.equals("Createorder")){
							Intent io = new Intent(PaymentActivity.this,
									CreateOrderActivity.class);
							startActivity(io);
							finish();

						}else if(moveto.equals("Mydealer")){

							Intent io = new Intent(PaymentActivity.this,
									MyDealersActivity.class);
							io.putExtra("Key","MenuClick");
							startActivity(io);
							finish();

						}else if(moveto.equals("profile")){

							Intent io = new Intent(PaymentActivity.this,
									ProfileActivity.class);
							startActivity(io);
							finish();

						}
						else if(moveto.equals("postnotes")) {

							if (Constants.USER_TYPE.equals("D")) {
								Intent io = new Intent(PaymentActivity.this,
										DealersComplaintActivity.class);
								startActivity(io);
								finish();

							}else {
								Intent io = new Intent(PaymentActivity.this,
										MerchantComplaintActivity.class);
								startActivity(io);
								finish();

							}
						}else if (moveto.equals("clientvisit")) {

								Intent to = new Intent(PaymentActivity.this,
										DealerClientVisit.class);
								startActivity(to);
								finish();

							}




					}


					dialog.dismiss();

				}catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
				}

			}

			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();


				JsonServiceHandler2 = new JsonServiceHandler(Utils.strGetProfileDetail+Constants.USER_ID, PaymentActivity.this);
				JsonAccountObject = JsonServiceHandler2.ServiceDataGet();
				return null;
			}
		}.execute(new Void[]{});
	}

	private void getAccessToken() {

		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";
			@Override
			protected void onPreExecute() {
				dialog = ProgressDialog.show(PaymentActivity.this, "In Progress",
						"Validating...", true, true);
			}

			@Override
			protected void onPostExecute(Void result) {
				try
				{
					accessToken = JsonAccountObject.getString("access_token");

					//	strMsg = JsonAccountObject.getString("message");
					createOrder();


				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					//	Log.e("CategoryActivityException",e.toString());
				}

				dialog.dismiss();
			}

			@Override
			protected Void doInBackground(Void... params) {
				dialog.show();
				JSONObject jsonObject = new JSONObject();

				JsonServiceHandler = new JsonServiceHandlerForPayment(Utils.strCreateAccessToken,jsonObject.toString(),
						PaymentActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceDataNoHeader();
				return null;
			}
		}.execute(new Void[]{});

	}


	private void  getAmount() {
		
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";
			@Override
			protected void onPreExecute() {
				dialog = ProgressDialog.show(PaymentActivity.this, "In Progress",
						"Validating...", true, true);
			}

			@Override
			protected void onPostExecute(Void result) {
				try
				{
					strStatus = JsonAccountObject.getString("status");
					//	strMsg = JsonAccountObject.getString("message");

					if(strStatus=="true"){

						EditTextViewAmount.setText(JsonAccountObject.getJSONArray("data").getJSONObject(0).getString("pymtamount"));
						EditTextViewMobile.setText(Constants.PAYMENT);
						
					}else{
						//showAlertDialogToast("");
					}

				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
				//	Log.e("CategoryActivityException",e.toString());
				}

				dialog.dismiss();
			}

			@Override
			protected Void doInBackground(Void... params) {
				dialog.show();
				JSONObject jsonObject = new JSONObject();
				
				JsonServiceHandler1 = new JsonServiceHandler(Utils.strGetPayment+Constants.PAYMENT,
						PaymentActivity.this);
				JsonAccountObject = JsonServiceHandler1.ServiceDataGet();
				return null;
			}


		}.execute(new Void[]{});

	}



	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == com.instamojo.android.helpers.Constants.REQUEST_CODE && data != null) {
			String orderID = data.getStringExtra(com.instamojo.android.helpers.Constants.ORDER_ID);
			String transactionID = data.getStringExtra(com.instamojo.android.helpers.Constants.TRANSACTION_ID);
			String paymentID = data.getStringExtra(com.instamojo.android.helpers.Constants.PAYMENT_ID);

			// Check transactionID, orderID, and orderID for null before using them to check the Payment status.
			if (orderID != null && transactionID != null && paymentID != null) {
				Log.e("orderID",orderID);
				Log.e("transactionID",transactionID);
				Log.e("paymentID",paymentID);

				PaymentOrderId=orderID;
				PaymentTransactionId=transactionID;
				Paymenttrcn_mojo_id=  paymentID;
				checkPaymentStatus(PaymentOrderId);

			} else {
				showToast("Oops!! Payment was cancelled");
				pymtstatus="Cancelled";
				Paymenttrcn_mojo_id="";
				PaymentTransactionId=paymentId;
				//UpdatePayment1(pymtstatus, paymentAmount, Paymenttrcn_mojo_id, PaymentTransactionId);
			}
		}
	}



	private HttpUrl.Builder getHttpURLBuilder() {
		return new HttpUrl.Builder()
				.scheme("https")
				.host("sample-sdk-server.instamojo.com");
	}

	private void checkPaymentStatus(final String ID) {

		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";
			@Override
			protected void onPreExecute() {
				dialog = ProgressDialog.show(PaymentActivity.this, "In Progress",
						"Validating...", true, true);
			}

			@Override
			protected void onPostExecute(Void result) {
				try
				{
					strStatus = JsonAccountObject.getString("success");
					//	strMsg = JsonAccountObject.getString("message");

					if(strStatus=="true"){

						JSONObject job=JsonAccountObject.getJSONObject("payment_request");
						JSONArray jsonArray=job.getJSONArray("payments");
						for (int i = 0; i < jsonArray.length(); i++) {
							pymtstatus=jsonArray.getJSONObject(0).getString("status");
							Log.e("status",pymtstatus);
							UpdatePayment(pymtstatus, paymentAmount, Paymenttrcn_mojo_id, PaymentTransactionId);
						}



					}else{

					}

				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					//	Log.e("CategoryActivityException",e.toString());
				}

				dialog.dismiss();
			}

			@Override
			protected Void doInBackground(Void... params) {
				dialog.show();
				JSONObject jsonObject = new JSONObject();

				JsonServiceHandler = new JsonServiceHandlerForPayment(Utils.strGetPaymentStatus+ID,jsonObject.toString(),
						PaymentActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceData();
				return null;
			}
		}.execute(new Void[] {});

	}

	private void UpdatePayment(final String pymtstatus,final String paymentAmount,final String trcn_mojo_id,final String transactionId) {

		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";
			@Override
			protected void onPreExecute() {
				dialog = ProgressDialog.show(PaymentActivity.this, "In Progress",
						"Validating...", true, true);
			}

			@Override
			protected void onPostExecute(Void result) {
				try
				{
					strStatus = JsonAccountObject.getString("status");
					strMsg = JsonAccountObject.getString("message");

					if(strStatus=="true"){

						String pymtstatus=JsonAccountObject.getString("pymtstatus");

						if(pymtstatus.equals("Credit")){
							Intent io = new Intent(PaymentActivity.this,
									PaymentConfirmationActivity.class);
							io.putExtra("RefNo",trcn_mojo_id);
							io.putExtra("message","Congratulations");
							io.putExtra("status","You have successfully made the payment");

							startActivity(io);
							finish();

						}else{
							Intent io = new Intent(PaymentActivity.this,
									PaymentConfirmationActivity.class);
							io.putExtra("RefNo",trcn_mojo_id);
							io.putExtra("message"," ");
							io.putExtra("status","Payment Failed");

							startActivity(io);
							finish();

						}


					}else{
						showToast(strMsg);
					}

				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					//	Log.e("CategoryActivityException",e.toString());
				}

				dialog.dismiss();
			}

			@Override
			protected Void doInBackground(Void... params) {
				dialog.show();
				JSONObject jsonObject = new JSONObject();
				try {

					//	jsonObject.put("amount", paymentAmount);
					jsonObject.put("userid", Constants.USER_ID);
					jsonObject.put("pymtamount", paymentAmount);
					jsonObject.put("pymtstatus", pymtstatus);
					jsonObject.put("pymttenure", Constants.PAYMENT);
					jsonObject.put("pymtref", trcn_mojo_id);



				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				JsonServiceHandler1 = new JsonServiceHandler(Utils.strUpdatePayment+transactionId,jsonObject.toString(),
						PaymentActivity.this);
				JsonAccountObject = JsonServiceHandler1.ServiceData();
				return null;
			}
		}.execute(new Void[]{});

	}

	private void UpdatePayment1(final String pymtstatus,final String paymentAmount,final String trcn_mojo_id,final String transactionId) {

		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";
			@Override
			protected void onPreExecute() {
				dialog = ProgressDialog.show(PaymentActivity.this, "In Progress",
						"Validating...", true, true);
			}

			@Override
			protected void onPostExecute(Void result) {
				try
				{
					strStatus = JsonAccountObject.getString("status");
					strMsg = JsonAccountObject.getString("message");

					if(strStatus=="true"){

					//	showToast(strMsg);
					}else{
					//	showToast(strMsg);
					}

				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					//	Log.e("CategoryActivityException",e.toString());
				}

				dialog.dismiss();
			}

			@Override
			protected Void doInBackground(Void... params) {
				dialog.show();
				JSONObject jsonObject = new JSONObject();
				try {

					//	jsonObject.put("amount", paymentAmount);
					jsonObject.put("userid", Constants.USER_ID);
					jsonObject.put("pymtamount", paymentAmount);
					jsonObject.put("pymtstatus", pymtstatus);
					jsonObject.put("pymttenure", Constants.PAYMENT);
					jsonObject.put("pymtref", trcn_mojo_id);



				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				JsonServiceHandler1 = new JsonServiceHandler(Utils.strUpdatePayment+transactionId,jsonObject.toString(),
						PaymentActivity.this);
				JsonAccountObject = JsonServiceHandler1.ServiceData();
				return null;
			}
		}.execute(new Void[]{});

	}
	public void onBackPressed() {
		exitAlret();
	}

	private void exitAlret() {
		AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
		localBuilder.setCancelable(false);
		localBuilder.setMessage("Do you want to Exit?");
		localBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {

						finish();

					}
				});
		localBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {
						paramDialogInterface.cancel();
					}
				});
		localBuilder.create().show();
	}


	public void showAlertDialogToast( String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();

		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

	}


}
