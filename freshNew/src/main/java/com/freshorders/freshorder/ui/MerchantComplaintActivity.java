package com.freshorders.freshorder.ui;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.activity.DistanceCalActivity;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.MDealerCompDropdownDomain;
import com.freshorders.freshorder.toonline.migration.PendingDataMigrationActivity;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.DbBitmapUtility;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.UploadImagePostNote;
import com.freshorders.freshorder.utils.Utils;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class MerchantComplaintActivity extends Activity {
	final static int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1;
	TextView menuIcon, textViewAssetMenuProfile, textViewAssetMenuMyOrders,
			textViewAssetMenuMyDealers, textViewAssetMenuProducts,
			textViewAssetMenuPayment, textViewAssetMenuComplaint,
			textViewAssetMenuSignout,textViewAssetMenuCreateOrder,textViewAssetMenuClientVisit,textViewAssetClientVisit,
			textViewAssetMenuPaymentCollection,textViewAssetMenuRefresh,textViewAssetMenuDistanceCalculation,
			textViewAssetMenuStockEntry,textViewAssetMenuAddMerchant,textViewAssetMenuAcknowledge,textViewAssetMenuExport
			,textViewAssetMenusurveyuser,textViewAssetMenudownloadscheme,textViewAssetMenuMyDayPlan,textViewAssetMenuMySales,
			textViewAssetMenuDistStock, textViewAssetMenuStockOnly, textViewAssetMenuMigration, textViewAssetMenuStockAudit;

	int menuCliccked;
	Uri imageUri  = null;
	MerchantComplaintActivity CameraActivity = null;
	public  static  String imageId;
	public int Dealerselected=0;
	public  static String Path="",path1="";
	public  static String CapturedImageDetails ;
	Bitmap mBitmap1=null;
	 Button photo;
	LinearLayout linearLayoutMenuParent,linearLayoutProfile, linearLayoutMyOrders,
			linearLayoutMyDealers, linearLayoutProducts, linearLayoutPayment,
			linearLayoutComplaint, linearLayoutSignout,linearLayoutCreateOrder,linearLayoutClientVisit,linearLayoutClientVisit1,
            linearLayoutPaymentCollection,linearLayoutRefresh,linearLayoutDistanceCalculation,
			linearLayoutAcknowledge,linearLayoutStockEntry,linearLayoutAddMerchant
			,linearLayoutsurveyuser,linearLayoutdownloadscheme,linearLayoutMyDayPlan,linearLayoutMySales,linearLayoutExport,
			linearLayoutDistStock, linearLayoutStockOnly, linearLayoutMigration, linearLayoutStockAudit;;

	private void showMenu(){

		MyApplication app = MyApplication.getInstance();

		if(!app.isCreateOrder()){
			linearLayoutCreateOrder.setVisibility(View.GONE);
		}
		if(!app.isProfile()){
			linearLayoutProfile.setVisibility(View.GONE);
		}
		if(!app.isMyDayPlan()){
			linearLayoutMyDayPlan.setVisibility(View.GONE);
		}
		if(!app.isMyOrders()){
			linearLayoutMyOrders.setVisibility(View.GONE);
		}
		if(!app.isAddMerchant()){
			linearLayoutAddMerchant.setVisibility(View.GONE);
		}
		if(!app.isMySales()){
			linearLayoutMySales.setVisibility(View.GONE);
		}
		if(!app.isPostNotes()){
			linearLayoutComplaint.setVisibility(View.GONE);
		}
		if(!app.isMyClientVisit()){
			linearLayoutClientVisit.setVisibility(View.GONE);
		}
		if(!app.isAcknowledge()){
			linearLayoutAcknowledge.setVisibility(View.GONE);
		}
		if(!app.isPaymentCollection()){
			linearLayoutPaymentCollection.setVisibility(View.GONE);
		}
		if(!app.isPkdDataCapture()){
			linearLayoutStockEntry.setVisibility(View.GONE);
		}
		if(!app.isDistributorStock()){
			linearLayoutDistStock.setVisibility(View.GONE);
		}
		if(!app.isSurveyUser()){
			linearLayoutsurveyuser.setVisibility(View.GONE);
		}
		if(!app.isDownLoadScheme()){
			linearLayoutdownloadscheme.setVisibility(View.GONE);
		}
		if(!app.isDistanceCalculation()){
			linearLayoutDistanceCalculation.setVisibility(View.GONE);
		}
		if(!app.isRefresh()){
			linearLayoutRefresh.setVisibility(View.GONE);
		}
		if(!app.isLogout()){
			linearLayoutSignout.setVisibility(View.GONE);
		}
		if(!app.isClosingStock()){
			linearLayoutStockOnly.setVisibility(View.GONE);
		}
		if(!app.isClosingStockAudit()){
			linearLayoutStockAudit.setVisibility(View.GONE);
		}
		if(!app.isPendingData()){
			linearLayoutMigration.setVisibility(View.GONE);
		}
	}

	JSONObject JsonAccountObject = null;
	JSONArray JsonAccountArray = null;
	DatabaseHandler databaseHandler;
	Button buttonSend;
	ImageView imageView;
	AutoCompleteTextView autocompleteTextTraders;
	EditText EditTextViewDescription;
	String Description,DuserID,traders;
	JsonServiceHandler JsonServiceHandler ;
	ArrayList<MDealerCompDropdownDomain> arrayListDealerDomain;
	ArrayList<String> arrayListDealer;
	public static String moveto="NULL",PaymentStatus="NULL";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(1);
		if(Constants.USER_TYPE.equals("S")){
			setContentView(R.layout.merchant_postnotes_activity);
		}else
			if(Constants.USER_TYPE.equals("D")){
				setContentView(R.layout.merchant_complaint_screen);

		}
			else
			if(Constants.USER_TYPE.equals("M")){
				setContentView(R.layout.merchant_complaint_screen1);

			}

		CameraActivity=this;
		databaseHandler = new DatabaseHandler(getApplicationContext());
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();
		Log.e("what??", String.valueOf(mBitmap1));
		Log.e("what!!!", String.valueOf(Constants.setimage));
		Cursor cur;
		///Kumaravel /// For Offline Count
		TextView tvFailedCount = (TextView) findViewById(R.id.id_post_note_failedCount);
		TextView tvOfflineCount = (TextView) findViewById(R.id.id_post_note_offlineCount);
		String strFailedCount = tvFailedCount.getText().toString();
		String strOfflineCount = tvOfflineCount.getText().toString();

		Cursor cur_count = databaseHandler.getPostNotePending();
		if(cur_count != null && cur_count.getCount() > 0){
			tvOfflineCount.setText((strOfflineCount+cur_count.getCount()));
			cur_count.close();
		}else {
			tvOfflineCount.setText((strOfflineCount+0));
		}
		Cursor cur_failed_count = databaseHandler.getPostNoteFailed();
		if(cur_failed_count != null && cur_failed_count.getCount() > 0){
			tvFailedCount.setText((strFailedCount+cur_failed_count.getCount()));
			cur_failed_count.close();
		}else {
			tvFailedCount.setText((strFailedCount+0));
		}

		cur = databaseHandler.getDetails();
		cur.moveToFirst();

		Log.e("Insertion Check",
				cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_username)));
		Log.e("Insertion Check",
				cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_password)));
		Log.e("Insertion Check",
				cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_usertype)));

		PaymentStatus = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_paymentStatus));

		buttonSend = (Button) findViewById(R.id.buttonSend);
		//buttonSendimage= (Button) findViewById(R.id.buttonSendimage);
		photo = (Button) findViewById(R.id.buttonSendimage);
		autocompleteTextTraders = (AutoCompleteTextView) findViewById(R.id.autocompleteTextTraders);
		EditTextViewDescription = (EditText) findViewById(R.id.EditTextViewDescription);
		linearLayoutCreateOrder = (LinearLayout) findViewById(R.id.linearLayoutCreateOrder);
		linearLayoutProfile = (LinearLayout) findViewById(R.id.linearLayoutProfile);
		linearLayoutMyOrders = (LinearLayout) findViewById(R.id.linearLayoutMyOrders);
		linearLayoutMyDealers = (LinearLayout) findViewById(R.id.linearLayoutMyDealers);
		linearLayoutProducts = (LinearLayout) findViewById(R.id.linearLayoutProducts);
		linearLayoutPayment = (LinearLayout) findViewById(R.id.linearLayoutPayment);
		linearLayoutComplaint = (LinearLayout) findViewById(R.id.linearLayoutComplaint);
		linearLayoutClientVisit= (LinearLayout) findViewById(R.id.linearLayoutClientVisit);
		linearLayoutClientVisit1= (LinearLayout) findViewById(R.id.linearLayoutClientVisit1);
		linearLayoutSignout = (LinearLayout) findViewById(R.id.linearLayoutSignout);
		linearLayoutPaymentCollection= (LinearLayout) findViewById(R.id.linearLayoutPaymentCollection);
		linearLayoutExport= (LinearLayout) findViewById(R.id.linearLayoutExport);
		linearLayoutAcknowledge= (LinearLayout) findViewById(R.id.linearLayoutAcknowledge);
		linearLayoutStockEntry= (LinearLayout) findViewById(R.id.linearLayoutStockEntry);
		linearLayoutAddMerchant = (LinearLayout) findViewById(R.id.linearLayoutAddMerchant);
		linearLayoutDistanceCalculation= (LinearLayout) findViewById(R.id.linearLayoutDistanceCalculation);
		linearLayoutdownloadscheme= (LinearLayout) findViewById(R.id.linearLayoutdownloadscheme);
		linearLayoutsurveyuser= (LinearLayout) findViewById(R.id.linearLayoutsurveyuser);
		linearLayoutMyDayPlan = (LinearLayout) findViewById(R.id.linearLayoutMyDayPlan);
		linearLayoutMySales = (LinearLayout) findViewById(R.id.linearLayoutMySales);

		linearLayoutStockOnly = (LinearLayout) findViewById(R.id.linearLayoutStockOnly);
		linearLayoutStockAudit = findViewById(R.id.linearLayoutStockAudit);
		linearLayoutMigration = (LinearLayout) findViewById(R.id.linearLayoutMigration);

		imageView= (ImageView) findViewById(R.id.imageView);
        linearLayoutRefresh= (LinearLayout) findViewById(R.id.linearLayoutRefresh);
        textViewAssetMenuRefresh = (TextView) findViewById(R.id.textViewAssetMenuRefresh);
		//linearlayoutdump1= (LinearLayout) findViewById(R.id.linearlayoutdump1);
		if(Constants.setdealerpostnts==""&&Constants.setdiscription==""){
			Log.e("ins","ins");
			autocompleteTextTraders.setText("");
			EditTextViewDescription.setText("");
		}else {
			Log.e("ous","ous");
			autocompleteTextTraders.setText(Constants.setdealerpostnts);
			EditTextViewDescription.setText(Constants.setdiscription);
			DuserID=Constants.setduserid;
		}
		/*
		if(netCheck()==true){
			loadingDealer();
		} */  //by Kumaravel for offline support
		autocompleteTextTraders.setVisibility(View.GONE);
		traders = "Nambi";
		Dealerselected = 1;
		if(Constants.DUSER_ID.isEmpty()){

		}else {
			DuserID = Constants.DUSER_ID;
		}
		// change finished
		arrayListDealer = new ArrayList<String>();
		arrayListDealerDomain = new ArrayList<MDealerCompDropdownDomain>();

		autocompleteTextTraders.setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				autocompleteTextTraders.showDropDown();
				if (autocompleteTextTraders.equals("")) {
					InputMethodManager iaa = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					iaa.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
					Dealerselected=0;
				}
				return false;
			}
		});
		autocompleteTextTraders
				.setOnItemClickListener(new OnItemClickListener() {
					public void onItemClick(AdapterView<?> parent, View view,
											int position, long id) {
						DuserID=arrayListDealerDomain.get(position).getID();
						Dealerselected=1;
					}
				});

		if (Constants.USER_TYPE.equals("D")) {

			linearLayoutMyDealers.setVisibility(View.GONE);
			linearLayoutProducts.setVisibility(View.VISIBLE);
			linearLayoutCreateOrder.setVisibility(View.GONE);
			linearLayoutPaymentCollection.setVisibility(View.GONE);
			linearLayoutDistanceCalculation.setVisibility((View.GONE));
            linearLayoutRefresh.setVisibility(View.GONE);
		} else if (Constants.USER_TYPE.equals("S")){
			linearLayoutMyDealers.setVisibility(View.GONE);
			linearLayoutProducts.setVisibility(View.GONE);
			linearLayoutCreateOrder.setVisibility(View.VISIBLE);
			linearLayoutPaymentCollection.setVisibility(View.VISIBLE);
			linearLayoutDistanceCalculation.setVisibility(View.VISIBLE);
            linearLayoutRefresh.setVisibility(View.VISIBLE);
			//linearlayoutdump1.setVisibility(View.VISIBLE);
		}else {
			linearLayoutPayment.setVisibility(LinearLayout.VISIBLE);
			linearLayoutMyDealers.setVisibility(View.VISIBLE);
			linearLayoutProducts.setVisibility(View.GONE);
			linearLayoutCreateOrder.setVisibility(View.GONE);
			linearLayoutDistanceCalculation.setVisibility(View.GONE);
            linearLayoutRefresh.setVisibility(View.GONE);
			linearLayoutMyDayPlan.setVisibility(View.GONE);
		}

		buttonSend.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Description = EditTextViewDescription.getText().toString();

				Log.e("cos", Constants.setdiscription);
				//traders = autocompleteTextTraders.getText().toString();
				//Log.e("Dealerselected", String.valueOf(Dealerselected));
				//if (traders.equals("")) {
				//	Dealerselected = 0;
				//}
				Log.e("cos", traders);
				if (Dealerselected == 1 && !traders.startsWith(".") && !traders.isEmpty()) {
					if (!Description.isEmpty() && !Description.startsWith(".")) {
						//SendComplaint();
						if(netCheckWithoutAlert()) {
							UploadImagePostNote ufs = new UploadImagePostNote(
									MerchantComplaintActivity.this, path1, Description, DuserID, Constants.USER_ID);
							ufs.execute();
							Dealerselected = 0;
						}else {
							// Loading Data to Local DB For Online  --- Kumaravel
							loadOfflinePostNote(Description, DuserID, Constants.USER_ID);
							Dealerselected = 0;
							Constants.setimage=null;
							Constants.setdiscription="";
							Constants.setdealerpostnts="";
							Constants.setduserid="";
							showAlert("Notes Posted Successfully");
						}
					} else {
						showAlertDialogToast("Description should not be empty");
					}
				} else {
					showAlertDialogToast("Traders should not be empty");
				}


			}


		});

		menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
		textViewAssetMenuCreateOrder= (TextView) findViewById(R.id.textViewAssetMenuCreateOrder);
		textViewAssetMenuProfile = (TextView) findViewById(R.id.textViewAssetMenuProfile);
		textViewAssetMenuMyOrders = (TextView) findViewById(R.id.textViewAssetMenuMyOrders);
		textViewAssetMenuMyDealers = (TextView) findViewById(R.id.textViewAssetMenuMyDealers);
		textViewAssetMenuProducts = (TextView) findViewById(R.id.textViewAssetMenuProducts);
		textViewAssetMenuPayment = (TextView) findViewById(R.id.textViewAssetMenuPayment);
		textViewAssetMenuComplaint = (TextView) findViewById(R.id.textViewAssetMenuComplaint);
		textViewAssetMenuSignout = (TextView) findViewById(R.id.textViewAssetMenuSignout);
		textViewAssetMenuClientVisit= (TextView) findViewById(R.id.textViewAssetMenuClientVisit);
		textViewAssetMenuExport= (TextView) findViewById(R.id.textViewAssetMenuExport);
		textViewAssetMenuPaymentCollection= (TextView) findViewById(R.id.textViewAssetMenuPaymentCollection);
		linearLayoutMenuParent = (LinearLayout) findViewById(R.id.linearLayoutMenuParent);
		textViewAssetMenuDistanceCalculation= (TextView) findViewById(R.id.textViewAssetMenuDistanceCalculation);
		textViewAssetMenuAddMerchant = (TextView) findViewById(R.id.textViewAssetMenuAddMerchant);
		textViewAssetMenuAcknowledge = (TextView) findViewById(R.id.textViewAssetMenuAcknowledge);
		textViewAssetMenuStockEntry = (TextView) findViewById(R.id.textViewAssetMenuStockEntry);
		textViewAssetClientVisit = (TextView) findViewById(R.id.textViewAssetClientVisit);
		textViewAssetMenudownloadscheme= (TextView) findViewById(R.id.textViewAssetMenudownloadscheme);
		textViewAssetMenusurveyuser= (TextView) findViewById(R.id.textViewAssetMenusurveyuser);
		textViewAssetMenuMyDayPlan= (TextView) findViewById(R.id.textViewAssetMenuMyDayPlan);
		textViewAssetMenuMySales= (TextView) findViewById(R.id.textViewAssetMenuMySales);

		Typeface font = Typeface.createFromAsset(getAssets(),
				"fontawesome-webfont.ttf");
		menuIcon.setTypeface(font);
		textViewAssetMenuCreateOrder.setTypeface(font);
		textViewAssetMenuProfile.setTypeface(font);
		textViewAssetMenuMyOrders.setTypeface(font);
		textViewAssetMenuMyDealers.setTypeface(font);
		textViewAssetMenuProducts.setTypeface(font);
		textViewAssetMenuPayment.setTypeface(font);
		textViewAssetMenuComplaint.setTypeface(font);
		textViewAssetMenuSignout.setTypeface(font);
		textViewAssetMenuClientVisit.setTypeface(font);
		textViewAssetClientVisit.setTypeface(font);
		textViewAssetMenuPaymentCollection.setTypeface(font);
        textViewAssetMenuRefresh.setTypeface(font);
		textViewAssetMenuExport.setTypeface(font);
		textViewAssetMenuAcknowledge.setTypeface(font);
		textViewAssetMenuStockEntry.setTypeface(font);
		textViewAssetMenuAddMerchant.setTypeface(font);
		textViewAssetMenuDistanceCalculation.setTypeface(font);
		textViewAssetMenusurveyuser.setTypeface(font);
		textViewAssetMenudownloadscheme.setTypeface(font);
		textViewAssetMenuMyDayPlan.setTypeface(font);
		textViewAssetMenuMySales.setTypeface(font);

		linearLayoutDistStock = (LinearLayout) findViewById(R.id.linearLayoutDistStock);
		textViewAssetMenuDistStock = (TextView) findViewById(R.id.textViewAssetMenuDistStock);
		textViewAssetMenuDistStock.setTypeface(font);

		textViewAssetMenuStockOnly = (TextView) findViewById(R.id.textViewAssetMenuStockOnly);
		textViewAssetMenuStockAudit = (TextView) findViewById(R.id.textViewAssetMenuStockAudit);
		textViewAssetMenuMigration = findViewById(R.id.textViewAssetMenuMigration);

		textViewAssetMenuStockOnly.setTypeface(font);
		textViewAssetMenuStockAudit.setTypeface(font);
		textViewAssetMenuMigration.setTypeface(font);



		if(cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_usertype)).equals("S")){

			linearLayoutClientVisit.setVisibility(View.GONE);

		}
		menuIcon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (menuCliccked == 0) {
					linearLayoutMenuParent.setVisibility(View.VISIBLE);
					menuCliccked = 1;
				} else {
					linearLayoutMenuParent.setVisibility(View.GONE);
					menuCliccked = 0;
				}

			}
		});

		linearLayoutMenuParent.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
			}
		});

		linearLayoutProfile.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				Constants.setimage = null;
				Constants.setdiscription = "";
				Constants.setdealerpostnts = "";
				Constants.setduserid = "";
				moveto = "profile";
				if (netCheckWithoutAlert() == true) {
					getData();
				} else {
					showAlertDialogToast("Please Check Your internet connection");
				}


			}
		});

		linearLayoutMyOrders.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Constants.checkproduct = 0;
				Constants.setimage=null;
				Constants.setdiscription="";
				Constants.setdealerpostnts="";
				Constants.setduserid="";
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				Constants.orderstatus="Success";
				moveto="myorder";
				if( netCheckWithoutAlert()==true){
					getData();
				}else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment before place order");
					}else {

						Intent io = new Intent(MerchantComplaintActivity.this,
								SalesManOrderActivity.class);
						Constants.setimage = null;
						startActivity(io);
						finish();
						/*switch (Constants.USER_TYPE) {
							case "D": {
								Intent io = new Intent(MerchantComplaintActivity.this,
										DealersOrderActivity.class);
								startActivity(io);
								finish();
								break;
							}
							case "M": {
								Intent io = new Intent(MerchantComplaintActivity.this,
										MerchantOrderActivity.class);
								Constants.setimage = null;
								startActivity(io);
								finish();
								break;
							}
							default: {
								Intent io = new Intent(MerchantComplaintActivity.this,
										SalesManOrderActivity.class);
								Constants.setimage = null;
								startActivity(io);
								finish();
								break;
							}
						} */
					}
				}


			}
		});

        linearLayoutRefresh.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("Clicked", "Clicked");

                if(netCheck()){

                    final Dialog qtyDialog = new Dialog(MerchantComplaintActivity.this);
                    qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    qtyDialog.setContentView(R.layout.warning_dialog);

                    final Button buttonUpdateOk, buttonUpdateCancel;

                    final TextView textQtyValidate;

                    buttonUpdateOk = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateOk);
                    buttonUpdateCancel = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateCancel);

                    textQtyValidate = (TextView) qtyDialog
                            .findViewById(R.id.textQtyValidate);
                    qtyDialog.show();


                    buttonUpdateOk.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            databaseHandler.synTableDelete();
                            Constants.refreshscreen="postnotes";
                            Intent io = new Intent(MerchantComplaintActivity.this, com.freshorders.freshorder.ui.RefreshActivity.class);
                            startActivity(io);
                            finish();
                            qtyDialog.dismiss();
                        }

                    });

                    buttonUpdateCancel
                            .setOnClickListener(new View.OnClickListener() {

								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub

									qtyDialog.dismiss();

								}
							});

                }/*else{
                    showAlertDialogToast("Please Check Your internet connection");
                }*/
            }

        });


        linearLayoutMyDealers.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				moveto="mydealer";
				Constants.setimage=null;
				Constants.setdiscription="";
				Constants.setdealerpostnts="";
				Constants.setduserid = "";
				if( netCheckWithoutAlert()==true){
					getData();
				}else{
					showAlertDialogToast("Please Check Your internet connection");
				}




			}
		});

		linearLayoutProducts.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				moveto="product";
				Constants.setimage=null;
				Constants.setdiscription="";
				Constants.setdealerpostnts="";
				Constants.setduserid="";
				if( netCheckWithoutAlert()==true){
					getData();
				}else{
					showAlertDialogToast("Please Check Your internet connection");
				}



			}
		});

		linearLayoutCreateOrder.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				MyApplication.getInstance().setTemplate8ForTemplate2(false);

				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				moveto="createorder";
				Constants.checkproduct=0;
				Constants.orderid="";
				Constants.setimage=null;
				Constants.setdiscription="";
				Constants.setdealerpostnts="";
				Constants.setduserid="";
				if( netCheckWithoutAlert()==true){
					getData();
				}else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment before place order");
					} else {
						Intent io = new Intent(MerchantComplaintActivity.this,
								CreateOrderActivity.class);
						startActivity(io);
						finish();
					}
				}


			}
		});

		linearLayoutPayment.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
                moveto="payment";
				Constants.setimage=null;
				Constants.setdiscription="";
				Constants.setdealerpostnts="";
				Constants.setduserid="";
				if( netCheckWithoutAlert()==true){
					getData();
				}else{
					showAlertDialogToast("Please Check Your internet connection");
				}


			}
		});

		linearLayoutComplaint.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;

			}
		});

		linearLayoutClientVisit1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				moveto="clientvisit";
				Constants.setimage=null;
				Constants.setdiscription="";
				Constants.setdealerpostnts="";
				Constants.setduserid="";
				if( netCheckWithoutAlert()==true){
					getData();
				}else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment before place order");
					} else {
						Intent io = new Intent(MerchantComplaintActivity.this,
								SMClientVisitHistory.class);
						startActivity(io);
						finish();
					}
				}

			}
		});

		linearLayoutPaymentCollection.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				moveto = "paymentcoll";
				Constants.setimage=null;
				Constants.setdiscription="";
				Constants.setdealerpostnts="";
				Constants.setduserid="";
				if( netCheckWithoutAlert()==true){
					getData();
				}else{
					showAlertDialogToast("Please Check Your internet connection");
				}




			}
		});

		linearLayoutStockEntry.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				moveto = "stockentry";
				Constants.setimage=null;
				Constants.setdiscription="";
				Constants.setdealerpostnts="";
				Constants.setduserid="";
				if( netCheckWithoutAlert()==true){
					getData();
				}else{
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment");
					}else {
						Intent io = new Intent(MerchantComplaintActivity.this,
								OutletStockEntry.class);
						startActivity(io);
						finish();
					}
				}


			}
		});
		linearLayoutDistStock.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				moveto = "diststock";
				Constants.setimage=null;
				Constants.setdiscription="";
				Constants.setdealerpostnts="";
				Constants.setduserid="";
				if( netCheckWithoutAlert()){
					getData();
				}else{
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment");
					} else {
						Intent io = new Intent(MerchantComplaintActivity.this,
								DistributrStockActivity.class);
						startActivity(io);
						finish();
					}
				}
			}
		});
		linearLayoutAcknowledge.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				moveto = "acknowledge";
				Constants.setimage=null;
				Constants.setdiscription="";
				Constants.setdealerpostnts="";
				Constants.setduserid="";
				if( netCheckWithoutAlert()==true){
					getData();
				}else{

					showAlertDialogToast("Please Check Your internet connection");
				}

			}
		});

		linearLayoutAddMerchant.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				moveto = "addmerchant";
				Constants.setimage=null;
				Constants.setdiscription="";
				Constants.setdealerpostnts="";
				Constants.setduserid="";
				if( netCheckWithoutAlert()){
					getData();
				}else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment before place order");
					} else {
						Intent io = new Intent(MerchantComplaintActivity.this,
								AddMerchantNew.class);
						startActivity(io);
						finish();
					}
				}


			}
		});

		linearLayoutdownloadscheme.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				if(netCheck()){

					final Dialog qtyDialog = new Dialog(MerchantComplaintActivity.this);
					qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					qtyDialog.setContentView(R.layout.warning_dialog);

					final Button buttonUpdateOk, buttonUpdateCancel;

					final TextView textQtyValidate;

					buttonUpdateOk = (Button) qtyDialog
							.findViewById(R.id.buttonUpdateOk);
					buttonUpdateCancel = (Button) qtyDialog
							.findViewById(R.id.buttonUpdateCancel);

					textQtyValidate = (TextView) qtyDialog
							.findViewById(R.id.textQtyValidate);
					qtyDialog.show();


					buttonUpdateOk.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							databaseHandler.deletescheme();
							Constants.downloadScheme = "postnotes";
							Intent io = new Intent(MerchantComplaintActivity.this, SchemeDownload.class);
							startActivity(io);
							finish();
							qtyDialog.dismiss();
						}

					});

					buttonUpdateCancel.setOnClickListener(new View.OnClickListener() {

								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub

									qtyDialog.dismiss();

								}
							});

				}else{
					showAlertDialogToast("Please Check Your internet connection");
				}

			}
		});
		linearLayoutsurveyuser.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				moveto = "surveyuser";
				Constants.setimage=null;
				Constants.setdiscription="";
				Constants.setdealerpostnts="";
				Constants.setduserid="";
				if(netCheckWithoutAlert()==true){
					getData();
				}else{
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment before place order");
					} else {
						Intent to = new Intent(MerchantComplaintActivity.this, SurveyMerchant.class);
						startActivity(to);
						finish();
					}
				}
			}
		});
		linearLayoutExport.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				Constants.filter="0";
				if(netCheckWithoutAlert()==true){
					getDetails("export");
				}
				else{
					showAlertDialogToast("Please Check Your internet connection");
				}

			}
		});
		/*linearLayoutClientVisit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				if(netCheckWithoutAlert()==true){
					getDetails("clientvisit");
				}else{
					showAlertDialogToast("Please Check Your internet connection");
				}
			}
		}); */ // kumaravel repeat code
		linearLayoutClientVisit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				if(netCheckWithoutAlert()==true){
					getDetails("clientvisit");
				}else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment before place order");
					} else {
						Intent io = new Intent(MerchantComplaintActivity.this,
								SMClientVisitHistory.class);
						startActivity(io);
						finish();
					}
				}
			}
		});
		linearLayoutDistanceCalculation.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;

					Intent io = new Intent(MerchantComplaintActivity.this, DistanceCalActivity.class);
					startActivity(io);

			}
		});
		linearLayoutMyDayPlan.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				moveto = "mydayplan";
				Constants.setimage=null;
				Constants.setdiscription="";
				Constants.setdealerpostnts="";
				Constants.setduserid="";
				if( netCheckWithoutAlert()==true){
					getData();
				}else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment before place order");
					} else {
						Intent io = new Intent(MerchantComplaintActivity.this,
								MyDayPlan.class);
						startActivity(io);
						finish();
					}
				}



			}
		});

		linearLayoutMySales.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				moveto = "mysales";
				Constants.setimage=null;
				Constants.setdiscription="";
				Constants.setdealerpostnts="";
				Constants.setduserid="";
				if( netCheckWithoutAlert()==true){
					getData();
				}else{

					showAlertDialogToast("Please Check Your internet connection");
				}


			}
		});

		linearLayoutSignout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				// TODO Auto-generated method stub
				databaseHandler.delete();
				Intent io = new Intent(MerchantComplaintActivity.this, SigninActivity.class);
				Constants.setimage=null;
				Constants.setdiscription="";
				Constants.setdealerpostnts="";
				Constants.setduserid="";
				startActivity(io);
				finish();
			}
		});

		if(Constants.setimage==null){
			Log.e("inside", String.valueOf(Constants.setimage));
			imageView.setVisibility(ImageView.GONE);
		mBitmap1=null;
			path1="";
			photo.setVisibility(Button.VISIBLE);
		}else{
			imageView.setVisibility(ImageView.VISIBLE);
			imageView.setImageBitmap(Constants.setimage);
			photo.setVisibility(Button.GONE);

		}

		imageView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
            Intent io=new Intent(MerchantComplaintActivity.this,FullImagePostNoteActivity.class);
				Constants.setdiscription=EditTextViewDescription.getText().toString();
				io.putExtra("id", imageId);
				startActivity(io);
				finish();
			}
		});


		photo.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				int permissionCheck = ContextCompat.checkSelfPermission(MerchantComplaintActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

				if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
					ActivityCompat.requestPermissions(
							MerchantComplaintActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constants.WRITE_EXTERNAL_STORAGE);
				} else {
					Log.e("1", "1");
					Constants.setimage=null;
					callMethod();
				}

			}

		});

		linearLayoutMigration.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				Intent io = new Intent(MerchantComplaintActivity.this,
						PendingDataMigrationActivity.class);
				startActivity(io);
			}
		});

		linearLayoutStockOnly.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				Constants.checkproduct = 0;
				Constants.orderid = "";
				Constants.SalesMerchant_Id = "";
				Constants.Merchantname = "";
				MyApplication.getInstance().setTemplate8ForTemplate2(true);
				if (netCheck()) {
					Intent io = new Intent(MerchantComplaintActivity.this,
							ClosingStockDashBoardActivity.class);
					startActivity(io);
					finish();
				} else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals("") || PaymentStatus.equalsIgnoreCase("null")) {
						showAlertDialogToast("Please make payment before place order");
					} else {
						Intent io = new Intent(MerchantComplaintActivity.this,
								ClosingStockDashBoardActivity.class);
						startActivity(io);
						finish();
					}
				}
			}
		});

		linearLayoutStockAudit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				MyApplication.getInstance().setTemplate8ForTemplate2(false);
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				Constants.checkproduct = 0;
				Constants.orderid = "";
				Constants.SalesMerchant_Id = "";
				Constants.Merchantname = "";
				if (netCheck()) {
					Intent io = new Intent(MerchantComplaintActivity.this,
							ClosingStockAudit.class);
					startActivity(io);
					finish();
				} else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals("") || PaymentStatus.equalsIgnoreCase("null")) {
						showAlertDialogToast("Please make payment before place order");
					} else {
						Intent io = new Intent(MerchantComplaintActivity.this,
								ClosingStockAudit.class);
						startActivity(io);
						finish();
					}
				}
			}
		});

		///////////
		showMenu();
		///////////
	}
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		switch (requestCode) {

			case  Constants.WRITE_EXTERNAL_STORAGE:
				if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
					Constants.setimage=null;
					callMethod();
				}
				break;

			default:
				break;
		}
	}
	private void  callMethod(){

		String fileName = "Camera_Example.jpg";

		ContentValues values = new ContentValues();

		values.put(MediaStore.Images.Media.TITLE, fileName);

		values.put(MediaStore.Images.Media.DESCRIPTION,"Image capture by camera");

		imageUri = getContentResolver().insert(
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

		Log.e("values", String.valueOf(values));

		Intent intent = new Intent( MediaStore.ACTION_IMAGE_CAPTURE );

		intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);

		intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);

		startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
		Log.e("2", "2");

	}
	@Override
	protected void onActivityResult( int requestCode, int resultCode, Intent data)
	{
		if ( requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {

			if ( resultCode == RESULT_OK) {
				Log.e("3","3");
				imageId = convertImageUriToFile( imageUri,CameraActivity);

Log.e("what???",imageId);
				new LoadImagesFromSDCard().execute(""+imageId);

			} else if ( resultCode == RESULT_CANCELED) {


			} else {


			}
		}
	}
	public static String convertImageUriToFile ( Uri imageUri, Activity activity )  {

		Cursor cursor = null;
		int imageID = 0;

		try {

			/*********** Which columns values want to get *******/
			String [] proj={
					MediaStore.Images.Media.DATA,
					MediaStore.Images.Media._ID,
					MediaStore.Images.Thumbnails._ID,
					MediaStore.Images.ImageColumns.ORIENTATION
			};
			Log.e("proj", String.valueOf(proj));

			cursor = activity.managedQuery(

					imageUri,         //  Get data for specific image URI
					proj,             //  Which columns to return
					null,             //  WHERE clause; which rows to return (all rows)
					null,             //  WHERE clause selection arguments (none)
					null              //  Order-by clause (ascending by name)

			);

			//  Get Query Data

			int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media._ID);
			int columnIndexThumb = cursor.getColumnIndexOrThrow(MediaStore.Images.Thumbnails._ID);
			int file_ColumnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

			//int orientation_ColumnIndex = cursor.
			//    getColumnIndexOrThrow(MediaStore.Images.ImageColumns.ORIENTATION);

			int size = cursor.getCount();
			Log.e("size", String.valueOf(size));

			/*******  If size is 0, there are no images on the SD Card. *****/

			if (size == 0) {

				Log.e("4","4");

			}
			else
			{
				Log.e("5","5");
				int thumbID = 0;
				if (cursor.moveToFirst()) {

					/**************** Captured image details ************/

					/*****  Used to show image on view in LoadImagesFromSDCard class ******/
					imageID     = cursor.getInt(columnIndex);

					thumbID     = cursor.getInt(columnIndexThumb);

					Path = cursor.getString(file_ColumnIndex);

					//String orientation =  cursor.getString(orientation_ColumnIndex);

					CapturedImageDetails = " CapturedImageDetails : \n\n"
							+" ImageID :"+imageID+"\n"
							+" ThumbID :"+thumbID+"\n"
							+" Path :"+Path+"\n";


					Log.e("CapturedImageDetails", CapturedImageDetails);
					// Show Captured Image detail on activity


				}
			}
		} finally {
			if (cursor != null) {

			}
		}



		return ""+imageID;
	}

	public class LoadImagesFromSDCard  extends AsyncTask<String, Void, Void> {

		private ProgressDialog Dialog = new ProgressDialog(MerchantComplaintActivity.this);



		protected void onPreExecute() {
			/****** NOTE: You can call UI Element here. *****/

			// Progress Dialog
			Dialog.setMessage(" Loading image from Sdcard..");
			Dialog.show();
		}


		// Call after onPreExecute method
		protected Void doInBackground(String... urls) {

			Bitmap bitmap = null;
			Bitmap newBitmap = null;
			Uri uri = null;

			try {

				uri = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "" + urls[0]);


				bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(uri));

				if (bitmap != null) {
					Log.e("6","6");
					newBitmap = Bitmap.createScaledBitmap(bitmap, 350, 250, true);

					bitmap.recycle();

					mBitmap1 = newBitmap;
					path1 =Path;
					Constants.setimage=mBitmap1;
				}
			} catch (IOException e) {

				cancel(true);
			}

			return null;
		}


		protected void onPostExecute(Void unused) {




            imageView.setVisibility(ImageView.VISIBLE);
			imageView.setImageBitmap(mBitmap1);
            photo.setVisibility(Button.GONE);





			Dialog.dismiss();
			Log.e("showImg", "showImg");


		}

	}

	private void getData() {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog= ProgressDialog.show(MerchantComplaintActivity.this, "",
						"Loading...", true, true);

			}

			@Override
			protected void onPostExecute(Void result) {

				try {

					PaymentStatus=(JsonAccountObject.getString("pymtstatus"));
					Log.e("PaymentStatus",PaymentStatus);

					if(PaymentStatus.equals("Pending")||PaymentStatus.equals(null) ||PaymentStatus.equals("")||PaymentStatus.equals("null")){
						showAlertDialogToast("Please make payment to place order");
					}else {
						if(moveto.equals("createorder")){
							Intent io = new Intent(MerchantComplaintActivity.this,
									CreateOrderActivity.class);
							startActivity(io);
							finish();
						}

						else if (moveto.equals("myorder")){
							if (Constants.USER_TYPE.equals("D")) {
								Intent io = new Intent(MerchantComplaintActivity.this,
										DealersOrderActivity.class);
								startActivity(io);
								finish();
							} else if (Constants.USER_TYPE.equals("M")) {
								Intent io = new Intent(MerchantComplaintActivity.this,
										MerchantOrderActivity.class);
								Constants.setimage=null;
								startActivity(io);
								finish();
							} else {
								Intent io = new Intent(MerchantComplaintActivity.this,
										SalesManOrderActivity.class);
								Constants.setimage=null;
								startActivity(io);
								finish();
							}
						}else if(moveto.equals("product")){
							Intent io = new Intent(MerchantComplaintActivity.this, ProductActivity.class);
							startActivity(io);
							Constants.setimage=null;
							finish();
						}else if(moveto.equals("mydealer")){
							Intent io = new Intent(MerchantComplaintActivity.this, MyDealersActivity.class);
							io.putExtra("Key","MenuClick");
							Constants.setimage=null;
							startActivity(io);
							finish();
						}
						else if(moveto.equals("profile")){
							Intent io = new Intent(MerchantComplaintActivity.this, ProfileActivity.class);
							startActivity(io);
							finish();
						}
						else if(moveto.equals("clientvisit")){
							Intent io = new Intent(MerchantComplaintActivity.this, SMClientVisitHistory.class);
							startActivity(io);
							finish();
						}
						else if(moveto.equals("paymentcoll")){
							Intent io = new Intent(MerchantComplaintActivity.this, SalesmanPaymentCollectionActivity.class);
							startActivity(io);
                          		finish();
						}else if(moveto.equals("payment")){
                            Intent io = new Intent(MerchantComplaintActivity.this, PaymentActivity.class);
                            startActivity(io);
                            finish();
                        } else if(moveto.equals("addmerchant")){
							Intent io = new Intent(MerchantComplaintActivity.this,
									AddMerchantNew.class);
							startActivity(io);
							finish();
						}
						else if(moveto.equals("stockentry")){
							Intent io = new Intent(MerchantComplaintActivity.this,
									OutletStockEntry.class);
							startActivity(io);
							finish();
						}
						else if(moveto.equals("acknowledge")){
							Intent io = new Intent(MerchantComplaintActivity.this,
									SalesmanAcknowledgeActivity.class);
							startActivity(io);
							finish();
						}
						else if(moveto.equals("surveyuser")){
							Intent io = new Intent(MerchantComplaintActivity.this,
									SurveyMerchant.class);
							startActivity(io);
							finish();
						}
						else if(moveto.equals("mydayplan")){
							Intent io = new Intent(MerchantComplaintActivity.this,
									MyDayPlan.class);
							startActivity(io);
							finish();
						}
						else if(moveto.equals("mysales")){
							Intent io = new Intent(MerchantComplaintActivity.this,
									MySalesActivity.class);
							startActivity(io);
							finish();
						} else if (moveto.equals("diststock")) {
							Intent io = new Intent(MerchantComplaintActivity.this,
									DistributrStockActivity.class);
							startActivity(io);
							finish();
						}
					}

					dialog.dismiss();

				}catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
				}

			}

			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();


				JsonServiceHandler = new JsonServiceHandler(Utils.strGetProfileDetail+Constants.USER_ID, MerchantComplaintActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceDataGet();
				return null;
			}
		}.execute(new Void[]{});
	}
	private void SendComplaint() {
		// TODO Auto-generated method stub
		
	
	new AsyncTask<Void, Void, Void>() {
		ProgressDialog dialog;
		String strStatus = "";
		String strMsg = "";

		@Override
		protected void onPreExecute() {
			dialog= ProgressDialog.show(MerchantComplaintActivity.this, "",
					"Loading...", true, true);

		}

		@Override
		protected void onPostExecute(Void result) {

			try {

				strStatus = JsonAccountObject.getString("status");
				Log.e("return status", strStatus);
				strMsg = JsonAccountObject.getString("message");
				Log.e("return message", strMsg);

				if (strStatus.equals("true")) {
					
					autocompleteTextTraders.setText("");
					EditTextViewDescription.setText("");
					showAlertDialogToast(strMsg);
				}else{
					
					showAlertDialogToast(strMsg);
				}
				dialog.dismiss();
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (Exception e) {

			}

		}

		@Override
		protected Void doInBackground(Void... params) {

			JSONObject jsonObject = new JSONObject();
			try {
				jsonObject.put("muserid", Constants.USER_ID);
				jsonObject.put("duserid", DuserID);
				jsonObject.put("particular", Description);
			
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			

			JsonServiceHandler = new JsonServiceHandler(Utils.strAddComplaint,jsonObject.toString(), MerchantComplaintActivity.this);
			JsonAccountObject = JsonServiceHandler.ServiceData();
			return null;
		}
	}.execute(new Void[] {});
}
	private void loadingDealer() {
		new AsyncTask<Void, Void, Void>() {
			String cityName;

			@Override
			protected void onPreExecute() {

			}

			@Override
			protected void onPostExecute(Void result) {
				try {
					MDealerCompDropdownDomain cd;
					
					JSONArray JsonArray = JsonAccountObject.getJSONArray("data");
					for (int i = 0; i < JsonArray.length(); i++) {
						cd = new MDealerCompDropdownDomain();
						JsonAccountObject = JsonArray.getJSONObject(i);

						cd.setName(JsonAccountObject.getJSONObject("D").getString("fullname"));
						cd.setComName(JsonAccountObject.getJSONObject("D").getString("companyname"));
						cd.setID(JsonAccountObject.getString("duserid"));

						arrayListDealerDomain.add(cd);
						arrayListDealer.add(JsonAccountObject.getJSONObject("D").getString("fullname"));
						// Log.e(id,name+" "+JsonAccountObject.getString("type"));
					}

				} catch (JSONException e) {
					Log.e("JSONException", e.toString());
					// toastDisplay("Service Error");
				} catch (Exception e) {
					Log.e("Exception", e.toString());
					// toastDisplay("Something went wrong");
				}

				ArrayAdapter<String> adapter = new ArrayAdapter<String>(
						MerchantComplaintActivity.this, R.layout.textview, R.id.textView,
						arrayListDealer);
				autocompleteTextTraders.setAdapter(adapter);
			}

			@Override
			protected Void doInBackground(Void... params) {

				JsonServiceHandler = new JsonServiceHandler(Utils.strAddComplaintGetDealer+Constants.USER_ID,
						 MerchantComplaintActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceDataGet();
				return null;
			}
		}.execute(new Void[] {

		});
	}
	private void getDetails(final String paymentcheck) {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog= ProgressDialog.show(MerchantComplaintActivity.this, "",
						"Loading...", true, true);

			}

			@Override
			protected void onPostExecute(Void result) {

				try {

					PaymentStatus=(JsonAccountObject.getString("pymtstatus"));
					Log.e("PaymentStatus",PaymentStatus);


					if(PaymentStatus.equals("Pending")||PaymentStatus.equals(null) ||PaymentStatus.equals("")||PaymentStatus.equals("null")){
						showAlertDialogToast("Please make payment before place order");
					}else{
						Log.e("paymentcheck", paymentcheck);

						if(paymentcheck.equals("profile")){
							Intent io = new Intent(MerchantComplaintActivity.this,
									ProfileActivity.class);
							startActivity(io);
							finish();

						}else if(paymentcheck.equals("product")){
							Intent io = new Intent(MerchantComplaintActivity.this,
									ProductActivity.class);
							startActivity(io);
							finish();

						}else if(paymentcheck.equals("export")){
							Intent io = new Intent(MerchantComplaintActivity.this,
									ExportActivity.class);
							startActivity(io);
							finish();

						}else if(paymentcheck.equals("myorders")){
							Intent to = new Intent(MerchantComplaintActivity.this,
									DealersOrderActivity.class);
							startActivity(to);
							finish();

						}else if(paymentcheck.equals("clientvisit")){

							Intent io = new Intent(MerchantComplaintActivity.this,
									DealerClientVisit.class);
							startActivity(io);
							finish();

						}



					}

					dialog.dismiss();


				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
				}

			}

			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();


				JsonServiceHandler = new JsonServiceHandler(Utils.strGetProfileDetail+Constants.USER_ID, MerchantComplaintActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceDataGet();
				return null;
			}
		}.execute(new Void[]{});
	}


	protected void toastDisplay(String msg) {
	Toast toast = Toast.makeText(MerchantComplaintActivity.this, msg,
			Toast.LENGTH_SHORT);
	toast.setGravity(Gravity.CENTER, 0, 0);
	toast.show();

}
public boolean netCheck() {
	// for network connection
	try {
		ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo mWifi = connManager
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

		ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo mNetwork = connectionManager
				.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

		Object result = null;
		if (mWifi.isConnected() || mNetwork.isConnected()) {
			return true;
		}

		else if (result == null) {
			showAlertDialog(MerchantComplaintActivity.this,
					"No Internet Connection",
					"Please Check Your internet connection.", false);
			return false;
		}
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return false;
}

	public boolean netCheckWithoutAlert() {
		// for network connection
		try {
			ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetwork = connectionManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			Object result = null;
			if (mWifi.isConnected() || mNetwork.isConnected()) {
				return true;
			}

			else if (result == null) {

				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
public void showAlertDialog(Context context, String title, String message,
		Boolean status) {
	AlertDialog alertDialog = new AlertDialog.Builder(context).create();
	alertDialog.setTitle(title);
	alertDialog.setMessage(message);
	alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
	alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
			new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
				}
			});
	alertDialog.show();
}

	public void showAlertDialogToast( String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();

		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

	}

	public void showAlert( String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
						MerchantComplaintActivity.this.finish();
						startActivity(getIntent());
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();

		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

	}


	public void onBackPressed() {
		exitAlret();
	}

	private void exitAlret() {
		AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
		localBuilder.setCancelable(false);
		localBuilder.setMessage("Do you want to Exit?");
		localBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {

						finish();

					}
				});
		localBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {
						paramDialogInterface.cancel();
					}
				});
		localBuilder.create().show();
	}

	private void loadOfflinePostNote(String description, String duserID, String userId){
	    try {
            ContentValues cv = new ContentValues();
            cv.put(DatabaseHandler.KEY_POST_NOTE_USER_ID, userId);
            cv.put(DatabaseHandler.KEY_POST_NOTE_D_USER_ID, duserID);
            cv.put(DatabaseHandler.KEY_POST_NOTE_DESCRIPTION, description);
            if (imageView.getVisibility() == View.VISIBLE) {
                Bitmap bm =((BitmapDrawable)imageView.getDrawable()).getBitmap();
                cv.put(DatabaseHandler.KEY_POST_NOTE_IMAGE, DbBitmapUtility.getImageBytes(bm));
            } else {
                cv.put(DatabaseHandler.KEY_POST_NOTE_IMAGE, Constants.EMPTY);
            }
            cv.put(DatabaseHandler.KEY_POST_NOTE_IS_NEED_TO_MIGRATE, 1); // one indicate need to migrate data from local to server
			databaseHandler.loadPostNote(cv);
        }catch (Exception e){
	        e.printStackTrace();
			showAlert("Something Wrong May Need Technical Help");
        }
	}

}
