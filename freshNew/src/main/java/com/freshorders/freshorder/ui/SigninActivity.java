package com.freshorders.freshorder.ui;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.activity.ManagerActivity;
import com.freshorders.freshorder.asyntask.LoadOnlineOrder;
import com.freshorders.freshorder.crashreport.AppCrashConstants;
import com.freshorders.freshorder.crashreport.AppCrashUtil;
import com.freshorders.freshorder.crashreport.ExternalStorageUtil;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.model.CommonResponseModel;
import com.freshorders.freshorder.receiver.StartTrackingBroadCast;
import com.freshorders.freshorder.receiver.StopTrackingBroadCast;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.MenuSetting;
import com.freshorders.freshorder.utils.MilkSociety;
import com.freshorders.freshorder.utils.NotificationUtil;
import com.freshorders.freshorder.utils.TemplateSetting;
import com.freshorders.freshorder.utils.Utils;
import com.freshorders.freshorder.volleylib.APIClient;
import com.freshorders.freshorder.volleylib.APIInterface;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.freshorders.freshorder.db.DatabaseHandler.KEY_IS_FAT_SNF_TBL_LOADED;
import static com.freshorders.freshorder.db.DatabaseHandler.KEY_IS_SNF_CAL_TBL_LOADED;
import static com.freshorders.freshorder.db.DatabaseHandler.KEY_TEMPLATE_KEY;
import static com.freshorders.freshorder.db.DatabaseHandler.KEY_TEMPLATE_USER_ID;
import static com.freshorders.freshorder.db.DatabaseHandler.KEY_TEMPLATE_VALUE;
import static com.freshorders.freshorder.service.PrefManager.FCM_TOKEN;
import static com.freshorders.freshorder.service.PrefManager.IS_DELIVERED_FCM_TOKEN;
import static com.freshorders.freshorder.service.PrefManager.KEY_IS_HIERARCHY_PLACE_FILE_LOADED;
import static com.freshorders.freshorder.service.PrefManager.KEY_MANAGER_TYPE;
import static com.freshorders.freshorder.service.PrefManager.KEY_PATH_HIERARCHY_PLACE_FILE;
import static com.freshorders.freshorder.utils.Constants.BR_REQUEST_CODE_START_LIVE_TRACK;
import static com.freshorders.freshorder.utils.Constants.BR_REQUEST_CODE_STOP_LIVE_TRACK;
import static com.freshorders.freshorder.utils.Constants.DEFAULT_TEMPLATE_NO;
import static com.freshorders.freshorder.utils.Constants.EMPTY;
import static com.freshorders.freshorder.utils.Constants.MENU_DISTANCE_CALCULATION_CODE;
import static com.freshorders.freshorder.utils.Constants.MILK_TEMPLATE_NO;
import static com.freshorders.freshorder.utils.Constants.PLACE_HIERARCHY_FILE_JSON;
import static com.freshorders.freshorder.utils.Constants.SELF_START_TIME_LIVE_TRACK;
import static com.freshorders.freshorder.utils.Constants.SELF_STOP_TIME_LIVE_TRACK;
import static com.freshorders.freshorder.utils.Utils.strFCMSendURL;
import static com.freshorders.freshorder.utils.Utils.strPlaceHierarchy;

public class SigninActivity extends Activity {

	private static final String TAG = SigninActivity.class.getSimpleName();
	//Kumaravel
	ProgressDialog dialogs = null;

	Button buttonSignUp, buttonSignIn,buttonGetPassword,buttonCancel;
	ProgressBar mProgressBar;
	EditText EditTextViewMobile, EditTextViewPassword,editTextEnterMobile;
	ObjectAnimator progressAnimator;
	JSONObject JsonAccountObject = null;
	JSONArray JsonAccountArray = null;
	DatabaseHandler databaseHandler;
	JsonServiceHandler JsonServiceHandler;
	Button buttonOTPOK, buttonOTPResend;
	public static String strOTP,strForgotmblnum,clientdealerid;
	String strmobile;
	Dialog dialogOtp,dialogForgotPassword;
	static EditText editTextEnterOTP;
	TextView textViewTimer,textViewForgotPassword,textViewTimerText;

	public static String strUT="null",strID="null",strName="null",profilestatus="null",payment="null",paymentStatus="null",email="null",dealerid="",dealercompname="",dealfullname="",dealmob="",dealaddr="";

	// Kumaravel
	String strUN;
	String strPW;
	private String TEMPLATE_TYPE = "0";


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(1);
		setContentView(R.layout.signin_screen);
		netCheck();

		//Kumaravel 14-07-2019
		new PrefManager(this).setStringDataByKey(PrefManager.KEY_DISTRIBUTOR_NAME,"");
		Constants.distributorname = "";

		mProgressBar = (ProgressBar) findViewById(R.id.progressBar1);
		progressAnimator = ObjectAnimator.ofInt(mProgressBar, "progress", 0,
				1000);
		databaseHandler = new DatabaseHandler(getApplicationContext());
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();

		textViewForgotPassword= (TextView) findViewById(R.id.textViewForgotPassword);
		EditTextViewMobile = (EditText) findViewById(R.id.EditTextViewMobile);
		EditTextViewPassword = (EditText) findViewById(R.id.EditTextViewPassword);
		buttonSignIn = (Button) findViewById(R.id.buttonSignIn);
		buttonSignUp = (Button) findViewById(R.id.buttonSignUp);
		
		
		dialogOtp = new Dialog(SigninActivity.this);
		dialogOtp.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialogOtp.setContentView(R.layout.otp_registration_screen);
		editTextEnterOTP = (EditText) dialogOtp
				.findViewById(R.id.editTextEnterOTP);

		buttonOTPOK = (Button) dialogOtp.findViewById(R.id.buttonOTPOK);
		textViewTimer = (TextView) dialogOtp.findViewById(R.id.textViewTimer);
		textViewTimerText= (TextView) dialogOtp.findViewById(R.id.textViewTimerText);
		buttonOTPResend = (Button) dialogOtp.findViewById(R.id.buttonOTPResend);
		strOTP = editTextEnterOTP.getText().toString();

		dialogForgotPassword= new Dialog(SigninActivity.this);
		dialogForgotPassword.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialogForgotPassword.setContentView(R.layout.forgot_password_screen);
		editTextEnterMobile = (EditText) dialogForgotPassword
				.findViewById(R.id.editTextEnterMobile);

		buttonGetPassword = (Button) dialogForgotPassword.findViewById(R.id.buttonGetPassword);

		buttonCancel = (Button) dialogForgotPassword.findViewById(R.id.buttonCancel);
		strForgotmblnum = editTextEnterMobile.getText().toString();

		textViewForgotPassword.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				//dialogForgotPassword.show();
				String strblnum = EditTextViewMobile.getText().toString();
				if(!strblnum.isEmpty()&&strblnum.startsWith("")){
					getPassword(strblnum);
				}else{
					showAlertDialogToast("Mobile number should not be empty");
				}

			}
		});
		buttonCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				dialogForgotPassword.dismiss();

			}
		});

		buttonGetPassword.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				strForgotmblnum = EditTextViewMobile.getText().toString();
				if (!strForgotmblnum.isEmpty() && !strForgotmblnum.startsWith(" ")) {


				//	getPassword(strblnum);


				}
			}
		});

		buttonOTPOK.setOnClickListener(new OnClickListener() {

			@SuppressLint("NewApi")
			@Override
			public void onClick(View v) {

				strOTP = editTextEnterOTP.getText().toString();
				if (!strOTP.isEmpty() && !strOTP.startsWith(" ")) {
					
						buttonOTPResend.setEnabled(true);
						buttonOTPOK.setEnabled(true);
						editTextEnterOTP.setEnabled(true);
						otpCheck();

					
				}

			}
		});

		buttonOTPResend.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				

				otpResend();
				

			}
		});
		
		
		
		buttonSignUp.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.e("boolean", String.valueOf(signinnetcheck()));
				if(signinnetcheck()==false){
					showAlertDialog(SigninActivity.this, "No Internet Connection",
							"Please Check Your internet connection.", false);
					Log.e("inside","isnside");
			}else{
					Log.e("outside","outside");
					Intent io = new Intent(SigninActivity.this,
							SignupActivity.class);
					startActivity(io);
					finish();
					overridePendingTransition(R.anim.fadein, R.anim.fade_out);
				}}
		});

		buttonSignIn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				/*netCheck();*/
			
				strUN = EditTextViewMobile.getText().toString();
				strPW = EditTextViewPassword.getText().toString();

				if (!strUN.isEmpty() && !strUN.startsWith(" ")) {
                   // if (strUN.length()>9) {
                        if (!strPW.isEmpty() && !strPW.startsWith(" ")) {
                          // if (strPW.length() > 8) {
                                if (netCheck()) {
                                    loginCheck(strUN, strPW);
                                }

                                Log.e("en", "en");

                                Constants.Merchantname = "";
                                Constants.SalesMerchant_Id = "";
                          /* } else {
                                showAlertDialogToast("Password must be contain atleast 8 characters");
                            }*/

                        } else {
                            //toastDisplay("Password should not be empty");
                            showAlertDialogToast("Password should not be empty");
                            EditTextViewPassword.setText("");
                        }
                    /*}else {

                            showAlertDialogToast("Invalid mobile number");
                        }*/
				} else {
					//toastDisplay("Username should not be empty");
					showAlertDialogToast("Mobile number should not be empty");
					EditTextViewMobile.setText("");
				}

				// TODO Auto-generated method stub

			}

		});
	}
public boolean signinnetcheck(){{
	// for network connection
	try {
		ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo mWifi = connManager
				.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

		ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo mNetwork = connectionManager
				.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

		Object result = null;
		if (mWifi.isConnected() || mNetwork.isConnected()) {
			return true;
		}

		else if (result == null) {

			return false;
		}
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	return false;
}

}
	private void getPassword(final String strblnum) {
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog = ProgressDialog.show(SigninActivity.this, "In Progress",
						"Validating...", true, true);
			}

			@Override
			protected void onPostExecute(Void result) {
				try {
					// JsonAccountArray =
					// JsonAccountObject.getJSONArray("response");
					// for (int i = 0; i < JsonAccountArray.length(); i++) {
					// JSONObject job=new JSONObject();
					// job=JsonAccountArray.getJSONObject(0);
					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);

					if (strStatus.equals("true")) {
						//toastDisplay("" + strMsg);
						showAlertDialogToast(strMsg);
					//	dialogForgotPassword.dismiss();
					//	Intent intentLogin = new Intent(SigninActivity.this,
				//				SigninActivity.class);
				//		startActivity(intentLogin);
				//		finish();
//
					} else {
						//toastDisplay("" + strMsg);
						showAlertDialogToast(strMsg);
						editTextEnterMobile.setText("");


					}

				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					Log.e("loginActivityException", e.toString());
				}

				dialog.dismiss();
			}

			@Override
			protected Void doInBackground(Void... params) {
				dialog.show();

				JSONObject jsonObject = new JSONObject();
				try {

					jsonObject.put("mobileno", strblnum);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				JsonServiceHandler = new JsonServiceHandler(
						Utils.strForgotPassword, jsonObject.toString(),
						SigninActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceData();
				return null;
			}
		}.execute(new Void[] {});

}

	private void loginCheck(final String strUN, final String strPW) {
		// TODO Auto-generated method stub

		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				mProgressBar.setVisibility(View.VISIBLE);
				buttonSignIn.setText("Loading..");
				progressAnimator.setDuration(1000);
				progressAnimator.setRepeatCount(40);
				progressAnimator.setInterpolator(new LinearInterpolator());
				progressAnimator.start();

			}

			@Override
			protected void onPostExecute(Void result) {

				try {
					Log.e("Login","...................." + JsonAccountObject.toString());
					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);
					Log.e("LoginResponse","...........................welcome"+strStatus);
					if (strStatus.equals("true")) {

						mProgressBar.setVisibility(View.GONE);
						buttonSignIn.setText("Success");
						buttonSignIn.setBackgroundColor(Color.parseColor("#b1d36f"));;
						JSONObject job = new JSONObject();



						try {
						job = JsonAccountObject.getJSONObject("data");
						Log.e("LoginData","............"+job.toString());
						///Kumaravel
                            if(job.has("usertype")){

                                Constants.USER_TYPE = job.getString("usertype");
                                if(Constants.USER_TYPE.equals("D")) {
									new PrefManager(SigninActivity.this).setStringDataByKey(KEY_MANAGER_TYPE, "D");
                                    Constants.DUSER_ID = job.getString("userid");

									loginDetailStoreForDistributor(job);////////////

                                    Intent intent = new Intent(SigninActivity.this, ManagerActivity.class);
                                    ///////////end
                                    startActivity(intent);
                                    finish();//////////added
                                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                                    finish();
                                }else if(Constants.USER_TYPE.equals("U")){
									Constants.DUSER_ID = job.getString("duserid");

									loginDetailStoreForManagerType_U(job);////////////
									new PrefManager(SigninActivity.this).setStringDataByKey(KEY_MANAGER_TYPE, "U");

									///////////////// use string response
									String responseString = JsonServiceHandler.getJson();

									loadHierarchyPlaceFile_Type_U(job, responseString);
								}
                            }

						JSONObject jsonDealer=job.optJSONObject("dealer");
						if(jsonDealer != null)
						Constants.DUSER_ID=strID = jsonDealer.optString("duserid");

							// Kumaravel for setting Template
							JSONObject d = jsonDealer.getJSONObject("D");
							JSONArray settings = d.getJSONArray("settings");
							for(int z = 0; z < settings.length(); z++){
								JSONObject obj = settings.getJSONObject(z);
								Log.e("refkey",".............:::"+obj.get("refkey"));
								Log.e("refvalue",".............:::"+obj.get("refvalue"));
								Log.e("userid",".............:::"+obj.get("userid"));
								if(obj.get("refkey").equals("ORDTEMPLATE")) {
									String strTemplateNo  = obj.get("refvalue").toString();
									if(strTemplateNo.isEmpty()){
										TEMPLATE_TYPE = DEFAULT_TEMPLATE_NO; /// this is fixed default Model
									}else {
										TEMPLATE_TYPE = strTemplateNo;
									}
                                    loadTemplate(job,obj.get("userid").toString(), "ORDTEMPLATE", obj.get("refvalue").toString());
									break;
								}else if( z == settings.length() - 1){  /// not containing order template key so assign default template
									TEMPLATE_TYPE = DEFAULT_TEMPLATE_NO;  /// this is fixed default Model
									loadTemplate(job,obj.get("userid").toString(), "ORDTEMPLATE", TEMPLATE_TYPE);
								}
							}

						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}else{
						
    if(strMsg.equals("Account verification pending, Please activate your account")){
	mProgressBar.setVisibility(View.GONE);
	buttonSignIn.setText("Signin");
	buttonSignIn.setBackgroundColor(Color.parseColor("#ecce03"));
	dialogOtp.show();
	
	 new CountDownTimer(30000, 1000) {

	    public void onTick(long millisUntilFinished) {
	    	textViewTimer.setText("We are trying read the OTP message automatically waiting for " + millisUntilFinished / 1000  +" seconds ..");
	    }

	    public void onFinish() {
	    	textViewTimer.setText("Enter the OTP");
	    }
	 }.start();
						}else{
							mProgressBar.setVisibility(View.GONE);
							buttonSignIn.setText("Signin");
							buttonSignIn.setBackgroundColor(Color.parseColor("#ecce03"));;
						//toastDisplay(strMsg);
	showAlertDialogToast(strMsg);
						}
					}


				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					//Log.e("ProductActivityException", e.toString());
				}

			}

			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();
				JSONObject jsonObject1 = new JSONObject();

				try {
					jsonObject.put("mobileno", strUN);
					jsonObject.put("password", strPW);


				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		//String URL=Utils.strSignInUrl+strUN+Utils.strSignInUrlAdd+strPW;
				Log.e("SinginURL",".............." + jsonObject.toString());
				JsonServiceHandler = new JsonServiceHandler(Utils.strSignInUrl,jsonObject.toString(), SigninActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceData();
				return null;
			}
		}.execute(new Void[] {});
	}
	
	private void otpCheck() {
		strOTP = editTextEnterOTP.getText().toString();
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog = ProgressDialog.show(SigninActivity.this, "In Progress",
						"Validating...", true, true);
			}

			@Override
			protected void onPostExecute(Void result) {
				try {
					// JsonAccountArray =
					// JsonAccountObject.getJSONArray("response");
					// for (int i = 0; i < JsonAccountArray.length(); i++) {
					// JSONObject job=new JSONObject();
					// job=JsonAccountArray.getJSONObject(0);
					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);

					if (strStatus.equals("true")) {
						//showAlertDialogToast(strMsg);
						textViewTimerText.setText(strMsg);
						//toastDisplay("" + strMsg);
						dialogOtp.dismiss();
						Intent intentLogin = new Intent(SigninActivity.this,
								SigninActivity.class);
						startActivity(intentLogin);
						finish();

					} else {
						//showAlertDialogToast(strMsg);
						textViewTimerText.setText(strMsg);
						//toastDisplay("" + strMsg);
						editTextEnterOTP.setText("");
						buttonOTPResend.setEnabled(true);
						buttonOTPOK.setEnabled(true);
						editTextEnterOTP.setEnabled(true);

					}

				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					Log.e("loginActivityException", e.toString());
				}

				dialog.dismiss();
			}

			@Override
			protected Void doInBackground(Void... params) {
				dialog.show();

				JSONObject jsonObject = new JSONObject();
				try {
				jsonObject.put("otp", strOTP);
				jsonObject.put("mobileno", strmobile);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				JsonServiceHandler = new JsonServiceHandler(
						Utils.strVerifyOtp, jsonObject.toString(),
						SigninActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceData();
				return null;
			}
		}.execute(new Void[] {});
	}
	
	private void otpResend() {
	
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog = ProgressDialog.show(SigninActivity.this, "In Progress",
						"Validating...", true, true);
			}

			@Override
			protected void onPostExecute(Void result) {
				try {
					// JsonAccountArray =
					// JsonAccountObject.getJSONArray("response");
					// for (int i = 0; i < JsonAccountArray.length(); i++) {
					// JSONObject job=new JSONObject();
					// job=JsonAccountArray.getJSONObject(0);
					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);

					if (strStatus.equals("true")) {
						//showAlertDialogToast(strMsg);
						//toastDisplay("" + strMsg);
						textViewTimerText.setText(strMsg);
						
					} else {
						//showAlertDialogToast(strMsg);
						//toastDisplay("" + strMsg);
						textViewTimerText.setText(strMsg);
						

					}

				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					Log.e("loginActivityException", e.toString());
				}

				dialog.dismiss();
			}

			@Override
			protected Void doInBackground(Void... params) {
				dialog.show();

				JSONObject jsonObject = new JSONObject();
				try {
				
				jsonObject.put("mobileno", strmobile);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				JsonServiceHandler = new JsonServiceHandler(
						Utils.strResendOtp, jsonObject.toString(),
						SigninActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceData();
				return null;
			}
		}.execute(new Void[] {});
	}
	public void merchantCount() {
		new AsyncTask<Void, Void, Void>() {
			String strStatus = "", strMsg;
			//ProgressDialog dialog;
			String city;

			@Override
			protected void onPreExecute() {

			}
			@Override
			protected void onPostExecute(Void result) {

				try {
					Log.e("onPostExecute","onPostExecute");
					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);
					//int in_count= databaseHandler.getMerchantCount()- Integer.parseInt(Constants.onLineCount);
                    if(strStatus.equalsIgnoreCase("true")) {
                        Constants.onLineCount = JsonAccountObject.getInt("count");
                        databaseHandler.addConstant("OUTLETCOUNT", "" + Constants.onLineCount);

                        Log.e("countMerchant", JsonAccountObject.getString("count"));
                    }else{
						databaseHandler.addConstant("OUTLETCOUNT","0");
					}

					//Log.e("OfflineCount", String.valueOf(databaseHandler.getMerchantCount()));
				} catch (JSONException e) {
					e.printStackTrace();

				}
//				dialog.dismiss();
			}


			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();
				try {
					jsonObject.put("userid", Constants.USER_ID);
					jsonObject.put("usertype", Constants.USER_TYPE);




				JsonServiceHandler = new JsonServiceHandler(Utils.strGetMerchantList, jsonObject.toString(), SigninActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceData();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return null;
			}
		}.execute(new Void[]{});
	}


	private void DistributorLoad() {
		Log.e("DistributorLoad","DistributorLoad");
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog = ProgressDialog.show(SigninActivity.this, "",
						"Distributor Loading...", true, false);

			}

			@Override
			protected void onPostExecute(Void result) {

				try {
				    if(JsonAccountObject != null) {
                        strStatus = JsonAccountObject.getString("status");
                        Log.e("return status", strStatus);
                        strMsg = JsonAccountObject.getString("message");
                        Log.e("return message", strMsg);
                        if (strStatus.equals("true")) {
                            JSONArray distributorArray = JsonAccountObject.getJSONArray("data");
                            if(distributorArray.length() > 0){
                                List<ContentValues> listCV = new ArrayList<>();
                                for(int i = 0; i < distributorArray.length(); i++){
                                    ContentValues cv = new ContentValues();
                                    JSONObject obj = distributorArray.getJSONObject(i);
                                    String distributorId = obj.getString("distributorid");
                                    String distributorName = obj.getString("distributorname");
                                    String mobileNo = obj.getString("mobileno");
                                    cv.put(DatabaseHandler.KEY_DISTRIBUTOR_ID, distributorId);
                                    cv.put(DatabaseHandler.KEY_DISTRIBUTOR_NAME, distributorName);
                                    cv.put(DatabaseHandler.KEY_DISTRIBUTOR_MOBILE, mobileNo);
                                    listCV.add(cv);
                                }
                                databaseHandler.loadDistributor(listCV);
                            }
                        }
                    }

				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(dialog != null) {
                        dialog.dismiss();
                    }
                }

			}

			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();
				try {
					String suserId = "";
					if(databaseHandler == null){
					    databaseHandler = new DatabaseHandler(SigninActivity.this);
                    }
					Cursor cur = databaseHandler.getDetails();
					if(cur != null && cur.getCount() > 0){
						cur.moveToFirst();
						suserId = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_id));
                        cur.close();
					}


					jsonObject.put("suserid",suserId);
					Log.e("suserid-----",suserId);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				JsonServiceHandler = new JsonServiceHandler(Utils.strDistributor, jsonObject.toString(),SigninActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceDataGet();
				return null;
			}
		}.execute(new Void[]{});
	}

	public void SalesmanDataLink() {
		new AsyncTask<Void, Void, Void>() {

			String strStatus = "", strMsg = "";
			JSONObject JsonAccountObject=new JSONObject();

			@Override
			protected void onPreExecute() {

			}

			@Override
			protected void onPostExecute(Void result) {
				try {



					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);

					if (strStatus.equals("true")) {
						JSONArray jobjson = JsonAccountObject.getJSONArray("data");
						Log.e("job1NEW_______", String.valueOf(jobjson));
						for (int i = 0; i < jobjson.length(); i++) {

							JSONObject jobobj = new JSONObject();
							jobobj = jobjson.getJSONObject(i);
							Log.e("jobNEW_______", String.valueOf(jobobj));
							//String settingid = jobobj.getString("settingid");

							String userid = jobobj.getString("userid");
							String refkey = jobobj.getString("refkey");
							String refvalue = jobobj.getString("refvalue");

//
//							String industryid = jobobj.getString("industryid");
//							String updateddt = jobobj.getString("updateddt");
//							Log.e("refvalue--", String.valueOf(refvalue));
//							//String data_type = jobobj.getString("data_type");
//							String remarks = jobobj.getString("status");
//
							System.out.println("SHISETTINGS : userid,refkey,refvalue:"+userid+"¬"+refkey+"¬"+refvalue);
							databaseHandler.addUserSetting(userid,refkey,refvalue,null,"Active","");


						}
						String strCFV=databaseHandler.getSalesmanDataUserSetting("SCUSFIELD");
						System.out.println("SHICFV:"+strCFV);
					}

				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {

				}

			}


			@SuppressLint("LongLogTag")
			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();
				JsonServiceHandler = new JsonServiceHandler(Utils.strsettingsalesmandata +Constants.DUSER_ID,SigninActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceDataGet();
				System.out.println("SETTINGS------>"+JsonAccountObject);
				System.out.println("URL:"+Utils.strsettingsalesmandata +Constants.DUSER_ID);
				System.out.println("JsonAccountObjectSCUSF:"+JsonAccountObject);


				return null;

			}

		}.execute(new Void[]{});

	}




	protected void toastDisplay(String msg) {
		Toast toast = Toast.makeText(SigninActivity.this, msg,
				Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

	}
	
	  public void recivedSms(String message) 
	     {
	    try 
	      {
	    	Log.e(message, message);
	    	
	    	String[] splited = message.split("\\s+");
	    	editTextEnterOTP.setText(splited[splited.length-1]);
	    	otpCheck();
	      } 
	      catch (Exception e)
	          {         
	                }
	    }

	public boolean netCheck() {
		// for network connection
		try {
			ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetwork = connectionManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			Object result = null;
			if (mWifi.isConnected() || mNetwork.isConnected()) {
				return true;
			}

			else if (result == null) {
				showAlertDialog(SigninActivity.this, "No Internet Connection",
						"Please Check Your internet connection.", false);
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	

	// for network connection messg
			@SuppressWarnings("deprecation")
			public void showAlertDialog(Context context, String title, String message,
					Boolean status) {
				AlertDialog alertDialog = new AlertDialog.Builder(context).create();
				alertDialog.setTitle(title);
				alertDialog.setMessage(message);
				alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
				alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
					}
				});
				alertDialog.show();
			}
	public void showAlertDialogToast( String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();

		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

	}
	public void onBackPressed() {
		exitAlret();
	}

	private void exitAlret() {
		AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
		localBuilder.setCancelable(false);
		localBuilder.setMessage("Do you want to Exit?");
		localBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {

						finish();

					}
				});
		localBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {
						paramDialogInterface.cancel();
					}
				});
		localBuilder.create().show();
	}

	public void loadTemplate(JSONObject job, String strUserid, String templatekey, String templateNO){
		try{
			//////templateNO = "7";
			dialogs = ProgressDialog.show(SigninActivity.this, "In Progress",
					"Initial Setup Loading...", true, false);
			Log.e("loadTemplate", "....................................." + templateNO);
			//DatabaseHandler databaseHandler = new DatabaseHandler(getApplicationContext());
			int userid = Integer.parseInt(strUserid);
			ContentValues contentValues = new ContentValues();
			contentValues.put(KEY_TEMPLATE_USER_ID, userid);
			contentValues.put(KEY_TEMPLATE_KEY, templatekey);
			contentValues.put(KEY_TEMPLATE_VALUE, templateNO);
			databaseHandler.addTemplateType(contentValues);

			TemplateSetting.templateSetting(templateNO); // need to uncomment for proper work
			//TemplateSetting.templateSetting("10");

			if(TEMPLATE_TYPE.equals(MILK_TEMPLATE_NO)){
				Cursor cur = databaseHandler.getDairyDataCheckTbl();
				if(cur != null && cur.getCount() > 0){
					boolean isSnf_cal_loaded = false;
					boolean isFat_snf_loaded = false;
					cur.moveToFirst();
					isSnf_cal_loaded = cur.getInt(cur.getColumnIndex(KEY_IS_SNF_CAL_TBL_LOADED)) == 1;
					isFat_snf_loaded = cur.getInt(cur.getColumnIndex(KEY_IS_FAT_SNF_TBL_LOADED)) == 1;
					// the following is identify which table is need to load
					if(!isFat_snf_loaded || !isSnf_cal_loaded){
						if(!isFat_snf_loaded && !isSnf_cal_loaded){
							doFatSnfMigration(job,true, false);// true indicate Both Table Should Load
						}else if(!isFat_snf_loaded){
							doFatSnfMigration(job,false, false);// false indicate load only one table is enough
						}else if(!isSnf_cal_loaded){
							doSnfCalMigration(job);// false indicate load only one table is enough
						}
					}else {
						commonProcess(job);//////////
					}
				}else {
					doFatSnfMigration(job,true, true);// true indicate Both Table Should Load another one is indicate to identify which query want to be use
				}

				Log.e("JsonAccountObject","............"+JsonAccountObject.toString());
			}else if(!TEMPLATE_TYPE.equals("0")){
				commonProcess(job);
			}else {
				closeDialog();
				showD("Template Model Not Assigned For You");
			}

		}catch (Exception e){
			closeDialog();
			e.printStackTrace();
			showD("Something Went Wrong" + e.getMessage());
		}
	}

	private void doFatSnfMigration(final JSONObject job,final boolean isBothMigration, boolean isFirstTime){
		JsonServiceHandler fatSnfTblService = new JsonServiceHandler(Utils.urlFatSnfTbl, EMPTY, SigninActivity.this);
		new MilkSociety.MilkRateTableFetchAsyncTask(isFirstTime, databaseHandler,fatSnfTblService, new MilkSociety.MilkTableFetchListener() {
			@Override
			public void milkRateFetchFinished(boolean result, String statusMSG, Long inputLength, Long storedLength) {
				try {
					if (result) {
						if (inputLength.equals(storedLength)) {
							changeDialogText("Fat SNF Table Stored Successfully");
							if (isBothMigration) {
								doSnfCalMigration(job); // need to call Common Process inside migration
							} else {
								commonProcess(job);
							}
						} else {
							closeDialog();
							showD("Table data loading incorrectly stopped. So start App again "+statusMSG);
							Log.e("doFatSnfMigration", "............statusMSG.." + statusMSG);
						}
					} else {
						closeDialog();
						showD("Something Went Wrong  " + statusMSG);
						Log.e("doFatSnfMigration", "............statusMSG.." + statusMSG);
					}
				}catch (Exception e){
					e.printStackTrace();
				}
			}
		}).execute();
	}

	private void doSnfCalMigration(final JSONObject job){
		JsonServiceHandler snfCalService = new JsonServiceHandler(Utils.urlSnfCalTbl, EMPTY, SigninActivity.this);
		new MilkSociety.SNFCalculationAsyncTask(snfCalService, databaseHandler, new MilkSociety.SNFCalMigrationListener() {
			@Override
			public void snfCalculationLoadFinished(boolean result, String statusMSG, Long inputLength, Long storedLength) {
				try {
					if (result) {
						if (inputLength.equals(storedLength)) {
							changeDialogText("SNF Calc table stored successfully");
							commonProcess(job);
						} else {
							closeDialog();
							showD("Table data loading incorrectly. App stopped, start App again "+statusMSG);
							Log.e("doSnfCalMigration", "............statusMSG.." + statusMSG);
						}
					} else {
						closeDialog();
						showD("Something Went Wrong  " + statusMSG);
						Log.e("doSnfFatMigration", "............statusMSG.." + statusMSG);
					}
				}catch (Exception e){
					e.printStackTrace();
				}
			}
		}).execute();
	}

	public void changeDialogText(final String strMSG){
		Handler handler = new Handler(Looper.getMainLooper());
		handler.post(new Runnable() {
			@Override
			public void run() {
				// Call UI related methods.
				if(dialogs != null){
					if(dialogs.isShowing()){
						dialogs.setMessage(strMSG);
					}
				}
			}
		});
	}

	public void closeDialog(){
		if(dialogs != null){
			if(dialogs.isShowing()){
				dialogs.dismiss();
			}
		}
	}

	public void showD(final String strMSG){
		Handler handler = new Handler(Looper.getMainLooper());
		handler.post(new Runnable() {
			@Override
			public void run() {
				// Call UI related methods.
				showAlertDialogToast(strMSG);
			}
		});
	}

	public void loginDetailStoreForManagerType_U(JSONObject job){
		try{
			strUT = job.getString("usertype");
			strID = job.getString("userid");
			strName = job.getString("fullname");
			profilestatus = Constants.EMPTY;//job.getString("profilestatus");
			payment = job.getString("pymttenure");
			paymentStatus = job.getString("pymtstatus");
			email = job.getString("emailid");
			Log.e("paymentStatus", paymentStatus);
			Log.e("usertype", strUT);

			String dUserId = job.getString("duserid");

			ContentValues contentValues = new ContentValues();
			contentValues.put(DatabaseHandler.KEY_username, strUN);
			contentValues.put(DatabaseHandler.KEY_password, strPW);
			contentValues.put(DatabaseHandler.KEY_usertype, strUT);
			contentValues.put(DatabaseHandler.KEY_name, strName);
			contentValues.put(DatabaseHandler.KEY_id, strID);
			contentValues.put(DatabaseHandler.KEY_payment, payment);
			contentValues.put(DatabaseHandler.KEY_paymentStatus, paymentStatus);
			contentValues.put(DatabaseHandler.KEY_email, email);
			contentValues.put(DatabaseHandler.KEY_clientdealid, dUserId); ////////////this should use for duserid
			contentValues.put(DatabaseHandler.KEY_selectedBeat, "");
			contentValues.put(DatabaseHandler.KEY_selectedDate, "");

			databaseHandler.addDetails(contentValues);
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	public void loginDetailStoreForDistributor(JSONObject job){
		try{
			strUT = job.getString("usertype");
			strID = job.getString("userid");
			strName = job.getString("fullname");
			profilestatus = Constants.EMPTY;//job.getString("profilestatus");
			payment = job.getString("pymttenure");
			paymentStatus = job.getString("pymtstatus");
			email = job.getString("emailid");
			Log.e("paymentStatus", paymentStatus);
			Log.e("usertype", strUT);

			ContentValues contentValues = new ContentValues();
			contentValues.put(DatabaseHandler.KEY_username, strUN);
			contentValues.put(DatabaseHandler.KEY_password, strPW);
			contentValues.put(DatabaseHandler.KEY_usertype, strUT);
			contentValues.put(DatabaseHandler.KEY_name, strName);
			contentValues.put(DatabaseHandler.KEY_id, strID);
			contentValues.put(DatabaseHandler.KEY_payment, payment);
			contentValues.put(DatabaseHandler.KEY_paymentStatus, paymentStatus);
			contentValues.put(DatabaseHandler.KEY_email, email);
			contentValues.put(DatabaseHandler.KEY_clientdealid, "0");
			contentValues.put(DatabaseHandler.KEY_selectedBeat, "");
			contentValues.put(DatabaseHandler.KEY_selectedDate, "");

			databaseHandler.addDetails(contentValues);
		}catch (Exception e){
			e.printStackTrace();
		}
	}

	public void commonProcess(JSONObject job) {

		try {
			Log.e("commonProcess", "....................................." + job.toString());
			JSONObject job1 = new JSONObject();

			strUT = job.getString("usertype");
			strID = job.getString("userid");
			strName = job.getString("fullname");
			profilestatus = job.getString("profilestatus");
			payment = job.getString("pymttenure");
			paymentStatus = job.getString("pymtstatus");
			email = job.getString("email");
			Log.e("paymentStatus", paymentStatus);
			Log.e("usertype", strUT);


			if (strUT.equals("S")) {
				job1 = job.getJSONObject("dealer").getJSONObject("D");
				clientdealerid = job1.getString("userid");
				dealmob = job1.getString("mobileno");
				dealfullname = job1.getString("fullname");
				dealercompname = job1.getString("companyname");
				dealaddr = job1.getString("address");

			} else {
				clientdealerid = "0";
			}
			ContentValues contentValues = new ContentValues();
			contentValues.put(DatabaseHandler.KEY_username, strUN);
			contentValues.put(DatabaseHandler.KEY_password, strPW);
			contentValues.put(DatabaseHandler.KEY_usertype, strUT);
			contentValues.put(DatabaseHandler.KEY_name, strName);
			contentValues.put(DatabaseHandler.KEY_id, strID);
			contentValues.put(DatabaseHandler.KEY_payment, payment);
			contentValues.put(DatabaseHandler.KEY_paymentStatus, paymentStatus);
			contentValues.put(DatabaseHandler.KEY_email, email);
			contentValues.put(DatabaseHandler.KEY_clientdealid, clientdealerid);
			contentValues.put(DatabaseHandler.KEY_selectedBeat, "");
			contentValues.put(DatabaseHandler.KEY_selectedDate, "");

			databaseHandler.addDetails(contentValues);

			Constants.syn = 0;

			Cursor cur;
			cur = databaseHandler.getDetails();
			cur.moveToFirst();

			Constants.USER_TYPE = cur.getString(cur
					.getColumnIndex(DatabaseHandler.KEY_usertype));
			Constants.USER_ID = cur.getString(cur
					.getColumnIndex(DatabaseHandler.KEY_id));
			Constants.PAYMENT = cur.getString(cur
					.getColumnIndex(DatabaseHandler.KEY_payment));

			Constants.USER_EMAIL = cur.getString(cur
					.getColumnIndex(DatabaseHandler.KEY_email));
			Constants.USER_FULLNAME = cur.getString(cur
					.getColumnIndex(DatabaseHandler.KEY_name));


			Log.e("Insertion Check", cur.getString(cur
					.getColumnIndex(DatabaseHandler.KEY_username)));
			Log.e("Insertion Check", cur.getString(cur
					.getColumnIndex(DatabaseHandler.KEY_payment)));
			Log.e("Insertion Check", cur.getString(cur
					.getColumnIndex(DatabaseHandler.KEY_password)));
			Log.e("Insertion Check", cur.getString(cur
					.getColumnIndex(DatabaseHandler.KEY_usertype)));
			Log.e("Insertion Check", cur.getString(cur
					.getColumnIndex(DatabaseHandler.KEY_clientdealid)));
			Log.e("paymentStatus", paymentStatus);

			Log.e("email", email);
			String firstLine = "UserName : " + Constants.USER_NAME +
					"\tUserID : " + Constants.USER_ID +
					"\tUserType : " + Constants.USER_TYPE +
					"\tKEY_username : " + cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_username)) +
					"\tKEY_password : " + cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_password)) +
					"\n";
			AppCrashConstants.USER_DETAILS = firstLine;

			AppCrashUtil.initialCrashCustomerInfo(AppCrashConstants.USER_DETAILS);

			closeDialog(); ////////////////////////////////////////////////////////CLOSE

			if (strName.equals("null") || profilestatus.equals("N")) {
				Log.e("", "inside");
				Intent intent = new Intent(SigninActivity.this, ProfileActivityInitial.class);
				startActivity(intent);
				overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
				finish();

			} else if (profilestatus.equals("Y") && paymentStatus.equals("Pending")) {
				if (Constants.USER_TYPE.equals("D")) {
					Intent intent = new Intent(SigninActivity.this, DealerPaymentActivity.class);

					startActivity(intent);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
					finish();
				} else if (Constants.USER_TYPE.equals("U")) { /////////new change 11-09-2019
					Intent intent = new Intent(SigninActivity.this, DealerPaymentActivity.class);

					startActivity(intent);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
					finish();
				} else if (Constants.USER_TYPE.equals("M")) {
					Intent intent = new Intent(SigninActivity.this, PaymentActivity.class);

					startActivity(intent);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
					finish();

				} else {
					//Kumaravel 10/01/2019
					if(Constants.USER_TYPE.equals("S")) {
						loadToadyOrder();
					}else {
						signingCommonProcess();
					}
				}
				/*Log.e("","inside");
				 */
			} else {

				if (Constants.USER_TYPE.equals("D")) {
					new PrefManager(SigninActivity.this).setStringDataByKey(KEY_MANAGER_TYPE, "D");
					///////Intent intent = new Intent(SigninActivity.this, DealersOrderActivity.class); //for new Requirement
					/////newly added lines
					Intent intent = new Intent(SigninActivity.this, ManagerActivity.class);
					///////////end
					startActivity(intent);
					finish();//////////added
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
					finish();
				} else if (Constants.USER_TYPE.equals("U")) { ////newly added for Hierarchy place
					////////////////////////////////////////// here not come any request we handle before

				} else if (Constants.USER_TYPE.equals("M")) {
					Intent intent = new Intent(SigninActivity.this, MerchantOrderActivity.class);

					startActivity(intent);
					overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
					finish();

				} else {
					//Kumaravel 10/01/2019
					if(Constants.USER_TYPE.equals("S")) {
						loadToadyOrder();
					}else {
						Log.e("USER_TYPE","................"+Constants.USER_TYPE);
						signingCommonProcess();
					}
				}
			}

		}catch (Exception e){
			e.printStackTrace();
		}
	}

	private void menuSetting(){
		setMenuFalse();
		showDialog("Menu Setting...");
		try {
			JSONStringer urlString = new JSONStringer().object().key("where")
					.object()
					.key("refkey").value("MOBILE_MENUS")
					.key("userid").value(Constants.DUSER_ID)
					.endObject()
					.endObject();
			DatabaseHandler databaseHandler = new DatabaseHandler(this);
			JsonServiceHandler jsonServiceHandler = new JsonServiceHandler(Utils.strMenuSetting, urlString.toString(), this);
			new MenuSetting.LoadMenuAsyncTask(databaseHandler,
					jsonServiceHandler, new MenuSetting.MenuSettingFetchingListener() {
				@Override
				public void complete() {
					Log.e(TAG, "...................Menu Setting Complete");
					/////////////////////////////////////
					///////////////////////////////////
					if(isTrackMenuFound()){
						Log.e(TAG, "Track menu found");
						///////////
						startAndStopTrackingService();/////////////// 09-08-2019
						///////////////
						getFCMToken();
					}else {
						Log.e(TAG, "Track menu not found");
						dismissDialog();
						signingFinalProcess();
					}


				}
			}).execute();
		}catch (Exception e){
			Log.e(TAG,"...............Menu..Setting..Exception Occurred");
			e.printStackTrace();
		}finally {
			Log.e(TAG,"...................Finally Called");
		}
	}

	private void getFCMToken(){

		PrefManager pre = new PrefManager(SigninActivity.this);

		pre.setBooleanDataByKey(IS_DELIVERED_FCM_TOKEN, false);
		pre.setStringDataByKey(FCM_TOKEN, "");


		boolean isDeliveredFCMToken = pre.getBooleanDataByKey(IS_DELIVERED_FCM_TOKEN);
		String token = pre.getStringDataByKey(FCM_TOKEN);
		if(!isDeliveredFCMToken){
			if(token.length() > 0){
				sendFCMToken();
			}else {
				//// Refresh token, get and send
				getToken();
			}
		}else {
			dismissDialog();
			signingFinalProcess();
		}
		//String fcmToken = pre.getStringDataByKey(FCM_TOKEN);
		//NotificationUtil.showMyNotification(getApplicationContext(),R.string.fcm_message, fcmToken + ":::" + isFCM, 9002);


	}

	private void getToken(){

		FirebaseInstanceId.getInstance().getInstanceId()
				.addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
					@Override
					public void onComplete(@NonNull Task<InstanceIdResult> task) {
						if (!task.isSuccessful()) {
							Log.w(TAG, "getInstanceId failed", task.getException());
							dismissDialog();
							signingFinalProcess();
							return;
						}

						// Get new Instance ID token
						String token = "";
						if(task.getResult() != null){
							token = task.getResult().getToken();
						}


						// Log and toast
						String msg = getString(R.string.msg_token_fmt, token);
						Log.d(TAG, msg);
						Toast.makeText(SigninActivity.this, msg, Toast.LENGTH_LONG).show();
						NotificationUtil.showMyNotification(getApplicationContext(),R.string.fcm_message, token, 9001);

						final PrefManager pre = new PrefManager(SigninActivity.this);
						pre.setStringDataByKey(FCM_TOKEN, token);

						sendFCMToken();
					}
				});
	}

	private void sendFCMToken(){
		final PrefManager pre = new PrefManager(SigninActivity.this);
		final String token = pre.getStringDataByKey(FCM_TOKEN);
		APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
		String userId;
		Cursor cur;
		cur = databaseHandler.getDetails();
		if(cur != null && cur.getCount() > 0) {
			cur.moveToFirst();
			userId = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_id));
			cur.close();
		}else {
			return;
		}

		if (apiInterface != null) {
			final Call<CommonResponseModel> call = apiInterface.sendFCMToken(strFCMSendURL+userId, token);
			call.enqueue(new Callback<CommonResponseModel>() {
				@Override
				public void onResponse(Call<CommonResponseModel> call, Response<CommonResponseModel> response) {
					dismissDialog();
					signingFinalProcess();

					if(response.isSuccessful()) {
						CommonResponseModel res = response.body();
						if(res != null) {
							if(res.isStatus()) {
								pre.setBooleanDataByKey(PrefManager.IS_DELIVERED_FCM_TOKEN, true);
								new com.freshorders.freshorder.utils.Log(SigninActivity.this).ee(TAG,"Success..." + token);
							}
						}
					}else {
						Log.e(TAG,"response failed.........FCM Token Submitting");
						pre.setBooleanDataByKey(PrefManager.IS_DELIVERED_FCM_TOKEN, false);
						new com.freshorders.freshorder.utils.Log(SigninActivity.this).ee(TAG,"Failed..." + token);
					}
				}

				@Override
				public void onFailure(Call<CommonResponseModel> call, Throwable t) {
					pre.setBooleanDataByKey(PrefManager.IS_DELIVERED_FCM_TOKEN, false);
					dismissDialog();
					signingFinalProcess();
				}
			});
		}
	}

	private boolean isTrackMenuFound(){
		DatabaseHandler db = new DatabaseHandler(SigninActivity.this);
		Cursor menuCur = db.getMenuItems();
		if(menuCur != null && menuCur.getCount() > 0){
			menuCur.moveToFirst();
			for(int i = 0; i < menuCur.getCount(); i++) {
				int currentVisibility = menuCur.getInt(menuCur.getColumnIndex(DatabaseHandler.KEY_MENU_IS_VISIBLE));
				String currentMenuCode = menuCur.getString(menuCur.getColumnIndex(DatabaseHandler.KEY_MENU_CODE));
				if (MENU_DISTANCE_CALCULATION_CODE.equalsIgnoreCase(currentMenuCode)) {
					if (currentVisibility == 1) {
						return true;
					}
				}
				menuCur.moveToNext();
			}
		}
		return false;
	}

	private void startAndStopTrackingService(){


		final long delay;
		final long delayStop;

		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		calendar.set(Calendar.HOUR_OF_DAY, SELF_START_TIME_LIVE_TRACK);

		AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

		Intent intent = new Intent(this, StartTrackingBroadCast.class);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(this.getApplicationContext(), BR_REQUEST_CODE_START_LIVE_TRACK, intent,
				PendingIntent.FLAG_UPDATE_CURRENT);

		delay = AlarmManager.INTERVAL_DAY;
		/*
		if (DateTime.now().getHourOfDay() < SELF_START_TIME_LIVE_TRACK) {
			delay = new Duration(DateTime.now() , DateTime.now().withTimeAtStartOfDay().plusHours(SELF_START_TIME_LIVE_TRACK)).getStandardMinutes();
		} else {
			delay = new Duration(DateTime.now() , DateTime.now().withTimeAtStartOfDay().plusDays(1).plusHours(SELF_START_TIME_LIVE_TRACK)).getStandardMinutes();
		} */

		alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), delay, pendingIntent);  //AlarmManager.INTERVAL_FIFTEEN_MINUTES

		/////////////////////////////////////////////////////
		Intent intentStop = new Intent(this, StopTrackingBroadCast.class);
		PendingIntent piStop = PendingIntent.getBroadcast(this.getApplicationContext(), BR_REQUEST_CODE_STOP_LIVE_TRACK, intentStop,
				PendingIntent.FLAG_UPDATE_CURRENT);
		delayStop = AlarmManager.INTERVAL_DAY;
		/*
		if (DateTime.now().getHourOfDay() < SELF_START_TIME_LIVE_TRACK) {
			delayStop = new Duration(DateTime.now() , DateTime.now().withTimeAtStartOfDay().plusHours(SELF_START_TIME_LIVE_TRACK)).getStandardMinutes();
		} else {
			delayStop = new Duration(DateTime.now() , DateTime.now().withTimeAtStartOfDay().plusDays(1).plusHours(SELF_START_TIME_LIVE_TRACK)).getStandardMinutes();
		} */

		AlarmManager alarmManager1 = (AlarmManager) getSystemService(ALARM_SERVICE);
		Calendar calendar1 = Calendar.getInstance();
		calendar1.setTimeInMillis(System.currentTimeMillis());
		calendar1.set(Calendar.HOUR_OF_DAY, SELF_STOP_TIME_LIVE_TRACK);


		alarmManager1.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar1.getTimeInMillis(), delayStop, piStop);  //AlarmManager.INTERVAL_FIFTEEN_MINUTES

		//startTrackingOnAppStart();

	}

	private void signingFinalProcess(){
		SalesmanDataLink();
		merchantCount();

		Constants.jsonArCustomFieldsMerchant = null;
		Intent io = new Intent(SigninActivity.this,
				SalesManOrderActivity.class);
		startActivity(io);
		finish();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
		finish();
	}

	private void signingCommonProcess(){
        loadHierarchyPlaceFile();
		//menuSetting();
	}

	private void moveToMangerActivity(){
		Intent intent = new Intent(SigninActivity.this, ManagerActivity.class);
		startActivity(intent);
		finish();
		overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
		finish();
	}

	private void loadHierarchyPlaceFile_Type_U(JSONObject obj, String responseString){
		//showDialog(getResources().getString(R.string.data_loading));
		////////////////////////////// Delete previous file
		try{
			String path = new PrefManager(this).getStringDataByKey(KEY_PATH_HIERARCHY_PLACE_FILE);
			ExternalStorageUtil.deleteHierarchyPlaceFile(path, SigninActivity.this);
			new PrefManager(SigninActivity.this).setBooleanDataByKey(KEY_IS_HIERARCHY_PLACE_FILE_LOADED, false);
		}catch (Exception e){
			///dismissDialog();
			e.printStackTrace();
			showD(getResources().getString(R.string.technical_fault) + e.getMessage());
			return;
		}
		if(responseString == null || responseString.isEmpty()){
			showD(getResources().getString(R.string.technical_fault) + "Response not received from server");
			return;
		}
		//////////////////////////////
		try {
			JSONArray hierarchyObj = obj.getJSONArray("hierarchyobj");
			if(hierarchyObj != null && hierarchyObj.length() > 0){
				String filePath = ExternalStorageUtil.createOrGetHierarchyFolderFile(SigninActivity.this);
				if (!filePath.isEmpty()) {
					////Gson gson = new Gson();
					///String result = gson.toJson(obj);
					if (createHierarchyFile(filePath, responseString)) {
						new PrefManager(SigninActivity.this).setBooleanDataByKey(KEY_IS_HIERARCHY_PLACE_FILE_LOADED, true);
						Log.e(TAG, ".....Hierarchy....Loaded...success...................");

						moveToMangerActivity();

					}else {
						showD(getResources().getString(R.string.not_able_to_load_place_data));
					}
				}else{
					showD(getResources().getString(R.string.not_able_to_create_place_file) + "File Path Empty");
				}
			}else {
				showD(getResources().getString(R.string.this_login_not_contain_place));
			}
		}catch (Exception e){
			e.printStackTrace();
			showD(getResources().getString(R.string.technical_fault) + e.getMessage());
		}
		/*
		APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
		if(apiInterface != null) {
			JSONStringer jsonStringer = new JSONStringer();
			try {
				jsonStringer.object()
						.key("include").array().value("children").endArray()
						.key("where")
						.object()
						.key("duserid").value(Constants.DUSER_ID)
						.key("parentlevelid").value(null)
						.endObject()
						.endObject();

				String url = strPlaceHierarchy + jsonStringer;
				final Call<ResponseBody> call = apiInterface.getPlaceHierarchy(url);

				call.enqueue(new Callback<ResponseBody>() {
					@Override
					public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
						dismissDialog();
						if(response.isSuccessful()) {
							//PlaceHierarchyResModel obj = response.body();
							try {
								String result = response.body().string();
								JSONObject obj = new JSONObject(result);
								String status = obj.getString("status");
								Log.e(TAG, "........Status........" + status);
								if (status != null && status.equalsIgnoreCase("true")) {
									JSONArray data = obj.getJSONArray("data");
									if (data != null && data.length() > 0) {
										//Gson gson = new Gson();
										//String treeObj = gson.toJson(data);
										String filePath = ExternalStorageUtil.createOrGetHierarchyFolderFile(SigninActivity.this);
										if (!filePath.isEmpty()) {
											if (createHierarchyFile(filePath, result)) {
												new PrefManager(SigninActivity.this).setBooleanDataByKey(KEY_IS_HIERARCHY_PLACE_FILE_LOADED, true);
												Log.e(TAG, ".....Hierarchy....Loaded...success...................");

												moveToMangerActivity();

											}else {
												showD(getResources().getString(R.string.not_able_to_load_place_data));
											}
										}else{
											showD(getResources().getString(R.string.not_able_to_create_place_file) + "File Path Empty");
										}
									}else {
										showD(getResources().getString(R.string.no_response_from_server) + "--Place data not found--");
									}
								} else {
									Log.e(TAG, "........Status.......Failed.");
									showD(getResources().getString(R.string.no_response_from_server) + "--Status False--");

								}
							}catch (Exception e){
								showD(getResources().getString(R.string.technical_fault) + e.getMessage());
															}
						}else{
							Log.e(TAG, "........response.......Failed.");
							showD(getResources().getString(R.string.no_response_from_server) + "--ResponseBody--");
						}
					}

					@Override
					public void onFailure(Call<ResponseBody> call, Throwable t) {
						dismissDialog();
						Log.e(TAG,"........response.......onFailure." + t.getMessage() );
						showD(getResources().getString(R.string.technical_fault) +  t.getMessage());
					}
				});
			}catch (Exception e){
				dismissDialog();
				e.printStackTrace();
				showD(getResources().getString(R.string.technical_fault) + e.getMessage());
			}
		}else {
			dismissDialog();
			showD(getResources().getString(R.string.technical_fault)+"---APIInterface not created");
		}  */  /// no need now ...
	}

	private void loadHierarchyPlaceFile(){

		////////////////////////////// Delete previous file
		try{
			String path = new PrefManager(this).getStringDataByKey(KEY_PATH_HIERARCHY_PLACE_FILE);
			ExternalStorageUtil.deleteHierarchyPlaceFile(path, SigninActivity.this);
			new PrefManager(SigninActivity.this).setBooleanDataByKey(KEY_IS_HIERARCHY_PLACE_FILE_LOADED, false);
		}catch (Exception e){
			e.printStackTrace();
		}
		//////////////////////////////
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        if(apiInterface != null) {
            JSONStringer jsonStringer = new JSONStringer();
            try {
                jsonStringer.object()
                        .key("include").array().value("children").endArray()
                        .key("where")
                            .object()
                                .key("duserid").value(Constants.DUSER_ID)
                                .key("parentlevelid").value(null)
                            .endObject()
                        .endObject();

                String url = strPlaceHierarchy + jsonStringer;
                final Call<ResponseBody> call = apiInterface.getPlaceHierarchy(url);

                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if(response.isSuccessful()) {
							//PlaceHierarchyResModel obj = response.body();
							try {
								String result = response.body().string();
								JSONObject obj = new JSONObject(result);
								String status = obj.getString("status");
								Log.e(TAG, "........Status........" + status);
								if (status != null && status.equalsIgnoreCase("true")) {
									JSONArray data = obj.getJSONArray("data");
									if (data != null && data.length() > 0) {
										//Gson gson = new Gson();
										//String treeObj = gson.toJson(data);
										String filePath = ExternalStorageUtil.createOrGetHierarchyFolderFile(SigninActivity.this);
										if (!filePath.isEmpty()) {
											if (createHierarchyFile(filePath, result)) {
												new PrefManager(SigninActivity.this).setBooleanDataByKey(KEY_IS_HIERARCHY_PLACE_FILE_LOADED, true);
												Log.e(TAG, ".....Hierarchy....Loaded...success...................");

											}
										}
										menuSetting();
									}
								} else {
									Log.e(TAG, "........Status.......Failed.");
									menuSetting();

								}
							}catch (Exception e){
								showD(getResources().getString(R.string.technical_fault) + e.getMessage());
								menuSetting();
							}
						}else{
							Log.e(TAG, "........response.......Failed.");
							menuSetting();
						}

                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
						Log.e(TAG,"........response.......onFailure." + t.getMessage() );
                        menuSetting();
                    }
                });
            }catch (Exception e){
                e.printStackTrace();
                showD(getResources().getString(R.string.technical_fault) + e.getMessage());
            }
        }
    }

	private boolean createHierarchyFile(String path, String jsonString){

            String FILENAME = PLACE_HIERARCHY_FILE_JSON;
            try {
                BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(path));
                bufferedWriter.write(jsonString);
                bufferedWriter.close();
                return true;
            } catch (FileNotFoundException fileNotFound) {
                return false;
            } catch (IOException ioException) {
                return false;
            }
    }

	private void loadToadyOrder(){

		JSONObject jsonObject = new JSONObject();

		try {
			Log.e("TodayOrder","..........................Constants.USER_ID"+Constants.USER_ID);
			jsonObject.put("muserid",Constants.USER_ID);  ///Constants.USER_ID  not working
			jsonObject.put("usertype","S");
			JsonServiceHandler = new JsonServiceHandler(this);
			showDialog(Constants.TODAY_ORDER_LOADING);
			new LoadOnlineOrder.LoadTodayOrderAsyncTask(databaseHandler, JsonServiceHandler, new LoadOnlineOrder.TodayOrderListener() {
				@Override
				public void onLoadFinished() {
					dismissDialog();
					signingCommonProcess();
				}

				@Override
				public void onLoadFailed(String msg) {
					dismissDialog();
					Toast.makeText(SigninActivity.this,/*"Online order loading failed"*/""+msg, Toast.LENGTH_LONG).show();
					signingCommonProcess();
				}
			}).execute();

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private void setMenuFalse(){
		MyApplication app = MyApplication.getInstance();

		app.setProfile(false);
		app.setMyDayPlan(false);
		app.setMyOrders(false);
		app.setAddMerchant(false);
		app.setCreateOrder(false);

		app.setMySales(false);
		app.setPostNotes(false);
		app.setMyClientVisit(false);
		app.setAcknowledge(false);
		app.setPaymentCollection(false);

		app.setPkdDataCapture(false);
		app.setDistributorStock(false);
		app.setSurveyUser(false);
		app.setDownLoadScheme(false);
		app.setDistanceCalculation(false);

		app.setRefresh(false);
		app.setLogout(false);
		app.setClosingStock(false);
		app.setClosingStockAudit(false);
		app.setPendingData(false);

	}


	private ProgressDialog dialog;

	public void showDialog(String  msg) {
		if(dialog == null){
			dialog = ProgressDialog.show(this, "",
					msg, true, false);
		}
	}

	public void dismissDialog() {
		if (dialog != null && dialog.isShowing()) {
			dialog.dismiss();
		}
	}

}
