package com.freshorders.freshorder.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Pavithra on 05-01-2017.
 */
public class SchemeDownload extends Activity {
    public static Context context;
    public static JSONObject JsonAccountObject = null;
    public static JSONArray JsonAccountArray = null;
    public static DatabaseHandler databaseHandler1;
    private static SQLiteDatabase db;
    public static String SchemeCount="",selected_dealer="";
    public static com.freshorders.freshorder.utils.JsonServiceHandler JsonServiceHandler ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.refresh_activity);
        netCheck();
        context = SchemeDownload.this;
        databaseHandler1 = new DatabaseHandler(getApplicationContext());
        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();
        Cursor cursor;
        cursor = databaseHandler1.getdealer();
        Log.e("count", String.valueOf(cursor.getCount()));
                if(cursor.getCount()>0){
                    cursor.moveToFirst();
                    selected_dealer = cursor.getString(cursor
                            .getColumnIndex(DatabaseHandler.KEY_Duserid));
                }
        databaseHandler1.deletescheme();
        offlineScheme(selected_dealer);
    }

    public   void offlineScheme(final String selected_dealer) {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            String strStatus = "",strMsg="";

            ProgressDialog dialog;
            @Override
            protected void onPreExecute() {
                dialog= ProgressDialog.show(context, "",
                        "Initializing Scheme table ...", true, true);
                dialog.setCancelable(false);
            }


            @Override
            protected void onPostExecute(Void result) {

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {


                        JSONArray job1 = JsonAccountObject.getJSONArray("data");


                        for (int i = 0; i < job1.length(); i++) {

                            JSONObject job = new JSONObject();
                            job = job1.getJSONObject(i);
                            String schmeduserid=job.getString("duserid");
                            String schemeprodid = job.getString("prodid");
                            String schemestartdate =job.getString("start_date");
                            String schemeenddate=job.getString("end_date");
                            String schemetype=job.getString("schemetype");
                            String schemediscount=job.getString("discount");
                            String schemefreeProdid=job.getString("free_prodid");
                            String schemefreeQty=job.getString("free_qty");
                            String schemeremark=job.getString("remarks");
                            String schemestatus=job.getString("status");

                            databaseHandler1.addSchemes(schemeprodid, schmeduserid, schemestartdate, schemeenddate, schemediscount,schemetype,
                                    schemefreeProdid,schemefreeQty,schemestatus,schemeremark);


                        }
                        Cursor cur;
                        cur = databaseHandler1.getSchemeCount();
                        Log.e("dealercount", String.valueOf(cur.getCount()));
                        SchemeCount = String.valueOf(cur.getCount());

                        dialog.dismiss();



                    }else{
                        dialog.dismiss();

                    }
                    if(SchemeCount.equals("null")||SchemeCount.isEmpty()){
                        SchemeCount="0";
                    }

                    showAlertDialogToast("Scheme Count :" + SchemeCount );


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {

                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();

                try {
                    jsonObject.put("duserid",selected_dealer);


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                JsonServiceHandler = new JsonServiceHandler(Utils.strgetScheme,jsonObject.toString(),context );
                JsonAccountObject = JsonServiceHandler.ServiceData();
                return null;
            }
        }.execute(new Void[]{});

    }
    public  void showAlertDialogToast( String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        if (Constants.downloadScheme.equals("createorder")) {
                            Intent to = new Intent(context,
                                    CreateOrderActivity.class);
                            context.startActivity(to);
                            finish();
                        } else if (Constants.downloadScheme.equals("mydealer")) {
                            Intent io = new Intent(context,
                                    MyDealersActivity.class);
                            io.putExtra("Key", "menuclick");
                            startActivity(io);
                            finish();
                        } else if (Constants.downloadScheme.equals("postnotes")) {
                            Intent io = new Intent(context,
                                    MerchantComplaintActivity.class);
                            startActivity(io);
                            finish();
                        } else if (Constants.downloadScheme.equals("clientvisit")) {
                            Intent to = new Intent(context,
                                    SMClientVisitHistory.class);
                            startActivity(to);
                            finish();
                        } else if (Constants.downloadScheme.equals("paymentcollection")) {
                            Intent to = new Intent(context,
                                    SalesmanPaymentCollectionActivity.class);
                            startActivity(to);
                            finish();
                        } else if (Constants.downloadScheme.equals("profile")) {
                            Intent io = new Intent(context,
                                    ProfileActivity.class);
                            startActivity(io);
                            finish();

                        } else if (Constants.downloadScheme.equals("myorder")) {
                            Intent io = new Intent(context,
                                    SalesManOrderActivity.class);
                            startActivity(io);
                            finish();

                        } else if (Constants.downloadScheme.equals("addmerchant")) {
                            Intent io = new Intent(context,
                                    AddMerchantNew.class);
                            startActivity(io);
                            finish();

                        } else if (Constants.downloadScheme.equals("stockentry")) {
                            Intent io = new Intent(context,
                                    OutletStockEntry.class);
                            startActivity(io);
                            finish();

                        } else if (Constants.downloadScheme.equals("acknowledge")) {
                            Intent io = new Intent(context,
                                    SalesmanAcknowledgeActivity.class);
                            startActivity(io);
                            finish();

                        }
                        else if (Constants.downloadScheme.equals("surveyuser")) {
                            Intent io = new Intent(context,
                                    SurveyMerchant.class);
                            startActivity(io);
                            finish();

                        }
                        else if (Constants.downloadScheme.equals("mydayplan")) {
                            Intent io = new Intent(context,
                                    MyDayPlan.class);
                            startActivity(io);
                            finish();

                        }
                        else if (Constants.downloadScheme.equals("mysales")) {
                            Intent io = new Intent(context,
                                    MySalesActivity.class);
                            startActivity(io);
                            finish();

                        }
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }


    public void onBackPressed() {
        exitAlret();
    }

    private void exitAlret() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        localBuilder.setMessage("Do you want to Exit?");
        localBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {

                        finish();

                    }
                });
        localBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {
                        paramDialogInterface.cancel();
                    }
                });
        localBuilder.create().show();
    }
    public boolean netCheck() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            }

            else if (result == null) {
               /* showAlertDialog(SalesManOrderActivity.this,
                        "No Internet Connection",
                        "Please Check Your internet connection.", false);*/
                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

}
