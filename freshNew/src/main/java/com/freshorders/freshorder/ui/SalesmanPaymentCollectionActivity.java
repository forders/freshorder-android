package com.freshorders.freshorder.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.activity.DistanceCalActivity;
import com.freshorders.freshorder.adapter.SalesmanPaymentCollectionAdapter;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.CollectionDomain;
import com.freshorders.freshorder.domain.MDealerCompDropdownDomain;
import com.freshorders.freshorder.model.PaymentCollectionModel;
import com.freshorders.freshorder.model.PaymentCollectionSend;
import com.freshorders.freshorder.popup.PendingSurveyPopup;
import com.freshorders.freshorder.popup.PopupSurveyPayment;
import com.freshorders.freshorder.toonline.migration.PendingDataMigrationActivity;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class SalesmanPaymentCollectionActivity extends Activity {

    TextView menuIcon, textViewAssetMenuProfile, textViewAssetMenuMyOrders,
            textViewAssetMenuMyDealers, textViewAssetMenuProducts,
            textViewAssetMenuPayment, textViewAssetMenuComplaint,
            textViewAssetMenuSignout, textViewAssetMenuCreateOrder,textViewAssetMenuDistanceCalculation,
            textViewAssetMenuClientVisit, textViewAssetMenuPaymentCollection, textViewAssetMenuRefresh,
            textViewAssetMenuStockEntry, textViewAssetMenuAddMerchant, textViewAssetMenuAcknowledge, textViewAssetMenusurveyuser, textViewAssetMenudownloadscheme, textViewAssetMenuMyDayPlan, textViewAssetMenuMySales,
            textViewAssetMenuDistStock, textViewAssetMenuStockOnly, textViewAssetMenuMigration, textViewAssetMenuStockAudit;

    int menuCliccked;

    LinearLayout linearLayoutMenuParent, linearLayoutProfile, linearLayoutMyOrders,
            linearLayoutMyDealers, linearLayoutProducts, linearLayoutPayment,
            linearLayoutComplaint, linearLayoutSignout, linearLayoutEdit, linearLayoutDetails,linearLayoutDistanceCalculation,
            linearLayoutCreateOrder, linearLayoutClientVisit, linearLayoutPaymentCollection,
            linearlayoutoverallayout, linearLayoutRefresh,
            linearLayoutAcknowledge, linearLayoutStockEntry, linearLayoutAddMerchant, linearLayoutsurveyuser, linearLayoutdownloadscheme, linearLayoutMyDayPlan, linearLayoutMySales,
            linearLayoutDistStock, linearLayoutStockOnly, linearLayoutMigration, linearLayoutStockAudit;

    private void showMenu(){

        MyApplication app = MyApplication.getInstance();

        if(!app.isCreateOrder()){
            linearLayoutCreateOrder.setVisibility(View.GONE);
        }
        if(!app.isProfile()){
            linearLayoutProfile.setVisibility(View.GONE);
        }
        if(!app.isMyDayPlan()){
            linearLayoutMyDayPlan.setVisibility(View.GONE);
        }
        if(!app.isMyOrders()){
            linearLayoutMyOrders.setVisibility(View.GONE);
        }
        if(!app.isAddMerchant()){
            linearLayoutAddMerchant.setVisibility(View.GONE);
        }
        if(!app.isMySales()){
            linearLayoutMySales.setVisibility(View.GONE);
        }
        if(!app.isPostNotes()){
            linearLayoutComplaint.setVisibility(View.GONE);
        }
        if(!app.isMyClientVisit()){
            linearLayoutClientVisit.setVisibility(View.GONE);
        }
        if(!app.isAcknowledge()){
            linearLayoutAcknowledge.setVisibility(View.GONE);
        }
        if(!app.isPaymentCollection()){
            linearLayoutPaymentCollection.setVisibility(View.GONE);
        }
        if(!app.isPkdDataCapture()){
            linearLayoutStockEntry.setVisibility(View.GONE);
        }
        if(!app.isDistributorStock()){
            linearLayoutDistStock.setVisibility(View.GONE);
        }
        if(!app.isSurveyUser()){
            linearLayoutsurveyuser.setVisibility(View.GONE);
        }
        if(!app.isDownLoadScheme()){
            linearLayoutdownloadscheme.setVisibility(View.GONE);
        }
        if(!app.isDistanceCalculation()){
            linearLayoutDistanceCalculation.setVisibility(View.GONE);
        }
        if(!app.isRefresh()){
            linearLayoutRefresh.setVisibility(View.GONE);
        }
        if(!app.isLogout()){
            linearLayoutSignout.setVisibility(View.GONE);
        }
        if(!app.isClosingStock()){
            linearLayoutStockOnly.setVisibility(View.GONE);
        }
        if(!app.isClosingStockAudit()){
            linearLayoutStockAudit.setVisibility(View.GONE);
        }
        if(!app.isPendingData()){
            linearLayoutMigration.setVisibility(View.GONE);
        }
    }

    public Dialog qtyDialog;
    AutoCompleteTextView autocompletemrchantlist;
    TextView textViewcross_1, balamt, textviewnodata, textViewshowbal, textviewamt, textviewbalanceamt;
    ListView listView;
    Button buttonpay;
    public static SalesmanPaymentCollectionAdapter adapter;
    ArrayList<CollectionDomain> arraylistpaymentcollection;
    JSONObject JsonAccountObject = null;
    JSONArray JsonAccountArray = null;
    DatabaseHandler databaseHandler;
    JsonServiceHandler JsonServiceHandler;
    ArrayList<MDealerCompDropdownDomain> arrayListDealerDomain, arrayListSearch, arrayListmerchant;
    ArrayList<String> arrayListDealer;
    public static int smpymntmerchantselected = 0;
    String muserId = "", merchantmobileno = "";
    String time;
    Date dateStr = null;
    Double amount = 0.0;
    Double balamount = 0.0;
    public static String PaymentStatus = "NULL", moveto = "NULL", dealername;
    String bill_no;
    public static Integer[] billno;
    String dayOfTheWeek = "", beatMerchants, userBeatModel = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub

        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.salesman_paymentcollection_activity);

        databaseHandler = new DatabaseHandler(getApplicationContext());
        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();
        textviewamt = (TextView) findViewById(R.id.textviewamt);

        Cursor curPay;
        curPay = databaseHandler.getDetails();
        if(curPay != null && curPay.getCount() > 0){
            curPay.moveToFirst();
            PaymentStatus = curPay.getString(curPay.getColumnIndex(DatabaseHandler.KEY_paymentStatus));
            curPay.close();
        }

        textviewbalanceamt = (TextView) findViewById(R.id.textviewbalanceamt);
        autocompletemrchantlist = (AutoCompleteTextView) findViewById(R.id.autocompletemrchantlist);
        balamt = (TextView) findViewById(R.id.balamt);
        textViewcross_1 = (TextView) findViewById(R.id.textViewcross_1);
        textViewshowbal = (TextView) findViewById(R.id.textViewshowbal);
        listView = (ListView) findViewById(R.id.listView);
        buttonpay = (Button) findViewById(R.id.buttonpay);
        textviewnodata = (TextView) findViewById(R.id.textViewnodata);
        arrayListDealer = new ArrayList<String>();
        arrayListDealerDomain = new ArrayList<MDealerCompDropdownDomain>();
        arrayListmerchant = new ArrayList<MDealerCompDropdownDomain>();
        arrayListSearch = new ArrayList<MDealerCompDropdownDomain>();

        linearlayoutoverallayout = (LinearLayout) findViewById(R.id.linearlayoutoverallayout);
        linearLayoutCreateOrder = (LinearLayout) findViewById(R.id.linearLayoutCreateOrder);
        linearLayoutProfile = (LinearLayout) findViewById(R.id.linearLayoutProfile);
        linearLayoutMyOrders = (LinearLayout) findViewById(R.id.linearLayoutMyOrders);
        linearLayoutMyDealers = (LinearLayout) findViewById(R.id.linearLayoutMyDealers);
        linearLayoutRefresh = (LinearLayout) findViewById(R.id.linearLayoutRefresh);
        textViewAssetMenuRefresh = (TextView) findViewById(R.id.textViewAssetMenuRefresh);
        linearLayoutPayment = (LinearLayout) findViewById(R.id.linearLayoutPayment);
        linearLayoutComplaint = (LinearLayout) findViewById(R.id.linearLayoutComplaint);
        linearLayoutSignout = (LinearLayout) findViewById(R.id.linearLayoutSignout);
        linearLayoutDistanceCalculation = (LinearLayout) findViewById(R.id.linearLayoutDistanceCalculation);
        linearLayoutClientVisit = (LinearLayout) findViewById(R.id.linearLayoutClientVisit);
        linearLayoutPaymentCollection = (LinearLayout) findViewById(R.id.linearLayoutPaymentCollection);

        linearLayoutAcknowledge = (LinearLayout) findViewById(R.id.linearLayoutAcknowledge);
        linearLayoutStockEntry = (LinearLayout) findViewById(R.id.linearLayoutStockEntry);
        linearLayoutAddMerchant = (LinearLayout) findViewById(R.id.linearLayoutAddMerchant);

        linearLayoutdownloadscheme = (LinearLayout) findViewById(R.id.linearLayoutdownloadscheme);
        linearLayoutsurveyuser = (LinearLayout) findViewById(R.id.linearLayoutsurveyuser);
        linearLayoutMyDayPlan = (LinearLayout) findViewById(R.id.linearLayoutMyDayPlan);
        linearLayoutMySales = (LinearLayout) findViewById(R.id.linearLayoutMySales);

        linearLayoutStockOnly = (LinearLayout) findViewById(R.id.linearLayoutStockOnly);
        linearLayoutStockAudit = findViewById(R.id.linearLayoutStockAudit);
        linearLayoutMigration = (LinearLayout) findViewById(R.id.linearLayoutMigration);

        menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
        textViewAssetMenuCreateOrder = (TextView) findViewById(R.id.textViewAssetMenuCreateOrder);
        textViewAssetMenuProfile = (TextView) findViewById(R.id.textViewAssetMenuProfile);
        textViewAssetMenuMyOrders = (TextView) findViewById(R.id.textViewAssetMenuMyOrders);
        textViewAssetMenuMyDealers = (TextView) findViewById(R.id.textViewAssetMenuMyDealers);

        textViewAssetMenuPayment = (TextView) findViewById(R.id.textViewAssetMenuPayment);
        textViewAssetMenuComplaint = (TextView) findViewById(R.id.textViewAssetMenuComplaint);
        textViewAssetMenuSignout = (TextView) findViewById(R.id.textViewAssetMenuSignout);
        textViewAssetMenuDistanceCalculation= (TextView) findViewById(R.id.textViewAssetMenuDistanceCalculation);
        textViewAssetMenuClientVisit = (TextView) findViewById(R.id.textViewAssetMenuClientVisit);
        textViewAssetMenuPaymentCollection = (TextView) findViewById(R.id.textViewAssetMenuPaymentCollection);

        textViewAssetMenuAddMerchant = (TextView) findViewById(R.id.textViewAssetMenuAddMerchant);
        textViewAssetMenuAcknowledge = (TextView) findViewById(R.id.textViewAssetMenuAcknowledge);
        textViewAssetMenuStockEntry = (TextView) findViewById(R.id.textViewAssetMenuStockEntry);

        textViewAssetMenudownloadscheme = (TextView) findViewById(R.id.textViewAssetMenudownloadscheme);
        textViewAssetMenusurveyuser = (TextView) findViewById(R.id.textViewAssetMenusurveyuser);
        textViewAssetMenuMyDayPlan = (TextView) findViewById(R.id.textViewAssetMenuMyDayPlan);
        textViewAssetMenuMySales = (TextView) findViewById(R.id.textViewAssetMenuMySales);

        linearLayoutMenuParent = (LinearLayout) findViewById(R.id.linearLayoutMenuParent);

        textviewamt.setText("Amt" + "\n" + "(Rs)");
        textviewbalanceamt.setText("Bal Amt" + "\n" + "(Rs)");
        Typeface font = Typeface.createFromAsset(getAssets(),
                "fontawesome-webfont.ttf");

        textViewcross_1.setTypeface(font);
        menuIcon.setTypeface(font);
        textViewAssetMenuCreateOrder.setTypeface(font);
        textViewAssetMenuProfile.setTypeface(font);
        textViewAssetMenuMyOrders.setTypeface(font);
        textViewAssetMenuMyDealers.setTypeface(font);
        textViewAssetMenuRefresh.setTypeface(font);
        textViewAssetMenuPayment.setTypeface(font);
        textViewAssetMenuComplaint.setTypeface(font);
        textViewAssetMenuSignout.setTypeface(font);
        textViewAssetMenuDistanceCalculation.setTypeface(font);
        textViewAssetMenuClientVisit.setTypeface(font);
        textViewAssetMenuPaymentCollection.setTypeface(font);

        textViewAssetMenuAcknowledge.setTypeface(font);
        textViewAssetMenuStockEntry.setTypeface(font);
        textViewAssetMenuAddMerchant.setTypeface(font);

        textViewAssetMenusurveyuser.setTypeface(font);
        textViewAssetMenudownloadscheme.setTypeface(font);
        textViewAssetMenuMyDayPlan.setTypeface(font);
        textViewAssetMenuMySales.setTypeface(font);

        linearLayoutDistStock = (LinearLayout) findViewById(R.id.linearLayoutDistStock);
        textViewAssetMenuDistStock = (TextView) findViewById(R.id.textViewAssetMenuDistStock);
        textViewAssetMenuDistStock.setTypeface(font);

        textViewAssetMenuStockOnly = (TextView) findViewById(R.id.textViewAssetMenuStockOnly);
        textViewAssetMenuStockAudit = (TextView) findViewById(R.id.textViewAssetMenuStockAudit);
        textViewAssetMenuMigration = findViewById(R.id.textViewAssetMenuMigration);

        textViewAssetMenuStockOnly.setTypeface(font);
        textViewAssetMenuStockAudit.setTypeface(font);
        textViewAssetMenuMigration.setTypeface(font);

        linearLayoutMyDealers.setVisibility(View.GONE);

        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date d = new Date();
        dayOfTheWeek = sdf.format(d);
        Log.e("dateday", dayOfTheWeek);

        Cursor cur1;
        cur1 = databaseHandler.getUserSetting();
        cur1.moveToFirst();
        userBeatModel = cur1.getString(cur1.getColumnIndex(DatabaseHandler.KEY_settingrefvalue));
        Log.e("userBeatModel", userBeatModel);

        if (userBeatModel.equalsIgnoreCase("C")) {
            Log.e("userModel C", "userModel C");
            Cursor curr;
            curr = databaseHandler.getDetails();
            curr.moveToFirst();
            String selectedbeat = curr.getString(curr.getColumnIndex(DatabaseHandler.KEY_selectedBeat));
            String selectedDate = curr.getString(curr.getColumnIndex(DatabaseHandler.KEY_selectedDate));
            String todayDate = new SimpleDateFormat(
                    "yyyy-MM-dd").format(new java.util.Date());
            if (todayDate.equals(selectedDate) && !selectedbeat.equals("")) {
                getBeatMerchant(selectedbeat);

            } else {
                Constants.beatassignscreen = "PaymentCollection";
                Intent to = new Intent(SalesmanPaymentCollectionActivity.this, CompanyBeatActivity.class);
                startActivity(to);
                finish();
            }


        } else {
            Log.e("userModel D", "userModel D");
            offlineBeat();
        }


        if (muserId.equals("")) {
            textviewnodata.setVisibility(View.VISIBLE);
            textviewnodata.setText("Please choose store");
            listView.setVisibility(View.GONE);
            buttonpay.setVisibility(View.GONE);

        }


        autocompletemrchantlist.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                autocompletemrchantlist.showDropDown();

                textViewcross_1.setVisibility(View.VISIBLE);

                textViewcross_1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        autocompletemrchantlist.setText("");
                        Constants.paymentmerchname = "";
                        Constants.paymentmerchant_id = "";
                        smpymntmerchantselected = 0;
                        Log.e("Merchant", Constants.paymentmerchname);
                        Log.e("selected merchant Id", Constants.paymentmerchant_id);
                        if (autocompletemrchantlist.getText().equals("")) {
                            InputMethodManager iaa = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            iaa.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                        }

                    }
                });

                return false;
            }
        });
        autocompletemrchantlist
                .setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {

                        String mnsme = autocompletemrchantlist.getText().toString();

                        for (MDealerCompDropdownDomain cd : arrayListmerchant) {

                            if (cd.getComName().equals(mnsme)) {
                                muserId = cd.getID();

                                merchantmobileno = cd.getmobileno();
                                Constants.paymentmerchant_id = muserId;
                                smpymntmerchantselected = 1;

                                if (smpymntmerchantselected == 1) {

                                    textViewcross_1.setVisibility(View.GONE);
                                    InputMethodManager iaa = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                    iaa.hideSoftInputFromWindow(autocompletemrchantlist.getWindowToken(), 0);
                                }
                                if (!autocompletemrchantlist.getText().equals("")) {
                                    myOrders();
                                }

                                Constants.paymentmerchname = mnsme;
                                Log.e("Merchant", Constants.paymentmerchname);
                                Log.e("selected merchant Id", Constants.paymentmerchant_id);
                                Log.e("smobileno", merchantmobileno);

                            }
                        }


                    }
                });


        buttonpay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Payamount();
                newPayAmount();///////////////new
            }
        });


        linearlayoutoverallayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textViewcross_1.setVisibility(View.GONE);
            }
        });

        menuIcon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (menuCliccked == 0) {
                    linearLayoutMenuParent.setVisibility(View.VISIBLE);
                    menuCliccked = 1;
                } else {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                }

            }
        });

        linearLayoutMenuParent.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
            }
        });

        linearLayoutProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto = "profile";
                if (netCheckwithoutAlert() == true) {
                    getData();
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });
        linearLayoutDistanceCalculation.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto = "profile";

                  Intent into=new Intent(SalesmanPaymentCollectionActivity.this, DistanceCalActivity.class);
                    startActivity(into);


            }
        });
        linearLayoutRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("Clicked", "Clicked");

                if (netCheck()) {

                    final Dialog qtyDialog = new Dialog(SalesmanPaymentCollectionActivity.this);
                    qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    qtyDialog.setContentView(R.layout.warning_dialog);

                    final Button buttonUpdateOk, buttonUpdateCancel;

                    final TextView textQtyValidate;

                    buttonUpdateOk = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateOk);
                    buttonUpdateCancel = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateCancel);

                    textQtyValidate = (TextView) qtyDialog
                            .findViewById(R.id.textQtyValidate);
                    qtyDialog.show();


                    buttonUpdateOk.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            databaseHandler.synTableDelete();
                            Constants.refreshscreen = "paymentcollection";
                            Intent io = new Intent(SalesmanPaymentCollectionActivity.this, RefreshActivity.class);
                            startActivity(io);
                            finish();
                            qtyDialog.dismiss();
                        }

                    });

                    buttonUpdateCancel
                            .setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub

                                    qtyDialog.dismiss();

                                }
                            });

                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }
            }

        });

        linearLayoutMyOrders.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Constants.orderstatus = "Success";
                Constants.checkproduct = 0;
                linearLayoutMenuParent.setVisibility(View.GONE);
                moveto = "myorder";
                if (netCheckwithoutAlert() == true) {
                    getData();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {

                        Intent io = new Intent(SalesmanPaymentCollectionActivity.this,
                                SalesManOrderActivity.class);
                        io.putExtra("Key", "menuclick");
                        Constants.setimage = null;
                        startActivity(io);
                        finish();
                    }
                }

            }
        });

        linearLayoutMyDealers.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                moveto = "Mydealer";
                if (netCheckwithoutAlert() == true) {
                    getData();
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }


            }
        });


        linearLayoutCreateOrder.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                MyApplication.getInstance().setTemplate8ForTemplate2(false);

                linearLayoutMenuParent.setVisibility(View.GONE);
                moveto = "createorder";
                Constants.checkproduct = 0;
                Constants.orderid = "";
                if (netCheckwithoutAlert() == true) {
                    getData();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment to place order");
                    } else {
                        Intent io = new Intent(SalesmanPaymentCollectionActivity.this, CreateOrderActivity.class);
                        startActivity(io);
                        finish();
                    }
                }


            }
        });


        linearLayoutComplaint.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto = "postnotes";
                if (netCheckwithoutAlert() == true) {
                    getData();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment");
                    } else {
                        Intent io = new Intent(SalesmanPaymentCollectionActivity.this,
                                MerchantComplaintActivity.class);
                        startActivity(io);
                        finish();
                    }
                }

            }
        });

        linearLayoutClientVisit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto = "clientvisit";
                if (netCheckwithoutAlert() == true) {
                    getData();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment");
                    } else {
                        Intent io = new Intent(SalesmanPaymentCollectionActivity.this,
                                SMClientVisitHistory.class);
                        startActivity(io);
                        finish();
                    }
                }


            }
        });
        linearLayoutPaymentCollection.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;

            }
        });

        linearLayoutStockEntry.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto = "stockentry";
                if (netCheckwithoutAlert() == true) {
                    getData();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment");
                    }else {
                        Intent io = new Intent(SalesmanPaymentCollectionActivity.this,
                                OutletStockEntry.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });
        linearLayoutDistStock.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto = "diststock";
                if (netCheckwithoutAlert() == true) {
                    getData();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment");
                    } else {
                        Intent io = new Intent(SalesmanPaymentCollectionActivity.this,
                                DistributrStockActivity.class);
                        startActivity(io);
                        finish();
                    }
                }


            }
        });
        linearLayoutAcknowledge.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto = "acknowledge";
                if (netCheckwithoutAlert() == true) {
                    getData();
                } else {

                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });

        linearLayoutAddMerchant.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto = "addmerchant";
                if (netCheckwithoutAlert() == true) {
                    getData();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent to = new Intent(SalesmanPaymentCollectionActivity.this,
                                AddMerchantNew.class);
                        startActivity(to);
                        finish();
                    }
                }

            }
        });
        linearLayoutdownloadscheme.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck()) {

                    final Dialog qtyDialog = new Dialog(SalesmanPaymentCollectionActivity.this);
                    qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    qtyDialog.setContentView(R.layout.warning_dialog);

                    final Button buttonUpdateOk, buttonUpdateCancel;

                    final TextView textQtyValidate;

                    buttonUpdateOk = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateOk);
                    buttonUpdateCancel = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateCancel);

                    textQtyValidate = (TextView) qtyDialog
                            .findViewById(R.id.textQtyValidate);
                    qtyDialog.show();


                    buttonUpdateOk.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            databaseHandler.deletescheme();

                            Constants.downloadScheme = "paymentcollection";
                            Intent io = new Intent(SalesmanPaymentCollectionActivity.this, SchemeDownload.class);
                            startActivity(io);
                            finish();
                            qtyDialog.dismiss();
                        }

                    });

                    buttonUpdateCancel
                            .setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub

                                    qtyDialog.dismiss();

                                }
                            });

                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });
        linearLayoutsurveyuser.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheck()) {
                    moveto = "surveyuser";
                    getData();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent to = new Intent(SalesmanPaymentCollectionActivity.this, SurveyMerchant.class);
                        startActivity(to);
                        finish();
                    }
                }
            }
        });

        linearLayoutMyDayPlan.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    moveto = "mydayplan";
                    getData();
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }
            }
        });

        linearLayoutMySales.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    moveto = "mysales";
                    getData();
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }
            }
        });

        linearLayoutSignout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                // TODO Auto-generated method stub
                databaseHandler.delete();
                Intent io = new Intent(SalesmanPaymentCollectionActivity.this, SigninActivity.class);
                startActivity(io);
                finish();
            }
        });
        linearLayoutMigration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Intent io = new Intent(SalesmanPaymentCollectionActivity.this,
                        PendingDataMigrationActivity.class);
                startActivity(io);
            }
        });

        linearLayoutStockOnly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Constants.checkproduct = 0;
                Constants.orderid = "";
                Constants.SalesMerchant_Id = "";
                Constants.Merchantname = "";
                MyApplication.getInstance().setTemplate8ForTemplate2(true);
                if (netCheck()) {
                    Intent io = new Intent(SalesmanPaymentCollectionActivity.this,
                            ClosingStockDashBoardActivity.class);
                    startActivity(io);
                    finish();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals("") || PaymentStatus.equalsIgnoreCase("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(SalesmanPaymentCollectionActivity.this,
                                ClosingStockDashBoardActivity.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });

        linearLayoutStockAudit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().setTemplate8ForTemplate2(false);
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Constants.checkproduct = 0;
                Constants.orderid = "";
                Constants.SalesMerchant_Id = "";
                Constants.Merchantname = "";
                if (netCheck()) {
                    Intent io = new Intent(SalesmanPaymentCollectionActivity.this,
                            ClosingStockAudit.class);
                    startActivity(io);
                    finish();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals("") || PaymentStatus.equalsIgnoreCase("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(SalesmanPaymentCollectionActivity.this,
                                ClosingStockAudit.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });

        ///////////
        showMenu();
        ////////////

    }

    public void getBeatMerchant(String beatname) {
        Cursor cursor;
        cursor = databaseHandler.getbeatMerchant(beatname);
        String merchants = "";
        int i = 0;
        if (cursor != null && cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {

                do {
                    if (i == 0) {
                        if (cursor.getString(0).equals("New User")) {
                            merchants = "'" + cursor.getString(0) + "'";
                        } else {
                            merchants = cursor.getString(0);
                        }

                        i++;
                    } else {
                        if (cursor.getString(0).equals("New User")) {
                            merchants = merchants + ", '" + cursor.getString(0) + "'";
                        } else {
                            merchants = merchants + "," + cursor.getString(0);
                        }

                    }


                } while (cursor.moveToNext());


            }
        }
        Log.e("merchants list", merchants);
        loadingMerchant(merchants);
    }

    private void getData() {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "";

            @Override
            protected void onPreExecute() {
                dialog = ProgressDialog.show(SalesmanPaymentCollectionActivity.this, "",
                        "Loading...", true, true);

            }

            @Override
            protected void onPostExecute(Void result) {

                try {

                    PaymentStatus = (JsonAccountObject.getString("pymtstatus"));
                    Log.e("PaymentStatus", PaymentStatus);

                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment to place order");
                    } else {
                        if (moveto.equals("myorder")) {

                            Intent io = new Intent(SalesmanPaymentCollectionActivity.this,
                                    SalesManOrderActivity.class);
                            startActivity(io);
                            finish();

                        } else if (moveto.equals("createorder")) {
                            Intent io = new Intent(SalesmanPaymentCollectionActivity.this,
                                    CreateOrderActivity.class);
                            startActivity(io);
                            finish();
                        } else if (moveto.equals("Mydealer")) {
                            Intent io = new Intent(SalesmanPaymentCollectionActivity.this,
                                    MyDealersActivity.class);
                            io.putExtra("Key", "MenuClick");
                            startActivity(io);
                            finish();
                        } else if (moveto.equals("profile")) {
                            Intent io = new Intent(SalesmanPaymentCollectionActivity.this,
                                    ProfileActivity.class);
                            startActivity(io);
                            finish();
                        } else if (moveto.equals("postnotes")) {
                            Intent io = new Intent(SalesmanPaymentCollectionActivity.this,
                                    MerchantComplaintActivity.class);
                            startActivity(io);
                            finish();
                        } else if (moveto.equals("clientvisit")) {
                            Intent io = new Intent(SalesmanPaymentCollectionActivity.this,
                                    SMClientVisitHistory.class);
                            startActivity(io);
                            finish();
                        } else if (moveto.equals("addmerchant")) {
                            Intent io = new Intent(SalesmanPaymentCollectionActivity.this,
                                    AddMerchantNew.class);
                            startActivity(io);
                            finish();
                        } else if (moveto.equals("stockentry")) {
                            Intent io = new Intent(SalesmanPaymentCollectionActivity.this,
                                    OutletStockEntry.class);
                            startActivity(io);
                            finish();
                        } else if (moveto.equals("acknowledge")) {
                            Intent io = new Intent(SalesmanPaymentCollectionActivity.this,
                                    SalesmanAcknowledgeActivity.class);
                            startActivity(io);
                            finish();
                        } else if (moveto.equals("surveyuser")) {
                            Intent to = new Intent(SalesmanPaymentCollectionActivity.this,
                                    SurveyMerchant.class);
                            startActivity(to);
                            finish();
                        } else if (moveto.equals("mydayplan")) {
                            Intent to = new Intent(SalesmanPaymentCollectionActivity.this,
                                    MyDayPlan.class);
                            startActivity(to);
                            finish();
                        } else if (moveto.equals("mysales")) {
                            Intent to = new Intent(SalesmanPaymentCollectionActivity.this,
                                    MySalesActivity.class);
                            startActivity(to);
                            finish();
                        } else if (moveto.equals("diststock")) {
                            Intent io = new Intent(SalesmanPaymentCollectionActivity.this,
                                    DistributrStockActivity.class);
                            startActivity(io);
                            finish();
                        }

                    }

                    dialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();


                JsonServiceHandler = new JsonServiceHandler(Utils.strGetProfileDetail + Constants.USER_ID, SalesmanPaymentCollectionActivity.this);
                JsonAccountObject = JsonServiceHandler.ServiceDataGet();
                return null;
            }
        }.execute(new Void[]{});
    }

    public void offlineBeat() {
        Cursor curs;
        curs = databaseHandler.getbeatwithMerchant(dayOfTheWeek);
        Log.e("beatCount", String.valueOf(curs.getCount()));

        int i = 0;

        if (curs != null && curs.getCount() > 0) {
            if (curs.moveToFirst()) {

                do {
                    Log.e("beat inside", "beat inside");
                    if (i == 0) {
                        if (curs.getString(0).equals("New User")) {
                            beatMerchants = "'" + curs.getString(0) + "'";
                        } else {
                            beatMerchants = curs.getString(0);
                        }


                        i++;
                    } else {
                        if (curs.getString(0).equals("New User")) {
                            beatMerchants = beatMerchants + "," + "'" + curs.getString(0) + "'";
                        } else {
                            beatMerchants = beatMerchants + "," + curs.getString(0);
                        }


                    }

                    Log.e("beatMerchants", beatMerchants);


                } while (curs.moveToNext());


            }
            loadingMerchant(beatMerchants);
        } else {
            showAlertDialogToast("No Merchants available for this day");
        }

    }

    public void loadingMerchant(String beatMerchants) {


        Cursor curs;
        curs = databaseHandler.getmerchantoncurrentday(beatMerchants);
        Log.e("merchantCount", String.valueOf(curs.getCount()));

        if (curs != null && curs.getCount() > 0) {
            if (curs.moveToFirst()) {

                do {
                    Log.e("inside", "inside");
                    MDealerCompDropdownDomain cd = new MDealerCompDropdownDomain();
                    cd.setComName(curs.getString(1));
                    cd.setID(String.valueOf(curs.getString(2)));
                    cd.setName(curs.getString(3));
                    cd.setmobileno(curs.getString(4));
                    ;
                    arrayListDealerDomain.add(cd);
                    arrayListSearch.add(cd);
                    arrayListDealer.add(curs.getString(1));
                    arrayListmerchant.add(cd);

                } while (curs.moveToNext());


            }
            Log.e("outside", "outside");
        } else {

        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                SalesmanPaymentCollectionActivity.this, R.layout.textview, R.id.textView,
                arrayListDealer);

        autocompletemrchantlist.setAdapter(adapter);


    }

    private void myOrders() {
        // TODO Auto-generated method stub

        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialogs;
            String strStatus = "";

            @Override
            protected void onPreExecute() {
                dialogs = ProgressDialog.show(SalesmanPaymentCollectionActivity.this, "",
                        "Updating...", true, false);

            }

            @Override
            protected void onPostExecute(Void result) {
                String strMsg = "";
                if(dialogs != null){
                    dialogs.dismiss();
                }
                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    buttonpay.setVisibility(View.VISIBLE);

                    if (strStatus.equals("true")) {

                        JSONArray job1 = JsonAccountObject.getJSONArray("data");

                        textViewshowbal.setVisibility(View.VISIBLE);
                        textviewnodata.setVisibility(View.GONE);
                        listView.setVisibility(View.VISIBLE);
                        balamt.setVisibility(View.VISIBLE);
                        arraylistpaymentcollection = new ArrayList<CollectionDomain>();
                        balamount = 0.0;
                        for (int i = 0; i < job1.length(); i++) {

                            JSONObject job = new JSONObject();
                            job = job1.getJSONObject(i);
                            CollectionDomain dod = new CollectionDomain();

                            dod.setBalncAmount(Double.parseDouble(job.getString("balamount")));
                            dod.setBillAmount(job.getString("billtotal"));
                            dod.setBillno(job.getString("billno"));
                            dod.setbillid(job.getString("billid"));
                            balamount = balamount + Double.parseDouble(job.getString("balamount"));

                            time = job.getString("billdt");

                            String inputPattern = "yyyy-MM-dd";
                            String outputPattern = "dd-MMM-yyyy ";
                            SimpleDateFormat inputFormat = new SimpleDateFormat(
                                    inputPattern);
                            SimpleDateFormat outputFormat = new SimpleDateFormat(
                                    outputPattern);

                            String str = null;

                            try {
                                dateStr = inputFormat.parse(time);
                                str = outputFormat.format(dateStr);
                                dod.setBillDate(str);
                                // Log.e("str", str);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            arraylistpaymentcollection.add(dod);
                        }

                        setadap();
                        balamount = Double.parseDouble(new DecimalFormat("##.##").format(balamount));
                        balamt.setText(String.valueOf(balamount));

                    } else {
                        textviewnodata.setVisibility(View.VISIBLE);
                        listView.setVisibility(View.GONE);
                        textviewnodata.setText("No data found");
                        balamt.setVisibility(View.GONE);
                        textViewshowbal.setVisibility(View.GONE);
                        buttonpay.setVisibility(View.GONE);
                    }

                    //dialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                    if(dialogs != null){
                        dialogs.dismiss();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if(dialogs != null){
                        dialogs.dismiss();
                    }
                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();

                try {

                    jsonObject.put("suserid", Constants.USER_ID);
                    jsonObject.put("muserid", muserId);
                    jsonObject.put("usertype", Constants.USER_TYPE);
                    jsonObject.put("paylist", "Y");

                } catch (JSONException e) {
                    e.printStackTrace();
                }


                JsonServiceHandler = new JsonServiceHandler(
                        Utils.strsalespaymntclctn, jsonObject.toString(),
                        SalesmanPaymentCollectionActivity.this);
                JsonAccountObject = JsonServiceHandler.ServiceData();
                return null;
            }
        }.execute(new Void[]{});
    }

    public void setadap() {
        // TODO Auto-generated method stub

        adapter = new SalesmanPaymentCollectionAdapter(SalesmanPaymentCollectionActivity.this,
                R.layout.item_screen_paymentcollection, arraylistpaymentcollection);

        listView.setAdapter(adapter);


    }

    public void newPayAmount(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        PopupSurveyPayment paymentPopup = new PopupSurveyPayment(SalesmanPaymentCollectionActivity.this,findViewById(R.id.linearlayoutoverallayout),
                buttonpay,displayMetrics, Constants.paymentmerchname,balamount, new PopupSurveyPayment.PaymentCompleteListener() {
            @Override
            public void success(PaymentCollectionModel result) {
                payamount(result);
            }

            @Override
            public void failed() {

            }
        });
        paymentPopup.showPopupWindow();
    }

    public void Payamount() {
        qtyDialog = new Dialog(SalesmanPaymentCollectionActivity.this);
        qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        qtyDialog.setContentView(R.layout.payment_collection_dialog);

        final EditText editTextpayamount;
        Button buttonUpdateOk, buttonUpdateCancel;

        buttonUpdateOk = (Button) qtyDialog
                .findViewById(R.id.buttonUpdateOk);
        buttonUpdateCancel = (Button) qtyDialog
                .findViewById(R.id.buttonUpdateCancel);
        editTextpayamount = (EditText) qtyDialog
                .findViewById(R.id.editTextpayamount);


        qtyDialog.show();

        InputMethodManager inn = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        inn.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        buttonUpdateOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager iss = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                iss.hideSoftInputFromWindow(editTextpayamount.getWindowToken(), 0);
                amount = Double.parseDouble(editTextpayamount.getText().toString());

                double balanceamount = Double.parseDouble(String.valueOf(balamount));
                double payamount = Double.parseDouble(String.valueOf(amount));

                Log.e("payamount", String.valueOf(payamount));
                Log.e("amount", String.valueOf(balamount));

                if (balanceamount < payamount) {
                    Log.e("inside", "inside");
                    showAlertDialogToast("The amount you enter is greater than the balance amount");

                } else {
                    //////////////////payamount(amount);
                }
            }
        });
        buttonUpdateCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager iss = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                iss.hideSoftInputFromWindow(editTextpayamount.getWindowToken(), 0);
                qtyDialog.dismiss();

            }
        });
    }


    public void payamount(final PaymentCollectionModel result) {
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";

            @Override
            protected void onPreExecute() {
                dialog = ProgressDialog.show(SalesmanPaymentCollectionActivity.this, "",
                        "Loading....", true, false);

            }

            @Override
            protected void onPostExecute(Void result) {
                String strMsg = "";
                if( dialog != null){
                    dialog.dismiss();
                }
                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {

                        showAlertDialogToast1(strMsg);

                        ////qtyDialog.dismiss(); no need

                    } else {

                    }

                    //dialog.dismiss();
                } catch (JSONException e) {
                    if( dialog != null){
                        dialog.dismiss();
                    }
                    e.printStackTrace();
                } catch (Exception e) {
                    if( dialog != null){
                        dialog.dismiss();
                    }
                    e.printStackTrace();
                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();
                JSONObject jsonObject1 = new JSONObject();
                JSONArray jobarray = new JSONArray();

                int a = 0;

                Cursor cur;
                cur = databaseHandler.getdealer();
                Log.e("dealercount", String.valueOf(cur.getCount()));


                if (cur != null && cur.getCount() > 0) {
                    if (cur.moveToLast()) {

                        dealername = cur.getString(4);
                        Log.e("Ddate", String.valueOf(cur.getString(4)));
                    }
                }

                Log.e("size", String.valueOf(arraylistpaymentcollection.size()));
                billno = new Integer[arraylistpaymentcollection.size()];

                try {
                    for (int i = 0; i < arraylistpaymentcollection.size(); i++) {

                        billno[i] = Integer.valueOf(arraylistpaymentcollection.get(i).getbillid());
                    }

                    for (int j = 0; j < billno.length; j++) {
                        jobarray.put(billno[j]);
                    }

                    Log.e("Check","......................result.getNotes()......."
                            + result.getNotes() + "...result.getPymtmode()..."+ result.getPymtmode());
                    StringBuilder sb1 = new StringBuilder();
                    if(result.getPymtmode().equals("Cheque")) {
                        JSONObject json = new JSONObject(result.getNotes());
                        int len = json.length();
                        for (Iterator<String> it = json.keys(); it.hasNext(); ) {
                            String k = it.next();
                            sb1.append(k);
                            sb1.append(":");
                            sb1.append(json.getString(k));
                            if (len != 1) {
                                sb1.append(",");
                            }
                            len--;
                        }
                    }else {
                        sb1.append(result.getNotes());
                    }

                    jsonObject.put("suserid", Constants.USER_ID);
                    jsonObject.put("updateddt", result.getUpdateddt());
                    jsonObject.put("bill_list", jobarray);
                    jsonObject.put("notes", sb1);
                    //jsonObject.put("notes", result.getNotes());
                    jsonObject.put("pymtmode", result.getPymtmode());
                    jsonObject.put("payamt", result.getPayamt());

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                String out = jsonObject.toString().replaceAll("\\\\", "");

                JsonServiceHandler = new JsonServiceHandler(
                        Utils.strupdatepaymntcollectionNew, out/*jsonObject.toString()*/,
                        SalesmanPaymentCollectionActivity.this);
                JsonAccountObject = JsonServiceHandler.ServiceData();


                return null;
            }
        }.execute(new Void[]{});


    }


    public boolean netCheckwithoutAlert() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            } else if (result == null) {

                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public void showAlertDialogToast1(String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {


                        myOrders();
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    public void showAlertDialogToast(String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                        InputMethodManager inn = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                        inn.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    public boolean netCheck() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            } else if (result == null) {
               /* showAlertDialog(SalesManOrderActivity.this,
                        "No Internet Connection",
                        "Please Check Your internet connection.", false);*/
                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

}
