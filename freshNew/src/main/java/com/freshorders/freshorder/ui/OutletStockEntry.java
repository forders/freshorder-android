package com.freshorders.freshorder.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.activity.DistanceCalActivity;
import com.freshorders.freshorder.adapter.OutleyStockEntryAdapter;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.MDealerCompDropdownDomain;
import com.freshorders.freshorder.domain.OutletStockEntryDomain;
import com.freshorders.freshorder.toonline.migration.PendingDataMigrationActivity;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class OutletStockEntry extends Activity {

    TextView menuIcon, textViewAssetMenuProfile, textViewAssetMenuMyOrders,
            textViewAssetMenuMyDealers, textViewAssetMenuProducts,
            textViewAssetMenuPayment, textViewAssetMenuComplaint,textViewAssetMenuDistanceCalculation,
            textViewAssetMenuSignout,textViewAssetMenuCreateOrder,textViewAssetMenuExport,
            textViewAssetMenuClientVisit,textViewAssetMenuPaymentCollection,textViewAssetMenuRefresh,
            textViewAssetMenuStockEntry,textViewAssetMenuAddMerchant,textViewAssetMenuAcknowledge
            ,textViewAssetMenusurveyuser,textViewAssetMenuDistStock, textViewAssetMenuGRN,
            textViewAssetMenudownloadscheme,textViewAssetMenuMyDayPlan,textViewAssetMenuMySales,
            textViewAssetMenuOrderAcceptance, textViewAssetMenuStockOnly, textViewAssetMenuMigration, textViewAssetMenuStockAudit;

    public int q= 0;
    int menuCliccked;

    LinearLayout linearLayoutMenuParent,linearLayoutProfile, linearLayoutMyOrders,
            linearLayoutMyDealers, linearLayoutProducts, linearLayoutPayment,
            linearLayoutComplaint, linearLayoutSignout,linearLayoutEdit,linearLayoutDetails,
            linearLayoutCreateOrder,linearLayoutExport,linearLayoutClientVisit,linearLayoutPaymentCollection,
            linearlayoutoverallayout,linearLayoutRefresh,linearLayoutDistanceCalculation,
            linearLayoutAcknowledge,linearLayoutStockEntry,linearLayoutAddMerchant
            ,linearLayoutsurveyuser,linearLayoutdownloadscheme,linearlayoutSearchIcon,linearLayoutMyDayPlan,linearLayoutMySales,
            linearLayoutDistStock, linearLayoutgrn, linearLayoutorderacceptance, linearLayoutStockOnly, linearLayoutMigration, linearLayoutStockAudit;

    private void showMenu(){

        MyApplication app = MyApplication.getInstance();

        if(!app.isCreateOrder()){
            linearLayoutCreateOrder.setVisibility(View.GONE);
        }
        if(!app.isProfile()){
            linearLayoutProfile.setVisibility(View.GONE);
        }
        if(!app.isMyDayPlan()){
            linearLayoutMyDayPlan.setVisibility(View.GONE);
        }
        if(!app.isMyOrders()){
            linearLayoutMyOrders.setVisibility(View.GONE);
        }
        if(!app.isAddMerchant()){
            linearLayoutAddMerchant.setVisibility(View.GONE);
        }
        if(!app.isMySales()){
            linearLayoutMySales.setVisibility(View.GONE);
        }
        if(!app.isPostNotes()){
            linearLayoutComplaint.setVisibility(View.GONE);
        }
        if(!app.isMyClientVisit()){
            linearLayoutClientVisit.setVisibility(View.GONE);
        }
        if(!app.isAcknowledge()){
            linearLayoutAcknowledge.setVisibility(View.GONE);
        }
        if(!app.isPaymentCollection()){
            linearLayoutPaymentCollection.setVisibility(View.GONE);
        }
        if(!app.isPkdDataCapture()){
            linearLayoutStockEntry.setVisibility(View.GONE);
        }
        if(!app.isDistributorStock()){
            linearLayoutDistStock.setVisibility(View.GONE);
        }
        if(!app.isSurveyUser()){
            linearLayoutsurveyuser.setVisibility(View.GONE);
        }
        if(!app.isDownLoadScheme()){
            linearLayoutdownloadscheme.setVisibility(View.GONE);
        }
        if(!app.isDistanceCalculation()){
            linearLayoutDistanceCalculation.setVisibility(View.GONE);
        }
        if(!app.isRefresh()){
            linearLayoutRefresh.setVisibility(View.GONE);
        }
        if(!app.isLogout()){
            linearLayoutSignout.setVisibility(View.GONE);
        }
        if(!app.isClosingStock()){
            linearLayoutStockOnly.setVisibility(View.GONE);
        }
        if(!app.isClosingStockAudit()){
            linearLayoutStockAudit.setVisibility(View.GONE);
        }
        if(!app.isPendingData()){
            linearLayoutMigration.setVisibility(View.GONE);
        }
    }

    public  Dialog qtyDialog;
    AutoCompleteTextView autocompletemrchantlist;
    TextView textViewcross_1,textviewnodata;
    ListView listView;
    Button buttonpay;

    JSONObject JsonAccountObject = null;
    JSONArray JsonAccountArray = null;
    DatabaseHandler databaseHandler;
    JsonServiceHandler JsonServiceHandler ;
    ArrayList<MDealerCompDropdownDomain> arrayListDealerDomain,arrayListSearch,arrayListmerchant;
    ArrayList<String> arrayListDealer;
    public static int smpymntmerchantselected=0;
    String muserId="",merchantmobileno="";
    OutleyStockEntryAdapter adapter;
    public static String PaymentStatus="NULL",moveto="NULL",dealername,time="";
    static ArrayList<OutletStockEntryDomain> arraylistDealerOrderList;
    Date dateStr;
    public static String companyname,mname, fullname, date, address, orderId,merchantid,selectedmechantname,onlineorderno,Paymentdate="NULL",salesuserid="NULL";
    public static OutletStockEntryDomain domainSelected;
    public static String prodid,prodname,prodcode,userid,pdate,pcompanyname,Porderdate,qty,fqty,Dorderdate,
            prodprice,prodtax,mfgdate,expdate,batchno,uom,searchclick="0",defaultuom="";
    public static ArrayList<OutletStockEntryDomain> arraylistMerchantOrderDetailList;
    String dayOfTheWeek="",beatMerchants,userBeatModel ="";

    public static EditText editTextSearchField;
    public static TextView textViewHeader,textViewAssetSearch,textViewCross;
    @Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		super.onCreate(savedInstanceState);
		requestWindowFeature(1);
        setContentView(R.layout.outlet_stock_entry);

        databaseHandler = new DatabaseHandler(getApplicationContext());
        ///Kumaravel /// For Offline Count
        TextView tvFailedCount = (TextView) findViewById(R.id.id_pkd_failedCount);
        TextView tvOfflineCount = (TextView) findViewById(R.id.id_pkd_offlineCount);
        String strFailedCount = tvFailedCount.getText().toString();
        String strOfflineCount = tvOfflineCount.getText().toString();

        Cursor cur_count = databaseHandler.getPKDPending();
        if(cur_count != null && cur_count.getCount() > 0){
            tvOfflineCount.setText((strOfflineCount+cur_count.getCount()));
            cur_count.close();
        }else {
            tvOfflineCount.setText((strOfflineCount+0));
        }
        Cursor cur_failed_count = databaseHandler.getPKDFailed();
        if(cur_failed_count != null && cur_failed_count.getCount() > 0){
            tvFailedCount.setText((strFailedCount+cur_failed_count.getCount()));
            cur_failed_count.close();
        }else {
            tvFailedCount.setText((strFailedCount+0));
        }

        Cursor curPay;
        curPay = databaseHandler.getDetails();
        if(curPay != null && curPay.getCount() > 0){
            curPay.moveToFirst();
            PaymentStatus = curPay.getString(curPay.getColumnIndex(DatabaseHandler.KEY_paymentStatus));
            curPay.close();
        }


		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();

		autocompletemrchantlist=(AutoCompleteTextView) findViewById(R.id.autocompletemrchantlist);

		textViewcross_1=(TextView) findViewById(R.id.textViewcross_1);

        textViewCross=(TextView) findViewById(R.id.textViewCross);
        editTextSearchField = (EditText) findViewById(R.id.editTextSearchField);
        textViewAssetSearch= (TextView) findViewById(R.id.textViewAssetSearch);
        textViewHeader= (TextView) findViewById(R.id.textViewHeader);

		listView=(ListView ) findViewById(R.id.listView);

        textviewnodata=(TextView) findViewById(R.id.textViewnodata);
        arrayListDealer = new ArrayList<String>();
        arrayListDealerDomain = new ArrayList<MDealerCompDropdownDomain>();
        arrayListmerchant= new ArrayList<MDealerCompDropdownDomain>();
        arrayListSearch= new ArrayList<MDealerCompDropdownDomain>();

        linearlayoutSearchIcon = (LinearLayout) findViewById(R.id.linearlayoutSearchIcon);
        linearlayoutoverallayout= (LinearLayout) findViewById(R.id.linearlayoutoverallayout);
        linearLayoutCreateOrder = (LinearLayout) findViewById(R.id.linearLayoutCreateOrder);
        linearLayoutProfile = (LinearLayout) findViewById(R.id.linearLayoutProfile);
        linearLayoutMyOrders = (LinearLayout) findViewById(R.id.linearLayoutMyOrders);
        linearLayoutMyDealers = (LinearLayout) findViewById(R.id.linearLayoutMyDealers);
        linearLayoutRefresh= (LinearLayout) findViewById(R.id.linearLayoutRefresh);
        textViewAssetMenuRefresh = (TextView) findViewById(R.id.textViewAssetMenuRefresh);
        linearLayoutPayment = (LinearLayout) findViewById(R.id.linearLayoutPayment);
        linearLayoutComplaint = (LinearLayout) findViewById(R.id.linearLayoutComplaint);
        linearLayoutSignout = (LinearLayout) findViewById(R.id.linearLayoutSignout);
        linearLayoutDistanceCalculation= (LinearLayout) findViewById(R.id.linearLayoutDistanceCalculation);
        linearLayoutClientVisit= (LinearLayout) findViewById(R.id.linearLayoutClientVisit);
        linearLayoutPaymentCollection= (LinearLayout) findViewById(R.id.linearLayoutPaymentCollection);

        linearLayoutAcknowledge= (LinearLayout) findViewById(R.id.linearLayoutAcknowledge);
        linearLayoutStockEntry= (LinearLayout) findViewById(R.id.linearLayoutStockEntry);
        linearLayoutAddMerchant = (LinearLayout) findViewById(R.id.linearLayoutAddMerchant);

        linearLayoutgrn = (LinearLayout) findViewById(R.id.linearLayoutGRN);
        linearLayoutorderacceptance = (LinearLayout) findViewById(R.id.linearLayoutOrderAcceptance);

        //////////////////////////// 30-07-2019
        linearLayoutgrn.setVisibility(View.GONE);
        linearLayoutorderacceptance.setVisibility(View.GONE);
        //////////////////////////

        linearLayoutdownloadscheme= (LinearLayout) findViewById(R.id.linearLayoutdownloadscheme);
        linearLayoutsurveyuser= (LinearLayout) findViewById(R.id.linearLayoutsurveyuser);
        linearLayoutMyDayPlan = (LinearLayout) findViewById(R.id.linearLayoutMyDayPlan);
        linearLayoutMySales = (LinearLayout) findViewById(R.id.linearLayoutMySales);

        menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
        textViewAssetMenuCreateOrder= (TextView) findViewById(R.id.textViewAssetMenuCreateOrder);
        textViewAssetMenuProfile = (TextView) findViewById(R.id.textViewAssetMenuProfile);
        textViewAssetMenuMyOrders = (TextView) findViewById(R.id.textViewAssetMenuMyOrders);
        textViewAssetMenuMyDealers = (TextView) findViewById(R.id.textViewAssetMenuMyDealers);

        textViewAssetMenuPayment = (TextView) findViewById(R.id.textViewAssetMenuPayment);
        textViewAssetMenuComplaint = (TextView) findViewById(R.id.textViewAssetMenuComplaint);
        textViewAssetMenuSignout = (TextView) findViewById(R.id.textViewAssetMenuSignout);

        textViewAssetMenuClientVisit= (TextView) findViewById(R.id.textViewAssetMenuClientVisit);
        textViewAssetMenuPaymentCollection= (TextView) findViewById(R.id.textViewAssetMenuPaymentCollection);

        textViewAssetMenuAddMerchant = (TextView) findViewById(R.id.textViewAssetMenuAddMerchant);
        textViewAssetMenuAcknowledge = (TextView) findViewById(R.id.textViewAssetMenuAcknowledge);
        textViewAssetMenuStockEntry = (TextView) findViewById(R.id.textViewAssetMenuStockEntry);
        textViewAssetMenuDistanceCalculation = (TextView) findViewById(R.id.textViewAssetMenuDistanceCalculation);
        textViewAssetMenudownloadscheme= (TextView) findViewById(R.id.textViewAssetMenudownloadscheme);
        textViewAssetMenusurveyuser= (TextView) findViewById(R.id.textViewAssetMenusurveyuser);
        textViewAssetMenuMyDayPlan= (TextView) findViewById(R.id.textViewAssetMenuMyDayPlan);
        textViewAssetMenuMySales= (TextView) findViewById(R.id.textViewAssetMenuMySales);
        textViewAssetMenuOrderAcceptance= (TextView) findViewById(R.id.textViewAssetMenuGRN);
        textViewAssetMenuGRN= (TextView) findViewById(R.id.textViewAssetMenuOrderAcceptance);

        textViewAssetMenuStockOnly = (TextView) findViewById(R.id.textViewAssetMenuStockOnly);
        textViewAssetMenuStockAudit = (TextView) findViewById(R.id.textViewAssetMenuStockAudit);
        textViewAssetMenuMigration = findViewById(R.id.textViewAssetMenuMigration);


        linearLayoutMenuParent = (LinearLayout) findViewById(R.id.linearLayoutMenuParent);


        Typeface font = Typeface.createFromAsset(getAssets(),
                "fontawesome-webfont.ttf");

        textViewcross_1.setTypeface(font);
        menuIcon.setTypeface(font);
        textViewAssetMenuCreateOrder.setTypeface(font);
        textViewAssetMenuProfile.setTypeface(font);
        textViewAssetMenuMyOrders.setTypeface(font);
        textViewAssetMenuMyDealers.setTypeface(font);
        textViewAssetMenuRefresh.setTypeface(font);
        textViewAssetMenuPayment.setTypeface(font);
        textViewAssetMenuComplaint.setTypeface(font);
        textViewAssetMenuSignout.setTypeface(font);
        textViewAssetMenuDistanceCalculation.setTypeface(font);
        textViewAssetMenuClientVisit.setTypeface(font);
        textViewAssetMenuPaymentCollection.setTypeface(font);

        textViewAssetMenuAcknowledge.setTypeface(font);
        textViewAssetMenuStockEntry.setTypeface(font);
        textViewAssetMenuAddMerchant.setTypeface(font);

        textViewAssetMenusurveyuser.setTypeface(font);
        textViewAssetMenudownloadscheme.setTypeface(font);
        textViewAssetMenuMyDayPlan.setTypeface(font);
        textViewAssetMenuMySales.setTypeface(font);
        textViewAssetMenuOrderAcceptance.setTypeface(font);
        textViewAssetMenuGRN.setTypeface(font);

        textViewAssetMenuStockOnly.setTypeface(font);
        textViewAssetMenuStockAudit.setTypeface(font);
        textViewAssetMenuMigration.setTypeface(font);

        linearLayoutStockOnly = (LinearLayout) findViewById(R.id.linearLayoutStockOnly);
        linearLayoutStockAudit = findViewById(R.id.linearLayoutStockAudit);
        linearLayoutMigration = (LinearLayout) findViewById(R.id.linearLayoutMigration);

        linearLayoutMyDealers.setVisibility(View.GONE);

        textViewAssetSearch.setTypeface(font);
        textViewCross.setTypeface(font);

        linearLayoutDistStock = (LinearLayout) findViewById(R.id.linearLayoutDistStock);
        textViewAssetMenuDistStock = (TextView) findViewById(R.id.textViewAssetMenuDistStock);
        textViewAssetMenuDistStock.setTypeface(font);

        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date d = new Date();
        dayOfTheWeek = sdf.format(d);
        Log.e("dateday",dayOfTheWeek);

        Cursor cur1;
        cur1 = databaseHandler.getUserSetting();
        cur1.moveToFirst();
        userBeatModel = cur1.getString(cur1.getColumnIndex(DatabaseHandler.KEY_settingrefvalue));
        Log.e("userBeatModel",userBeatModel);

        if(userBeatModel.equalsIgnoreCase("C")){
            Log.e("userModel C","userModel C");
            Cursor curr;
            curr = databaseHandler.getDetails();
            curr.moveToFirst();
            String selectedbeat = curr.getString(curr.getColumnIndex(DatabaseHandler.KEY_selectedBeat));
            String selectedDate = curr.getString(curr.getColumnIndex(DatabaseHandler.KEY_selectedDate));
            String todayDate = new SimpleDateFormat(
                    "yyyy-MM-dd").format(new java.util.Date());
            if(todayDate.equals(selectedDate) && !selectedbeat.equals("")){
                getBeatMerchant(selectedbeat);
                /*if(netCheck()) {
                    getBeatMerchant(selectedbeat);
                }*/



            }else{
                Constants.beatassignscreen ="OutletEntry";
                Intent to = new Intent(OutletStockEntry.this, CompanyBeatActivity.class);
                startActivity(to);
                finish();
            }



        }else{
            Log.e("userModel D","userModel D");
            offlineBeat();
            /*if(netCheck()) {
                offlineBeat();
            }*/

        }
        if(Constants.setorder==1){

            Log.e("merchant name",Constants.paymentmerchname);
            autocompletemrchantlist.setText(Constants.paymentmerchname);

            setadap();
        }else{
            arraylistMerchantOrderDetailList = new ArrayList<OutletStockEntryDomain>();
        }


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String autocomplete=autocompletemrchantlist.getText().toString();
                Log.e("autocomplete",autocomplete);
                if (!autocomplete.equals("")) {


                    domainSelected = arraylistMerchantOrderDetailList.get(position);
                    companyname = arraylistMerchantOrderDetailList.get(position).getprodname();
                    fullname = arraylistMerchantOrderDetailList.get(position).getprodcode();
                    date = arraylistMerchantOrderDetailList.get(position).getCompanyname();
                    address = arraylistMerchantOrderDetailList.get(position).getDuserid();
                    prodid= arraylistMerchantOrderDetailList.get(position).getprodid();
                    merchantid = Constants.paymentmerchant_id;//Constants.paymentmerchname
                    selectedmechantname = Constants.paymentmerchname;
                    defaultuom= arraylistMerchantOrderDetailList.get(position).getUOM();

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(listView.getWindowToken(), 0);
                   /* Intent to = new Intent(OutletStockEntry.this, OutletStockEntryDetails.class);
                    startActivity(to);
                    finish();*/

                   final Dialog qtyDialog = new Dialog(OutletStockEntry.this);
                    qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    qtyDialog.setContentView(R.layout.outlet_refkey_dialog);

                    final Button buttonEntry, buttonView;

                    buttonEntry = (Button) qtyDialog
                            .findViewById(R.id.buttonEntry);
                    buttonView = (Button) qtyDialog
                            .findViewById(R.id.buttonView);

                    qtyDialog.show();

                    buttonEntry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent to = new Intent(OutletStockEntry.this, OutletStockEntryDetails.class);
                            startActivity(to);
                            finish();
                        }
                    });

                    buttonView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent to = new Intent(OutletStockEntry.this, OutletStockView.class);
                            startActivity(to);
                            finish();
                        }
                    });

                }else{
                    showAlertDialogToast("Please select store");
                }
            }
        });


        autocompletemrchantlist.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                autocompletemrchantlist.showDropDown();

                textViewcross_1.setVisibility(View.VISIBLE);

                textViewcross_1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        autocompletemrchantlist.setText("");
                        Constants.paymentmerchname = "";
                        Constants.paymentmerchant_id = "";
                        smpymntmerchantselected = 0;
                        Log.e("Merchant", Constants.paymentmerchname);
                        Log.e("selected merchant Id", Constants.paymentmerchant_id);
                        if (autocompletemrchantlist.getText().equals("")) {
                            InputMethodManager iaa = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            iaa.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                        }

                    }
                });

                return false;
            }
        });
        autocompletemrchantlist
                .setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {

                        String mnsme = autocompletemrchantlist.getText().toString();

                        for (MDealerCompDropdownDomain cd : arrayListmerchant) {

                            if (cd.getComName().equals(mnsme)) {
                                muserId = cd.getID();
                                Constants.MERCHANT_PRIMARY_KEY_FOR_PKD = cd.getMerchantRowId();//Kumaravel for new Merchant PKD
                                merchantmobileno = cd.getmobileno();
                                Constants.paymentmerchant_id = muserId;
                                smpymntmerchantselected = 1;

                                if (smpymntmerchantselected == 1) {

                                    textViewcross_1.setVisibility(View.GONE);
                                    InputMethodManager iaa = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                    iaa.hideSoftInputFromWindow(autocompletemrchantlist.getWindowToken(), 0);
                                }
                                if (!autocompletemrchantlist.getText().equals("")) {
                                    final Cursor c = databaseHandler.getOutletStockEntryData();
                                    if (c != null && c.getCount() > 0) {
                                        if (c.moveToFirst()) {
                                            String entryData = c.getString(1);
                                            getOutletStockDetail(entryData);
                                        }
                                        c.close();
                                        setadap();
                                    }else {
                                        textviewnodata.setVisibility(View.VISIBLE);
                                        listView.setVisibility(View.GONE);
                                        textviewnodata.setText("No data found");
                                    }

                                  /*  Cursor curs;
                                    curs = databaseHandler.getSetingProduct( Constants.paymentmerchant_id );
                                    Log.e("seleted Products", String.valueOf(curs.getCount()));
                                    String newproducts="";
                                    if(curs!=null && curs.getCount() > 0) {

                                        if (curs.moveToLast()) {
                                            newproducts = curs.getString(0);
                                            Log.e("newproducts", newproducts);
                                            loadingSettingProducts(newproducts);
                                        }
                                    }else{
                                        //showAlertDialogToast("No Products Available");
                                        loadingOfflineProducts();
                                    }*/
                                }

                                Constants.paymentmerchname = mnsme;
                                Log.e("Merchant", Constants.paymentmerchname);
                                Log.e("selected merchant Id", Constants.paymentmerchant_id);
                                Log.e("smobileno", merchantmobileno);



                            }
                        }


                    }
                });


        final Cursor c = databaseHandler.getOutletStockEntryData();
        if (c != null && c.getCount() > 0) {
            if (c.moveToFirst()) {
                String entryData = c.getString(1);
                getOutletStockDetail(entryData);
            }
            c.close();
            setadap();
        }else {
            textviewnodata.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
            textviewnodata.setText("No data found");
        }

        linearlayoutSearchIcon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub
                if (searchclick == "0") {
                    Log.e("size", String.valueOf(arraylistMerchantOrderDetailList.size()));
                    if(arraylistMerchantOrderDetailList.size()!=0) {
                        linearLayoutMenuParent.setVisibility(View.GONE);
                        textViewHeader.setVisibility(TextView.GONE);
                        editTextSearchField.setVisibility(EditText.VISIBLE);
                        editTextSearchField.setFocusable(true);
                        textViewcross_1.setVisibility(View.GONE);
                        editTextSearchField.requestFocus();
                        textViewCross.setVisibility(TextView.VISIBLE);
                        InputMethodManager iaa = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        iaa.hideSoftInputFromWindow(linearlayoutSearchIcon.getWindowToken(), 0);
                        InputMethodManager iss = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        iss.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                        Log.e("Keyboard", "Show");
                        searchclick = "1";
                    }
                } else {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    textViewHeader.setVisibility(TextView.VISIBLE);
                    editTextSearchField.setVisibility(EditText.GONE);
                    textViewCross.setVisibility(TextView.GONE);
                    textViewcross_1.setVisibility(View.GONE);
                    /*InputMethodManager iss = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    iss.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);*/
                    InputMethodManager iss = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    iss.hideSoftInputFromWindow(linearlayoutSearchIcon.getWindowToken(), 0);
                    searchclick = "0";
                }

            }
        });

        editTextSearchField.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

                arraylistMerchantOrderDetailList = new ArrayList<OutletStockEntryDomain>();
                String searchText = editTextSearchField.getText().toString().toLowerCase(Locale.getDefault());

//                Log.e("sizeofsearch", String.valueOf(SalesManOrderActivity.arrayListOutletStockSearchResults.size()));

                if (SalesManOrderActivity.arrayListOutletStockSearchResults != null &&
                        SalesManOrderActivity.arrayListOutletStockSearchResults.size() != 0) {

                    for (OutletStockEntryDomain pd : SalesManOrderActivity.arrayListOutletStockSearchResults) {

                        if (pd.getprodname().toLowerCase(Locale.getDefault()).contains(searchText) || pd.getprodcode().toLowerCase(Locale.getDefault()).contains(searchText)) {

                            arraylistMerchantOrderDetailList.add(pd);
                        }
                    }
                }
                OutletStockEntry.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
						if(arraylistMerchantOrderDetailList != null &&
						arraylistMerchantOrderDetailList.size() > 0){
                        adapter = new OutleyStockEntryAdapter(
                                OutletStockEntry.this,
                                R.layout.item_outlet_stock_entry,
                                arraylistMerchantOrderDetailList);
                        listView.setAdapter(adapter);							
						}
                    }
                });


            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {

            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                textViewCross.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        editTextSearchField.setText("");

                        if (editTextSearchField.equals("")) {
                            InputMethodManager iss = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            iss.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                        }
                    }
                });
            }
        });
        linearlayoutoverallayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textViewcross_1.setVisibility(View.GONE);
            }
        });

        menuIcon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (menuCliccked == 0) {

                    linearLayoutMenuParent.setVisibility(View.VISIBLE);
                    menuCliccked = 1;
                } else {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                }
                textViewHeader.setVisibility(TextView.VISIBLE);
                editTextSearchField.setVisibility(EditText.GONE);
                textViewCross.setVisibility(TextView.GONE);
                textViewcross_1.setVisibility(View.GONE);
                editTextSearchField.setText("");
                searchclick="0";
               InputMethodManager iss = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                iss.hideSoftInputFromWindow(linearlayoutSearchIcon.getWindowToken(), 0);

            }
        });

        linearLayoutMenuParent.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
            }
        });

        linearLayoutProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto="profile";
                if( netCheckwithoutAlert()==true){
                    getData();
                }else{
                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });
        linearLayoutDistanceCalculation.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                //moveto="profile";

                    Intent into=new Intent(OutletStockEntry.this, DistanceCalActivity.class);
                    startActivity(into);


            }
        });
        linearLayoutRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("Clicked", "Clicked");

                if(netCheck()){

                    final Dialog qtyDialog = new Dialog(OutletStockEntry.this);
                    qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    qtyDialog.setContentView(R.layout.warning_dialog);

                    final Button buttonUpdateOk, buttonUpdateCancel;

                    final TextView textQtyValidate;

                    buttonUpdateOk = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateOk);
                    buttonUpdateCancel = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateCancel);

                    textQtyValidate = (TextView) qtyDialog
                            .findViewById(R.id.textQtyValidate);
                    qtyDialog.show();


                    buttonUpdateOk.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            databaseHandler.synTableDelete();
                            Constants.refreshscreen = "stockentry";
                            Intent io = new Intent(OutletStockEntry.this, RefreshActivity.class);
                            startActivity(io);
                            finish();
                            qtyDialog.dismiss();
                        }

                    });

                    buttonUpdateCancel
                            .setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub

                                    qtyDialog.dismiss();

                                }
                            });

                }else{
                    showAlertDialogToast("Please Check Your internet connection");
                }
            }

        });

        linearLayoutMyOrders.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Constants.orderstatus="Success";
                Constants.checkproduct = 0;
                linearLayoutMenuParent.setVisibility(View.GONE);
                moveto="myorder";
                if(netCheckwithoutAlert()){
                    getData();
                }else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(OutletStockEntry.this,
                                SalesManOrderActivity.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });

        linearLayoutMyDealers.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                moveto="Mydealer";
                if( netCheckwithoutAlert()==true){
                    getData();
                }else{
                    showAlertDialogToast("Please Check Your internet connection");
                }



            }
        });

        linearLayoutgrn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);

                Intent io = new Intent(OutletStockEntry.this,
                        GRNActivity.class);
                startActivity(io);

            }
        });
        linearLayoutorderacceptance.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                Intent io = new Intent(OutletStockEntry.this,
                        OrderAcceptanceActivity.class);
                startActivity(io);

            }
        });

        linearLayoutCreateOrder.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                MyApplication.getInstance().setTemplate8ForTemplate2(false);

                linearLayoutMenuParent.setVisibility(View.GONE);
                moveto="createorder";
                Constants.checkproduct=0;
                Constants.orderid="";
                if( netCheckwithoutAlert()==true){
                    getData();
                }else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(OutletStockEntry.this,
                                CreateOrderActivity.class);
                        startActivity(io);
                        finish();
                    }
                }


            }
        });


        linearLayoutComplaint.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto="postnotes";
                if( netCheckwithoutAlert()==true){
                    getData();
                }else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(OutletStockEntry.this,
                                MerchantComplaintActivity.class);
                        startActivity(io);
                        finish();
                    }
                }

            }
        });

        linearLayoutClientVisit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto="clientvisit";
                if( netCheckwithoutAlert()==true){
                    getData();
                }else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(OutletStockEntry.this,
                                ClientVisitActivity.class);
                        startActivity(io);
                        finish();
                    }
                }


            }
        });
        linearLayoutPaymentCollection.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto="paymentcoll";
                if( netCheckwithoutAlert()==true){
                    getData();
                }else{
                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });


        linearLayoutStockEntry.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;

            }
        });
        linearLayoutAcknowledge.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto = "acknowledge";
                if( netCheckwithoutAlert()==true){
                    getData();
                }else{

                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });

        linearLayoutAddMerchant.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto = "addmerchant";
                if( netCheckwithoutAlert()==true){
                    getData();
                }else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(OutletStockEntry.this,
                                AddMerchantNew.class);
                        startActivity(io);
                        finish();
                    }
                }

            }
        });

        linearLayoutdownloadscheme.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(netCheck()){

                    final Dialog qtyDialog = new Dialog(OutletStockEntry.this);
                    qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    qtyDialog.setContentView(R.layout.warning_dialog);

                    final Button buttonUpdateOk, buttonUpdateCancel;

                    final TextView textQtyValidate;

                    buttonUpdateOk = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateOk);
                    buttonUpdateCancel = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateCancel);

                    textQtyValidate = (TextView) qtyDialog
                            .findViewById(R.id.textQtyValidate);
                    qtyDialog.show();


                    buttonUpdateOk.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            databaseHandler.deletescheme();
                            Constants.downloadScheme = "stockentry";
                            Intent io = new Intent(OutletStockEntry.this, SchemeDownload.class);
                            startActivity(io);
                            finish();
                            qtyDialog.dismiss();
                        }

                    });

                    buttonUpdateCancel
                            .setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub

                                    qtyDialog.dismiss();

                                }
                            });

                }else{
                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });
        linearLayoutsurveyuser.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //if (netCheck() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    moveto="surveyuser";
                    if(netCheckwithoutAlert()){
                        getData();
                    }else {
                        if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                            showAlertDialogToast("Please make payment before place order");
                        } else {
                            Intent io = new Intent(OutletStockEntry.this,
                                    SurveyMerchant.class);
                            startActivity(io);
                            finish();
                        }
                    }


            }
        });
        linearLayoutDistStock.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheck()) {
                    moveto="diststock";
                    getData();
                }else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment");
                    } else {
                        Intent io = new Intent(OutletStockEntry.this,
                                DistributrStockActivity.class);
                        startActivity(io);
                        finish();
                    }
                }

            }
        });

        linearLayoutMyDayPlan.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto="mydayplan";
                if (netCheckwithoutAlert()) {
                    getData();
                }else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(OutletStockEntry.this,
                                MyDayPlan.class);
                        startActivity(io);
                        finish();
                    }
                }

            }
        });

        linearLayoutMySales.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    moveto="mysales";
                    getData();
                }else {
                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });

        linearLayoutSignout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                // TODO Auto-generated method stub
                databaseHandler.delete();
                Intent io = new Intent(OutletStockEntry.this, SigninActivity.class);
                startActivity(io);
                finish();
            }
        });

        linearLayoutMigration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Intent io = new Intent(OutletStockEntry.this,
                        PendingDataMigrationActivity.class);
                startActivity(io);
            }
        });

        linearLayoutStockOnly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Constants.checkproduct = 0;
                Constants.orderid = "";
                Constants.SalesMerchant_Id = "";
                Constants.Merchantname = "";
                MyApplication.getInstance().setTemplate8ForTemplate2(true);
                if (netCheck()) {
                    Intent io = new Intent(OutletStockEntry.this,
                            ClosingStockDashBoardActivity.class);
                    startActivity(io);
                    finish();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals("") || PaymentStatus.equalsIgnoreCase("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(OutletStockEntry.this,
                                ClosingStockDashBoardActivity.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });

        linearLayoutStockAudit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().setTemplate8ForTemplate2(false);
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Constants.checkproduct = 0;
                Constants.orderid = "";
                Constants.SalesMerchant_Id = "";
                Constants.Merchantname = "";
                if (netCheck()) {
                    Intent io = new Intent(OutletStockEntry.this,
                            ClosingStockAudit.class);
                    startActivity(io);
                    finish();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals("") || PaymentStatus.equalsIgnoreCase("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(OutletStockEntry.this,
                                ClosingStockAudit.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });

        //////////////
        showMenu();
        ///////////////

	}
    public void getBeatMerchant(String beatname){
        Cursor cursor;
        cursor = databaseHandler.getbeatMerchant(beatname);
        String merchants="";
        int i=0;
        if(cursor!=null && cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {

                do{
                    if(i==0){
                        if(cursor.getString(0).equals("New User")){
                            merchants = "'"+cursor.getString(0)+"'";
                        }else{
                            merchants = cursor.getString(0);
                        }

                        i++;
                    }else{
                        if(cursor.getString(0).equals("New User")){
                            merchants = merchants+", '"+cursor.getString(0)+"'";
                        }else{
                            merchants = merchants+","+cursor.getString(0);
                        }

                    }




                } while (cursor.moveToNext());


            }
        }

        offlinemerchant(merchants);
    }

    private void getData() {
        if (moveto.equals("myorder")) {

            Intent io = new Intent(OutletStockEntry.this,
                    SalesManOrderActivity.class);
            startActivity(io);
            finish();

        }else if(moveto.equals("createorder")){
            Intent io = new Intent(OutletStockEntry.this,
                    CreateOrderActivity.class);
            startActivity(io);
            finish();
        }else if(moveto.equals("Mydealer")){
            Intent io = new Intent(OutletStockEntry.this,
                    MyDealersActivity.class);
            io.putExtra("Key","MenuClick");
            startActivity(io);
            finish();
        }
        else if(moveto.equals("profile")){
            Intent io = new Intent(OutletStockEntry.this,
                    ProfileActivity.class);
            startActivity(io);
            finish();
        }
        else if(moveto.equals("postnotes")){
            Intent io = new Intent(OutletStockEntry.this,
                    MerchantComplaintActivity.class);
            startActivity(io);
            finish();
        }
        else if(moveto.equals("clientvisit")){
            Intent io = new Intent(OutletStockEntry.this,
                    SMClientVisitHistory.class);
            startActivity(io);
            finish();
        }else if(moveto.equals("addmerchant")){
            Intent io = new Intent(OutletStockEntry.this, AddMerchantNew.class);
            startActivity(io);
            finish();
        }
        else if(moveto.equals("acknowledge")){
            Intent io = new Intent(OutletStockEntry.this, SalesmanAcknowledgeActivity.class);
            startActivity(io);
            finish();
        }
        else if(moveto.equals("paymentcoll")){
            Intent io = new Intent(OutletStockEntry.this, SalesmanPaymentCollectionActivity.class);
            startActivity(io);
            finish();
        }

        else if(moveto.equals("surveyuser")){
            Intent io = new Intent(OutletStockEntry.this, SurveyMerchant.class);
            startActivity(io);
            finish();
        }
        else if(moveto.equals("mydayplan")){
            Intent io = new Intent(OutletStockEntry.this, MyDayPlan.class);
            startActivity(io);
            finish();
        }
        else if(moveto.equals("mysales")){
            Intent io = new Intent(OutletStockEntry.this, MySalesActivity.class);
            startActivity(io);
            finish();
        } else if (moveto.equals("diststock")) {
            Intent io = new Intent(OutletStockEntry.this,
                    DistributrStockActivity.class);
            startActivity(io);
            finish();
        }
    }
    public void offlineBeat(){
        Cursor curs;
        curs = databaseHandler.getbeatwithMerchant(dayOfTheWeek);
        Log.e("beatCount", String.valueOf(curs.getCount()));

        int i=0;

        if(curs!=null && curs.getCount() > 0) {
            if (curs.moveToFirst()) {

                do {
                    Log.e("beat inside", "beat inside");
                    if(i==0){
                        if(curs.getString(0).equals("New User")){
                            beatMerchants="'"+curs.getString(0)+"'";
                        }else{
                            beatMerchants=curs.getString(0);
                        }


                        i++;
                    }else{
                        if(curs.getString(0).equals("New User")){
                            beatMerchants=beatMerchants+","+ "'"+curs.getString(0)+"'";
                        }else{
                            beatMerchants=beatMerchants+","+ curs.getString(0);
                        }


                    }
                    Log.e("beatMerchants", beatMerchants);


                } while (curs.moveToNext());


            }
            offlinemerchant(beatMerchants);
        }else{
            showAlertDialogToast("No Merchants available for this day");
        }

    }

    public void offlinemerchant(String beatMerchants)  {


        Cursor curs;
        curs = databaseHandler.getmerchantoncurrentday(beatMerchants);
        Log.e("merchantCount", String.valueOf(curs.getCount()));

        if(curs!=null && curs.getCount() > 0) {
            if (curs.moveToFirst()) {

                do{
                    Log.e("inside", "inside");
                    MDealerCompDropdownDomain cd = new MDealerCompDropdownDomain();
                    cd.setComName(curs.getString(1));
                    cd.setID(String.valueOf(curs.getString(2)));
                    cd.setName(curs.getString(3));
                    cd.setmobileno(curs.getString(4));

                    cd.setMerchantRowId(curs.getInt(0));///Kumaravel for New Merchant

                    arrayListDealerDomain.add(cd);
                    arrayListSearch.add(cd);
                    arrayListDealer.add(curs.getString(1));
                    arrayListmerchant.add(cd);

                } while (curs.moveToNext());


            }
            Log.e("outside", "outside");
        }else{

        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                OutletStockEntry.this, R.layout.textview, R.id.textView,
                arrayListDealer);

        autocompletemrchantlist.setAdapter(adapter);

        listView.setVisibility(View.GONE);
        textviewnodata.setVisibility(View.VISIBLE);
        textviewnodata.setText("No Outlet Product(s)");


    }

   /* public void loadingOfflineProducts(){
        arraylistMerchantOrderDetailList = new ArrayList<OutletStockEntryDomain>();
        SalesManOrderActivity.arrayListSearchResults= new ArrayList<OutletStockEntryDomain>();
        Cursor curs;
        curs = databaseHandler.getRemarks();
        //curs.moveToFirst();
        Log.e("productCount", String.valueOf(curs.getCount()));

        if(curs!=null && curs.getCount() > 0) {
            if (curs.moveToFirst()) {

                do{
                    Log.e("inside", "inside");
                    OutletStockEntryDomain dod = new OutletStockEntryDomain();
                    dod.setDuserid(String.valueOf(curs.getInt(1))); //(curs.getColumnIndex(DatabaseHandler.KEY_userid)))
                    dod.setCompanyname(curs.getString(2)); //(curs.getColumnIndex(DatabaseHandler.KEY_companyname))
                    dod.setprodcode(curs.getString(3)); //(curs.getColumnIndex(DatabaseHandler.KEY_prodcode))
                    dod.setprodid(curs.getString(4)); //String.valueOf(curs.getInt(curs.getColumnIndex(DatabaseHandler.KEY_prodid)))
                    dod.setprodname(curs.getString(5)); //curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_prodname))
                    dod.setqty(curs.getString(6)); //curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_quantity))
                    dod.setFreeQty(curs.getString(7)); //curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_freeqty))
                    dod.setDate(curs.getString(8)); //curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_updateddt))
                    //dod.setFreeQty(curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_currentdate)));
                    dod.setprodprice(curs.getString(10));
                    dod.setprodtax(curs.getString(11));
                    dod.setserialno(String.valueOf(q + 1));
                    dod.setBatchno(curs.getString(12));
                    dod.setExpdate(curs.getString(14));
                    dod.setMgfDate(curs.getString(13));
                    dod.setUOM(curs.getString(15));

                    Log.e("id", curs.getString(0));
                    Log.e("userid", curs.getString(1));
                    Log.e("cmpnyname", curs.getString(2));
                    Log.e("productcode", curs.getString(3));
                    Log.e("product id", curs.getString(4));
                    Log.e("product name", curs.getString(5));
                    Log.e("qty", curs.getString(6));
                    Log.e("fqty", curs.getString(7));
                    Log.e("date", curs.getString(8));
                    Log.e("price", curs.getString(10));
                    Log.e("tax", curs.getString(11));

                    arraylistMerchantOrderDetailList.add(dod);
                    SalesManOrderActivity.arrayListSearchResults.add(dod);
                    q++;
                } while (curs.moveToNext());


            }
            Log.e("outside", "outside");
        }
        Log.e("size", String.valueOf(arraylistMerchantOrderDetailList.size()));


        setadap();
    }


    public void loadingSettingProducts(String prodid){
        arraylistMerchantOrderDetailList = new ArrayList<OutletStockEntryDomain>();
        SalesManOrderActivity.arrayListSearchResults= new ArrayList<OutletStockEntryDomain>();

        Cursor curs;
        curs = databaseHandler.getFilterProduct(prodid);
        //curs.moveToFirst();
        Log.e("SelectedproductCount", String.valueOf(curs.getCount()));

        if(curs!=null && curs.getCount() > 0) {
            if (curs.moveToFirst()) {

                do{
                    Log.e("inside", "inside");
                    OutletStockEntryDomain dod = new OutletStockEntryDomain();
                    dod.setDuserid(String.valueOf(curs.getInt(1))); //(curs.getColumnIndex(DatabaseHandler.KEY_userid)))
                    dod.setCompanyname(curs.getString(2)); //(curs.getColumnIndex(DatabaseHandler.KEY_companyname))
                    dod.setprodcode(curs.getString(3)); //(curs.getColumnIndex(DatabaseHandler.KEY_prodcode))
                    dod.setprodid(curs.getString(4)); //String.valueOf(curs.getInt(curs.getColumnIndex(DatabaseHandler.KEY_prodid)))
                    dod.setprodname(curs.getString(5)); //curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_prodname))
                    dod.setqty(curs.getString(6)); //curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_quantity))
                    dod.setFreeQty(curs.getString(7)); //curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_freeqty))
                    dod.setDate(curs.getString(8)); //curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_updateddt))
                    //dod.setFreeQty(curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_currentdate)));
                    dod.setprodprice(curs.getString(10));
                    dod.setprodtax(curs.getString(11));
                    dod.setserialno(String.valueOf(q + 1));
                    dod.setBatchno(curs.getString(12));
                    dod.setExpdate(curs.getString(14));
                    dod.setMgfDate(curs.getString(13));
                    dod.setUOM(curs.getString(15));

                    Log.e("id", curs.getString(0));
                    Log.e("userid", curs.getString(1));
                    Log.e("cmpnyname", curs.getString(2));
                    Log.e("productcode", curs.getString(3));
                    Log.e("product id", curs.getString(4));
                    Log.e("product name", curs.getString(5));
                    Log.e("qty", curs.getString(6));
                    Log.e("fqty", curs.getString(7));
                    Log.e("date", curs.getString(8));
                    Log.e("price", curs.getString(10));
                    Log.e("tax", curs.getString(11));

                    arraylistMerchantOrderDetailList.add(dod);
                    SalesManOrderActivity.arrayListSearchResults.add(dod);
                    q++;
                } while (curs.moveToNext());


            }
            setadap();
            Log.e("outside", "outside");
        }else{
            textviewnodata.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
            textviewnodata.setText("No data found");
        }
        Log.e("size", String.valueOf(arraylistMerchantOrderDetailList.size()));



    }*/

    /*public  void OfflineProducts(){
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "",strMsg="";



            @Override
            protected void onPreExecute() {

                dialog = ProgressDialog.show(OutletStockEntry.this, "",
                        "Loading ...", true,false);

            }


            @Override
            protected void onPostExecute(Void result) {

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {

                        JSONArray job1 = JsonAccountObject.getJSONArray("data");
                        arraylistMerchantOrderDetailList = new ArrayList<OutletStockEntryDomain>();
                       SalesManOrderActivity.arrayListSearchResults= new ArrayList<OutletStockEntryDomain>();
                        for (int i = 0; i < job1.length(); i++) {

                            JSONObject job = new JSONObject();
                            try {
                                job = job1.getJSONObject(i);
                                OutletStockEntryDomain dod = new OutletStockEntryDomain();
                                dod.setDuserid(job.getString("userid"));
                                dod.setCompanyname( job.getString("companyname"));
                                dod.setprodcode(job.getString("prodcode"));
                                dod.setprodid(job.getString("prodid"));
                                dod.setprodname( job.getString("prodname"));
                                dod.setqty("");
                                dod.setFreeQty("");
                                dod.setDate(job.getString("updateddt"));
                                dod.setprodprice(job.getString("prodprice"));
                                dod.setprodtax(job.getString("prodtax"));
                                dod.setserialno(String.valueOf(i + 1));
                                dod.setBatchno(""); //job.getString("batchno")
                                dod.setExpdate(""); //job.getString("expirydt")
                                dod.setMgfDate(""); //job.getString("manufdt")
                                dod.setUOM(job.getString("uom"));

                                arraylistMerchantOrderDetailList.add(dod);
                                SalesManOrderActivity.arrayListSearchResults.add(dod);
                            }catch (JSONException e) {
                                e.printStackTrace();}
                            catch (Exception e) {

                            }

                        }
                        setadap();
                    }else{
                        textviewnodata.setVisibility(View.VISIBLE);
                        listView.setVisibility(View.GONE);
                        textviewnodata.setText("No data found");
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {

                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("userid", Constants.USER_ID);
                    jsonObject.put("status","Active");

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                JsonServiceHandler = new JsonServiceHandler(Utils.strofflineproduct, jsonObject.toString(), OutletStockEntry.this);
                JsonAccountObject = JsonServiceHandler.ServiceData();

                return null;

            }

        }.execute(new Void[]{});

    }*/

    private void getOutletStockDetail(final String data){
        if(TextUtils.isEmpty(data))
            return;

        JSONObject outletObj = null;
        try {
            outletObj = new JSONObject(data);
            JSONArray job1 = outletObj.optJSONArray("data");
            arraylistMerchantOrderDetailList = new ArrayList<>();
            for (int i = 0; i < job1.length(); i++) {
                JSONObject job = job1.getJSONObject(i);
                OutletStockEntryDomain dod = new OutletStockEntryDomain();
                dod.setDuserid(job.getString("userid"));
                dod.setCompanyname(job.getString("companyname"));
                dod.setprodcode(job.getString("prodcode"));
                dod.setprodid(job.getString("prodid"));
                dod.setprodname(job.getString("prodname"));
                dod.setqty("");
                dod.setFreeQty("");
                dod.setDate(job.getString("updateddt"));
                dod.setprodprice(job.getString("prodprice"));
                dod.setprodtax(job.getString("prodtax"));
                dod.setserialno(String.valueOf(i + 1));
                dod.setBatchno(""); //job.getString("batchno")
                dod.setExpdate(""); //job.getString("expirydt")
                dod.setMgfDate(""); //job.getString("manufdt")
                dod.setUOM(job.getString("uom"));
                arraylistMerchantOrderDetailList.add(dod);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

	public void setadap() {
		// TODO Auto-generated method stub

        Log.e("size", String.valueOf(arraylistMerchantOrderDetailList.size()));
        Log.e("adapter","adapter");
		adapter = new OutleyStockEntryAdapter(OutletStockEntry.this,
				R.layout.item_outlet_stock_entry, arraylistMerchantOrderDetailList);
        textviewnodata.setVisibility(View.GONE);
        listView.setVisibility(View.VISIBLE);
        textViewAssetSearch.setVisibility(View.VISIBLE);
        listView.setAdapter(adapter);

        Log.e("outside", "outside");
        Constants.setorder=0;
	}

    public boolean netCheckwithoutAlert() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            }

            else if (result == null) {

                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
    public void showAlertDialogToast1( String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                       // myOrders();
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }
    public void showAlertDialogToast( String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                        InputMethodManager inn = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                        inn.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

	public boolean netCheck() {
		// for network connection
		try {
			ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetwork = connectionManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			Object result = null;
			if (mWifi.isConnected() || mNetwork.isConnected()) {
				return true;
			}

			else if (result == null) {
               /* showAlertDialog(SalesManOrderActivity.this,
                        "No Internet Connection",
                        "Please Check Your internet connection.", false);*/
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

    public void onBackPressed() {
        exitAlret();
    }
    private void exitAlret() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        localBuilder.setMessage("Do you want to Exit?");
        localBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {

                        finish();

                    }
                });
        localBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {
                        paramDialogInterface.cancel();
                    }
                });
        localBuilder.create().show();
    }
}
