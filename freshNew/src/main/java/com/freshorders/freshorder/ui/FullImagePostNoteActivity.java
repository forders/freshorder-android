package com.freshorders.freshorder.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.utils.Constants;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by ragul on 30/09/16.
 */
public class FullImagePostNoteActivity extends Activity {

    ImageView imagepostnote;
    Bitmap bitmap;
    LinearLayout linearLayoutBack;
    TextView textViewAssetMenu,textviewdeletemenu;
    String path,url;
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.full_image_post_note);
        imagepostnote=(ImageView) findViewById(R.id.imagepostnote);

        linearLayoutBack=(LinearLayout) findViewById(R.id.linearLayoutBack);
        textViewAssetMenu=(TextView) findViewById(R.id.textViewAssetMenu);
        textviewdeletemenu=(TextView) findViewById(R.id.textviewdeletemenu);
        Typeface font = Typeface.createFromAsset(getAssets(),
                "fontawesome-webfont.ttf");
        textViewAssetMenu.setTypeface(font);
        textviewdeletemenu.setTypeface(font);
        Intent io = getIntent();
        path = io.getExtras().getString("id");

        Log.e("image path",path);


        new DownloadImage().execute(path);
       /* Uri uri = Uri.parse(path);
        Log.e("image path",path);
        new DownloadImage().execute(uri);*/
        linearLayoutBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent to = new Intent(FullImagePostNoteActivity.this, MerchantComplaintActivity.class);

                startActivity(to);
                FullImagePostNoteActivity.this.finish();

            }
        });
        textviewdeletemenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int imagesize = MerchantOrderActivity.arraylistPdoductImages.size();
                if (imagesize == 1) {
                    showAlertDialogToast("Have Only One Image, Can't delete!!!");

                } else {

                    Intent to = new Intent(FullImagePostNoteActivity.this, MerchantComplaintActivity.class);
                    Constants.setimage=null;
                    startActivity(to);
                    finish();
                }
            }
        });
    }
    private class DownloadImage extends AsyncTask<String, Void, Bitmap> {
        ProgressDialog dialog;
        @Override
        protected Bitmap doInBackground(String... urls) {

            Bitmap bitmap = null;
            Bitmap newBitmap = null;
            Uri uri = null;

            try {

                uri = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "" + urls[0]);

                bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(uri));


            } catch (IOException e) {

                cancel(true);
            }

            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {

            imagepostnote.setImageBitmap(result);
            dialog.dismiss();
        }

        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show(FullImagePostNoteActivity.this, "",
                    "Loading...", true, true);
            dialog.setCancelable(false);

        }
    }
    public void showAlertDialogToast( String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent io=new Intent(FullImagePostNoteActivity.this,MerchantComplaintActivity.class);
                startActivity(io);
                finish();
            }
        });
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    public void onBackPressed() {
        exitAlret();
    }

    private void exitAlret() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        localBuilder.setMessage("Do you want to Exit?");
        localBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {

                        finish();

                    }
                });
        localBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {
                        paramDialogInterface.cancel();
                    }
                });
        localBuilder.create().show();
    }
}
