package com.freshorders.freshorder.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.AppPaymentHsitoryAdapter;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.AppPaymentHistoryDomain;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by ragul on 03/10/16.
 */
public class AppPaymentHistoryActivity extends Activity {


    TextView textViewdate,textViewAmount,textViewpayStatus,textViewfullName,textViewName,textViewemail;

    JSONObject JsonAccountObject = null;
    JSONArray JsonAccountArray = null;
    DatabaseHandler databaseHandler;
    JsonServiceHandler JsonServiceHandler;

    TextView menuIcon, textViewAssetMenuProfile, textViewAssetMenuMyOrders,
            textViewAssetMenuMyDealers, textViewAssetMenuProducts,
            textViewAssetMenuPayment, textViewAssetMenuComplaint,
            textViewAssetMenuSignout,textViewAssetMenuCreateOrder;

    LinearLayout linearLayoutMenuParent, linearLayoutProfile,
            linearLayoutMyOrders, linearLayoutMyDealers, linearLayoutProducts,
            linearLayoutPayment, linearLayoutComplaint, linearLayoutSignout,
            linearLayoutCreateOrder,linearLayoutBack,linearLayoutButton,linearLayoutHeading;
    Date dateStr = null;
    public static String time;
    ListView listViewOrders;
    int menuCliccked;

    ArrayList<AppPaymentHistoryDomain> arraylistpaymethistory;
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.app_payment_history);
        databaseHandler = new DatabaseHandler(getApplicationContext());
        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();
        textViewemail = (TextView) findViewById(R.id.textviewemail);
        textViewAmount = (TextView) findViewById(R.id.textViewAmount);
        textViewdate = (TextView) findViewById(R.id.textViewdate);
        textViewpayStatus = (TextView) findViewById(R.id.textViewpayStatus);
        textViewfullName = (TextView) findViewById(R.id.textViewfullName);


        paymenthistory();
        menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
        listViewOrders = (ListView) findViewById(R.id.listViewOrders);
        linearLayoutCreateOrder = (LinearLayout) findViewById(R.id.linearLayoutCreateOrder);
        linearLayoutProfile = (LinearLayout) findViewById(R.id.linearLayoutProfile);
        linearLayoutMyOrders = (LinearLayout) findViewById(R.id.linearLayoutMyOrders);
        linearLayoutMyDealers = (LinearLayout) findViewById(R.id.linearLayoutMyDealers);
        linearLayoutProducts = (LinearLayout) findViewById(R.id.linearLayoutProducts);
        linearLayoutPayment = (LinearLayout) findViewById(R.id.linearLayoutPayment);
        linearLayoutComplaint = (LinearLayout) findViewById(R.id.linearLayoutComplaint);
        linearLayoutSignout = (LinearLayout) findViewById(R.id.linearLayoutSignout);
        linearLayoutBack = (LinearLayout) findViewById(R.id.linearLayoutBack);
        linearLayoutButton= (LinearLayout) findViewById(R.id.linearLayoutButton);
        linearLayoutHeading= (LinearLayout) findViewById(R.id.linearLayoutHeading);


        textViewAssetMenuCreateOrder= (TextView) findViewById(R.id.textViewAssetMenuCreateOrder);
        textViewAssetMenuProfile = (TextView) findViewById(R.id.textViewAssetMenuProfile);
        textViewAssetMenuMyOrders = (TextView) findViewById(R.id.textViewAssetMenuMyOrders);
        textViewAssetMenuMyDealers = (TextView) findViewById(R.id.textViewAssetMenuMyDealers);
        textViewAssetMenuProducts = (TextView) findViewById(R.id.textViewAssetMenuProducts);
        textViewAssetMenuPayment = (TextView) findViewById(R.id.textViewAssetMenuPayment);
        textViewAssetMenuComplaint = (TextView) findViewById(R.id.textViewAssetMenuComplaint);
        textViewAssetMenuSignout = (TextView) findViewById(R.id.textViewAssetMenuSignout);
        linearLayoutMenuParent = (LinearLayout) findViewById(R.id.linearLayoutMenuParent);


        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();
        Typeface font = Typeface.createFromAsset(getAssets(),
                "fontawesome-webfont.ttf");
        menuIcon.setTypeface(font);

        textViewAssetMenuCreateOrder.setTypeface(font);
        textViewAssetMenuProfile.setTypeface(font);

        textViewAssetMenuMyOrders.setTypeface(font);
        textViewAssetMenuMyDealers.setTypeface(font);
        textViewAssetMenuProducts.setTypeface(font);
        textViewAssetMenuPayment.setTypeface(font);
        textViewAssetMenuComplaint.setTypeface(font);
        textViewAssetMenuSignout.setTypeface(font);

        Cursor cur;
        cur = databaseHandler.getDetails();
        cur.moveToFirst();
        Log.e("Insertion Check",
                cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_username)));
        Log.e("Insertion Check",
                cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_password)));
        Log.e("Insertion Check",
                cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_usertype)));


        Constants.USER_FULLNAME = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_name));
        Constants.USER_EMAIL = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_email));

        textViewfullName.setText(Constants.USER_FULLNAME);
        textViewemail.setText(Constants.USER_EMAIL);

        linearLayoutBack.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Constants.adapterset = 0;

                if(Constants.USER_TYPE.equals("D")) {
                    Intent io = new Intent(AppPaymentHistoryActivity.this, DealerPaymentActivity.class);
                    startActivity(io);
                    finish();
                }else{
                    Intent io = new Intent(AppPaymentHistoryActivity.this, PaymentActivity.class);
                    startActivity(io);
                    finish();
                }
            }
        });
        linearLayoutMenuParent.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
            }
        });

        linearLayoutProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Intent io = new Intent(AppPaymentHistoryActivity.this,
                        ProfileActivity.class);
                startActivity(io);
                finish();

            }
        });

        linearLayoutMyOrders.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Constants.checkproduct = 0;
                Intent io = new Intent(AppPaymentHistoryActivity.this,
                        MerchantOrderActivity.class);
                startActivity(io);
                finish();

            }
        });

        linearLayoutMyDealers.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                // TODO Auto-generated method stub
                Intent io = new Intent(AppPaymentHistoryActivity.this,
                        MyDealersActivity.class);
                io.putExtra("Key", "menuclick");
                startActivity(io);
                finish();
            }
        });

        linearLayoutProducts.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                // TODO Auto-generated method stub
                Intent io = new Intent(AppPaymentHistoryActivity.this,
                        ProductActivity.class);
                startActivity(io);
                finish();
            }
        });

        linearLayoutCreateOrder.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                // TODO Auto-generated method stub
                Intent io = new Intent(AppPaymentHistoryActivity.this,
                        CreateOrderActivity.class);
                startActivity(io);
                finish();
            }
        });

        linearLayoutPayment.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                 linearLayoutMenuParent.setVisibility(View.GONE);
                 menuCliccked = 0;
                 Intent io = new Intent(AppPaymentHistoryActivity.this,
                        PaymentActivity.class);
                 startActivity(io);
                 finish();
            }
        });

        linearLayoutComplaint.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (Constants.USER_TYPE.equals("D")) {
                    Intent io = new Intent(AppPaymentHistoryActivity.this,
                            DealersComplaintActivity.class);
                    startActivity(io);
                    finish();
                } else {
                    Intent io = new Intent(AppPaymentHistoryActivity.this,
                            MerchantComplaintActivity.class);
                    startActivity(io);
                    finish();
                }

            }
        });

        linearLayoutSignout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                // TODO Auto-generated method stub
                databaseHandler.delete();
                Intent io = new Intent(AppPaymentHistoryActivity.this,
                        SigninActivity.class);
                startActivity(io);
                finish();
            }
        });
    }
    public void paymenthistory(){
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "";

            @Override
            protected void onPreExecute() {
                dialog= ProgressDialog.show(AppPaymentHistoryActivity.this,"",
                        "Loading...", true, true);

            }

            @Override
            protected void onPostExecute(Void result) {

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {

                        JSONArray job1 = JsonAccountObject.getJSONArray("data");
                        arraylistpaymethistory=new ArrayList<AppPaymentHistoryDomain>();
                        for (int i = 0; i < job1.length(); i++) {

                            try{

                                JSONObject job = new JSONObject();
                                job=job1.getJSONObject(i);
                                AppPaymentHistoryDomain dod=new AppPaymentHistoryDomain();

                                dod.setpymntid(job.getString("pymntid"));
                                dod.setpymtamount(job.getString("pymtamount"));
                                dod.setpymtstatus(job.getString("pymtstatus"));
                                Log.e("paymentid", job.getString("pymntid"));

                                String[] date1=job.getString("pymtdate").split("T");

                                time = date1[0];
                                String inputPattern = "yyyy-MM-dd";
                                String outputPattern = "dd-MMM-yyyy ";
                                SimpleDateFormat inputFormat = new SimpleDateFormat(
                                        inputPattern);
                                SimpleDateFormat outputFormat = new SimpleDateFormat(
                                        outputPattern);

                                String str = null;

                                try {
                                    dateStr = inputFormat.parse(time);
                                    str = outputFormat.format(dateStr);
                                    dod.setpymtdate(str);
                                    // Log.e("str", str);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                 if(Constants.USER_TYPE.equals("D")){

                         arraylistpaymethistory.add(dod);
                    }else{

                 arraylistpaymethistory.add(dod);
                        }
                               // MerchantOrderActivity.arraypaymenthistory.add(dod);
                            }catch (Exception e){

                            }

                        }
                        setpaymentadapter();
                    }else{
                        showAlertDialogToast(strMsg);


                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {

                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();


                JsonServiceHandler = new JsonServiceHandler(Utils.strGetPaymentHsitory+Constants.USER_ID, AppPaymentHistoryActivity.this);
                JsonAccountObject = JsonServiceHandler.ServiceDataGet();
                return null;
            }
        }.execute(new Void[] {});
    }
    public void setpaymentadapter(){


        if(Constants.USER_TYPE.equals("D")){

            AppPaymentHsitoryAdapter adapter=new AppPaymentHsitoryAdapter(AppPaymentHistoryActivity.this, R.layout.item_payment_history, arraylistpaymethistory);
            listViewOrders.setAdapter(adapter);
        }else{

            AppPaymentHsitoryAdapter adapter=new AppPaymentHsitoryAdapter(AppPaymentHistoryActivity.this, R.layout.item_payment_history, arraylistpaymethistory);
            listViewOrders.setAdapter(adapter);
        }



    }
        protected void toastDisplay(String msg) {
        Toast toast = Toast.makeText(AppPaymentHistoryActivity.this, msg,
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();

    }

    public boolean netCheck() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            }

            else if (result == null) {
                showAlertDialog(AppPaymentHistoryActivity.this,
                        "No Internet Connection",
                        "Please Check Your internet connection.", false);
                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public void showAlertDialog(Context context, String title, String message,
                                Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        alertDialog.show();
    }

    public void showAlertDialogToast( String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    public void onBackPressed() {
        exitAlret();
    }

    private void exitAlret() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        localBuilder.setMessage("Do you want to Exit?");
        localBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {

                        finish();

                    }
                });
        localBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {
                        paramDialogInterface.cancel();
                    }
                });
        localBuilder.create().show();
    }
}