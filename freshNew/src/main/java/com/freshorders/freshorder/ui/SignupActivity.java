package com.freshorders.freshorder.ui;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.freshorders.freshorder.AlertDialogManager;
import com.freshorders.freshorder.ConnectionDetector;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.ServerUtilities;
import com.freshorders.freshorder.WakeLocker;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;
import com.google.android.gcm.GCMRegistrar;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.animation.LinearInterpolator;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import static com.freshorders.freshorder.CommonUtilities.DISPLAY_MESSAGE_ACTION;
import static com.freshorders.freshorder.CommonUtilities.EXTRA_MESSAGE;
import static com.freshorders.freshorder.CommonUtilities.SENDER_ID;
import static com.freshorders.freshorder.CommonUtilities.SERVER_URL;

public class SignupActivity extends Activity {
	Button buttonSignUp;
	Spinner dropdown;
	EditText editTextUserName,editTextPassword;
	static EditText editTextEnterOTP;
	TextView textViewTimer,textViewTimerText,textViewAssetMenu;
	String Username,Password,usertype;
	ProgressBar mProgressBar;
	ObjectAnimator progressAnimator;
	JSONObject JsonAccountObject = null;
	JSONArray JsonAccountArray = null;
	DatabaseHandler databaseHandler;
	JsonServiceHandler JsonServiceHandler;
	Button buttonOTPOK, buttonOTPResend;
	public static String strOTP;
	String strmobile;
	Dialog dialogOtp;
	public static String strID;
	public static String name;
	public static String email;
	ConnectionDetector cd;
	AlertDialogManager alert = new AlertDialogManager();
	AsyncTask<Void, Void, Void> mRegisterTask;
	public static String strMsg = "",strStatus1="";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(1);
		setContentView(R.layout.signup_screen);
		
		buttonSignUp = (Button)findViewById(R.id.buttonSignUp);
		editTextUserName = (EditText)findViewById(R.id.editTextUserName);
		editTextPassword = (EditText)findViewById(R.id.editTextPassword);
		textViewAssetMenu=(TextView) findViewById(R.id.textViewAssetMenu);
		mProgressBar = (ProgressBar) findViewById(R.id.progressBar1);
		progressAnimator = ObjectAnimator.ofInt(mProgressBar, "progress", 0,
				1000);
		
		databaseHandler = new DatabaseHandler(getApplicationContext());
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();
		dropdown = (Spinner)findViewById(R.id.spinnerPayment);
		String[] items = new String[]{"Merchant", "Dealer/Distributor/Admin"};//"Salesman/QC"
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items);
		dropdown.setAdapter(adapter);
		
		dialogOtp = new Dialog(SignupActivity.this);
		dialogOtp.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialogOtp.setContentView(R.layout.otp_registration_screen);
		editTextEnterOTP = (EditText) dialogOtp
				.findViewById(R.id.editTextEnterOTP);

		buttonOTPOK = (Button) dialogOtp.findViewById(R.id.buttonOTPOK);
		textViewTimer = (TextView) dialogOtp.findViewById(R.id.textViewTimer);
		textViewTimerText= (TextView) dialogOtp.findViewById(R.id.textViewTimerText);
		buttonOTPResend = (Button) dialogOtp.findViewById(R.id.buttonOTPResend);
		strOTP = editTextEnterOTP.getText().toString();
		Typeface font = Typeface.createFromAsset(getAssets(),
				"fontawesome-webfont.ttf");
		textViewAssetMenu.setTypeface(font);
		netCheck();
		textViewAssetMenu.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent io=new Intent(SignupActivity.this,SigninActivity.class);
				startActivity(io);
				finish();
			}
		});
		buttonOTPOK.setOnClickListener(new OnClickListener() {

			@SuppressLint("NewApi")
			@Override
			public void onClick(View v) {

				strOTP = editTextEnterOTP.getText().toString();
				if (!strOTP.isEmpty() && !strOTP.startsWith(" ")) {
					
						buttonOTPResend.setEnabled(true);
						buttonOTPOK.setEnabled(true);
						editTextEnterOTP.setEnabled(true);
						otpCheck();

					
				}

			}
		});

		buttonOTPResend.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				

				otpResend();
				

			}
		});
		
		
		
		buttonSignUp.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Username = editTextUserName.getText().toString();
				int size = Username.length();
				Password = editTextPassword.getText().toString();
				usertype = dropdown.getSelectedItem().toString();
         Log.e("size",String.valueOf(size));
				if (!Username.isEmpty() && !Username.startsWith(" "))
					if (size>9) {
					if (!Password.isEmpty() && !Password.startsWith(" ")) {
                       /* if(Password.length()>8){
                            signUp(Username, Password, usertype);
                            Log.e("en", "en");

                        }else{
                            showAlertDialogToast("Password must be contain atleast 8 characters");
                        }*/
						signUp(Username, Password, usertype);
					} else {
						showAlertDialogToast("Password should not be empty");
						//		EditTextViewPassword.setText("");
					}
					} else {
						showAlertDialogToast("Invalid mobile number");
						//		EditTextViewPassword.setText("");
					}

				else{
						showAlertDialogToast("Username should not be empty");
						//			EditTextViewMobile.setText("");
					}



			}
		});
		
		}
	
	private void otpCheck() {
		strOTP = editTextEnterOTP.getText().toString();
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog = ProgressDialog.show(SignupActivity.this, "In Progress",
						"Validating...", true, true);
			}

			@Override
			protected void onPostExecute(Void result) {
				try {
					// JsonAccountArray =
					// JsonAccountObject.getJSONArray("response");
					// for (int i = 0; i < JsonAccountArray.length(); i++) {
					// JSONObject job=new JSONObject();
					// job=JsonAccountArray.getJSONObject(0);
					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);

					if (strStatus.equals("true")) {
						//showAlertDialogToast("" + strMsg);
						textViewTimerText.setText(strMsg);
						dialogOtp.dismiss();
						Intent intentLogin = new Intent(SignupActivity.this,
								SigninActivity.class);
						startActivity(intentLogin);
						finish();

					} else {
						//showAlertDialogToast("" + strMsg);
						textViewTimerText.setText(strMsg);
						editTextEnterOTP.setText("");
						buttonOTPResend.setEnabled(true);
						buttonOTPOK.setEnabled(true);
						editTextEnterOTP.setEnabled(true);

					}

				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					Log.e("loginActivityException", e.toString());
				}

				dialog.dismiss();
			}

			@Override
			protected Void doInBackground(Void... params) {
				dialog.show();

				JSONObject jsonObject = new JSONObject();
				try {
				jsonObject.put("otp", strOTP);
				jsonObject.put("mobileno", strmobile);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				JsonServiceHandler = new JsonServiceHandler(
						Utils.strVerifyOtp, jsonObject.toString(),
						SignupActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceData();
				return null;
			}
		}.execute(new Void[] {});
	}
	
	private void otpResend() {
	
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog = ProgressDialog.show(SignupActivity.this, "In Progress",
						"Validating...", true, true);
			}

			@Override
			protected void onPostExecute(Void result) {
				try {
					// JsonAccountArray =
					// JsonAccountObject.getJSONArray("response");
					// for (int i = 0; i < JsonAccountArray.length(); i++) {
					// JSONObject job=new JSONObject();
					// job=JsonAccountArray.getJSONObject(0);
					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);

					if (strStatus.equals("true")) {
						//showAlertDialogToast("" + strMsg);
						textViewTimerText.setText(strMsg);
						
					} else {
						//showAlertDialogToast("" + strMsg);
						textViewTimerText.setText(strMsg);
						

					}

				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					Log.e("loginActivityException", e.toString());
				}

				dialog.dismiss();
			}

			@Override
			protected Void doInBackground(Void... params) {
				dialog.show();

				JSONObject jsonObject = new JSONObject();
				try {
				
				jsonObject.put("mobileno", strmobile);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				JsonServiceHandler = new JsonServiceHandler(
						Utils.strResendOtp, jsonObject.toString(),
						SignupActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceData();
				return null;
			}
		}.execute(new Void[]{});
	}
	
	private void signUp(final String username,final String password,
			final String usertype) {
		// TODO Auto-generated method stub

		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;



			@Override
			protected void onPreExecute() {
				mProgressBar.setVisibility(View.VISIBLE);
				buttonSignUp.setText("Loading..");
				progressAnimator.setDuration(1000);
				progressAnimator.setRepeatCount(40);
				progressAnimator.setInterpolator(new LinearInterpolator());
				progressAnimator.start();

			}

			@Override
			protected void onPostExecute(Void result) {

				try {

					strStatus1 = JsonAccountObject.getString("status");
					Log.e("return status", strStatus1);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);
				
					if (strStatus1.equals("true")) {
						//dialogOtp.show();
						mProgressBar.setVisibility(View.GONE);
						buttonSignUp.setText("Success");
						showAlertDialogToast(strMsg);
						buttonSignUp.setBackgroundColor(Color.parseColor("#b1d36f"));;
						//dialogOtp.show();
						JSONObject job = new JSONObject();
						job = JsonAccountObject.getJSONObject("data");
						strmobile = job.getString("mobileno");
						strID = job.getString("userid");

					//	String strName = job.getString("fullname");
						String profilestatus = job.getString("profilestatus");
						buttonSignUp.setBackgroundColor(Color.parseColor("#ecce03"));
						buttonSignUp.setText("Signup");
						new CountDownTimer(30000, 1000) {

						    public void onTick(long millisUntilFinished) {
						    	textViewTimer.setText("We are trying read the OTP message automatically waiting for " + millisUntilFinished / 1000  +" seconds ..");
						    }

						    public void onFinish() {
						    	textViewTimer.setText("Enter the OTP");
						    }
						}.start();
						
						Cursor cur;
						cur = databaseHandler.getDetails();
						cur.moveToFirst();

						getValue();
						
						
						
					}else{
						mProgressBar.setVisibility(View.GONE);
						showAlertDialogToast(strMsg);
						buttonSignUp.setText("Signup");
					}

				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					//Log.e("ProductActivityException", e.toString());
				}

			}

			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();
				

				try {
					jsonObject.put("mobileno", username);
					jsonObject.put("password", password);
					jsonObject.put("usertype", usertype);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	
				
				JsonServiceHandler = new JsonServiceHandler(Utils.strSignUpUrl,jsonObject.toString(), SignupActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceData();
				return null;
			}
		}.execute(new Void[] {});
		
	}
	
	   public void recivedSms(String message) 
	     {
	    try 
	      {
	    	Log.e(message, message);
	    	
	    	String[] splited = message.split("\\s+");
	    	editTextEnterOTP.setText(splited[splited.length-1]);
	    	otpCheck();
	      } 
	      catch (Exception e) 
	          {         
	                }
	    }
	protected void toastDisplay(String msg) {
		Toast toast = Toast.makeText(SignupActivity.this, msg,
				Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

	}
	
	

	public boolean netCheck() {
		// for network connection
		try {
			 ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			 NetworkInfo mWifi = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			 ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			 NetworkInfo mNetwork = connectionManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			 Object result = null;
			 if (mWifi.isConnected() || mNetwork.isConnected()) {
				return true;
			}

			 else if (result == null) {
				showAlertDialog(SignupActivity.this,
						"No Internet Connection",
						"Please Check Your internet connection.", false);
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	public void showAlertDialog(Context context, String title, String message,
			Boolean status) {
		 AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		 alertDialog.setTitle(title);
		 alertDialog.setMessage(message);
		 alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
		 alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
					}
				});
		 alertDialog.show();
	}

	@SuppressWarnings("null")
	@SuppressLint("NewApi")
	public void getValue() {
		cd = new ConnectionDetector(getApplicationContext());

		// Check if Internet present
		if (!cd.isConnectingToInternet()) {
			// Internet Connection is not present
			//alert.showAlertDialog(SplashActivity.this,
			//	"Internet Connection Error",
			//"Please connect to working Internet connection", false);
			// stop executing code by return
			return;
		}

		// Check if GCM configuration is set
		if (SERVER_URL == null || SENDER_ID == null || SERVER_URL.length() == 0
				|| SENDER_ID.length() == 0) {
			// GCM sernder id / server url is missing
			alert.showAlertDialog(SignupActivity.this, "Configuration Error!",
					"Please set your Server URL and GCM Sender ID", false);
			// stop executing code by return
			return;
		}

				/*
				 * Click event on Register button
				 */
		// Read EditText dat
		email = getEmail(getApplicationContext());
		Cursor cur ;
		cur = databaseHandler.getDetails();
		cur.moveToFirst();
	//	name = cur.getString(cur
	//			.getColumnIndex(DatabaseHandler.KEY_username));
		try {
			email.isEmpty();
			String[] parts = email.split("@");
			//	name = parts[0];
		} catch (NullPointerException e) {
			email = "noname@noname.com";
			//	name = "noname";
			// Toast.makeText(getApplicationContext(), email,
			// Toast.LENGTH_SHORT).show();
		}

		// String email = txtEmail.getText().toString();

		// Check if user filled the form
		if (name.trim().length() > 0 && email.trim().length() > 0) {
			// Launch Main Activity
			register();
		} else {
			// user doen't filled that data
			// ask him to fill the form
			alert.showAlertDialog(SignupActivity.this, "Registration Error!",
					"Please enter your details", false);
		}
	}

	static String getEmail(Context context) {
		AccountManager accountManager = AccountManager.get(context);
		Account account = getAccount(accountManager);

		if (account == null) {
			return null;
		} else {
			return account.name;
		}
	}

	private static Account getAccount(AccountManager accountManager) {
		 Account[] accounts = accountManager.getAccountsByType("com.google");
		 Account account;
		 if (accounts.length > 0) {
			account = accounts[0];
		} else {
			account = null;
		}
		 return account;
	}

	public void register() {

		cd = new ConnectionDetector(getApplicationContext());

		// Check if Internet present
		if (!cd.isConnectingToInternet()) {
			// Internet Connection is not present
			alert.showAlertDialog(SignupActivity.this,
					"Internet Connection Error",
					"Please connect to working Internet connection", false);
			// stop executing code by return
			return;
		}

		// Getting name, email from intent

		// Make sure the device has the proper dependencies.
		GCMRegistrar.checkDevice(this);

		// Make sure the manifest was properly set - comment out this line
		// while developing the app, then uncomment it when it's ready.
		GCMRegistrar.checkManifest(this);

		registerReceiver(mHandleMessageReceiver, new IntentFilter(
				DISPLAY_MESSAGE_ACTION));

		// Get GCM registration id
		final String regId = GCMRegistrar.getRegistrationId(this);

		// Check if regid already presents
		if (regId.equals("")) {
			// Registration is not present, register now with GCM
			GCMRegistrar.register(this, SENDER_ID);
		} else {
			// Device is already registered on GCM
			if (GCMRegistrar.isRegisteredOnServer(this)) {
				// Skips registration.
				// Toast.makeText(getApplicationContext(),
				// "Already registered with GCM", Toast.LENGTH_LONG).show();
			} else {
				// Try to register again, but not in the UI thread.
				// It's also necessary to cancel the thread onDestroy(),
				// hence the use of AsyncTask instead of a raw thread.
				final Context context = this;
				mRegisterTask = new AsyncTask<Void, Void, Void>() {

					@Override
					protected Void doInBackground(Void... params) {
						// Register on our server
						// On server creates a new user
						ServerUtilities.register(context, name, email, regId);
						return null;
					}

					@Override
					protected void onPostExecute(Void result) {
						mRegisterTask = null;
					}

				};
				mRegisterTask.execute(null, null, null);
			}
		}
	}

	/**
	 * Receiving push messages
	 * */
	private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			@SuppressWarnings("unused")
			String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
			// Waking up mobile if it is sleeping
			WakeLocker.acquire(getApplicationContext());

			/**
			 * Take appropriate action on this message depending upon your app
			 * requirement For now i am just displaying it on the screen
			 * */

			// Showing received message
			// Toast.makeText(getApplicationContext(), "New Message: " +
			// newMessage, Toast.LENGTH_LONG).show();

			// Releasing wake lock
			WakeLocker.release();
		}
	};

	@Override
	protected void onDestroy() {
		if (mRegisterTask != null) {
			mRegisterTask.cancel(true);
		}
		try {
			unregisterReceiver(mHandleMessageReceiver);
			GCMRegistrar.onDestroy(this);
		} catch (Exception e) {
			//Log.e("UnRegister Receiver Error", "> " + e.getMessage());
		}
		super.onDestroy();
	}

	public void showAlertDialogToast( final String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						Log.e("strMsg", strMsg);
						if (strMsg.equals("Already registered with us, Please Login")) {
							Log.e("inside", strMsg);
							Intent to = new Intent(SignupActivity.this, SigninActivity.class);
							startActivity(to);
							finish();
							dialog.cancel();
						} else if (strStatus1.equals("true")) {
dialogOtp.show();
						} else {
							dialog.cancel();
						}

					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();

		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

	}

	/*public void onBackPressed() {
		exitAlret();
	}

	private void exitAlret() {
		AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
		localBuilder.setCancelable(false);
		localBuilder.setMessage("Do you want to Exit?");
		localBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {

						finish();

					}
				});
		localBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {
						paramDialogInterface.cancel();
					}
				});
		localBuilder.create().show();
	}*/
}
