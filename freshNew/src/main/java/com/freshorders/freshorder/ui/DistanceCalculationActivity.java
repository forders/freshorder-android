package com.freshorders.freshorder.ui;
import android.Manifest;
import android.annotation.TargetApi;
import android.content.pm.PackageManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.freshorders.freshorder.*;
import com.freshorders.freshorder.adapter.DistanceCalculationAdapter;
import com.freshorders.freshorder.adapter.DistanceCalculationAdapterUnSync;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.DistanceCalculationDomain;
import com.freshorders.freshorder.helper.DistanceCalculatorService;
import com.freshorders.freshorder.helper.StoppableRunnable;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.GPSTracker;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.freshorders.freshorder.utils.Utils.apiKey;
import static com.freshorders.freshorder.utils.Utils.destination;


/**
 * Created by Karthika on 28/02/2018.
 */

public class DistanceCalculationActivity extends Activity {
    static Intent serviceIntent = null;
    static boolean isServiceRunning = false;
    static Button ButtonAction, ButtonAction1;
    static Button buttonSync;
    static TextView textViewDistance;
    static TextView textViewAmount;
    public static boolean isTRunning = false;
    public static SQLiteDatabase db;
    String settingid, industryid, refkey, data_type, remarks;
    static String date, lat, lng, startdt, enddt;
    GPSTracker gps;
    public static String status = "Active";
    public static Context context1;
    String userid;
    public static ListView listViewDistance,listViewDistance1;
    public static JSONObject JsonAccountObject = null;
    public static JSONArray JsonAccountArray = null;
    public static JsonServiceHandler JsonServiceHandler;
    public static ArrayList<DistanceCalculationDomain> arrayListObjects;

    DistanceCalculationAdapter adapter;
    public static DatabaseHandler databaseHandler;
    public static Context context;

    //menu objects
    public static TextView textViewNoData1;
    TextView menuIcon, textViewAssetMenuProfile, textViewAssetMenuMyOrders,DateClick,TextviewGeoLogsSyn,TextviewSyn,
            textViewAssetMenuMyDealers, textViewAssetMenuProducts, textViewNoData,//textViewNoData1,
            textViewAssetMenuPayment, textViewAssetMenuComplaint, textViewAssetMenuDistanceCalculation,
            textViewAssetMenuSignout, textViewAssetMenuClientVisit, textViewAssetMenuPaymentCollection,
            textViewNodata, textViewAssetMenuCreateOrder, textViewcross_1, textViewAssetMenuRefresh, textViewAssetMenuStockEntry, textViewAssetMenuAddMerchant, textViewAssetMenuAcknowledge, textViewAssetMenusurveyuser, textViewAssetMenudownloadscheme, textViewcross_2, textViewAssetMenuMyDayPlan, textViewAssetMenuMySales,
            textViewAssetMenuDistStock;

    public static int menuCliccked;

    LinearLayout linearLayoutMenuParent, linearLayoutProfile, linearLayoutDistanceCalculation,
            linearLayoutMyOrders, linearLayoutMyDealers, linearLayoutProducts,
            linearLayoutPayment, linearLayoutPaymentCollection, linearLayoutComplaint, linearLayoutSignout,
            linearlayoutSearchIcon, linearLayoutCreateOrder, linearLayoutClientVisit, linearLayoutRefresh,
            linearLayoutAcknowledge, linearLayoutStockEntry, linearLayoutAddMerchant,
            linearLayoutsurveyuser, linearLayoutdownloadscheme, linearLayoutMyDayPlan, linearLayoutMySales,
            linearLayoutDistStock;

    String PaymentStatus = "null", suserid, duserid = null, updateddt;

    /* Location*/
    public static String TAG = "DistanceCalculationActivity";

    static double pLat = 0, pLong = 0;
    static double cLat = 0, cLong = 0;

    public Handler mHandler = new Handler();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.distance_calculation_screen);
        Log.e("Oncreate", "Oncreate");

        int permissionCheck1 = ContextCompat.checkSelfPermission(DistanceCalculationActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);

        if (permissionCheck1 != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    DistanceCalculationActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.LOCATION_PERMISSION_REQUEST_CODE);
        }

        ButtonAction = (Button) findViewById(R.id.ButtonAction);
        buttonSync=(Button) findViewById(R.id.ButtonSync);
        textViewDistance = (TextView) findViewById(R.id.TextviewDistance);

        textViewAmount = (TextView) findViewById(R.id.TextviewAmount);

        linearLayoutRefresh = (LinearLayout) findViewById(R.id.linearLayoutRefresh);
        listViewDistance = (ListView) findViewById(R.id.listViewDistance);
        listViewDistance1 = (ListView) findViewById(R.id.listViewDistance1);
        textViewNoData = (TextView) findViewById(R.id.textViewNoData);
        TextviewGeoLogsSyn = (TextView) findViewById(R.id.TextviewGeoLogsSyn);
        TextviewSyn= (TextView) findViewById(R.id.TextviewSyn);
        textViewNoData1 = (TextView) findViewById(R.id.textViewNoData1);
        DateClick= (TextView) findViewById(R.id.DateClick);
        linearLayoutMyDealers = (LinearLayout) findViewById(R.id.linearLayoutMyDealers);
        linearLayoutProducts = (LinearLayout) findViewById(R.id.linearLayoutProducts);
        linearLayoutPayment = (LinearLayout) findViewById(R.id.linearLayoutPayment);

        linearLayoutComplaint = (LinearLayout) findViewById(R.id.linearLayoutComplaint);
        linearLayoutSignout = (LinearLayout) findViewById(R.id.linearLayoutSignout);
        linearLayoutClientVisit = (LinearLayout) findViewById(R.id.linearLayoutClientVisit);
        linearLayoutPaymentCollection = (LinearLayout) findViewById(R.id.linearLayoutPaymentCollection);
        linearLayoutCreateOrder = (LinearLayout) findViewById(R.id.linearLayoutCreateOrder);
        linearLayoutProfile = (LinearLayout) findViewById(R.id.linearLayoutProfile);
        linearLayoutMyOrders = (LinearLayout) findViewById(R.id.linearLayoutMyOrders);
        linearLayoutAcknowledge = (LinearLayout) findViewById(R.id.linearLayoutAcknowledge);
        linearLayoutStockEntry = (LinearLayout) findViewById(R.id.linearLayoutStockEntry);
        linearLayoutAddMerchant = (LinearLayout) findViewById(R.id.linearLayoutAddMerchant);
        linearLayoutdownloadscheme = (LinearLayout) findViewById(R.id.linearLayoutdownloadscheme);
        linearLayoutsurveyuser = (LinearLayout) findViewById(R.id.linearLayoutsurveyuser);
        linearLayoutMyDayPlan = (LinearLayout) findViewById(R.id.linearLayoutMyDayPlan);
        linearLayoutMySales = (LinearLayout) findViewById(R.id.linearLayoutMySales);
        linearLayoutDistanceCalculation = (LinearLayout) findViewById(R.id.linearLayoutDistanceCalculation);
        menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
        textViewAssetMenuMyDealers = (TextView) findViewById(R.id.textViewAssetMenuMyDealers);
        textViewAssetMenuProducts = (TextView) findViewById(R.id.textViewAssetMenuProducts);
        textViewAssetMenuPayment = (TextView) findViewById(R.id.textViewAssetMenuPayment);
        textViewAssetMenuComplaint = (TextView) findViewById(R.id.textViewAssetMenuComplaint);
        textViewAssetMenuSignout = (TextView) findViewById(R.id.textViewAssetMenuSignout);
        textViewAssetMenuClientVisit = (TextView) findViewById(R.id.textViewAssetMenuClientVisit);
        textViewAssetMenuAddMerchant = (TextView) findViewById(R.id.textViewAssetMenuAddMerchant);
        textViewAssetMenuAcknowledge = (TextView) findViewById(R.id.textViewAssetMenuAcknowledge);
        textViewAssetMenuStockEntry = (TextView) findViewById(R.id.textViewAssetMenuStockEntry);
        textViewAssetMenudownloadscheme = (TextView) findViewById(R.id.textViewAssetMenudownloadscheme);
        textViewAssetMenusurveyuser = (TextView) findViewById(R.id.textViewAssetMenusurveyuser);
        textViewAssetMenuPaymentCollection = (TextView) findViewById(R.id.textViewAssetMenuPaymentCollection);
        textViewAssetMenuRefresh = (TextView) findViewById(R.id.textViewAssetMenuRefresh);
        textViewAssetMenuCreateOrder = (TextView) findViewById(R.id.textViewAssetMenuCreateOrder);
        textViewAssetMenuProfile = (TextView) findViewById(R.id.textViewAssetMenuProfile);
        textViewAssetMenuMyOrders = (TextView) findViewById(R.id.textViewAssetMenuMyOrders);
        textViewAssetMenuMyDayPlan = (TextView) findViewById(R.id.textViewAssetMenuMyDayPlan);
        textViewAssetMenuMySales = (TextView) findViewById(R.id.textViewAssetMenuMySales);
        textViewAssetMenuDistanceCalculation = (TextView) findViewById(R.id.textViewAssetMenuDistanceCalculation);
        linearLayoutMenuParent = (LinearLayout) findViewById(R.id.linearLayoutMenuParent);
        Typeface font = Typeface.createFromAsset(getAssets(),
                "fontawesome-webfont.ttf");
        menuIcon.setTypeface(font);
        textViewAssetMenuCreateOrder.setTypeface(font);
        textViewAssetMenuProfile.setTypeface(font);
        textViewAssetMenuMyOrders.setTypeface(font);
        textViewAssetMenuMyDealers.setTypeface(font);
        textViewAssetMenuProducts.setTypeface(font);
        textViewAssetMenuPayment.setTypeface(font);
        textViewAssetMenuComplaint.setTypeface(font);
        textViewAssetMenuSignout.setTypeface(font);
        textViewAssetMenuClientVisit.setTypeface(font);
        textViewAssetMenuPaymentCollection.setTypeface(font);
        textViewAssetMenuRefresh.setTypeface(font);
        textViewAssetMenuDistanceCalculation.setTypeface(font);
        textViewAssetMenuAcknowledge.setTypeface(font);
        textViewAssetMenuStockEntry.setTypeface(font);
        textViewAssetMenuAddMerchant.setTypeface(font);
        textViewAssetMenusurveyuser.setTypeface(font);
        textViewAssetMenudownloadscheme.setTypeface(font);
        textViewAssetMenuMyDayPlan.setTypeface(font);
        textViewAssetMenuMySales.setTypeface(font);

        linearLayoutDistStock = (LinearLayout) findViewById(R.id.linearLayoutDistStock);
        textViewAssetMenuDistStock = (TextView) findViewById(R.id.textViewAssetMenuDistStock);
        textViewAssetMenuDistStock.setTypeface(font);

        databaseHandler = new DatabaseHandler(getApplicationContext());
        Cursor cur;
        cur = databaseHandler.getDetails();
        cur.moveToFirst();
        userid = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_id));
        Cursor cursorUnSyncLogs=databaseHandler.getUnSyncedSMGeoLogs();
        buttonSync.setVisibility(View.INVISIBLE);
        //buttonSync.setEnabled(false);
        gps = new GPSTracker(DistanceCalculationActivity.this);
        if (gps.canGetLocation() == false) {
            showSettingsAlert();
        }
        if (netCheckwithoutAlert() == true) {
            discal();
        }
        databaseHandler = new DatabaseHandler(getApplicationContext());
        Cursor curs;
        curs = databaseHandler.getdealer();
        curs.moveToFirst();
        Constants.DUSER_ID = curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_Duserid));
        // ButtonAction1 = (Button) findViewById(R.id.ButtonAction1);
        if(cursorUnSyncLogs!=null)
        {
            if(cursorUnSyncLogs.getCount()>0)
            {
                buttonSync.setVisibility(View.VISIBLE);
            }
        }
        else{
            buttonSync.setVisibility(View.GONE);
        }

        Cursor curPay;
        curPay = databaseHandler.getDetails();
        if(curPay != null && curPay.getCount() > 0){
            curPay.moveToFirst();
            PaymentStatus = curPay.getString(curPay.getColumnIndex(DatabaseHandler.KEY_paymentStatus));
            curPay.close();
        }


        if (isServiceRunning == true) {
            ButtonAction.setText("Stop");
            setTravelDetails();
        } else {
            ButtonAction.setText("Start");
            textViewDistance.setText("");
            textViewAmount.setText("");
        }
        TextviewSyn.setTypeface(null, Typeface.BOLD);
        TextviewGeoLogsSyn.setText("Distance to be Synced : "+databaseHandler.getUnSyncedTotalDistance()+" meters");
        TextviewGeoLogsSyn.setTypeface(null, Typeface.BOLD);

//DateClick.setOnClickListener(new View.OnClickListener() {
        // @Override
        // public void onClick(View v) {
        //   Intent into =new Intent(DistanceCalculationActivity.this,LocalData.class);
        // startActivity(into);
        //}
//});
        buttonSync.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(netCheckwithoutAlert()) {
                    DistanceCalculationActivity.syncGeoLogs(getBaseContext());
                    showAlertDialogToast1("Are you want to Sync local data's?");


                }
                else{
                    showAlertDialogToast("No Internet..Logs cannot be synced.");
                }
            }
        });

        ButtonAction.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                Cursor curT=databaseHandler.getSMGeoLogs();
                System.out.println("SHICURSOR1:");
                if(curT!=null)
                {
                    System.out.println(databaseHandler.getCursorItems(curT));
                }
                System.out.println("SHICURSOR2:");
                curT=databaseHandler.getDatewiseUnSyncedSMGeoLogs();
                if(curT!=null)
                {
                    System.out.println(databaseHandler.getCursorItems(curT));
                }
                System.out.println("SHICURSOR3:");
                curT=databaseHandler.getDatewiseUnSyncedSmGeoLogs();
                if(curT!=null)
                {
                    System.out.println(databaseHandler.getCursorItems(curT));
                }



                Cursor cursorUnSyncLogs=databaseHandler.getUnSyncedSMGeoLogs();
                if (serviceIntent == null) {
                    serviceIntent = new Intent(getBaseContext(), DistanceCalculatorService.class);
                }

                if (!isServiceRunning) {
                    startService(serviceIntent);
                    isServiceRunning = true;
                    ButtonAction.setText("Stop");
                    textViewDistance.setText("Distance : 0.0km");
                    textViewAmount.setText("Amount : ₹0.0");

                } else {
                    stopService(serviceIntent);
                    isServiceRunning = false;
                    ButtonAction.setText("Start");
                    textViewDistance.setText("");
                    textViewAmount.setText("");
                }
                if(cursorUnSyncLogs!=null)
                {
                    if(cursorUnSyncLogs.getCount()>0)
                    {

                        buttonSync.setVisibility(View.VISIBLE);
                    }
                }
                else{
                    buttonSync.setVisibility(View.GONE);
                }
                TextviewGeoLogsSyn.setText("Distance to be Synced : "+databaseHandler.getUnSyncedTotalDistance() + " meters");
                TextviewGeoLogsSyn.setTypeface(null, Typeface.BOLD);
            }

        });

        menuIcon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (menuCliccked == 0) {
                    linearLayoutMenuParent.setVisibility(View.VISIBLE);
                    InputMethodManager iss = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    iss.hideSoftInputFromWindow(menuIcon.getWindowToken(), 0);
                    menuCliccked = 1;
                } else {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    InputMethodManager iss = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    iss.hideSoftInputFromWindow(menuIcon.getWindowToken(), 0);
                    menuCliccked = 0;
                }

            }
        });


        linearLayoutDistanceCalculation.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;


            }
        });


        linearLayoutMenuParent.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);

                menuCliccked = 0;
            }
        });

        linearLayoutCreateOrder.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;

                if (netCheckwithoutAlert() == true) {
                    getDetails("createorder");
                } else {
                    Intent io = new Intent(DistanceCalculationActivity.this,
                            CreateOrderActivity.class);
                    startActivity(io);
                    finish();

                }

            }
        });
        linearLayoutProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub

                if (netCheckwithoutAlert() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    getDetails("profile");
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });

        linearLayoutMyOrders.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheckwithoutAlert()) {
                    Constants.orderstatus = "Success";
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    getDetails("myorders");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {

                        Constants.orderstatus = "Success";
                        Constants.Merchantname = "";
                        Constants.SalesMerchant_Id = "";

                        Intent io = new Intent(DistanceCalculationActivity.this,
                                SalesManOrderActivity.class);
                        io.putExtra("Key", "menuclick");
                        Constants.setimage = null;
                        startActivity(io);
                        finish();
                    }

                }


            }
        });

        linearLayoutRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("Clicked", "Clicked");
                if (netCheckwithoutAlert()) {

                    final Dialog qtyDialog = new Dialog(DistanceCalculationActivity.this);
                    qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    qtyDialog.setContentView(R.layout.warning_dialog);

                    final Button buttonUpdateOk, buttonUpdateCancel;

                    final TextView textQtyValidate;

                    buttonUpdateOk = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateOk);
                    buttonUpdateCancel = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateCancel);

                    textQtyValidate = (TextView) qtyDialog
                            .findViewById(R.id.textQtyValidate);
                    qtyDialog.show();


                    buttonUpdateOk.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            databaseHandler.synTableDelete();
                            Constants.refreshscreen = "createorder";
                            Intent io = new Intent(DistanceCalculationActivity.this, RefreshActivity.class);
                            startActivity(io);
                            finish();
                            qtyDialog.dismiss();
                        }

                    });

                    buttonUpdateCancel
                            .setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub

                                    qtyDialog.dismiss();

                                }
                            });

                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }
            }

        });


        linearLayoutMyDealers.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (netCheckwithoutAlert() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    getDetails("mydealer");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(DistanceCalculationActivity.this,
                                MyDealersActivity.class);
                        io.putExtra("Key", "menuclick");
                        startActivity(io);
                        finish();

                    }
                }

            }
        });


        linearLayoutComplaint.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheckwithoutAlert()) {
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    getDetails("postnotes");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment");
                    } else {
                        Constants.Merchantname = "";
                        Constants.SalesMerchant_Id = "";

                        Intent io = new Intent(DistanceCalculationActivity.this,
                                MerchantComplaintActivity.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });
        linearLayoutClientVisit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheckwithoutAlert()) {
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    getDetails("clientvisit");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Constants.Merchantname = "";
                        Constants.SalesMerchant_Id = "";

                        Intent to = new Intent(DistanceCalculationActivity.this,
                                SMClientVisitHistory.class);
                        startActivity(to);
                        finish();
                    }
                }

            }
        });

        linearLayoutPaymentCollection.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheckwithoutAlert() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    getDetails("paymentcoll");
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }


            }
        });

        linearLayoutAcknowledge.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheckwithoutAlert() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    getDetails("acknowledge");
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }


            }
        });
        linearLayoutStockEntry.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheckwithoutAlert()) {
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    getDetails("stockentry");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment");
                    }else {
                        Constants.Merchantname = "";
                        Constants.SalesMerchant_Id = "";

                        Intent io = new Intent(DistanceCalculationActivity.this,
                                OutletStockEntry.class);
                        startActivity(io);
                        finish();
                    }
                }


            }
        });
        linearLayoutDistStock.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheckwithoutAlert() == true) {
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    getDetails("diststock");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment");
                    } else {
                        Constants.Merchantname = "";
                        Constants.SalesMerchant_Id = "";

                        Intent io = new Intent(DistanceCalculationActivity.this,
                                DistributrStockActivity.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });


        linearLayoutAddMerchant.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheckwithoutAlert()) {
                    Constants.orderstatus = "Success";
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    getDetails("addmerchant");
                } else {
                    Constants.orderstatus = "Success";
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";

                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent to = new Intent(DistanceCalculationActivity.this,
                                AddMerchantNew.class);
                        startActivity(to);
                        finish();
                    }
                }

            }
        });
        linearLayoutdownloadscheme.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheckwithoutAlert()) {

                    final Dialog qtyDialog = new Dialog(DistanceCalculationActivity.this);
                    qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    qtyDialog.setContentView(R.layout.warning_dialog);

                    final Button buttonUpdateOk, buttonUpdateCancel;

                    final TextView textQtyValidate;

                    buttonUpdateOk = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateOk);
                    buttonUpdateCancel = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateCancel);

                    textQtyValidate = (TextView) qtyDialog
                            .findViewById(R.id.textQtyValidate);
                    qtyDialog.show();


                    buttonUpdateOk.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            databaseHandler.deletescheme();
                            Constants.downloadScheme = "createorder";
                            Intent io = new Intent(DistanceCalculationActivity.this, SchemeDownload.class);
                            startActivity(io);
                            finish();
                            qtyDialog.dismiss();
                        }

                    });

                    buttonUpdateCancel
                            .setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub

                                    qtyDialog.dismiss();

                                }
                            });

                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });
        linearLayoutsurveyuser.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheckwithoutAlert()) {
                    getDetails("surveyuser");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent to = new Intent(DistanceCalculationActivity.this, SurveyMerchant.class);
                        startActivity(to);
                        finish();
                    }
                }

            }
        });

        linearLayoutMyDayPlan.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;

                if (netCheckwithoutAlert()) {
                    getDetails("MyDayPlan");
                } else {
                    Intent to = new Intent(DistanceCalculationActivity.this,
                            MyDayPlan.class);
                    startActivity(to);
                    finish();
                }
            }
        });

        linearLayoutMySales.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheckwithoutAlert() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    getDetails("mysales");
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }
            }
        });

        linearLayoutSignout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                // TODO Auto-generated method stub
                Constants.Merchantname = "";
                Constants.SalesMerchant_Id = "";
                databaseHandler.delete();
                Intent io = new Intent(DistanceCalculationActivity.this,
                        SigninActivity.class);
                startActivity(io);
                finish();
            }
        });

        Cursor curT=databaseHandler.getSMGeoLogs();
        System.out.println("SHICURSOR1:");
        if(curT!=null)
        {
            System.out.println(databaseHandler.getCursorItems(curT));
        }
        System.out.println("SHICURSOR2:");
        curT=databaseHandler.getDatewiseUnSyncedSMGeoLogs();
        if(curT!=null)
        {
            System.out.println(databaseHandler.getCursorItems(curT));
        }
        System.out.println("SHICURSOR3:");
        curT=databaseHandler.getDatewiseUnSyncedSMGeoLogs();
        if(curT!=null)
        {
            System.out.println(databaseHandler.getCursorItems(curT));
        }


    }



    public static void setTravelDetails() {
        textViewDistance.setText("Distance : " + databaseHandler.getTotalDistance(DistanceCalculatorService.strBatchId) / 1000 + " km");
        textViewAmount.setText("Amount : ₹ " + databaseHandler.getTotalAmount(DistanceCalculatorService.strBatchId) * 2.5 / 1000);
        //textViewAmount.setText("Amount : ? "+databaseHandler.getTotalAmount(DistanceCalculatorService.strBatchId));
    }


    public void startThreadAction() {
        isTRunning = true;
        gps = new GPSTracker(DistanceCalculationActivity.this);
        if (gps.canGetLocation() == false) {
            showSettingsAlert();
        } else {
            showAlertDialogToast("Your driving distance will be calculated from this moment");
            trackSet = new JSONArray();
            arlTrackSet = new ArrayList();
            totalamount = 0;
            totaldistance = 0;
            AmountCalculation();
            onStartThread(Constants.TRACK_INTERVAL * 1000);
        }
    }

    public void AmountCalculation() {
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "", strMsg = "";

            @Override
            protected void onPreExecute() {


            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
                try {


                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {
                        dialog.dismiss();
                        JSONArray job1 = JsonAccountObject.getJSONArray("data");
                        Log.e("job1NEW_______", String.valueOf(job1));
                        for (int i = 0; i < job1.length(); i++) {

                            JSONObject job = new JSONObject();
                            job = job1.getJSONObject(i);
                            Log.e("jobNEW_______", String.valueOf(job));
                            settingid = job.getString("settingid");
                            userid = job.getString("userid");
                            industryid = job.getString("industryid");
                            updateddt = job.getString("updateddt");
                            refkey = job.getString("refkey");
                            double refvalue = Double.parseDouble(job.getString("refvalue"));
                            Log.e("refvalue--", String.valueOf(refvalue));
                            data_type = job.getString("data_type");
                            remarks = job.getString("remarks");
                            status = job.getString("status");
                            apirefvalue = refvalue;

                        }


                    } else {
                        if (strStatus.equals("false")) {
                            apirefvalue = Constants.refvalue;
                            Log.e("apirefvaluefalse", String.valueOf(apirefvalue));

                        }

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {

                }

            }


            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();


                JsonServiceHandler = new JsonServiceHandler(Utils.stramountcalculation + Constants.DUSER_ID, DistanceCalculationActivity.this);
                JsonAccountObject = JsonServiceHandler.ServiceDataGet();


                Log.e("JsonAccountObjectNEW", String.valueOf(JsonAccountObject));


                return null;

            }

        }.execute(new Void[]{});

    }

    public void discal() {
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "", strMsg = "";

            @Override
            protected void onPreExecute() {

                dialog = ProgressDialog.show(DistanceCalculationActivity.this, "",
                        "Loading...", true, true);
                dialog.setCancelable(false);
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
                try {


                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {
                        dialog.dismiss();
                        listViewDistance.setVisibility(View.VISIBLE);
                        textViewNoData.setVisibility(View.GONE);

                        arrayListObjects = new ArrayList<DistanceCalculationDomain>();
                        JSONArray job1 = JsonAccountObject.getJSONArray("data");
                        for (int i = 0; i < job1.length(); i++) {

                            JSONObject job = new JSONObject();
                            DistanceCalculationDomain db = new DistanceCalculationDomain();

                            job = job1.getJSONObject(i);
                            String smname = job.getString("smname");
                            String logtime = job.getString("logtime");
                            Log.e("logtime setadap",logtime);
                            String updateddt = job.getString("updateddt");
                            String apidistance = (job.getString("traveldistance"));
                            String apiamount = (job.getString("amount"));
                            db.setSmname(smname);
                            db.setLogtime(logtime);
                            db.setUpdateddt(updateddt);
                            db.setTraveldistance(apidistance);
                            db.setAmount(apiamount);
                            arrayListObjects.add(db);

                        }

                        setadap();

                    } else {
                        dialog.dismiss();
                        listViewDistance.setVisibility(View.GONE);
                        textViewNoData.setVisibility(View.VISIBLE);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }


            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();
                Log.e("jsonObject222222", String.valueOf(jsonObject));
                String date = "yyyy-MM-dd";

                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                enddt = df.format(c.getTime());
                SimpleDateFormat inputFormat = new SimpleDateFormat(date);
                startdt = inputFormat.format(new Date().getTime() - 7 * 24 * 60 * 60 * 1000);

                try {
                    jsonObject.put("suserid", Constants.USER_ID);
                    jsonObject.put("status", "Active");
                    jsonObject.put("usertype", "S");
                    jsonObject.put("startdt", startdt);
                    jsonObject.put("enddt", enddt);
                    jsonObject.put("mode", "WEB");
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                Log.e("strsumdistancecal",jsonObject.toString());
                JsonServiceHandler = new JsonServiceHandler(Utils.strsumdistancecal, jsonObject.toString(), DistanceCalculationActivity.this);
                JsonAccountObject = JsonServiceHandler.ServiceData();


                return null;

            }

        }.execute(new Void[]{});

    }

    public static void discal2() {
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "", strMsg = "";

            @Override
            protected void onPreExecute() {


            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
                try {


                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {
                        dialog.dismiss();
//                        listViewDistance.setVisibility(View.VISIBLE);
//                        textViewNoData.setVisibility(View.GONE);

                        arrayListObjects = new ArrayList<DistanceCalculationDomain>();
                        JSONArray job1 = JsonAccountObject.getJSONArray("data");
                        for (int i = 0; i < job1.length(); i++) {

                            JSONObject job = new JSONObject();
                            DistanceCalculationDomain db = new DistanceCalculationDomain();

                            job = job1.getJSONObject(i);
                            String smname = job.getString("smname");
                            String logtime = job.getString("logtime");
                            Log.e("logtime setadap",logtime);
                            String updateddt = job.getString("updateddt");
                            String apidistance = (job.getString("traveldistance"));
                            String apiamount = (job.getString("amount"));
                            db.setSmname(smname);
                            db.setLogtime(logtime);
                            db.setUpdateddt(updateddt);
                            db.setTraveldistance(apidistance);
                            db.setAmount(apiamount);
                            arrayListObjects.add(db);

                        }

                        DistanceCalculationAdapter adapter = new DistanceCalculationAdapter(context,
                                R.layout.item_distance_screen, arrayListObjects);
                        //textViewNoData.setVisibility(View.GONE);
                        //listViewDistance.setVisibility(View.VISIBLE);
                        listViewDistance.setAdapter(adapter);

                    } else {
                        // dialog.dismiss();
//                        listViewDistance.setVisibility(View.GONE);
//                        textViewNoData.setVisibility(View.VISIBLE);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {

                }

            }


            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();
                Log.e("jsonObject222222", String.valueOf(jsonObject));
                String date = "yyyy-MM-dd";

                Calendar c = Calendar.getInstance();
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                enddt = df.format(c.getTime());
                SimpleDateFormat inputFormat = new SimpleDateFormat(date);
                startdt = inputFormat.format(new Date().getTime() - 7 * 24 * 60 * 60 * 1000);

                try {
                    jsonObject.put("suserid", Constants.USER_ID);
                    jsonObject.put("status", "Active");
                    jsonObject.put("usertype", "S");
                    jsonObject.put("startdt", startdt);
                    jsonObject.put("enddt", enddt);
                    jsonObject.put("mode", "WEB");
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                JsonServiceHandler = new JsonServiceHandler(Utils.strsumdistancecal, jsonObject.toString(), context);
                JsonAccountObject = JsonServiceHandler.ServiceData();


                return null;

            }

        }.execute(new Void[]{});

    }

    @TargetApi(23)
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permission, @NonNull int[] grantResults) {

        Log.e(",l", String.valueOf(requestCode));
        Log.e("size", String.valueOf(grantResults.length));

        if (requestCode == Constants.LOCATION_PERMISSION_REQUEST_CODE) {
            Log.e("requestcode", String.valueOf(grantResults[0]));
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                Intent io = new Intent(DistanceCalculationActivity.this, DistanceCalculationActivity.class);
                startActivity(io);
                finish();
            } else {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.LOCATION_PERMISSION_REQUEST_CODE);
            }


        }
    }

    public static void syncGeoLogs(final Context context) {

        new AsyncTask<Void, Void, Void>() {

            String strStatus = "";
            String strMsg = "";

            @Override
            protected void onPreExecute() {

            }

            @Override
            protected void onPostExecute(Void result) {

                DistanceCalculationActivity.discal2();

            }

            @Override
            public Void doInBackground(Void... params) {
                try {
                    Cursor cursor = databaseHandler.getUnSyncedSMGeoLogs();
                    JSONArray jsonArray = null;
                    if (cursor != null) {
                        if (cursor.getCount() > 0) {
                            jsonArray = databaseHandler.getCursorItems(cursor);
                        }
                    }
                    JsonServiceHandler JsonServiceHandler = new JsonServiceHandler(Utils.strSaveTracklog, jsonArray.toString(),context);
                    JSONObject responseObject = JsonServiceHandler.ServiceData();
                    if (responseObject.get("status").toString().equalsIgnoreCase("true")) {
                        databaseHandler.removeAllGeoLogs();
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return null;
            }
        }.execute(new Void[]{});
    }

    public void tracklog() {
        new AsyncTask<Void, Void, Void>() {

            String strStatus = "";
            String strMsg = "";

            @Override
            protected void onPreExecute() {

            }

            @Override
            protected void onPostExecute(Void result) {

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);
                    if (strStatus.equals("false")) {
                        Date d = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
                        date = ft.format(d).toString();
                        Log.e("time", date);
                        db.execSQL("INSERT INTO tracklog(suserid,logtime,geolat,geolong,status,updateddt,distance,amount) VALUES ('" + suserid + "','" + date + "','" + lat + "','" + lng + "','" + status + "','" + date + "','" +apidistance + "','" + apiamount + "' ) ");
                        Cursor cursr;
                        String selectQuery1 = "SELECT  * FROM tracklog ";
                        cursr = db.rawQuery(selectQuery1, null);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected Void doInBackground(Void... params) {


                Log.e("pgeolong11111", String.valueOf(pLong));
                Log.e("ts Before API call",trackSet.toString() );
                JsonServiceHandler = new JsonServiceHandler(Utils.strSaveTracklog, trackSet.toString(), context1);
                JsonAccountObject = JsonServiceHandler.ServiceData();
                trackSet=new JSONArray();
                return null;
            }
        }.execute(new Void[]{});


    }


    public void distanceAPI(final String pLat, final String pLong, final String cLat, final String cLong
    ) {
        new AsyncTask<Void, Void, Void>() {

            String strStatus = "";
            String strMsg = "";

            @Override
            protected void onPreExecute() {

            }

            @Override
            protected void onPostExecute(Void result) {
                try {
                    strStatus = JsonAccountObject.getString("status");
                    if (strStatus.equals("OK")) {
                        JSONObject jsonObject = JsonAccountObject.getJSONArray("rows").getJSONObject(0).getJSONArray("elements").getJSONObject(0).getJSONObject("distance");
                        if (jsonObject != null) {
                            String text = jsonObject.getString("text");
                            double value = Double.parseDouble(jsonObject.getString("value"));
                            apidistance=value;
                            totaldistance+=apidistance;
                            Log.e("totaldistance", String.valueOf(totaldistance));
                            Log.e("apidistance", String.valueOf(apidistance));
                            Log.e("pLat", String.valueOf(pLat));
                            Log.e("pLong", String.valueOf(pLong));
                            Log.e("cLat", String.valueOf(cLat));
                            Log.e("cLong", String.valueOf(cLong));
                            // Log.e(String.valueOf(totaldistance)+"~"+apidistance+"~"+pLat+"~"+pLong+"~"+cLat+"~"+cLong);
                            Log.e("value API", String.valueOf(apidistance));
                            apiamount = apidistance * apirefvalue / 1000;
                            totalamount+=apiamount;
                            Log.e("API Amount before round", String.valueOf(apiamount));

                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            protected Void doInBackground(Void... params) {
                String distanceURL = Utils.strdistance + pLat + "," + pLong + destination + cLat + "," + cLong + apiKey;
                Log.e(TAG, "distanceURL: " + distanceURL);
                JsonServiceHandler = new JsonServiceHandler(distanceURL, DistanceCalculationActivity.this);
                Log.e(TAG, "JsonServiceHandler: " + JsonServiceHandler.toString());
                JsonAccountObject = JsonServiceHandler.ServiceData();
                Log.e(TAG, "JsonAccountObject: " + JsonAccountObject.toString());
                return null;

            }
        }.execute(new Void[]{});


    }



    public void setadap() {
        // TODO Auto-generated method stub
        Log.e("size", String.valueOf(arrayListObjects.size()));
        Log.e("adapter", "adapter");
        adapter = new DistanceCalculationAdapter(DistanceCalculationActivity.this,
                R.layout.item_distance_screen, arrayListObjects);
        textViewNoData.setVisibility(View.GONE);
        listViewDistance.setVisibility(View.VISIBLE);
        listViewDistance.setAdapter(adapter);

    }
    public void UnSynData() {
        // TODO Auto-generated method stub
        Log.e("UnSyncData","UnSyncData");
        Cursor cursorUnSyncLogs=databaseHandler.getDatewiseUnSyncedSmGeoLogs();
        if(cursorUnSyncLogs!=null){
            System.out.println("InsideUnSynData_cursorUnSyncLogs:"+databaseHandler.getCursorItems(cursorUnSyncLogs));}
        Log.e("cursorUnSyncLogs--", String.valueOf(cursorUnSyncLogs));
        cursorUnSyncLogs=databaseHandler.getDatewiseUnSyncedSmGeoLogs();
        cursorUnSyncLogs.moveToFirst();
        if(cursorUnSyncLogs.getCount()>0) {
            Log.e("cursorUnSyncLogs > 0", String.valueOf(cursorUnSyncLogs.getCount()));
            ArrayList<DistanceCalculationDomain> arrayListObjects;
            arrayListObjects = new ArrayList<DistanceCalculationDomain>();
            DistanceCalculationDomain dod = new DistanceCalculationDomain();
            Log.e("dod--", String.valueOf(dod));
            Log.e("dod1","dod1");
            dod.setLogtime(cursorUnSyncLogs.getString(0));
            Log.e("logtimeUnsynData", cursorUnSyncLogs.getString(0));
            dod.setTraveldistance(cursorUnSyncLogs.getString(1));
            Log.e("traveldistance", cursorUnSyncLogs.getString(1));
            dod.setAmount(cursorUnSyncLogs.getString(2));
            Log.e("amount", cursorUnSyncLogs.getString(2));
            arrayListObjects.add(dod);
            Log.e("arrayListObjects--", String.valueOf(arrayListObjects));
            Log.e("sizeUnSync", String.valueOf(arrayListObjects.size()));
            Log.e("adapterUnSync", "adapterUnSync");
            DistanceCalculationAdapterUnSync adapterUnSync = new DistanceCalculationAdapterUnSync(DistanceCalculationActivity.this,
                    R.layout.item_distance_screen, arrayListObjects);
            Log.e("adapterUnSync", String.valueOf(adapterUnSync));
            textViewNoData1.setVisibility(View.GONE);
            listViewDistance1.setVisibility(View.VISIBLE);
            listViewDistance1.setAdapter(adapterUnSync);
        }
    }


    private void getDetails(final String paymentcheck) {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "";

            @Override
            protected void onPreExecute() {
                dialog = ProgressDialog.show(DistanceCalculationActivity.this, "",
                        "Loading...", true, true);

            }

            @Override
            protected void onPostExecute(Void result) {

                try {

                    PaymentStatus = (JsonAccountObject.getString("pymtstatus"));
                    Log.e("PaymentStatus", PaymentStatus);


                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Log.e("paymentcheck", paymentcheck);

                        if (paymentcheck.equals("profile")) {
                            Intent io = new Intent(DistanceCalculationActivity.this,
                                    ProfileActivity.class);
                            startActivity(io);
                            finish();

                        } else if (paymentcheck.equals("myorders")) {
                            Intent io = new Intent(DistanceCalculationActivity.this,
                                    SalesManOrderActivity.class);
                            startActivity(io);
                            finish();

                        } else if (paymentcheck.equals("mydealer")) {
                            Intent io = new Intent(DistanceCalculationActivity.this,
                                    MyDealersActivity.class);
                            io.putExtra("Key", "menuclick");
                            startActivity(io);
                            finish();

                        } else if (paymentcheck.equals("postnotes")) {
                            Intent io = new Intent(DistanceCalculationActivity.this,
                                    MerchantComplaintActivity.class);
                            startActivity(io);
                            finish();

                        } else if (paymentcheck.equals("placeorder")) {
                            Constants.imagesetcheckout = 1;
                            Intent to = new Intent(DistanceCalculationActivity.this,
                                    SalesManOrderCheckoutActivity.class);
                            startActivity(to);
                            finish();
                        } else if (paymentcheck.equals("clientvisit")) {
                            Intent to = new Intent(DistanceCalculationActivity.this,
                                    SMClientVisitHistory.class);
                            startActivity(to);
                            finish();
                        } else if (paymentcheck.equals("paymentcoll")) {
                            Intent to = new Intent(DistanceCalculationActivity.this,
                                    SalesmanPaymentCollectionActivity.class);
                            startActivity(to);
                            finish();
                        } else if (paymentcheck.equals("addmerchant")) {
                            Intent to = new Intent(DistanceCalculationActivity.this,
                                    AddMerchantNew.class);
                            startActivity(to);
                            finish();
                        } else if (paymentcheck.equals("acknowledge")) {
                            Intent to = new Intent(DistanceCalculationActivity.this,
                                    SalesmanAcknowledgeActivity.class);
                            startActivity(to);
                            finish();
                        } else if (paymentcheck.equals("stockentry")) {
                            Intent to = new Intent(DistanceCalculationActivity.this,
                                    OutletStockEntry.class);
                            startActivity(to);
                            finish();
                        } else if (paymentcheck.equals("surveyuser")) {
                            Intent to = new Intent(DistanceCalculationActivity.this,
                                    SurveyMerchant.class);
                            startActivity(to);
                            finish();
                        } else if (paymentcheck.equals("mydayplan")) {
                            Intent to = new Intent(DistanceCalculationActivity.this,
                                    MyDayPlan.class);
                            startActivity(to);
                            finish();
                        } else if (paymentcheck.equals("mysales")) {
                            Intent to = new Intent(DistanceCalculationActivity.this,
                                    MySalesActivity.class);
                            startActivity(to);
                            finish();
                        } else if (paymentcheck.equals("diststock")) {
                            Intent io = new Intent(DistanceCalculationActivity.this,
                                    DistributrStockActivity.class);
                            startActivity(io);
                            finish();
                        }

                    }

                    dialog.dismiss();


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();


                JsonServiceHandler = new JsonServiceHandler(Utils.strGetProfileDetail + Constants.USER_ID, DistanceCalculationActivity.this);
                JsonAccountObject = JsonServiceHandler.ServiceDataGet();
                return null;
            }
        }.execute(new Void[]{});
    }



    public StoppableRunnable mTask = new StoppableRunnable() {
        public void stoppableRun() {
            onStartThread(Constants.TRACK_INTERVAL*1000);
        }
    };

    public void   onStopThread() {
        mTask.stop();

        mHandler.removeCallbacks(mTask);
    }

    public void onStartThread(long delayMillis) {

        if(trackSet.length()>=Constants.GEO_RECORDS_UPLOAD_LIMIT)
        {
            Log.e("->data to db server...","RecCount:"+trackSet.length()+"Obj:"+trackSet.toString());
            tracklog();
        }
        db = openOrCreateDatabase("freshorders", 0, null);
        gps = new GPSTracker(DistanceCalculationActivity.this);
        if (gps.canGetLocation() == true) {
            double latitude = gps.getLatitude();
            double longitude = gps.getLongitude();
            cLat = latitude;
            cLong = longitude;
            if(pLat==0) {
                pLat = cLat;

            }

            if(pLong==0) {
                pLong = cLong;

            }
            //  AmountCalculation();
            distanceAPI(String.valueOf(pLat), String.valueOf(pLong), String.valueOf(cLat), String.valueOf(cLong));
            pLat = cLat;
            pLong = cLong;
            Date d = new Date();
            SimpleDateFormat ft = new SimpleDateFormat("yyyy-MMM-dd HH:mm:ss");
            date = ft.format(d).toString();
            Log.e("time", date);
            JSONObject jsonObject = new JSONObject();

            try {

                jsonObject.put("suserid", Constants.USER_ID);
                jsonObject.put("logtime", date);
                jsonObject.put("geolat", cLat);
                jsonObject.put("geolong", cLong);
                jsonObject.put("pgeolat", pLat);
                jsonObject.put("pgeolong", pLong);
                jsonObject.put("status", status);
                jsonObject.put("updateddt", date);
                jsonObject.put("distance", apidistance);
                jsonObject.put("amount", apiamount);
                trackSet.put(jsonObject);
                Log.e("distance", String.valueOf(apidistance));
                Log.e("amount", String.valueOf(apiamount));
                Log.e("pgeolong", String.valueOf(pLong));
                Log.e("trackSet API", String.valueOf(trackSet));
                Log.e("trackSet count", String.valueOf(trackSet.length()));

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }
        mHandler.postDelayed(mTask, delayMillis);

    }


    static JSONArray trackSet = null;
    ArrayList arlTrackSet= null;
    double apidistance=0,apiamount=0;
    static double apirefvalue=0;
    static double totaldistance=0,totalamount=0;

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(DistanceCalculationActivity.this);

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.delete);

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, 2);

            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    /*  public boolean netCheck() {
          // for network connection
          try {
              ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
              NetworkInfo mWifi = connManager
                      .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

              ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
              NetworkInfo mNetwork = connectionManager
                      .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

              Object result = null;
              if (mWifi.isConnected() || mNetwork.isConnected()) {
                  return true;
              } else if (result == null) {
                  showAlertDialogToast(
                          "No Internet Connection, Please Check Your internet connection.");
                  return false;
              }
          } catch (Exception e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
          }
          return false;
      }
  */
    public void showAlertDialogToast(String message) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setMessage(message);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert = builder.create();
        alert.show();

        Button buttonbackground = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }
    public void showAlertDialogToast1(String message) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setMessage(message);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent into=new Intent(DistanceCalculationActivity.this,BlankActivity.class);
                        startActivity(into);
                    }
                });


        AlertDialog alert = builder.create();
        alert.show();

        Button buttonbackground = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    public void onBackPressed() {
        exitAlret();
    }

    private void exitAlret() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        localBuilder.setMessage("Do you want to Exit?");
        localBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {

                        finish();

                    }
                });
        localBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {
                        paramDialogInterface.cancel();
                    }
                });
        localBuilder.create().show();
    }

    public boolean netCheckwithoutAlert() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            } else if (result == null) {

                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }


    public void showAlertDialog(Context context, String title, String message,
                                Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        alertDialog.show();
    }

    public void stopThreadAction() {
        isTRunning=false;
        DecimalFormat format = new DecimalFormat("##.00");
        String strTtoatlDistance="0 km";
        if(totaldistance >=1000)
        {
            strTtoatlDistance= format.format(totaldistance/1000) + " km";
        }
        else
        {
            strTtoatlDistance= totaldistance + " m";
        }
        distanceAPI(String.valueOf(pLat), String.valueOf(pLong), String.valueOf(cLat), String.valueOf(cLong));
        showAlertDialogToast("You total distance covered is " + strTtoatlDistance + " and your allowence is" + format.format(totalamount));
        tracklog();
        onStopThread();
        discal();
    }


}