package com.freshorders.freshorder.ui;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.AddEodAdapter;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.AddEodDomain;
import com.freshorders.freshorder.toonline.migration.PendingDataMigrationActivity;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.GPSTracker;
import com.freshorders.freshorder.utils.GPSTrackerActivity;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Pavithra on 11-01-2017.
 */
public class EODActivity extends Activity {
    double EODgeolat,EODgeolog;
    String date,name,PaymentStatus="NULL",currentDate;
    public static JSONObject JsonAccountObject = null;
    public static JSONArray JsonAccountArray = null;
    public static JsonServiceHandler JsonServiceHandler ;
    TextView textViewDate,textViewSalesmanName,textViewDistrict,textViewTotalorder,
            textViewTotalSales,textViewmild200,textviewnodata;
    public static DatabaseHandler databaseHandler;
    Button Share;
    SQLiteDatabase db;
    AddEodDomain domainSelected;
    public static int merchantselected=0;
    TextView menuIcon, textViewAssetMenuProfile, textViewAssetMenuMyOrders,
            textViewAssetMenuMyDealers, textViewAssetMenuProducts,
            textViewAssetMenuPayment, textViewAssetMenuComplaint,
            textViewAssetMenuSignout,textViewAssetMenuClientVisit,textViewAssetMenuPaymentCollection,
            textViewNodata,textViewAssetMenuCreateOrder,textViewcross_1,textViewAssetMenuRefresh
            ,textViewAssetMenuStockEntry,textViewAssetMenuAddMerchant,textViewAssetMenuAcknowledge
            ,textViewAssetMenusurveyuser,textViewAssetMenudownloadscheme,textviewtotalcalls,textViewPC,
            textViewcp100,textViewcp200,textViewcp300,textViewperfecttotal,textViewmild100,textViewurich500,
            textViewurn100,textViewCT100,textViewCT200,textViewtotaltrisakthi,textViewts100,textViewTA100,
            textViewTA250,edittextroute,edittexttown,textViewNewOutlet,
            textViewAssetMenuStockOnly, textViewAssetMenuMigration, textViewAssetMenuStockAudit,
            textViewAssetMenuBack, textViewAssetMenuHome;


    LinearLayout linearLayoutMenuParent, linearLayoutProfile,
            linearLayoutMyOrders, linearLayoutMyDealers, linearLayoutProducts,
            linearLayoutPayment,linearLayoutPaymentCollection, linearLayoutComplaint, linearLayoutSignout,
            linearlayoutSearchIcon,linearLayoutCreateOrder,linearLayoutClientVisit,linearLayoutRefresh,
            linearLayoutAcknowledge,linearLayoutStockEntry,linearLayoutAddMerchant,
            linearLayoutsurveyuser,linearLayoutdownloadscheme, linearLayoutStockOnly, linearLayoutMigration, linearLayoutStockAudit,
            linearLayoutHome, linearLayoutBack;

    private void showMenu(){

        MyApplication app = MyApplication.getInstance();

        if(!app.isCreateOrder()){
            linearLayoutCreateOrder.setVisibility(View.GONE);
        }
        if(!app.isProfile()){
            linearLayoutProfile.setVisibility(View.GONE);
        }
        if(!app.isMyDayPlan()){
            //linearLayoutMyDayPlan.setVisibility(View.GONE);
        }
        if(!app.isMyOrders()){
            linearLayoutMyOrders.setVisibility(View.GONE);
        }
        if(!app.isAddMerchant()){
            linearLayoutAddMerchant.setVisibility(View.GONE);
        }
        if(!app.isMySales()){
            //linearLayoutMySales.setVisibility(View.GONE);
        }
        if(!app.isPostNotes()){
            linearLayoutComplaint.setVisibility(View.GONE);
        }
        if(!app.isMyClientVisit()){
            linearLayoutClientVisit.setVisibility(View.GONE);
        }
        if(!app.isAcknowledge()){
            linearLayoutAcknowledge.setVisibility(View.GONE);
        }
        if(!app.isPaymentCollection()){
            linearLayoutPaymentCollection.setVisibility(View.GONE);
        }
        if(!app.isPkdDataCapture()){
            linearLayoutStockEntry.setVisibility(View.GONE);
        }
        if(!app.isDistributorStock()){
            //linearLayoutDistStock.setVisibility(View.GONE);
        }
        if(!app.isSurveyUser()){
            linearLayoutsurveyuser.setVisibility(View.GONE);
        }
        if(!app.isDownLoadScheme()){
            linearLayoutdownloadscheme.setVisibility(View.GONE);
        }
        if(!app.isDistanceCalculation()){
            //linearLayoutDistanceCalculation.setVisibility(View.GONE);
        }
        if(!app.isRefresh()){
            linearLayoutRefresh.setVisibility(View.GONE);
        }
        if(!app.isLogout()){
            linearLayoutSignout.setVisibility(View.GONE);
        }
        if(!app.isClosingStock()){
            linearLayoutStockOnly.setVisibility(View.GONE);
        }
        if(!app.isClosingStockAudit()){
            linearLayoutStockAudit.setVisibility(View.GONE);
        }
        if(!app.isPendingData()){
            linearLayoutMigration.setVisibility(View.GONE);
        }

        ///////////////////////////
        linearLayoutHome.setVisibility(View.VISIBLE);
        linearLayoutBack.setVisibility(View.VISIBLE);
    }

    public static int menuCliccked;
      Double totalSale=0.0;

    EditText editTextdetails1,editTextdetails2;
    public static String detail1,detail2,totalCalls="0",pC,perfect100gm="0",perfect200gm="0",perfect500gm="0",perfectTotal="0",
    mild100gm="0",uRich500gm="0",ultraRichNewPc="0",trisakthi100gm="0",trisakthi200gm="0",totalTrisakthi="0",sTea100gm="0",
            dTea100gm="0",dtea250gm="0",totalteaSales="0",grandTeaSales="0",newOutlet="0",mild200gm="0";

    GPSTracker gps;
    String DuserID;
    ArrayList<AddEodDomain> arraylist;
    RecyclerView recyclerView;
    //Kumaravel
    private TextView tvSalesValue;
    private String strSalesValue = "0";
    private double totalSalesValue = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.eod_activity);
        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();
        textViewDate = (TextView)findViewById(R.id.textViewDate);
        textViewSalesmanName = (TextView)findViewById(R.id.textViewSalesmanName);
        textviewnodata = (TextView)findViewById(R.id.textviewnodata);
        edittextroute = (EditText)findViewById(R.id.edittextroute);
        edittexttown = (EditText)findViewById(R.id.edittexttown);

        //textViewDistrict = (TextView)findViewById(R.id.textViewDistrict);
       // textViewTotalorder = (TextView)findViewById(R.id.textViewTotalorder);
       // textViewTotalSales = (TextView)findViewById(R.id.textViewTotalSales);
        Share = (Button)findViewById(R.id.Share);
        gps = new GPSTracker(EODActivity.this);
        textViewAssetMenuRefresh = (TextView) findViewById(R.id.textViewAssetMenuRefresh);
        menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
        textViewAssetMenuCreateOrder= (TextView) findViewById(R.id.textViewAssetMenuCreateOrder);

        textViewAssetMenuProfile = (TextView) findViewById(R.id.textViewAssetMenuProfile);
        textViewAssetMenuMyOrders = (TextView) findViewById(R.id.textViewAssetMenuMyOrders);
        textViewAssetMenuMyDealers = (TextView) findViewById(R.id.textViewAssetMenuMyDealers);
        textViewAssetMenuProducts = (TextView) findViewById(R.id.textViewAssetMenuProducts);
        textViewAssetMenuPayment = (TextView) findViewById(R.id.textViewAssetMenuPayment);
        textViewAssetMenuComplaint = (TextView) findViewById(R.id.textViewAssetMenuComplaint);
        textViewAssetMenuSignout = (TextView) findViewById(R.id.textViewAssetMenuSignout);
        textViewAssetMenuClientVisit = (TextView) findViewById(R.id.textViewAssetClientVisit);
        textViewNewOutlet= (TextView) findViewById(R.id.textViewNewOutlet);
        textViewAssetMenuAddMerchant = (TextView) findViewById(R.id.textViewAssetMenuAddMerchant);
        textViewAssetMenuAcknowledge = (TextView) findViewById(R.id.textViewAssetMenuAcknowledge);
        textViewAssetMenuStockEntry = (TextView) findViewById(R.id.textViewAssetMenuStockEntry);

        textViewAssetMenudownloadscheme= (TextView) findViewById(R.id.textViewAssetMenudownloadscheme);
        textViewAssetMenusurveyuser= (TextView) findViewById(R.id.textViewAssetMenusurveyuser);
        textViewAssetMenuPaymentCollection= (TextView) findViewById(R.id.textViewAssetMenuPaymentCollection);

        textViewAssetMenuStockOnly = (TextView) findViewById(R.id.textViewAssetMenuStockOnly);
        textViewAssetMenuStockAudit = (TextView) findViewById(R.id.textViewAssetMenuStockAudit);
        textViewAssetMenuMigration = findViewById(R.id.textViewAssetMenuMigration);

        textViewAssetMenuBack = findViewById(R.id.textViewAssetMenuBack);
        textViewAssetMenuHome = findViewById(R.id.textViewAssetMenuHome);



        textviewtotalcalls = (TextView) findViewById(R.id.textviewtotalcalls);
        textViewPC = (TextView) findViewById(R.id.textViewPC);
        tvSalesValue = (TextView) findViewById(R.id.textViewSalesValue);// Kumaravel
        recyclerView=(RecyclerView)findViewById(R.id.recycler_view);
        textViewcp100 = (TextView) findViewById(R.id.textViewcp100);
       /* textViewcp100 = (TextView) findViewById(R.id.textViewcp100);
       textViewcp200 = (TextView) findViewById(R.id.textViewcp200);
        textViewcp300 = (TextView) findViewById(R.id.textViewcp300);
        textViewperfecttotal = (TextView) findViewById(R.id.textViewperfecttotal);
        textViewmild100 = (TextView) findViewById(R.id.textViewmild100);
        textViewmild100 = (TextView) findViewById(R.id.textViewmild100);

        textViewurich500 = (TextView) findViewById(R.id.textViewurich500);
        textViewurn100 = (TextView) findViewById(R.id.textViewurn100);
        textViewCT100 = (TextView) findViewById(R.id.textViewCT100);
        textViewCT200 = (TextView) findViewById(R.id.textViewCT200);
        textViewtotaltrisakthi = (TextView) findViewById(R.id.textViewtotaltrisakthi);
        textViewts100 = (TextView) findViewById(R.id.textViewts100);
        textViewTA100 = (TextView) findViewById(R.id.textViewTA100);
        textViewTA250 = (TextView) findViewById(R.id.textViewTA250);
        textViewmild200= (TextView) findViewById(R.id.textViewmild200);
*/
        databaseHandler = new DatabaseHandler(getApplicationContext());

        linearLayoutRefresh= (LinearLayout) findViewById(R.id.linearLayoutRefresh);
        linearLayoutCreateOrder = (LinearLayout) findViewById(R.id.linearLayoutCreateOrder);
        linearLayoutProfile = (LinearLayout) findViewById(R.id.linearLayoutProfile);
        linearLayoutMyOrders = (LinearLayout) findViewById(R.id.linearLayoutMyOrders);
        linearLayoutMyDealers = (LinearLayout) findViewById(R.id.linearLayoutMyDealers);
        linearLayoutProducts = (LinearLayout) findViewById(R.id.linearLayoutProducts);
        linearLayoutPayment = (LinearLayout) findViewById(R.id.linearLayoutPayment);
        linearLayoutComplaint = (LinearLayout) findViewById(R.id.linearLayoutComplaint);
        linearLayoutSignout = (LinearLayout) findViewById(R.id.linearLayoutSignout);
        linearLayoutClientVisit= (LinearLayout) findViewById(R.id.linearLayoutClientVisit);
        linearLayoutPaymentCollection= (LinearLayout) findViewById(R.id.linearLayoutPaymentCollection);

        linearLayoutAcknowledge= (LinearLayout) findViewById(R.id.linearLayoutAcknowledge);
        linearLayoutStockEntry= (LinearLayout) findViewById(R.id.linearLayoutStockEntry);
        linearLayoutAddMerchant = (LinearLayout) findViewById(R.id.linearLayoutAddMerchant);

        linearLayoutMenuParent = (LinearLayout) findViewById(R.id.linearLayoutMenuParent);
        linearLayoutdownloadscheme= (LinearLayout) findViewById(R.id.linearLayoutdownloadscheme);
        linearLayoutsurveyuser= (LinearLayout) findViewById(R.id.linearLayoutsurveyuser);

        linearLayoutStockOnly = (LinearLayout) findViewById(R.id.linearLayoutStockOnly);
        linearLayoutStockAudit = findViewById(R.id.linearLayoutStockAudit);
        linearLayoutMigration = (LinearLayout) findViewById(R.id.linearLayoutMigration);

        linearLayoutHome = (LinearLayout) findViewById(R.id.linearLayoutHome);
        linearLayoutBack = (LinearLayout) findViewById(R.id.linearLayoutBack);


        Typeface font = Typeface.createFromAsset(getAssets(),
                "fontawesome-webfont.ttf");
        menuIcon.setTypeface(font);
        textViewAssetMenuCreateOrder.setTypeface(font);
        textViewAssetMenuProfile.setTypeface(font);
        textViewAssetMenuMyOrders.setTypeface(font);
        textViewAssetMenuMyDealers.setTypeface(font);
        textViewAssetMenuProducts.setTypeface(font);
        textViewAssetMenuPayment.setTypeface(font);
        textViewAssetMenuComplaint.setTypeface(font);
        textViewAssetMenuSignout.setTypeface(font);
        textViewAssetMenuClientVisit.setTypeface(font);
        textViewAssetMenuPaymentCollection.setTypeface(font);
        textViewAssetMenuRefresh.setTypeface(font);

        textViewAssetMenuAcknowledge.setTypeface(font);
        textViewAssetMenuStockEntry.setTypeface(font);
        textViewAssetMenuAddMerchant.setTypeface(font);
        textViewAssetMenusurveyuser.setTypeface(font);
        textViewAssetMenudownloadscheme.setTypeface(font);

        textViewAssetMenuStockOnly.setTypeface(font);
        textViewAssetMenuStockAudit.setTypeface(font);
        textViewAssetMenuMigration.setTypeface(font);

        textViewAssetMenuBack.setTypeface(font);
        textViewAssetMenuHome.setTypeface(font);

        if (Constants.USER_TYPE.equals("D")) {
            linearLayoutMyDealers.setVisibility(View.GONE);
            linearLayoutProducts.setVisibility(View.VISIBLE);
            linearLayoutCreateOrder.setVisibility(View.GONE);
        } else if (Constants.USER_TYPE.equals("M")){
            linearLayoutMyDealers.setVisibility(View.VISIBLE);
            linearLayoutProducts.setVisibility(View.GONE);
            linearLayoutCreateOrder.setVisibility(View.GONE);
        }else {
            linearLayoutMyDealers.setVisibility(View.GONE);
            linearLayoutProducts.setVisibility(View.GONE);
            linearLayoutCreateOrder.setVisibility(View.VISIBLE);
        }
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());

        recyclerView.setLayoutManager(mLayoutManager);

        Cursor cur;
        cur = databaseHandler.getDetails();
        cur.moveToFirst();
        name=cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_name));
                Log.e("Insertion Check", name);

         date = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss").format(new java.util.Date());
        Log.e("date", date);
        currentDate= new SimpleDateFormat("yyyy-MM-dd").format(new Date());

        //Get the DuserID
        cur = databaseHandler.getdealer();
        Log.e("dealercount", String.valueOf(cur.getCount()));


        if (cur != null && cur.getCount() > 0) {
            if (cur.moveToLast()) {

                DuserID = cur.getString(3);
                Log.e("DuserID", DuserID);
            }
        }

        int permissionCheck1 = ContextCompat.checkSelfPermission(EODActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);

        if (permissionCheck1 != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    EODActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.LOCATION_PERMISSION_REQUEST_CODE);
        }else{

/*

            try {
                getcurrentlocation();
            } catch (IOException e) {
                e.printStackTrace();
            }
*/

        }

        String date1 = "yyyy-MM-dd";

        SimpleDateFormat inputFormat = new SimpleDateFormat(date1);
        String dt =inputFormat.format(new Date());
        Cursor curs;
        curs = databaseHandler.getSuccessheader("Success",dt);
        Log.e("HeaderCount", String.valueOf(curs.getCount()));
        if (curs != null && curs.getCount() > 0) {
            curs.moveToFirst();
            totalSale = 0.0;
            while (!curs.isAfterLast()) {
                totalSale = totalSale + Double.parseDouble(curs.getString(9).toString()) ;
                curs.moveToNext();
            }
        }

        String newDate = new SimpleDateFormat("yyyy-MM-dd hh:mm aa",Locale.getDefault()).format(new java.util.Date());
        Log.e("date", date);
        textViewDate.setText("Date : "+"\t"+newDate);
        //textViewDate.setText("Date : "+"\t"+date);
        textViewSalesmanName.setText("Salesman Name : "+"\t"+name);
     //   getEOD();
        Cursor cur1;
        cur1 = databaseHandler.getDetails();
        cur1.moveToFirst();

        Constants.USER_ID=cur1.getString(cur1
                .getColumnIndex(DatabaseHandler.KEY_id));
        Log.e("SuserID",Constants.USER_ID);
        DuserID= cur1.getString(cur1
                .getColumnIndex(DatabaseHandler.KEY_clientdealid));
        Log.e("DuserID",DuserID);


        if(Constants.USER_ID!=null &&DuserID!=null)
        {
            if(netCheck()) {
                getEod(DuserID, Constants.USER_ID, currentDate);
            }

        }
        String prodcode,prodname,qtotal,uom;

        Share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // edittexttown.setText("Town :  " + "\t" + edittexttown.getText().toString());
               // edittextroute.setText("Route : " + " \t" + edittextroute.getText().toString());
                String town="Town :  " + "\t" + edittexttown.getText().toString();
                String route="Route :  " + "\t" + edittextroute.getText().toString();
                //Append the arraylist in String

                if(arraylist.size()!=0){

                    list();
                }
                String content = textViewDate.getText().toString() + "\n" +
                        textViewSalesmanName.getText().toString() + "\n" +
                        town + "\n" +
                        route + "\n" +
                        textviewtotalcalls.getText().toString() + "\n" +
                        textViewPC.getText().toString() + "\n" +
                        textViewNewOutlet.getText().toString() + "\n" +
                        tvSalesValue.getText().toString() + "\n" +



                 /*  for (int i = 0; i < AddEodDomain.size();i++) {
                       prodcode[i] = AddEodDomain.get(i).getprodcode();
                       prodname[i] = AddEodDomain.get(i).getprodname();
                       qtotal[i] = AddEodDomain.get(i).getqtotal();
                       uom[i] = AddEodDomain.get(i).getuom();
                }
                  */      /*textViewcp100.getText().toString()+"\n"+
                        textViewcp200.getText().toString()+"\n"+
                        textViewcp300.getText().toString()+"\n"+
                        textViewperfecttotal.getText().toString()+"\n"+
                        textViewmild100.getText().toString()+"\n"+
                        textViewmild200.getText().toString()+"\n"+
                        textViewurich500.getText().toString()+"\n"+
                        textViewurn100.getText().toString()+"\n"+
                        textViewCT100.getText().toString()+"\n"+
                        textViewCT200.getText().toString()+"\n"+
                        textViewts100.getText().toString()+"\n"+
                        textViewTA100.getText().toString()+"\n"+
                        textViewTA250.getText().toString()+"\n"+
                        textViewts100.getText().toString()+"\n"+
                        textViewtotaltrisakthi.getText().toString()+"\n"+
                        textViewTotalorder.getText().toString()+"\n"+
                        textViewTotalSales.getText().toString()+"\n"+*/
                        textViewcp100.getText().toString();

                if(netCheck()) {


                    Intent intent2 = new Intent();
                    intent2.setAction(Intent.ACTION_SEND);
                    intent2.setType("text/plain");
                    intent2.putExtra(android.content.Intent.EXTRA_SUBJECT, "EOD Report");
                    intent2.putExtra(Intent.EXTRA_TEXT, content);
                    startActivity(Intent.createChooser(intent2, "Share via"));
                }
            }
        });

        menuIcon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (menuCliccked == 0) {
                    linearLayoutMenuParent.setVisibility(View.VISIBLE);

                    InputMethodManager iss = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    iss.hideSoftInputFromWindow(menuIcon.getWindowToken(), 0);
                    menuCliccked = 1;
                } else {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                   
                    InputMethodManager iss = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    iss.hideSoftInputFromWindow(menuIcon.getWindowToken(), 0);
                    menuCliccked = 0;
                }

            }
        });


        linearLayoutMenuParent.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);

                menuCliccked = 0;
            }
        });

        linearLayoutCreateOrder.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                MyApplication.getInstance().setTemplate8ForTemplate2(false);

                if (netCheck() == true) {
                    Constants.checkproduct = 0;
                    Constants.orderid = "";
                    Constants.SalesMerchant_Id = "";
                    Constants.Merchantname = "";
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    getDetails("Createorder");
                }else{
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    }else{
                        Intent io = new Intent(EODActivity.this,
                                CreateOrderActivity.class);
                        startActivity(io);
                        finish();
                    }
                }

            }
        });

        linearLayoutProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub

                if(netCheck()==true){
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Constants.Merchantname="";
                    Constants.SalesMerchant_Id="";
                    merchantselected=0;
                    getDetails("profile");
                }else{
                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });

        linearLayoutMyOrders.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(netCheck()==true) {
                    Constants.orderstatus = "Success";
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Constants.Merchantname="";
                    Constants.SalesMerchant_Id="";
                    merchantselected=0;
                    getDetails("myorders");
                }else{
                    if(PaymentStatus.equals("Pending")||PaymentStatus.equals(null) ||PaymentStatus.equals("")||PaymentStatus.equals("null")){
                        showAlertDialogToast("Please make payment before place order");
                    }else{
                        Intent io = new Intent(EODActivity.this,
                                SalesManOrderActivity.class);
                        startActivity(io);
                        finish();
                    }
                }


            }
        });

        linearLayoutRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("Clicked", "Clicked");

                if(netCheck()){

                    final Dialog qtyDialog = new Dialog(EODActivity.this);
                    qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    qtyDialog.setContentView(R.layout.warning_dialog);

                    final Button buttonUpdateOk, buttonUpdateCancel;

                    final TextView textQtyValidate;

                    buttonUpdateOk = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateOk);
                    buttonUpdateCancel = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateCancel);

                    textQtyValidate = (TextView) qtyDialog
                            .findViewById(R.id.textQtyValidate);
                    qtyDialog.show();


                    buttonUpdateOk.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            databaseHandler.synTableDelete();
                            Constants.refreshscreen="createorder";
                            Intent io = new Intent(EODActivity.this, RefreshActivity.class);
                            startActivity(io);
                            finish();
                            qtyDialog.dismiss();
                        }

                    });

                    buttonUpdateCancel
                            .setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub

                                    qtyDialog.dismiss();

                                }
                            });

                }else{
                    showAlertDialogToast("Please Check Your internet connection");
                }
            }

        });


        linearLayoutMyDealers.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(netCheck()==true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Constants.Merchantname="";
                    Constants.SalesMerchant_Id="";
                    merchantselected=0;
                    getDetails("mydealer");
                }else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(EODActivity.this,
                                MyDealersActivity.class);
                        io.putExtra("Key","menuclick");
                        startActivity(io);
                        finish();

                    }
                }

            }
        });




        linearLayoutComplaint.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //if (netCheck() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Constants.Merchantname="";
                    Constants.SalesMerchant_Id="";
                    merchantselected=0;
                    getDetails("postnotes");
                //}else {
                //    showAlertDialogToast("Please Check Your internet connection");
                //}
            }
        });
        linearLayoutClientVisit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Constants.Merchantname="";
                    Constants.SalesMerchant_Id="";

                    // TODO Auto-generated method stub
                    merchantselected = 0;
                    getDetails("clientvisit");
                }else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    }else{
                        Intent to = new Intent(EODActivity.this,
                                SMClientVisitHistory.class);
                        startActivity(to);
                        finish();
                    }
                }

            }
        });

        linearLayoutPaymentCollection.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Constants.Merchantname="";
                    Constants.SalesMerchant_Id="";
                    merchantselected=0;
                    getDetails("paymentcoll");
                }else {
                    showAlertDialogToast("Please Check Your internet connection");
                }


            }
        });

        linearLayoutAcknowledge.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Constants.Merchantname="";
                    Constants.SalesMerchant_Id="";
                    merchantselected=0;
                    getDetails("acknowledge");
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }


            }
        });
        linearLayoutStockEntry.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Constants.Merchantname="";
                    Constants.SalesMerchant_Id="";
                    merchantselected=0;
                    getDetails("stockentry");
                }else {
                    showAlertDialogToast("Please Check Your internet connection");
                }


            }
        });


        linearLayoutAddMerchant.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(netCheck()==true) {
                    Constants.orderstatus = "Success";
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Constants.Merchantname="";
                    Constants.SalesMerchant_Id="";
                    merchantselected=0;
                    getDetails("addmerchant");
                }else{
                    if(PaymentStatus.equals("Pending")||PaymentStatus.equals(null) ||PaymentStatus.equals("")||PaymentStatus.equals("null")){
                        showAlertDialogToast("Please make payment before place order");
                    }else{
                        Intent io = new Intent(EODActivity.this,
                                AddMerchantNew.class);
                        startActivity(io);
                        finish();
                    }
                }


            }
        });
        linearLayoutdownloadscheme.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(netCheck()){

                    final Dialog qtyDialog = new Dialog(EODActivity.this);
                    qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    qtyDialog.setContentView(R.layout.warning_dialog);

                    final Button buttonUpdateOk, buttonUpdateCancel;

                    final TextView textQtyValidate;

                    buttonUpdateOk = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateOk);
                    buttonUpdateCancel = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateCancel);

                    textQtyValidate = (TextView) qtyDialog
                            .findViewById(R.id.textQtyValidate);
                    qtyDialog.show();


                    buttonUpdateOk.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            databaseHandler.deletescheme();
                            Constants.downloadScheme = "createorder";
                            Intent io = new Intent(EODActivity.this, SchemeDownload.class);
                            startActivity(io);
                            finish();
                            qtyDialog.dismiss();
                        }

                    });

                    buttonUpdateCancel
                            .setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub

                                    qtyDialog.dismiss();

                                }
                            });

                }else{
                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });
        linearLayoutsurveyuser.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //if (netCheck() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    getDetails("surveyuser");
                //}else {
                //    showAlertDialogToast("Please Check Your internet connection");
                //}

              /*  Intent to = new Intent(SalesManOrderActivity.this,
                        SalesmanAcknowledgeActivity.class);
                startActivity(to);
                finish();*/

            }
        });

        linearLayoutSignout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                // TODO Auto-generated method stub
                Constants.Merchantname="";
                Constants.SalesMerchant_Id="";
                merchantselected=0;

                databaseHandler.delete();
                Intent io = new Intent(EODActivity.this,
                        SigninActivity.class);
                startActivity(io);
                finish();
            }
        });


        linearLayoutMigration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Intent io = new Intent(EODActivity.this,
                        PendingDataMigrationActivity.class);
                startActivity(io);
            }
        });

        linearLayoutBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Intent io = new Intent(EODActivity.this,
                        SalesmanAcknowledgeActivity.class);
                startActivity(io);
            }
        });

        linearLayoutHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Intent io = new Intent(EODActivity.this,
                        SalesManOrderActivity.class);
                startActivity(io);
            }
        });

        linearLayoutStockOnly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Constants.checkproduct = 0;
                Constants.orderid = "";
                Constants.SalesMerchant_Id = "";
                Constants.Merchantname = "";
                MyApplication.getInstance().setTemplate8ForTemplate2(true);
                if (netCheck()) {
                    Intent io = new Intent(EODActivity.this,
                            ClosingStockDashBoardActivity.class);
                    startActivity(io);
                    finish();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals("") || PaymentStatus.equalsIgnoreCase("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(EODActivity.this,
                                ClosingStockDashBoardActivity.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });

        linearLayoutStockAudit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().setTemplate8ForTemplate2(false);
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Constants.checkproduct = 0;
                Constants.orderid = "";
                Constants.SalesMerchant_Id = "";
                Constants.Merchantname = "";
                if (netCheck()) {
                    Intent io = new Intent(EODActivity.this,
                            ClosingStockAudit.class);
                    startActivity(io);
                    finish();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals("") || PaymentStatus.equalsIgnoreCase("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(EODActivity.this,
                                ClosingStockAudit.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });

        //////////////
        showMenu();
        //////////////

    }

    public void list()
    {
        StringBuilder builder=new StringBuilder();
        int i=1;
        for(AddEodDomain product:arraylist)
        {
            builder.append(i+".  "+product.getProdname()+"["+product.getProdcode()+"]"+"\t : "+product.getQtotal()+" "+product.getUom()+"\n\n");
            i++;
        }

        textViewcp100.setText(builder.toString());
    }

    public void getcurrentlocation() throws IOException {

        if (gps.canGetLocation() == true) {

               /* createordergeolat = gps.getLatitude();
            		createordergeolog = gps.getLongitude();

                    		Log.e("lat", String.valueOf(createordergeolat));
            			Log.e("log", String.valueOf(createordergeolog));
*/

            Intent intent = new Intent(this, GPSTrackerActivity.class);
            startActivityForResult(intent,1);

        }else {
            showSettingsAlert();

        }

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1){
          /*  Bundle extras = data.getExtras();
            EODgeolog = extras.getDouble("Longitude");
            EODgeolat = extras.getDouble("Latitude");

            Log.e("lat", String.valueOf(EODgeolat));
                     Log.e("log", String.valueOf(EODgeolog));
                       Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            List<Address> addresses = null;
            try {
                addresses = geocoder.getFromLocation(EODgeolat,EODgeolog, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }

            String address = addresses.get(0).getAddressLine(0);
                      String city = addresses.get(0).getSubLocality();
                      String subcity = addresses.get(0).getLocality();
            String stateName = addresses.get(0).getAdminArea();
                       Log.e("address", address);
                      Log.e("city",city);
            Log.e("sub city",city);
                       Log.e("stateName", subcity);*/
        }

        if(requestCode  == 2){
            Intent intent = new Intent(this, GPSTrackerActivity.class);
            startActivityForResult(intent,1);
        }


    }

    @TargetApi(23)
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permission, @NonNull int[] grantResults) {

        Log.e("requestcode", String.valueOf(requestCode));
        Log.e("size", String.valueOf(grantResults.length));

        if (requestCode == Constants.LOCATION_PERMISSION_REQUEST_CODE) {
            Log.e("requestcode", String.valueOf(grantResults[0] ));
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent io=new Intent(EODActivity.this,ClientVisitActivity.class);
                startActivity(io);
                finish();
            } else {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.LOCATION_PERMISSION_REQUEST_CODE);
            }


        }
    }
    private void getDetails(final String paymentcheck) {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "";

            @Override
            protected void onPreExecute() {
                dialog= ProgressDialog.show(EODActivity.this, "",
                        "Loading...", true, true);

            }

            @Override
            protected void onPostExecute(Void result) {

                try {

                    PaymentStatus=(JsonAccountObject.getString("pymtstatus"));
                    Log.e("PaymentStatus",PaymentStatus);


                    if(PaymentStatus.equals("Pending")||PaymentStatus.equals(null) ||PaymentStatus.equals("")||PaymentStatus.equals("null")){
                        showAlertDialogToast("Please make payment before place order");
                    }else{
                        Log.e("paymentcheck", paymentcheck);

                        if(paymentcheck.equals("profile")){
                            Intent io = new Intent(EODActivity.this,
                                    ProfileActivity.class);
                            startActivity(io);
                            finish();

                        }
                        else if(paymentcheck.equals("myorders")){
                            Intent io = new Intent(EODActivity.this,
                                    SalesManOrderActivity.class);
                            startActivity(io);
                            finish();

                        }else if(paymentcheck.equals("mydealer")){
                            Intent io = new Intent(EODActivity.this,
                                    MyDealersActivity.class);
                            io.putExtra("Key","menuclick");
                            startActivity(io);
                            finish();

                        }else if(paymentcheck.equals("postnotes")){
                            Intent io = new Intent(EODActivity.this,
                                    MerchantComplaintActivity.class);
                            startActivity(io);
                            finish();

                        } else if(paymentcheck.equals("placeorder")){
                            Intent to = new Intent(EODActivity.this,
                                    SalesManOrderCheckoutActivity.class);
                            startActivity(to);
                            finish();
                        }
                        else if(paymentcheck.equals("clientvisit")){
                            Intent to = new Intent(EODActivity.this,
                                    SMClientVisitHistory.class);
                            startActivity(to);
                            finish();
                        }
                        else if(paymentcheck.equals("paymentcoll")){
                            Intent to = new Intent(EODActivity.this,
                                    SalesmanPaymentCollectionActivity.class);
                            startActivity(to);
                            finish();
                        }
                        else if(paymentcheck.equals("Createorder")){
                            Intent to = new Intent(EODActivity.this,
                                    CreateOrderActivity.class);
                            startActivity(to);
                            finish();
                        } else if(paymentcheck.equals("addmerchant")){
                            Intent to = new Intent(EODActivity.this,
                                    AddMerchantNew.class);
                            startActivity(to);
                            finish();
                        } else if(paymentcheck.equals("acknowledge")){
                            Intent to = new Intent(EODActivity.this,
                                    SalesmanAcknowledgeActivity.class);
                            startActivity(to);
                            finish();
                        } else if(paymentcheck.equals("stockentry")){
                            Intent to = new Intent(EODActivity.this,
                                    OutletStockEntry.class);
                            startActivity(to);
                            finish();
                        }
                        else if(paymentcheck.equals("surveyuser")){
                            Intent to = new Intent(EODActivity.this,
                                    SurveyMerchant.class);
                            startActivity(to);
                            finish();
                        }






                    }

                    dialog.dismiss();


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();


                JsonServiceHandler = new JsonServiceHandler(Utils.strGetProfileDetail+Constants.USER_ID, EODActivity.this);
                JsonAccountObject = JsonServiceHandler.ServiceDataGet();
                return null;
            }
        }.execute(new Void[]{});
    }

    private void getEod(final String DuserID, final String SuserID, final String CurrentDate) {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "";

            @Override
            protected void onPreExecute() {
                dialog= ProgressDialog.show(EODActivity.this, "",
                        "Loading...", true, true);


            }

            @Override
            protected void onPostExecute(Void result) {


                try {
                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);
                    arraylist=new ArrayList<AddEodDomain>();
                    if (strStatus.equals("true")) {

                        JSONArray job = new JSONArray();
                        try {
                            job = JsonAccountObject.getJSONArray("data");

                            Log.e("EOD","..........job::"+job.toString());

                            for (int i = 0; i < job.length(); i++) {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject = job.getJSONObject(i);

                                AddEodDomain dod=new AddEodDomain();
                                dod.setProdcode(jsonObject.getString("prodcode"));
                                dod.setProdname(jsonObject.getString("prodname"));
                                dod.setQtotal(jsonObject.getString("qtotal"));
                                dod.setUom(jsonObject.getString("uom"));
                                //Kumaravel
                                if(jsonObject.has("value")){
                                    if(jsonObject.getString("value") != null){
                                        dod.setSalesValue(jsonObject.getString("value"));
                                        double value = Double.parseDouble(jsonObject.getString("value"));
                                        totalSalesValue = totalSalesValue + value;
                                    }
                                }
                                arraylist.add(dod);

                            }

                            if(JsonAccountObject.getString("totalcount")==null){
                                totalCalls = "0";

                            }else {
                                totalCalls = JsonAccountObject.getString("totalcount");

                            }

                            if(JsonAccountObject.getString("pccount")==null){
                                pC = "0";

                            }else {
                                pC = JsonAccountObject.getString("pccount");
                            }

                            setAdapter();
                            dialog.dismiss();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }else {

                        totalCalls="0";
                        pC = "0";
                        strSalesValue = "0";
                        recyclerView.setVisibility(View.GONE);
                        textviewnodata.setVisibility(View.VISIBLE);
                        dialog.dismiss();

                    }


                    db = EODActivity.this.openOrCreateDatabase("freshorders", 0, null);
                    Cursor  cursr = db.rawQuery("SELECT * FROM merchanttable where date = "+"'"+currentDate+"'", null);
                    Log.e("Query","SELECT * FROM merchanttable where date = "+"'"+currentDate+"'");
                    if(cursr!=null && cursr.getCount() > 0) {
                        //newOutlet = cursr.getCount()+"";
                        newOutlet = String.valueOf(cursr.getCount());
                        Log.e("newOutlet", newOutlet);
                        cursr.close();
                    }
                    else
                    {
                        newOutlet ="0";
                    }

                    ////// Kumaravel new Changes (nambi) for wrong count
                    if(JsonAccountObject.has("outlet")){
                        String count = JsonAccountObject.getString("outlet");
                        if(count!= null) {
                            newOutlet = count;
                        }
                    }//////////////////


                    setValues();

                } catch (JSONException e) {
                    e.printStackTrace();
                }




            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();

                try {

                    jsonObject.put("fromdt",CurrentDate);
                    jsonObject.put("todt",CurrentDate);
                    jsonObject.put("duserid",DuserID);
                    jsonObject.put("suserid", SuserID);


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                JsonServiceHandler = new JsonServiceHandler(Utils.strorderdetails,jsonObject.toString(), EODActivity.this);
                JsonAccountObject = JsonServiceHandler.ServiceData();
                return null;
            }
        }.execute(new Void[]{});
    }

    public void setAdapter()
    {
        recyclerView.setVisibility(View.VISIBLE);
        textviewnodata.setVisibility(View.GONE);
        AddEodAdapter adapter=new AddEodAdapter(arraylist,EODActivity.this);
        recyclerView.setAdapter(adapter);
    }
    public void showAlertDialogToast( String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }


    public void showSettingsAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(EODActivity.this);

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.delete);

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, 2);

            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public void onBackPressed() {
        exitAlret();
    }

    private void exitAlret() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        localBuilder.setMessage("Do you want to Exit?");
        localBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {

                        finish();

                    }
                });
        localBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {
                        paramDialogInterface.cancel();
                    }
                });
        localBuilder.create().show();
    }
    public boolean netCheck() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            }

            else if (result == null) {

                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }


    public void getEOD(){
        try{

        Cursor cursr,curs2;
        db = EODActivity.this.openOrCreateDatabase("freshorders", 0, null);
        cursr = db.rawQuery("SELECT * from merchanttable", null);
      //  totalCalls = String.valueOf(cursr.getCount());
     //   Log.e("totalCalls", totalCalls);
        cursr = db.rawQuery("SELECT  * FROM orderheader where (pushstatus = 'Success') AND (date(orderdt) = "+"'"+currentDate+"')"+"ORDER BY oflnordid DESC", null);
      //  pC = String.valueOf(cursr.getCount());
      //  Log.e("PC", pC);
        cursr = db.rawQuery("SELECT * FROM oredrdetail where productcode =  AND isfree = 'NF' AND (date(deliverydt) = "+"'"+currentDate+"')", null);
        if(cursr!=null && cursr.getCount() > 0) {
            cursr = db.rawQuery("SELECT SUM(qty) FROM oredrdetail where productcode = 'CP100' AND isfree = 'NF' AND (date(deliverydt) = "+"'"+currentDate+"')", null);
            if (cursr.moveToFirst()) {
            perfect100gm = cursr.getString(0);}
            Log.e("perfect100gm", perfect100gm);
        }

            curs2 = db.rawQuery("SELECT * FROM oredrdetail where productcode = 'CP200' AND isfree = 'NF' AND (date(deliverydt) = "+"'"+currentDate+"') ", null);
            Log.e("curs2", String.valueOf(curs2.getCount()));
            if(curs2!=null && curs2.getCount() > 0) {
                curs2 = db.rawQuery("SELECT SUM(qty) FROM oredrdetail where productcode = 'CP200' AND isfree = 'NF' AND (date(deliverydt) = "+"'"+currentDate+"')", null);
                if (curs2.moveToFirst()) {
            perfect200gm = curs2.getString(0);
            Log.e("perfect200gm", perfect200gm);
            }
            }

        cursr = db.rawQuery("SELECT  * FROM oredrdetail where productcode = 'CP500' AND isfree = 'NF' AND (date(deliverydt) = "+"'"+currentDate+"') ", null);
        if(cursr!=null && cursr.getCount() > 0) {
            cursr = db.rawQuery("SELECT  SUM(qty) FROM oredrdetail where productcode = 'CP500' AND isfree = 'NF' AND (date(deliverydt) = "+"'"+currentDate+"') ", null);
            if (cursr.moveToLast()) {
            perfect500gm = cursr.getString(0);}
            Log.e("perfect300gm", perfect500gm);
            }

        int ptotal=0;
        ptotal= Integer.parseInt(perfect100gm)+ Integer.parseInt(perfect200gm)+ Integer.parseInt(perfect500gm);
        perfectTotal = String.valueOf(ptotal);
        Log.e("perfectTotal", perfectTotal);

        cursr = db.rawQuery("SELECT * FROM oredrdetail where productcode = 'CM100' AND isfree = 'NF' AND (date(deliverydt) = "+"'"+currentDate+"')", null);
        if(cursr!=null && cursr.getCount() > 0) {if (cursr.moveToLast()) {
            cursr = db.rawQuery("SELECT *SUM(qty) FROM oredrdetail where productcode = 'CM100' AND isfree = 'NF' AND (date(deliverydt) = "+"'"+currentDate+"')", null);
            mild100gm = cursr.getString(0);}
           Log.e("mild100gm", mild100gm);
            }

        cursr = db.rawQuery("SELECT  * FROM oredrdetail where productcode = 'CM200' AND isfree = 'NF' AND (date(deliverydt) ="+"'"+currentDate+"')" , null);
        if(cursr!=null && cursr.getCount() > 0) {
            cursr = db.rawQuery("SELECT SUM(qty) FROM oredrdetail where productcode = 'CM200' AND isfree = 'NF' AND (date(deliverydt) = "+"'"+currentDate+"')", null);
            if (cursr.moveToLast()) {
            mild200gm = cursr.getString(0);}
            Log.e("mild200gm", mild200gm);
        }

        cursr = db.rawQuery("SELECT  * FROM oredrdetail where productcode = 'UR500' AND isfree = 'NF' AND (date(deliverydt) = "+"'"+currentDate+"')", null);
        if(cursr!=null && cursr.getCount() > 0) {
            cursr = db.rawQuery("SELECT SUM(qty) FROM oredrdetail where productcode = 'UR500' AND isfree = 'NF' AND (date(deliverydt) = "+"'"+currentDate+"')", null);
            if (cursr.moveToLast()) {
            uRich500gm = cursr.getString(0);}
            Log.e("uRich500gm", uRich500gm);
        }

        cursr = db.rawQuery("SELECT * FROM oredrdetail where productcode = 'URN100' AND isfree = 'NF' AND (date(deliverydt) = "+"'"+currentDate+"')", null);
        if(cursr!=null && cursr.getCount() > 0) {
            cursr = db.rawQuery("SELECT  SUM(qty) FROM oredrdetail where productcode = 'URN100' AND isfree = 'NF' AND (date(deliverydt) = "+"'"+currentDate+"')", null);
            if (cursr.moveToLast()) {
            ultraRichNewPc = cursr.getString(0);}
            Log.e("ultraRichNewPc", ultraRichNewPc);
        }

        cursr = db.rawQuery("SELECT  * FROM oredrdetail where productcode = 'CT100' AND isfree = 'NF' AND (date(deliverydt) = "+"'"+currentDate+"')", null);
        if(cursr!=null && cursr.getCount() > 0) {
            cursr = db.rawQuery("SELECT  SUM(qty) FROM oredrdetail where productcode = 'CT100' AND isfree = 'NF' AND (date(deliverydt) = "+"'"+currentDate+"')", null);
            if (cursr.moveToLast()) {
            trisakthi100gm = cursr.getString(0);}
            Log.e("trisakthi100gm", trisakthi100gm);
        }

        cursr = db.rawQuery("SELECT * FROM oredrdetail where productcode = 'CT200' AND isfree = 'NF' AND (date(deliverydt) = "+"'"+currentDate+"') ", null);
        if(cursr!=null && cursr.getCount() > 0) {
            cursr = db.rawQuery("SELECT  SUM(qty) FROM oredrdetail where productcode = 'CT200' AND isfree = 'NF' AND (date(deliverydt) = "+"'"+currentDate+"')", null);
            if (cursr.moveToLast()) {
            trisakthi200gm = cursr.getString(0);}
            Log.e("trisakthi200gm", trisakthi200gm);
        }


        int ttotal=0;
        ttotal= Integer.parseInt(trisakthi100gm)+ Integer.parseInt(trisakthi200gm);
        totalTrisakthi = String.valueOf(ttotal);
        Log.e("totalTrisakthi", totalTrisakthi);


        cursr = db.rawQuery("SELECT  * FROM oredrdetail where productcode = 'TS100' AND isfree = 'NF' AND (date(deliverydt) = "+"'"+currentDate+"')", null);
        if(cursr!=null && cursr.getCount() > 0) {
            cursr = db.rawQuery("SELECT  SUM(qty) FROM oredrdetail where productcode = 'TS100' AND isfree = 'NF' AND (date(deliverydt) = "+"'"+currentDate+"')", null);
            if (cursr.moveToLast()) {
            sTea100gm = cursr.getString(0);}
            Log.e("sTea100gm", sTea100gm);
        }

        cursr = db.rawQuery("SELECT * FROM oredrdetail where productcode = 'TA100' AND isfree = 'NF' AND (date(deliverydt) = "+"'"+currentDate+"')", null);
        if(cursr!=null && cursr.getCount() > 0) {
            cursr = db.rawQuery("SELECT  SUM(qty) FROM oredrdetail where productcode = 'TA100' AND isfree = 'NF' AND (date(deliverydt) = "+"'"+currentDate+"')", null);
            if (cursr.moveToLast()) {
            dTea100gm = cursr.getString(0);}
            Log.e("dTea100gm", dTea100gm);
        }

        cursr = db.rawQuery("SELECT  * FROM oredrdetail where productcode = 'TA250' AND isfree = 'NF' AND (date(deliverydt) = "+"'"+currentDate+"')", null);
        if(cursr!=null && cursr.getCount() > 0) {
            cursr = db.rawQuery("SELECT  SUM(qty) FROM oredrdetail where productcode = 'TA250' AND isfree = 'NF' AND (date(deliverydt) = "+"'"+currentDate+"')", null);
            if (cursr.moveToLast()) {
            dtea250gm = cursr.getString(0);}
            Log.e("dtea250gm", dtea250gm);
        }

        int totalteaSale=0;
        totalteaSale= Integer.parseInt(sTea100gm)+ Integer.parseInt(dTea100gm)+ Integer.parseInt(dtea250gm);
        totalteaSales = String.valueOf(totalteaSale);
        Log.e("totalteaSales", totalteaSales);

        int grandtotalsale=0;
        grandtotalsale= ptotal+ ttotal+ totalteaSale;
        grandTeaSales = String.valueOf(grandtotalsale);
        Log.e("grandTeaSales", grandTeaSales);

        cursr = db.rawQuery("SELECT * FROM merchanttable where date = "+"'"+currentDate+"'", null);
        if(cursr!=null && cursr.getCount() > 0) {
            //cursr = db.rawQuery("SELECT * FROM merchanttable where date = "+"'"+currentDate+"'", null);
           // if (cursr.moveToLast()) {
            newOutlet = cursr.getCount()+"";//}
           Log.e("newOutlet", newOutlet);
        }

        }catch (NullPointerException e){

        }
       // setValues();
    }

    private String getFormattedAmount(String amounts){
        if(amounts != null && !amounts.isEmpty()) {
            DecimalFormat IndianCurrencyFormat = new DecimalFormat("##,##,###.00");
            return IndianCurrencyFormat.format(amounts);
        }
        return "";
    }

    private String getFormattedAmounts(String amounts){

        if(amounts != null && !amounts.isEmpty()) {
            Format format = NumberFormat.getCurrencyInstance(new Locale("en", "in"));
            return (format.format(new BigDecimal(amounts)));
        }
        return "";
    }

    public void setValues(){

        textviewtotalcalls.setText("Total Calls :  "+totalCalls);
        textViewPC.setText("PC :  "+pC);
        textViewNewOutlet.setText("New Outlets : "+newOutlet);
        String strValue = tvSalesValue.getText().toString();
        tvSalesValue.setText((strValue + getFormattedAmounts(String.valueOf(totalSalesValue))));
        /*textViewcp100.setText("Perfect 100gm :  "+perfect100gm);
        textViewcp200.setText("Perfect 200gm :  "+perfect200gm);
        textViewcp300.setText("Perfect 300gm :  "+perfect500gm);
        textViewperfecttotal.setText("Perfect Total :  "+perfectTotal);
        textViewmild100.setText("Mild 100gm :  "+mild100gm);
        textViewmild200.setText("Mild 200gm :  "+mild200gm);
        textViewurich500.setText("URich 500gm :  "+uRich500gm);
        textViewurn100.setText("Ultra Rich New PC :  "+ultraRichNewPc);
        textViewCT100.setText("TriSakthi 100gm :  "+trisakthi100gm);
        textViewCT200.setText("TriSakthi 200gm :  "+trisakthi200gm);
        textViewtotaltrisakthi.setText("Total TriSakthi : "+totalTrisakthi);
        textViewts100.setText("STea 100gm : "+sTea100gm);
        textViewTA100.setText("DTea 100gm : "+dTea100gm);
        textViewTA250.setText("DTea 250gm : "+dtea250gm);
        textViewTotalorder.setText("Total Tea Sales : "+totalteaSales);
        textViewTotalSales.setText("Grand Tea Sales : "+grandTeaSales);*/

    }
}
