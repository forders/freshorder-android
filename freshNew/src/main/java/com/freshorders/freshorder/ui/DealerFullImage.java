package com.freshorders.freshorder.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.DealerImageAdapter;


import java.io.InputStream;

/**
 * Created by ragul on 24/08/16.
 */
public class DealerFullImage extends Activity {

    ProgressDialog pDialog;
    ImageView img;
    Bitmap bitmap;
    LinearLayout linearLayoutBack;
    TextView textViewAssetMenu,textviewdeletemenu;
    int position;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.fullimageview);

        linearLayoutBack =(LinearLayout) findViewById(R.id.linearLayoutBack);
        textViewAssetMenu =(TextView) findViewById(R.id.textViewAssetMenu);
        textviewdeletemenu =(TextView) findViewById(R.id.textviewdeletemenu);
        Typeface font = Typeface.createFromAsset(getAssets(),
                "fontawesome-webfont.ttf");
        textViewAssetMenu.setTypeface(font);
        textviewdeletemenu.setTypeface(font);
        Intent i = getIntent();
        position = i.getExtras().getInt("id");

        DealerImageAdapter imageAdapter = new DealerImageAdapter(this);

        img = (ImageView) findViewById(R.id.image);
        String url = (String)imageAdapter.getItem(position);

        new DownloadImage().execute(url);
        textviewdeletemenu.setVisibility(View.VISIBLE);
        linearLayoutBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent to = new Intent(DealerFullImage.this, DealerOrderDetailsActivity.class);
                startActivity(to);
                DealerFullImage.this.finish();

            }
        });
        textviewdeletemenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int imagesize=DealersOrderActivity.arraylistPdoductImages.size();
                if(imagesize==1){
                    showAlertDialogToast("Have Only One Image, Can't delete!!!");

                }else {
                    DealersOrderActivity.arraylistPdoductImages.remove(position);
                    Log.e("removedImage", DealerOrderDetailsActivity.list.get(position));
                    String[] removed = DealerOrderDetailsActivity.list.get(position).split("/");
                    DealersOrderActivity.removedImage.add(removed[removed.length - 1]);
                    DealerOrderDetailsActivity.list.remove(position);
                    Intent to = new Intent(DealerFullImage.this, DealerOrderDetailsActivity.class);
                    startActivity(to);
                    DealerFullImage.this.finish();
                }
            }
        });

    }

    private class DownloadImage extends AsyncTask<String, Void, Bitmap> {
        ProgressDialog dialog;
        @Override
        protected Bitmap doInBackground(String... URL) {
            String imageURL = URL[0];
            Bitmap bitmap = null;
            try {
                InputStream input = new java.net.URL(imageURL).openStream();
                bitmap = BitmapFactory.decodeStream(input);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {

            img.setImageBitmap(result);
            dialog.dismiss();
        }

        @Override
        protected void onPreExecute() {
            dialog = ProgressDialog.show(DealerFullImage.this, "",
                    "Loading...", true, true);

        }
    }
    public void showAlertDialogToast( String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent io=new Intent(DealerFullImage.this,DealerOrderDetailsActivity.class);
                startActivity(io);
                finish();
            }
        });
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }
    public void onBackPressed() {
        exitAlret();
    }

    private void exitAlret() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        localBuilder.setMessage("Do you want to Exit?");
        localBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {

                        finish();

                    }
                });
        localBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {
                        paramDialogInterface.cancel();
                    }
                });
        localBuilder.create().show();
    }
}