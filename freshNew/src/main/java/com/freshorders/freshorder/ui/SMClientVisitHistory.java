package com.freshorders.freshorder.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.activity.DistanceCalActivity;
import com.freshorders.freshorder.adapter.DealerOrderListAdapter;
import com.freshorders.freshorder.adapter.DealerVisitAdapter;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.DealerOrderListDomain;
import com.freshorders.freshorder.domain.DealerVisitDomain;
import com.freshorders.freshorder.domain.MDealerCompDropdownDomain;
import com.freshorders.freshorder.toonline.migration.PendingDataMigrationActivity;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class SMClientVisitHistory extends Activity {

    TextView menuIcon, textViewAssetMenuProfile, textViewAssetMenuMyOrders,
            textViewAssetMenuMyDealers, textViewAssetMenuProducts,
            textViewAssetMenuPayment, textViewAssetMenuComplaint,textViewAssetMenuDistanceCalculation,
            textViewAssetMenuSignout, textViewAssetMenuPaymentCollection, textViewAssetClientVisit,
            textViewNodata, textViewHeader, textViewAssetMenuCreateOrder, textViewCross,
            textViewAssetMenuupdate, textviewupdate, textViewAssetMenuRefresh, textViewAssetMenuStockEntry, textViewAssetMenuAddMerchant, textViewAssetMenuAcknowledge, textViewAssetMenusurveyuser, textViewAssetMenudownloadscheme, textViewAssetMenuMyDayPlan, textViewAssetMenuMySales,
            textViewAssetMenuDistStock, textViewAssetMenuStockOnly, textViewAssetMenuMigration, textViewAssetMenuStockAudit;

    ListView listViewOrders;
    public static int menuCliccked, selectedPosition;

    LinearLayout linearLayoutMenuParent, linearLayoutProfile,linearLayoutDistanceCalculation,
            linearLayoutMyOrders, linearLayoutMyDealers, linearLayoutProducts,
            linearLayoutPayment, linearLayoutComplaint, linearLayoutClientVisit, linearLayoutSignout, linearLayoutCreateOrder, linearLayoutPaymentCollection, linearLayoutSuccess, linearLayoutFailure,
            linearLayoutPending, linearLayoutupdate, linearLayoutRefresh, linearLayoutAcknowledge,
            linearLayoutStockEntry, linearLayoutAddMerchant, linearLayoutsurveyuser, linearLayoutdownloadscheme, linearLayoutMyDayPlan, linearLayoutMySales,
            linearLayoutDistStock, linearLayoutStockOnly, linearLayoutMigration, linearLayoutStockAudit;

    private void showMenu(){

        MyApplication app = MyApplication.getInstance();

        if(!app.isCreateOrder()){
            linearLayoutCreateOrder.setVisibility(View.GONE);
        }
        if(!app.isProfile()){
            linearLayoutProfile.setVisibility(View.GONE);
        }
        if(!app.isMyDayPlan()){
            linearLayoutMyDayPlan.setVisibility(View.GONE);
        }
        if(!app.isMyOrders()){
            linearLayoutMyOrders.setVisibility(View.GONE);
        }
        if(!app.isAddMerchant()){
            linearLayoutAddMerchant.setVisibility(View.GONE);
        }
        if(!app.isMySales()){
            linearLayoutMySales.setVisibility(View.GONE);
        }
        if(!app.isPostNotes()){
            linearLayoutComplaint.setVisibility(View.GONE);
        }
        if(!app.isMyClientVisit()){
            linearLayoutClientVisit.setVisibility(View.GONE);
        }
        if(!app.isAcknowledge()){
            linearLayoutAcknowledge.setVisibility(View.GONE);
        }
        if(!app.isPaymentCollection()){
            linearLayoutPaymentCollection.setVisibility(View.GONE);
        }
        if(!app.isPkdDataCapture()){
            linearLayoutStockEntry.setVisibility(View.GONE);
        }
        if(!app.isDistributorStock()){
            linearLayoutDistStock.setVisibility(View.GONE);
        }
        if(!app.isSurveyUser()){
            linearLayoutsurveyuser.setVisibility(View.GONE);
        }
        if(!app.isDownLoadScheme()){
            linearLayoutdownloadscheme.setVisibility(View.GONE);
        }
        if(!app.isDistanceCalculation()){
            linearLayoutDistanceCalculation.setVisibility(View.GONE);
        }
        if(!app.isRefresh()){
            linearLayoutRefresh.setVisibility(View.GONE);
        }
        if(!app.isLogout()){
            linearLayoutSignout.setVisibility(View.GONE);
        }
        if(!app.isClosingStock()){
            linearLayoutStockOnly.setVisibility(View.GONE);
        }
        if(!app.isClosingStockAudit()){
            linearLayoutStockAudit.setVisibility(View.GONE);
        }
        if(!app.isPendingData()){
            linearLayoutMigration.setVisibility(View.GONE);
        }
    }

    JSONObject JsonAccountObject = null;
    JSONArray JsonAccountArray = null;
    DatabaseHandler databaseHandler;
    JsonServiceHandler JsonServiceHandler;
    public static ArrayList<DealerVisitDomain> arraylistDealerVisitDomain;
    public static String companyname, fullname, date, address, orderId, morderId, PaymentStatus = "NULL", onlineorderno, Paymentdate = "NULL", salesuserid = "NULL";
    Date dateStr = null;
    public static String time, searchclick = "0", strMsg = "";
    ;
    EditText editTextSearchField;
    public static DealerVisitDomain domainSelected;
    public static DealerVisitAdapter adapter;
    Button buttonPlaceOrder, buttonBlue, buttonYellow, buttonGreen, ButtonPending, ButtonFailure, ButtonSuccess;
    public static String Count, mname, sname, lat, lng, desc;
    public static String prodid, prodname, prodcode, userid, pdate, pcompanyname, Porderdate, qty, fqty, Dorderdate, prodprice, prodtax, moveto = "NULL";
    public static int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.sm_client_visit_history);
        netCheck();
        databaseHandler = new DatabaseHandler(getApplicationContext());
        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();


        Cursor cur;
        cur = databaseHandler.getDetails();
        cur.moveToFirst();
        Log.e("Insertion Check",
                cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_username)));
        Log.e("Insertion Check",
                cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_password)));
        Log.e("Insertion Check",
                cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_usertype)));
        PaymentStatus = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_paymentStatus));

        linearLayoutCreateOrder = (LinearLayout) findViewById(R.id.linearLayoutCreateOrder);
        listViewOrders = (ListView) findViewById(R.id.listViewOrders);
        linearLayoutProfile = (LinearLayout) findViewById(R.id.linearLayoutProfile);
        linearLayoutMyOrders = (LinearLayout) findViewById(R.id.linearLayoutMyOrders);
        linearLayoutMyDealers = (LinearLayout) findViewById(R.id.linearLayoutMyDealers);
        linearLayoutProducts = (LinearLayout) findViewById(R.id.linearLayoutProducts);
        linearLayoutPayment = (LinearLayout) findViewById(R.id.linearLayoutPayment);
        linearLayoutComplaint = (LinearLayout) findViewById(R.id.linearLayoutComplaint);
        linearLayoutSignout = (LinearLayout) findViewById(R.id.linearLayoutSignout);
        linearLayoutSuccess = (LinearLayout) findViewById(R.id.linearLayoutSuccess);
        linearLayoutFailure = (LinearLayout) findViewById(R.id.linearLayoutFailure);
        linearLayoutPending = (LinearLayout) findViewById(R.id.linearLayoutPending);
        linearLayoutDistanceCalculation= (LinearLayout) findViewById(R.id.linearLayoutDistanceCalculation);
        linearLayoutClientVisit = (LinearLayout) findViewById(R.id.linearLayoutClientVisit);
        linearLayoutPaymentCollection = (LinearLayout) findViewById(R.id.linearLayoutPaymentCollection);
        linearLayoutAcknowledge = (LinearLayout) findViewById(R.id.linearLayoutAcknowledge);
        linearLayoutStockEntry = (LinearLayout) findViewById(R.id.linearLayoutStockEntry);
        linearLayoutAddMerchant = (LinearLayout) findViewById(R.id.linearLayoutAddMerchant);
        linearLayoutdownloadscheme = (LinearLayout) findViewById(R.id.linearLayoutdownloadscheme);
        linearLayoutsurveyuser = (LinearLayout) findViewById(R.id.linearLayoutsurveyuser);
        linearLayoutMyDayPlan = (LinearLayout) findViewById(R.id.linearLayoutMyDayPlan);
        linearLayoutMySales = (LinearLayout) findViewById(R.id.linearLayoutMySales);
        buttonPlaceOrder = (Button) findViewById(R.id.buttonPlaceOrder);
        buttonBlue = (Button) findViewById(R.id.buttonBlue);
        buttonYellow = (Button) findViewById(R.id.buttonYellow);
        buttonGreen = (Button) findViewById(R.id.buttonGreen);
        ButtonPending = (Button) findViewById(R.id.ButtonPending);
        ButtonFailure = (Button) findViewById(R.id.ButtonFailure);
        ButtonSuccess = (Button) findViewById(R.id.ButtonSuccess);
        linearLayoutRefresh = (LinearLayout) findViewById(R.id.linearLayoutRefresh);
        textViewAssetMenuRefresh = (TextView) findViewById(R.id.textViewAssetMenuRefresh);

        linearLayoutStockOnly = (LinearLayout) findViewById(R.id.linearLayoutStockOnly);
        linearLayoutStockAudit = findViewById(R.id.linearLayoutStockAudit);
        linearLayoutMigration = (LinearLayout) findViewById(R.id.linearLayoutMigration);

        if (Constants.USER_TYPE.equals("D")) {
            linearLayoutMyDealers.setVisibility(View.GONE);
            linearLayoutProducts.setVisibility(View.VISIBLE);
            linearLayoutCreateOrder.setVisibility(View.GONE);
            linearLayoutRefresh.setVisibility(View.GONE);
            linearLayoutPaymentCollection.setVisibility(View.GONE);
            linearLayoutDistanceCalculation.setVisibility(View.GONE);
        } else if (Constants.USER_TYPE.equals("M")) {
            linearLayoutMyDealers.setVisibility(View.VISIBLE);
            linearLayoutProducts.setVisibility(View.GONE);
            linearLayoutCreateOrder.setVisibility(View.GONE);
            linearLayoutPaymentCollection.setVisibility(View.GONE);
            linearLayoutRefresh.setVisibility(View.GONE);
            linearLayoutDistanceCalculation.setVisibility(View.GONE);
        } else {
            linearLayoutMyDealers.setVisibility(View.VISIBLE);
            linearLayoutProducts.setVisibility(View.GONE);
            linearLayoutCreateOrder.setVisibility(View.VISIBLE);
            linearLayoutRefresh.setVisibility(View.VISIBLE);
            linearLayoutDistanceCalculation.setVisibility(View.VISIBLE);
        }

        menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
        textViewAssetMenuCreateOrder = (TextView) findViewById(R.id.textViewAssetMenuCreateOrder);
        textViewHeader = (TextView) findViewById(R.id.textViewHeader);
        textViewNodata = (TextView) findViewById(R.id.textViewNodata);
        textViewAssetMenuProfile = (TextView) findViewById(R.id.textViewAssetMenuProfile);
        textViewAssetMenuMyOrders = (TextView) findViewById(R.id.textViewAssetMenuMyOrders);
        textViewAssetMenuMyDealers = (TextView) findViewById(R.id.textViewAssetMenuMyDealers);
        textViewAssetMenuProducts = (TextView) findViewById(R.id.textViewAssetMenuProducts);
        textViewAssetMenuPayment = (TextView) findViewById(R.id.textViewAssetMenuPayment);
        textViewAssetMenuComplaint = (TextView) findViewById(R.id.textViewAssetMenuComplaint);
        textViewAssetMenuSignout = (TextView) findViewById(R.id.textViewAssetMenuSignout);
        textViewAssetClientVisit = (TextView) findViewById(R.id.textViewAssetClientVisit);
        textViewAssetMenuDistanceCalculation= (TextView) findViewById(R.id.textViewAssetMenuDistanceCalculation);
        editTextSearchField = (EditText) findViewById(R.id.editTextSearchField);
        textViewCross = (TextView) findViewById(R.id.textViewCross);

        textViewAssetMenuPaymentCollection = (TextView) findViewById(R.id.textViewAssetMenuPaymentCollection);
        textViewAssetMenuAddMerchant = (TextView) findViewById(R.id.textViewAssetMenuAddMerchant);
        textViewAssetMenuAcknowledge = (TextView) findViewById(R.id.textViewAssetMenuAcknowledge);
        textViewAssetMenuStockEntry = (TextView) findViewById(R.id.textViewAssetMenuStockEntry);

        textViewAssetMenudownloadscheme = (TextView) findViewById(R.id.textViewAssetMenudownloadscheme);
        textViewAssetMenusurveyuser = (TextView) findViewById(R.id.textViewAssetMenusurveyuser);
        textViewAssetMenuMyDayPlan = (TextView) findViewById(R.id.textViewAssetMenuMyDayPlan);
        textViewAssetMenuMySales = (TextView) findViewById(R.id.textViewAssetMenuMySales);


        linearLayoutMenuParent = (LinearLayout) findViewById(R.id.linearLayoutMenuParent);
        Typeface font = Typeface.createFromAsset(getAssets(),
                "fontawesome-webfont.ttf");
        menuIcon.setTypeface(font);

        textViewAssetMenuCreateOrder.setTypeface(font);
        textViewAssetMenuProfile.setTypeface(font);
        textViewAssetMenuMyOrders.setTypeface(font);
        textViewAssetMenuMyDealers.setTypeface(font);
        textViewAssetMenuProducts.setTypeface(font);
        textViewAssetMenuPayment.setTypeface(font);
        textViewAssetMenuComplaint.setTypeface(font);
        textViewAssetMenuSignout.setTypeface(font);
        textViewAssetClientVisit.setTypeface(font);
        textViewAssetMenuPaymentCollection.setTypeface(font);
        textViewCross.setTypeface(font);
        textViewAssetMenuRefresh.setTypeface(font);
        textViewAssetMenuDistanceCalculation.setTypeface(font);
        textViewAssetMenuAcknowledge.setTypeface(font);
        textViewAssetMenuStockEntry.setTypeface(font);
        textViewAssetMenuAddMerchant.setTypeface(font);

        textViewAssetMenusurveyuser.setTypeface(font);
        textViewAssetMenudownloadscheme.setTypeface(font);
        textViewAssetMenuMyDayPlan.setTypeface(font);
        textViewAssetMenuMySales.setTypeface(font);

        linearLayoutDistStock = (LinearLayout) findViewById(R.id.linearLayoutDistStock);
        textViewAssetMenuDistStock = (TextView) findViewById(R.id.textViewAssetMenuDistStock);
        textViewAssetMenuDistStock.setTypeface(font);

        textViewAssetMenuStockOnly = (TextView) findViewById(R.id.textViewAssetMenuStockOnly);
        textViewAssetMenuStockAudit = (TextView) findViewById(R.id.textViewAssetMenuStockAudit);
        textViewAssetMenuMigration = findViewById(R.id.textViewAssetMenuMigration);

        textViewAssetMenuStockOnly.setTypeface(font);
        textViewAssetMenuStockAudit.setTypeface(font);
        textViewAssetMenuMigration.setTypeface(font);

        linearLayoutMyDealers.setVisibility(View.GONE);
        initialcount();

        MyOrdersList(Constants.ClientStatus);

        if (Constants.ClientStatus.equals("Success")) {
            buttonBlue.setBackgroundColor(Color.parseColor("#0768AA"));
            buttonYellow.setBackgroundColor(Color.parseColor("#ecce03"));
            buttonGreen.setBackgroundColor(Color.parseColor("#b1d36f"));
        } else if (Constants.ClientStatus.equals("Pending")) {
            buttonBlue.setBackgroundColor(Color.parseColor("#03acec"));
            buttonYellow.setBackgroundColor(Color.parseColor("#ecce03"));
            buttonGreen.setBackgroundColor(Color.parseColor("#84A544"));
        }

        ButtonSuccess.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Log.e("linearLayoutSuccess", "linearLayoutSuccess");
                buttonBlue.setBackgroundColor(Color.parseColor("#0768AA"));
                buttonYellow.setBackgroundColor(Color.parseColor("#ecce03"));
                buttonGreen.setBackgroundColor(Color.parseColor("#b1d36f"));
                MyOrdersList("Success");


            }
        });
        ButtonFailure.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Log.e("linearLayoutFailure", "linearLayoutFailure");
                buttonBlue.setBackgroundColor(Color.parseColor("#03acec"));
                buttonYellow.setBackgroundColor(Color.parseColor("#D3DD00"));
                buttonGreen.setBackgroundColor(Color.parseColor("#b1d36f"));
                MyOrdersList("Failed");

            }
        });
        ButtonPending.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Log.e("linearLayoutFailure", "linearLayoutFailure");
                buttonBlue.setBackgroundColor(Color.parseColor("#03acec"));
                buttonYellow.setBackgroundColor(Color.parseColor("#ecce03"));
                buttonGreen.setBackgroundColor(Color.parseColor("#84A544"));
                MyOrdersList("Pending");

            }
        });

        buttonPlaceOrder.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent to = new Intent(SMClientVisitHistory.this,
                        ClientVisitActivity.class);
                startActivity(to);
                finish();


            }
        });


        menuIcon.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (menuCliccked == 0) {
                    linearLayoutMenuParent.setVisibility(View.VISIBLE);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(menuIcon.getWindowToken(), 0);
                    editTextSearchField.setVisibility(EditText.GONE);
                    textViewHeader.setVisibility(TextView.VISIBLE);
                    textViewCross.setVisibility(TextView.GONE);
                    menuCliccked = 1;
                } else {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    textViewHeader.setVisibility(TextView.VISIBLE);
                    editTextSearchField.setVisibility(EditText.GONE);
                    textViewCross.setVisibility(TextView.GONE);
                    menuCliccked = 0;
                }

            }
        });


        listViewOrders.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                domainSelected = arraylistDealerVisitDomain.get(position);
                date = arraylistDealerVisitDomain.get(position).getDate();
                sname = arraylistDealerVisitDomain.get(position).getSname();
                mname = arraylistDealerVisitDomain.get(position).getMname();
                lat = arraylistDealerVisitDomain.get(position).getGeoLat();
                lng = arraylistDealerVisitDomain.get(position).getGeoLng();
                desc = arraylistDealerVisitDomain.get(position).getNotes();

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(listViewOrders.getWindowToken(), 0);
                Intent to = new Intent(SMClientVisitHistory.this, DealerClientVisitDetailActivity.class);
                startActivity(to);
                finish();


            }
        });
        menuIcon.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (menuCliccked == 0) {
                    linearLayoutMenuParent.setVisibility(View.VISIBLE);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(menuIcon.getWindowToken(), 0);
                    editTextSearchField.setVisibility(EditText.GONE);
                    textViewHeader.setVisibility(TextView.VISIBLE);
                    textViewCross.setVisibility(TextView.GONE);
                    menuCliccked = 1;
                } else {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    textViewHeader.setVisibility(TextView.VISIBLE);
                    editTextSearchField.setVisibility(EditText.GONE);
                    textViewCross.setVisibility(TextView.GONE);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(menuIcon.getWindowToken(), 0);
                    menuCliccked = 0;
                }

            }
        });

        linearLayoutMenuParent.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
            }
        });

        linearLayoutRefresh.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("Clicked", "Clicked");

                if (netCheck()) {

                    final Dialog qtyDialog = new Dialog(SMClientVisitHistory.this);
                    qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    qtyDialog.setContentView(R.layout.warning_dialog);

                    final Button buttonUpdateOk, buttonUpdateCancel;

                    final TextView textQtyValidate;

                    buttonUpdateOk = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateOk);
                    buttonUpdateCancel = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateCancel);

                    textQtyValidate = (TextView) qtyDialog
                            .findViewById(R.id.textQtyValidate);
                    qtyDialog.show();


                    buttonUpdateOk.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            databaseHandler.synTableDelete();
                            Constants.refreshscreen = "clientvisit";
                            Intent io = new Intent(SMClientVisitHistory.this, com.freshorders.freshorder.ui.RefreshActivity.class);
                            startActivity(io);
                            finish();
                            qtyDialog.dismiss();
                        }

                    });

                    buttonUpdateCancel
                            .setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub

                                    qtyDialog.dismiss();

                                }
                            });

                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }
            }

        });


        linearLayoutProfile.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub

                if (netCheck() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    getDetails("profile");
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });
        linearLayoutDistanceCalculation.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub


                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                   Intent into=new Intent(SMClientVisitHistory.this, DistanceCalActivity.class);
                    startActivity(into);


            }
        });
        linearLayoutMyOrders.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheck()) {
                    Constants.orderstatus = "Success";
                    getDetails("myorders");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(SMClientVisitHistory.this,
                                SalesManOrderActivity.class);
                        startActivity(io);
                        finish();
                    }
                }


            }
        });

        linearLayoutMyDealers.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (netCheck() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    getDetails("mydealer");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(SMClientVisitHistory.this,
                                MyDealersActivity.class);
                        io.putExtra("Key", "menuclick");
                        startActivity(io);
                        finish();

                    }
                }

            }
        });

        linearLayoutComplaint.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    getDetails("postnotes");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment");
                    } else {
                        Intent io = new Intent(SMClientVisitHistory.this,
                                MerchantComplaintActivity.class);
                        startActivity(io);
                        finish();
						/*if (Constants.USER_TYPE.equals("D")) {
							Intent io = new Intent(ProfileActivity.this,
									DealersComplaintActivity.class);
							startActivity(io);
							finish();

						} else {
							Intent io = new Intent(ProfileActivity.this,
									MerchantComplaintActivity.class);
							startActivity(io);
							finish();

						}  */
                    }
                }
            }
        });

        linearLayoutCreateOrder.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                MyApplication.getInstance().setTemplate8ForTemplate2(false);

                if (netCheck() == true) {
                    Constants.checkproduct = 0;
                    Constants.orderid = "";
                    Constants.SalesMerchant_Id = "";
                    Constants.Merchantname = "";
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    getDetails("Createorder");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(SMClientVisitHistory.this,
                                CreateOrderActivity.class);
                        startActivity(io);
                        finish();
                    }
                }

            }
        });

        linearLayoutClientVisit.setOnClickListener(new OnClickListener() {

            @Override

            public void onClick(View v) {

                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Constants.ClientStatus = "Success";
                MyOrdersList("Success");
            }
        });


        linearLayoutPaymentCollection.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    getDetails("paymentcoll");
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }


            }
        });
        linearLayoutAddMerchant.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck() == true) {
                    Constants.orderstatus = "Success";
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    getDetails("addmerchant");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(SMClientVisitHistory.this,
                                AddMerchantNew.class);
                        startActivity(io);
                        finish();
                    }
                }


            }
        });

        linearLayoutAcknowledge.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    getDetails("acknowledge");
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }


            }
        });

        linearLayoutStockEntry.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    getDetails("stockentry");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment");
                    }else {
                        Intent io = new Intent(SMClientVisitHistory.this,
                                OutletStockEntry.class);
                        startActivity(io);
                        finish();
                    }
                }


            }
        });
        linearLayoutDistStock.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
               if (netCheck()) {
                    getDetails("diststock");
                } else {
                   if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                       showAlertDialogToast("Please make payment");
                   } else {
                       Intent io = new Intent(SMClientVisitHistory.this,
                               DistributrStockActivity.class);
                       startActivity(io);
                       finish();
                   }
                }


            }
        });
        linearLayoutdownloadscheme.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck()) {

                    final Dialog qtyDialog = new Dialog(SMClientVisitHistory.this);
                    qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    qtyDialog.setContentView(R.layout.warning_dialog);

                    final Button buttonUpdateOk, buttonUpdateCancel;

                    final TextView textQtyValidate;

                    buttonUpdateOk = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateOk);
                    buttonUpdateCancel = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateCancel);

                    textQtyValidate = (TextView) qtyDialog
                            .findViewById(R.id.textQtyValidate);
                    qtyDialog.show();


                    buttonUpdateOk.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            databaseHandler.deletescheme();
                            Constants.downloadScheme = "clientvisit";
                            Intent io = new Intent(SMClientVisitHistory.this, SchemeDownload.class);
                            startActivity(io);
                            finish();
                            qtyDialog.dismiss();
                        }

                    });

                    buttonUpdateCancel
                            .setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub

                                    qtyDialog.dismiss();

                                }
                            });

                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });
        linearLayoutsurveyuser.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheck() == true) {
                    getDetails("surveyuser");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent to = new Intent(SMClientVisitHistory.this, SurveyMerchant.class);
                        startActivity(to);
                        finish();
                    }
                }

            }
        });

        linearLayoutMyDayPlan.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    getDetails("mydayplan");
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }


            }
        });

        linearLayoutMySales.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    getDetails("mysales");
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }


            }
        });

        linearLayoutSignout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                // TODO Auto-generated method stub
                databaseHandler.delete();
                Intent io = new Intent(SMClientVisitHistory.this,
                        SigninActivity.class);
                startActivity(io);
                finish();
            }
        });

        linearLayoutMigration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Intent io = new Intent(SMClientVisitHistory.this,
                        PendingDataMigrationActivity.class);
                startActivity(io);
            }
        });

        linearLayoutStockOnly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Constants.checkproduct = 0;
                Constants.orderid = "";
                Constants.SalesMerchant_Id = "";
                Constants.Merchantname = "";
                MyApplication.getInstance().setTemplate8ForTemplate2(true);
                if (netCheck()) {
                    Intent io = new Intent(SMClientVisitHistory.this,
                            ClosingStockDashBoardActivity.class);
                    startActivity(io);
                    finish();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals("") || PaymentStatus.equalsIgnoreCase("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(SMClientVisitHistory.this,
                                ClosingStockDashBoardActivity.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });

        linearLayoutStockAudit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().setTemplate8ForTemplate2(false);
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Constants.checkproduct = 0;
                Constants.orderid = "";
                Constants.SalesMerchant_Id = "";
                Constants.Merchantname = "";
                if (netCheck()) {
                    Intent io = new Intent(SMClientVisitHistory.this,
                            ClosingStockAudit.class);
                    startActivity(io);
                    finish();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals("") || PaymentStatus.equalsIgnoreCase("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(SMClientVisitHistory.this,
                                ClosingStockAudit.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });

        //////////////
        showMenu();
        /////////////
    }

    private void getDetails(final String paymentcheck) {
        if (paymentcheck.equals("profile")) {
            Intent io = new Intent(SMClientVisitHistory.this,
                    ProfileActivity.class);
            startActivity(io);
            finish();

        } else if (paymentcheck.equals("myorders")) {
            Intent io = new Intent(SMClientVisitHistory.this,
                    SalesManOrderActivity.class);
            startActivity(io);
            finish();

        } else if (paymentcheck.equals("mydealer")) {
            Intent io = new Intent(SMClientVisitHistory.this,
                    MyDealersActivity.class);
            io.putExtra("Key", "menuclick");
            startActivity(io);
            finish();

        } else if (paymentcheck.equals("postnotes")) {
            Intent io = new Intent(SMClientVisitHistory.this,
                    MerchantComplaintActivity.class);
            startActivity(io);
            finish();

        } else if (paymentcheck.equals("placeorder")) {
            Intent to = new Intent(SMClientVisitHistory.this,
                    SalesManOrderCheckoutActivity.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("clientvisit")) {
            Intent to = new Intent(SMClientVisitHistory.this,
                    SMClientVisitHistory.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("paymentcoll")) {
            Intent to = new Intent(SMClientVisitHistory.this,
                    SalesmanPaymentCollectionActivity.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("Createorder")) {
            Intent to = new Intent(SMClientVisitHistory.this,
                    CreateOrderActivity.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("addmerchant")) {
            Intent to = new Intent(SMClientVisitHistory.this,
                    AddMerchantNew.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("acknowledge")) {
            Intent to = new Intent(SMClientVisitHistory.this,
                    SalesmanAcknowledgeActivity.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("stockentry")) {
            Intent to = new Intent(SMClientVisitHistory.this,
                    OutletStockEntry.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("surveyuser")) {
            Intent to = new Intent(SMClientVisitHistory.this,
                    SurveyMerchant.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("mydayplan")) {
            Intent to = new Intent(SMClientVisitHistory.this,
                    MyDayPlan.class);
            startActivity(to);
            finish();
        }else if (paymentcheck.equals("mysales")) {
            Intent to = new Intent(SMClientVisitHistory.this,
                    MySalesActivity.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("diststock")) {
            Intent io = new Intent(SMClientVisitHistory.this,
                    DistributrStockActivity.class);
            startActivity(io);
            finish();
        }
    }


    public void initialcount() {
        String date = "yyyy-MM-dd";

        SimpleDateFormat inputFormat = new SimpleDateFormat(date);
        String dt = inputFormat.format(new Date());
        Cursor curs;

        curs = databaseHandler.getClientVisit("Success");
        Count = String.valueOf(curs.getCount());
        Log.e("ButtonSuccess", Count);
        ButtonSuccess.setText("Success" + "(" + Count + ")");


        curs = databaseHandler.getClientVisit("Pending");
        Count = String.valueOf(curs.getCount());
        Log.e("ButtonPending", Count);
        ButtonPending.setText("Pending" + "(" + Count + ")");

        curs = databaseHandler.getClientVisit("Failed");
        Count = String.valueOf(curs.getCount());
        Log.e("ButtonFailure", Count);
        ButtonFailure.setText("Failed" + "(" + Count + ")");
    }


    public void MyOrdersList(String status) {
        final JSONArray resultSet = new JSONArray();

        String date = "yyyy-MM-dd";

        SimpleDateFormat inputFormat = new SimpleDateFormat(date);
        String dt = inputFormat.format(new Date());


        Cursor curs;
        if(status.equals("Success")){
            curs = databaseHandler.getClientVisitByDate(status, dt);
        }else {
            curs = databaseHandler.getClientVisit(status);
        }

        Log.e("HeaderCount", String.valueOf(curs.getCount()));
        Count = String.valueOf(curs.getCount());
        if (status.equals("Success")) {
            ButtonSuccess.setText("Success" + "(" + Count + ")");
        } else if (status.equals("Pending")) {
            ButtonPending.setText("Pending" + "(" + Count + ")");
        } else if (status.equals("Failed")) {
            ButtonFailure.setText("Failed" + "(" + Count + ")");
        }

        JSONArray jsonArry = new JSONArray();
        curs.moveToFirst();
        while (!curs.isAfterLast()) {

            int totalColumn = curs.getColumnCount();

            JSONObject rowObject = new JSONObject();

            for (int i = 0; i < totalColumn; i++) {
                if (curs.getColumnName(i) != null) {

                    try {

                        if (curs.getString(i) != null) {
                            Log.d("TAG_NAME", curs.getString(i));
                            rowObject.put(curs.getColumnName(i), curs.getString(i));
                        } else {
                            rowObject.put(curs.getColumnName(i), "");
                        }
                    } catch (Exception e) {
                        Log.d("TAG_NAME", e.getMessage());
                    }
                }

            }


            jsonArry.put(rowObject);
            Log.e("jsonarray", jsonArry.toString());

            curs.moveToNext();
        }
        curs.close();


        Log.e("SaveArray", jsonArry.toString());
        int c = Integer.parseInt(Count);
        if (c != 0) {

            try {
                Log.e("inside", "inside");
                arraylistDealerVisitDomain = new ArrayList<DealerVisitDomain>();

                for (int i = 0; i < jsonArry.length(); i++) {
                    JSONObject job = new JSONObject();
                    job = jsonArry.getJSONObject(i);
                    DealerVisitDomain dd = new DealerVisitDomain();

                    dd.setMname(job.getString("mname"));
                    dd.setSerialNo(String.valueOf(i + 1));
                    dd.setGeoLng(job.getString("geolong"));
                    dd.setGeoLat(job.getString("geolat"));
                    dd.setNotes(job.getString("remarks"));
                    dd.setPlanId(job.getString("suserid"));
                    dd.setSname(job.getString("id"));

                    time = job.getString("orderplndt");
                    Log.e("time", time);
                    String inputPattern = "yyyy-MM-dd HH:mm:ss";
                    String outputPattern = "dd-MMM-yyyy HH:mm:ss";
                    SimpleDateFormat inputFormat1 = new SimpleDateFormat(inputPattern);
                    SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

                    String str = null;

                    try {
                        dateStr = inputFormat1.parse(time);
                        str = outputFormat.format(dateStr);
                        dd.setDate(str);
                        //Log.e("str", str);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    arraylistDealerVisitDomain.add(dd);


                    Log.e("Size", String.valueOf(arraylistDealerVisitDomain.size()));

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            setadap();
        } else {
            Log.e("outside", "outside");
            listViewOrders.setVisibility(View.GONE);
            textViewNodata.setVisibility(View.VISIBLE);
            if (status.equals("Success")) {
                textViewNodata.setText("No items");
            } else if (status.equals("Pending")) {
                textViewNodata.setText("No Pending items");
            } else if (status.equals("Failed")) {
                textViewNodata.setText("No Failed items");
            }


        }
    }


    public void setadap() {
        // TODO Auto-generated method stub

        adapter = new DealerVisitAdapter(SMClientVisitHistory.this, R.layout.item_dealer_visit_page, arraylistDealerVisitDomain);
        listViewOrders.setVisibility(View.VISIBLE);
        listViewOrders.setAdapter(adapter);

    }

    protected void toastDisplay(String msg) {
        Toast toast = Toast.makeText(SMClientVisitHistory.this, msg,
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();

    }

    public boolean netCheck() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            } else if (result == null) {
               /* showAlertDialog(SalesManOrderActivity.this,
                        "No Internet Connection",
                        "Please Check Your internet connection.", false);*/
                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public void showAlertDialog(Context context, String title, String message,
                                Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        alertDialog.show();
    }

    public void showAlertDialogToast(String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    public boolean netCheckwithoutAlert() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            } else if (result == null) {

                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public void onBackPressed() {
        exitAlret();
    }

    private void exitAlret() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        localBuilder.setMessage("Do you want to Exit?");
        localBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {

                        finish();

                    }
                });
        localBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {
                        paramDialogInterface.cancel();
                    }
                });
        localBuilder.create().show();
    }

}
