package com.freshorders.freshorder.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.activity.DistanceCalActivity;
import com.freshorders.freshorder.adapter.MySalesAdapter;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.OutletStockEntryDomain;
import com.freshorders.freshorder.toonline.migration.PendingDataMigrationActivity;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;
import com.leavjenn.smoothdaterangepicker.date.SmoothDateRangePickerFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class MySalesActivity extends Activity {
    private TextView textViewCalendar, textViewSatrtDate, textViewEndDate, textViewNoData, textViewAssetMenu;
    private int year, hr, sec, mins, day, mnth;
    public static Date dateStr = null, dateStr1 = null;
    ListView listViewMySales;
    String fromDate = "", toDate = "";
    public static JSONObject JsonAccountObject = null;
    public static JSONArray JsonAccountArray = null;
    public static JsonServiceHandler JsonServiceHandler;
    ArrayList<OutletStockEntryDomain> arraylistMySales;
    MySalesAdapter adapter;
    public static DatabaseHandler databaseHandler;
    public static Context context;
    Date dateStr11 = null, manufdateStr = null;

    public static String PaymentStatus = "NULL", moveto = "NULL";

    TextView menuIcon, textViewAssetMenuProfile, textViewAssetMenuMyOrders,
            textViewAssetMenuMyDealers, textViewAssetMenuProducts,
            textViewAssetMenuPayment, textViewAssetMenuComplaint,textViewAssetMenuDistanceCalculation,
            textViewAssetMenuSignout, textViewAssetMenuClientVisit, textViewAssetMenuPaymentCollection,
            textViewNodata, textViewAssetMenuCreateOrder, textViewcross_1, textViewAssetMenuRefresh, textViewAssetMenuStockEntry, textViewAssetMenuAddMerchant, textViewAssetMenuAcknowledge, textViewAssetMenusurveyuser, textViewAssetMenudownloadscheme, textViewAssetMenuMyDayPlan, textViewAssetMenuMySales,
            textViewAssetMenuDistStock, textViewAssetMenuStockOnly, textViewAssetMenuMigration, textViewAssetMenuStockAudit;

    public static int menuCliccked;


    LinearLayout linearLayoutMenuParent, linearLayoutProfile,
            linearLayoutMyOrders, linearLayoutMyDealers, linearLayoutProducts,
            linearLayoutPayment, linearLayoutPaymentCollection, linearLayoutComplaint, linearLayoutSignout,
            linearlayoutSearchIcon, linearLayoutCreateOrder, linearLayoutClientVisit, linearLayoutRefresh,
            linearLayoutAcknowledge, linearLayoutStockEntry, linearLayoutAddMerchant,
            linearLayoutsurveyuser, linearLayoutdownloadscheme, linearLayoutMyDayPlan, linearLayoutMySales,linearLayoutDistanceCalculation,
            linearLayoutDistStock, linearLayoutStockOnly, linearLayoutMigration, linearLayoutStockAudit;;

    private void showMenu(){

        MyApplication app = MyApplication.getInstance();

        if(!app.isCreateOrder()){
            linearLayoutCreateOrder.setVisibility(View.GONE);
        }
        if(!app.isProfile()){
            linearLayoutProfile.setVisibility(View.GONE);
        }
        if(!app.isMyDayPlan()){
            linearLayoutMyDayPlan.setVisibility(View.GONE);
        }
        if(!app.isMyOrders()){
            linearLayoutMyOrders.setVisibility(View.GONE);
        }
        if(!app.isAddMerchant()){
            linearLayoutAddMerchant.setVisibility(View.GONE);
        }
        if(!app.isMySales()){
            linearLayoutMySales.setVisibility(View.GONE);
        }
        if(!app.isPostNotes()){
            linearLayoutComplaint.setVisibility(View.GONE);
        }
        if(!app.isMyClientVisit()){
            linearLayoutClientVisit.setVisibility(View.GONE);
        }
        if(!app.isAcknowledge()){
            linearLayoutAcknowledge.setVisibility(View.GONE);
        }
        if(!app.isPaymentCollection()){
            linearLayoutPaymentCollection.setVisibility(View.GONE);
        }
        if(!app.isPkdDataCapture()){
            linearLayoutStockEntry.setVisibility(View.GONE);
        }
        if(!app.isDistributorStock()){
            linearLayoutDistStock.setVisibility(View.GONE);
        }
        if(!app.isSurveyUser()){
            linearLayoutsurveyuser.setVisibility(View.GONE);
        }
        if(!app.isDownLoadScheme()){
            linearLayoutdownloadscheme.setVisibility(View.GONE);
        }
        if(!app.isDistanceCalculation()){
            linearLayoutDistanceCalculation.setVisibility(View.GONE);
        }
        if(!app.isRefresh()){
            linearLayoutRefresh.setVisibility(View.GONE);
        }
        if(!app.isLogout()){
            linearLayoutSignout.setVisibility(View.GONE);
        }
        if(!app.isClosingStock()){
            linearLayoutStockOnly.setVisibility(View.GONE);
        }
        if(!app.isClosingStockAudit()){
            linearLayoutStockAudit.setVisibility(View.GONE);
        }
        if(!app.isPendingData()){
            linearLayoutMigration.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.mysales_activity);

        textViewCalendar = (TextView) findViewById(R.id.textViewCalendar);
        textViewSatrtDate = (TextView) findViewById(R.id.textViewSatrtDate);
        textViewEndDate = (TextView) findViewById(R.id.textViewEndDate);
        listViewMySales = (ListView) findViewById(R.id.listViewMySales);
        textViewNoData = (TextView) findViewById(R.id.textViewNoData);

        menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
        linearLayoutMenuParent = (LinearLayout) findViewById(R.id.linearLayoutMenuParent);

        linearLayoutProfile = (LinearLayout) findViewById(R.id.linearLayoutProfile);
        linearLayoutCreateOrder = (LinearLayout) findViewById(R.id.linearLayoutCreateOrder);
        linearLayoutMyOrders = (LinearLayout) findViewById(R.id.linearLayoutMyOrders);
        linearLayoutMyDealers = (LinearLayout) findViewById(R.id.linearLayoutMyDealers);
        linearLayoutProducts = (LinearLayout) findViewById(R.id.linearLayoutProducts);
        linearLayoutPayment = (LinearLayout) findViewById(R.id.linearLayoutPayment);
        linearLayoutComplaint = (LinearLayout) findViewById(R.id.linearLayoutComplaint);
        linearLayoutClientVisit = (LinearLayout) findViewById(R.id.linearLayoutClientVisit);
        linearLayoutPaymentCollection = (LinearLayout) findViewById(R.id.linearLayoutPaymentCollection);
        linearLayoutAcknowledge = (LinearLayout) findViewById(R.id.linearLayoutAcknowledge);
        linearLayoutStockEntry = (LinearLayout) findViewById(R.id.linearLayoutStockEntry);
        linearLayoutAddMerchant = (LinearLayout) findViewById(R.id.linearLayoutAddMerchant);
        linearLayoutdownloadscheme = (LinearLayout) findViewById(R.id.linearLayoutdownloadscheme);
        linearLayoutsurveyuser = (LinearLayout) findViewById(R.id.linearLayoutsurveyuser);
        linearLayoutMyDayPlan = (LinearLayout) findViewById(R.id.linearLayoutMyDayPlan);
        linearLayoutMySales = (LinearLayout) findViewById(R.id.linearLayoutMySales);
        linearLayoutSignout = (LinearLayout) findViewById(R.id.linearLayoutSignout);
        linearLayoutRefresh= (LinearLayout) findViewById(R.id.linearLayoutRefresh);
        linearLayoutDistanceCalculation= (LinearLayout) findViewById(R.id.linearLayoutDistanceCalculation);
        linearLayoutMyDealers.setVisibility(View.GONE);

        textViewAssetMenuCreateOrder = (TextView) findViewById(R.id.textViewAssetMenuCreateOrder);
        textViewAssetMenuProfile = (TextView) findViewById(R.id.textViewAssetMenuProfile);
        textViewAssetMenuMyOrders = (TextView) findViewById(R.id.textViewAssetMenuMyOrders);
        textViewAssetMenuMyDealers = (TextView) findViewById(R.id.textViewAssetMenuMyDealers);
        textViewAssetMenuProducts = (TextView) findViewById(R.id.textViewAssetMenuProducts);
        textViewAssetMenuPayment = (TextView) findViewById(R.id.textViewAssetMenuPayment);
        textViewAssetMenuComplaint = (TextView) findViewById(R.id.textViewAssetMenuComplaint);
        textViewAssetMenuSignout = (TextView) findViewById(R.id.textViewAssetMenuSignout);
        textViewAssetMenuClientVisit = (TextView) findViewById(R.id.textViewAssetMenuClientVisit);
        textViewAssetMenuAddMerchant = (TextView) findViewById(R.id.textViewAssetMenuAddMerchant);
        textViewAssetMenuAcknowledge = (TextView) findViewById(R.id.textViewAssetMenuAcknowledge);
        textViewAssetMenuStockEntry = (TextView) findViewById(R.id.textViewAssetMenuStockEntry);
        textViewAssetMenudownloadscheme = (TextView) findViewById(R.id.textViewAssetMenudownloadscheme);
        textViewAssetMenusurveyuser = (TextView) findViewById(R.id.textViewAssetMenusurveyuser);
        textViewAssetMenuMyDayPlan = (TextView) findViewById(R.id.textViewAssetMenuMyDayPlan);
        textViewAssetMenuMySales = (TextView) findViewById(R.id.textViewAssetMenuMySales);
        textViewAssetMenuPaymentCollection = (TextView) findViewById(R.id.textViewAssetMenuPaymentCollection);
        textViewAssetMenuRefresh = (TextView) findViewById(R.id.textViewAssetMenuRefresh);
        textViewAssetMenuDistanceCalculation = (TextView) findViewById(R.id.textViewAssetMenuDistanceCalculation);
        databaseHandler = new DatabaseHandler(getApplicationContext());
        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();

        Cursor curPay;
        curPay = databaseHandler.getDetails();
        if(curPay != null && curPay.getCount() > 0){
            curPay.moveToFirst();
            PaymentStatus = curPay.getString(curPay.getColumnIndex(DatabaseHandler.KEY_paymentStatus));
            curPay.close();
        }

        Typeface font = Typeface.createFromAsset(getAssets(),
                "fontawesome-webfont.ttf");
        textViewCalendar.setTypeface(font);
        menuIcon.setTypeface(font);
        textViewAssetMenuCreateOrder.setTypeface(font);
        textViewAssetMenuProfile.setTypeface(font);
        textViewAssetMenuMyOrders.setTypeface(font);
        textViewAssetMenuMyDealers.setTypeface(font);
        textViewAssetMenuProducts.setTypeface(font);
        textViewAssetMenuPayment.setTypeface(font);
        textViewAssetMenuComplaint.setTypeface(font);
        textViewAssetMenuSignout.setTypeface(font);
        textViewAssetMenuClientVisit.setTypeface(font);
        textViewAssetMenuPaymentCollection.setTypeface(font);
        textViewAssetMenuRefresh.setTypeface(font);
        textViewAssetMenuAcknowledge.setTypeface(font);
        textViewAssetMenuStockEntry.setTypeface(font);
        textViewAssetMenuAddMerchant.setTypeface(font);
        textViewAssetMenusurveyuser.setTypeface(font);
        textViewAssetMenudownloadscheme.setTypeface(font);
        textViewAssetMenuMyDayPlan.setTypeface(font);
        textViewAssetMenuMySales.setTypeface(font);
        textViewAssetMenuDistanceCalculation.setTypeface(font);

        linearLayoutDistStock = (LinearLayout) findViewById(R.id.linearLayoutDistStock);
        textViewAssetMenuDistStock = (TextView) findViewById(R.id.textViewAssetMenuDistStock);
        textViewAssetMenuDistStock.setTypeface(font);

        textViewAssetMenuStockOnly = (TextView) findViewById(R.id.textViewAssetMenuStockOnly);
        textViewAssetMenuStockAudit = (TextView) findViewById(R.id.textViewAssetMenuStockAudit);
        textViewAssetMenuMigration = findViewById(R.id.textViewAssetMenuMigration);

        textViewAssetMenuStockOnly.setTypeface(font);
        textViewAssetMenuStockAudit.setTypeface(font);
        textViewAssetMenuMigration.setTypeface(font);

        linearLayoutStockOnly = (LinearLayout) findViewById(R.id.linearLayoutStockOnly);
        linearLayoutStockAudit = findViewById(R.id.linearLayoutStockAudit);
        linearLayoutMigration = (LinearLayout) findViewById(R.id.linearLayoutMigration);

        menuIcon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (menuCliccked == 0) {
                    linearLayoutMenuParent.setVisibility(View.VISIBLE);
                    InputMethodManager iss = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    iss.hideSoftInputFromWindow(menuIcon.getWindowToken(), 0);
                    menuCliccked = 1;
                } else {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    InputMethodManager iss = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    iss.hideSoftInputFromWindow(menuIcon.getWindowToken(), 0);
                    menuCliccked = 0;
                }

            }
        });

        menuIcon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (menuCliccked == 0) {
                    linearLayoutMenuParent.setVisibility(View.VISIBLE);
                    menuCliccked = 1;
                } else {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                }

            }
        });

        linearLayoutMenuParent.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
            }
        });

        linearLayoutProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto = "profile";
                if (netCheckwithoutAlert() == true) {
                    getData();
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });

        linearLayoutRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("Clicked", "Clicked");

                if (netCheck()) {

                    final Dialog qtyDialog = new Dialog(MySalesActivity.this);
                    qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    qtyDialog.setContentView(R.layout.warning_dialog);

                    final Button buttonUpdateOk, buttonUpdateCancel;

                    final TextView textQtyValidate;

                    buttonUpdateOk = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateOk);
                    buttonUpdateCancel = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateCancel);

                    textQtyValidate = (TextView) qtyDialog
                            .findViewById(R.id.textQtyValidate);
                    qtyDialog.show();


                    buttonUpdateOk.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            databaseHandler.synTableDelete();
                            Constants.refreshscreen = "mysales";
                            Intent io = new Intent(MySalesActivity.this, RefreshActivity.class);
                            startActivity(io);
                            finish();
                            qtyDialog.dismiss();
                        }

                    });

                    buttonUpdateCancel
                            .setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub

                                    qtyDialog.dismiss();

                                }
                            });

                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }
            }

        });

        linearLayoutMyOrders.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Constants.checkproduct = 0;
                linearLayoutMenuParent.setVisibility(View.GONE);
                moveto = "myorder";
                if (netCheckwithoutAlert()) {
                    Constants.orderstatus = "Success";
                    getData();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Constants.orderstatus = "Success";
                        Intent io = new Intent(MySalesActivity.this,
                                SalesManOrderActivity.class);
                        io.putExtra("Key","menuclick");
                        Constants.setimage = null;
                        startActivity(io);
                        finish();
                        /*if (Constants.USER_TYPE.equals("D")) {
                            Intent io = new Intent(MySalesActivity.this,
                                    DealersOrderActivity.class);
                            io.putExtra("Key","menuclick");
                            startActivity(io);
                            finish();
                        } else if (Constants.USER_TYPE.equals("M")) {
                            Intent io = new Intent(MySalesActivity.this,
                                    MerchantOrderActivity.class);
                            io.putExtra("Key","menuclick");
                            Constants.setimage = null;
                            startActivity(io);
                            finish();
                        } else {
                            Intent io = new Intent(MySalesActivity.this,
                                    SalesManOrderActivity.class);
                            io.putExtra("Key","menuclick");
                            Constants.setimage = null;
                            startActivity(io);
                            finish();
                        }  */

                    }
                }
            }
        });

        linearLayoutMyDealers.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                moveto = "Mydealer";
                if (netCheckwithoutAlert() == true) {
                    getData();
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }


            }
        });


        linearLayoutCreateOrder.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                MyApplication.getInstance().setTemplate8ForTemplate2(false);

                linearLayoutMenuParent.setVisibility(View.GONE);
                moveto = "createorder";
                Constants.checkproduct = 0;
                Constants.orderid = "";
                if (netCheckwithoutAlert() == true) {
                    getData();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment to place order");
                    } else {
                        Intent io = new Intent(MySalesActivity.this, CreateOrderActivity.class);
                        startActivity(io);
                        finish();
                    }
                }


            }
        });


        linearLayoutComplaint.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto = "postnotes";
                if (netCheckwithoutAlert() == true) {
                    getData();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment");
                    } else {
                        Intent io = new Intent(MySalesActivity.this,
                                MerchantComplaintActivity.class);
                        startActivity(io);
                        finish();
                        /*if (Constants.USER_TYPE.equals("D")) {
                            Intent io = new Intent(MySalesActivity.this,
                                    DealersComplaintActivity.class);
                            startActivity(io);
                            finish();

                        } else {
                            Intent io = new Intent(MySalesActivity.this,
                                    MerchantComplaintActivity.class);
                            startActivity(io);
                            finish();

                        } */
                    }
                }

            }
        });

        linearLayoutClientVisit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto = "clientvisit";
                if (netCheckwithoutAlert() == true) {
                    getData();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment");
                    } else {
                        Intent io = new Intent(MySalesActivity.this,
                                SMClientVisitHistory.class);
                        startActivity(io);
                        finish();
                        /*if (Constants.USER_TYPE.equals("D")) {
                            Intent io = new Intent(MySalesActivity.this,
                                    DealerClientVisit.class);
                            startActivity(io);
                            finish();
                        } else {
                            Intent io = new Intent(MySalesActivity.this,
                                    SMClientVisitHistory.class);
                            startActivity(io);
                            finish();
                        }  */
                    }
                }


            }
        });
        linearLayoutPaymentCollection.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto = "paymentcoll";
                if (netCheckwithoutAlert() == true) {
                    getData();
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });

        linearLayoutStockEntry.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto = "stockentry";
                if (netCheckwithoutAlert()) {
                    getData();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment");
                    }else {
                        Intent io = new Intent(MySalesActivity.this,
                                OutletStockEntry.class);
                        startActivity(io);
                        finish();
                    }
                }


            }
        });

        linearLayoutDistStock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto = "diststock";
                if (netCheckwithoutAlert() == true) {
                    getData();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment");
                    } else {
                        Intent io = new Intent(MySalesActivity.this,
                                DistributrStockActivity.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });

        linearLayoutAcknowledge.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto = "acknowledge";
                if (netCheckwithoutAlert() == true) {
                    getData();
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });

        linearLayoutAddMerchant.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                moveto = "addmerchant";
                if (netCheckwithoutAlert() == true) {
                    getData();
                } else {

                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment");
                    } else {
                        Intent to = new Intent(MySalesActivity.this,
                                AddMerchantNew.class);
                        startActivity(to);
                        finish();
                    }
                }

            }
        });
        linearLayoutdownloadscheme.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck()) {

                    final Dialog qtyDialog = new Dialog(MySalesActivity.this);
                    qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    qtyDialog.setContentView(R.layout.warning_dialog);

                    final Button buttonUpdateOk, buttonUpdateCancel;

                    final TextView textQtyValidate;

                    buttonUpdateOk = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateOk);
                    buttonUpdateCancel = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateCancel);

                    textQtyValidate = (TextView) qtyDialog
                            .findViewById(R.id.textQtyValidate);
                    qtyDialog.show();


                    buttonUpdateOk.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            databaseHandler.deletescheme();

                            Constants.downloadScheme = "mysales";
                            Intent io = new Intent(MySalesActivity.this, SchemeDownload.class);
                            startActivity(io);
                            finish();
                            qtyDialog.dismiss();
                        }

                    });

                    buttonUpdateCancel
                            .setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub

                                    qtyDialog.dismiss();

                                }
                            });

                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });

        linearLayoutsurveyuser.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheck() == true) {
                    moveto = "surveyuser";
                    getData();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent to = new Intent(MySalesActivity.this, SurveyMerchant.class);
                        startActivity(to);
                        finish();
                    }
                }
            }
        });
        linearLayoutDistanceCalculation.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    //moveto = "surveyuser";
                    Intent io = new Intent(MySalesActivity.this, DistanceCalActivity.class);
                    startActivity(io);

            }
        });
        linearLayoutMyDayPlan.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck()) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    moveto = "mydayplan";
                    getData();
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }
            }
        });

        linearLayoutMySales.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
            }
        });

        linearLayoutSignout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                // TODO Auto-generated method stub
                databaseHandler.delete();
                Intent io = new Intent(MySalesActivity.this, SigninActivity.class);
                startActivity(io);
                finish();
            }
        });

        linearLayoutMigration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Intent io = new Intent(MySalesActivity.this,
                        PendingDataMigrationActivity.class);
                startActivity(io);
            }
        });

        linearLayoutStockOnly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Constants.checkproduct = 0;
                Constants.orderid = "";
                Constants.SalesMerchant_Id = "";
                Constants.Merchantname = "";
                MyApplication.getInstance().setTemplate8ForTemplate2(true);
                if (netCheck()) {
                    Intent io = new Intent(MySalesActivity.this,
                            ClosingStockDashBoardActivity.class);
                    startActivity(io);
                    finish();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals("") || PaymentStatus.equalsIgnoreCase("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(MySalesActivity.this,
                                ClosingStockDashBoardActivity.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });

        linearLayoutStockAudit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().setTemplate8ForTemplate2(false);
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Constants.checkproduct = 0;
                Constants.orderid = "";
                Constants.SalesMerchant_Id = "";
                Constants.Merchantname = "";
                if (netCheck()) {
                    Intent io = new Intent(MySalesActivity.this,
                            ClosingStockAudit.class);
                    startActivity(io);
                    finish();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals("") || PaymentStatus.equalsIgnoreCase("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(MySalesActivity.this,
                                ClosingStockAudit.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });

        showCurrentDateOnView();

        textViewCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SmoothDateRangePickerFragment smoothDateRangePickerFragment =
                        SmoothDateRangePickerFragment
                                .newInstance(new SmoothDateRangePickerFragment.OnDateRangeSetListener() {
                                    @Override
                                    public void onDateRangeSet(SmoothDateRangePickerFragment view,
                                                               int yearStart, int monthStart,
                                                               int dayStart, int yearEnd,
                                                               int monthEnd, int dayEnd) {
                                        String date = "You picked the following date range: \n"
                                                + "From " + dayStart + "/" + (++monthStart)
                                                + "/" + yearStart + " To " + dayEnd + "/"
                                                + (++monthEnd) + "/" + yearEnd;

                                        String startDate = dayStart + "-" + (monthStart) + "-" + yearStart;
                                        String endDate = dayEnd + "-" + (monthEnd) + "-" + yearEnd;

                                        fromDate = yearStart + "-" + monthStart + "-" + dayStart;
                                        toDate = yearEnd + "-" + monthEnd + "-" + dayEnd;


                                        String inputPattern = "dd-MM-yyyy";
                                        String outputPattern = "dd-MMM-yyyy";
                                        SimpleDateFormat inputFormat = new SimpleDateFormat(
                                                inputPattern);
                                        SimpleDateFormat outputFormat = new SimpleDateFormat(
                                                outputPattern);

                                        String str = null, str1 = null;
                                        try {
                                            dateStr = inputFormat.parse(startDate);
                                            str = outputFormat.format(dateStr);
                                            textViewSatrtDate.setText(str);


                                            dateStr1 = inputFormat.parse(endDate);
                                            str1 = outputFormat.format(dateStr1);
                                            textViewEndDate.setText(str1);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        if (netCheck()) {
                                            mysales(textViewSatrtDate.getText().toString(), textViewEndDate.getText().toString());
                                        }
                                    }
                                });
                smoothDateRangePickerFragment.show(getFragmentManager(), "Datepickerdialog");
            }
        });

        ////////////
        showMenu();
        ///////////

    }

    public void mysales(final String startdate, final String enddate) {
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "", strMsg = "";

            @Override
            protected void onPreExecute() {

                dialog = ProgressDialog.show(MySalesActivity.this, "",
                        "Loading...", true, true);
                dialog.setCancelable(false);
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
                try {


                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);


                    if (strStatus.equals("true")) {
                        dialog.dismiss();
                        listViewMySales.setVisibility(View.VISIBLE);
                        textViewNoData.setVisibility(View.GONE);
                        arraylistMySales = new ArrayList<OutletStockEntryDomain>();
                        JSONArray job1 = JsonAccountObject.getJSONArray("data");

                        for (int i = 0; i < job1.length(); i++) {

                            JSONObject job = new JSONObject();
                            OutletStockEntryDomain db = new OutletStockEntryDomain();

                            job = job1.getJSONObject(i);
                            String prodname = job.getString("prodname");
                            String prodcode = job.getString("prodcode");

                            String qtotal = job.getString("qtotal");
                            String uom = job.getString("uom");

                            if (qtotal.equals(null) || qtotal.equals("null")|| qtotal.equals(" ")) {
                                qtotal = "0";
                            }


                            if (uom.equals(null) || uom.equals("null")|| uom.equals(" ")) {
                                uom = "";
                            }

                            db.setprodcode(prodcode);
                            db.setprodname(prodname);
                            db.setqty(qtotal);
                            db.setUOM(uom);
                            db.setserialno(String.valueOf(i + 1));
                            db.setPvalue(job.getString("value"));

                            arraylistMySales.add(db);
                        }

                        setadap();

                    } else {
                        dialog.dismiss();
                        listViewMySales.setVisibility(View.GONE);
                        textViewNoData.setVisibility(View.VISIBLE);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {

                }

            }


            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();

                String inputPattern = "dd-MMM-yyyy";
                String outputPattern = "yyyy-MM-dd";
                SimpleDateFormat inputFormat = new SimpleDateFormat(
                        inputPattern);
                SimpleDateFormat outputFormat = new SimpleDateFormat(
                        outputPattern);

                String str = null, manufstr = null;
                try {
                    dateStr11 = inputFormat.parse(startdate);
                    manufdateStr = inputFormat.parse(enddate);
                    str = outputFormat.format(dateStr11);
                    manufstr = outputFormat.format(manufdateStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    jsonObject.put("suserid", Constants.USER_ID);
                    jsonObject.put("reporttype", "SALES");
                    jsonObject.put("usertype", "S");
                    jsonObject.put("fromdt", str);
                    jsonObject.put("todt", manufstr);


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                JsonServiceHandler = new JsonServiceHandler(Utils.strMySales, jsonObject.toString(), MySalesActivity.this);
                JsonAccountObject = JsonServiceHandler.ServiceData();


                return null;

            }

        }.execute(new Void[]{});

    }

    public void setadap() {
        // TODO Auto-generated method stub

        adapter = new MySalesAdapter(MySalesActivity.this, R.layout.item_my_sales_screen, arraylistMySales);
        listViewMySales.setVisibility(View.VISIBLE);
        listViewMySales.setAdapter(adapter);
        textViewNoData.setVisibility(View.GONE);
    }

    private void showCurrentDateOnView() {
        // TODO Auto-generated method stub


        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        mnth = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        hr = c.get(Calendar.HOUR_OF_DAY);
        mins = c.get(Calendar.MINUTE);
        sec = c.get(Calendar.SECOND);

        String monthLength = String.valueOf(mnth + 1);
        if (monthLength.length() == 1) {
            monthLength = "0" + monthLength;
        }
        String dayLength = String.valueOf(day);
        if (dayLength.length() == 1) {
            dayLength = "0" + dayLength;
        }


        textViewSatrtDate.setText(new StringBuilder()
                .append(year).append("-").append(monthLength).append("-").append(dayLength)
        );


        String time = textViewSatrtDate.getText().toString();
        fromDate = time;
        toDate = time;

        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd-MMM-yyyy ";
        SimpleDateFormat inputFormat = new SimpleDateFormat(
                inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(
                outputPattern);

        String str = null;
        try {
            dateStr = inputFormat.parse(time);
            str = outputFormat.format(dateStr);
            textViewSatrtDate.setText(str);
            textViewEndDate.setText(str);

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (netCheck()) {
            mysales(textViewSatrtDate.getText().toString(), textViewEndDate.getText().toString());
        }

    }

    private void getData() {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "";

            @Override
            protected void onPreExecute() {
                dialog = ProgressDialog.show(MySalesActivity.this, "",
                        "Loading...", true, true);

            }

            @Override
            protected void onPostExecute(Void result) {

                try {

                    PaymentStatus = (JsonAccountObject.getString("pymtstatus"));
                    Log.e("PaymentStatus", PaymentStatus);

                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment to place order");
                    } else {
                        if (moveto.equals("myorder")) {

                            Intent io = new Intent(MySalesActivity.this,
                                    SalesManOrderActivity.class);
                            startActivity(io);
                            finish();

                        } else if (moveto.equals("createorder")) {
                            Intent io = new Intent(MySalesActivity.this,
                                    CreateOrderActivity.class);
                            startActivity(io);
                            finish();
                        } else if (moveto.equals("Mydealer")) {
                            Intent io = new Intent(MySalesActivity.this,
                                    MyDealersActivity.class);
                            io.putExtra("Key", "MenuClick");
                            startActivity(io);
                            finish();
                        } else if (moveto.equals("profile")) {
                            Intent io = new Intent(MySalesActivity.this,
                                    ProfileActivity.class);
                            startActivity(io);
                            finish();
                        } else if (moveto.equals("postnotes")) {
                            Intent io = new Intent(MySalesActivity.this,
                                    MerchantComplaintActivity.class);
                            startActivity(io);
                            finish();
                        } else if (moveto.equals("clientvisit")) {
                            Intent io = new Intent(MySalesActivity.this,
                                    SMClientVisitHistory.class);
                            startActivity(io);
                            finish();
                        } else if (moveto.equals("addmerchant")) {
                            Intent io = new Intent(MySalesActivity.this,
                                    AddMerchantNew.class);
                            startActivity(io);
                            finish();
                        } else if (moveto.equals("stockentry")) {
                            Intent io = new Intent(MySalesActivity.this,
                                    OutletStockEntry.class);
                            startActivity(io);
                            finish();
                        } else if (moveto.equals("acknowledge")) {
                            Intent io = new Intent(MySalesActivity.this,
                                    SalesmanAcknowledgeActivity.class);
                            startActivity(io);
                            finish();
                        } else if (moveto.equals("surveyuser")) {
                            Intent to = new Intent(MySalesActivity.this,
                                    SurveyMerchant.class);
                            startActivity(to);
                            finish();
                        } else if (moveto.equals("mydayplan")) {
                            Intent to = new Intent(MySalesActivity.this,
                                    MyDayPlan.class);
                            startActivity(to);
                            finish();
                        } else if (moveto.equals("paymentcoll")) {
                            Intent to = new Intent(MySalesActivity.this,
                                    SalesmanPaymentCollectionActivity.class);
                            startActivity(to);
                            finish();
                        } else if (moveto.equals("diststock")) {
                            Intent io = new Intent(MySalesActivity.this,
                                    DistributrStockActivity.class);
                            startActivity(io);
                            finish();
                        }

                    }

                    dialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();


                JsonServiceHandler = new JsonServiceHandler(Utils.strGetProfileDetail + Constants.USER_ID, MySalesActivity.this);
                JsonAccountObject = JsonServiceHandler.ServiceDataGet();
                return null;
            }
        }.execute(new Void[]{});
    }

    public boolean netCheck() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            } else if (result == null) {
                showAlertDialog(MySalesActivity.this,
                        "No Internet Connection",
                        "Please Check Your internet connection.", false);
                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public void showAlertDialog(Context context, String title, String message,
                                Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        alertDialog.show();
    }

    public void onBackPressed() {
        exitAlret();
    }

    private void exitAlret() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        localBuilder.setMessage("Do you want to Exit?");
        localBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {

                        finish();

                    }
                });
        localBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {
                        paramDialogInterface.cancel();
                    }
                });
        localBuilder.create().show();
    }

    public void showAlertDialogToast(String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                        InputMethodManager inn = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                        inn.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    public boolean netCheckwithoutAlert() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            } else if (result == null) {

                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

}

