package com.freshorders.freshorder.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.DealerOrderDetailAdapter;
import com.freshorders.freshorder.adapter.SalesmanCameraAdapter;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.DealerOrderDetailDomain;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class SalesmanOrderDetailsActivity extends Activity {

	TextView textViewName,textViewAddress,textViewDate,menuIcon,textViewdelord,textViewQty;
	LinearLayout linearLayoutBack;
	JSONObject JsonAccountObject = null;
	JSONArray JsonAccountArray = null;
	JsonServiceHandler JsonServiceHandler ;
	public static ArrayList<DealerOrderDetailDomain> arraylistDealerOrderDetailList;
	ListView listViewOrders;
	Date dateStr = null;
	public static String time;
	Button buttonUpdate,buttonAddNewProducts,buttonAddproductsecond,buttonUpdatesecond;
	DatabaseHandler databaseHandler;
	public static String[] orderdtid, orderid,prodid,prodPrice,qty,date,status,freeQty,duserid,flag,
			defaultuom,selectedUom,deliverydate,deliverymode,deliverytime,pvalue,defaultuomtotal;
	String[] ordtlId;
	public static String pushStatus, OrderId;
	ArrayList<String> Orderdtlist ;
	SQLiteDatabase db;
	public static int popup=0;
	LinearLayout linearlayoutsecond,linearlayoutmain,linearlayoutmaingallery;
	Gallery gridViewcamera;
	double approximateTotal=0.0;
	String appr="";
	private boolean isOrderProcessed = false;  // kumaravel need to check if online order is delivered or partial delivered
	LinearLayout llCStock;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(1);
		setContentView(R.layout.dealer_order_detail_screen);
		databaseHandler = new DatabaseHandler(getApplicationContext());
		netCheck();
		db=getApplicationContext().openOrCreateDatabase("freshorders", 0, null);
		llCStock = findViewById(R.id.ll_closing_stock);
		textViewName = (TextView) findViewById(R.id.textViewName);
		textViewAddress = (TextView) findViewById(R.id.textViewAddress);
		textViewDate = (TextView) findViewById(R.id.textViewDate);
		textViewdelord = (TextView) findViewById(R.id.textViewdelord);
		textViewQty = (TextView) findViewById(R.id.textViewQty);
		menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
		listViewOrders = (ListView) findViewById(R.id.listViewOrders);
		buttonUpdate = (Button) findViewById(R.id.buttonUpdate);
		buttonAddNewProducts= (Button) findViewById(R.id.buttonAddproduct);
		buttonAddproductsecond= (Button) findViewById(R.id.buttonAddproductsecond);
		buttonUpdatesecond= (Button) findViewById(R.id.buttonUpdatesecond);
		gridViewcamera= (Gallery) findViewById(R.id.gridViewcamera);
		linearlayoutsecond= (LinearLayout) findViewById(R.id.linearlayoutsecond);
		linearlayoutmain= (LinearLayout) findViewById(R.id.linearlayoutmain);
		linearlayoutmaingallery= (LinearLayout) findViewById(R.id.linearlayoutmaingallery);
		Orderdtlist =new  ArrayList<String>();
		linearLayoutBack = (LinearLayout) findViewById(R.id.linearLayoutBack);
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();

		gridViewcamera.setSelection(1);
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) gridViewcamera.getLayoutParams();
		mlp.setMargins(-450, 0, 0, 0);

		Typeface font = Typeface.createFromAsset(getAssets(),
				"fontawesome-webfont.ttf");
		menuIcon.setTypeface(font);
		textViewdelord.setTypeface(font);
		//textViewName.setText(SalesManOrderActivity.fullname);

		if(MyApplication.getInstance().isTemplate2() ||
				MyApplication.getInstance().isTemplate1()){
			llCStock.setVisibility(View.VISIBLE);
		}else {
			llCStock.setVisibility(View.GONE);
		}

		//Kumaravel 14/01/2019
		Cursor curOrderStatus;
		try{
			curOrderStatus = databaseHandler.getOrderStatus(Integer.parseInt(SalesManOrderActivity.orderId));
			if(curOrderStatus != null && curOrderStatus.getCount() > 0){
				curOrderStatus.moveToFirst();
				isOrderProcessed = curOrderStatus.getInt(0) > 0;
				curOrderStatus.close();
			}
		}catch (Exception e){
			e.printStackTrace();
			isOrderProcessed = true; // Suppose Exception catched we don't know process status so we block
		}

		Cursor  curapx;
		curapx=databaseHandler.getapproximatevalue(SalesManOrderActivity.orderId);
		if (curapx != null && curapx.getCount() > 0) {

			if (curapx.moveToFirst()) {
				double value = Double.parseDouble(curapx.getString(0));
				DecimalFormat df = new DecimalFormat(".00");
				df.setRoundingMode(RoundingMode.DOWN);
				String appr=df.format(value);
				String[] name =SalesManOrderActivity.fullname.split("\\(");
				textViewName.setText(name[0]+ "(Rs." + appr + ")");
			}
		}


		textViewDate.setText(SalesManOrderActivity.date);
		orderstatus();
		products();

		gridViewcamera.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
				Intent i = new Intent(getApplicationContext(), SM_FullGalleryView1.class);
				i.putExtra("position", position);
				startActivity(i);
				SalesmanOrderDetailsActivity.this.finish();
			}
		});
		Log.e("size", String.valueOf(SalesManOrderActivity.image.length));
		if(SalesManOrderActivity.image.length==0) {


			linearlayoutmaingallery.setVisibility(View.GONE);

		}else {

			SalesManOrderActivity.arrayListimagebitmap.clear();
			for (int i = 0; i < SalesManOrderActivity.image.length; i++) {

				BitmapFactory.Options bmOptions = new BitmapFactory.Options();
				Bitmap bitmap = BitmapFactory.decodeFile(SalesManOrderActivity.image[i], bmOptions);
				bitmap = Bitmap.createScaledBitmap(bitmap, 350, 250, true);
				SalesManOrderActivity.arrayListimagebitmap.add(bitmap);
			}

			gridViewcamera.setAdapter(new SalesmanCameraAdapter(this));

		}


		if(pushStatus.equals("Success")){
			linearlayoutmain.setVisibility(View.GONE);
			linearlayoutsecond.setVisibility(View.VISIBLE);
			buttonAddproductsecond.setVisibility(View.GONE);
			textViewAddress.setText( "Order Id: " + SalesManOrderActivity.onlineorderno + ",");
			OrderId =SalesManOrderActivity.onlineorderno;
			if(netCheck()==false){
				linearlayoutsecond.setVisibility(View.GONE);
			}
			if(netCheck()==true){
				if(isOrderProcessed){
					linearlayoutsecond.setVisibility(View.GONE);
				}
				myOrders();
				Log.e("Myorders...","Myorders....................CHECK...............******************."+Orderdtlist.size());
			}

		}else{
			OrderId =SalesManOrderActivity.orderId;
			textViewAddress.setText("Order Id:" + " T " + SalesManOrderActivity.orderId + ",");
		}
		linearLayoutBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if(pushStatus.equals("Success")){
					Constants.orderstatus="Success";
				}else{
					Constants.orderstatus="Pending";
				}
				Intent to = new Intent(SalesmanOrderDetailsActivity.this, SalesManOrderActivity.class);
				startActivity(to);
				finish();
			}
		});


		buttonUpdatesecond.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				orderdtid = new String[arraylistDealerOrderDetailList.size()];
				orderid = new String[arraylistDealerOrderDetailList.size()];
				prodid = new String[arraylistDealerOrderDetailList.size()];
				qty = new String[arraylistDealerOrderDetailList.size()];
				prodPrice= new String[arraylistDealerOrderDetailList.size()];
				date = new String[arraylistDealerOrderDetailList.size()];
				status = new String[arraylistDealerOrderDetailList.size()];
				freeQty	 = new String[arraylistDealerOrderDetailList.size()];
				duserid = new String[arraylistDealerOrderDetailList.size()];
				flag= new String[arraylistDealerOrderDetailList.size()];
				defaultuom= new String[arraylistDealerOrderDetailList.size()];
				selectedUom= new String[arraylistDealerOrderDetailList.size()];
				deliverydate= new String[arraylistDealerOrderDetailList.size()];
				deliverymode= new String[arraylistDealerOrderDetailList.size()];
				deliverytime= new String[arraylistDealerOrderDetailList.size()];
				pvalue= new String[arraylistDealerOrderDetailList.size()];
				defaultuomtotal= new String[arraylistDealerOrderDetailList.size()];
				approximateTotal=0.0;
				for (int i = 0; i < arraylistDealerOrderDetailList.size(); i++) {
					orderdtid[i] =arraylistDealerOrderDetailList.get(i).getorddtlid();
					orderid[i] =  arraylistDealerOrderDetailList.get(i).getOrderid();
					prodid[i] =arraylistDealerOrderDetailList.get(i).getprodid();
					qty[i]=arraylistDealerOrderDetailList.get(i).getqty();
					prodPrice[i]=arraylistDealerOrderDetailList.get(i).getProdprice();
					date[i] =arraylistDealerOrderDetailList.get(i).getDate();
					status[i]=arraylistDealerOrderDetailList.get(i).getordstatus();
					freeQty[i]=arraylistDealerOrderDetailList.get(i).getFreeQty();
					duserid[i]=arraylistDealerOrderDetailList.get(i).getDuserid();
					flag[i]=arraylistDealerOrderDetailList.get(i).getFlag();
					defaultuom[i]=arraylistDealerOrderDetailList.get(i).getUOM();
					selectedUom[i]=arraylistDealerOrderDetailList.get(i).getSelcetedUOM();
					deliverymode[i]=arraylistDealerOrderDetailList.get(i).getDeliverymode();
					deliverydate[i]=arraylistDealerOrderDetailList.get(i).getDeliverydate();
					deliverytime[i]=arraylistDealerOrderDetailList.get(i).getDeliverytime();
					pvalue[i]=arraylistDealerOrderDetailList.get(i).getPvalue();
					defaultuomtotal[i]=arraylistDealerOrderDetailList.get(i).getDefaultUomTotal();
					if (flag[i].equals("NF")) {
						approximateTotal =approximateTotal+Double.parseDouble( pvalue[i]);
					}
				}

				saveOrder();
			}
		});


		buttonUpdate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				orderdtid = new String[arraylistDealerOrderDetailList.size()];
				orderid = new String[arraylistDealerOrderDetailList.size()];
				prodid = new String[arraylistDealerOrderDetailList.size()];
				qty = new String[arraylistDealerOrderDetailList.size()];
				prodPrice = new String[arraylistDealerOrderDetailList.size()];
				date = new String[arraylistDealerOrderDetailList.size()];
				status = new String[arraylistDealerOrderDetailList.size()];
				freeQty	 = new String[arraylistDealerOrderDetailList.size()];
				duserid = new String[arraylistDealerOrderDetailList.size()];
				flag= new String[arraylistDealerOrderDetailList.size()];
				defaultuom= new String[arraylistDealerOrderDetailList.size()];
				selectedUom= new String[arraylistDealerOrderDetailList.size()];
				deliverydate= new String[arraylistDealerOrderDetailList.size()];
				deliverymode= new String[arraylistDealerOrderDetailList.size()];
				deliverytime= new String[arraylistDealerOrderDetailList.size()];
				pvalue= new String[arraylistDealerOrderDetailList.size()];
				defaultuomtotal= new String[arraylistDealerOrderDetailList.size()];
				approximateTotal=0.0;
				for (int i = 0; i < arraylistDealerOrderDetailList.size(); i++) {
					orderdtid[i] =arraylistDealerOrderDetailList.get(i).getorddtlid();
					orderid[i] =  arraylistDealerOrderDetailList.get(i).getOrderid();
					prodid[i] =arraylistDealerOrderDetailList.get(i).getprodid();
					qty[i]=arraylistDealerOrderDetailList.get(i).getqty();
					prodPrice[i]=arraylistDealerOrderDetailList.get(i).getProdprice();
					date[i] =arraylistDealerOrderDetailList.get(i).getDate();
					status[i]=arraylistDealerOrderDetailList.get(i).getordstatus();
					freeQty[i]=arraylistDealerOrderDetailList.get(i).getFreeQty();
					duserid[i]=arraylistDealerOrderDetailList.get(i).getDuserid();
					flag[i]=arraylistDealerOrderDetailList.get(i).getFlag();
					defaultuom[i]=arraylistDealerOrderDetailList.get(i).getUOM();
					selectedUom[i]=arraylistDealerOrderDetailList.get(i).getSelcetedUOM();
					deliverymode[i]=arraylistDealerOrderDetailList.get(i).getDeliverymode();
					deliverydate[i]=arraylistDealerOrderDetailList.get(i).getDeliverydate();
					deliverytime[i]=arraylistDealerOrderDetailList.get(i).getDeliverytime();
					pvalue[i]=arraylistDealerOrderDetailList.get(i).getPvalue();
					defaultuomtotal[i]=arraylistDealerOrderDetailList.get(i).getDefaultUomTotal();
					if (flag[i].equals("NF")) {
						approximateTotal =approximateTotal+Double.parseDouble( pvalue[i]);

					}
				}

				saveupdate();
			}
		});

		buttonAddNewProducts.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Constants.orderid=SalesManOrderActivity.orderId;
				Constants.SalesMerchant_Id =SalesManOrderActivity.morderId;
				Constants.Merchantname=SalesManOrderActivity.mname;
				Constants.checkproduct=0;
				Cursor  curapx;
				curapx=databaseHandler.getapproximatevalue(SalesManOrderActivity.orderId);
				if (curapx != null && curapx.getCount() > 0) {

					if (curapx.moveToFirst()) {
						double value = Double.parseDouble(curapx.getString(0));
						DecimalFormat df = new DecimalFormat(".00");
						df.setRoundingMode(RoundingMode.DOWN);
						String appr=df.format(value);
						Constants.approximate=Double.parseDouble(appr);
					}
				}

				Log.e("approximate", String.valueOf(Constants.approximate));
				Intent to = new Intent (SalesmanOrderDetailsActivity.this,CreateOrderActivity.class);
				startActivity(to);
				finish();
			}
		});

		textViewdelord.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				showAlertDialogToastdelorder();
			}
		});

		//Kumaravel 06/1/2019
		//need to hide update button for milk Template (update no need)
		if(Constants.TEMPLATE_NO.equals(Constants.MILK_TEMPLATE_NO)){
			linearlayoutsecond.setVisibility(View.GONE);
		}

	}
	public void cancelpush(){

		Cursor curs;
		db=getApplicationContext().openOrCreateDatabase("freshorders", 0, null);
		curs=db.rawQuery("SELECT onlineorderno from orderheader where pushstatus ='Cancelled'", null);
		Log.e("HeaderCountCanled", String.valueOf(curs.getCount()));

		if(curs!=null && curs.getCount() > 0) {
			curs.moveToFirst();
			final JSONArray resultDetail = new JSONArray();
			while (curs.isAfterLast() == false) {

				int totalColumn = curs.getColumnCount();
				JSONObject rowObject = new JSONObject();

				for (int i = 0; i < totalColumn; i++) {
					if (curs.getColumnName(i) != null) {

						try {

							if (curs.getString(i) != null) {
								Log.d("TAG_NAME", curs.getString(i));
								rowObject.put(curs.getColumnName(i), curs.getString(i));
							} else {
								rowObject.put(curs.getColumnName(i), "");
							}


						} catch (Exception e) {
							Log.d("TAG_NAME", e.getMessage());
						}
					}
				}
				resultDetail.put(rowObject);
				curs.moveToNext();
			}
			Log.e("sendordarray", resultDetail.toString());
			curs.close();

			//pushing values to mstr DB
			new AsyncTask<Void, Void, Void>() {
				ProgressDialog dialog;
				String strStatus = "";
				String strMsg = "",strOrderId ="";

				@Override
				protected void onPreExecute() {

				}

				@Override
				protected void onPostExecute(Void result) {


				}
				@Override
				protected Void doInBackground(Void... params) {
					JSONObject jsonObject = new JSONObject();
					JSONObject jsonObject1 = new JSONObject();
					try{
						Log.e("arraysize", String.valueOf(resultDetail.length()));

						for(int k=0;k<resultDetail.length();k++){
							jsonObject1 = resultDetail.getJSONObject(k);
							Log.e("values cancelled", resultDetail.getJSONObject(k).getString("onlineorderno"));
							try {
								jsonObject.put("ordstatus","Cancelled");
							} catch (JSONException e) {
								e.printStackTrace();
							}

							JsonServiceHandler = new JsonServiceHandler(Utils.strdeleteorder + resultDetail.getJSONObject(k).getString("onlineorderno") ,jsonObject.toString(),
									SalesmanOrderDetailsActivity.this);
							JsonAccountObject = JsonServiceHandler.ServiceData();


							try {

								strStatus = JsonAccountObject.getString("status");
								Log.e("return status", strStatus);
								strMsg = JsonAccountObject.getString("message");
								Log.e("return message", strMsg);
								if(strStatus.equals("true")){
									Log.e("ofliord", SalesManOrderActivity.orderId);
									db.execSQL("DELETE FROM oredrdetail where ( ordstatus = 'Cancelled' ) AND ( oflnordid = " + SalesManOrderActivity.orderId + " )");
									db.execSQL("DELETE FROM orderheader where ( pushstatus = 'Cancelled' ) AND ( oflnordid = " + SalesManOrderActivity.orderId + " ) ");
									Log.e("deletediside", "seleted");
									Cursor cursor;
									cursor = databaseHandler.getorderheader();

									Log.e("count", String.valueOf(cursor.getCount()));


								}

							} catch (JSONException e) {
								e.printStackTrace();
							} catch (Exception e) {

							}


						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
					return null;

				}
			}.execute(new Void[]{});


		}
	}

	public void DBupdate(){
		for (int i = 0; i < arraylistDealerOrderDetailList.size(); i++) {

			ContentValues contentValues = new ContentValues();
			contentValues.put(DatabaseHandler.KEY_orddtlId, orderdtid[i]);
			if(arraylistDealerOrderDetailList.get(i).getFlag().equals("F")){
				contentValues.put(DatabaseHandler.KEY_qty, freeQty[i]);
			}
			if(arraylistDealerOrderDetailList.get(i).getFlag().equals("NF")){
				contentValues.put(DatabaseHandler.KEY_qty, qty[i]);
			}
			contentValues.put(DatabaseHandler.KEY_orderStatus,  status[i]);
			contentValues.put(DatabaseHandler.KEY_order_uom,  selectedUom[i]);
			contentValues.put(DatabaseHandler.KEY_default_uom_total,defaultuomtotal[i]);
			contentValues.put(DatabaseHandler.KEY_pvalue,pvalue[i]);
			contentValues.put(DatabaseHandler.KEY_prate,prodPrice[i]);
			contentValues.put(DatabaseHandler.KEY_Odeliverydt,  deliverydate[i]);
			contentValues.put(DatabaseHandler.KEY_delivery_time,  deliverytime[i]);
			contentValues.put(DatabaseHandler.KEY_delivery_mode,  deliverymode[i]);
			String inputPattern = "dd-MMM-yyyy";
			String outputPattern = "yyyy-MM-dd";
			SimpleDateFormat inputFormat = new SimpleDateFormat(
					inputPattern);
			SimpleDateFormat outputFormat = new SimpleDateFormat(
					outputPattern);

			String str = null;

			try {
				dateStr = inputFormat.parse(date[i]);
				str = outputFormat.format(dateStr);
				contentValues.put(DatabaseHandler.KEY_Oupdateddt, str);

			} catch (Exception e) {
				e.printStackTrace();
			}

			//contentValues.put(DatabaseHandler.KEY_Oupdateddt, date[i]);
			databaseHandler.updateorderDetail(contentValues);

		}

		if(pushStatus.equals("Success")){
			DecimalFormat df = new DecimalFormat(".00");
			df.setRoundingMode(RoundingMode.DOWN);
			appr=df.format(approximateTotal);

			ContentValues contentValues1 = new ContentValues();
			contentValues1.put(DatabaseHandler.KEY_onlineOrderNo, OrderId);
			contentValues1.put(DatabaseHandler.KEY_aprxordval, appr);
			databaseHandler.updateorderHeader(contentValues1,"Success");

		}else{
			DecimalFormat df = new DecimalFormat(".00");
			df.setRoundingMode(RoundingMode.DOWN);
			appr=df.format(approximateTotal);

			ContentValues contentValues1 = new ContentValues();
			contentValues1.put(DatabaseHandler.KEY_orderno, OrderId);
			contentValues1.put(DatabaseHandler.KEY_aprxordval, approximateTotal);
			databaseHandler.updateorderHeader(contentValues1,"Pending");

		}
		String[] name =SalesManOrderActivity.fullname.split("\\(");
		textViewName.setText(name[0]+ "(Rs." + appr + ")");
		products();
	}

	public void saveupdate(){

		for (int i = 0; i < arraylistDealerOrderDetailList.size(); i++) {

			Log.e("deliverymode",deliverymode[i]);
			ContentValues contentValues = new ContentValues();
			contentValues.put(DatabaseHandler.KEY_orddtlId, orderdtid[i]);
			if(arraylistDealerOrderDetailList.get(i).getFlag().equals("F")){
				contentValues.put(DatabaseHandler.KEY_qty, freeQty[i]);
			}
			if(arraylistDealerOrderDetailList.get(i).getFlag().equals("NF")){
				contentValues.put(DatabaseHandler.KEY_qty, qty[i]);
			}
			contentValues.put(DatabaseHandler.KEY_orderStatus, status[i]);
			contentValues.put(DatabaseHandler.KEY_order_uom, selectedUom[i]);
			contentValues.put(DatabaseHandler.KEY_delivery_mode, deliverymode[i]);
			contentValues.put(DatabaseHandler.KEY_delivery_time, deliverytime[i]);
			contentValues.put(DatabaseHandler.KEY_prate, prodPrice[i]);
			contentValues.put(DatabaseHandler.KEY_Odeliverydt, deliverydate[i]);
			contentValues.put(DatabaseHandler.KEY_default_uom_total, defaultuomtotal[i]);
			contentValues.put(DatabaseHandler.KEY_pvalue,  pvalue[i]);
			String inputPattern = "dd-MMM-yyyy";
			String outputPattern = "yyyy-MM-dd";
			SimpleDateFormat inputFormat = new SimpleDateFormat(
					inputPattern);
			SimpleDateFormat outputFormat = new SimpleDateFormat(
					outputPattern);

			String str = null;

			try {
				dateStr = inputFormat.parse(date[i]);
				str = outputFormat.format(dateStr);
				contentValues.put(DatabaseHandler.KEY_Oupdateddt, str);

			} catch (Exception e) {
				e.printStackTrace();
			}
			//contentValues.put(DatabaseHandler.KEY_Oupdateddt, date[i]);
			databaseHandler.updateorderDetail(contentValues);

		}
		if(pushStatus.equals("Success")){
			DecimalFormat df = new DecimalFormat(".00");
			df.setRoundingMode(RoundingMode.DOWN);
			appr=df.format(approximateTotal);

			ContentValues contentValues1 = new ContentValues();
			contentValues1.put(DatabaseHandler.KEY_onlineOrderNo, OrderId);
			contentValues1.put(DatabaseHandler.KEY_aprxordval, appr);
			databaseHandler.updateorderHeader(contentValues1,"Success");

		}else{
			DecimalFormat df = new DecimalFormat(".00");
			df.setRoundingMode(RoundingMode.DOWN);
			appr=df.format(approximateTotal);

			ContentValues contentValues1 = new ContentValues();
			contentValues1.put(DatabaseHandler.KEY_orderno, OrderId);
			contentValues1.put(DatabaseHandler.KEY_aprxordval, appr);
			databaseHandler.updateorderHeader(contentValues1,"Pending");

		}
		String[] name =SalesManOrderActivity.fullname.split("\\(");
		textViewName.setText(name[0]+ "(Rs." + appr + ")");

		Cursor curs;
		curs = databaseHandler.getorderdetail(SalesManOrderActivity.orderId);
		if(curs!=null && curs.getCount() > 0) {
			if (curs.moveToFirst()) {

				do {

				/*	Log.e("duserid", curs.getString(2));
					Log.e("Dcompany", curs.getString(3));
					Log.e("prodid", curs.getString(4));
					Log.e("qty", curs.getString(5));
					Log.e("isfree", curs.getString(6));
					Log.e("ordstatus", curs.getString(7));
					Log.e("ordslno", curs.getString(8));
					Log.e("productcode", curs.getString(9));*/
					Log.e("delivertmode", curs.getString(26));
					Log.e("updateddt", curs.getString(11));
					Log.e("default UOM", curs.getString(22));
					Log.e("ordered UOM", curs.getString(23));
					Log.e("deliverdate", curs.getString(24));
					Log.e("delivertime", curs.getString(25));

				} while (curs.moveToNext());

			}
		}

		showAlertDialogToast("Updated Successfully");
		products();
	}

	private void myOrders() {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog = ProgressDialog.show(SalesmanOrderDetailsActivity.this, "",
						"Loading...Data..", true, false);

			}

			@Override
			protected void onPostExecute(Void result) {

				try {

					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);

					if (strStatus.equals("true")) {

						JSONArray job1 = JsonAccountObject.getJSONArray("data");
						ordtlId = new String[JsonAccountObject.getJSONArray("data").length()];
						//arraylistDealerOrderDetailList=new ArrayList<DealerOrderDetailDomain>();
						for (int i = 0; i < job1.length(); i++) {

							JSONObject job = new JSONObject();
							job=job1.getJSONObject(i);
							Log.e("inside","inside");
							String ordtlId=job.getString("orddtlid");
							Log.e("orddtlid",ordtlId);
							Orderdtlist.add(ordtlId);

							/*orderdtlId[i] =job.getString("ordid");
							Log.e("dtlid","dtlid");

							String orderid =job.getString("ordid");
							Log.e("orderid",orderid);*/
							/*DealerOrderDetailDomain dod=new DealerOrderDetailDomain();
							dod.setOrderid(job.getString("ordid"));
							dod.setorddtlid(job.getString("orddtlid"));
							dod.setprodid(job.getString("prodid"));
							dod.setFlag(job.getString("isfree"));
							Constants.serialno=job.getString("ordslno");
							dod.setProdcode(job.getJSONObject("product").getString("prodcode"));
							dod.setproductname(job.getJSONObject("product").getString("prodname"));
							String[] date1=job.getString("updateddt").split("T");


							time = date1[0];

							String inputPattern = "yyyy-MM-dd";
							String outputPattern = "dd-MMM-yyyy";
							SimpleDateFormat inputFormat = new SimpleDateFormat(
									inputPattern);
							SimpleDateFormat outputFormat = new SimpleDateFormat(
									outputPattern);

							String str = null;

							try {
								dateStr = inputFormat.parse(time);
								str = outputFormat.format(dateStr);
								dod.setDate(str);

							} catch (ParseException e) {
								e.printStackTrace();
							}

							dod.setqty(job.getString("qty"));
							dod.setordstatus(job.getString("ordstatus"));
							arraylistDealerOrderDetailList.add(dod);*/
						}
						Log.e("orderdtlId",String.valueOf(Orderdtlist.size()));
					}

					dialog.dismiss();
				} catch (JSONException e) {
					e.printStackTrace();
					dialog.dismiss();
				} catch (Exception e) {
					dialog.dismiss();
				}

			}

			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();


				JsonServiceHandler = new JsonServiceHandler(Utils.strdealerOderdetail+SalesManOrderActivity.onlineorderno, SalesmanOrderDetailsActivity.this); //+Utils.strMerSingleOderdetailAdditon+SalesManOrderActivity.companyname
				JsonAccountObject = JsonServiceHandler.ServiceDataGet();
				return null;
			}
		}.execute(new Void[]{});
	}


	public void products(){
		Cursor curs;
		curs = databaseHandler.getorderdetail(SalesManOrderActivity.orderId);
		Log.e("HeaderCount", String.valueOf(curs.getCount()));
		Log.e("PT", Constants.PaymentType);
		arraylistDealerOrderDetailList=new ArrayList<DealerOrderDetailDomain>();
		String newread = null;
		String[] resultread;
		if(curs!=null && curs.getCount() > 0) {
			if (curs.moveToFirst()) {

				do{

					DealerOrderDetailDomain dod=new DealerOrderDetailDomain();
					dod.setorddtlid(curs.getString(0));
					dod.setOrderid(curs.getString(1));
					dod.setDuserid(curs.getString(2));
					dod.setprodid(curs.getString(4));
					dod.setqty(curs.getString(5));
					dod.setFlag(curs.getString(6));
					dod.setProdprice(curs.getString(13));
					dod.setProductTax(curs.getString(14));
					dod.setPvalue(curs.getString(15));
					dod.setisread(curs.getString(16));
					dod.setUOM(curs.getString(22));
					dod.setSelcetedUOM(curs.getString(23));
					dod.setDefaultUomTotal(curs.getString(24));
					dod.setDeliverytime(curs.getString(25));
					dod.setDeliverymode(curs.getString(26));
					dod.setDeliverydate(curs.getString(12));

					dod.setCurrStock(curs.getString(27));  ///////Kumaravel Closing Stock stock

					Log.e("SelectedUOM", String.valueOf(curs.getString(23)));
					Cursor curs1;
					curs1 = databaseHandler.getProductUnits(curs.getString(4));
					Log.e("Product Unit count", String.valueOf(curs1.getCount()));
					curs1.moveToFirst();
					if(curs1!=null && curs1.getCount() > 0) {
						dod.setUnitGram(curs1.getString(0));
						dod.setUnitPerUom(curs1.getString(1));
						Log.e("Uom gram",curs1.getString(0));
						Log.e("Unit per uom",curs1.getString(1));
					}
					if(curs.getString(22).equals(curs.getString(23))){
						Log.e("equal",curs.getString(23));
						if(curs.getString(6).equals("NF")){
							dod.setSelecetdQtyUomUnit(curs1.getString(1));
						}else{
							dod.setSelecetdFreeQtyUomUnit(curs1.getString(1));
						}


					}else{
						Log.e("different",curs.getString(23));
						Cursor curs2;
						curs2 = databaseHandler.getUomUnit(curs.getString(4),curs.getString(23));
						curs2.moveToFirst();
						if(curs2!=null && curs2.getCount() > 0) {
							if(curs.getString(6).equals("NF")){
								dod.setSelecetdQtyUomUnit(curs2.getString(0));
								dod.setSelecetdFreeQtyUomUnit(dod.getUnitPerUom());
								Log.e("Qty Uom unit",curs2.getString(0));
							}else {
								dod.setSelecetdFreeQtyUomUnit(curs2.getString(0));
								dod.setSelecetdQtyUomUnit(dod.getUnitPerUom());
								Log.e("FreeQty Uom unit",curs2.getString(0));
							}

						}
					}


					String orderid=curs.getString(1);
					Constants.serialno=curs.getString(8);
					String[] read=curs.getString(16).split(",");
					for(int i=0;i<read.length;i++){

						if(read[i].equals(Constants.USER_ID)){
							List<String> list = new ArrayList<String>(Arrays.asList(read));
							list.remove(read[i]);
							read = list.toArray(new String[0]);

						}
					}
					int j=0;
					int a;
					resultread=new String[read.length];
					for(a=0;a<read.length;a++) {
						if (j == 0) {
							newread =read[a];
							j++;
						}else{
							newread=newread+","+read[a];
						}
					}
					db.execSQL("UPDATE oredrdetail SET isread = "+"'"+newread+"'"+" WHERE oflnordid =" + orderid);

					if(curs.getString(6).equals("F")){
						dod.setFreeQty(curs.getString(5));
					}else{
						dod.setFreeQty("0");
					}

					dod.setordstatus(curs.getString(7));
					dod.setProdcode(curs.getString(9));
					dod.setproductname(curs.getString(10));

					//String[] date1=job.getString("updateddt").split("T");
					//dod.setDate(date1[0]);

					time = curs.getString(11);

					String inputPattern = "yyyy-MM-dd";
					String outputPattern = "dd-MMM-yyyy";
					SimpleDateFormat inputFormat = new SimpleDateFormat(
							inputPattern);
					SimpleDateFormat outputFormat = new SimpleDateFormat(
							outputPattern);

					String str = null;

					try {
						dateStr = inputFormat.parse(time);
						str = outputFormat.format(dateStr);
						dod.setDate(str);
						// Log.e("str", str);
					} catch (Exception e) {
						e.printStackTrace();
					}
					arraylistDealerOrderDetailList.add(dod);
				} while (curs.moveToNext());
			}

            DealerOrderDetailAdapter adapter = new
                    DealerOrderDetailAdapter(SalesmanOrderDetailsActivity.this,
                    R.layout.item_dealer_order_detail, arraylistDealerOrderDetailList);
            listViewOrders.setAdapter(adapter);

		}


	}

	public void orderstatus(){
		Cursor curs;
		curs = databaseHandler.getOrderHeaderStatus(SalesManOrderActivity.orderId);
		Log.e("HeaderCount", String.valueOf(curs.getCount()));

		if(curs!=null && curs.getCount() > 0) {
			if (curs.moveToFirst()) {

				do{

					Log.e("pushstatus", curs.getString(0));
					pushStatus =curs.getString(0);

				} while (curs.moveToNext());


			}
			Log.e("outside", "outside");

		}


	}

	private void saveApproximateTotal() {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog= ProgressDialog.show(SalesmanOrderDetailsActivity.this, "",
						"Loading...", true, true);
			}

			@Override
			protected void onPostExecute(Void result) {

				try {

					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);
					DBupdate();

					showAlertDialogToast(strMsg);

				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					//Log.e("ProductActivityException", e.toString());
				}
				dialog.dismiss();
			}

			@Override
			protected Void doInBackground(Void... params) {
				JSONObject jsonObject = new JSONObject();
				try {

					jsonObject.put("aprxordval", approximateTotal);


				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				JsonServiceHandler = new JsonServiceHandler(Utils.strUpdateOrderHeader+OrderId,jsonObject.toString(), SalesmanOrderDetailsActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceData();


				return null;
			}
		}.execute(new Void[]{});
	}


	private void saveOrder() {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog = ProgressDialog.show(SalesmanOrderDetailsActivity.this, "",
						"Loading...", true, true);
				dialog.setCancelable(false);
			}

			@Override
			protected void onPostExecute(Void result) {

				try {

					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);
					//DBupdate();
					saveApproximateTotal();
					//showAlertDialogToast(strMsg);

				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					//Log.e("ProductActivityException", e.toString());
				}
				dialog.dismiss();
			}

			@Override
			protected Void doInBackground(Void... params) {
				JSONObject jsonObject = new JSONObject();
				try {
					JSONArray jsonArray = new JSONArray();
					JSONObject jsonObject1;


					for (int i = 0; i < arraylistDealerOrderDetailList.size(); i++) {
						jsonObject1 = new JSONObject();
						try {


							if(freeQty[i].equals("")){
								freeQty[i]="0";
							}
							if(qty[i].equals("")){
								qty[i]="0";
							}
							double frqty = Float.parseFloat(freeQty[i]);
							double qtty = Float.parseFloat(qty[i]);

							if(frqty>=0){
								jsonObject1.put("qty", freeQty[i]);
								jsonObject1.put("isfree", flag[i]);
								Log.e("isfree",flag[i]);
							}

							if(qtty>0){
								jsonObject1.put("qty", qty[i]);
								jsonObject1.put("isfree",flag[i]);
								Log.e("isfree", flag[i]);
							}

							if(pushStatus.equals("Success")){
								Log.e("Orderdtlist_test","...............size "+ Orderdtlist.get(i));
								jsonObject1.put("orddtlid", Orderdtlist.get(i));
							}

							jsonObject1.put("ordid", SalesManOrderActivity.onlineorderno);
							jsonObject1.put("prodid",prodid[i]);
							jsonObject1.put("deliverydt",date[i]);
							jsonObject1.put("ordstatus", status[i]);
							jsonObject1.put("duserid", duserid[i]);
							jsonObject1.put("ord_uom", selectedUom[i]);
							jsonObject1.put("prate",prodPrice[i]);
							jsonObject1.put("cqty", defaultuomtotal[i]);
							Log.e("cqty", defaultuomtotal[i]);
							jsonObject1.put("pvalue", pvalue[i]);
							jsonObject1.put("deliverydt",deliverydate[i]);
							jsonObject1.put("deliverytype", deliverymode[i]);
							jsonObject1.put("deliverytime", deliverytime[i]);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						jsonArray.put(jsonObject1);
					}

					jsonObject.put("orderdtls", jsonArray);


				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				JsonServiceHandler = new JsonServiceHandler(Utils.strsavedealerOderdetail,jsonObject.toString(), SalesmanOrderDetailsActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceData();


				return null;
			}
		}.execute(new Void[]{});
	}

	protected void toastDisplay(String msg) {
		Toast toast = Toast.makeText(SalesmanOrderDetailsActivity.this, msg,
				Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

	}
	public boolean netCheck() {
		// for network connection
		try {
			ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetwork = connectionManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			Object result = null;
			if (mWifi.isConnected() || mNetwork.isConnected()) {
				return true;
			}

			else if (result == null) {
				/*showAlertDialog(SalesmanOrderDetailsActivity.this,
						"No Internet Connection",
						"Please Check Your internet connection.", false);*/
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	public void showAlertDialog(Context context, String title, String message,
								Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
					}
				});
		alertDialog.show();
	}
	public void showAlertDialogToast1( String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						Intent io = new Intent(SalesmanOrderDetailsActivity.this, SalesManOrderActivity.class);
						approximateTotal=0.0;
						startActivity(io);
						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();
		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

	}
	public void showAlertDialogToast( String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						if (popup == 1) {
							Intent io = new Intent(SalesmanOrderDetailsActivity.this, SalesmanOrderDetailsActivity.class);
							startActivity(io);
						}
						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();
		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));


	}
	public void showAlertDialogToastdelorder() {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage("Do you want to cancel the order?");
		builder1.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						Log.e("ofliord", SalesManOrderActivity.orderId);
						db.execSQL("UPDATE oredrdetail SET ordstatus = 'Cancelled'  WHERE oflnordid = " + SalesManOrderActivity.orderId);
						db.execSQL("UPDATE orderheader SET pushstatus = 'Cancelled'  WHERE oflnordid = " + SalesManOrderActivity.orderId);
						showAlertDialogToast1("Order successfully deleted");
						popup = 1;
						Cursor curs1, curs2;
						db = getApplicationContext().openOrCreateDatabase("freshorders", 0, null);
						curs1 = db.rawQuery("SELECT * from orderheader where pushstatus ='Cancelled'", null);
						curs2 = db.rawQuery("SELECT * from oredrdetail where ordstatus ='Cancelled'", null);
						Log.e("count1", String.valueOf(curs1.getCount()));
						Log.e("count2", String.valueOf(curs2.getCount()));
						Log.e("netcheck", String.valueOf(netCheck()));

						if (netCheck() == true) {
							Log.e("deleted11", "deleted22");
							cancelpush();

						} else {
							Log.e("deleted", "deleted");
							Log.e("Pending", pushStatus);
							Log.e("ofliord", SalesManOrderActivity.orderId);
							if (pushStatus.equals("Pending")) {
								Log.e("Pending", "Pending");
								db.execSQL("DELETE FROM oredrdetail where ( ordstatus = 'Cancelled' ) AND ( oflnordid = " + SalesManOrderActivity.orderId + " )");
								db.execSQL("DELETE FROM orderheader where ( pushstatus = 'Cancelled' ) AND ( oflnordid = " + SalesManOrderActivity.orderId + " ) ");
								Log.e("deletediside", "seleted");
								Cursor curs3, curs4;

								curs3 = db.rawQuery("SELECT * from orderheader where pushstatus ='Cancelled'", null);
								curs4 = db.rawQuery("SELECT * from oredrdetail where ordstatus ='Cancelled'", null);
								Log.e("count3", String.valueOf(curs3.getCount()));
								Log.e("count4", String.valueOf(curs4.getCount()));

							}

						}
						dialog.cancel();
					}
				});
		builder1.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();



	}
	public void onBackPressed() {
		exitAlret();
	}

	private void exitAlret() {
		AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
		localBuilder.setCancelable(false);
		localBuilder.setMessage("Do you want to Exit?");
		localBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {

						finish();

					}
				});
		localBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {
						paramDialogInterface.cancel();
					}
				});
		localBuilder.create().show();
	}

}
