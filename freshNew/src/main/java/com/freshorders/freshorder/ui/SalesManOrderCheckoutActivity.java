package com.freshorders.freshorder.ui;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Gallery;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.MerchantOrderCheckoutAdapter;
import com.freshorders.freshorder.adapter.MerchantOrderCheckoutAdapterTemplate1;
import com.freshorders.freshorder.adapter.MerchantOrderCheckoutAdapterTemplate10;
import com.freshorders.freshorder.adapter.MerchantOrderCheckoutAdapterTemplate2;
import com.freshorders.freshorder.adapter.MerchantOrderCheckoutAdapterTemplate3;
import com.freshorders.freshorder.adapter.MerchantOrderCheckoutAdapterTemplate4;
import com.freshorders.freshorder.adapter.MerchantOrderCheckoutAdapterTemplate8;
import com.freshorders.freshorder.adapter.MerchantOrderCheckoutAdapterTemplate9;
import com.freshorders.freshorder.adapter.SalesmanCameraAdapter;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.MerchantOrderDetailDomain;
import com.freshorders.freshorder.domain.MerchantOrderDomainSelected;
import com.freshorders.freshorder.popup.Template2ShowPaymentPopup;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.GPSTracker;
import com.freshorders.freshorder.utils.GPSTrackerActivity;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.SM_UploadToServerCheckout;
import com.freshorders.freshorder.utils.SalesmanLocation;
import com.freshorders.freshorder.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import static com.freshorders.freshorder.utils.Constants.CURRENT_MILLIS;
import static com.freshorders.freshorder.utils.Constants.TEMPLATE5;
import static com.freshorders.freshorder.utils.Constants.TEMPLATE_NO;


public class SalesManOrderCheckoutActivity extends Activity implements LocationListener {

    private String merchant_name_extra = "";//Kumaravel 11/03/2019
    private JSONArray stockOrderArray = new JSONArray();
    // Kumaravel For Template2
    static String selecteddeliverymode = "", monthLength="", dayLength="";
    public static int mYear, mMonth, mDay, mHour, mMinute,mSeconds;

    final static int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 4;
    SalesManOrderCheckoutActivity CameraActivity = null;
    public static String Path = "", path1 = "";
    public static String imageId, imagepath = "";
    TextView menuIcon;
    JSONObject JsonAccountObject = null;
    JSONArray JsonAccountArray = null;
    JsonServiceHandler JsonServiceHandler;
    ListView listViewOrders;
    LinearLayout linearLayoutBack;
    DatabaseHandler databaseHandler;
    public SQLiteDatabase db;
    public static String[] orderdtid, userid, prodid, qty, date, status, freeQty, paymentType, companyname, prodCode, prodName,
            price, ptax, pvalue, fread, qread, batchno, expdate, mfgdate, outstock, uom, ordruom, orderfreeuom,unitgram,qtyUomUnit,freeqtyUomUnit,deliverydate,deliverytime,deliverymode, currstock;
    Button buttonAddPlaceOrder;
    public static String payment_type, orderdate, Orderno, flag, productStatus, mode, paymenttype, pushstauts,
            OfflineOrderNo, discntval, discntprcnt, apprxordval, ordertotal, pymnttoatal, selected_dealer, lasttrackdate, todayDate, endtime,
            beatrowid;
    public Double pval, fval;
    public Double updtpval, updtfval;
    Double apprvalue = 0.0;
    public int approximate = 0;
    public static double geolat, geolog;
    public static BroadcastReceiver locationtrack;
    int year, month, day, hr, mins, sec, deliverydt;

    Gallery gridViewcamera;
    TextView textviewcameraicon;
    public static String CapturedImageDetails;
    Uri imageUri = null;
    Bitmap mBitmap1 = null;
    MerchantOrderDomainSelected md;
    public String imagepath1 = "", imagepath2 = "", imagepath3 = "", imagepath4 = "", imagepath5 = "", imagepath6 = "",
            imagepath7 = "", imagepath8 = "", imagepath9 = "", imagepath10 = "", imagepath11 = "", imagepath12 = "",
            merchantRowid = "";

    GPSTracker gps;
    protected Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.salesman_order_checkout_screen);

        Intent intent = getIntent();
        if (intent != null && intent.hasExtra("MERCHANT_NAME")) {
            merchant_name_extra = intent.getStringExtra("MERCHANT_NAME");
            Log.e("MERCHANT_NAME","...........MERCHANT_NAME"+merchant_name_extra);
        }
        if(intent != null){
            Location location = intent.getParcelableExtra(Constants.EXTRA_LOCATION);
            if(location != null) {
                geolat = location.getLatitude();
                geolog = location.getLongitude();
            }
        }


        netCheck();
        CameraActivity = this;
        buttonAddPlaceOrder = (Button) findViewById(R.id.buttonAddPlaceOrder);
        menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
        listViewOrders = (ListView) findViewById(R.id.listViewOrders);
        linearLayoutBack = (LinearLayout) findViewById(R.id.linearLayoutBack);

        locationtrack = new SalesmanLocation();
        gridViewcamera = (Gallery) findViewById(R.id.gridViewcamera);
        gridViewcamera.setSelection(1);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) gridViewcamera.getLayoutParams();
        mlp.setMargins(0, 0, 0, 0);
        //////////////gps = new GPSTracker(SalesManOrderCheckoutActivity.this);

        textviewcameraicon = (TextView) findViewById(R.id.textviewcameraicon);
        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();
        db = getApplicationContext().openOrCreateDatabase("freshorders", 0, null);
        databaseHandler = new DatabaseHandler(getApplicationContext());
        Typeface font = Typeface.createFromAsset(getAssets(),
                "fontawesome-webfont.ttf");
        menuIcon.setTypeface(font);
        textviewcameraicon.setTypeface(font);
        startService(new Intent(this, MyService.class));

        /*
        try {
            getcurrentlocation();
        } catch (IOException e) {
            e.printStackTrace();
        }  *///////////////

        textviewcameraicon.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    //requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constants.WRITE_EXTERNAL_STORAGE);
                    if(isWriteStorageAllowed()){
                        callMethod();
                    }else {
                        requestWriteStoragePermission();
                    }

                } else {
                    callMethod();
                }

            }
        });

        if (Constants.imagesetcheckout == 1) {
            //  Log.e("sizeofimage", String.valueOf(SalesManOrderActivity.arraylistimagepath.size()));
            if (SalesManOrderActivity.arraylistimagepath.size() != 0) {
                setadp();
            }
        }
        menuIcon.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Intent io = new Intent(SalesManOrderCheckoutActivity.this, CreateOrderActivity.class);
                /////////Constants.checkproduct = 1;
                Constants.checkproduct = 0; ////// need to reload
                startActivity(io);
                finish();

            }
        });

        buttonAddPlaceOrder.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Log.e("OrerCount","........................"+ CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size());
                if (!String.valueOf(CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()).equals("0")) {

                    paymentProcess();////////////
                } else {
                    showAlertDialogToast("Please Select the Products");

                }
            }
        });

        if(!MyApplication.getInstance().isTemplate8ForTemplate2()) {
            /// template5
            if (MyApplication.getInstance().isTemplate5()) {
                MerchantOrderCheckoutAdapter adapter = new MerchantOrderCheckoutAdapter(
                        SalesManOrderCheckoutActivity.this,
                        R.layout.item_merchant_order_details,
                        CreateOrderActivity.arraylistMerchantOrderDetailListSelected);
                listViewOrders.setAdapter(adapter);
            }
            /// template1
            if (MyApplication.getInstance().isTemplate1()) {

                MerchantOrderCheckoutAdapterTemplate1 adapter = new MerchantOrderCheckoutAdapterTemplate1(
                        SalesManOrderCheckoutActivity.this,
                        R.layout.item_merchant_order_details_template1,
                        CreateOrderActivity.arraylistMerchantOrderDetailListSelected);
                listViewOrders.setAdapter(adapter);

            }

            /// template2
            if (MyApplication.getInstance().isTemplate2()) {
                // Common for both tem1 and tem2
                {
                    MerchantOrderCheckoutAdapterTemplate2 adapter = new MerchantOrderCheckoutAdapterTemplate2(
                            SalesManOrderCheckoutActivity.this,
                            R.layout.item_merchant_order_details_template2,
                            CreateOrderActivity.arraylistMerchantOrderDetailListSelected,
                            CreateOrderActivity.itemListUOM);
                    listViewOrders.setAdapter(adapter);
                }

            }

            /// template3
            if (MyApplication.getInstance().isTemplate3()) {
                // Common for both tem1 and tem2
                MerchantOrderCheckoutAdapterTemplate3 adapter = new MerchantOrderCheckoutAdapterTemplate3(
                        SalesManOrderCheckoutActivity.this,
                        R.layout.item_merchant_order_details_template3,
                        CreateOrderActivity.arraylistMerchantOrderDetailListSelected,
                        CreateOrderActivity.itemListUOM);
                listViewOrders.setAdapter(adapter);

            }
            /// template4
            if (MyApplication.getInstance().isTemplate4()) {
                // Common for both tem1 and tem2
                MerchantOrderCheckoutAdapterTemplate4 adapter = new MerchantOrderCheckoutAdapterTemplate4(
                        SalesManOrderCheckoutActivity.this,
                        R.layout.item_merchant_order_details_template4,
                        CreateOrderActivity.arraylistMerchantOrderDetailListSelected,
                        CreateOrderActivity.itemListUOM);
                listViewOrders.setAdapter(adapter);

            }
            /// template9
            if (MyApplication.getInstance().isTemplate9()) {
                // Common for both tem1 and tem2
                MerchantOrderCheckoutAdapterTemplate9 adapter = new MerchantOrderCheckoutAdapterTemplate9(
                        SalesManOrderCheckoutActivity.this,
                        R.layout.item_merchant_order_details_template9,
                        CreateOrderActivity.arraylistMerchantOrderDetailListSelected,
                        CreateOrderActivity.itemListUOM);
                listViewOrders.setAdapter(adapter);
            }
            /// template10
            if (MyApplication.getInstance().isTemplate10()) {
                // Common for both tem1 and tem2
                MerchantOrderCheckoutAdapterTemplate10 adapter = new MerchantOrderCheckoutAdapterTemplate10(
                        SalesManOrderCheckoutActivity.this,
                        R.layout.item_merchant_order_details_template10,
                        CreateOrderActivity.arraylistMerchantOrderDetailListSelected,
                        CreateOrderActivity.itemListUOM);
                listViewOrders.setAdapter(adapter);
            }
        }else {
            textviewcameraicon.setVisibility(View.INVISIBLE);/////////////
            MerchantOrderCheckoutAdapterTemplate8 adapter = new MerchantOrderCheckoutAdapterTemplate8(
                    SalesManOrderCheckoutActivity.this,
                    R.layout.item_merchant_order_details_template8,
                    CreateOrderActivity.arraylistMerchantOrderDetailListSelected,
                    CreateOrderActivity.itemListUOM);
            listViewOrders.setAdapter(adapter);

        }


    }
    public void getcurrentlocation() throws IOException {

        if (gps.canGetLocation() == true) {

               /* createordergeolat = gps.getLatitude();
            		createordergeolog = gps.getLongitude();

                    		Log.e("lat", String.valueOf(createordergeolat));
            			Log.e("log", String.valueOf(createordergeolog));
*/

            Intent intent = new Intent(this, GPSTrackerActivity.class);
            startActivityForResult(intent,1);

        }else {
            showSettingsAlert();

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Constants.WRITE_EXTERNAL_STORAGE:

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    callMethod();


                } else {
                    //finish();
                    Toast.makeText(this,"Oops you denied the permission Your Missing Some Functionality",Toast.LENGTH_LONG).show();
                }
                return;
        }
    }

    private boolean isWriteStorageAllowed() {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        //If permission is granted returning true otherwise false
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void requestWriteStoragePermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.WRITE_EXTERNAL_STORAGE)){
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
            //android.support.v7.app.AlertDialog.Builder alertBuilder = new android.support.v7.app.AlertDialog.Builder(this);
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
            alertBuilder.setCancelable(true);
            alertBuilder.setTitle("Permission necessary");
            alertBuilder.setMessage("This App Need Your Storage Permission To Take Order Images");
            alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                //@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions((Activity)SalesManOrderCheckoutActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constants.WRITE_EXTERNAL_STORAGE);
                }
            });
            AlertDialog alert = alertBuilder.create();
            alert.show();
        }else {
            //And finally ask for the permission
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constants.WRITE_EXTERNAL_STORAGE);
        }
    }

   /* public void onRequestPermissionsResult(int requestCode, @NonNull String[] permission, @NonNull int[] grantResults) {

        Log.e("requestcode", String.valueOf(requestCode));
        Log.e("size", String.valueOf(grantResults.length));

    if(requestCode == Constants.WRITE_EXTERNAL_STORAGE){

            if(permission.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                Log.e("grantResults", String.valueOf(grantResults[0] ));
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    callMethod();
                } else {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constants.WRITE_EXTERNAL_STORAGE);
                }
            }
        }
    }*/

 /*  public void onResume()
    {
        super.onResume();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(locationtrack, filter);

    }

    public void onPause()
    {
        super.onPause();
        unregisterReceiver(locationtrack);
    }*/



    private void callMethod() {

        String fileName = "Camera_Example.jpg";

        ContentValues values = new ContentValues();

        values.put(MediaStore.Images.Media.TITLE, fileName);

        values.put(MediaStore.Images.Media.DESCRIPTION, "Image capture by camera");

        imageUri = getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        Log.e("values", String.valueOf(values));

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);

        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);

        startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
        Log.e("2", "2");

    }

    public void showSettingsAlert(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(SalesManOrderCheckoutActivity.this);

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.delete);

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {

                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS );
                startActivityForResult(intent, 2);

            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == 1){
            Bundle extras = data.getExtras();
            geolog = extras.getDouble("Longitude");
            geolat = extras.getDouble("Latitude");
            //Log.e("geolog", String.valueOf(geolog));

        }
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {

            if (resultCode == RESULT_OK) {
                Log.e("3", "3");
                imageId = convertImageUriToFile(imageUri, CameraActivity);

                Log.e("what???", imageId);

                new LoadImagesFromSDCard().execute("" + imageId);

            } else if (resultCode == RESULT_CANCELED) {


            } else {


            }
        }

        if(requestCode  == 2){
            Intent intent = new Intent(this, GPSTrackerActivity.class);
            startActivityForResult(intent,1);
        }


    }

    public static String convertImageUriToFile(Uri imageUri, Activity activity) {

        Cursor cursor = null;
        int imageID = 0;

        try {

            /*********** Which columns values want to get *******/
            String[] proj = {
                    MediaStore.Images.Media.DATA,
                    MediaStore.Images.Media._ID,
                    MediaStore.Images.Thumbnails._ID,
                    MediaStore.Images.ImageColumns.ORIENTATION
            };
            Log.e("proj", String.valueOf(proj));

            cursor = activity.managedQuery(

                    imageUri,         //  Get data for specific image URI
                    proj,             //  Which columns to return
                    null,             //  WHERE clause; which rows to return (all rows)
                    null,             //  WHERE clause selection arguments (none)
                    null              //  Order-by clause (ascending by name)

            );

            //  Get Query Data

            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media._ID);
            int columnIndexThumb = cursor.getColumnIndexOrThrow(MediaStore.Images.Thumbnails._ID);
            int file_ColumnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

            //int orientation_ColumnIndex = cursor.
            //    getColumnIndexOrThrow(MediaStore.Images.ImageColumns.ORIENTATION);

            int size = cursor.getCount();
            Log.e("size", String.valueOf(size));

            /*******  If size is 0, there are no images on the SD Card. *****/

            if (size == 0) {

                Log.e("4", "4");

            } else {
                Log.e("5", "5");
                int thumbID = 0;
                if (cursor.moveToFirst()) {

                    /**************** Captured image details ************/

                    /*****  Used to show image on view in LoadImagesFromSDCard class ******/
                    imageID = cursor.getInt(columnIndex);

                    thumbID = cursor.getInt(columnIndexThumb);

                    Path = cursor.getString(file_ColumnIndex);

                    //String orientation =  cursor.getString(orientation_ColumnIndex);

                    CapturedImageDetails = " CapturedImageDetails : \n\n"
                            + " ImageID :" + imageID + "\n"
                            + " ThumbID :" + thumbID + "\n"
                            + " Path :" + Path + "\n";


                    Log.e("CapturedImageDetails", CapturedImageDetails);
                    // Show Captured Image detail on activity
                    SalesManOrderActivity.arraylistimagepath.add(Path);
                    Log.e("pathsize", String.valueOf(SalesManOrderActivity.arraylistimagepath.size()));
                }
            }
        } finally {
            if (cursor != null) {

            }
        }


        return "" + imageID;
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.e("lat", String.valueOf(location.getLatitude()));
        Log.e("long", String.valueOf(location.getLongitude()));
        geolat=location.getLatitude();
        geolog=location.getLongitude();
        Toast.makeText(this,"lat :"+location.getLatitude()+"/n"+"long"+location.getLongitude(),Toast.LENGTH_LONG);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public class LoadImagesFromSDCard extends AsyncTask<String, Void, Void> {

        private ProgressDialog Dialog = new ProgressDialog(SalesManOrderCheckoutActivity.this);


        protected void onPreExecute() {
            /****** NOTE: You can call UI Element here. *****/

            // Progress Dialog
            Dialog.setMessage(" Loading image from Sdcard..");
            Dialog.show();
        }


        // Call after onPreExecute method
        protected Void doInBackground(String... urls) {

            Bitmap bitmap = null;
            Bitmap newBitmap = null;
            Uri uri = null;

            try {

                uri = Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "" + urls[0]);


                bitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(uri));

                if (bitmap != null) {
                    Log.e("6", "6");
                    newBitmap = Bitmap.createScaledBitmap(bitmap, 350, 250, true);

                    bitmap.recycle();

                    mBitmap1 = newBitmap;

                    Log.e("pathload", String.valueOf(mBitmap1));
                    path1 = Path;

                }
            } catch (IOException e) {

                cancel(true);
            }

            return null;
        }


        protected void onPostExecute(Void unused) {


            SalesManOrderActivity.arrayListimagebitmap.add(mBitmap1);
            Log.e("pathload", String.valueOf(mBitmap1));
            Log.e("path", String.valueOf(path1));

            setadp();
            Log.e("pathsize", String.valueOf(SalesManOrderActivity.arrayListimagebitmap.size()));

            Dialog.dismiss();
            Log.e("showImg", "showImg");


        }

    }

    public void setadp() {
        gridViewcamera.setAdapter(new SalesmanCameraAdapter(this));
    }

    public static void removeid(String id) {

        for (MerchantOrderDetailDomain pd : CreateOrderActivity.arraylistMerchantOrderDetailList) {
            if (pd.getprodid().equals(id)) {
                pd.setqty("");
                pd.setFreeQty("");

                Constants.checkproduct = 1;               // CreateOrderActivity.arraylistMerchantOrderDetailList.add(pd);
            }
        }

    }

    private void saveOrder() {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "", strOrderId = "";

            @Override
            protected void onPreExecute() {
                dialog = ProgressDialog.show(SalesManOrderCheckoutActivity.this, "",
                        "Loading...", true, true);
            }

            @Override
            protected void onPostExecute(Void result) {

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);
                    if (strStatus.equals("true")) {
                        strOrderId = JsonAccountObject.getString("data");
                        Log.e("strOrderId", strOrderId);

                        CreateOrderActivity.arraylistMerchantOrderDetailListSelected.clear();
                        Intent to = new Intent(SalesManOrderCheckoutActivity.this, MerchantOrderConfirmation.class);
                        to.putExtra("strOrderId", strOrderId);
                        startActivity(to);
                        finish();


                    } else {
                        showAlertDialogToast(strMsg);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    //Log.e("ProductActivityException", e.toString());
                }
                dialog.dismiss();
            }

            @Override
            protected Void doInBackground(Void... params) {
                JSONObject jsonObject = new JSONObject();
                try {
                    JSONArray jsonArray = new JSONArray();
                    JSONObject jsonObject1;

                    for (int i = 0; i < CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size(); i++) {
                        jsonObject1 = new JSONObject();
                        try {

                            jsonObject1.put("prodid", prodid[i]);
                            jsonObject1.put("qty", qty[i]);
                            jsonObject1.put("duserid", userid[i]);
                            jsonObject1.put("fqty", freeQty[i]);
                            jsonObject1.put("index", i);


                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        jsonArray.put(jsonObject1);

                    }


                    jsonObject.put("suserid", Constants.USER_ID);
                    jsonObject.put("muserid", Constants.SalesMerchant_Id);
                    jsonObject.put("distributorname", Constants.distributorname);


                    if (Constants.PaymentType.equals("Cash")) {
                        payment_type = "C";
                        jsonObject.put("iscash", "C");
                    } else if (Constants.PaymentType.equals("Credit")) {
                        jsonObject.put("iscash", "CR");
                        payment_type = "CR";
                    }

                    Log.i("Order Details",jsonObject.toString());

                    jsonObject.put("orderdtls", jsonArray);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                JsonServiceHandler = new JsonServiceHandler(Utils.strsavemerchantOderdetail, jsonObject.toString(), SalesManOrderCheckoutActivity.this);
                JsonAccountObject = JsonServiceHandler.ServiceData();
                return null;
            }
        }.execute(new Void[]{});
    }

    public void saveOfflineHeader() {
        Log.e("PT", Constants.PaymentType);
        String iscash;
        if (Constants.PaymentType.equals("Credit")) {
            iscash = "CR";
        } else {
            iscash = "C";
        }
        //Kumaravel Prevent from empty merchant's order
        if(Constants.Merchantname.isEmpty()){
            Constants.Merchantname = merchant_name_extra;
        }

        String inputFormat1 = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss").format(new Date());
        Log.e("inputFormat", inputFormat1);

        String uuid = UUID.randomUUID().toString(); /////////Kumaravel Added for more specific id
        Log.e("UUID","................"+uuid);
        /////String inputPattern = "ddMMyyhhmmss";  ////////Kumaravel
        String inputPattern = "yyMMddhhmmssMs";

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        String dt = inputFormat.format(new Date());
        endtime = new SimpleDateFormat(
                "HH:mm:ss").format(new java.util.Date());
        Log.e("endtime", endtime);
        String offlineOrderNo = Constants.USER_ID + dt + CURRENT_MILLIS;
        Log.e("OfflineOrderNO",".................." + offlineOrderNo);
        Log.e("OfflineOrderNO",".................." + Constants.USER_ID + dt);
        Log.e("Constants.startTime", Constants.startTime);
        Log.e("Constants.endtime", inputFormat1);
        if(!MyApplication.getInstance().isTemplate8ForTemplate2()) {
            databaseHandler.addorderheader(Constants.USER_ID, Constants.SalesMerchant_Id, Constants.Merchantname, iscash, "Pending", inputFormat1, "0",
                    offlineOrderNo, "null", geolat, geolog, Constants.startTime, endtime, "");
        }else {
            databaseHandler.addStockOrderHeader(Constants.USER_ID, Constants.SalesMerchant_Id, Constants.Merchantname, iscash, "Pending", inputFormat1, "0",
                    offlineOrderNo , "null", geolat, geolog, Constants.startTime, endtime, "");
        }

        //Kumaravel MOR Table
        if(Constants.SalesMerchant_Id.equalsIgnoreCase("New User")) {
            ContentValues cv = new ContentValues();
            cv.put(DatabaseHandler.KEY_MERCHANT_ORDER_MERCHANT_ID, Constants.EMPTY);
            cv.put(DatabaseHandler.KEY_MERCHANT_ORDER_ORDER_OFFLINE_ID, offlineOrderNo);
            cv.put(DatabaseHandler.KEY_MERCHANT_ORDER_MERCHANT_OFFLINE_ID, Constants.OFFLINE_MERCHANT_ID);
            databaseHandler.loadMOR(cv);
        }


        Cursor curs;
        if(!MyApplication.getInstance().isTemplate8ForTemplate2()) {
            curs = databaseHandler.getorderheader();
        }else {
            curs = databaseHandler.getStockOrderHeader();
        }
        Log.e("orderHeaderCount", String.valueOf(curs.getCount()));

        if (curs != null && curs.getCount() > 0) {

            if (curs.moveToLast()) {
                Orderno = curs.getString(0);
                paymenttype = curs.getString(3);
                pushstauts = curs.getString(4);
                String date = curs.getString(5);
                Log.e("onlineorderno", curs.getString(6));
                Log.e("orderno", Orderno);
                Log.e("Hdate", inputFormat1);
                Log.e("HPT", paymenttype);
                //Log.e("HPS", pushstauts);

                saveofflineDetail(Orderno);
            }
        }

    }

    public void saveofflineDetail(String orderno) {
        Log.e("orde number", String.valueOf(orderno));
        productStatus = "Pending";


        for (int i = 0; i < CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size(); i++) {

            Double frqty = 0d;
            Double qtty = 0d;
            if(!MyApplication.getInstance().isTemplate8ForTemplate2()) {
                if(!freeQty[i].isEmpty()) {
                    frqty = Double.parseDouble(freeQty[i]);
                }
                if(!qty[i].isEmpty()) {
                    qtty = Double.parseDouble(qty[i]);
                }
            }

            String inputFormat = new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss").format(new Date());
            fread = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
            qread = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];

            if(!MyApplication.getInstance().isTemplate8ForTemplate2()) {

                if (frqty > 0) {
                    flag = "F";

                    fread[i] = Constants.USER_ID + "," + Constants.SalesMerchant_Id + "," + userid[i];

                    Double totalunit = (frqty) * (Double.parseDouble(freeqtyUomUnit[i]));
                    pval = (totalunit * (Double.parseDouble(price[i]) + (Double.parseDouble(price[i]) * (Double.parseDouble(ptax[i]) / 100))));

                    Log.e("pval", String.valueOf(pval));
                    Double defaultuomTotal = ((frqty) * (Double.parseDouble(unitgram[i])) * (Double.parseDouble(freeqtyUomUnit[i]))) / 1000;
                    // apprvalue=apprvalue+pval;
                    Log.e("defaultuomTotal", String.valueOf(defaultuomTotal));
                    imagepath = "0";

                    for (int j = 0; j < SalesManOrderActivity.arraylistimagepath.size(); j++) {
                        if (j == 0) {
                            imagepath = SalesManOrderActivity.arraylistimagepath.get(j);

                        } else {
                            imagepath = imagepath + "," + SalesManOrderActivity.arraylistimagepath.get(j);
                        }


                    }
                    String deldate = "";
                    if (!deliverydate[i].equals("")) {
                        SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
                        Date date = null;
                        try {
                            date = fmt.parse(deliverydate[i]);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        SimpleDateFormat fmtOut = new SimpleDateFormat("yyyy-MM-dd");
                        deldate = fmtOut.format(date);
                        Log.e("deliverydate", deldate);
                    }
                    //


                    databaseHandler.addorderdetail(Orderno, userid[i], companyname[i], prodid[i], freeQty[i], flag, productStatus, String.valueOf(i), prodCode[i], prodName[i], deldate, price[i], ptax[i], pval,
                            fread[i], "", batchno[i], mfgdate[i], expdate[i], imagepath, uom[i], orderfreeuom[i], defaultuomTotal, deliverytime[i], deliverymode[i], currstock[i]);


                }
                if (qtty >= 0) { /// important Kumaravel 06-06-2019 equal symbol added for allowing zero for closing stock purpose
                    flag = "NF";
                    qread[i] = Constants.USER_ID + "," + Constants.SalesMerchant_Id + "," + userid[i];

                    Double defaultuomTotal = 0d;///this is split for template10

                    if(MyApplication.getInstance().isTemplate10()){///newly added for template10 17-08-2019
                        fval = Double.parseDouble(price[i]);
                        apprvalue = apprvalue + fval;
                        defaultuomTotal = qtty;  /////////////////////newly added for template10 28-08-2019
                    }else {
                        Double totalunit = (qtty) * (Double.parseDouble(qtyUomUnit[i]));
                        fval = totalunit * (Double.parseDouble(price[i]) + (Double.parseDouble(price[i]) * (Double.parseDouble(ptax[i]) / 100)));
                        defaultuomTotal = ((qtty) * (Double.parseDouble(unitgram[i])) * (Double.parseDouble(qtyUomUnit[i]))) / 1000;
                        apprvalue = apprvalue + fval;
                    }
                    imagepath = "0";

                    for (int j = 0; j < SalesManOrderActivity.arraylistimagepath.size(); j++) {
                        if (j == 0) {
                            imagepath = SalesManOrderActivity.arraylistimagepath.get(j);

                        } else {
                            imagepath = imagepath + "," + SalesManOrderActivity.arraylistimagepath.get(j);

                        }

                    }
                    String deldate = "";
                    if (!deliverydate[i].equals("")) {
                        SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
                        Date date = null;
                        try {
                            date = fmt.parse(deliverydate[i]);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        SimpleDateFormat fmtOut = new SimpleDateFormat("yyyy-MM-dd");
                        deldate = fmtOut.format(date);
                        Log.e("deliverydate", deldate);

                    }

                    databaseHandler.addorderdetail(Orderno, userid[i], companyname[i], prodid[i], qty[i], flag, productStatus, String.valueOf(i), prodCode[i], prodName[i],
                            deldate, price[i], ptax[i], fval, qread[i], "", batchno[i], mfgdate[i], expdate[i], imagepath, uom[i], ordruom[i], defaultuomTotal, deliverytime[i], deliverymode[i], currstock[i]);

                }
            } else {
                imagepath = "0";
                flag = "NF";
                String deldate = "";
                Double defaultuomTotal = 0d, fval = 0d;
                qty[i] = Constants.CLOSING_STOCK_QUANTITY;/////////////////////////////////////
                productStatus = "Pending";
                try {
                    qread[i] = Constants.USER_ID + "," + Constants.SalesMerchant_Id + "," + userid[i];
                    for (int j = 0; j < SalesManOrderActivity.arraylistimagepath.size(); j++) {
                        if (j == 0) {
                            imagepath = SalesManOrderActivity.arraylistimagepath.get(j);

                        } else {
                            imagepath = imagepath + "," + SalesManOrderActivity.arraylistimagepath.get(j);

                        }

                    }

                    databaseHandler.addStockOrderDetail(Orderno, userid[i], companyname[i], prodid[i], qty[i],
                            flag, productStatus, String.valueOf(i), prodCode[i], prodName[i],
                            deldate, price[i], ptax[i], fval, qread[i], "", batchno[i],
                            mfgdate[i], expdate[i], imagepath, uom[i], ordruom[i], defaultuomTotal,
                            deliverytime[i], deliverymode[i], currstock[i]);  /// outstock set to closing stock Kumaravel
                    /*// This for closing stock menu by Kumaravel
                    JSONObject rowObject = new JSONObject();

                    rowObject.put("oflnordid", String.valueOf(i + 1));
                    rowObject.put("duserid", userid[i]);
                    rowObject.put("Dcompany", companyname[i]);
                    rowObject.put("prodid", prodid[i]);
                    rowObject.put("qty", "0");
                    rowObject.put("flag", "NF");
                    rowObject.put("productStatus", "Pending");
                    rowObject.put("ordslno", String.valueOf(i));
                    rowObject.put("productcode", prodCode[i]);
                    rowObject.put("productname", prodName[i]);
                    rowObject.put("deliverydt", Constants.EMPTY);
                    rowObject.put("prate", price[i]);
                    rowObject.put("ptax", ptax[i]);
                    rowObject.put("pvalue", String.valueOf(0));
                    rowObject.put("isread", qread[i]);
                    rowObject.put("stock", CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getCurrStock());
                    rowObject.put("batchno", batchno[i]);
                    rowObject.put("manufdt", mfgdate[i]);
                    rowObject.put("expirydt", expdate[i]);
                    rowObject.put("ordimage", imagepath);
                    rowObject.put("default_uom", uom[i]);
                    rowObject.put("ord_uom", ordruom[i]);
                    rowObject.put("deliverytime", deliverytime[i]);
                    rowObject.put("deliverytype", deliverymode[i]);
                    rowObject.put("currstock", currstock[i]);
                    rowObject.put("ordimage", i);
                    rowObject.put("ordimage", i);  */
                }catch (Exception e){
                    e.printStackTrace();
                    showAlertDialogToast("some think wrong" + e.getMessage());
                }


            }


        }

        CreateOrderActivity.arraylistMerchantOrderDetailListSelected.clear();
        Cursor curs;
        if(!MyApplication.getInstance().isTemplate8ForTemplate2()) {
            db.execSQL("UPDATE orderheader SET aprxordval =" + apprvalue + "  WHERE oflnordid =" + orderno);
            curs = databaseHandler.getorderheader();
        }else {
            approximate = 1;
            db.execSQL("UPDATE stockOrderHeader SET aprxordval =" + approximate + "  WHERE oflnordid =" + orderno);
            curs = databaseHandler.getStockOrderHeader();
        }





        if (curs != null && curs.getCount() > 0) {

            if (curs.moveToLast()) {
                Orderno = curs.getString(0);
                approximate = curs.getInt(9);


            }
        }


        if (netCheck()) {
            if(!MyApplication.getInstance().isTemplate8ForTemplate2()) {
                convertjson();
            }else {
                convertJSONForStock();
            }

        } else {
            if(!MyApplication.getInstance().isTemplate8ForTemplate2()) {
                Constants.orderstatus = "Pending";
                Intent to = new Intent(SalesManOrderCheckoutActivity.this, MerchantOrderConfirmation.class);
                to.putExtra("strOrderId", "T" + Orderno);
                startActivity(to);
                finish();
            }else {
                Toast.makeText(getApplicationContext(), "Order Placed Successfully", Toast.LENGTH_SHORT).show();
                Constants.orderstatus = "Pending";
                Intent to = new Intent(SalesManOrderCheckoutActivity.this, ClosingStockDashBoardActivity.class);
                to.putExtra("strOrderId", "T" + Orderno);
                startActivity(to);
                finish();
            }
        }

    }

    private void convertJSONForStock(){
        final JSONArray resultSet = new JSONArray();


        Cursor curs;
        curs = databaseHandler.getStockOrderHeader();
        Log.e("HeaderCount", String.valueOf(curs.getCount()));
        //curs.moveToFirst();

        curs.moveToLast(); /// here i changed to move only current order

        while (curs.isAfterLast() == false) {

            int totalColumn = curs.getColumnCount();
            JSONObject rowObject = new JSONObject();

            for (int i = 0; i < totalColumn; i++) {
                if (curs.getColumnName(i) != null) {

                    try {

                        if (curs.getString(i) != null) {
                            if (!curs.getString(i).equals("New User")) {
                                Log.d("TAG_NAME", curs.getString(i));
                                rowObject.put(curs.getColumnName(i), curs.getString(i));
                            }

                        } else {
                            if (!curs.getColumnName(i).equals("muserid")) {
                                rowObject.put(curs.getColumnName(i), "");
                            }

                        }
                    } catch (Exception e) {
                        Log.d("TAG_NAME", e.getMessage());
                    }
                }

            }
            // Added by SK for pushing distributorname
            try {
                //Kumaravel 13-07-2019
                if(Constants.distributorname == null || Constants.distributorname.isEmpty()){
                    Constants.distributorname = new PrefManager(SalesManOrderCheckoutActivity.this).getStringDataByKey(PrefManager.KEY_DISTRIBUTOR_NAME);
                }
                rowObject.put("distributorname",Constants.distributorname);
            } catch (Exception e) {
                Log.d("Exception in Dist. name", e.getMessage());
            }
            // End of SK Changes

            try {
                db = getApplicationContext().openOrCreateDatabase("freshorders", 0, null);

                Cursor curs1;
                curs1 = db.rawQuery("SELECT * from dealertable", null);
                Log.e("dEALER", String.valueOf(curs1.getCount()));
                if (curs1.getCount() > 0) {
                    curs1.moveToFirst();
                    selected_dealer = curs1.getString(curs1
                            .getColumnIndex(DatabaseHandler.KEY_Duserid));
                }

                Cursor cursor2;
                Constants.Merchantname = rowObject.getString("mname");

                cursor2 = db.rawQuery("SELECT * from merchanttable where companyname =" + "'" + Constants.Merchantname + "'", null);
                Log.e("HeaderCountCanled", String.valueOf(cursor2.getCount()));

                cursor2.moveToFirst();
                while (cursor2.isAfterLast() == false) {

                    int Columncount = cursor2.getColumnCount();
                    Log.e("merchantColumn", String.valueOf(cursor2.getColumnCount()));
                    JSONObject merchantdetail = new JSONObject();
                    for (int j = 0; j < Columncount; j++) {
                        if (cursor2.getColumnName(j) != null) {
                            try {
                                if (cursor2.getColumnName(j).equals("cflag")) {
                                    if (cursor2.getString(j).equals("R")) {
                                        merchantdetail.put(cursor2.getColumnName(j), "G");
                                    } else if (cursor2.getString(j).equals("G")) {
                                        merchantdetail.put(cursor2.getColumnName(j), "");
                                    } else {
                                        merchantdetail.put(cursor2.getColumnName(j), "");
                                    }

                                } else {
                                    if (cursor2.getString(j) != null) {
                                        if (!cursor2.getString(j).equals("New User")) {

                                            merchantdetail.put(cursor2.getColumnName(j), cursor2.getString(j));

                                        }
                                    } else {
                                        if (!cursor2.getColumnName(j).equals("userid")) {
                                            merchantdetail.put(cursor2.getColumnName(j), "");
                                        }

                                    }
                                }


                            } catch (Exception e) {
                                Log.d("Merchant_Exception", e.getMessage());
                            }
                        }

                    }
                    merchantdetail.put("usertype", "M");
                    merchantdetail.put("duserid", selected_dealer);
                    rowObject.put("merchant", merchantdetail);
                    cursor2.moveToNext();
                }

                cursor2.close();

                Log.e("Detailarray", rowObject.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }


            Log.e("jsonarray", rowObject.toString());

            JSONArray resultDetail = new JSONArray();
            try {

                Cursor cursor;
                cursor = databaseHandler.getStockOrderDetail(rowObject.getString("oflnordid"));
                Log.e("DetailCount", String.valueOf(cursor.getCount()));
                cursor.moveToFirst();
                while (cursor.isAfterLast() == false) {

                    int Columncount = cursor.getColumnCount();
                    Log.e("columnCount", String.valueOf(cursor.getColumnCount()));
                    JSONObject detailObject = new JSONObject();

                    for (int i = 0; i < Columncount; i++) //17
                    {
                        if (cursor.getColumnName(i) != null) {

                            try {

                                if (cursor.getString(i) != null) {
                                    Log.d("TAG_NAME", cursor.getString(i));
                                    detailObject.put(cursor.getColumnName(i), cursor.getString(i));
                                } else {
                                    detailObject.put(cursor.getColumnName(i), "");
                                }
                            } catch (Exception e) {
                                Log.d("TAG_NAME", e.getMessage());
                            }
                        }

                    }

                    resultDetail.put(detailObject);
                    cursor.moveToNext();
                }

                cursor.close();

                Log.e("Detailarray", resultDetail.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                rowObject.put("orderdtls", resultDetail);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            resultSet.put(rowObject);
            curs.moveToNext();
        }
        curs.close();


        Log.e("SaveArray", resultSet.toString());


        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "", strOrderId = "";

            @Override
            protected void onPreExecute() {
                dialog = ProgressDialog.show(SalesManOrderCheckoutActivity.this, "",
                        "Loading...", true, true);
            }

            @Override
            protected void onPostExecute(Void result) {

                try {
                    //if(!MyApplication.getInstance().isTemplate1()) {
                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);
                    //}
                    if (strStatus.equals("true")) {
                        strOrderId = JsonAccountObject.getJSONObject("data").getString("ordid");
                        Log.e("strOrderId", strOrderId);
                        String muserid = JsonAccountObject.getJSONObject("data").getString("muserid");
                        Log.e("muserid", muserid);


                        String updatedFlag = resultSet.getJSONObject(0).getJSONObject("merchant").getString("cflag");

                        CreateOrderActivity.arraylistMerchantOrderDetailListSelected.clear();
                        CreateOrderActivity.arraylistMerchantOrderDetailList.clear();


                        ContentValues contentValues = new ContentValues();
                        contentValues.put(DatabaseHandler.KEY_orderno, OfflineOrderNo);
                        contentValues.put(DatabaseHandler.KEY_pushStatus, "Success");
                        contentValues.put(DatabaseHandler.KEY_onlineOrderNo, strOrderId);
                        databaseHandler.updateStockOrderHeader(contentValues);

                        Cursor curs;
                        curs = databaseHandler.getStockOrderHeaderStatus(OfflineOrderNo);
                        Log.e("HeaderStatusCount", String.valueOf(curs.getCount()));
                        Constants.orderstatus = "Success";
                        if (curs != null && curs.getCount() > 0) {

                            if (curs.moveToFirst()) {
                                Orderno = curs.getString(0);
                                Log.e("Pushstatus", curs.getString(0));
                            }
                        }

                        for (int m = 0; m < SalesManOrderActivity.arraylistimagepath.size(); m++) {
                            Log.e("inside", "inside");

                            if (m == 0) {
                                imagepath1 = SalesManOrderActivity.arraylistimagepath.get(0).toString();
                            } else if (m == 1) {
                                imagepath2 = SalesManOrderActivity.arraylistimagepath.get(1).toString();
                            } else if (m == 2) {
                                imagepath3 = SalesManOrderActivity.arraylistimagepath.get(2).toString();
                            } else if (m == 3) {
                                imagepath4 = SalesManOrderActivity.arraylistimagepath.get(3).toString();
                            } else if (m == 4) {
                                imagepath5 = SalesManOrderActivity.arraylistimagepath.get(4).toString();
                            } else if (m == 5) {
                                imagepath6 = SalesManOrderActivity.arraylistimagepath.get(5).toString();
                            } else if (m == 6) {
                                imagepath7 = SalesManOrderActivity.arraylistimagepath.get(6).toString();
                            } else if (m == 7) {
                                imagepath8 = SalesManOrderActivity.arraylistimagepath.get(7).toString();
                            } else if (m == 8) {
                                imagepath9 = SalesManOrderActivity.arraylistimagepath.get(8).toString();
                            } else if (m == 9) {
                                imagepath10 = SalesManOrderActivity.arraylistimagepath.get(9).toString();
                            } else if (m == 10) {
                                imagepath11 = SalesManOrderActivity.arraylistimagepath.get(10).toString();
                            } else if (m == 11) {
                                imagepath12 = SalesManOrderActivity.arraylistimagepath.get(11).toString();
                            }

                            Log.e("path", String.valueOf(SalesManOrderActivity.arraylistimagepath.get(m)));

                        }

                        if (SalesManOrderActivity.arraylistimagepath.size() != 0) {


                            SM_UploadToServerCheckout ufs = new SM_UploadToServerCheckout(
                                    SalesManOrderCheckoutActivity.this, imagepath1, imagepath2, imagepath3, imagepath4,
                                    imagepath5, imagepath6, imagepath7, imagepath8,
                                    imagepath9, imagepath10, imagepath11, imagepath12, strOrderId);

                            ufs.execute();

                        } else {
                            Constants.orderstatus = "Success";
                            Toast.makeText(getApplicationContext(), "Order Placed successfully", Toast.LENGTH_SHORT).show();
                            Intent to = new Intent(SalesManOrderCheckoutActivity.this, ClosingStockDashBoardActivity.class);
                            to.putExtra("strOrderId", strOrderId);
                            startActivity(to);
                            finish();
                        }


                    } else {
                        showAlertDialogToast(strMsg);

                        ContentValues contentValues = new ContentValues();
                        contentValues.put(DatabaseHandler.KEY_orderno, OfflineOrderNo);
                        contentValues.put(DatabaseHandler.KEY_pushStatus, "Failed");
                        databaseHandler.updateStockOrderHeader(contentValues);

                        Cursor curs;
                        curs = databaseHandler.getStockOrderHeaderStatus(OfflineOrderNo);
                        Log.e("HeaderStatusCount", String.valueOf(curs.getCount()));
                        Constants.orderstatus = "Failed";
                        if (curs != null && curs.getCount() > 0) {

                            if (curs.moveToFirst()) {
                                Orderno = curs.getString(0);
                                Log.e("Pushstatus", curs.getString(0));
                            }
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {

                }

                dialog.dismiss();
            }

            @Override
            protected Void doInBackground(Void... params) {
                JSONObject jsonObject = new JSONObject();
                try {

                    jsonObject = resultSet.getJSONObject(0);
                    OfflineOrderNo = jsonObject.getString("oflnordid");
                    System.out.println("Before Change: "+jsonObject);
                    JSONObject jsonObjMerchant=jsonObject.getJSONObject("merchant");
                    System.out.println("Before  Change jsonObjMerchant: "+jsonObjMerchant);
                    jsonObjMerchant.put("createdby",Constants.USER_ID);
                    jsonObjMerchant.put("customdata", "");
                    System.out.println("After Change jsonObjMerchant: "+jsonObjMerchant);
                    jsonObject.put("merchant",jsonObjMerchant);

                    System.out.println("After Change: "+jsonObject);
                    JsonServiceHandler = new JsonServiceHandler(Utils.strsavemerchantOderdetail, jsonObject.toString(), SalesManOrderCheckoutActivity.this);
                    JsonAccountObject = JsonServiceHandler.ServiceData();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;

            }
        }.execute(new Void[]{});


    }


    public void convertjson() {
        final JSONArray resultSet = new JSONArray();


        Cursor curs;
        curs = databaseHandler.getorderheader();
        Log.e("HeaderCount", String.valueOf(curs.getCount()));
        curs.moveToFirst();
        while (curs.isAfterLast() == false) {

            int totalColumn = curs.getColumnCount();
            JSONObject rowObject = new JSONObject();

            for (int i = 0; i < totalColumn; i++) {
                if (curs.getColumnName(i) != null) {

                    try {

                        if (curs.getString(i) != null) {
                            if (!curs.getString(i).equals("New User")) {
                                Log.d("TAG_NAME", curs.getString(i));
                                rowObject.put(curs.getColumnName(i), curs.getString(i));
                            }

                        } else {
                            if (!curs.getColumnName(i).equals("muserid")) {
                                rowObject.put(curs.getColumnName(i), "");
                            }

                        }
                    } catch (Exception e) {
                        Log.d("TAG_NAME", e.getMessage());
                    }
                }

            }
            // Added by SK for pushing distributorname
            try {
                //Kumaravel 13-07-2019
                if(Constants.distributorname == null || Constants.distributorname.isEmpty()){
                    Constants.distributorname = new PrefManager(SalesManOrderCheckoutActivity.this).getStringDataByKey(PrefManager.KEY_DISTRIBUTOR_NAME);
                }
                rowObject.put("distributorname",Constants.distributorname);
            } catch (Exception e) {
                Log.d("Exception in Dist. name", e.getMessage());
            }
            // End of SK Changes

            try {
                db = getApplicationContext().openOrCreateDatabase("freshorders", 0, null);

                Cursor curs1;
                curs1 = db.rawQuery("SELECT * from dealertable", null);
                Log.e("dEALER", String.valueOf(curs1.getCount()));
                if (curs1.getCount() > 0) {
                    curs1.moveToFirst();
                    selected_dealer = curs1.getString(curs1
                            .getColumnIndex(DatabaseHandler.KEY_Duserid));
                }

                Cursor cursor2;
                Constants.Merchantname = rowObject.getString("mname");

                cursor2 = db.rawQuery("SELECT * from merchanttable where companyname =" + "'" + Constants.Merchantname + "'", null);
                Log.e("HeaderCountCanled", String.valueOf(cursor2.getCount()));

                cursor2.moveToFirst();
                while (cursor2.isAfterLast() == false) {

                    int Columncount = cursor2.getColumnCount();
                    Log.e("merchantColumn", String.valueOf(cursor2.getColumnCount()));
                    JSONObject merchantdetail = new JSONObject();
                    for (int j = 0; j < Columncount; j++) {
                        if (cursor2.getColumnName(j) != null) {
                            try {
                                if (cursor2.getColumnName(j).equals("cflag")) {
                                    if (cursor2.getString(j).equals("R")) {
                                        merchantdetail.put(cursor2.getColumnName(j), "G");
                                    } else if (cursor2.getString(j).equals("G")) {
                                        merchantdetail.put(cursor2.getColumnName(j), "");
                                    } else {
                                        merchantdetail.put(cursor2.getColumnName(j), "");
                                    }

                                } else {
                                    if (cursor2.getString(j) != null) {
                                        if (!cursor2.getString(j).equals("New User")) {

                                            merchantdetail.put(cursor2.getColumnName(j), cursor2.getString(j));

                                        }
                                    } else {
                                        if (!cursor2.getColumnName(j).equals("userid")) {
                                            merchantdetail.put(cursor2.getColumnName(j), "");
                                        }

                                    }
                                }


                            } catch (Exception e) {
                                Log.d("Merchant_Exception", e.getMessage());
                            }
                        }

                    }
                    merchantdetail.put("usertype", "M");
                    merchantdetail.put("duserid", selected_dealer);
                    rowObject.put("merchant", merchantdetail);
                    cursor2.moveToNext();
                }

                cursor2.close();

                Log.e("Detailarray", rowObject.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }


            Log.e("jsonarray", rowObject.toString());

            JSONArray resultDetail = new JSONArray();
            try {

                Cursor cursor;
                cursor = databaseHandler.getorderdetail(rowObject.getString("oflnordid"));
                Log.e("DetailCount", String.valueOf(cursor.getCount()));
                cursor.moveToFirst();
                while (cursor.isAfterLast() == false) {

                    int Columncount = cursor.getColumnCount();
                    Log.e("columnCount", String.valueOf(cursor.getColumnCount()));
                    JSONObject detailObject = new JSONObject();

                    for (int i = 0; i < Columncount; i++) //17
                    {
                        if (cursor.getColumnName(i) != null) {

                            try {

                                if (cursor.getString(i) != null) {
                                    Log.d("TAG_NAME", cursor.getString(i));
                                    // important Kumaravel 06-06-2019  //////////////////////
                                    if(cursor.getColumnName(i).equals("qty")){
                                        Log.e("Qty","......................................");
                                        if(MyApplication.getInstance().isTemplate1() || MyApplication.getInstance().isTemplate2()){
                                            Log.e("Qty","................cursor.getString(i).........::::"+cursor.getString(i));
                                            if(cursor.getString(i).equals("0")){
                                                detailObject.put(cursor.getColumnName(i), Constants.PRODUCT_QUANTITY); //////////
                                            }else {
                                                detailObject.put(cursor.getColumnName(i), cursor.getString(i));
                                            }
                                        }else {
                                            detailObject.put(cursor.getColumnName(i), cursor.getString(i));
                                        }
                                    }else{
                                        detailObject.put(cursor.getColumnName(i), cursor.getString(i));
                                    }
                                    /////// Change End//////////////////////////////////////
                                } else {
                                    detailObject.put(cursor.getColumnName(i), "");
                                }
                            } catch (Exception e) {
                                Log.d("TAG_NAME", e.getMessage());
                            }
                        }

                    }

                    resultDetail.put(detailObject);
                    cursor.moveToNext();
                }

                cursor.close();

                Log.e("Detailarray", resultDetail.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                rowObject.put("orderdtls", resultDetail);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            resultSet.put(rowObject);
            curs.moveToNext();
        }
        curs.close();


        Log.e("SaveArray", resultSet.toString());


        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "", strOrderId = "";

            @Override
            protected void onPreExecute() {
                dialog = ProgressDialog.show(SalesManOrderCheckoutActivity.this, "",
                        "Loading...", true, true);
            }

            @Override
            protected void onPostExecute(Void result) {

                try {
                    //if(!MyApplication.getInstance().isTemplate1()) {
                        strStatus = JsonAccountObject.getString("status");
                        Log.e("return status", strStatus);
                        strMsg = JsonAccountObject.getString("message");
                        Log.e("return message", strMsg);
                    //}
                    if (strStatus.equals("true")) {
                        strOrderId = JsonAccountObject.getJSONObject("data").getString("ordid");
                        Log.e("strOrderId", strOrderId);
                        String muserid = JsonAccountObject.getJSONObject("data").getString("muserid");
                        Log.e("muserid", muserid);


                        String updatedFlag = resultSet.getJSONObject(0).getJSONObject("merchant").getString("cflag");

                        Log.e("updatedFlag", updatedFlag);
                        ContentValues contentValues1 = new ContentValues();
                        contentValues1.put(DatabaseHandler.KEY_Mcompanyname, resultSet.getJSONObject(0).getString("mname"));
                        contentValues1.put(DatabaseHandler.KEY_Muserid, muserid);
                        contentValues1.put(DatabaseHandler.KEY_MFlag, updatedFlag);
                        databaseHandler.updateMerchant(contentValues1);
                        Log.e("updateMerchant", String.valueOf(contentValues1));
                        Cursor curs1;
                        curs1 = databaseHandler.getmerchantDetails(muserid);
                        Log.e("Outlet/Merchant Count", String.valueOf(curs1.getCount()));

                        if (curs1 != null && curs1.getCount() > 0) {

                            if (curs1.moveToFirst()) {
                                Log.e("rowid ", curs1.getString(0));
                                merchantRowid = curs1.getString(0);

                            }
                        }

                        databaseHandler.updateBeatMercahant(muserid, merchantRowid);
                        Cursor curs2;
                        curs2 = databaseHandler.getbeat();
                        Log.e("countbeat ", String.valueOf(curs2.getCount()));

                        beatupdate();
                        CreateOrderActivity.arraylistMerchantOrderDetailListSelected.clear();
                        CreateOrderActivity.arraylistMerchantOrderDetailList.clear();


                        ContentValues contentValues = new ContentValues();
                        contentValues.put(DatabaseHandler.KEY_orderno, OfflineOrderNo);
                        contentValues.put(DatabaseHandler.KEY_pushStatus, "Success");
                        contentValues.put(DatabaseHandler.KEY_onlineOrderNo, strOrderId);
                        databaseHandler.updateOrderHeader(contentValues);

                        Cursor curs;
                        curs = databaseHandler.getOrderHeaderStatus(OfflineOrderNo);
                        Log.e("HeaderStatusCount", String.valueOf(curs.getCount()));

                        if (curs != null && curs.getCount() > 0) {

                            if (curs.moveToFirst()) {
                                Orderno = curs.getString(0);
                                Log.e("Pushstatus", curs.getString(0));
                            }
                        }

                        for (int m = 0; m < SalesManOrderActivity.arraylistimagepath.size(); m++) {
                            Log.e("inside", "inside");

                            if (m == 0) {
                                imagepath1 = SalesManOrderActivity.arraylistimagepath.get(0).toString();
                            } else if (m == 1) {
                                imagepath2 = SalesManOrderActivity.arraylistimagepath.get(1).toString();
                            } else if (m == 2) {
                                imagepath3 = SalesManOrderActivity.arraylistimagepath.get(2).toString();
                            } else if (m == 3) {
                                imagepath4 = SalesManOrderActivity.arraylistimagepath.get(3).toString();
                            } else if (m == 4) {
                                imagepath5 = SalesManOrderActivity.arraylistimagepath.get(4).toString();
                            } else if (m == 5) {
                                imagepath6 = SalesManOrderActivity.arraylistimagepath.get(5).toString();
                            } else if (m == 6) {
                                imagepath7 = SalesManOrderActivity.arraylistimagepath.get(6).toString();
                            } else if (m == 7) {
                                imagepath8 = SalesManOrderActivity.arraylistimagepath.get(7).toString();
                            } else if (m == 8) {
                                imagepath9 = SalesManOrderActivity.arraylistimagepath.get(8).toString();
                            } else if (m == 9) {
                                imagepath10 = SalesManOrderActivity.arraylistimagepath.get(9).toString();
                            } else if (m == 10) {
                                imagepath11 = SalesManOrderActivity.arraylistimagepath.get(10).toString();
                            } else if (m == 11) {
                                imagepath12 = SalesManOrderActivity.arraylistimagepath.get(11).toString();
                            }

                            Log.e("path", String.valueOf(SalesManOrderActivity.arraylistimagepath.get(m)));

                        }

                        if (SalesManOrderActivity.arraylistimagepath.size() != 0) {


                            SM_UploadToServerCheckout ufs = new SM_UploadToServerCheckout(
                                    SalesManOrderCheckoutActivity.this, imagepath1, imagepath2, imagepath3, imagepath4,
                                    imagepath5, imagepath6, imagepath7, imagepath8,
                                    imagepath9, imagepath10, imagepath11, imagepath12, strOrderId);

                            ufs.execute();

                        } else {
                            Intent to = new Intent(SalesManOrderCheckoutActivity.this, MerchantOrderConfirmation.class);
                            to.putExtra("strOrderId", strOrderId);
                            startActivity(to);
                            finish();
                        }


                    } else {
                        showAlertDialogToast(strMsg);

                        ContentValues contentValues = new ContentValues();
                        contentValues.put(DatabaseHandler.KEY_orderno, OfflineOrderNo);
                        contentValues.put(DatabaseHandler.KEY_pushStatus, "Failed");
                        databaseHandler.updateOrderHeader(contentValues);

                        Cursor curs;
                        curs = databaseHandler.getOrderHeaderStatus(OfflineOrderNo);
                        Log.e("HeaderStatusCount", String.valueOf(curs.getCount()));

                        if (curs != null && curs.getCount() > 0) {

                            if (curs.moveToFirst()) {
                                Orderno = curs.getString(0);
                                Log.e("Pushstatus", curs.getString(0));
                            }
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {

                }

                dialog.dismiss();
            }

            @Override
            protected Void doInBackground(Void... params) {
                JSONObject jsonObject = new JSONObject();
                try {

                    jsonObject = resultSet.getJSONObject(0);
                    OfflineOrderNo = jsonObject.getString("oflnordid");
                    System.out.println("Before Change: "+jsonObject);
                    JSONObject jsonObjMerchant=jsonObject.getJSONObject("merchant");
                    System.out.println("Before  Change jsonObjMerchant: "+jsonObjMerchant);
                    jsonObjMerchant.put("createdby",Constants.USER_ID);
                    jsonObjMerchant.put("customdata", "");
                    System.out.println("After Change jsonObjMerchant: "+jsonObjMerchant);
                    jsonObject.put("merchant",jsonObjMerchant);

                    System.out.println("After Change: "+jsonObject);
                    JsonServiceHandler = new JsonServiceHandler(Utils.strsavemerchantOderdetail, jsonObject.toString(), SalesManOrderCheckoutActivity.this);
                    JsonAccountObject = JsonServiceHandler.ServiceData();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;

            }
        }.execute(new Void[]{});

    }

    public void cancelpush() {

        Cursor curs;
        db = getApplicationContext().openOrCreateDatabase("freshorders", 0, null);
        curs = db.rawQuery("SELECT onlineorderno from orderheader where pushstatus ='Cancelled'", null);
        Log.e("HeaderCountCanled", String.valueOf(curs.getCount()));

        if (curs != null && curs.getCount() > 0) {
            curs.moveToFirst();
            final JSONArray resultDetail = new JSONArray();
            while (curs.isAfterLast() == false) {

                int totalColumn = curs.getColumnCount();
                JSONObject rowObject = new JSONObject();

                for (int i = 0; i < totalColumn; i++) {
                    if (curs.getColumnName(i) != null) {

                        try {

                            if (curs.getString(i) != null) {
                                Log.d("TAG_NAME", curs.getString(i));
                                rowObject.put(curs.getColumnName(i), curs.getString(i));
                            } else {
                                rowObject.put(curs.getColumnName(i), "");
                            }
                            resultDetail.put(rowObject);

                        } catch (Exception e) {
                            Log.d("TAG_NAME", e.getMessage());
                        }
                    }
                }

                curs.moveToNext();
            }
            Log.e("sendordarray", resultDetail.toString());
            curs.close();

            //pushing values to mstr DB
       /* new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "",strOrderId ="";

            @Override
            protected void onPreExecute() {

            }

            @Override
            protected void onPostExecute(Void result) {
               *//* JSONObject jsonObject = new JSONObject();


                try {
                    jsonObject.put("ordstatus","Cancelled");
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                JsonServiceHandler = new JsonServiceHandler(
                        Utils.strdeleteorder + MerchantOrderActivity.orderId ,jsonObject.toString(),
                        MerchantSingleDetailActivity.this);
                JsonAccountObject = JsonServiceHandler.ServiceData();
                return null;*//*
            }
            @Override
            protected Void doInBackground(Void... params) {
                JSONObject jsonObject = new JSONObject();
                try{

                    for(int k=0;k<resultDetail.length();k++){
                        jsonObject = resultDetail.getJSONObject(k);

                        JsonServiceHandler = new JsonServiceHandler(Utils.strsavemerchantOderdetail,jsonObject.toString(),context);
                        JsonAccountObject = JsonServiceHandler.ServiceData();


                        try {

                            strStatus = JsonAccountObject.getString("status");
                            Log.e("return status", strStatus);
                            strMsg = JsonAccountObject.getString("message");
                            Log.e("return message", strMsg);
                            if(strStatus.equals("true")){
                                strOrderId = JsonAccountObject.getString("data");
                                Log.e("strOrderId", strOrderId);
                                Cursor cur;



                                db.execSQL("UPDATE orderheader SET pushstatus = 'Success',onlineorderno ="+strOrderId+" WHERE oflnordid ="+OfflineOrderNo);
                                // db.execSQL("UPDATE oredrdetail SET oflnordid ="+strOrderId+" WHERE oflnordid ="+OfflineOrderNo);
                                // db.execSQL("UPDATE orderheader SET pushstatus = 'Success' WHERE oflnordid ="+OfflineOrderNo);


                                cur=db.rawQuery("SELECT pushstatus from orderheader where pushstatus ='Success'", null);
                                Log.e("HeaderStatusCount", String.valueOf(cur.getCount()));

                                if(cur!=null && cur.getCount() > 0) {

                                    if (cur.moveToFirst()) {

                                        Log.e("Pushstatus",cur.getString(0) );
                                    }
                                }



                            }else{

                                Cursor curs;

                                db.execSQL("UPDATE orderheader SET pushstatus = 'Failed',onlineorderno ="+strOrderId+" WHERE oflnordid ="+OfflineOrderNo);

                                curs=db.rawQuery("SELECT pushstatus from orderheader where pushstatus ='Failed'", null);
                                if(curs!=null && curs.getCount() > 0) {

                                    if (curs.moveToFirst()) {
                                        Orderno = curs.getString(0);
                                        Log.e("Pushstatus",curs.getString(0) );
                                    }
                                }
                            }



                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (Exception e) {

                        }


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;

            }
        }.execute(new Void[]{});*/


        }
    }


    public void getDetailJson(String ordno) {
        JSONArray resultDetail = new JSONArray();


        Cursor cursor;
        cursor = databaseHandler.getorderdetail(ordno);
        Log.e("DetailCount", String.valueOf(cursor.getCount()));
        cursor.moveToFirst();
        while (cursor.isAfterLast() == false) {

            int Columncount = cursor.getColumnCount();
            JSONObject detailObject = new JSONObject();

            for (int i = 0; i < Columncount; i++) {
                if (cursor.getColumnName(i) != null) {

                    try {

                        if (cursor.getString(i) != null) {
                            Log.d("TAG_NAME", cursor.getString(i));
                            detailObject.put(cursor.getColumnName(i), cursor.getString(i));
                        } else {
                            detailObject.put(cursor.getColumnName(i), "");
                        }
                    } catch (Exception e) {
                        Log.d("TAG_NAME", e.getMessage());
                    }
                }

            }

            resultDetail.put(detailObject);
            cursor.moveToNext();
        }

        cursor.close();

        Log.e("Detailarray", resultDetail.toString());


    }

    ;

    public void orderdata() {
        Cursor curs;
        curs = databaseHandler.getorder();
        Log.e("HeaderCount", String.valueOf(curs.getCount()));
        Log.e("PT", Constants.PaymentType);
        if (curs != null && curs.getCount() > 0) {
            if (curs.moveToFirst()) {

                do {
                    Log.e("order_no", curs.getString(0));
                    Log.e("suerid", curs.getString(1));
                    Log.e("muerid", curs.getString(2));
                    Log.e("PaymentType", curs.getString(3));

                    Log.e("pushstatus", curs.getString(4));
                    Log.e("orderdate", curs.getString(5));
                    Log.e("rowid", curs.getString(6));
                    Log.e("orderno", curs.getString(7));
                    Log.e("duserid", curs.getString(8));
                    Log.e("prodid", curs.getString(9));
                    Log.e("qty", curs.getString(10));
                    Log.e("flag", curs.getString(11));
                    Log.e("orderstatus", curs.getString(12));
                    Log.e("serialno", curs.getString(13));


                } while (curs.moveToNext());


            }
            Log.e("outside", "outside");
        }


    }

    public void updateProducts() {
        orderfreeuom = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        ordruom = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        uom = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        outstock = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        batchno = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        expdate = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        mfgdate = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        prodCode = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        prodName = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        companyname = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        userid = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        prodid = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        qty = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        freeQty = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        price = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        ptax = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        unitgram= new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        qtyUomUnit= new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        freeqtyUomUnit= new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        fread = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        qread = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        deliverydate = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        deliverymode = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        deliverytime = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        currstock = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];

        for (int i = 0; i < CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size(); i++) {

            userid[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getUserid();
            prodid[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getprodid();
            qty[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getqty();
            freeQty[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getFreeQty();
            companyname[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getCompanyname();
            prodCode[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getprodcode();
            prodName[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getprodname();
            price[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getprice();
            ptax[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getptax();
            batchno[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getBatchno();
            expdate[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getExpdate();
            mfgdate[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getMgfDate();
            outstock[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getOutstock();
            uom[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getUOM();
            ordruom[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getorderuom();
            orderfreeuom[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getorderfreeuom();
            unitgram[i]=CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getUnitGram();
            qtyUomUnit[i]=CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getSelectedQtyUomUnit();
            freeqtyUomUnit[i]=CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getSelectedFreeQtyUomUnit();
            deliverymode[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getDeliverymode();
            deliverytime[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getDeliverytime();
            deliverydate[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getDeliverydate();
            currstock[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getCurrStock();
            Log.e("qtyUomUnit", qtyUomUnit[i]);

            /// Log.e("approxi", String.valueOf(Constants.approximate));

            Float fqtty = Float.parseFloat(freeQty[i]);
            Float qtty = Float.parseFloat(qty[i]);
            int serialno = Integer.parseInt(Constants.serialno);

            serialno = serialno + 1;
            Constants.serialno = String.valueOf(serialno);
            String inputFormat = new SimpleDateFormat(
                    "yyyy-MM-dd HH:mm:ss").format(new Date());
            if (fqtty > 0) {
                String flag = "F";
                fread[i] = Constants.USER_ID + "," + Constants.SalesMerchant_Id + "," + userid[i];
                String ordimage = "0";

                Float totalunit=   (fqtty) *(Float.parseFloat(freeqtyUomUnit[i]));
                updtpval = (totalunit*(Double.parseDouble(price[i]) + (Double.parseDouble(price[i])*(Double.parseDouble(ptax[i])/100))));

                Double defaultuomTotal =( (fqtty) *(Double.parseDouble(unitgram[i]))*(Double.parseDouble(freeqtyUomUnit[i])))/1000;


                //Constants.approximate = Constants.approximate + updtpval;

                Log.e(" F uom[i]", uom[i]);
                Log.e(" F ordruom[i]", orderfreeuom[i]);
                Log.e(" F prodName[i]", prodName[i]);
                Log.e(" F freeQty[i]", freeQty[i]);

                String deldate="";
                if(!deliverydate[i].equals("")){
                    SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
                    Date date = null;
                    try {
                        date = fmt.parse(deliverydate[i]);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    SimpleDateFormat fmtOut = new SimpleDateFormat("yyyy-MM-dd");
                    deldate= fmtOut.format(date);
                    Log.e("deliverydate",deldate);

                }
                databaseHandler.addorderdetail(Constants.orderid, userid[i], companyname[i], prodid[i], freeQty[i], flag, "Pending", String.valueOf(serialno), prodCode[i], prodName[i], deldate, price[i], ptax[i],
                        updtpval, fread[i], "", batchno[i], mfgdate[i], expdate[i], ordimage, uom[i], orderfreeuom[i],defaultuomTotal,deliverytime[i],deliverymode[i], currstock[i]);

            }
            if (qtty > 0) {
                String flag = "NF";
                qread[i] = Constants.USER_ID + "," + Constants.SalesMerchant_Id + "," + userid[i];
                String ordimage = "0";

                Double totalunit=   (qtty) *(Double.parseDouble(qtyUomUnit[i]));
                updtpval = (totalunit*(Double.parseDouble(price[i]) + (Double.parseDouble(price[i])*(Double.parseDouble(ptax[i])/100))));

                Double defaultuomTotal =( (qtty) *(Double.parseDouble(unitgram[i]))*(Double.parseDouble(qtyUomUnit[i])))/1000;
                Log.e(" Constants.approximate", String.valueOf(Constants.approximate));
                Constants.approximate = Constants.approximate + updtpval;

                Log.e(" NF uom[i]", uom[i]);
                Log.e(" NF ordruom[i]", ordruom[i]);
                Log.e(" NF qty[i]", qty[i]);
                Log.e(" NF prodName[i]", prodName[i]);

                String deldate="";
                if (!deliverydate[i].equals("")) {
                    SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");

                    Date date = null;
                    try {
                        date = fmt.parse(deliverydate[i]);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    SimpleDateFormat fmtOut = new SimpleDateFormat("yyyy-MM-dd");
                    deldate = fmtOut.format(date);
                    Log.e("deliverydate",deldate);

                }
                databaseHandler.addorderdetail(Constants.orderid, userid[i], companyname[i], prodid[i], qty[i], flag, "Pending", String.valueOf(serialno), prodCode[i], prodName[i], deldate,price[i], ptax[i], updtpval, qread[i],
                        "", batchno[i], mfgdate[i], expdate[i], ordimage, uom[i], ordruom[i],defaultuomTotal,deliverytime[i],deliverymode[i], currstock[i]);
            }
        }

        db.execSQL("UPDATE orderheader SET aprxordval =" + Constants.approximate + "  WHERE oflnordid =" + Constants.orderid);

        Cursor curs;
        curs = databaseHandler.getorderheader();


        if (curs != null && curs.getCount() > 0) {

            if (curs.moveToLast()) {
                Orderno = curs.getString(0);
                approximate = curs.getInt(9);
                Log.e("approximate", String.valueOf(approximate));
            }
        }

        showAlertDialogToast("Updated Successfully");
        Constants.orderstatus = "Pending";


        Intent to = new Intent(SalesManOrderCheckoutActivity.this,
                SalesmanOrderDetailsActivity.class);
        Constants.approximate = 0;
        startActivity(to);
        finish();
    }

    /*
        I(Kumaravel) just Split the (PaymentCheck()) function for template2  19-12-2018

     */

    private void paymentProcess(){
        Log.e("array", String.valueOf(SalesManOrderActivity.arrayListimagebitmap.size()));
        if (Constants.orderid != "") {
            updateProducts();
        } else {
            if(MyApplication.getInstance().isTemplate8ForTemplate2()){
                for(int i = 0; i < CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size(); i++) {
                    MerchantOrderDomainSelected item = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i);
                    item.setDeliverydate("");
                    item.setDeliverytime("");
                    item.setDeliverymode(Constants.EMPTY);
                }
                paymentCommonProcess(); ///////
            }else if(MyApplication.getInstance().isTemplate9() ||
                    MyApplication.getInstance().isTemplate10()){

                for(int i = 0; i < CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size(); i++) {
                    MerchantOrderDomainSelected item = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i);
                    item.setDeliverydate("");
                    item.setDeliverytime("");
                    item.setDeliverymode(Constants.EMPTY);
                }
                paymentCommonProcess(); ///////

            } else {
                if (MyApplication.getInstance().isTemplate2() ||
                        MyApplication.getInstance().isTemplate3() ||
                        MyApplication.getInstance().isTemplate4()) {
                    paymentForTemplate2();
                } else {
                    paymentcheck();
                }
            }
        }
    }

    private void paymentForTemplate2(){
        final String[] strDate = {""};
        final String[] strTime = {""};
        //Code Change 05-06-2019 By Kumaravel For Prevent from Quantity 0 Loading
        if(!MyApplication.getInstance().isTemplate2()) { //only template 2 and 3
            for (int i = 0; i < CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size(); i++) {
                MerchantOrderDomainSelected item = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i);
                if (item.getqty().equals("0")) {
                    showAlertDialogToast(this.getResources().getString(R.string.zero_quantity_notification));
                    return;
                }
            }
        }

        // code change end 05-06-2019

        final Template2ShowPaymentPopup obj = new
                Template2ShowPaymentPopup(SalesManOrderCheckoutActivity.this,(View)buttonAddPlaceOrder);
        final View qtyDialog = obj.createPopup();

        if(qtyDialog == null){
            return;///need to show message
        }
        /*
        final Dialog qtyDialog = new Dialog(SalesManOrderCheckoutActivity.this);
        qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        qtyDialog.setContentView(R.layout.payment_type_template2);


        qtyDialog.setCancelable(false);
        qtyDialog.show();///  */




        Button buttonUpdateOk, buttonUpdateCancel;

        buttonUpdateOk = (Button) qtyDialog
                .findViewById(R.id.buttonUpdateOk);
        buttonUpdateCancel = (Button) qtyDialog
                .findViewById(R.id.buttonUpdateCancel);

        final EditText editTextTime = (EditText) qtyDialog
                .findViewById(R.id.editTextTime);
        final EditText editTextDate = (EditText) qtyDialog
                .findViewById(R.id.editTextDate);
        final Spinner textviewdelivery = (Spinner) qtyDialog
                .findViewById(R.id.textviewdelivery);
        final LinearLayout linearlayoutdatetime = (LinearLayout) qtyDialog
                .findViewById(R.id.linearlayoutdatetime);


        try {
            ArrayAdapter<String> deliverydapt = new ArrayAdapter<String>(SalesManOrderCheckoutActivity.this, android.R.layout.simple_spinner_dropdown_item, Constants.deliverymode);
            textviewdelivery.setAdapter(deliverydapt);
        }catch (Exception e){
            e.printStackTrace();
        }
        textviewdelivery.setSelection(0); /// Default Position

        /*
        editTextDate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get Current Date
                int mYear, mMonth, mDay;
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(SalesManOrderCheckoutActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                String monthLength = String.valueOf(monthOfYear);
                                if (monthLength.length() == 1) {
                                    monthLength = "0" + monthLength;
                                }
                                String dayLength = String.valueOf(dayOfMonth);
                                if (dayLength.length() == 1) {
                                    dayLength = "0" + dayLength;
                                }
                                editTextDate.setText(dayLength + "-" + (Integer.parseInt(monthLength) + 1) + "-" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });  */  // gide by vel for two time occur 16-07-2019

        textviewdelivery.setOnItemSelectedListener( new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selecteddeliverymode = ((TextView) view).getText().toString();/////////
                if (selecteddeliverymode.equals("Scheduled")) {
                    linearlayoutdatetime.setVisibility(View.VISIBLE);
                    final Calendar c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);
                    mHour = c.get(Calendar.HOUR_OF_DAY);
                    mMinute = c.get(Calendar.MINUTE);
                    mSeconds= c.get(Calendar.SECOND);
                    monthLength = String.valueOf(mMonth + 1);
                    if (monthLength.length() == 1) {
                        monthLength = "0" + monthLength;
                    }
                    dayLength = String.valueOf(mDay);
                    if (dayLength.length() == 1) {
                        dayLength = "0" + dayLength;
                    }

                    String minuteLength = String.valueOf(mMinute);
                    if (minuteLength.length() == 1) {
                        minuteLength = "0" + minuteLength;
                    }

                    String AM_PM ;
                    if(mHour < 12) {
                        AM_PM = "AM";
                    } else {
                        AM_PM = "PM";
                    }
                    // set current date into textview
                    strDate[0] = dayLength + "-" + monthLength + "-" + mYear; // Month is 0 based, just add 1
                    editTextDate.setText(strDate[0]);

                    strTime[0] = mHour + ":" + minuteLength; // Month is 0 based, just add 1
                    editTextTime.setText(strTime[0]);

                } else {
                    linearlayoutdatetime.setVisibility(View.GONE);
                    editTextTime.setText("");
                    editTextDate.setText("");
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        /*
        textviewdelivery.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                textviewdelivery.setText("");
                ArrayAdapter<String> deliverydapt = new ArrayAdapter<String>(SalesManOrderCheckoutActivity.this, android.R.layout.simple_spinner_dropdown_item, Constants.deliverymode);
                textviewdelivery.setAdapter(deliverydapt);
                textviewdelivery.showDropDown();
                InputMethodManager iss = (InputMethodManager) (SalesManOrderCheckoutActivity.this).getSystemService(Context.INPUT_METHOD_SERVICE);
                iss.hideSoftInputFromWindow(textviewdelivery.getWindowToken(), 0);
                return false;
            }
        });  */

        editTextTime.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final int mHour, mMinute,mSeconds;
                final Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);
                mSeconds = c.get(Calendar.SECOND);
                // Launch Time Picker Dialog
                TimePickerDialog timePickerDialog = new TimePickerDialog(SalesManOrderCheckoutActivity.this,
                        new TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {
                                String minLength = String.valueOf(minute);
                                if (minLength.length() == 1) {
                                    minLength = "0" + minLength;
                                }
                                strTime[0] = hourOfDay + ":" + minLength;
                                editTextTime.setText(strTime[0]);
                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();
            }
        });

        editTextDate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get Current Date
                int mYear, mMonth, mDay;
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog datePickerDialog = new DatePickerDialog(SalesManOrderCheckoutActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                String monthLength = String.valueOf(monthOfYear);
                                if (monthLength.length() == 1) {
                                    monthLength = "0" + monthLength;
                                }
                                String dayLength = String.valueOf(dayOfMonth);
                                if (dayLength.length() == 1) {
                                    dayLength = "0" + dayLength;
                                }
                                strDate[0] = dayLength + "-" + (Integer.parseInt(monthLength) + 1) + "-" + year;
                                editTextDate.setText(strDate[0]);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        PopupWindow pop = obj.showPopup();

        buttonUpdateOk.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                RadioGroup radioPayment;
                RadioButton radioCash;
                radioPayment = (RadioGroup) qtyDialog.findViewById(R.id.radioPayment);
                int selectedId = radioPayment.getCheckedRadioButtonId();
                Log.e("Date","............Date::" + strDate[0] + "...Time:: " + strTime[0]);
                // find the radiobutton by returned id
                radioCash = (RadioButton) qtyDialog.findViewById(selectedId);
                Constants.PaymentType = radioCash.getText().toString();
                obj.closePopup();
                // saveOrder();

                for(int i = 0; i < CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size(); i++) {
                    MerchantOrderDomainSelected item = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i);
                    if (selecteddeliverymode.equals(getResources().getString(R.string.delivery_mode_scheduled))) {
                        item.setDeliverymode(selecteddeliverymode);
                        //item.setDeliverydate(editTextDate.getText().toString());
                        //item.setDeliverytime(editTextTime.getText().toString());
                        item.setDeliverydate(strDate[0]);
                        item.setDeliverytime(strTime[0]);
                        Log.e("Date","............Date::" + strDate[0] + "...Time:: " + strTime[0]);
                    } else {
                        item.setDeliverydate("");
                        item.setDeliverytime("");
                        item.setDeliverymode(selecteddeliverymode);
                    }
                }
                paymentCommonProcess(); ///////
            }
        });


        buttonUpdateCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                obj.closePopup();
            }
        });

    }

    private void paymentCommonProcess(){

        Log.e("Selected count", String.valueOf(CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()));
        for (int i = 0; i < CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size(); i++) {
            String prodid = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getprodid();
            Log.e("uomproduct", CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getorderuom());
            Log.e("default", CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getUOM());
            /// kumaravel need to asign uom per unit
            if(!TEMPLATE_NO.equals(TEMPLATE5)) { /// this is set for unit uom calculation
                MerchantOrderDomainSelected item = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i);
                Cursor curs = databaseHandler.getUomUnit(item.getprodid(), item.getorderuom()); //// findUOMUnit
                curs.moveToFirst();
                if (curs != null && curs.getCount() > 0) {
                    item.setSelecetdQtyUomUnit(curs.getString(0));
                    //item.setSelecetdQtyUomUnit(item.getUnitPerUom());
                    Log.e("else   selectedUOM ", item.getUOM());
                    Log.e("else   QtyUomUnit ", item.getUnitPerUom());
                    Log.e("else   Orderuom ", item.getorderuom());
                }
            }
            Cursor cur;
            cur = databaseHandler.getScheme(prodid);
            Log.e("scheme count", String.valueOf(cur.getCount()));
            if (cur.getCount() != 0) {
                cur.moveToFirst();
                String freeProdid = cur.getString(7);
                Log.e("NewProdid", freeProdid);
                String discount = cur.getString(5);
                Log.e("Newdiscount", discount);
                String freeQty = cur.getString(8);
                Log.e("NewfreeQty", freeQty);
                MerchantOrderDomainSelected dod = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i);
                if (!discount.equals("0") && !discount.equals("null") && !discount.isEmpty()) {
                    Log.e("discount", discount);

                    double price = Double.parseDouble(CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getprice());
                    price = price - Double.parseDouble(discount);
                    Log.e("Original price", CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getprice());
                    Log.e("New price", String.valueOf(price));

                    Log.e("Free qty", CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getFreeQty());

                    dod.setprice(String.valueOf(price));
                    CreateOrderActivity.arraylistMerchantOrderDetailListSelected.set(i, dod).setprice(String.valueOf(price));

                    Log.e("Updatde price", CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getprice());
                    Log.e("Free qty", CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getFreeQty());

                    Log.e("Updated count", String.valueOf(CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()));
                } else {
                    Log.e("freeProdid", freeProdid);
                    if (freeProdid.equals(prodid)) {
                        dod.setFreeQty(freeQty);
                        Log.e("updatedfreeqty", dod.getFreeQty());
                        CreateOrderActivity.arraylistMerchantOrderDetailListSelected.set(i, dod).setFreeQty(freeQty);

                    } else {
                        Cursor curs;
                        curs = databaseHandler.getSchemeProduct(freeProdid);
                        curs.moveToFirst();
                        Log.e("Free product count", String.valueOf(curs.getCount()));
                        if (curs.getCount() != 0) {
                            MerchantOrderDomainSelected dd = new MerchantOrderDomainSelected();
                            dd.setFreeQty(freeQty);
                            dd.setDate(curs.getString(8));
                            dd.setprodcode(curs.getString(3));
                            dd.setprodid(curs.getString(4));
                            dd.setprodname(curs.getString(5));
                            dd.setqty("0");
                            dd.setOutstock("");
                            dd.setCompanyname(curs.getString(2));
                            dd.setUserid(curs.getString(1));
                            dd.setprice(curs.getString(10));
                            dd.setptax(curs.getString(11));
                            dd.setusrdlrid("");
                            dd.setBatchno(curs.getString(12));
                            dd.setExpdate(curs.getString(13));
                            dd.setMgfDate(curs.getString(14));
                            dd.setUOM(curs.getString(15));
                            dd.setUnitGram(curs.getString(17));
                            dd.setUnitPerUom(curs.getString(18));
                            dd.setCurrStock(curs.getString(20));
                            dod.setSelecetdQtyUomUnit(curs.getString(18));
                            dod.setSelecetdFreeQtyUomUnit(curs.getString(18));
                            dd.setorderuom(CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getorderuom());
                            int serialNo = Integer.parseInt(CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getserialno());
                            dd.setserialno(String.valueOf(serialNo + 1));
                            CreateOrderActivity.arraylistMerchantOrderDetailListSelected.add(dd);
                            Log.e("Selected  Count", String.valueOf(CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()));
                        }
                    }
                }
            }
        }
        orderfreeuom = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        ordruom = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        uom = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        outstock = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        batchno = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        expdate = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        mfgdate = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        prodCode = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        prodName = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        companyname = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        userid = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        prodid = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        qty = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        freeQty = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        price = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        ptax = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        unitgram = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        qtyUomUnit = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        freeqtyUomUnit= new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        deliverydate = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        deliverymode = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        deliverytime = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];
        currstock = new String[CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()];

        for (int i = 0; i < CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size(); i++) {
            userid[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getUserid();
            prodid[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getprodid();
            qty[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getqty();
            freeQty[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getFreeQty();
            companyname[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getCompanyname();
            prodCode[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getprodcode();
            prodName[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getprodname();
            price[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getprice();
            ptax[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getptax();
            batchno[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getBatchno();
            expdate[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getExpdate();
            mfgdate[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getMgfDate();
            outstock[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getOutstock();
            uom[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getUOM();
            ordruom[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getorderuom();
            orderfreeuom[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getorderfreeuom();
            unitgram[i] =CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getUnitGram();
            qtyUomUnit[i]=CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getSelectedQtyUomUnit();
            freeqtyUomUnit[i]=CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getSelectedFreeQtyUomUnit();
            deliverymode[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getDeliverymode();
            deliverytime[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getDeliverytime();
            deliverydate[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getDeliverydate();
            currstock[i] = CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(i).getCurrStock();

            Log.e("ordruom", ordruom[i]);
            Log.e("orderfreeuom", orderfreeuom[i]);
        }

        Log.e("payment_type", Constants.PaymentType);

        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        hr = c.get(Calendar.HOUR_OF_DAY);
        mins = c.get(Calendar.MINUTE);
        sec = c.get(Calendar.SECOND);
        deliverydt = year + (month + 1) + day;

        String inputFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss").format(new Date());
        todayDate = inputFormat;
        Cursor curs2;
        if(!MyApplication.getInstance().isTemplate8ForTemplate2()) {
            curs2 = databaseHandler.getallorderheader();
        }else {
            curs2 = databaseHandler.getAllStockOrderHeader();
        }
        Log.e("orders", String.valueOf(curs2.getCount()));

        if (curs2 != null && curs2.getCount() > 0) {

            if (curs2.moveToLast()) {
                lasttrackdate = curs2.getString(6);

                try {
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    Date date1 = formatter.parse(lasttrackdate);
                    Date date2 = formatter.parse(todayDate);

                    if (date1.compareTo(date2) < 0) {

                        System.out.println("Date1 is before Date2");
                        IntentFilter filter = new IntentFilter();
                        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
                        registerReceiver(locationtrack, filter);

                    } else {

                    }

                } catch (ParseException e1) {
                    e1.printStackTrace();
                }
            }
        } else {
            Log.e("inside", "inside");
            IntentFilter filter = new IntentFilter();
            filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
            registerReceiver(locationtrack, filter);

        }
        saveOfflineHeader();
        // need to clear selected Items Kumaravel
        MyApplication.previousSelected.clear();//////////////
        MyApplication.previousSelectedForSearch.clear();//////////
    }


    public void paymentcheck() {


        final Dialog qtyDialog = new Dialog(SalesManOrderCheckoutActivity.this);
        qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        qtyDialog.setContentView(R.layout.paymenttype);


        Button buttonUpdateOk, buttonUpdateCancel;

        buttonUpdateOk = (Button) qtyDialog
                .findViewById(R.id.buttonUpdateOk);
        buttonUpdateCancel = (Button) qtyDialog
                .findViewById(R.id.buttonUpdateCancel);

        qtyDialog.show();///

        buttonUpdateOk.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {


                RadioGroup radioPayment;
                RadioButton radioCash;
                radioPayment = (RadioGroup) qtyDialog.findViewById(R.id.radioPayment);
                int selectedId = radioPayment.getCheckedRadioButtonId();

                // find the radiobutton by returned id
                radioCash = (RadioButton) qtyDialog.findViewById(selectedId);
                Constants.PaymentType = radioCash.getText().toString();
                //dod.setpaymentType(item.getPaymentType());
                qtyDialog.dismiss();
                // saveOrder();

                paymentCommonProcess(); ///////

            }

        });

        buttonUpdateCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                qtyDialog.dismiss();
            }
        });
    }

    public void beatupdate() {

        Cursor cursor;
        cursor = db.rawQuery("SELECT * from beat where flag ='Inactive'", null);
        Log.e("beatCount", String.valueOf(cursor.getCount()));
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            final JSONArray beatarray = new JSONArray();
            while (cursor.isAfterLast() == false) {

                int Columncount = cursor.getColumnCount();
                JSONObject beatobject = new JSONObject();


                for (int i = 0; i < 15; i++) {
                    if (cursor.getColumnName(i) != null) {

                        try {

                            if (cursor.getString(i) != null) {
                                Log.d("TAG_NAME", cursor.getString(i));
                                beatobject.put(cursor.getColumnName(i), cursor.getString(i));
                            } else {
                                beatobject.put(cursor.getColumnName(i), "");

                            }
                        } catch (Exception e) {
                            Log.d("TAG_NAME", e.getMessage());
                        }
                    }

                }


                beatarray.put(beatobject);

                cursor.moveToNext();
            }

            cursor.close();

            Log.e("Detailarray", beatarray.toString());

            new AsyncTask<Void, Void, Void>() {
                String strStatus = "";
                String strMsg = "";

                @Override
                protected void onPreExecute() {

                }

                @Override
                protected void onPostExecute(Void result) {
                    Log.e("Success",".........result"+result);
                }

                @Override
                protected Void doInBackground(Void... params) {
                    JSONObject jsonObject = new JSONObject();
                    try {

                        for (int k = 0; k < beatarray.length(); k++) {
                            jsonObject = beatarray.getJSONObject(k);
                            beatrowid = jsonObject.getString("rowid");
                            JsonServiceHandler = new JsonServiceHandler(Utils.strsavebeat, jsonObject.toString(), getApplicationContext());
                            JsonAccountObject = JsonServiceHandler.ServiceData();

                            try {

                                strStatus = JsonAccountObject.getString("status");
                                Log.e("return status", strStatus);
                                strMsg = JsonAccountObject.getString("message");
                                Log.e("return message", strMsg);

                                if (strStatus.equals("true")) {

                                    db.execSQL("UPDATE beat SET flag = 'Active' WHERE rowid =" + "'" + beatrowid + "'");
                                    if (k == (beatarray.length() - 1)) {

                                    }
                                } else {

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (Exception e) {

                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return null;

                }
            }.execute(new Void[]{});

        } else {

        }
    }

    /*
        public void location() {

            Log.e("LocationManager","LocationManager");
            LocationManager locationManager =
                    (LocationManager) SalesManOrderCheckoutActivity.this.getSystemService(Context.LOCATION_SERVICE);
            // Define a listener that responds to location updates
            LocationListener locationListener = new LocationListener() {
                public void onLocationChanged(Location location) {
                    // Called when a new location is found by the network location provider.
                    String latitude = Double.toString(location.getLatitude());
                    String longitude = Double.toString(location.getLongitude());
                    Log.e("latitude",latitude);
                    Log.e("longitude",longitude);
                    Toast.makeText(getApplicationContext(), "Your Location is:" + latitude + "--" + longitude, Toast.LENGTH_LONG);
                }

                public void onStatusChanged(String provider, int status, Bundle extras) {
                }

                public void onProviderEnabled(String provider) {
                }

                public void onProviderDisabled(String provider) {
                }
            };
            // Register the listener with the Location Manager to receive location updates
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        }
    */
    public boolean netCheck() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            }

            else if (result == null) {
               /* showAlertDialog(SalesManOrderCheckoutActivity.this,
                        "No Internet Connection",
                        "Please Check Your internet connection.", false);*/
                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
    public void showAlertDialog(Context context, String title, String message,
                                Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        alertDialog.show();
    }

    protected void toastDisplay(String msg) {
        Toast toast = Toast.makeText(SalesManOrderCheckoutActivity.this, msg,
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();

    }

    public void showAlertDialogToast( String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    public void onBackPressed() {
        exitAlret();
    }

    private void exitAlret() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        localBuilder.setMessage("Do you want to Exit?");
        localBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {

                        finish();

                    }
                });
        localBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {
                        paramDialogInterface.cancel();
                    }
                });
        localBuilder.create().show();
    }
}
