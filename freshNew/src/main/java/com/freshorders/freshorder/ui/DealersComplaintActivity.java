package com.freshorders.freshorder.ui;

import java.net.URI;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.DealerComplaintAdapter;
import com.freshorders.freshorder.adapter.DealerOrderListAdapter;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.DealerComplaintDomain;
import com.freshorders.freshorder.domain.DealerOrderListDomain;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class DealersComplaintActivity extends Activity {

	TextView menuIcon, textViewAssetMenuProfile, textViewAssetMenuMyOrders,
			textViewAssetMenuMyDealers, textViewAssetMenuProducts,
			textViewAssetMenuPayment, textViewAssetMenuComplaint,
			textViewAssetMenuSignout,textViewDescription,textViewAssetMenuCreateOrder,
			textViewAssetMenuExport,textViewAssetMenuSClientVisit;
	int menuCliccked;
	LinearLayout linearLayoutMenuParent, linearLayoutProfile,
			linearLayoutMyOrders, linearLayoutMyDealers, linearLayoutProducts,
			linearLayoutPayment, linearLayoutComplaint,
			linearLayoutSignout,linearLayoutCreateOrder,linearLayoutExport,linearLayoutClientVisit;
	JSONObject JsonAccountObject = null;
	JSONArray JsonAccountArray = null;
	DatabaseHandler databaseHandler;
	JsonServiceHandler JsonServiceHandler ;
	ArrayList<DealerComplaintDomain> arraylistDealerComplaintList;
	  Date dateStr = null;
	  public static String time;
	  ListView listViewComplaints;
	  Dialog dialogOtp;
	  Button buttonDelete;
	  public static String cmpID,complaintContent,PaymentStatus="NULL",moveto="NULL",imagepost="";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(1);
		setContentView(R.layout.dealer_complaint_screen);
		netCheck();
		databaseHandler = new DatabaseHandler(getApplicationContext());
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();

		Cursor cur;
		cur = databaseHandler.getDetails();
		cur.moveToFirst();
		Log.e("Insertion Check",
				cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_username)));
		Log.e("Insertion Check",
				cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_password)));
		Log.e("Insertion Check",
				cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_usertype)));
		myList();
		linearLayoutCreateOrder = (LinearLayout) findViewById(R.id.linearLayoutCreateOrder);
		listViewComplaints= (ListView) findViewById(R.id.listViewComplaints);
		linearLayoutProfile = (LinearLayout) findViewById(R.id.linearLayoutProfile);
		linearLayoutMyOrders = (LinearLayout) findViewById(R.id.linearLayoutMyOrders);
		linearLayoutMyDealers = (LinearLayout) findViewById(R.id.linearLayoutMyDealers);
		linearLayoutProducts = (LinearLayout) findViewById(R.id.linearLayoutProducts);
		linearLayoutPayment = (LinearLayout) findViewById(R.id.linearLayoutPayment);
		linearLayoutComplaint = (LinearLayout) findViewById(R.id.linearLayoutComplaint);
		linearLayoutSignout = (LinearLayout) findViewById(R.id.linearLayoutSignout);
		linearLayoutExport= (LinearLayout) findViewById(R.id.linearLayoutExport);
		linearLayoutClientVisit= (LinearLayout) findViewById(R.id.linearLayoutClientVisit);

		if (Constants.USER_TYPE.equals("D")) {
			linearLayoutMyDealers.setVisibility(View.GONE);
			linearLayoutProducts.setVisibility(View.VISIBLE);
			linearLayoutCreateOrder.setVisibility(View.GONE);
			linearLayoutExport.setVisibility(View.VISIBLE);
		} else if (Constants.USER_TYPE.equals("M")){
			linearLayoutMyDealers.setVisibility(View.VISIBLE);
			linearLayoutProducts.setVisibility(View.GONE);
			linearLayoutExport.setVisibility(View.GONE);
			linearLayoutCreateOrder.setVisibility(View.GONE);
		}else {
			linearLayoutMyDealers.setVisibility(View.VISIBLE);
			linearLayoutProducts.setVisibility(View.GONE);
			linearLayoutExport.setVisibility(View.GONE);
			linearLayoutCreateOrder.setVisibility(View.VISIBLE);
		}

		menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
		textViewAssetMenuCreateOrder= (TextView) findViewById(R.id.textViewAssetMenuCreateOrder);
		textViewAssetMenuProfile = (TextView) findViewById(R.id.textViewAssetMenuProfile);
		textViewAssetMenuMyOrders = (TextView) findViewById(R.id.textViewAssetMenuMyOrders);
		textViewAssetMenuMyDealers = (TextView) findViewById(R.id.textViewAssetMenuMyDealers);
		textViewAssetMenuProducts = (TextView) findViewById(R.id.textViewAssetMenuProducts);
		textViewAssetMenuPayment = (TextView) findViewById(R.id.textViewAssetMenuPayment);
		textViewAssetMenuComplaint = (TextView) findViewById(R.id.textViewAssetMenuComplaint);
		textViewAssetMenuSignout = (TextView) findViewById(R.id.textViewAssetMenuSignout);
		textViewAssetMenuExport = (TextView) findViewById(R.id.textViewAssetMenuExport);
		textViewAssetMenuSClientVisit = (TextView) findViewById(R.id.textViewAssetMenuSClientVisit);

		linearLayoutMenuParent = (LinearLayout) findViewById(R.id.linearLayoutMenuParent);
		Typeface font = Typeface.createFromAsset(getAssets(),
				"fontawesome-webfont.ttf");
		menuIcon.setTypeface(font);
		textViewAssetMenuCreateOrder.setTypeface(font);
		textViewAssetMenuProfile.setTypeface(font);
		textViewAssetMenuMyOrders.setTypeface(font);
		textViewAssetMenuMyDealers.setTypeface(font);
		textViewAssetMenuProducts.setTypeface(font);
		textViewAssetMenuPayment.setTypeface(font);
		textViewAssetMenuComplaint.setTypeface(font);
		textViewAssetMenuSignout.setTypeface(font);
		textViewAssetMenuExport.setTypeface(font);
		textViewAssetMenuSClientVisit.setTypeface(font);
		
		dialogOtp = new Dialog(DealersComplaintActivity.this);
		dialogOtp.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialogOtp.setContentView(R.layout.dialog_complaint);
		textViewDescription = (TextView) dialogOtp
				.findViewById(R.id.textViewDescription);

		buttonDelete = (Button) dialogOtp.findViewById(R.id.buttonDelete);
		

		buttonDelete.setOnClickListener(new OnClickListener() {

			@SuppressLint("NewApi")
			@Override
			public void onClick(View v) {

			
						Delete();

					
				

			}

			
		});
		
		
		menuIcon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (menuCliccked == 0) {
					linearLayoutMenuParent.setVisibility(View.VISIBLE);
					menuCliccked = 1;
				} else {
					linearLayoutMenuParent.setVisibility(View.GONE);
					menuCliccked = 0;
				}

			}
		});

		linearLayoutMenuParent.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
			}
		});

		linearLayoutProfile.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				if(netCheckWithoutAlert()==true){
					getDetails("profile");
				}else{
					showAlertDialogToast("Please Check Your internet connection");
				}

			}
		});

		linearLayoutMyOrders.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				if(netCheckWithoutAlert()){
					getDetails("myorders");
				}else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment before place order");
					} else {
						switch (Constants.USER_TYPE) {
							case "D": {
								Intent io = new Intent(DealersComplaintActivity.this,
										DealersOrderActivity.class);
								io.putExtra("Key", "menuclick");
								startActivity(io);
								finish();
								break;
							}
							case "M": {
								Intent io = new Intent(DealersComplaintActivity.this,
										MerchantOrderActivity.class);
								io.putExtra("Key", "menuclick");
								Constants.setimage = null;
								startActivity(io);
								finish();
								break;
							}
							default: {
								Intent io = new Intent(DealersComplaintActivity.this,
										SalesManOrderActivity.class);
								io.putExtra("Key", "menuclick");
								Constants.setimage = null;
								startActivity(io);
								finish();
								break;
							}
						}
					}
				}


			}
		});

		linearLayoutMyDealers.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				// TODO Auto-generated method stub
				Intent io = new Intent(DealersComplaintActivity.this,
						MyDealersActivity.class);
				startActivity(io);
				finish();

			}
		});

		linearLayoutProducts.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				if(netCheckWithoutAlert()==true){
					getDetails("product");
				}else {
					showAlertDialogToast("Please Check Your internet connection");
				}

			}
		});
		linearLayoutCreateOrder.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				// TODO Auto-generated method stub
				Intent io = new Intent(DealersComplaintActivity.this,
						CreateOrderActivity.class);
				startActivity(io);
				finish();

			}
		});

		linearLayoutPayment.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				if(netCheckWithoutAlert()==true){
					Intent io = new Intent(DealersComplaintActivity.this,
							DealerPaymentActivity.class);
					startActivity(io);
					finish();
				}else {
					showAlertDialogToast("Please Check Your internet connection");
				}



			}
		});
		linearLayoutExport.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				Constants.filter="0";
				if(netCheckWithoutAlert()==true){
					getDetails("export");
				}else {
					showAlertDialogToast("Please Check Your internet connection");
				}
			}
		});


		linearLayoutComplaint.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
			}
		});
		linearLayoutClientVisit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				if(netCheckWithoutAlert()){
					getDetails("clientvisit");
				}else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment");
					} else {
						if (Constants.USER_TYPE.equals("D")) {
							Intent io = new Intent(DealersComplaintActivity.this,
									DealerClientVisit.class);
							startActivity(io);
							finish();
						} else {
							Intent io = new Intent(DealersComplaintActivity.this,
									SMClientVisitHistory.class);
							startActivity(io);
							finish();
						}
					}
				}
			}
		});


		linearLayoutSignout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				// TODO Auto-generated method stub
				databaseHandler.delete();
				Intent io = new Intent(DealersComplaintActivity.this,
						SigninActivity.class);
				startActivity(io);
				finish();
			}
		});
		
		listViewComplaints.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				cmpID=arraylistDealerComplaintList.get(position).getCmpID();
				complaintContent=arraylistDealerComplaintList.get(position).getDescription();
				imagepost=arraylistDealerComplaintList.get(position).getimage();
				Log.e("image1size", String.valueOf(imagepost));

				Intent to = new Intent(DealersComplaintActivity.this,ComplaintsDeleteActivity.class);
				startActivity(to);
				finish();


			}
		});

	}
	private void getDetails(final String paymentcheck) {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog= ProgressDialog.show(DealersComplaintActivity.this, "",
						"Loading...", true, true);

			}

			@Override
			protected void onPostExecute(Void result) {

				try {

					PaymentStatus=(JsonAccountObject.getString("pymtstatus"));
					Log.e("PaymentStatus",PaymentStatus);


					if(PaymentStatus.equals("Pending")||PaymentStatus.equals(null) ||PaymentStatus.equals("")||PaymentStatus.equals("null")){
						showAlertDialogToast("Please make payment before place order");
					}else{
						Log.e("paymentcheck", paymentcheck);

						if(paymentcheck.equals("profile")){
							Intent io = new Intent(DealersComplaintActivity.this,
									ProfileActivity.class);
							startActivity(io);
							finish();

						}else if(paymentcheck.equals("product")){
							Intent io = new Intent(DealersComplaintActivity.this,
									ProductActivity.class);
							startActivity(io);
							finish();

						}else if(paymentcheck.equals("export")){
							Intent io = new Intent(DealersComplaintActivity.this,
									ExportActivity.class);
							startActivity(io);
							finish();

						}else if(paymentcheck.equals("myorders")){
							Intent to = new Intent(DealersComplaintActivity.this,
									DealersOrderActivity.class);
							startActivity(to);
							finish();

						}else if(paymentcheck.equals("clientvisit")){

							Intent io = new Intent(DealersComplaintActivity.this,
									DealerClientVisit.class);
							startActivity(io);
							finish();

						}



					}

					dialog.dismiss();


				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
				}

			}

			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();


				JsonServiceHandler = new JsonServiceHandler(Utils.strGetProfileDetail+Constants.USER_ID, DealersComplaintActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceDataGet();
				return null;
			}
		}.execute(new Void[]{});
	}


	private void Delete() {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog= ProgressDialog.show(DealersComplaintActivity.this, "",
						"Loading...", true, true);

			}

			@Override
			protected void onPostExecute(Void result) {

				try {
					
					

					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);

					if (strStatus.equals("true")) {
						showAlertDialogToast(strMsg);
					
						//toastDisplay(strMsg);
						
						myList();
						dialogOtp.dismiss();
					
					}else{
					//	listViewOrders.setVisibility(View.GONE);
					//	textViewNodata.setVisibility(View.VISIBLE);
					//	textViewNodata.setText(strMsg);
						showAlertDialogToast(strMsg);
						//toastDisplay(strMsg);
						dialogOtp.dismiss();
					
					}
					
					dialog.dismiss();
				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					Log.e("ProductActivityException", e.toString());
				}

			}

			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();
				try {
					jsonObject.put("status", "deleted");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				

				JsonServiceHandler = new JsonServiceHandler(Utils.strDeleteComplaint+cmpID,jsonObject.toString(), DealersComplaintActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceData();
				return null;
			}
		}.execute(new Void[] {});
	}
	private void myList() {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog= ProgressDialog.show(DealersComplaintActivity.this, "",
						"Loading...", true, true);

			}

			@Override
			protected void onPostExecute(Void result) {

				try {

					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);

					if (strStatus.equals("true")) {
						
					
						
						JSONArray job1 = JsonAccountObject.getJSONArray("data");
						arraylistDealerComplaintList=new ArrayList<DealerComplaintDomain>();
						for (int i = 0; i < job1.length(); i++) {
							JSONObject job = new JSONObject();
							job = job1.getJSONObject(i);
							try {
								DealerComplaintDomain dod = new DealerComplaintDomain();
								dod.setAddress(job.getJSONObject("muser").getString("address"));
								dod.setMuserId(job.getJSONObject("muser").getString("userid"));
								;
								dod.setCompanyName(job.getJSONObject("muser").getString("companyname"));
								dod.setCmpID(job.getString("complid"));
								dod.setDescription(job.getString("particular"));

								dod.setimage(job.getString("attchmnt"));
								Log.e("", job.getString("attchmnt"));
								//dod.setDate(job.getString("orderdt"));
								time = job.getString("updateddt");

								String inputPattern = "yyyy-MM-dd HH:mm:ss";
								String outputPattern = "dd-MMM-yyyy HH:mm aa";
								SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
								SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

								String str = null;


									dateStr = inputFormat.parse(time);
									str = outputFormat.format(dateStr);
									dod.setDate(str);
									Log.e("str", str);


								arraylistDealerComplaintList.add(dod);
							} catch (ParseException e) {
								e.printStackTrace();
							}
							}

						DealerComplaintAdapter adapter=new DealerComplaintAdapter(DealersComplaintActivity.this, R.layout.item_add_dealer, arraylistDealerComplaintList);
						listViewComplaints.setVisibility(View.VISIBLE);
						listViewComplaints.setAdapter(adapter);
					//	textViewNodata.setVisibility(View.GONE);
					}else{
					//	listViewOrders.setVisibility(View.GONE);
					//	textViewNodata.setVisibility(View.VISIBLE);
					//	textViewNodata.setText(strMsg);
						//toastDisplay(strMsg);
						showAlertDialogToast(strMsg);
					
					}
					dialog.dismiss();
				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					Log.e("ProductActivityException", e.toString());
				}

			}

			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();
				

				JsonServiceHandler = new JsonServiceHandler(Utils.strGetComplaint+Constants.USER_ID+"&filter[where][status]=pending", DealersComplaintActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceDataGet();
				return null;
			}
		}.execute(new Void[] {});
	}
	protected void toastDisplay(String msg) {
		Toast toast = Toast.makeText(DealersComplaintActivity.this, msg,
				Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

	}
	public boolean netCheck() {
		// for network connection
		try {
			ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetwork = connectionManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			Object result = null;
			if (mWifi.isConnected() || mNetwork.isConnected()) {
				return true;
			}

			else if (result == null) {
				showAlertDialog(DealersComplaintActivity.this,
						"No Internet Connection",
						"Please Check Your internet connection.", false);
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public boolean netCheckWithoutAlert() {
		// for network connection
		try {
			ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetwork = connectionManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			Object result = null;
			if (mWifi.isConnected() || mNetwork.isConnected()) {
				return true;
			}

			else if (result == null) {

				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	public void showAlertDialog(Context context, String title, String message,
			Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
					}
				});
		alertDialog.show();
	}
	public void showAlertDialogToast( String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();

		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

	}

	public void onBackPressed() {
		exitAlret();
	}

	private void exitAlret() {
		AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
		localBuilder.setCancelable(false);
		localBuilder.setMessage("Do you want to Exit?");
		localBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {

						finish();

					}
				});
		localBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {
						paramDialogInterface.cancel();
					}
				});
		localBuilder.create().show();
	}

}
