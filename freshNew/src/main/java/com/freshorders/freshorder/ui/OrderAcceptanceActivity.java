package com.freshorders.freshorder.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.OrderAcceptanceAdapter;
import com.freshorders.freshorder.aync.IUserDetailCallBack;
import com.freshorders.freshorder.aync.UserDetailTask;
import com.freshorders.freshorder.model.GRN;
import com.freshorders.freshorder.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class OrderAcceptanceActivity extends Activity implements IUserDetailCallBack {

    private ListView orderAcceptanceListView;
    private TextView orderAcceptanceDataNotFound;
    private ProgressDialog mProgressDialog;
    private ArrayList<GRN> orderAcceptanceList = new ArrayList<>();
    private static final String TAG = OrderAcceptanceActivity.class.getSimpleName();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.order_acceptance_activity);
        orderAcceptanceListView = (ListView) findViewById(R.id.orderAcceptanceListView);
        orderAcceptanceDataNotFound = (TextView) findViewById(R.id.orderAcceptanceDataNotFoundTxtView);
        getOderAcceptance();
    }

    private void show() {
        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(OrderAcceptanceActivity.this, "",
                    "Initializing the order ...", true, true);
        }
    }

    private void dismissDialog() {
        if (mProgressDialog != null &&
                mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    public void getOderAcceptance() {
        show();
        Log.d(TAG, "getGrnOrder in ......");
        final UserDetailTask userDetail = new UserDetailTask(Utils.strOrderAcceptance,
                null, OrderAcceptanceActivity.this, this);
        userDetail.execute();
    }

    @Override
    public void userResponse(JSONObject jsonObject) {
        dismissDialog();

        if (jsonObject == null) {
            return;
        }
        Log.d(TAG, "grnResponse in ......" + jsonObject.toString());
        if (jsonObject.optString("status").equalsIgnoreCase("true")) {
            final JSONArray dataArr = jsonObject.optJSONArray("data");
            if (dataArr == null) {
                final String msg = jsonObject.optString("message");
                setAlert(msg);
                return;
            }
            orderAcceptanceList = Utils.getOrderList(dataArr);
            setOrderAcceptanceAdapter();

        } else if (jsonObject.optString("status").equalsIgnoreCase("false")) {
            final String msg = jsonObject.optString("message");
            setAlert(msg);
        }
    }


    private void setOrderAcceptanceAdapter() {
        orderAcceptanceDataNotFound.setVisibility(View.GONE);
        setListVisibility(View.VISIBLE);
        OrderAcceptanceAdapter grnAdapter = new OrderAcceptanceAdapter(OrderAcceptanceActivity.this,
                R.layout.grn_list_view, orderAcceptanceList);
        orderAcceptanceListView.setAdapter(grnAdapter);
        orderAcceptanceListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                navigateToNxtScreen(i);
            }
        });
    }

    private void setAlert(final String msg) {
        orderAcceptanceDataNotFound.setVisibility(View.VISIBLE);
        orderAcceptanceDataNotFound.setText(msg);
        setListVisibility(View.GONE);
    }

    private void setListVisibility(final int mode) {
        orderAcceptanceListView.setVisibility(mode);
    }

    private void navigateToNxtScreen(final int listPos) {

        Intent intent = new Intent(OrderAcceptanceActivity.this,
                OrderAcceptanceDetailScreenActivity.class);
        final Bundle b = new Bundle();
        b.putParcelableArrayList(GRNOrderScreenActivity.GRN_LIST, orderAcceptanceList);
        intent.putExtra(GRNOrderScreenActivity.GRN_CLICKED_POS, listPos);
        intent.putExtras(b);
        startActivity(intent);
    }
}
