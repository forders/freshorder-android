package com.freshorders.freshorder.ui;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.activity.DistanceCalActivity;
import com.freshorders.freshorder.adapter.DealerOrderListAdapter;
import com.freshorders.freshorder.aync.IUserDetailCallBack;
import com.freshorders.freshorder.aync.UserDetailTask;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.DealerOrderListDomain;
import com.freshorders.freshorder.domain.MDealerCompDropdownDomain;
import com.freshorders.freshorder.domain.OutletStockEntryDomain;
import com.freshorders.freshorder.template7.Template7TestActivity;
import com.freshorders.freshorder.toonline.migration.PendingDataMigrationActivity;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class SalesManOrderActivity extends BaseActivity implements IUserDetailCallBack {

    public static TextView menuIcon, textViewAssetMenuProfile, textViewAssetMenuMyOrders,
            textViewAssetMenuMyDealers, textViewAssetMenuProducts,
            textViewAssetMenuPayment, textViewAssetMenuComplaint,
            textViewAssetMenuSignout, textViewAssetMenuPaymentCollection,
            textViewAssetClientVisit, textViewNodata, textViewAssetSearch,
            textViewHeader, textViewAssetMenuCreateOrder, textViewCross, textViewAssetMenuDistanceCalculation,
            textViewAssetMenuRefresh, textViewAssetMenuStockEntry, textViewAssetMenuAddMerchant, textViewAssetMenuAcknowledge, textViewAssetMenusurveyuser, textViewAssetMenudownloadscheme, textViewAssetMenuMyDayPlan, textViewAssetMenuMySales,
            textViewAssetMenuDistStock, textViewAssetMenuStockOnly, textViewAssetMenuMigration, textViewAssetMenuStockAudit;

    public static ListView listViewOrders;
    public static int menuCliccked, selectedPosition;

    public static LinearLayout linearLayoutMenuParent, linearLayoutProfile,
            linearLayoutMyOrders, linearLayoutMyDealers, linearLayoutProducts,
            linearLayoutPayment, linearLayoutComplaint, linearLayoutClientVisit, linearLayoutAcknowledge, linearLayoutStockEntry, linearLayoutAddMerchant, linearLayoutSignout, linearlayoutSearchIcon, linearLayoutCreateOrder, linearLayoutPaymentCollection, linearLayoutSuccess, linearLayoutFailure,
            linearLayoutPending, linearLayoutRefresh, linearLayoutsurveyuser, linearLayoutdownloadscheme, linearLayoutMyDayPlan,
            linearLayoutMySales, linearLayoutDistanceCalculation,
            linearLayoutDistStock, linearLayoutStockOnly, linearLayoutMigration, linearLayoutStockAudit;

    private void showMenu(){

        MyApplication app = MyApplication.getInstance();

        if(!app.isCreateOrder()){
            linearLayoutCreateOrder.setVisibility(View.GONE);
        }
        if(!app.isProfile()){
            linearLayoutProfile.setVisibility(View.GONE);
        }
        if(!app.isMyDayPlan()){
            linearLayoutMyDayPlan.setVisibility(View.GONE);
        }
        if(!app.isMyOrders()){
            linearLayoutMyOrders.setVisibility(View.GONE);
        }
        if(!app.isAddMerchant()){
            linearLayoutAddMerchant.setVisibility(View.GONE);
        }
        if(!app.isMySales()){
            linearLayoutMySales.setVisibility(View.GONE);
        }
        if(!app.isPostNotes()){
            linearLayoutComplaint.setVisibility(View.GONE);
        }
        if(!app.isMyClientVisit()){
            linearLayoutClientVisit.setVisibility(View.GONE);
        }
        if(!app.isAcknowledge()){
            linearLayoutAcknowledge.setVisibility(View.GONE);
        }
        if(!app.isPaymentCollection()){
            linearLayoutPaymentCollection.setVisibility(View.GONE);
        }
        if(!app.isPkdDataCapture()){
            linearLayoutStockEntry.setVisibility(View.GONE);
        }
        if(!app.isDistributorStock()){
            linearLayoutDistStock.setVisibility(View.GONE);
        }
        if(!app.isSurveyUser()){
            linearLayoutsurveyuser.setVisibility(View.GONE);
        }
        if(!app.isDownLoadScheme()){
            linearLayoutdownloadscheme.setVisibility(View.GONE);
        }
        if(!app.isDistanceCalculation()){
            linearLayoutDistanceCalculation.setVisibility(View.GONE);
        }
        if(!app.isRefresh()){
            linearLayoutRefresh.setVisibility(View.GONE);
        }
        if(!app.isLogout()){
            linearLayoutSignout.setVisibility(View.GONE);
        }
        if(!app.isClosingStock()){
            linearLayoutStockOnly.setVisibility(View.GONE);
        }
        if(!app.isClosingStockAudit()){
            linearLayoutStockAudit.setVisibility(View.GONE);
        }
        if(!app.isPendingData()){
            linearLayoutMigration.setVisibility(View.GONE);
        }
    }

    public static DatabaseHandler databaseHandler;
    public static JSONObject JsonAccountObject = null;
    public static JSONArray JsonAccountArray = null;
    public static JsonServiceHandler JsonServiceHandler;
    public static ArrayList<DealerOrderListDomain> arraylistDealerOrderList, arraylistSearchResults;
    public static String companyname, fullname, date, address, orderId, morderId, PaymentStatus = "NULL", onlineorderno, Paymentdate = "NULL", salesuserid = "NULL";
    public static Date dateStr = null;
    public static String time, searchclick = "0", strMsg = "";
    private static SQLiteDatabase db;
    EditText editTextSearchField;
    public static DealerOrderListDomain domainSelected;
    public static DealerOrderListAdapter adapter;
    public static Button buttonPlaceOrder, buttonBlue, buttonYellow, buttonGreen, ButtonPending, ButtonFailure, ButtonSuccess;
    public static String Count, mname, selected_dealer, currentDate;
    public static String Dfullname, Duserid, Dcompanyname, UDID, Daddress;
    public static String prodid, prodname, prodcode, userid, pdate, pcompanyname, Porderdate, qty, fqty, Dorderdate, day,
            prodprice, prodtax, moveto = "NULL", mfgdate, expdate, batchno, uom;
    public static String Mfullname, Muserid, stockiest,route,Mcmpnyname, MerchantCount = "", DealerCount = "", ProductCount = "", SchemeCount = "", beatcount = "", dt, status,
            merchantmobileno, mflag, memail, maddress, ProductSetting = "", userSetting = "", userbeattype = "", clientDealerid = "", pan_no, strStockiest,strState,strRoute,customdata;
    public static int count = 0;
    public static ArrayList<Bitmap> arrayListimagebitmap;
    public static ArrayList<String> arraylistimagepath;
    public static Context context;
    public static String[] image;
    public static ArrayList<OutletStockEntryDomain> arrayListOutletStockSearchResults;
    public static double approximate;
    // SQLiteDatabase db;
    //uomtablestirng
    String uomid, uomprodid, uomduserid, uomname, uomvalue, uomstatus, uomupdateddt, isdefault;
    public static double approx = 0.0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.sm_order_screen);
        netCheck();
        databaseHandler = new DatabaseHandler(getApplicationContext());
        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();
        // myOrders();
        //arrayListSearchResults = new ArrayList<OutletStockEntryDomain>();
        context = SalesManOrderActivity.this;
        db = context.openOrCreateDatabase("freshorders", 0, null);
        Cursor cur;
        cur = databaseHandler.getDetails();
        cur.moveToFirst();
        Log.e("Insertion Check",
                cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_username)));
        Log.e("Insertion Check",
                cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_password)));
        Log.e("Insertion Check",
                cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_usertype)));
        clientDealerid = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_clientdealid));
        PaymentStatus = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_paymentStatus));
        arrayListimagebitmap = new ArrayList<Bitmap>();
        arraylistimagepath = new ArrayList<String>();
        linearLayoutCreateOrder = (LinearLayout) findViewById(R.id.linearLayoutCreateOrder);
        listViewOrders = (ListView) findViewById(R.id.listViewOrders);
        linearLayoutProfile = (LinearLayout) findViewById(R.id.linearLayoutProfile);
        linearLayoutMyOrders = (LinearLayout) findViewById(R.id.linearLayoutMyOrders);
        linearLayoutMyDealers = (LinearLayout) findViewById(R.id.linearLayoutMyDealers);
        linearLayoutProducts = (LinearLayout) findViewById(R.id.linearLayoutProducts);
        linearLayoutPayment = (LinearLayout) findViewById(R.id.linearLayoutPayment);
        linearLayoutComplaint = (LinearLayout) findViewById(R.id.linearLayoutComplaint);
        linearLayoutMyDayPlan = (LinearLayout) findViewById(R.id.linearLayoutMyDayPlan);
        linearLayoutMySales = (LinearLayout) findViewById(R.id.linearLayoutMySales);
        linearLayoutSignout = (LinearLayout) findViewById(R.id.linearLayoutSignout);
        linearLayoutDistanceCalculation = (LinearLayout) findViewById(R.id.linearLayoutDistanceCalculation);
        linearlayoutSearchIcon = (LinearLayout) findViewById(R.id.linearlayoutSearchIcon);
        linearLayoutSuccess = (LinearLayout) findViewById(R.id.linearLayoutSuccess);
        linearLayoutFailure = (LinearLayout) findViewById(R.id.linearLayoutFailure);
        linearLayoutPending = (LinearLayout) findViewById(R.id.linearLayoutPending);

        linearLayoutClientVisit = (LinearLayout) findViewById(R.id.linearLayoutClientVisit);
        linearLayoutAcknowledge = (LinearLayout) findViewById(R.id.linearLayoutAcknowledge);
        linearLayoutStockEntry = (LinearLayout) findViewById(R.id.linearLayoutStockEntry);
        linearLayoutAddMerchant = (LinearLayout) findViewById(R.id.linearLayoutAddMerchant);
        linearLayoutPaymentCollection = (LinearLayout) findViewById(R.id.linearLayoutPaymentCollection);

        linearLayoutdownloadscheme = (LinearLayout) findViewById(R.id.linearLayoutdownloadscheme);
        linearLayoutsurveyuser = (LinearLayout) findViewById(R.id.linearLayoutsurveyuser);

        linearLayoutRefresh = (LinearLayout) findViewById(R.id.linearLayoutRefresh);
        textViewAssetMenuRefresh = (TextView) findViewById(R.id.textViewAssetMenuRefresh);

        buttonPlaceOrder = (Button) findViewById(R.id.buttonPlaceOrder);
        buttonBlue = (Button) findViewById(R.id.buttonBlue);
        buttonYellow = (Button) findViewById(R.id.buttonYellow);
        buttonGreen = (Button) findViewById(R.id.buttonGreen);
        ButtonPending = (Button) findViewById(R.id.ButtonPending);
        ButtonFailure = (Button) findViewById(R.id.ButtonFailure);
        ButtonSuccess = (Button) findViewById(R.id.ButtonSuccess);

        if (Constants.USER_TYPE.equals("D")) {
            linearLayoutMyDealers.setVisibility(View.GONE);
            linearLayoutProducts.setVisibility(View.VISIBLE);
            linearLayoutCreateOrder.setVisibility(View.GONE);
            linearLayoutPaymentCollection.setVisibility(View.GONE);
            linearLayoutDistanceCalculation.setVisibility(View.GONE);
        } else if (Constants.USER_TYPE.equals("M")) {
            linearLayoutMyDealers.setVisibility(View.VISIBLE);
            linearLayoutProducts.setVisibility(View.GONE);
            linearLayoutCreateOrder.setVisibility(View.GONE);
            linearLayoutPaymentCollection.setVisibility(View.GONE);
            linearLayoutDistanceCalculation.setVisibility(View.GONE);
        } else {
            linearLayoutMyDealers.setVisibility(View.VISIBLE);
            linearLayoutProducts.setVisibility(View.GONE);
            linearLayoutCreateOrder.setVisibility(View.VISIBLE);
            linearLayoutDistanceCalculation.setVisibility(View.VISIBLE);
        }
        currentDate = new SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date());
        menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
        textViewAssetMenuCreateOrder = (TextView) findViewById(R.id.textViewAssetMenuCreateOrder);
        textViewAssetSearch = (TextView) findViewById(R.id.textViewAssetSearch);
        textViewHeader = (TextView) findViewById(R.id.textViewHeader);
        textViewNodata = (TextView) findViewById(R.id.textViewNodata);

        textViewAssetMenuProfile = (TextView) findViewById(R.id.textViewAssetMenuProfile);
        textViewAssetMenuMyOrders = (TextView) findViewById(R.id.textViewAssetMenuMyOrders);
        textViewAssetMenuMyDealers = (TextView) findViewById(R.id.textViewAssetMenuMyDealers);
        textViewAssetMenuProducts = (TextView) findViewById(R.id.textViewAssetMenuProducts);
        textViewAssetMenuPayment = (TextView) findViewById(R.id.textViewAssetMenuPayment);
        textViewAssetMenuComplaint = (TextView) findViewById(R.id.textViewAssetMenuComplaint);
        textViewAssetMenuSignout = (TextView) findViewById(R.id.textViewAssetMenuSignout);
        textViewAssetMenuDistanceCalculation = (TextView) findViewById(R.id.textViewAssetMenuDistanceCalculation);
        textViewAssetClientVisit = (TextView) findViewById(R.id.textViewAssetClientVisit);
        textViewAssetMenuAddMerchant = (TextView) findViewById(R.id.textViewAssetMenuAddMerchant);
        textViewAssetMenuAcknowledge = (TextView) findViewById(R.id.textViewAssetMenuAcknowledge);
        textViewAssetMenuStockEntry = (TextView) findViewById(R.id.textViewAssetMenuStockEntry);
        textViewAssetMenuStockOnly = (TextView) findViewById(R.id.textViewAssetMenuStockOnly);
        textViewAssetMenuStockAudit = (TextView) findViewById(R.id.textViewAssetMenuStockAudit);
        textViewAssetMenuMigration = findViewById(R.id.textViewAssetMenuMigration);

        textViewAssetMenudownloadscheme = (TextView) findViewById(R.id.textViewAssetMenudownloadscheme);
        textViewAssetMenusurveyuser = (TextView) findViewById(R.id.textViewAssetMenusurveyuser);

        textViewAssetMenuMyDayPlan = (TextView) findViewById(R.id.textViewAssetMenuMyDayPlan);
        textViewAssetMenuMySales = (TextView) findViewById(R.id.textViewAssetMenuMySales);

        editTextSearchField = (EditText) findViewById(R.id.editTextSearchField);
        textViewCross = (TextView) findViewById(R.id.textViewCross);

        textViewAssetMenuPaymentCollection = (TextView) findViewById(R.id.textViewAssetMenuPaymentCollection);
        linearLayoutMenuParent = (LinearLayout) findViewById(R.id.linearLayoutMenuParent);
        Typeface font = Typeface.createFromAsset(getAssets(),
                "fontawesome-webfont.ttf");
        menuIcon.setTypeface(font);

        textViewAssetMenuCreateOrder.setTypeface(font);
        textViewAssetSearch.setTypeface(font);
        textViewAssetMenuProfile.setTypeface(font);
        textViewAssetMenuMyOrders.setTypeface(font);
        textViewAssetMenuMyDealers.setTypeface(font);
        textViewAssetMenuProducts.setTypeface(font);
        textViewAssetMenuPayment.setTypeface(font);
        textViewAssetMenuComplaint.setTypeface(font);
        textViewAssetMenuSignout.setTypeface(font);
        textViewAssetClientVisit.setTypeface(font);
        textViewAssetMenuDistanceCalculation.setTypeface(font);
        textViewAssetMenuPaymentCollection.setTypeface(font);
        textViewCross.setTypeface(font);
        textViewAssetMenuRefresh.setTypeface(font);
        textViewAssetMenuMigration.setTypeface(font);

        textViewAssetMenuAcknowledge.setTypeface(font);
        textViewAssetMenuStockEntry.setTypeface(font);
        textViewAssetMenuAddMerchant.setTypeface(font);

        textViewAssetMenusurveyuser.setTypeface(font);
        textViewAssetMenudownloadscheme.setTypeface(font);
        textViewAssetMenuMyDayPlan.setTypeface(font);
        textViewAssetMenuMySales.setTypeface(font);

        linearLayoutMyDealers.setVisibility(View.GONE);

        linearLayoutDistStock = (LinearLayout) findViewById(R.id.linearLayoutDistStock);
        textViewAssetMenuDistStock = (TextView) findViewById(R.id.textViewAssetMenuDistStock);
        textViewAssetMenuDistStock.setTypeface(font);
        textViewAssetMenuStockOnly.setTypeface(font);
        textViewAssetMenuStockAudit.setTypeface(font);
        linearLayoutStockOnly = (LinearLayout) findViewById(R.id.linearLayoutStockOnly);
        linearLayoutStockAudit = findViewById(R.id.linearLayoutStockAudit);

        linearLayoutMigration = (LinearLayout) findViewById(R.id.linearLayoutMigration);
        linearLayoutMigration.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(true) {
                    Intent io = new Intent(SalesManOrderActivity.this,
                            PendingDataMigrationActivity.class);
                    startActivity(io);
                }else {
                    showAlertDialogToast(getResources().getString(R.string.migration_net_alert));
                }
            }
        });


        initialcount();
        Cursor cursor;
        cursor = databaseHandler.getdealer();
        Log.e("count", String.valueOf(cursor.getCount()));
        if (cursor.getCount() == 0) {
            Log.e("count", String.valueOf(cursor.getCount()));
            moveto = "sync";
            getData();


        } else {

            Log.e("count", "MyOrdersList" +
                    "");
            MyOrdersList(Constants.orderstatus);
        }

        if (Constants.orderstatus.equals("Success")) {
            buttonBlue.setBackgroundColor(Color.parseColor("#0768AA"));
            buttonYellow.setBackgroundColor(Color.parseColor("#ecce03"));
            buttonGreen.setBackgroundColor(Color.parseColor("#b1d36f"));
        } else if (Constants.orderstatus.equals("Pending")) {
            buttonBlue.setBackgroundColor(Color.parseColor("#03acec"));
            buttonYellow.setBackgroundColor(Color.parseColor("#ecce03"));
            buttonGreen.setBackgroundColor(Color.parseColor("#84A544"));
        }

        ButtonSuccess.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Log.e("linearLayoutSuccess", "linearLayoutSuccess");
                buttonBlue.setBackgroundColor(Color.parseColor("#0768AA"));
                buttonYellow.setBackgroundColor(Color.parseColor("#ecce03"));
                buttonGreen.setBackgroundColor(Color.parseColor("#b1d36f"));
                MyOrdersList("Success");


            }
        });
        ButtonFailure.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Log.e("linearLayoutFailure", "linearLayoutFailure");
                buttonBlue.setBackgroundColor(Color.parseColor("#03acec"));
                buttonYellow.setBackgroundColor(Color.parseColor("#D3DD00"));
                buttonGreen.setBackgroundColor(Color.parseColor("#b1d36f"));
                MyOrdersList("Failed");

            }
        });
        ButtonPending.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Log.e("linearLayoutFailure", "linearLayoutFailure");
                buttonBlue.setBackgroundColor(Color.parseColor("#03acec"));
                buttonYellow.setBackgroundColor(Color.parseColor("#ecce03"));
                buttonGreen.setBackgroundColor(Color.parseColor("#84A544"));
                MyOrdersList("Pending");

            }
        });

        buttonPlaceOrder.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("paystatus", PaymentStatus);
                MyApplication.getInstance().setTemplate8ForTemplate2(false);

                if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                    showAlertDialogToast("Please make payment before place order");
                } else {
                    Constants.checkproduct = 0;
                    Constants.orderid = "";
                    Constants.SalesMerchant_Id = "";
                    Constants.Merchantname = "";
                    Intent to;
                    if(Constants.TEMPLATE_NO.equals(Constants.TEMPLATE7)) { ///Testing Template
                        to = new Intent(SalesManOrderActivity.this,
                                Template7TestActivity.class);
                    }else {
                        to = new Intent(SalesManOrderActivity.this,
                                CreateOrderActivity.class);
                    }
                    startActivity(to);
                    finish();
                }


            }
        });

        linearlayoutSearchIcon.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (searchclick == "0") {
                    textViewHeader.setVisibility(TextView.GONE);
                    editTextSearchField.setVisibility(EditText.VISIBLE);
                    editTextSearchField.setFocusable(true);
                    editTextSearchField.requestFocus();
                    textViewCross.setVisibility(TextView.VISIBLE);
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    textViewCross.setVisibility(TextView.VISIBLE);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                    Log.e("Keyboard", "Show");
                    searchclick = "1";
                } else {
                    textViewHeader.setVisibility(TextView.VISIBLE);
                    editTextSearchField.setVisibility(EditText.GONE);
                    textViewCross.setVisibility(TextView.GONE);
                    textViewCross.setVisibility(TextView.GONE);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(linearlayoutSearchIcon.getWindowToken(), 0);
                    searchclick = "0";
                }

            }
        });

        editTextSearchField.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                arraylistDealerOrderList = new ArrayList<DealerOrderListDomain>();
                String searchText = editTextSearchField.getText().toString().toLowerCase(Locale.getDefault());
                for (DealerOrderListDomain pd : arraylistSearchResults) {
                    if (pd.getComName().toLowerCase(Locale.getDefault()).contains(searchText)) {
                        arraylistDealerOrderList.add(pd);
                    }
                }
                SalesManOrderActivity.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        adapter = new DealerOrderListAdapter(SalesManOrderActivity.this, R.layout.item_dealer_order_screen, arraylistDealerOrderList);
                        listViewOrders.setVisibility(View.VISIBLE);
                        listViewOrders.setAdapter(adapter);
                    }
                });

            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                textViewCross.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        editTextSearchField.setText("");

                        if (editTextSearchField.equals("")) {
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                        }

                    }
                });

            }
        });

        menuIcon.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (menuCliccked == 0) {
                    linearLayoutMenuParent.setVisibility(View.VISIBLE);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(menuIcon.getWindowToken(), 0);
                    editTextSearchField.setVisibility(EditText.GONE);
                    textViewHeader.setVisibility(TextView.VISIBLE);
                    textViewCross.setVisibility(TextView.GONE);
                    menuCliccked = 1;
                } else {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    textViewHeader.setVisibility(TextView.VISIBLE);
                    editTextSearchField.setVisibility(EditText.GONE);
                    textViewCross.setVisibility(TextView.GONE);
                    menuCliccked = 0;
                }

            }
        });


        listViewOrders.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                domainSelected = arraylistDealerOrderList.get(position);
                companyname = arraylistDealerOrderList.get(position).getFullname();
                fullname = arraylistDealerOrderList.get(position).getComName();
                date = arraylistDealerOrderList.get(position).getDate();
                address = arraylistDealerOrderList.get(position).getAddress();
                orderId = arraylistDealerOrderList.get(position).getOrderid();
                onlineorderno = arraylistDealerOrderList.get(position).getAddress();
                morderId = arraylistDealerOrderList.get(position).getMuserid();
                mname = arraylistDealerOrderList.get(position).getMname();
                approx = arraylistDealerOrderList.get(position).getapproximate();
                Constants.approximate = arraylistDealerOrderList.get(position).getapproximate();
                Log.e("appropriate", String.valueOf(approx));


                if (arraylistDealerOrderList.get(position).getorderimage().equals("0")) {
                    image = new String[0];
                } else {

                    image = arraylistDealerOrderList.get(position).getorderimage().split(",");
                }

                //
                Constants.serialno = arraylistDealerOrderList.get(position).getSerialno();
                Log.e("serialno", Constants.serialno);

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(listViewOrders.getWindowToken(), 0);
                Intent to = new Intent(SalesManOrderActivity.this, SalesmanOrderDetailsActivity.class);
                startActivity(to);
                finish();
                /*selectedPosition = position;
                listViewOrders.setAdapter(adapter);
                listViewOrders.setSelectionFromTop(position, 1);*/
            }
        });
        menuIcon.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (menuCliccked == 0) {
                    linearLayoutMenuParent.setVisibility(View.VISIBLE);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(menuIcon.getWindowToken(), 0);
                    editTextSearchField.setVisibility(EditText.GONE);
                    textViewHeader.setVisibility(TextView.VISIBLE);
                    textViewCross.setVisibility(TextView.GONE);
                    menuCliccked = 1;
                } else {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    textViewHeader.setVisibility(TextView.VISIBLE);
                    editTextSearchField.setVisibility(EditText.GONE);
                    textViewCross.setVisibility(TextView.GONE);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(menuIcon.getWindowToken(), 0);
                    menuCliccked = 0;
                }

            }
        });

        linearLayoutMenuParent.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
            }
        });

        linearLayoutProfile.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheck()) {
                    getDetails("profile");

                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });


        linearLayoutRefresh.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("Clicked", "Clicked");

                if (netCheck()) {

                    final Dialog qtyDialog = new Dialog(SalesManOrderActivity.this);
                    qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    qtyDialog.setContentView(R.layout.warning_dialog);

                    final Button buttonUpdateOk, buttonUpdateCancel;

                    final TextView textQtyValidate;

                    buttonUpdateOk = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateOk);
                    buttonUpdateCancel = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateCancel);

                    textQtyValidate = (TextView) qtyDialog
                            .findViewById(R.id.textQtyValidate);
                    qtyDialog.show();


                    buttonUpdateOk.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            databaseHandler.synTableDelete();
                            Constants.refreshscreen = "myorder";
                            Intent io = new Intent(SalesManOrderActivity.this, RefreshActivity.class);
                            startActivity(io);
                            finish();
                            qtyDialog.dismiss();
                        }

                    });

                    buttonUpdateCancel
                            .setOnClickListener(new OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub

                                    qtyDialog.dismiss();

                                }
                            });

                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }
            }

        });


        linearLayoutMyOrders.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Constants.orderstatus = "Success";
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;

            }
        });

        linearLayoutMyDealers.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheck() == true) {
                    getDetails("mydealer");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(SalesManOrderActivity.this,
                                MyDealersActivity.class);
                        io.putExtra("Key", "menuclick");
                        startActivity(io);
                        finish();

                    }
                }

            }
        });


        linearLayoutComplaint.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheck() == true) {
                    Intent io = new Intent(SalesManOrderActivity.this,
                            MerchantComplaintActivity.class);
                    startActivity(io);
                    finish();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(SalesManOrderActivity.this, MerchantComplaintActivity.class);
                        startActivity(io);
                        finish();
                        /*if (Constants.USER_TYPE.equals("D")) {
                            Intent io = new Intent(SalesManOrderActivity.this,
                                    DealersComplaintActivity.class);
                            startActivity(io);
                            finish();

                        } else {
                            Intent io = new Intent(SalesManOrderActivity.this,
                                    MerchantComplaintActivity.class);
                            startActivity(io);
                            finish();

                        } */
                    }
                }


            }
        });

        linearLayoutDistanceCalculation.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Intent io = new Intent(SalesManOrderActivity.this,
                            DistanceCalActivity.class);//////////////////////////////////////////////////////////////////////////
                    startActivity(io);
                    finish();



            }
        });

        linearLayoutCreateOrder.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                MyApplication.getInstance().setTemplate8ForTemplate2(false);
                Constants.checkproduct = 0;
                Constants.orderid = "";
                Constants.SalesMerchant_Id = "";
                Constants.Merchantname = "";

                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheck() == true) {
                    getDetails("Createorder");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(SalesManOrderActivity.this,
                                CreateOrderActivity.class);
                        startActivity(io);
                        finish();
                    }
                }

            }
        });


        linearLayoutStockOnly.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().setTemplate8ForTemplate2(true);
                Constants.checkproduct = 0;
                Constants.orderid = "";
                Constants.SalesMerchant_Id = "";
                Constants.Merchantname = "";
                if (netCheck()) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    //getDetails("Createorder");
                    Intent io = new Intent(SalesManOrderActivity.this,
                            ClosingStockDashBoardActivity.class);
                    startActivity(io);
                    finish();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals("") || PaymentStatus.equalsIgnoreCase("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(SalesManOrderActivity.this,
                                ClosingStockDashBoardActivity.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });


        linearLayoutStockAudit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().setTemplate8ForTemplate2(false);
                Constants.checkproduct = 0;
                Constants.orderid = "";
                Constants.SalesMerchant_Id = "";
                Constants.Merchantname = "";
                if (netCheck()) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    //getDetails("Createorder");
                    Intent io = new Intent(SalesManOrderActivity.this,
                            ClosingStockAudit.class);
                    startActivity(io);
                    finish();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals("") || PaymentStatus.equalsIgnoreCase("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(SalesManOrderActivity.this,
                                ClosingStockAudit.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });



        linearLayoutClientVisit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheck()) {
                    getDetails("clientvisit");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent to = new Intent(SalesManOrderActivity.this,
                                SMClientVisitHistory.class);
                        startActivity(to);
                        finish();
                    }
                }

            }
        });
        linearLayoutAddMerchant.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheck()) {
                    getDetails("addmerchant");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent to = new Intent(SalesManOrderActivity.this,
                                AddMerchantNew.class);
                        startActivity(to);
                        finish();
                    }
                }

            }
        });
        linearLayoutAcknowledge.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheck()) {
                    getDetails("acknowledge");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent to = new Intent(SalesManOrderActivity.this,
                                SalesmanAcknowledgeActivity.class);
                        startActivity(to);
                        finish();
                    }
                }

            }
        });
        linearLayoutStockEntry.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheck()) {
                    getDetails("stockentry");

                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment");
                    }else {
                        Intent io = new Intent(SalesManOrderActivity.this,
                                OutletStockEntry.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });

        linearLayoutDistStock.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;

                if (netCheck()) {
                    getDetails("diststock");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment");
                    } else {
                        Intent io = new Intent(SalesManOrderActivity.this,
                                DistributrStockActivity.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });

        linearLayoutPaymentCollection.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheck()) {
                    getDetails("paymentcoll");
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }


            }
        });

        linearLayoutdownloadscheme.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck()) {

                    final Dialog qtyDialog = new Dialog(SalesManOrderActivity.this);
                    qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    qtyDialog.setContentView(R.layout.warning_dialog);

                    final Button buttonUpdateOk, buttonUpdateCancel;

                    final TextView textQtyValidate;

                    buttonUpdateOk = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateOk);
                    buttonUpdateCancel = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateCancel);

                    textQtyValidate = (TextView) qtyDialog
                            .findViewById(R.id.textQtyValidate);
                    qtyDialog.show();


                    buttonUpdateOk.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            databaseHandler.deletescheme();
                            Constants.downloadScheme = "myorder";
                            Intent io = new Intent(SalesManOrderActivity.this, SchemeDownload.class);
                            startActivity(io);
                            finish();
                            qtyDialog.dismiss();
                        }

                    });

                    buttonUpdateCancel
                            .setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub

                                    qtyDialog.dismiss();

                                }
                            });

                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });
        linearLayoutsurveyuser.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheck()) {
                    getDetails("surveyuser");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent to = new Intent(SalesManOrderActivity.this, SurveyMerchant.class);
                        startActivity(to);
                        finish();
                    }
                }

            }
        });

        linearLayoutMyDayPlan.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (netCheck()) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    getDetails("mydayplan");
                }else {
                    showAlertDialogToast("Please Check Your internet connection");
                }


            }
        });
        linearLayoutMySales.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheck() == true) {
                    getDetails("mysales");
                }else {
                    showAlertDialogToast("Please Check Your internet connection");
                }


            }
        });


        linearLayoutSignout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                // TODO Auto-generated method stub
                databaseHandler.delete();
                Intent io = new Intent(SalesManOrderActivity.this,
                        SigninActivity.class);
                startActivity(io);
                finish();


            }
        });

        /////////////////////////
        showMenu();
        ///////////////////////
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (Constants.DUSER_ID == null || Constants.DUSER_ID.isEmpty()) {
            if(databaseHandler == null) {
                databaseHandler = new DatabaseHandler(getApplicationContext());
            }
            Cursor curs;
            curs = databaseHandler.getdealer();
            if(curs != null && curs.getCount() > 0) {
                curs.moveToFirst();
                Constants.DUSER_ID = curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_Duserid));
                Log.e("Constants.DUSER_ID", Constants.DUSER_ID);
                curs.close();
            }else {
                //showAlertDialogToast(getResources().getString(R.string.dealer_not_proper_load));
            }
        }
        if (netCheck() == true) {
            String strUrl = Utils.strCustomFieldMerchantURL + Constants.DUSER_ID;
            Log.e("strUrlelse SmOact", strUrl);
            loadCustomFields(strUrl);
        }
        else
        {
            if( Constants.jsonArCustomFieldsMerchant ==null)
            {
                try {
                    String strMerchantCustomFields = databaseHandler.getConstantValue("MERCHANTCUSTOMFIELDS");
                    Constants.jsonArCustomFieldsMerchant = new JSONArray(strMerchantCustomFields);
                }catch (Exception ex)
                {
                    ex.printStackTrace();
                }
            }
        }

        Log.e("jsonArCFMerchant:", "" + Constants.jsonArCustomFieldsMerchant);
    }

    public  void outletStockEntry(){
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "",strMsg="";



            @Override
            protected void onPreExecute() {

                dialog = ProgressDialog.show(SalesManOrderActivity.this, "",
                        "Loading ...", true,false);

            }

            @Override
            protected void onPostExecute(Void result) {

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {
                        databaseHandler.insertOutletStockEntry(JsonAccountObject.toString());
                    }
                    if(dialog.isShowing()){
                        dialog.dismiss();
                        getSurveyMerchant();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {

                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("userid", Constants.USER_ID);
                    jsonObject.put("status","Active");

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                JsonServiceHandler = new JsonServiceHandler(Utils.strofflineproduct, jsonObject.toString(), SalesManOrderActivity.this);
                JsonAccountObject = JsonServiceHandler.ServiceData();

                return null;

            }

        }.execute(new Void[]{});

    }

    private void loadCustomFields(final String url) {
        RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("url",url);
                try{
                    JSONObject jsonObject= null;
                    try {
                        jsonObject = new JSONObject(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if(jsonObject.getString("status").equalsIgnoreCase("true")){
                        Constants.jsonArCustomFieldsMerchant=jsonObject.getJSONArray("data");
                        databaseHandler.addConstant("MERCHANTCUSTOMFIELDS",""+Constants.jsonArCustomFieldsMerchant);
                    }
                }catch (JSONException e){e.printStackTrace();}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void getDetails(final String paymentcheck) {
        if(paymentcheck.equals("profile")){
            Intent io = new Intent(SalesManOrderActivity.this,
                    ProfileActivity.class);
            startActivity(io);
            finish();

        }
        else if(paymentcheck.equals("myorders")){
            Intent io = new Intent(SalesManOrderActivity.this,
                    SalesmanAcknowledgeActivity.class);
            startActivity(io);
            finish();

        }else if(paymentcheck.equals("mydealer")){
            Intent io = new Intent(SalesManOrderActivity.this,
                    MyDealersActivity.class);
            io.putExtra("Key","menuclick");
            startActivity(io);
            finish();

        }else if(paymentcheck.equals("postnotes")){
            Intent io = new Intent(SalesManOrderActivity.this,
                    MerchantComplaintActivity.class);
            startActivity(io);
            finish();

        } else if(paymentcheck.equals("placeorder")){
            Intent to = new Intent(SalesManOrderActivity.this,
                    SalesManOrderCheckoutActivity.class);
            startActivity(to);
            finish();
        }
        else if(paymentcheck.equals("clientvisit")){
            Intent to = new Intent(SalesManOrderActivity.this,
                    SMClientVisitHistory.class);
            startActivity(to);
            finish();
        }
        else if(paymentcheck.equals("paymentcoll")){
            Intent to = new Intent(SalesManOrderActivity.this,
                    SalesmanPaymentCollectionActivity.class);
            startActivity(to);
            finish();
        }
        else if(paymentcheck.equals("Createorder")){
            Intent to = new Intent(SalesManOrderActivity.this,
                    CreateOrderActivity.class);
            startActivity(to);
            finish();
        } else if(paymentcheck.equals("addmerchant")){
            Intent to = new Intent(SalesManOrderActivity.this,
                    AddMerchantNew.class);
            startActivity(to);
            finish();
        } else if(paymentcheck.equals("acknowledge")){
            Intent to = new Intent(SalesManOrderActivity.this,
                    SalesmanAcknowledgeActivity.class);
            startActivity(to);
            finish();
        } else if(paymentcheck.equals("stockentry")){
            Intent to = new Intent(SalesManOrderActivity.this,
                    OutletStockEntry.class);
            startActivity(to);
            finish();
        }
        else if(paymentcheck.equals("surveyuser")){
            Intent to = new Intent(SalesManOrderActivity.this,
                    SurveyMerchant.class);
            startActivity(to);
            finish();
        }
        else if(paymentcheck.equals("mydayplan")){
            Intent to = new Intent(SalesManOrderActivity.this,
                    MyDayPlan.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("diststock")) {
            Intent io = new Intent(SalesManOrderActivity.this,
                    DistributrStockActivity.class);
            startActivity(io);
            finish();
        }else if (paymentcheck.equals("mysales")) {
            Intent io = new Intent(SalesManOrderActivity.this, MySalesActivity.class);
            startActivity(io);
            finish();
        }
    }
    public   void getData() {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "";

            @Override
            protected void onPreExecute() {
                dialog= ProgressDialog.show(context, "",
                        "Loading...", true, true);

            }

            @Override
            protected void onPostExecute(Void result) {

                try {

                    PaymentStatus=(JsonAccountObject.getString("pymtstatus"));
                    //Paymentdate=JsonAccountObject.getString("nextpymtdt");
                    salesuserid=JsonAccountObject.getString("userid");
                    Log.e("PaymentStatus",PaymentStatus);

                    if(PaymentStatus.equals("Pending")||PaymentStatus.equals(null) ||PaymentStatus.equals("")||PaymentStatus.equals("null")){
                        showAlertDialogToast("Please make payment to place order");
                    }else {
                        if(moveto.equals("sync")) {
                            Paymentdate="hiiii";
                            ContentValues contentValues = new ContentValues();
                            contentValues.put(DatabaseHandler.KEY_id, salesuserid);
                            contentValues.put(DatabaseHandler.KEY_paymentdate, Paymentdate);
                            databaseHandler.updatesignintable(contentValues);
                            Cursor curs;
                            curs=databaseHandler.getDetails();
                            Log.e("countsignin", String.valueOf(curs.getCount()));
                            if(curs!=null && curs.getCount() > 0) {
                                if (curs.moveToFirst()) {

                                    Log.e("nextpayment", String.valueOf(curs.getString(8)));
                                }
                            }
                            offlineDealers();
                        }

                    }


                    dialog.dismiss();

                }catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();


                JsonServiceHandler = new JsonServiceHandler(Utils.strGetProfileDetail+Constants.USER_ID, context);
                JsonAccountObject = JsonServiceHandler.ServiceDataGet();
                return null;
            }
        }.execute(new Void[]{});
    }


    private ProgressDialog mProgressDialog;

    private void show(){
        if(mProgressDialog == null){
            mProgressDialog = ProgressDialog.show(SalesManOrderActivity.this, "",
                    "Initializing Product Setting table ...", true, true);
        }
    }

    private void dismissDialog(){
        if(mProgressDialog != null &&
                mProgressDialog.isShowing()){
            mProgressDialog.dismiss();
        }
    }

    @Override
    public void userResponse(JSONObject jsonObject) {
        if(jsonObject != null){
            try {
                if(jsonObject.getString("status").equalsIgnoreCase("true")){
                    Constants.jsonArCustomFieldsMerchant=jsonObject.getJSONArray("data");
                    databaseHandler.addConstant("MERCHANTCUSTOMFIELDS",""+Constants.jsonArCustomFieldsMerchant);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        dismissDialog();

        ///////////////////////////////////05-08-2019 Kumaravel
        if(databaseHandler == null){
            databaseHandler = new DatabaseHandler(SalesManOrderActivity.this);
        }
        String type = databaseHandler.getSalesmanDataUserSetting("BEAT_TYPE");
        if(!type.isEmpty()){
            Log.e("DistributorLoad",".....................type " + type);
            if(type.equalsIgnoreCase("C")){
                DistributorLoad();
            }else {
                offlineProductSetting();
            }
        }
        ////////////////////////////////////////////


    }

    public void getSurveyMerchant(){
        show();
        Log.d("SalesManOrder", "getSyncData in ......");
        final UserDetailTask userDetail = new UserDetailTask(Utils.strCustomFieldMerchantURL + Constants.DUSER_ID,
                null, SalesManOrderActivity.this, this);
        userDetail.execute();
    }

    private void DistributorLoad() {
        Log.e("DistributorLoad","DistributorLoad");
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "";

            @Override
            protected void onPreExecute() {
                dialog = ProgressDialog.show(SalesManOrderActivity.this, "",
                        "Distributor Loading...", true, false);

            }

            @Override
            protected void onPostExecute(Void result) {

                try {
                    if(JsonAccountObject != null) {
                        strStatus = JsonAccountObject.getString("status");
                        Log.e("return status", strStatus);
                        strMsg = JsonAccountObject.getString("message");
                        Log.e("return message", strMsg);
                        if (strStatus.equals("true")) {
                            JSONArray distributorArray = JsonAccountObject.getJSONArray("data");
                            if(distributorArray.length() > 0){
                                List<ContentValues> listCV = new ArrayList<>();
                                for(int i = 0; i < distributorArray.length(); i++){
                                    ContentValues cv = new ContentValues();
                                    JSONObject obj = distributorArray.getJSONObject(i);
                                    String distributorId = obj.getString("distributorid");
                                    String distributorName = obj.getString("distributorname");
                                    String mobileNo = obj.getString("mobileno");
                                    cv.put(DatabaseHandler.KEY_DISTRIBUTOR_ID, distributorId);
                                    cv.put(DatabaseHandler.KEY_DISTRIBUTOR_NAME, distributorName);
                                    cv.put(DatabaseHandler.KEY_DISTRIBUTOR_MOBILE, mobileNo);
                                    listCV.add(cv);
                                }
                                databaseHandler.loadDistributor(listCV);
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if(dialog != null) {
                        dialog.dismiss();
                        offlineProductSetting();///////////////// 05-08-2019 Kumaravel
                    }
                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();
                try {
                    String suserId = "";
                    if(databaseHandler == null){
                        databaseHandler = new DatabaseHandler(SalesManOrderActivity.this);
                    }
                    Cursor cur = databaseHandler.getDetails();
                    if(cur != null && cur.getCount() > 0){
                        cur.moveToFirst();
                        suserId = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_id));
                        cur.close();
                    }


                    jsonObject.put("suserid",suserId);
                    Log.e("suserid-----",suserId);

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                JsonServiceHandler = new JsonServiceHandler(Utils.strDistributor, jsonObject.toString(),SalesManOrderActivity.this);
                JsonAccountObject = JsonServiceHandler.ServiceData();
                return null;
            }
        }.execute(new Void[]{});
    }

    public   void offlineProductSetting() {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            String strStatus = "";

            ProgressDialog dialog;
            @Override
            protected void onPreExecute() {
                dialog= ProgressDialog.show(context, "",
                        "Initializing Product Setting table ...", true, true);
                dialog.setCancelable(false);
            }


            @Override
            protected void onPostExecute(Void result) {

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {


                        JSONArray job1 = JsonAccountObject.getJSONArray("data");


                        for (int i = 0; i < job1.length(); i++) {

                            JSONObject job = new JSONObject();
                            job = job1.getJSONObject(i);
                            String prodid=job.getString("prodid");
                            String refkey = job.getString("refkey");
                            String refvalue =job.getString("refvalue");
                            String muserid=job.getString("muserid");
                            String status=job.getString("status");
                            String updateddate=job.getString("updateddt");

                            databaseHandler.addProductStting(prodid, refkey, refvalue, muserid, status,updateddate,"");

                        }
                        Cursor cur;
                        cur = databaseHandler.getProductSetting();
                        Log.e("ProductSetting", String.valueOf(cur.getCount()));
                        ProductSetting = String.valueOf(cur.getCount());



                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {

                }
                dialog.dismiss();

                if(ProductCount.equals("null")||ProductCount.isEmpty()){
                    ProductCount="0";
                }
                if(MerchantCount.equals("null")||MerchantCount.isEmpty()){
                    MerchantCount="0";
                }
                if(SchemeCount.equals("null")||SchemeCount.isEmpty()){
                    SchemeCount="0";
                }
                if(beatcount.equals("null")||beatcount.isEmpty()){
                    beatcount="0";
                }

                Log.e("productcount",ProductCount);
                showAlertDialogToast("Stockiest/Dealer Count :" + DealerCount + "\n" + "Product Count :" + ProductCount + "\n" + "Outlet/Merchant Count :" + MerchantCount + "\n"+ "Scheme Count :" + SchemeCount);
                MyOrdersList("Success");
                Constants.syn = 1;

                dialog.dismiss();
            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();


                JsonServiceHandler = new JsonServiceHandler(Utils.getProductSetting,context);
                JsonAccountObject = JsonServiceHandler.ServiceDataGet();
                return null;
            }
        }.execute(new Void[]{});

    }
    public void productuom(){
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";

            @Override
            protected void onPreExecute() {

                dialog = ProgressDialog.show(context, "",
                        "Initializing Products UOM table ...", true,true);
                dialog.setCancelable(false);
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
                if (!dialog.isShowing()){
                    outletStockEntry();
                }
            }

            @Override
            protected Void doInBackground(Void... params) {

                JsonServiceHandler = new JsonServiceHandler(Utils.strproductuom+selected_dealer,context);
                JsonAccountObject = JsonServiceHandler.ServiceDataGet();

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {

                        JSONArray job1 = JsonAccountObject.getJSONArray("data");
                        count=job1.length();

                        for (int i = 0; i < job1.length(); i++) {

                            JSONObject job = new JSONObject();

                            try {
                                job = job1.getJSONObject(i);

                                uomid = job.getString("uomid");
                                uomprodid = job.getString("prodid");
                                uomduserid = job.getString("duserid");
                                uomname = job.getString("uomname");
                                uomupdateddt = job.getString("updateddt");
                                uomstatus = job.getString("status");
                                uomvalue = job.getString("uomvalue");
                                isdefault = job.getString("isdefault");
                                Log.e("isdefault-----",isdefault);
                                Log.e("uom",uomname);

                                databaseHandler.addProductUOM(uomid, uomprodid, uomduserid, uomname, uomvalue, uomupdateddt,uomstatus,isdefault);

                            }catch (JSONException e) {
                                e.printStackTrace();}
                            catch (Exception e) {

                            }

                        }
                        Cursor cur;
                        cur = databaseHandler.getProductUOM();
                        // ProductCount= String.valueOf(cur.getCount());
                        Log.e("productuomCount", String.valueOf(cur.getCount()));

                    }else{

                    }

                    dialog.dismiss();

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {

                }

                return null;

            }

        }.execute(new Void[]{});

    }

    public   void offlineUserSetting() {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            String strStatus = "";

            ProgressDialog dialog;
            @Override
            protected void onPreExecute() {
                dialog= ProgressDialog.show(context, "",
                        "Initializing User Setting table ...", true, true);
                dialog.setCancelable(false);
            }


            @Override
            protected void onPostExecute(Void result) {

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {


                        JSONArray job1 = JsonAccountObject.getJSONArray("data");


                        for (int i = 0; i < job1.length(); i++) {

                            JSONObject job = new JSONObject();
                            job = job1.getJSONObject(i);
                            String userid=job.getString("userid");
                            String refkey = job.getString("refkey");
                            String refvalue =job.getString("refvalue");
                            userbeattype=job.getString("refvalue");
                            String remarks=job.getString("remarks");
                            String status=job.getString("status");
                            Log.e("status","status");
                            String updateddate=job.getString("updateddt");

                            databaseHandler.addUserSetting(userid, refkey, refvalue, updateddate, status,remarks);
                        }
                        Cursor cur;
                        cur = databaseHandler.getUserSetting();
                        Log.e("UserSetting", String.valueOf(cur.getCount()));
                        userSetting = String.valueOf(cur.getCount());
                        cur.moveToFirst();
                        if(cur!=null && cur.getCount() > 0) {
                            if (cur.moveToLast()) {

                                Log.e("SettingModel", String.valueOf(cur.getString(3)));
                            }
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {

                }
                dialog.dismiss();
                if(userbeattype.equalsIgnoreCase("C")){
                    offlineCompanyModelBeat();
                }else{
                    offlineDistributorModelBeat();
                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();


                JsonServiceHandler = new JsonServiceHandler(Utils.getUserSetting+clientDealerid,context);
                JsonAccountObject = JsonServiceHandler.ServiceDataGet();
                return null;
            }
        }.execute(new Void[]{});

    }
    public   void offlineDealers() {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            String strStatus = "";

            ProgressDialog dialog;
            @Override
            protected void onPreExecute() {
                dialog= ProgressDialog.show(context, "",
                        "Initializing Dealer table ...", true, true);
                dialog.setCancelable(false);
            }


            @Override
            protected void onPostExecute(Void result) {

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {


                        JSONArray job1 = JsonAccountObject.getJSONArray("data");


                        for (int i = 0; i < job1.length(); i++) {

                            JSONObject job = new JSONObject();
                            job = job1.getJSONObject(i);
                            UDID=job.getString("usrdlrid");
                            Duserid = job.getJSONObject("D").getString("userid");
                            Dfullname =job.getJSONObject("D").getString("fullname");
                            Dcompanyname=job.getJSONObject("D").getString("companyname");
                            Daddress=job.getJSONObject("D").getString("address");


                            String inputFormat = new SimpleDateFormat(
                                    "yyyy-MM-dd HH:mm:ss").format(new Date());
                            Log.e("inputFormat",inputFormat);

                            databaseHandler.adddealer(Dcompanyname, UDID, Duserid, Dfullname, Daddress,inputFormat);


                        }
                        Cursor cur;
                        cur = databaseHandler.getdealer();
                        Log.e("dealercount", String.valueOf(cur.getCount()));
                        DealerCount = String.valueOf(cur.getCount());

                        if(cur!=null && cur.getCount() > 0) {
                            if (cur.moveToLast()) {

                                Log.e("Ddate", String.valueOf(cur.getString(6)));
                            }
                        }
                        dialog.dismiss();
                        OfflineProducts();


                    }else{
                        dialog.dismiss();
                        listViewOrders.setVisibility(View.GONE);
                        textViewNodata.setVisibility(View.VISIBLE);
                        textViewNodata.setText("No Items");

                        linearlayoutSearchIcon.setClickable(false);
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {

                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();


                JsonServiceHandler = new JsonServiceHandler(Utils.strofflineDealerList + Constants.USER_ID,context);
                JsonAccountObject = JsonServiceHandler.ServiceDataGet();
                return null;
            }
        }.execute(new Void[]{});

    }
    public   void OfflineProducts(){
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";



            @Override
            protected void onPreExecute() {

                dialog = ProgressDialog.show(context, "",
                        "Initializing Products table ...", true,true);
                dialog.setCancelable(false);



            }


            @Override
            protected void onPostExecute(Void result) {

                super.onPostExecute(result);
                if (!dialog.isShowing()){
                    offlineMerchant();
                }
            }


            @Override
            protected Void doInBackground(Void... params) {

                //Do something...
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("userid", Constants.USER_ID);
                    jsonObject.put("status","Active");

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                JsonServiceHandler = new JsonServiceHandler(Utils.strofflineproduct, jsonObject.toString(), context);
                JsonAccountObject = JsonServiceHandler.ServiceData();
                System.out.println("JsonAccountObject-->:"+JsonAccountObject);

                try {


                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {

                        JSONArray job1 = JsonAccountObject.getJSONArray("data");
                        System.out.println("job1-->"+job1);
                        count = job1.length();
                        int i;
                        for (i = 0; i < job1.length(); i++) {

                            JSONObject job = new JSONObject();

                            job = job1.getJSONObject(i);
                            System.out.println("job--->"+job);
                            prodid = job.getString("prodid");
                            prodname = job.getString("prodname");
                            System.out.println("prodname-->:"+ prodname);
                            prodcode = job.getString("prodcode");
                            userid = job.getString("userid");
                            qty = "";
                            fqty = "";
                            pdate = job.getString("updateddt");
                            pcompanyname = job.getString("companyname");
                            prodprice = job.getString("prodprice");
                            prodtax = job.getString("prodtax");
                            mfgdate ="";
                            expdate = "";
                            batchno = "";
                            uom = job.getString("uom");
                            String unitgram = job.getString("unitsgm");
                            String unitperuom = job.getString("unitperuom");
                            String prodimage=job.getString("prodimage");
                            if (unitgram.equals(null) || unitgram.isEmpty() || unitgram.equals("null")) {
                                unitgram = "0";
                            }
                            if (unitperuom.equals(null) || unitperuom.isEmpty() || unitperuom.equals("null")) {
                                unitperuom = "0";
                            }


                            if (uom.equals(null) || uom.isEmpty() || uom.equals("null")) {
                                uom = "";
                            }

                            if (prodprice.equals(null) || prodprice.isEmpty() || prodprice.equals("null")) {
                                prodprice = "0";
                            }

                            if (prodtax.equals(null) || prodtax.isEmpty() || prodtax.equals("null")) {
                                prodtax = "0";
                            }
                            //currentdate="2016-07-04";
                            String inputFormat = new SimpleDateFormat(
                                    "yyyy-MM-dd HH:mm:ss").format(new Date());

                            Log.e("unitgram",unitgram);
                            Log.e("unitperuom",unitperuom);

                            Log.e("insert", "insert");
                            databaseHandler.addProd(userid, pcompanyname, prodcode, prodid, prodname, qty, fqty, pdate, inputFormat,
                                    prodprice, prodtax, batchno, mfgdate, expdate, uom, "0",unitgram,unitperuom,prodimage,"");

                        }
                        Cursor cur;
                        cur = databaseHandler.getProduct();
                        System.out.println("SHICURPRODUCT:");
                        System.out.println(databaseHandler.getCursorItems(cur));
                        ProductCount = String.valueOf(cur.getCount());
                        Log.e("productCount", String.valueOf(cur.getCount()));

                        if (i == count) {
                            dialog.dismiss();

                        }



                    }else{
                        dialog.dismiss();
                       /* listViewOrders.setVisibility(View.GONE);
                        textViewNodata.setVisibility(View.VISIBLE);
                        textViewNodata.setText("No Products Found");

                        linearlayoutSearchIcon.setClickable(false);*/
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {

                }

                return null;

            }

        }.execute(new Void[]{});

    }



    public   void offlineMerchant() {
        new AsyncTask<Void, Void, Void>() {
            String strStatus="";
            ProgressDialog dialog;
            String city;
            @Override

            protected void onPreExecute() {
                super.onPreExecute();
                dialog= ProgressDialog.show(context, "",
                        "Initializing Merchant table ...", true,true);
                dialog.setCancelable(false);
            }
            @Override
            protected void onPostExecute(Void result) {

                super.onPostExecute(result);
                if (!dialog.isShowing()) {

                    offlineUserSetting();

                }
            }

            @Override
            protected Void doInBackground(Void... params) {
                JSONObject jsonObject = new JSONObject();

                try {
                    jsonObject.put("userid", Constants.USER_ID);
                    jsonObject.put("usertype", Constants.USER_TYPE);


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                JsonServiceHandler = new JsonServiceHandler(Utils.strGetMerchantList, jsonObject.toString(),context);
                JsonAccountObject = JsonServiceHandler.ServiceData();
                try {
                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);
                    if(strStatus.equals("true")) {
                        MDealerCompDropdownDomain cd;

                        JSONArray JsonArray = JsonAccountObject.getJSONArray("data");
                        int merchntcount=JsonArray.length();
                        int i;

                        for ( i= 0; i < JsonArray.length(); i++) {
                            cd = new MDealerCompDropdownDomain();
                            JsonAccountObject = JsonArray.getJSONObject(i);
                            Mfullname = JsonAccountObject.getString("fullname");
                            Mcmpnyname = JsonAccountObject.getString("companyname");
                            Muserid = JsonAccountObject.getString("userid");
                            merchantmobileno= JsonAccountObject.getString("mobileno");
                            mflag= "";//JsonAccountObject.getString("cflag");
                            memail="";//JsonAccountObject.getString("emailid");
                            maddress=JsonAccountObject.getString("address");
                            city=JsonAccountObject.getString("city");
                            strStockiest=JsonAccountObject.getString("stockiest");
                            strRoute=JsonAccountObject.getString("route");
                            String date[]=JsonAccountObject.getString("updateddt").split("\\s+");
                            Log.e("newdate",date[0]);
                            // day=JsonAccountObject.getString("address");

                            databaseHandler.addmerchant(Mcmpnyname, Muserid, Mfullname,merchantmobileno,mflag,memail,maddress,city,"","","","Saturday",date[0],pan_no,customdata,strStockiest,strRoute,strState);

                        }
                        Cursor cur;
                        cur = databaseHandler.getmerchant();

                        if(cur!=null && cur.getCount() > 0) {
                            if (cur.moveToLast()) {
                                Log.e("daytoday", String.valueOf(cur.getString(13)));

                            }
                        }

                        MerchantCount = String.valueOf(cur.getCount());

                        if(i==merchntcount) {
                            dialog.dismiss();
                        }
                    }else {
                        dialog.dismiss();
                      /*  listViewOrders.setVisibility(View.GONE);
                        textViewNodata.setVisibility(View.VISIBLE);
                        textViewNodata.setText("No Merchants Found");

                        linearlayoutSearchIcon.setClickable(false);*/


                    }

                } catch (JSONException e) {
                    Log.e("JSONException", e.toString());

                } catch (Exception e) {
                    Log.e("Exception", e.toString());


                }

                return null;
            }
        }.execute(new Void[] {});
    }

    public   void offlineScheme(final String selected_dealer) {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            String strStatus = "",strMsg="";

            ProgressDialog dialog;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialog= ProgressDialog.show(context, "",
                        "Initializing Scheme table ...", true, true);
                dialog.setCancelable(false);
            }


            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
                if (!dialog.isShowing()){
                    productuom();
                    // offlineProductSetting();

                }

            }

            @Override
            protected Void doInBackground(Void... params) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("duserid",selected_dealer);

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                JsonServiceHandler = new JsonServiceHandler(Utils.strgetScheme,jsonObject.toString(),context );
                JsonAccountObject = JsonServiceHandler.ServiceData();

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);
                    int i;
                    if (strStatus.equals("true")) {


                        JSONArray job1 = JsonAccountObject.getJSONArray("data");

                        int schmecount=job1.length();
                        for ( i = 0; i < job1.length(); i++) {

                            JSONObject job = new JSONObject();
                            job = job1.getJSONObject(i);
                            String schmeduserid=job.getString("duserid");
                            String schemeprodid = job.getString("prodid");
                            String schemestartdate =job.getString("start_date");
                            String schemeenddate=job.getString("end_date");
                            String schemetype=job.getString("schemetype");
                            String schemediscount=job.getString("discount");
                            String schemefreeProdid=job.getString("free_prodid");
                            String schemefreeQty=job.getString("free_qty");
                            String schemeremark=job.getString("remarks");
                            String schemestatus=job.getString("status");

                            databaseHandler.addSchemes(schemeprodid, schmeduserid, schemestartdate, schemeenddate, schemediscount, schemetype,
                                    schemefreeProdid,schemefreeQty,schemestatus,schemeremark);


                        }
                        Cursor cur;
                        cur = databaseHandler.getSchemeCount();
                        Log.e("dealercount", String.valueOf(cur.getCount()));
                        SchemeCount = String.valueOf(cur.getCount());


                        if(i==schmecount) {

                            dialog.dismiss();
                        }

                    }else{
                        dialog.dismiss();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {

                }

                return null;
            }
        }.execute(new Void[]{});

    }

    public void offlineDistributorModelBeat(){
        new AsyncTask<Void, Void, Void>() {
            String strStatus = "",strMsg="";

            ProgressDialog dialog;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialog= ProgressDialog.show(context, "",
                        "Initializing Beat table ...", true, true);
                dialog.setCancelable(false);
            }


            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
                if (!dialog.isShowing()){
                    Cursor cursor;
                    cursor = databaseHandler.getdealer();
                    Log.e("count", String.valueOf(cursor.getCount()));
                    if(cursor.getCount()>0){
                        cursor.moveToFirst();
                        selected_dealer = cursor.getString(cursor
                                .getColumnIndex(DatabaseHandler.KEY_Duserid));
                    }

                    offlineScheme(selected_dealer);

                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JsonServiceHandler = new JsonServiceHandler(Utils.strbeat+Constants.USER_ID+"&filter[where][status]=Active",context );
                JsonAccountObject = JsonServiceHandler.ServiceDataGet();

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);
                    int i;
                    if (strStatus.equals("true")) {


                        JSONArray job1 = JsonAccountObject.getJSONArray("data");

                        int beatcount1=job1.length();
                        for ( i = 0; i < job1.length(); i++) {

                            JSONObject job = new JSONObject();
                            job = job1.getJSONObject(i);
                            String beatid=""; //job.getString("beatid")
                            String muserid = job.getString("muserid");
                            String suserid =job.getString("suserid");
                            String duserid=job.getString("duserid");
                            String day_mon=job.getString("day_mon");
                            String day_tue=job.getString("day_tue");
                            String day_wed=job.getString("day_wed");
                            String day_thu=job.getString("day_thu");
                            String day_fri=job.getString("day_fri");
                            String day_sat=job.getString("day_sat");
                            String day_sun=job.getString("day_sun");
                            String status=job.getString("status");
                            String beattype="D"; //job.getString("beattype")
                            String beatname=""; //job.getString("beatname")



                            String inputFormat1 = new SimpleDateFormat(
                                    "yyyy-MM-dd HH:mm:ss").format(new Date());
                            Log.e("inputFormat",inputFormat1);

                            databaseHandler.addDistributorModelbeat(beatid, muserid, suserid, duserid, day_mon, day_tue,
                                    day_wed,day_thu,day_fri,day_sat,day_sun,status,inputFormat1,inputFormat1,"","",beattype,beatname);


                        }
                        Cursor cur;
                        cur = databaseHandler.getbeat();
                        Log.e("addDistributorModelbeat", String.valueOf(cur.getCount()));
                        beatcount = String.valueOf(cur.getCount());


                        if(i==beatcount1) {

                            dialog.dismiss();
                        }

                    }else{
                        dialog.dismiss();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {

                }

                return null;
            }
        }.execute(new Void[]{});
    }

    public void offlineCompanyBeatMerchant(final String beatid,final String beatname){
        new AsyncTask<Void, Void, Void>() {
            String strStatus = "",strMsg="";

            ProgressDialog dialog;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();

            }


            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);

            }

            @Override
            protected Void doInBackground(Void... params) {

                Cursor cursor;
                cursor = databaseHandler.getdealer();
                Log.e("count", String.valueOf(cursor.getCount()));
                if(cursor.getCount()>0){
                    cursor.moveToFirst();
                    selected_dealer = cursor.getString(cursor
                            .getColumnIndex(DatabaseHandler.KEY_Duserid));
                }
                JsonServiceHandler = new JsonServiceHandler(Utils.getCompanyBeatMerchant+selected_dealer+"&filter[where][id]="+beatid,context );
                JsonAccountObject = JsonServiceHandler.ServiceDataGet();

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);
                    int i;
                    if (strStatus.equals("true")) {

                        String muserid="";
                        JSONArray job1 = JsonAccountObject.getJSONArray("data");

                        for ( i = 0; i < job1.length(); i++) {

                            JSONObject job = new JSONObject();
                            job = job1.getJSONObject(i);

                            if(i==0){
                                muserid = job.getString("muserid");
                            }else{
                                muserid = muserid+","+job.getString("muserid");
                            }



                        }
                        Log.e("muserid",muserid);
                        databaseHandler.updateCompanyBeatMerchant(muserid,beatname);
                        Cursor cur;
                        cur =databaseHandler.getUpdatedBeat(muserid,beatname);
                        Log.e("addCompanyModelbeat", String.valueOf(cur.getCount()));
                        beatcount = String.valueOf(cur.getCount());

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {

                }

                return null;
            }
        }.execute(new Void[]{});

    }
    public   void offlineCompanyModelBeat() {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            String strStatus = "",strMsg="";

            ProgressDialog dialog;
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialog= ProgressDialog.show(context, "",
                        "Initializing Beat table ...", true, true);
                dialog.setCancelable(false);
            }


            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
                if (!dialog.isShowing()){
                    Cursor cursor;
                    cursor = databaseHandler.getdealer();
                    Log.e("count", String.valueOf(cursor.getCount()));
                    if(cursor.getCount()>0){
                        cursor.moveToFirst();
                        selected_dealer = cursor.getString(cursor
                                .getColumnIndex(DatabaseHandler.KEY_Duserid));
                    }

                    offlineScheme(selected_dealer);

                }

            }

            @Override
            protected Void doInBackground(Void... params) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("suserid", Constants.USER_ID);


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                JsonServiceHandler = new JsonServiceHandler(Utils.getdistributor,jsonObject.toString(),context );
               // Log.e("JsonServiceHandler------", String.valueOf(JsonServiceHandler));
                JsonAccountObject = JsonServiceHandler.ServiceData();

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);
                    int i;
                    if (strStatus.equals("true")) {


                        JSONArray job1 = JsonAccountObject.getJSONArray("data");

                        int beatcount1=job1.length();
                        for ( i = 0; i < job1.length(); i++) {

                            JSONObject job = new JSONObject();
                            Log.e("job-------S", String.valueOf(job));
                            job = job1.getJSONObject(i);
                            Log.e("job1-------S", String.valueOf(job1));
                            String beatid=job.getString("id");
                            Log.e("beatid",beatid);
                            String muserid ="" ; //job.getString("muserid")
                            String suserid =Constants.USER_ID;
                            Log.e("suserid",suserid);//job.getString("suserid")
                            String duserid=job.getString("duserid");
                            Log.e("duserid",duserid);
                            String day_mon=job.getString("day_mon");
                            Log.e("day_mon",day_mon);
                            String day_tue=job.getString("day_tue");
                            Log.e("day_tue",day_tue);
                            String day_wed=job.getString("day_wed");
                            Log.e("day_wed",day_wed);
                            String day_thu=job.getString("day_thu");
                            Log.e("day_thu",day_thu);
                            String day_fri=job.getString("day_fri");
                            Log.e("day_fri",day_fri);
                            String day_sat=job.getString("day_sat");
                            Log.e("day_sat",day_sat);
                            String day_sun=job.getString("day_sun");
                            Log.e("day_sun",day_sun);
                            String status=job.getString("status");
                            Log.e("status",status);
                            String beattype=job.getString("beattype");
                            Log.e("beattype",beattype);
                            String beatname=job.getString("beatnm");
                            Log.e("beatname",beatname);
                            String fullname=job.getString("fullname");
                            Log.e("fullname",fullname);
                            String usertype=job.getString("usertype");
                            Log.e("usertype",usertype);
                            String companyname=job.getString("companyname");
                            Log.e("companyname",companyname);

                            String inputFormat1 = new SimpleDateFormat(
                                    "yyyy-MM-dd HH:mm:ss").format(new Date());
                            Log.e("inputFormat",inputFormat1);

                            databaseHandler.addCompanyModelbeat(beatid, muserid, suserid, duserid, day_mon, day_tue,
                                    day_wed,day_thu,day_fri,day_sat,day_sun,status,inputFormat1,inputFormat1,"","",beattype,beatname,fullname,usertype,companyname);
                            offlineCompanyBeatMerchant(beatid,beatname);


                        }
                        Cursor cur;
                        cur = databaseHandler.getbeat();
                        Log.e("addCompanyModelbeat", String.valueOf(cur.getCount()));
                        beatcount = String.valueOf(cur.getCount());


                        if(i==beatcount1) {

                            dialog.dismiss();
                        }

                    }else{
                        dialog.dismiss();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {

                }

                return null;
            }
        }.execute(new Void[]{});

    }

    private void getDetails() {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "";

            @Override
            protected void onPreExecute() {
                dialog= ProgressDialog.show(SalesManOrderActivity.this, "",
                        "Loading...", true,true);

            }

            @Override
            protected void onPostExecute(Void result) {

                try {

                    PaymentStatus=(JsonAccountObject.getString("pymtstatus"));
                    Log.e("PaymentStatus",PaymentStatus);


                    if(PaymentStatus.equals("Pending")||PaymentStatus.equals(null) ||PaymentStatus.equals("")||PaymentStatus.equals("null")){
                        showAlertDialogToast("Please make payment before place order");
                    }else{

                        Intent to = new Intent(SalesManOrderActivity.this,
                                CreateOrderActivity .class);
                        startActivity(to);
                        finish();
                    }

                    dialog.dismiss();


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();


                JsonServiceHandler = new JsonServiceHandler(Utils.strGetProfileDetail+Constants.USER_ID, SalesManOrderActivity.this);
                JsonAccountObject = JsonServiceHandler.ServiceDataGet();
                return null;
            }
        }.execute(new Void[]{});
    }

    public  void initialcount(){
        String date = "yyyy-MM-dd";

        SimpleDateFormat inputFormat = new SimpleDateFormat(date);
        String dt =inputFormat.format(new Date());
        Cursor curs;

        curs = databaseHandler.getSuccessheader("Success",dt);
        Count= String.valueOf(curs.getCount());
        ButtonSuccess.setText("Success"+"("+Count+")");


        curs = databaseHandler.getSuccessheader("Pending", dt);
        Count= String.valueOf(curs.getCount());
        ButtonPending.setText("Pending" + "(" + Count + ")");

        curs = databaseHandler.getSuccessheader("Failed",dt);
        Count= String.valueOf(curs.getCount());
        ButtonFailure.setText("Failed"+"("+Count+")");
    }



    public static void MyOrdersList(String status){
        final   JSONArray resultSet = new JSONArray();

        String date = "yyyy-MM-dd";

        SimpleDateFormat inputFormat = new SimpleDateFormat(date);
        String dt =inputFormat.format(new Date());


        Cursor curs;
        curs = databaseHandler.getSuccessheader(status,dt);

        Log.e("HeaderCount", String.valueOf(curs.getCount()));
        Count= String.valueOf(curs.getCount());
        if(status.equals("Success")){
            ButtonSuccess.setText("Success"+"("+Count+")");
            Constants.totalorders=Count;
        }else if(status.equals("Pending")){
            ButtonPending.setText("Pending"+"("+Count+")");
        }else if(status.equals("Failed")){
            ButtonFailure.setText("Failed"+"("+Count+")");
        }
        curs.moveToFirst();
        while (curs.isAfterLast() == false) {

            int totalColumn = curs.getColumnCount();
            JSONObject rowObject = new JSONObject();

            for( int i=0 ;  i<totalColumn  ; i++ )
            {
                if( curs.getColumnName(i) != null  )
                {

                    try
                    {

                        if( curs.getString(i) != null )
                        {
                            Log.d("TAG_NAME", curs.getString(i) );
                            rowObject.put(curs.getColumnName(i) ,  curs.getString(i) );
                        }
                        else
                        {
                            rowObject.put( curs.getColumnName(i) ,  "" );
                        }
                    }
                    catch( Exception e )
                    {
                        Log.d("TAG_NAME", e.getMessage()  );
                    }
                }

            }



            Log.e("jsonarray-sk", rowObject.toString());

            JSONArray resultDetail 	= new JSONArray();
            try{

                Cursor dtlcursor;
                dtlcursor = databaseHandler.getorderdetail(rowObject.getString("oflnordid"));
                // System.out.println("SHICURPRODUCT:");
                // System.out.println(databaseHandler.getCursorItems(dtlcursor));
                // Log.e("oflnordid",rowObject.getString("oflnordid"));
                // Log.e("Cursor Count", String.valueOf(dtlcursor.getCount()));
                dtlcursor.moveToFirst();
                while (dtlcursor.isAfterLast() == false) {

                    int Columncount = dtlcursor.getColumnCount();
                    JSONObject detailObject = new JSONObject();

                    for( int i=0 ;  i<Columncount  ; i++ )
                    {
                        if( dtlcursor.getColumnName(i) != null  )
                        {

                            try
                            {

                                if( dtlcursor.getString(i) != null )
                                {
                                    Log.d("TAG_NAME", dtlcursor.getColumnName(i)+" : "+dtlcursor.getString(i) );
                                    detailObject.put(dtlcursor.getColumnName(i) ,  dtlcursor.getString(i) );
                                }
                                else
                                {
                                    detailObject.put( dtlcursor.getColumnName(i) ,  "" );
                                }
                            }
                            catch( Exception e )
                            {
                                Log.d("TAG_NAME", e.getMessage()  );
                            }
                        }

                    }

                    resultDetail.put(detailObject);
                    dtlcursor.moveToNext();
                }

                dtlcursor.close();

                Log.e("Detailarray", resultDetail.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }
            try{
                rowObject.put("orderdtls",resultDetail);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            resultSet.put(rowObject);
            curs.moveToNext();
        }
        curs.close();


        Log.e("SaveArray", resultSet.toString());
        int c=Integer.parseInt(Count);
        if(c!=0){

            try{
                Log.e("inside", "inside");
                arraylistDealerOrderList=new ArrayList<DealerOrderListDomain>();
                arraylistSearchResults=new ArrayList<DealerOrderListDomain>();
                for (int i = 0; i < resultSet.length(); i++) {
                    JSONObject job = new JSONObject();
                    job=resultSet.getJSONObject(i);
                    DealerOrderListDomain dod=new DealerOrderListDomain();

                    if( job.getString("pushstatus").equals("Success")){

                        dod.setAddress(job.getString("onlineorderno"));
                        Log.e("onlineorderno",dod.getAddress());
                    }else{

                        dod.setAddress( " T "+ job.getString("oflnordid"));
                        Log.e("oflnordid", dod.getAddress());
                    }

                    if(job.getString("aprxordval").equals("0.0")){
                        dod.setComName(job.getString("mname") + " (RS." + 0 + ")");
                    }else{
                        try {
                            double value = Double.parseDouble(job.getString("aprxordval"));

                            DecimalFormat df = new DecimalFormat(".00");
                            df.setRoundingMode(RoundingMode.DOWN);
                            String appr = df.format(value);

                            dod.setComName(job.getString("mname") + " (Rs." + appr + ")");
                            dod.setapproximate(value);
                        }catch (Exception e){

                        }
                    }

                    dod.setrowid(job.getJSONArray("orderdtls").getJSONObject(0).getString("rowid"));
                    Log.e("rowid", job.getJSONArray("orderdtls").getJSONObject(0).getString("rowid"));
                    String rowid=job.getJSONArray("orderdtls").getJSONObject(0).getString("rowid");
                    dod.setOrderid(job.getString("oflnordid"));
                    dod.setMuserid(job.getString("muserid"));
                    dod.setFullname(job.getJSONArray("orderdtls").getJSONObject(0).getString("duserid"));
                    dod.setSerialno(job.getJSONArray("orderdtls").getJSONObject(0).getString("ordslno"));
                    dod.setMname(job.getString("mname"));
                    dod.setisread(job.getJSONArray("orderdtls").getJSONObject(0).getString("isread"));
                    dod.setserialnonew(String.valueOf(i + 1));
                    Log.e("ordimage", job.getJSONArray("orderdtls").getJSONObject(0).getString("ordimage"));
                    if(!job.getJSONArray("orderdtls").getJSONObject(0).getString("ordimage").equals("0")){
                        dod.setorderimage(job.getJSONArray("orderdtls").getJSONObject(0).getString("ordimage"));
                    }else{
                        dod.setorderimage("0");
                    }


                    time=job.getString("orderdt");
                    Log.e("time",time);
                    String inputPattern1 = "yyyy-MM-dd";
                    String outputPattern = "dd-MMM-yyyy ";
                    SimpleDateFormat inputFormat1 = new SimpleDateFormat(inputPattern1);
                    SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

                    String str = null;

                    try {
                        dateStr = inputFormat1.parse(time);
                        str = outputFormat.format(dateStr);
                        dod.setDate(str);
                        //Log.e("str", str);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    arraylistDealerOrderList.add(dod);
                    arraylistSearchResults.add(dod);

                    Log.e("Size1",String.valueOf(arraylistDealerOrderList.size()));

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            setadap();
        }else{
            Log.e("outside", "outside");
            listViewOrders.setVisibility(View.GONE);
            textViewNodata.setVisibility(View.VISIBLE);
            if(status.equals("Success")){
                textViewNodata.setText("No items");
            }else if(status.equals("Pending")){
                textViewNodata.setText("No Pending items");
            }else if(status.equals("Failed")){
                textViewNodata.setText("No Failed items");
            }

            //buttonPlaceOrder.setVisibility(Button.VISIBLE);
            linearlayoutSearchIcon.setClickable(false);
        }
    }


    private void myOrders() {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";


            @Override
            protected void onPreExecute() {
                dialog= ProgressDialog.show(SalesManOrderActivity.this, "",
                        "Loading...", true, true);

            }

            @Override
            protected void onPostExecute(Void result) {

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {



                        JSONArray job1 = JsonAccountObject.getJSONArray("data");
                        arraylistDealerOrderList=new ArrayList<DealerOrderListDomain>();
                        arraylistSearchResults=new ArrayList<DealerOrderListDomain>();
                        for (int i = 0; i < job1.length(); i++) {
                            JSONObject job = new JSONObject();
                            job=job1.getJSONObject(i);
                            DealerOrderListDomain dod=new DealerOrderListDomain();
                            dod.setOrderid(job.getString("ordid"));
                            dod.setMuserid(job.getString("muserid"));
                            dod.setFullname(job.getString("duserid"));
                            dod.setComName(job.getString("mcompany"));
                            dod.setMname(job.getString("mfname"));
                            //dod.setDate(job.getString("orderdt"));
                            time=job.getString("orderdt");

                            String inputPattern = "yyyy-MM-dd";
                            String outputPattern = "dd-MMM-yyyy ";
                            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
                            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

                            String str = null;

                            try {
                                dateStr = inputFormat.parse(time);
                                str = outputFormat.format(dateStr);
                                dod.setDate(str);
                                //Log.e("str", str);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            dod.setAddress(job.getString("ordid"));
                            arraylistDealerOrderList.add(dod);
                            arraylistSearchResults.add(dod);

                        }
                        setadap();
                    }else{
                        listViewOrders.setVisibility(View.GONE);
                        textViewNodata.setVisibility(View.VISIBLE);
                        textViewNodata.setText(strMsg);
                        buttonPlaceOrder.setVisibility(Button.VISIBLE);
                        linearlayoutSearchIcon.setClickable(false);
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    // Log.e("ProductActivityException", e.toString());
                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();

                try {
                    jsonObject.put("muserid",Constants.USER_ID);
                    jsonObject.put("usertype","S");
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                JsonServiceHandler = new JsonServiceHandler(Utils.strMerchantOrderList,jsonObject.toString(), SalesManOrderActivity.this);
                JsonAccountObject = JsonServiceHandler.ServiceData();
                return null;
            }
        }.execute(new Void[]{});
    }

    public static  void setadap() {
        // TODO Auto-generated method stub

        adapter=new DealerOrderListAdapter(context, R.layout.item_dealer_order_screen, arraylistDealerOrderList);
        listViewOrders.setVisibility(View.VISIBLE);
        listViewOrders.setAdapter(adapter);
        textViewNodata.setVisibility(View.GONE);


    }

    protected void toastDisplay(String msg) {
        Toast toast = Toast.makeText(SalesManOrderActivity.this, msg,
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();

    }

    public boolean netCheck() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            }

            else if (result == null) {
               /* showAlertDialog(SalesManOrderActivity.this,
                        "No Internet Connection",
                        "Please Check Your internet connection.", false);*/
                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
    public void showAlertDialog(Context context, String title, String message,
                                Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        alertDialog.show();
    }

    public static  void showAlertDialogToast( String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    public void onBackPressed() {
        exitAlret();
    }

    private void exitAlret() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        localBuilder.setMessage("Do you want to Exit?");
        localBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {

                        finish();

                    }
                });
        localBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {
                        paramDialogInterface.cancel();
                    }
                });
        localBuilder.create().show();
    }

    public boolean isWriteStorageAllowed(Context context) {
        //Getting the permission status
        int result = ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        //If permission is granted returning true otherwise false
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void requestWriteStoragePermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(this,Manifest.permission.WRITE_EXTERNAL_STORAGE)){
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
            //android.support.v7.app.AlertDialog.Builder alertBuilder = new android.support.v7.app.AlertDialog.Builder(this);
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
            alertBuilder.setCancelable(true);
            alertBuilder.setTitle("Permission necessary");
            alertBuilder.setMessage("This App Need Your Storage Permission To Take Order Images");
            alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                //@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions((Activity)SalesManOrderActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constants.WRITE_EXTERNAL_STORAGE);
                }
            });
            AlertDialog alert = alertBuilder.create();
            alert.show();
        }else {
            //And finally ask for the permission
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, Constants.WRITE_EXTERNAL_STORAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Constants.WRITE_EXTERNAL_STORAGE:

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    //callMethod();


                } else {
                    //finish();
                    Toast.makeText(this,"Oops you denied the permission",Toast.LENGTH_LONG).show();
                }
                return;
        }
    }

}
