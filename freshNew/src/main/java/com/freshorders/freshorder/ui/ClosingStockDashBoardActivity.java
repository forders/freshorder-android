package com.freshorders.freshorder.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.activity.DistanceCalActivity;
import com.freshorders.freshorder.adapter.DealerOrderListAdapter;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.DealerOrderListDomain;
import com.freshorders.freshorder.domain.DealerVisitDomain;
import com.freshorders.freshorder.toonline.migration.PendingDataMigrationActivity;
import com.freshorders.freshorder.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ClosingStockDashBoardActivity extends AppCompatActivity {

    public static ArrayList<DealerOrderListDomain> arraylistDealerOrderList, arraylistSearchResults;
    private Context context = ClosingStockDashBoardActivity.this;

    TextView menuIcon, textViewAssetMenuProfile, textViewAssetMenuMyOrders,
            textViewAssetMenuMyDealers, textViewAssetMenuProducts,
            textViewAssetMenuPayment, textViewAssetMenuComplaint,textViewAssetMenuDistanceCalculation,
            textViewAssetMenuSignout, textViewAssetMenuPaymentCollection, textViewAssetClientVisit,
            textViewNodata, textViewHeader, textViewAssetMenuCreateOrder, textViewCross,
            textViewAssetMenuupdate, textviewupdate, textViewAssetMenuRefresh, textViewAssetMenuStockEntry, textViewAssetMenuAddMerchant, textViewAssetMenuAcknowledge, textViewAssetMenusurveyuser, textViewAssetMenudownloadscheme, textViewAssetMenuMyDayPlan, textViewAssetMenuMySales,
            textViewAssetMenuDistStock, textViewAssetSearch,
            textViewAssetMenuStockOnly, textViewAssetMenuMigration, textViewAssetMenuStockAudit;

    ListView listViewOrders;
    public static int menuCliccked, selectedPosition;

    LinearLayout linearLayoutMenuParent, linearLayoutProfile,linearLayoutDistanceCalculation,
            linearLayoutMyOrders, linearLayoutMyDealers, linearLayoutProducts,
            linearLayoutPayment, linearLayoutComplaint, linearLayoutClientVisit, linearLayoutSignout, linearLayoutCreateOrder, linearLayoutPaymentCollection, linearLayoutSuccess, linearLayoutFailure,
            linearLayoutPending, linearLayoutupdate, linearLayoutRefresh, linearLayoutAcknowledge,
            linearLayoutStockEntry, linearLayoutAddMerchant, linearLayoutsurveyuser, linearLayoutdownloadscheme, linearLayoutMyDayPlan, linearLayoutMySales,
            linearLayoutDistStock, linearlayoutSearchIcon, linearLayoutStockOnly, linearLayoutMigration, linearLayoutStockAudit;


    private void showMenu(){

        MyApplication app = MyApplication.getInstance();

        if(!app.isCreateOrder()){
            linearLayoutCreateOrder.setVisibility(View.GONE);
        }
        if(!app.isProfile()){
            linearLayoutProfile.setVisibility(View.GONE);
        }
        if(!app.isMyDayPlan()){
            linearLayoutMyDayPlan.setVisibility(View.GONE);
        }
        if(!app.isMyOrders()){
            linearLayoutMyOrders.setVisibility(View.GONE);
        }
        if(!app.isAddMerchant()){
            linearLayoutAddMerchant.setVisibility(View.GONE);
        }
        if(!app.isMySales()){
            linearLayoutMySales.setVisibility(View.GONE);
        }
        if(!app.isPostNotes()){
            linearLayoutComplaint.setVisibility(View.GONE);
        }
        if(!app.isMyClientVisit()){
            linearLayoutClientVisit.setVisibility(View.GONE);
        }
        if(!app.isAcknowledge()){
            linearLayoutAcknowledge.setVisibility(View.GONE);
        }
        if(!app.isPaymentCollection()){
            linearLayoutPaymentCollection.setVisibility(View.GONE);
        }
        if(!app.isPkdDataCapture()){
            linearLayoutStockEntry.setVisibility(View.GONE);
        }
        if(!app.isDistributorStock()){
            linearLayoutDistStock.setVisibility(View.GONE);
        }
        if(!app.isSurveyUser()){
            linearLayoutsurveyuser.setVisibility(View.GONE);
        }
        if(!app.isDownLoadScheme()){
            linearLayoutdownloadscheme.setVisibility(View.GONE);
        }
        if(!app.isDistanceCalculation()){
            linearLayoutDistanceCalculation.setVisibility(View.GONE);
        }
        if(!app.isRefresh()){
            linearLayoutRefresh.setVisibility(View.GONE);
        }
        if(!app.isLogout()){
            linearLayoutSignout.setVisibility(View.GONE);
        }
        if(!app.isClosingStock()){
            linearLayoutStockOnly.setVisibility(View.GONE);
        }
        if(!app.isClosingStockAudit()){
            linearLayoutStockAudit.setVisibility(View.GONE);
        }
        if(!app.isPendingData()){
            linearLayoutMigration.setVisibility(View.GONE);
        }
    }




    JSONObject JsonAccountObject = null;
    JSONArray JsonAccountArray = null;
    DatabaseHandler databaseHandler;
    com.freshorders.freshorder.utils.JsonServiceHandler JsonServiceHandler;
    public static ArrayList<DealerVisitDomain> arraylistDealerVisitDomain;
    public static String companyname, fullname, date, address, orderId, morderId, PaymentStatus = "NULL", onlineorderno, Paymentdate = "NULL", salesuserid = "NULL";
    Date dateStr = null;
    public static String time, searchclick = "0", strMsg = "";
    public static double approx = 0.0;
    ;
    EditText editTextSearchField;
    public static DealerOrderListDomain domainSelected;
    public DealerOrderListAdapter adapter;
    Button buttonPlaceOrder, buttonBlue, buttonYellow, buttonGreen, ButtonPending, ButtonFailure, ButtonSuccess;
    public static String Count, mname, sname, lat, lng, desc;
    public static String prodid, prodname, prodcode, userid, pdate, pcompanyname, Porderdate, qty, fqty, Dorderdate, prodprice, prodtax, moveto = "NULL";
    public static int count = 0;
    public static String[] image;
    public static ArrayList<Bitmap> arrayListimagebitmap;
    public static ArrayList<String> arraylistimagepath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //requestWindowFeature(1);
        setContentView(R.layout.activity_closing_stock_dash_board);
        MyApplication.getInstance().setTemplate8ForTemplate2(false);//////////////

        databaseHandler = new DatabaseHandler(getApplicationContext());
        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();

        arrayListimagebitmap = new ArrayList<Bitmap>();
        arraylistimagepath = new ArrayList<String>();
        Cursor cur;
        cur = databaseHandler.getDetails();
        cur.moveToFirst();
        Log.e("Insertion Check",
                cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_username)));
        Log.e("Insertion Check",
                cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_password)));
        Log.e("Insertion Check",
                cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_usertype)));
        PaymentStatus = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_paymentStatus));

        linearLayoutCreateOrder = (LinearLayout) findViewById(R.id.linearLayoutCreateOrder);
        listViewOrders = (ListView) findViewById(R.id.listViewOrders);
        linearLayoutProfile = (LinearLayout) findViewById(R.id.linearLayoutProfile);
        linearLayoutMyOrders = (LinearLayout) findViewById(R.id.linearLayoutMyOrders);
        linearLayoutMyDealers = (LinearLayout) findViewById(R.id.linearLayoutMyDealers);
        linearLayoutProducts = (LinearLayout) findViewById(R.id.linearLayoutProducts);
        linearLayoutPayment = (LinearLayout) findViewById(R.id.linearLayoutPayment);
        linearLayoutComplaint = (LinearLayout) findViewById(R.id.linearLayoutComplaint);
        linearLayoutSignout = (LinearLayout) findViewById(R.id.linearLayoutSignout);
        linearLayoutSuccess = (LinearLayout) findViewById(R.id.linearLayoutSuccess);
        linearLayoutFailure = (LinearLayout) findViewById(R.id.linearLayoutFailure);
        linearLayoutPending = (LinearLayout) findViewById(R.id.linearLayoutPending);
        linearLayoutDistanceCalculation= (LinearLayout) findViewById(R.id.linearLayoutDistanceCalculation);
        linearLayoutClientVisit = (LinearLayout) findViewById(R.id.linearLayoutClientVisit);
        linearLayoutPaymentCollection = (LinearLayout) findViewById(R.id.linearLayoutPaymentCollection);
        linearLayoutAcknowledge = (LinearLayout) findViewById(R.id.linearLayoutAcknowledge);
        linearLayoutStockEntry = (LinearLayout) findViewById(R.id.linearLayoutStockEntry);
        linearLayoutAddMerchant = (LinearLayout) findViewById(R.id.linearLayoutAddMerchant);
        linearLayoutdownloadscheme = (LinearLayout) findViewById(R.id.linearLayoutdownloadscheme);
        linearLayoutsurveyuser = (LinearLayout) findViewById(R.id.linearLayoutsurveyuser);
        linearLayoutMyDayPlan = (LinearLayout) findViewById(R.id.linearLayoutMyDayPlan);
        linearLayoutMySales = (LinearLayout) findViewById(R.id.linearLayoutMySales);
        linearlayoutSearchIcon = (LinearLayout) findViewById(R.id.linearlayoutSearchIcon);

        linearLayoutStockOnly = (LinearLayout) findViewById(R.id.linearLayoutStockOnly);
        linearLayoutStockAudit = findViewById(R.id.linearLayoutStockAudit);
        linearLayoutMigration = (LinearLayout) findViewById(R.id.linearLayoutMigration);

        buttonPlaceOrder = (Button) findViewById(R.id.buttonPlaceOrder);
        buttonBlue = (Button) findViewById(R.id.buttonBlue);
        buttonYellow = (Button) findViewById(R.id.buttonYellow);
        buttonGreen = (Button) findViewById(R.id.buttonGreen);
        ButtonPending = (Button) findViewById(R.id.ButtonPending);
        ButtonFailure = (Button) findViewById(R.id.ButtonFailure);
        ButtonSuccess = (Button) findViewById(R.id.ButtonSuccess);
        linearLayoutRefresh = (LinearLayout) findViewById(R.id.linearLayoutRefresh);
        textViewAssetMenuRefresh = (TextView) findViewById(R.id.textViewAssetMenuRefresh);
        textViewAssetSearch = (TextView) findViewById(R.id.textViewAssetSearch);
        menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
        Typeface font = Typeface.createFromAsset(getAssets(),
                "fontawesome-webfont.ttf");
        menuIcon.setTypeface(font);
        textViewAssetSearch.setTypeface(font);

        if (Constants.USER_TYPE.equals("D")) {
            linearLayoutMyDealers.setVisibility(View.GONE);
            linearLayoutProducts.setVisibility(View.VISIBLE);
            linearLayoutCreateOrder.setVisibility(View.GONE);
            linearLayoutRefresh.setVisibility(View.GONE);
            linearLayoutPaymentCollection.setVisibility(View.GONE);
            linearLayoutDistanceCalculation.setVisibility(View.GONE);
        } else if (Constants.USER_TYPE.equals("M")) {
            linearLayoutMyDealers.setVisibility(View.VISIBLE);
            linearLayoutProducts.setVisibility(View.GONE);
            linearLayoutCreateOrder.setVisibility(View.GONE);
            linearLayoutPaymentCollection.setVisibility(View.GONE);
            linearLayoutRefresh.setVisibility(View.GONE);
            linearLayoutDistanceCalculation.setVisibility(View.GONE);
        } else {
            linearLayoutMyDealers.setVisibility(View.VISIBLE);
            linearLayoutProducts.setVisibility(View.GONE);
            linearLayoutCreateOrder.setVisibility(View.VISIBLE);
            linearLayoutRefresh.setVisibility(View.VISIBLE);
            linearLayoutDistanceCalculation.setVisibility(View.VISIBLE);
        }
        textViewAssetMenuCreateOrder = (TextView) findViewById(R.id.textViewAssetMenuCreateOrder);
        textViewHeader = (TextView) findViewById(R.id.textViewHeader);
        textViewNodata = (TextView) findViewById(R.id.textViewNodata);
        textViewAssetMenuProfile = (TextView) findViewById(R.id.textViewAssetMenuProfile);
        textViewAssetMenuMyOrders = (TextView) findViewById(R.id.textViewAssetMenuMyOrders);
        textViewAssetMenuMyDealers = (TextView) findViewById(R.id.textViewAssetMenuMyDealers);
        textViewAssetMenuProducts = (TextView) findViewById(R.id.textViewAssetMenuProducts);
        textViewAssetMenuPayment = (TextView) findViewById(R.id.textViewAssetMenuPayment);
        textViewAssetMenuComplaint = (TextView) findViewById(R.id.textViewAssetMenuComplaint);
        textViewAssetMenuSignout = (TextView) findViewById(R.id.textViewAssetMenuSignout);
        textViewAssetClientVisit = (TextView) findViewById(R.id.textViewAssetClientVisit);
        textViewAssetMenuDistanceCalculation= (TextView) findViewById(R.id.textViewAssetMenuDistanceCalculation);
        editTextSearchField = (EditText) findViewById(R.id.editTextSearchField);
        textViewCross = (TextView) findViewById(R.id.textViewCross);

        textViewAssetMenuPaymentCollection = (TextView) findViewById(R.id.textViewAssetMenuPaymentCollection);
        textViewAssetMenuAddMerchant = (TextView) findViewById(R.id.textViewAssetMenuAddMerchant);
        textViewAssetMenuAcknowledge = (TextView) findViewById(R.id.textViewAssetMenuAcknowledge);
        textViewAssetMenuStockEntry = (TextView) findViewById(R.id.textViewAssetMenuStockEntry);

        textViewAssetMenudownloadscheme = (TextView) findViewById(R.id.textViewAssetMenudownloadscheme);
        textViewAssetMenusurveyuser = (TextView) findViewById(R.id.textViewAssetMenusurveyuser);
        textViewAssetMenuMyDayPlan = (TextView) findViewById(R.id.textViewAssetMenuMyDayPlan);
        textViewAssetMenuMySales = (TextView) findViewById(R.id.textViewAssetMenuMySales);


        linearLayoutMenuParent = (LinearLayout) findViewById(R.id.linearLayoutMenuParent);
        menuIcon.setTypeface(font);

        textViewAssetMenuCreateOrder.setTypeface(font);
        textViewAssetMenuProfile.setTypeface(font);
        textViewAssetMenuMyOrders.setTypeface(font);
        textViewAssetMenuMyDealers.setTypeface(font);
        textViewAssetMenuProducts.setTypeface(font);
        textViewAssetMenuPayment.setTypeface(font);
        textViewAssetMenuComplaint.setTypeface(font);
        textViewAssetMenuSignout.setTypeface(font);
        textViewAssetClientVisit.setTypeface(font);
        textViewAssetMenuPaymentCollection.setTypeface(font);
        textViewCross.setTypeface(font);
        textViewAssetMenuRefresh.setTypeface(font);
        textViewAssetMenuDistanceCalculation.setTypeface(font);
        textViewAssetMenuAcknowledge.setTypeface(font);
        textViewAssetMenuStockEntry.setTypeface(font);
        textViewAssetMenuAddMerchant.setTypeface(font);

        textViewAssetMenusurveyuser.setTypeface(font);
        textViewAssetMenudownloadscheme.setTypeface(font);
        textViewAssetMenuMyDayPlan.setTypeface(font);
        textViewAssetMenuMySales.setTypeface(font);

        linearLayoutDistStock = (LinearLayout) findViewById(R.id.linearLayoutDistStock);
        textViewAssetMenuDistStock = (TextView) findViewById(R.id.textViewAssetMenuDistStock);
        textViewAssetMenuDistStock.setTypeface(font);

        textViewAssetMenuStockOnly = (TextView) findViewById(R.id.textViewAssetMenuStockOnly);
        textViewAssetMenuStockAudit = (TextView) findViewById(R.id.textViewAssetMenuStockAudit);
        textViewAssetMenuMigration = findViewById(R.id.textViewAssetMenuMigration);

        textViewAssetMenuStockOnly.setTypeface(font);
        textViewAssetMenuStockAudit.setTypeface(font);
        textViewAssetMenuMigration.setTypeface(font);

        linearLayoutMyDealers.setVisibility(View.GONE);
        initialcount();

        Cursor cursor;
        cursor = databaseHandler.getdealer();
        Log.e("count", String.valueOf(cursor.getCount()));
        if (cursor.getCount() == 0) {
            Log.e("count", String.valueOf(cursor.getCount()));
            showAlertDialogToast("Please Click Refresh Menu");
        } else {
            Log.e("count", "MyOrdersList" + "");
            MyOrdersList(Constants.orderstatus);
        }

        if (Constants.orderstatus.equals("Success")) {
            buttonBlue.setBackgroundColor(Color.parseColor("#0768AA"));
            buttonYellow.setBackgroundColor(Color.parseColor("#ecce03"));
            buttonGreen.setBackgroundColor(Color.parseColor("#b1d36f"));
        } else if (Constants.orderstatus.equals("Pending")) {
            buttonBlue.setBackgroundColor(Color.parseColor("#03acec"));
            buttonYellow.setBackgroundColor(Color.parseColor("#ecce03"));
            buttonGreen.setBackgroundColor(Color.parseColor("#84A544"));
        } else if (Constants.orderstatus.equals("Failed")) {
            buttonBlue.setBackgroundColor(Color.parseColor("#03acec"));
            buttonYellow.setBackgroundColor(Color.parseColor("#D3DD00"));
            buttonGreen.setBackgroundColor(Color.parseColor("#b1d36f"));
        }

        ButtonSuccess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Log.e("linearLayoutSuccess", "linearLayoutSuccess");
                buttonBlue.setBackgroundColor(Color.parseColor("#0768AA"));
                buttonYellow.setBackgroundColor(Color.parseColor("#ecce03"));
                buttonGreen.setBackgroundColor(Color.parseColor("#b1d36f"));
                MyOrdersList("Success");


            }
        });
        ButtonFailure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Log.e("linearLayoutFailure", "linearLayoutFailure");
                buttonBlue.setBackgroundColor(Color.parseColor("#03acec"));
                buttonYellow.setBackgroundColor(Color.parseColor("#D3DD00"));
                buttonGreen.setBackgroundColor(Color.parseColor("#b1d36f"));
                MyOrdersList("Failed");

            }
        });
        ButtonPending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Log.e("linearLayoutFailure", "linearLayoutFailure");
                buttonBlue.setBackgroundColor(Color.parseColor("#03acec"));
                buttonYellow.setBackgroundColor(Color.parseColor("#ecce03"));
                buttonGreen.setBackgroundColor(Color.parseColor("#84A544"));
                MyOrdersList("Pending");

            }
        });

        buttonPlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().setTemplate8ForTemplate2(true);//////
                Constants.checkproduct = 0;
                Constants.orderid = "";
                Constants.SalesMerchant_Id = "";
                Constants.Merchantname = "";
                Intent to = new Intent(ClosingStockDashBoardActivity.this,
                        CreateOrderActivity.class);
                startActivity(to);
                finish();


            }
        });


        menuIcon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (menuCliccked == 0) {
                    linearLayoutMenuParent.setVisibility(View.VISIBLE);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(menuIcon.getWindowToken(), 0);
                    editTextSearchField.setVisibility(EditText.GONE);
                    textViewHeader.setVisibility(TextView.VISIBLE);
                    textViewCross.setVisibility(TextView.GONE);
                    menuCliccked = 1;
                } else {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    textViewHeader.setVisibility(TextView.VISIBLE);
                    editTextSearchField.setVisibility(EditText.GONE);
                    textViewCross.setVisibility(TextView.GONE);
                    menuCliccked = 0;
                }

            }
        });


        listViewOrders.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                domainSelected = arraylistDealerOrderList.get(position);
                companyname = arraylistDealerOrderList.get(position).getFullname();
                fullname = arraylistDealerOrderList.get(position).getComName();
                date = arraylistDealerOrderList.get(position).getDate();
                address = arraylistDealerOrderList.get(position).getAddress();
                orderId = arraylistDealerOrderList.get(position).getOrderid();
                onlineorderno = arraylistDealerOrderList.get(position).getAddress();
                morderId = arraylistDealerOrderList.get(position).getMuserid();
                mname = arraylistDealerOrderList.get(position).getMname();
                approx = arraylistDealerOrderList.get(position).getapproximate();
                Constants.approximate = arraylistDealerOrderList.get(position).getapproximate();
                Log.e("appropriate", String.valueOf(approx));


                if (arraylistDealerOrderList.get(position).getorderimage().equals("0")) {
                    image = new String[0];
                } else {

                    image = arraylistDealerOrderList.get(position).getorderimage().split(",");
                }

                //
                Constants.serialno = arraylistDealerOrderList.get(position).getSerialno();
                Log.e("serialno", Constants.serialno);

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(listViewOrders.getWindowToken(), 0);
                Intent to = new Intent(ClosingStockDashBoardActivity.this, ClosingStockOrderDetailList.class);
                startActivity(to);
                finish();
                /*selectedPosition = position;
                listViewOrders.setAdapter(adapter);
                listViewOrders.setSelectionFromTop(position, 1);*/

            }
        });
        menuIcon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (menuCliccked == 0) {
                    linearLayoutMenuParent.setVisibility(View.VISIBLE);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(menuIcon.getWindowToken(), 0);
                    editTextSearchField.setVisibility(EditText.GONE);
                    textViewHeader.setVisibility(TextView.VISIBLE);
                    textViewCross.setVisibility(TextView.GONE);
                    menuCliccked = 1;
                } else {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    textViewHeader.setVisibility(TextView.VISIBLE);
                    editTextSearchField.setVisibility(EditText.GONE);
                    textViewCross.setVisibility(TextView.GONE);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(menuIcon.getWindowToken(), 0);
                    menuCliccked = 0;
                }

            }
        });

        linearLayoutMenuParent.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
            }
        });

        linearLayoutRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("Clicked", "Clicked");

                if (netCheck()) {

                    final Dialog qtyDialog = new Dialog(ClosingStockDashBoardActivity.this);
                    qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    qtyDialog.setContentView(R.layout.warning_dialog);

                    final Button buttonUpdateOk, buttonUpdateCancel;

                    final TextView textQtyValidate;

                    buttonUpdateOk = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateOk);
                    buttonUpdateCancel = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateCancel);

                    textQtyValidate = (TextView) qtyDialog
                            .findViewById(R.id.textQtyValidate);
                    qtyDialog.show();


                    buttonUpdateOk.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            databaseHandler.synTableDelete();
                            Constants.refreshscreen = "clientvisit";
                            Intent io = new Intent(ClosingStockDashBoardActivity.this, com.freshorders.freshorder.ui.RefreshActivity.class);
                            startActivity(io);
                            finish();
                            qtyDialog.dismiss();
                        }

                    });

                    buttonUpdateCancel
                            .setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub

                                    qtyDialog.dismiss();

                                }
                            });

                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }
            }

        });


        linearLayoutProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub

                if (netCheck() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    getDetails("profile");
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });
        linearLayoutDistanceCalculation.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub


                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Intent into=new Intent(ClosingStockDashBoardActivity.this, DistanceCalActivity.class);
                startActivity(into);


            }
        });
        linearLayoutMyOrders.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheck() == true) {
                    Constants.orderstatus = "Success";
                    getDetails("myorders");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Constants.orderstatus = "Success";
                        Intent io = new Intent(ClosingStockDashBoardActivity.this,
                                SalesManOrderActivity.class);
                        startActivity(io);
                        finish();
                    }
                }


            }
        });

        linearLayoutMyDealers.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheck()) {
                    getDetails("mydealer");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(ClosingStockDashBoardActivity.this,
                                MyDealersActivity.class);
                        io.putExtra("Key", "menuclick");
                        startActivity(io);
                        finish();

                    }
                }

            }
        });

        linearLayoutComplaint.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    getDetails("postnotes");
                } else {
                    //showAlertDialogToast("Please Check Your internet connection");
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    getDetails("postnotes");
                }
            }
        });

        linearLayoutCreateOrder.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (netCheck() == true) {

                    MyApplication.getInstance().setTemplate8ForTemplate2(false);

                    Constants.checkproduct = 0;
                    Constants.orderid = "";
                    Constants.SalesMerchant_Id = "";
                    Constants.Merchantname = "";
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    getDetails("Createorder");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Constants.checkproduct = 0;
                        Constants.orderid = "";
                        Constants.SalesMerchant_Id = "";
                        Constants.Merchantname = "";
                        linearLayoutMenuParent.setVisibility(View.GONE);
                        menuCliccked = 0;

                        Intent io = new Intent(ClosingStockDashBoardActivity.this,
                                CreateOrderActivity.class);
                        startActivity(io);
                        finish();
                    }
                }

            }
        });

        linearLayoutClientVisit.setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View v) {

                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheck() == true) {
                    getDetails("clientvisit");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment");
                    } else {
                        Intent io = new Intent(ClosingStockDashBoardActivity.this,
                                SMClientVisitHistory.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });


        linearLayoutPaymentCollection.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    getDetails("paymentcoll");
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }


            }
        });
        linearLayoutAddMerchant.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheck() == true) {
                    Constants.orderstatus = "Success";
                    getDetails("addmerchant");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Constants.orderstatus = "Success";

                        Intent io = new Intent(ClosingStockDashBoardActivity.this,
                                AddMerchantNew.class);
                        startActivity(io);
                        finish();
                    }
                }


            }
        });

        linearLayoutAcknowledge.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    getDetails("acknowledge");
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }


            }
        });

        linearLayoutStockEntry.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;

                if (netCheck() == true) {
                    getDetails("stockentry");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment");
                    }else {
                        Intent io = new Intent(ClosingStockDashBoardActivity.this,
                                OutletStockEntry.class);
                        startActivity(io);
                        finish();
                    }
                }


            }
        });
        linearLayoutDistStock.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                 if (netCheck()) {
                    getDetails("diststock");
                } else {
                     if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                         showAlertDialogToast("Please make payment");
                     } else {
                         Intent io = new Intent(ClosingStockDashBoardActivity.this,
                                 DistributrStockActivity.class);
                         startActivity(io);
                         finish();
                     }
                }


            }
        });
        linearLayoutdownloadscheme.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck()) {

                    final Dialog qtyDialog = new Dialog(ClosingStockDashBoardActivity.this);
                    qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    qtyDialog.setContentView(R.layout.warning_dialog);

                    final Button buttonUpdateOk, buttonUpdateCancel;

                    final TextView textQtyValidate;

                    buttonUpdateOk = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateOk);
                    buttonUpdateCancel = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateCancel);

                    textQtyValidate = (TextView) qtyDialog
                            .findViewById(R.id.textQtyValidate);
                    qtyDialog.show();


                    buttonUpdateOk.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            databaseHandler.deletescheme();
                            Constants.downloadScheme = "clientvisit";
                            Intent io = new Intent(ClosingStockDashBoardActivity.this, SchemeDownload.class);
                            startActivity(io);
                            finish();
                            qtyDialog.dismiss();
                        }

                    });

                    buttonUpdateCancel
                            .setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub

                                    qtyDialog.dismiss();

                                }
                            });

                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });
        linearLayoutsurveyuser.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheck()) {
                    getDetails("surveyuser");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent to = new Intent(ClosingStockDashBoardActivity.this, SurveyMerchant.class);
                        startActivity(to);
                        finish();
                    }
                }

            }
        });

        linearLayoutMyDayPlan.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    getDetails("mydayplan");
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }


            }
        });

        linearLayoutMySales.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    getDetails("mysales");
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }


            }
        });

        linearLayoutSignout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                // TODO Auto-generated method stub
                databaseHandler.delete();
                Intent io = new Intent(ClosingStockDashBoardActivity.this,
                        SigninActivity.class);
                startActivity(io);
                finish();
            }
        });

        linearLayoutMigration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Intent io = new Intent(ClosingStockDashBoardActivity.this,
                        PendingDataMigrationActivity.class);
                startActivity(io);
            }
        });

        linearLayoutStockOnly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
            }
        });

        linearLayoutStockAudit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().setTemplate8ForTemplate2(false);
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Constants.checkproduct = 0;
                Constants.orderid = "";
                Constants.SalesMerchant_Id = "";
                Constants.Merchantname = "";
                if (netCheck()) {
                    Intent io = new Intent(ClosingStockDashBoardActivity.this,
                            ClosingStockAudit.class);
                    startActivity(io);
                    finish();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals("") || PaymentStatus.equalsIgnoreCase("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(ClosingStockDashBoardActivity.this,
                                ClosingStockAudit.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });

        linearlayoutSearchIcon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (searchclick == "0") {
                    textViewHeader.setVisibility(TextView.GONE);
                    editTextSearchField.setVisibility(EditText.VISIBLE);
                    editTextSearchField.setFocusable(true);
                    editTextSearchField.requestFocus();
                    textViewCross.setVisibility(TextView.VISIBLE);
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    textViewCross.setVisibility(TextView.VISIBLE);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                    Log.e("Keyboard", "Show");
                    searchclick = "1";
                } else {
                    textViewHeader.setVisibility(TextView.VISIBLE);
                    editTextSearchField.setVisibility(EditText.GONE);
                    textViewCross.setVisibility(TextView.GONE);
                    textViewCross.setVisibility(TextView.GONE);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(linearlayoutSearchIcon.getWindowToken(), 0);
                    searchclick = "0";
                }

            }
        });

        /////////////////
        showMenu();
        ///////////////
    }

    public void initialcount() {
        String date = "yyyy-MM-dd";

        SimpleDateFormat inputFormat = new SimpleDateFormat(date);
        String dt = inputFormat.format(new Date());
        Cursor curs;

        curs = databaseHandler.getStockSuccessheader("Success", dt);
        Count = String.valueOf(curs.getCount());
        Log.e("ButtonSuccess", Count);
        ButtonSuccess.setText("Success" + "(" + Count + ")");


        curs = databaseHandler.getStockSuccessheader("Pending", dt);
        Count = String.valueOf(curs.getCount());
        Log.e("ButtonPending", Count);
        ButtonPending.setText("Pending" + "(" + Count + ")");

        curs = databaseHandler.getStockSuccessheader("Failed", dt);
        Count = String.valueOf(curs.getCount());
        Log.e("ButtonFailure", Count);
        ButtonFailure.setText("Failed" + "(" + Count + ")");
    }

    public void MyOrdersList(String status){
        final   JSONArray resultSet = new JSONArray();

        String date = "yyyy-MM-dd";

        SimpleDateFormat inputFormat = new SimpleDateFormat(date);
        String dt =inputFormat.format(new Date());


        Cursor curs;
        curs = databaseHandler.getStockSuccessheader(status,dt);

        Log.e("HeaderCount", String.valueOf(curs.getCount()));
        Count= String.valueOf(curs.getCount());
        Constants.totalorders=Count;
        if(status.equals("Success")){
            ButtonSuccess.setText("Success"+"("+Count+")");
        }else if(status.equals("Pending")){
            ButtonPending.setText("Pending"+"("+Count+")");
        }else if(status.equals("Failed")){
            ButtonFailure.setText("Failed"+"("+Count+")");
        }
        curs.moveToFirst();
        while (curs.isAfterLast() == false) {

            int totalColumn = curs.getColumnCount();
            JSONObject rowObject = new JSONObject();

            for( int i=0 ;  i<totalColumn  ; i++ )
            {
                if( curs.getColumnName(i) != null  )
                {

                    try
                    {

                        if( curs.getString(i) != null )
                        {
                            Log.d("TAG_NAME", curs.getString(i) );
                            rowObject.put(curs.getColumnName(i) ,  curs.getString(i) );
                        }
                        else
                        {
                            rowObject.put( curs.getColumnName(i) ,  "" );
                        }
                    }
                    catch( Exception e )
                    {
                        Log.d("TAG_NAME", e.getMessage()  );
                    }
                }

            }

            Log.e("jsonarray-sk", rowObject.toString());

            JSONArray resultDetail 	= new JSONArray();
            try{

                Cursor dtlcursor;
                dtlcursor = databaseHandler.getStockOrderDetail(rowObject.getString("oflnordid"));
                // System.out.println("SHICURPRODUCT:");
                // System.out.println(databaseHandler.getCursorItems(dtlcursor));
                // Log.e("oflnordid",rowObject.getString("oflnordid"));
                // Log.e("Cursor Count", String.valueOf(dtlcursor.getCount()));
                dtlcursor.moveToFirst();
                while (dtlcursor.isAfterLast() == false) {

                    int Columncount = dtlcursor.getColumnCount();
                    JSONObject detailObject = new JSONObject();

                    for( int i=0 ;  i<Columncount  ; i++ )
                    {
                        if( dtlcursor.getColumnName(i) != null  )
                        {

                            try
                            {

                                if( dtlcursor.getString(i) != null )
                                {
                                    Log.d("TAG_NAME", dtlcursor.getColumnName(i)+" : "+dtlcursor.getString(i) );
                                    detailObject.put(dtlcursor.getColumnName(i) ,  dtlcursor.getString(i) );
                                }
                                else
                                {
                                    detailObject.put( dtlcursor.getColumnName(i) ,  "" );
                                }
                            }
                            catch( Exception e )
                            {
                                Log.d("TAG_NAME", e.getMessage()  );
                            }
                        }

                    }

                    resultDetail.put(detailObject);
                    dtlcursor.moveToNext();
                }

                dtlcursor.close();

                Log.e("Detailarray", resultDetail.toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }
            try{
                rowObject.put("orderdtls",resultDetail);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            resultSet.put(rowObject);
            curs.moveToNext();
        }
        curs.close();


        Log.e("SaveArray", resultSet.toString());
        int c=Integer.parseInt(Count);
        if(c!=0){

            try{
                Log.e("inside", "inside");
                arraylistDealerOrderList=new ArrayList<DealerOrderListDomain>();
                arraylistSearchResults=new ArrayList<DealerOrderListDomain>();
                for (int i = 0; i < resultSet.length(); i++) {
                    JSONObject job = new JSONObject();
                    job=resultSet.getJSONObject(i);
                    DealerOrderListDomain dod=new DealerOrderListDomain();

                    if( job.getString("pushstatus").equals("Success")){

                        dod.setAddress(job.getString("onlineorderno"));
                        Log.e("onlineorderno",dod.getAddress());
                    }else{

                        dod.setAddress( " T "+ job.getString("oflnordid"));
                        Log.e("oflnordid", dod.getAddress());
                    }

                    dod.setComName(job.getString("mname"));

                    dod.setrowid(job.getJSONArray("orderdtls").getJSONObject(0).getString("rowid"));
                    Log.e("rowid", job.getJSONArray("orderdtls").getJSONObject(0).getString("rowid"));
                    String rowid=job.getJSONArray("orderdtls").getJSONObject(0).getString("rowid");
                    dod.setOrderid(job.getString("oflnordid"));
                    dod.setMuserid(job.getString("muserid"));
                    dod.setFullname(job.getJSONArray("orderdtls").getJSONObject(0).getString("duserid"));
                    dod.setSerialno(job.getJSONArray("orderdtls").getJSONObject(0).getString("ordslno"));
                    dod.setMname(job.getString("mname"));
                    dod.setisread(job.getJSONArray("orderdtls").getJSONObject(0).getString("isread"));
                    dod.setserialnonew(String.valueOf(i + 1));
                    Log.e("ordimage", job.getJSONArray("orderdtls").getJSONObject(0).getString("ordimage"));
                    if(!job.getJSONArray("orderdtls").getJSONObject(0).getString("ordimage").equals("0")){
                        dod.setorderimage(job.getJSONArray("orderdtls").getJSONObject(0).getString("ordimage"));
                    }else{
                        dod.setorderimage("0");
                    }


                    time=job.getString("orderdt");
                    Log.e("time",time);
                    String inputPattern1 = "yyyy-MM-dd";
                    String outputPattern = "dd-MMM-yyyy ";
                    SimpleDateFormat inputFormat1 = new SimpleDateFormat(inputPattern1);
                    SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

                    String str = null;

                    try {
                        dateStr = inputFormat1.parse(time);
                        str = outputFormat.format(dateStr);
                        dod.setDate(str);
                        //Log.e("str", str);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    arraylistDealerOrderList.add(dod);
                    arraylistSearchResults.add(dod);

                    Log.e("Size1",String.valueOf(arraylistDealerOrderList.size()));

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            setadap();
        }else{
            Log.e("outside", "outside");
            listViewOrders.setVisibility(View.GONE);
            textViewNodata.setVisibility(View.VISIBLE);
            if(status.equals("Success")){
                textViewNodata.setText("No items");
            }else if(status.equals("Pending")){
                textViewNodata.setText("No Pending items");
            }else if(status.equals("Failed")){
                textViewNodata.setText("No Failed items");
            }

            //buttonPlaceOrder.setVisibility(Button.VISIBLE);
            linearlayoutSearchIcon.setClickable(false);
        }
    }

    public void setadap() {
        // TODO Auto-generated method stub

        adapter=new DealerOrderListAdapter(context, R.layout.item_dealer_order_screen, arraylistDealerOrderList);
        listViewOrders.setVisibility(View.VISIBLE);
        listViewOrders.setAdapter(adapter);
        textViewNodata.setVisibility(View.GONE);


    }

    protected void toastDisplay(String msg) {
        Toast toast = Toast.makeText(ClosingStockDashBoardActivity.this, msg,
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();

    }

    public boolean netCheck() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            } else if (result == null) {
               /* showAlertDialog(SalesManOrderActivity.this,
                        "No Internet Connection",
                        "Please Check Your internet connection.", false);*/
                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }


    private void getDetails(final String paymentcheck) {
        if (paymentcheck.equals("profile")) {
            Intent io = new Intent(ClosingStockDashBoardActivity.this,
                    ProfileActivity.class);
            startActivity(io);
            finish();

        } else if (paymentcheck.equals("myorders")) {
            Intent io = new Intent(ClosingStockDashBoardActivity.this,
                    SalesManOrderActivity.class);
            startActivity(io);
            finish();

        } else if (paymentcheck.equals("mydealer")) {
            Intent io = new Intent(ClosingStockDashBoardActivity.this,
                    MyDealersActivity.class);
            io.putExtra("Key", "menuclick");
            startActivity(io);
            finish();

        } else if (paymentcheck.equals("postnotes")) {
            Intent io = new Intent(ClosingStockDashBoardActivity.this,
                    MerchantComplaintActivity.class);
            startActivity(io);
            finish();

        } else if (paymentcheck.equals("placeorder")) {
            Intent to = new Intent(ClosingStockDashBoardActivity.this,
                    SalesManOrderCheckoutActivity.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("clientvisit")) {
            Intent to = new Intent(ClosingStockDashBoardActivity.this,
                    SMClientVisitHistory.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("paymentcoll")) {
            Intent to = new Intent(ClosingStockDashBoardActivity.this,
                    SalesmanPaymentCollectionActivity.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("Createorder")) {
            Intent to = new Intent(ClosingStockDashBoardActivity.this,
                    CreateOrderActivity.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("addmerchant")) {
            Intent to = new Intent(ClosingStockDashBoardActivity.this,
                    AddMerchantNew.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("acknowledge")) {
            Intent to = new Intent(ClosingStockDashBoardActivity.this,
                    SalesmanAcknowledgeActivity.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("stockentry")) {
            Intent to = new Intent(ClosingStockDashBoardActivity.this,
                    OutletStockEntry.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("surveyuser")) {
            Intent to = new Intent(ClosingStockDashBoardActivity.this,
                    SurveyMerchant.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("mydayplan")) {
            Intent to = new Intent(ClosingStockDashBoardActivity.this,
                    MyDayPlan.class);
            startActivity(to);
            finish();
        }else if (paymentcheck.equals("mysales")) {
            Intent to = new Intent(ClosingStockDashBoardActivity.this,
                    MySalesActivity.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("diststock")) {
            Intent io = new Intent(ClosingStockDashBoardActivity.this,
                    DistributrStockActivity.class);
            startActivity(io);
            finish();
        }
    }

    public void showAlertDialogToast(String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    public boolean netCheckwithoutAlert() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            } else if (result == null) {

                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public void onBackPressed() {
        exitAlret();
    }

    private void exitAlret() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        localBuilder.setMessage("Do you want to Exit?");
        localBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {

                        finish();

                    }
                });
        localBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {
                        paramDialogInterface.cancel();
                    }
                });
        localBuilder.create().show();
    }
}
