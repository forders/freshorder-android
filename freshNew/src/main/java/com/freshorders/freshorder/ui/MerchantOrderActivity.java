package com.freshorders.freshorder.ui;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.DealerOrderListAdapter;
import com.freshorders.freshorder.adapter.MerchantOrderListAdapter;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.AppPaymentHistoryDomain;
import com.freshorders.freshorder.domain.DealerOrderDetailDomain;
import com.freshorders.freshorder.domain.DealerOrderListDomain;
import com.freshorders.freshorder.domain.MerchantOrderListDomain;
import com.freshorders.freshorder.domain.ViewPagerDomain;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class MerchantOrderActivity extends Activity {

	TextView menuIcon, textViewAssetMenuProfile, textViewAssetMenuMyOrders,
			textViewAssetMenuMyDealers, textViewAssetMenuProducts,
			textViewAssetMenuPayment, textViewAssetMenuComplaint,
			textViewAssetMenuSignout,textViewAssetMenuCreateOrder;
	static TextView textViewNodata;
	TextView textViewHeader;
	TextView textViewAssetSearch,textViewCross;
	// RelativeLayout relativeLayoutMenuParent;
	int menuCliccked;
	LinearLayout linearLayoutMenuParent, linearLayoutProfile,
			linearLayoutMyOrders, linearLayoutMyDealers, linearLayoutProducts,
			linearLayoutPayment, linearLayoutComplaint, linearLayoutSignout,
			linearlayoutSearchIcon,linearLayoutCreateOrder;
	JSONObject JsonAccountObject = null;
	JSONArray JsonAccountArray = null;
	DatabaseHandler databaseHandler;
	JsonServiceHandler JsonServiceHandler;
	static ArrayList<MerchantOrderListDomain> arraylistMerchadntOrderList;
	ArrayList<MerchantOrderListDomain> arraylistSearcResultsList;
	static ListView listViewOrders;
	Date dateStr = null;
	public static String time, searchclick = "0",paymentcheck="NULL";
	Button buttonAddPlaceOrder,SendImage;
	public static String companyname, fullname, date, address, orderId,duserid,PaymentStatus="null";
	EditText editTextSearchField;
	public static String strMsg = "";
	public static MerchantOrderListAdapter adapter;
	public static MerchantOrderListDomain domainSelected;
	public static int selectedPosition;
	ImageView imageView;
	public static String[] mThumbIds;
	public static int imageTotal;
	ContentResolver contentResolver;
	public  static ArrayList<Bitmap> arrayListimagebitmap;
	public  static ArrayList<String> arraylistimagepath;
	public static  ArrayList<ViewPagerDomain> arraylistPdoductImages;
	public static ArrayList<DealerOrderDetailDomain> arraylistDealerOrderDetailList;
	public static ArrayList<AppPaymentHistoryDomain> arraypaymenthistory;
	public static   List<String> removedImage;
	File file;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(1);
		setContentView(R.layout.merchant_my_orders_screen);
		netCheck();
		databaseHandler = new DatabaseHandler(getApplicationContext());
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();
		arrayListimagebitmap=new ArrayList<Bitmap>();
		arraylistPdoductImages = new ArrayList<ViewPagerDomain>();
		removedImage =new ArrayList<String>();
		arraylistDealerOrderDetailList=new ArrayList<DealerOrderDetailDomain>();
		arraypaymenthistory=new ArrayList<AppPaymentHistoryDomain>();

		Cursor cur;
		cur = databaseHandler.getDetails();
		cur.moveToFirst();
		Log.e("Insertion Check",
				cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_username)));
		Log.e("Insertion Check",
				cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_password)));
		Log.e("Insertion Check",
				cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_usertype)));
		arraylistimagepath= new  ArrayList<String>();
		myOrders();

		textViewNodata = (TextView) findViewById(R.id.textViewNodata);
		listViewOrders = (ListView) findViewById(R.id.listViewOrders);
		linearLayoutCreateOrder = (LinearLayout) findViewById(R.id.linearLayoutCreateOrder);
		linearLayoutProfile = (LinearLayout) findViewById(R.id.linearLayoutProfile);
		linearLayoutMyOrders = (LinearLayout) findViewById(R.id.linearLayoutMyOrders);
		linearLayoutMyDealers = (LinearLayout) findViewById(R.id.linearLayoutMyDealers);
		linearLayoutProducts = (LinearLayout) findViewById(R.id.linearLayoutProducts);
		linearLayoutPayment = (LinearLayout) findViewById(R.id.linearLayoutPayment);
		linearLayoutComplaint = (LinearLayout) findViewById(R.id.linearLayoutComplaint);
		linearLayoutSignout = (LinearLayout) findViewById(R.id.linearLayoutSignout);
		buttonAddPlaceOrder = (Button) findViewById(R.id.buttonAddPlaceOrder);
		SendImage= (Button) findViewById(R.id.SendImage);
		linearlayoutSearchIcon = (LinearLayout) findViewById(R.id.linearlayoutSearchIcon);
		textViewHeader = (TextView) findViewById(R.id.textViewHeader);
		textViewCross=(TextView) findViewById(R.id.textViewCross);
		if (Constants.USER_TYPE.equals("D")) {
			linearLayoutMyDealers.setVisibility(View.GONE);
			linearLayoutProducts.setVisibility(View.VISIBLE);
			linearLayoutCreateOrder.setVisibility(View.GONE);
		} else if (Constants.USER_TYPE.equals("M")){
			linearLayoutMyDealers.setVisibility(View.VISIBLE);
			linearLayoutProducts.setVisibility(View.GONE);
			linearLayoutCreateOrder.setVisibility(View.GONE);
		}else {
			linearLayoutMyDealers.setVisibility(View.VISIBLE);
			linearLayoutProducts.setVisibility(View.GONE);
			linearLayoutCreateOrder.setVisibility(View.VISIBLE);
		}
		menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
		textViewAssetMenuCreateOrder= (TextView) findViewById(R.id.textViewAssetMenuCreateOrder);
		textViewAssetSearch = (TextView) findViewById(R.id.textViewAssetSearch);
		textViewAssetMenuProfile = (TextView) findViewById(R.id.textViewAssetMenuProfile);
		textViewAssetMenuMyOrders = (TextView) findViewById(R.id.textViewAssetMenuMyOrders);
		textViewAssetMenuMyDealers = (TextView) findViewById(R.id.textViewAssetMenuMyDealers);
		textViewAssetMenuProducts = (TextView) findViewById(R.id.textViewAssetMenuProducts);
		textViewAssetMenuPayment = (TextView) findViewById(R.id.textViewAssetMenuPayment);
		textViewAssetMenuComplaint = (TextView) findViewById(R.id.textViewAssetMenuComplaint);
		textViewAssetMenuSignout = (TextView) findViewById(R.id.textViewAssetMenuSignout);
		editTextSearchField = (EditText) findViewById(R.id.editTextSearchField);


		linearLayoutMenuParent = (LinearLayout) findViewById(R.id.linearLayoutMenuParent);
		Typeface font = Typeface.createFromAsset(getAssets(),
				"fontawesome-webfont.ttf");
		menuIcon.setTypeface(font);
		textViewAssetMenuCreateOrder.setTypeface(font);
		textViewAssetMenuProfile.setTypeface(font);
		textViewAssetSearch.setTypeface(font);
		textViewAssetMenuMyOrders.setTypeface(font);
		textViewAssetMenuMyDealers.setTypeface(font);
		textViewAssetMenuProducts.setTypeface(font);
		textViewAssetMenuPayment.setTypeface(font);
		textViewAssetMenuComplaint.setTypeface(font);
		textViewAssetMenuSignout.setTypeface(font);
		textViewCross.setTypeface(font);
		menuIcon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// TODO Auto-generated method stub
				/*InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(menuIcon.getWindowToken(), 0);*/
				if (menuCliccked == 0) {
					linearLayoutMenuParent.setVisibility(View.VISIBLE);
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(menuIcon.getWindowToken(), 0);
					editTextSearchField.setVisibility(EditText.GONE);
					textViewHeader.setVisibility(TextView.VISIBLE);
					textViewCross.setVisibility(TextView.GONE);
					menuCliccked = 1;
				} else {
					linearLayoutMenuParent.setVisibility(View.GONE);
					textViewHeader.setVisibility(TextView.VISIBLE);
					editTextSearchField.setVisibility(EditText.GONE);
					textViewCross.setVisibility(TextView.GONE);
					menuCliccked = 0;
				}

			}
		});


		SendImage.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {


				Intent to = new Intent(MerchantOrderActivity.this,
						CameraPhotoCapture.class);
				startActivity(to);
				finish();
			}
		});
		linearlayoutSearchIcon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (searchclick == "0") {
					textViewHeader.setVisibility(TextView.GONE);
					editTextSearchField.setVisibility(EditText.VISIBLE);
					editTextSearchField.setFocusable(true);
					editTextSearchField.requestFocus();
					textViewCross.setVisibility(TextView.VISIBLE);
					linearLayoutMenuParent.setVisibility(View.GONE);
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
					Log.e("Keyboard", "Show");
					searchclick = "1";
				} else {
					textViewHeader.setVisibility(TextView.VISIBLE);
					editTextSearchField.setVisibility(EditText.GONE);
					textViewCross.setVisibility(TextView.GONE);
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(linearlayoutSearchIcon.getWindowToken(), 0);
					searchclick = "0";
				}

			}
		});

		editTextSearchField.addTextChangedListener(new TextWatcher() {

			public void afterTextChanged(Editable s) {
				arraylistMerchadntOrderList = new ArrayList<MerchantOrderListDomain>();
				String searchText = editTextSearchField.getText().toString()
						.toLowerCase(Locale.getDefault());
				/*Button btn = (Button) findViewById(R.id.clear_text);
				btn.setOnClickListener(new View.OnClickListener(){
					@Override
					public void onClick(View v)
					{
						EditText et = (EditText) findViewById(R.id.editTextSearchField);
						et.setText("");
					}
				});*/
				for (MerchantOrderListDomain pd : arraylistSearcResultsList) {
					if (pd.getComName().toLowerCase(Locale.getDefault())
							.contains(searchText)) {
						arraylistMerchadntOrderList.add(pd);
					}
				}
				MerchantOrderActivity.this.runOnUiThread(new Runnable() {

					@Override
					public void run() {
						adapter = new MerchantOrderListAdapter(
								MerchantOrderActivity.this,
								R.layout.item_m_order_list,
								arraylistMerchadntOrderList);
						listViewOrders.setAdapter(adapter);
					}
				});

			}

			public void beforeTextChanged(CharSequence s, int start, int count,
										  int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before,
									  int count) {
				textViewCross.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						editTextSearchField.setText("");
						if(editTextSearchField.equals("")){
							InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
							imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
						}
					}
				});

			}
		});

		listViewOrders.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
									int position, long id) {
				// TODO Auto-generated method stub
				Constants.adapterset=0;
				domainSelected = arraylistMerchadntOrderList.get(position);
				companyname = arraylistMerchadntOrderList.get(position)
						.getFullname();
				fullname = arraylistMerchadntOrderList.get(position)
						.getComName();
				date = arraylistMerchadntOrderList.get(position).getDate();
				address = arraylistMerchadntOrderList.get(position)
						.getAddress();
				orderId = arraylistMerchadntOrderList.get(position)
						.getOrderid();
				duserid = arraylistMerchadntOrderList.get(position)
						.getDuserid();
				Constants.dealerid=arraylistMerchadntOrderList.get(position)
						.getDuserid();

				Log.e("position", String.valueOf(position));

				InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(listViewOrders.getWindowToken(), 0);
				Intent to = new Intent(MerchantOrderActivity.this,
						MerchantSingleDetailActivity.class);
				startActivity(to);
				finish();
            	/*selectedPosition = position;
				listViewOrders.setAdapter(adapter);
				listViewOrders.setSelectionFromTop(position, 1);*/


			}
		});







		buttonAddPlaceOrder.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// TODO Auto-generated method stub
				Constants.dealerid="";
				Constants.checkproduct =0;
				Constants.orderid="";
				Log.e("orderidclick", Constants.orderid);

				if( netCheckwithoutAlert()==true){
					getDetails("placeorder");
				}else{

					showAlertDialogToast("Please Check Your internet connection");
				}

			}
		});

		linearLayoutMenuParent.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
			}
		});

		linearLayoutProfile.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;

				if( netCheckwithoutAlert()==true){
					getDetails("profile");
				}else{

					showAlertDialogToast("Please Check Your internet connection");
				}


			}
		});

		linearLayoutMyOrders.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				Constants.checkproduct = 0;
				// TODO Auto-generated method stub
				/*
				 * if (Constants.USER_TYPE.equals("D")) { Intent io = new
				 * Intent(MerchantOrderActivity.this,
				 * DealersOrderActivity.class); startActivity(io); } else {
				 * Intent io = new Intent(MerchantOrderActivity.this,
				 * MerchantOrderActivity.class); startActivity(io); }
				 */

			}
		});

		linearLayoutMyDealers.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				// TODO Auto-generated method stub

				if( netCheckwithoutAlert()==true){
					getDetails("mydealer");
				}else{

					showAlertDialogToast("Please Check Your internet connection");
				}

			}
		});

		linearLayoutProducts.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				// TODO Auto-generated method stub
				Intent io = new Intent(MerchantOrderActivity.this,
						ProductActivity.class);
				startActivity(io);
				finish();
			}
		});

		linearLayoutCreateOrder.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				// TODO Auto-generated method stub
				Intent io = new Intent(MerchantOrderActivity.this,
						CreateOrderActivity.class);
				startActivity(io);
				finish();
			}
		});

		linearLayoutPayment.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				if( netCheckwithoutAlert()==true){
					Intent io = new Intent(MerchantOrderActivity.this,
							PaymentActivity.class);
					startActivity(io);
					finish();
				}else{

					showAlertDialogToast("Please Check Your internet connection");
				}

			}
		});

		linearLayoutComplaint.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;

				//if( netCheckwithoutAlert()==true){
					getDetails("postnotes");
				//}else{

				//	showAlertDialogToast("Please Check Your internet connection");
				//}


			}
		});

		linearLayoutSignout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				// TODO Auto-generated method stub
				databaseHandler.delete();
				Intent io = new Intent(MerchantOrderActivity.this,
						SigninActivity.class);
				startActivity(io);
				finish();
			}
		});
	}



	private void getDetails(final String paymentcheck) {
		if(paymentcheck.equals("profile")){
			Intent io = new Intent(MerchantOrderActivity.this,
					ProfileActivity.class);
			startActivity(io);
			finish();

		}else if(paymentcheck.equals("mydealer")){
			Intent io = new Intent(MerchantOrderActivity.this,
					MyDealersActivity.class);
			io.putExtra("Key","menuclick");
			startActivity(io);
			finish();

		}else if(paymentcheck.equals("postnotes")){
			Intent io = new Intent(MerchantOrderActivity.this,
					MerchantComplaintActivity.class);
			startActivity(io);
			finish();

		}else if(paymentcheck.equals("placeorder")){
			Intent to = new Intent(MerchantOrderActivity.this,
					MerchantOrderDetailActivity.class);
			startActivity(to);
			finish();
		}
	}


	private void myOrders() {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";

			@Override
			protected void onPreExecute() {
				dialog = ProgressDialog.show(MerchantOrderActivity.this, "",
						"Loading...", true, true);

			}

			@Override
			protected void onPostExecute(Void result) {

				try {

					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);

					if (strStatus.equals("true")) {

						JSONArray job1 = JsonAccountObject.getJSONArray("data");

						arraylistMerchadntOrderList = new ArrayList<MerchantOrderListDomain>();
						arraylistSearcResultsList = new ArrayList<MerchantOrderListDomain>();
						for (int i = 0; i < job1.length(); i++) {
							JSONObject job = new JSONObject();
							job = job1.getJSONObject(i);
							MerchantOrderListDomain dod = new MerchantOrderListDomain();
							dod.setOrderid(job.getString("ordid"));
							dod.setDuserid(job.getString("duserid"));
							dod.setFullname(job.getString("fullname"));
							dod.setComName(job.getString("companyname"));
							dod.setisread(job.getString("isread"));
							dod.setserialno(String.valueOf(i+1));
							// dod.setDate(job.getString("orderdt"));
							time = job.getString("orderdt");


							String inputPattern = "yyyy-MM-dd";
							String outputPattern = "dd-MMM-yyyy ";
							SimpleDateFormat inputFormat = new SimpleDateFormat(
									inputPattern);
							SimpleDateFormat outputFormat = new SimpleDateFormat(
									outputPattern);

							String str = null;

							try {
								dateStr = inputFormat.parse(time);
								str = outputFormat.format(dateStr);
								dod.setDate(str);
								// Log.e("str", str);
							} catch (ParseException e) {
								e.printStackTrace();
							}

							dod.setAddress(job.getString("address"));
							arraylistMerchadntOrderList.add(dod);
							arraylistSearcResultsList.add(dod);



						}
						setadap();
					} else {
						listViewOrders.setVisibility(View.GONE);
						textViewNodata.setVisibility(View.VISIBLE);
						textViewNodata.setText(strMsg);
						linearlayoutSearchIcon.setClickable(false);
						textViewAssetSearch.setVisibility(TextView.VISIBLE);

					}

					dialog.dismiss();
				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					//Log.e("ProductActivityException", e.toString());
				}

			}

			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();

				try {
					jsonObject.put("muserid",Constants.USER_ID);
					jsonObject.put("usertype",Constants.USER_TYPE);
				} catch (JSONException e) {
					e.printStackTrace();
				}


				JsonServiceHandler = new JsonServiceHandler(
						Utils.strMerchantOrderList  ,jsonObject.toString(),
						MerchantOrderActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceData();
				return null;
			}
		}.execute(new Void[]{});
	}


	public void setadap() {
		// TODO Auto-generated method stub
		for(int i=0;i<arraylistMerchadntOrderList.size();i++){
			Log.e("second",arraylistMerchadntOrderList.get(i).getOrderid());
		}
		adapter = new MerchantOrderListAdapter(MerchantOrderActivity.this,
				R.layout.item_m_order_list, arraylistMerchadntOrderList);
		listViewOrders.setVisibility(View.VISIBLE);
		listViewOrders.setAdapter(adapter);
		textViewNodata.setVisibility(View.GONE);

	}


	protected void toastDisplay(String msg) {
		Toast toast = Toast.makeText(MerchantOrderActivity.this, msg,
				Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

	}

	public boolean netCheck() {
		// for network connection
		try {
			ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetwork = connectionManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			Object result = null;
			if (mWifi.isConnected() || mNetwork.isConnected()) {
				return true;
			}

			else if (result == null) {
				showAlertDialog(MerchantOrderActivity.this,
						"No Internet Connection",
						"Please Check Your internet connection.", false);
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public void showAlertDialog(Context context, String title, String message,
								Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
					}
				});
		alertDialog.show();
	}

	public void showAlertDialogToast( String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();

		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

	}
	public boolean netCheckwithoutAlert() {
		// for network connection
		try {
			ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetwork = connectionManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			Object result = null;
			if (mWifi.isConnected() || mNetwork.isConnected()) {
				return true;
			}

			else if (result == null) {

				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	public void onBackPressed() {
		exitAlret();
	}

	private void exitAlret() {
		AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
		localBuilder.setCancelable(false);
		localBuilder.setMessage("Do you want to Exit?");
		localBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {

						finish();

					}
				});
		localBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {
						paramDialogInterface.cancel();
					}
				});
		localBuilder.create().show();
	}
}
