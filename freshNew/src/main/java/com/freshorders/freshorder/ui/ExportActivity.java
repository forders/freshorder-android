package com.freshorders.freshorder.ui;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import androidx.fragment.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.SalesmanListDomain;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Jayesh on 07-07-2016.
 */
public class ExportActivity extends FragmentActivity {
    EditText EditTextToDate, EditTextFromDate;
    Button Export;
    public static TextView textViewAssetMenu,textViewHeader;
    JSONObject JsonAccountObject = null;
    JSONArray JsonAccountArray = null;
    public static Date dateStr = null;
    private int year,hr,sec,mins;
    public int monthnew,newmnth,newyear,yearnew,newday,daynew;

    private int day,today;
    DatePicker datePicker;
    public static int i=0,k=0,size=0;
   public int selectslaes=0;
    public  static Integer[] selectedSalesman,cname;
    public static int menuCliccked,selectedPosition;
    LinearLayout linearLayoutMenuParent, linearLayoutProfile,
            linearLayoutMyOrders, linearLayoutMyDealers, linearLayoutProducts,
            linearLayoutPayment, linearLayoutComplaint, linearLayoutSignout,
            linearLayoutCreateOrder,linearLayoutExport,linearLayoutClientVisit;

    TextView  textViewAssetMenuProfile, textViewAssetMenuMyOrders,
            textViewAssetMenuMyDealers, textViewAssetMenuProducts,
            textViewAssetMenuPayment, textViewAssetMenuComplaint,
            textViewAssetMenuSignout,textViewAssetMenuCreateOrder,
            textViewAssetMenuExport,textViewAssetMenuSClientVisit;
    JsonServiceHandler JsonServiceHandler;
    protected Button selectColoursButton;
    DatabaseHandler databaseHandler;
    public static String strurl,fromdate,todate,downloadFileName,email,PaymentStatus="NULL";
    ArrayList<SalesmanListDomain> arraylistsaleslist;
    private List<String> saleslist;
    String[] sales;
    protected String[] colours;
    protected ArrayList<String> selectedColours  = new ArrayList<String>();
    public static DialogInterface.OnMultiChoiceClickListener coloursDialogListener;
    public static boolean[] checkedColours;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.export_screen);

        netCheck();
        getsaleslist();
        datePicker= (DatePicker) findViewById(R.id.datePicker1);
        EditTextFromDate = (EditText) findViewById(R.id.EditTextFromDate);
        EditTextToDate = (EditText) findViewById(R.id.EditTextToDate);
        textViewAssetMenu= (TextView) findViewById(R.id.textViewAssetMenu);
        textViewHeader= (TextView) findViewById(R.id.textViewHeader);
        Export = (Button) findViewById(R.id.Export);

        showCurrentDateOnView();
        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();
        linearLayoutCreateOrder = (LinearLayout) findViewById(R.id.linearLayoutCreateOrder);
        linearLayoutProfile = (LinearLayout) findViewById(R.id.linearLayoutProfile);
        linearLayoutMyOrders = (LinearLayout) findViewById(R.id.linearLayoutMyOrders);
        linearLayoutMyDealers = (LinearLayout) findViewById(R.id.linearLayoutMyDealers);
        linearLayoutProducts = (LinearLayout) findViewById(R.id.linearLayoutProducts);
        linearLayoutPayment = (LinearLayout) findViewById(R.id.linearLayoutPayment);
        linearLayoutComplaint = (LinearLayout) findViewById(R.id.linearLayoutComplaint);
        linearLayoutSignout = (LinearLayout) findViewById(R.id.linearLayoutSignout);
        linearLayoutExport= (LinearLayout) findViewById(R.id.linearLayoutExport);
        linearLayoutClientVisit= (LinearLayout) findViewById(R.id.linearLayoutClientVisit);
        if (Constants.USER_TYPE.equals("D")) {
            linearLayoutMyDealers.setVisibility(View.GONE);
            linearLayoutProducts.setVisibility(View.VISIBLE);
            linearLayoutCreateOrder.setVisibility(View.GONE);
            linearLayoutExport.setVisibility(View.VISIBLE);
        } else if (Constants.USER_TYPE.equals("M")){
            linearLayoutMyDealers.setVisibility(View.VISIBLE);
            linearLayoutProducts.setVisibility(View.GONE);
            linearLayoutCreateOrder.setVisibility(View.GONE);
            linearLayoutExport.setVisibility(View.GONE);
        }else {
            linearLayoutMyDealers.setVisibility(View.GONE);
            linearLayoutProducts.setVisibility(View.GONE);
            linearLayoutCreateOrder.setVisibility(View.VISIBLE);
            linearLayoutExport.setVisibility(View.GONE);
        }
        textViewAssetMenuCreateOrder= (TextView) findViewById(R.id.textViewAssetMenuCreateOrder);
        textViewAssetMenuProfile = (TextView) findViewById(R.id.textViewAssetMenuProfile);
        textViewAssetMenuMyOrders = (TextView) findViewById(R.id.textViewAssetMenuMyOrders);
        textViewAssetMenuMyDealers = (TextView) findViewById(R.id.textViewAssetMenuMyDealers);
        textViewAssetMenuProducts = (TextView) findViewById(R.id.textViewAssetMenuProducts);
        textViewAssetMenuPayment = (TextView) findViewById(R.id.textViewAssetMenuPayment);
        textViewAssetMenuComplaint = (TextView) findViewById(R.id.textViewAssetMenuComplaint);
        textViewAssetMenuSignout= (TextView) findViewById(R.id.textViewAssetMenuSignout);
        textViewAssetMenuExport= (TextView) findViewById(R.id.textViewAssetMenuExport);
        linearLayoutMenuParent = (LinearLayout) findViewById(R.id.linearLayoutMenuParent);
        textViewAssetMenuSClientVisit= (TextView) findViewById(R.id.textViewAssetMenuSClientVisit);
        selectColoursButton = (Button) findViewById(R.id.select_colours);
        Typeface font = Typeface.createFromAsset(getAssets(),
                "fontawesome-webfont.ttf");
        textViewAssetMenu.setTypeface(font);
        textViewAssetMenuCreateOrder.setTypeface(font);
        if(Constants.filter.equals("1")){
            textViewAssetMenu.setText(R.string.back_icon);
            textViewAssetMenu.setTypeface(font);
            textViewAssetMenu.setTextSize(15);
        }
        textViewAssetMenuProfile.setTypeface(font);
        textViewAssetMenuMyOrders.setTypeface(font);
        textViewAssetMenuMyDealers.setTypeface(font);
        textViewAssetMenuProducts.setTypeface(font);
        textViewAssetMenuPayment.setTypeface(font);
        textViewAssetMenuComplaint.setTypeface(font);
        textViewAssetMenuSignout.setTypeface(font);
        textViewAssetMenuExport.setTypeface(font);
        textViewAssetMenuSClientVisit.setTypeface(font);
        databaseHandler = new DatabaseHandler(getApplicationContext());
        Cursor cur;
        cur = databaseHandler.getDetails();
        cur.moveToFirst();
        Log.e("Insertion Check", cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_email)));
         email = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_email));


        if(Constants.filter.equals("1")){
            textViewHeader.setText("Salesmen Client Visit");
        }


        selectColoursButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch(view.getId()) {

                    case R.id.select_colours:

                        showSelectColoursDialog();

                        break;

                    default:

                        break;

                }
            }
        });

        textViewAssetMenu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if(Constants.filter.equals("1")){
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Intent io = new Intent(ExportActivity.this,
                            DealerClientVisit.class);
                    startActivity(io);
                    finish();
                }else {

                    if (menuCliccked == 0) {
                        linearLayoutMenuParent.setVisibility(View.VISIBLE);
                        menuCliccked = 1;
                    } else {
                        linearLayoutMenuParent.setVisibility(View.GONE);
                        menuCliccked = 0;
                    }
                }


            }
        });

        linearLayoutMenuParent.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
            }
        });

        linearLayoutProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if(netCheckWithoutAlert()==true){
                    getDetails("profile");
                }else{
                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });

        linearLayoutExport.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;

            }
        });


        linearLayoutMyOrders.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                //if(netCheckWithoutAlert()==true){
                    getDetails("myorders");
                //}
                //else{
                 //   showAlertDialogToast("Please Check Your internet connection");
                //}

            }
        });

        linearLayoutMyDealers.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;

                // TODO Auto-generated method stub
                Intent io = new Intent(ExportActivity.this,
                        MyDealersActivity.class);
                io.putExtra("Key","MenuClick");
                startActivity(io);
                finish();
            }
        });

        linearLayoutProducts.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if(netCheckWithoutAlert()==true){
                    getDetails("product");
                }
                else{
                    showAlertDialogToast("Please Check Your internet connection");
                }
            }
        });

        linearLayoutCreateOrder.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;

                // TODO Auto-generated method stub
                Intent io = new Intent(ExportActivity.this,
                        CreateOrderActivity.class);
                startActivity(io);
                finish();
            }
        });

        linearLayoutPayment.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if(netCheckWithoutAlert()==true){
                    Intent io = new Intent(ExportActivity.this,
                            DealerPaymentActivity.class);
                    startActivity(io);
                    finish();
                }
                else{
                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });

        linearLayoutComplaint.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                //if(netCheckWithoutAlert()==true){
                    getDetails("postnotes");
                //}
                //else{
                //    showAlertDialogToast("Please Check Your internet connection");
                //}

            }
        });

        linearLayoutClientVisit.setOnClickListener(new View.OnClickListener() {

          @Override
           public void onClick(View v) {
        linearLayoutMenuParent.setVisibility(View.GONE);
        menuCliccked = 0;
              //if(netCheckWithoutAlert()==true){
                  getDetails("clientvisit");
              //}else{
               //   showAlertDialogToast("Please Check Your internet connection");
              //}

    }
});
        linearLayoutSignout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                // TODO Auto-generated method stub
                databaseHandler.delete();
                Intent io = new Intent(ExportActivity.this,
                        SigninActivity.class);
                startActivity(io);
                finish();
            }
        });

        Export.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Cursor cur;
                cur = databaseHandler.getDetails();
                cur.moveToFirst();

                downloadFileName =cur.getString(cur
                        .getColumnIndex(DatabaseHandler.KEY_username));
                Log.e("name", downloadFileName);
                if(!Constants.filter.equals("1")) {

                    if(size==0){
                        showAlertDialogToast("Please select atleast one salesman");
                    }else {
                        SaveSalesmanId();
                    }
                }else{
                    SaveSalesmanId();
                }
            }
        });

        EditTextFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DatePickerDialog mdiDialog = new DatePickerDialog(
                        ExportActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view,
                                                  int year, int month,
                                                  int dayOfMonth) {
                                Log.e("date1", year + "-" + (month)
                                        + "-" + day);
                                // Toast.makeText(getContext(),year+
                                // " "+monthOfYear+" "+dayOfMonth,Toast.LENGTH_LONG).show();
                                Log.e("date", year + "-" + (month+1)
                                        + "-" + dayOfMonth);
                                String time =year + "-" + (month+1)
                                        + "-" + dayOfMonth;

                                String monthLength =String.valueOf(month+1);
                                Log.e("date", monthLength);
                                if(monthLength.length()==1){
                                    monthLength = "0"+monthLength;
                                }
                                String dayLength =String.valueOf(dayOfMonth);
                                if(dayLength.length()==1){
                                    dayLength = "0"+dayLength;
                                }
                                fromdate = year + "-" + monthLength
                                        + "-" + dayLength;
                               // Constants.fromDate =fromdate;
                                monthnew=month;
                                yearnew=year;
                                daynew=dayOfMonth;
                                Log.e("date3", String.valueOf(month));
                                Log.e("date2", String.valueOf(monthnew));
                                String inputPattern = "yyyy-MM-dd";
                                String outputPattern = "dd-MMM-yyyy ";
                                SimpleDateFormat inputFormat = new SimpleDateFormat(
                                        inputPattern);
                                SimpleDateFormat outputFormat = new SimpleDateFormat(
                                        outputPattern);

                                String str = null;
                                try {
                                    dateStr = inputFormat.parse(time);
                                    str = outputFormat.format(dateStr);
                                    EditTextFromDate.setText(str);
                                    // Log.e("str", str);
                                } catch (Exception  e) {
                                    e.printStackTrace();
                                }

                            }
                        }, yearnew, monthnew, daynew);
                mdiDialog.show();
            }
        });
        EditTextToDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog mdiDialog = new DatePickerDialog(
                        ExportActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view,
                                                  int year, int month,
                                                  int dayOfMonth) {
                                // Toast.makeText(getContext(),year+
                                // " "+monthOfYear+" "+dayOfMonth,Toast.LENGTH_LONG).show();
                                Log.e("date", year + "-" + (month+1)
                                        + "-" + dayOfMonth);
                                String time =year + "-" + (month+1)
                                        + "-" + dayOfMonth;

                                String monthLength =String.valueOf(month+1);
                                if(monthLength.length()==1){
                                    monthLength = "0"+monthLength;
                                }
                                String dayLength =String.valueOf(dayOfMonth);
                                if(dayLength.length()==1){
                                    dayLength = "0"+dayLength;
                                }

                                todate = year + "-" + monthLength
                                        + "-" + dayLength;
                                newmnth=month;
                                newyear=year;
                                newday=dayOfMonth;
                                String inputPattern = "yyyy-MM-dd";
                                String outputPattern = "dd-MMM-yyyy ";
                                SimpleDateFormat inputFormat = new SimpleDateFormat(
                                        inputPattern);
                                SimpleDateFormat outputFormat = new SimpleDateFormat(
                                        outputPattern);

                                String str = null;
                                try {
                                    dateStr = inputFormat.parse(time);
                                    str = outputFormat.format(dateStr);
                                    EditTextToDate.setText(str);
                                    // Log.e("str", str);
                                } catch (Exception  e) {
                                    e.printStackTrace();
                                }

                            }
                        }, newyear, newmnth, newday);
                mdiDialog.show();
            }
        });




    }
    private void showCurrentDateOnView() {
        // TODO Auto-generated method stub


        final Calendar c = Calendar.getInstance();
        yearnew = c.get(Calendar.YEAR);
        newyear= c.get(Calendar.YEAR);
        newmnth = c.get(Calendar.MONTH);
        monthnew=c.get(Calendar.MONTH);
        daynew = c.get(Calendar.DAY_OF_MONTH);
        newday=c.get(Calendar.DAY_OF_MONTH);
        hr= c.get(Calendar.HOUR_OF_DAY);
        mins= c.get(Calendar.MINUTE);
        sec= c.get(Calendar.SECOND);

    String monthLength =String.valueOf(newmnth+1);
        if(monthLength.length()==1){
             monthLength = "0"+monthLength;
        }
        String dayLength =String.valueOf(daynew);
        if(dayLength.length()==1){
            dayLength = "0"+dayLength;
        }

        Log.e("monthLength",monthLength);

        Log.e("dayLength",dayLength);
        // set current date into textview
        EditTextFromDate.setText(new StringBuilder()
                // Month is 0 based, just add 1
                        .append(yearnew).append("-").append(monthLength).append("-").append(dayLength)
                );

        todate=EditTextFromDate.getText().toString();
        fromdate=EditTextFromDate.getText().toString();

      String time=  EditTextFromDate.getText().toString();

        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd-MMM-yyyy ";
        SimpleDateFormat inputFormat = new SimpleDateFormat(
                inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(
                outputPattern);

        String str = null;
        try {
            dateStr = inputFormat.parse(time);
            str = outputFormat.format(dateStr);
            EditTextFromDate.setText(str);
            EditTextToDate.setText(str);
            // Log.e("str", str);
        } catch (Exception  e) {
            e.printStackTrace();
        }


        // set current date into datepicker
        datePicker.init(year, newmnth, day, null);

    }
    private void getDetails(final String paymentcheck) {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "";

            @Override
            protected void onPreExecute() {
                dialog= ProgressDialog.show(ExportActivity.this, "",
                        "Loading...", true, true);

            }

            @Override
            protected void onPostExecute(Void result) {

                try {

                    PaymentStatus=(JsonAccountObject.getString("pymtstatus"));
                    Log.e("PaymentStatus",PaymentStatus);


                    if(PaymentStatus.equals("Pending")||PaymentStatus.equals(null) ||PaymentStatus.equals("")||PaymentStatus.equals("null")){
                        showAlertDialogToast("Please make payment before place order");
                    }else{
                        Log.e("paymentcheck", paymentcheck);

                        if(paymentcheck.equals("profile")){
                            Intent io = new Intent(ExportActivity.this,
                                    ProfileActivity.class);
                            startActivity(io);
                            finish();

                        }else if(paymentcheck.equals("product")){
                            Intent io = new Intent(ExportActivity.this,
                                    ProductActivity.class);
                            startActivity(io);
                            finish();

                        }else if(paymentcheck.equals("postnotes")){
                            Intent io = new Intent(ExportActivity.this,
                                    DealersComplaintActivity.class);
                            startActivity(io);
                            finish();

                        }else if(paymentcheck.equals("myorders")){
                            Intent to = new Intent(ExportActivity.this,
                                    DealersOrderActivity.class);
                            startActivity(to);
                            finish();

                        }else if(paymentcheck.equals("clientvisit")){

                            Intent io = new Intent(ExportActivity.this,
                                    DealerClientVisit.class);
                            startActivity(io);
                            finish();

                        }


                    }

                    dialog.dismiss();


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();


                JsonServiceHandler = new JsonServiceHandler(Utils.strGetProfileDetail+Constants.USER_ID, ExportActivity.this);
                JsonAccountObject = JsonServiceHandler.ServiceDataGet();
                return null;
            }
        }.execute(new Void[]{});
    }

    private void getsaleslist(){
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "";
            @Override
            protected void onPreExecute() {
                dialog = ProgressDialog.show(ExportActivity.this, "In Progress",
                        "Validating...", true, true);
            }
            @Override
            protected void onPostExecute(Void result) {
                try{
                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);
                    if(strStatus.equals("true")) {


                        JSONArray JsonArray = JsonAccountObject.getJSONArray("data");
                        colours= new String[JsonArray.length()];

                        arraylistsaleslist = new ArrayList<SalesmanListDomain>();

                        for ( int i= 0; i < JsonArray.length(); i++) {

                            JSONObject obj=new JSONObject();

                            SalesmanListDomain dod=new SalesmanListDomain();
                            obj=JsonArray.getJSONObject(i);
                            dod.setDuserid(obj.getString("userid"));
                            dod.setName(obj.getString("companyname"));
                            dod.setfullname(obj.getString("fullname"));

                            arraylistsaleslist.add(dod);
                            colours[i]=obj.getString("fullname");


                        }




                    }else{
                        selectColoursButton.setVisibility(Button.GONE);
                        showAlertDialogToast(strMsg);

                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            @Override
            protected Void doInBackground(Void... params) {
                JSONObject jsonObject = new JSONObject();

                try {
                    jsonObject.put("userid", Constants.USER_ID);
                    jsonObject.put("usertype", Constants.USER_TYPE);
                    jsonObject.put("saleslist","Y");
                    jsonObject.put("userstatus","Active");

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                JsonServiceHandler = new JsonServiceHandler(Utils.strGetMerchantList, jsonObject.toString(),ExportActivity.this);
                JsonAccountObject = JsonServiceHandler.ServiceData();
                return null;
            }


        }.execute(new Void[]{});

    }

    protected void showSelectColoursDialog() {

        //boolean[] checkedColours = new boolean[colours.length];
if(k==0) {
    checkedColours = new boolean[colours.length];


    int count = colours.length;

    for (int i = 0; i < count; i++) {

       // checkedColours[i] = selectedColours.contains(colours[i]);
        checkedColours[i] = true;
        selectedColours.add(colours[i]);

        Log.e("colors1", String.valueOf(selectedColours));
    }

    coloursDialogListener = new DialogInterface.OnMultiChoiceClickListener() {


        public void onClick(DialogInterface dialog, int which, boolean isChecked) {


            Log.e("colorsclick", String.valueOf(selectedColours));
            if (isChecked) {
                Log.e("1", String.valueOf(colours[which]));
                selectedColours.add(colours[which]);

            } else {
                Log.e("2", String.valueOf(colours[which]));
                selectedColours.remove(colours[which]);


            }


        }

    };


k++;
} else {
    Log.e("k", String.valueOf(k));
    checkedColours = new boolean[colours.length];
    selectColoursButton.setText("");

    int count = colours.length;
    for (int i = 0; i < count; i++) {
        checkedColours[i] = selectedColours.contains(colours[i]);

    }
    coloursDialogListener = new DialogInterface.OnMultiChoiceClickListener() {


        public void onClick(DialogInterface dialog, int which, boolean isChecked) {


            Log.e("colors4", String.valueOf(selectedColours));
            if (isChecked) {
                Log.e("1", String.valueOf(colours[which]));
                selectedColours.add(colours[which]);

            } else {
                Log.e("2", String.valueOf(colours[which]));
                selectedColours.remove(colours[which]);


            }


        }

    };

}

            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setTitle("Select salesman");

            builder.setMultiChoiceItems(colours, checkedColours, coloursDialogListener);

            builder.setPositiveButton("Ok",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Log.e("colors", String.valueOf(selectedColours));
                            size=selectedColours.size();
                            if(size==0)
                            {
                                selectColoursButton.setText("Select Salesman");

                            }else{
                                selectColoursButton.setText("Selected Salesman"+"("+size+")");
                            }
                            dialog.cancel();
                        }
                    });
            //builder.setOnCancelListener((DialogInterface.OnCancelListener) this);
            AlertDialog dialog = builder.create();

            dialog.show();


    }
   /* protected void onChangeSelectedColours() {

        StringBuilder stringBuilder = new StringBuilder();

        for(CharSequence colour : selectedColours)
            if(selectedColours.size()==1){
                stringBuilder.append(colour);
            }else{
                stringBuilder.append(","+colour);
            }


        selectColoursButton.setText(stringBuilder.toString());

Log.e("text",selectColoursButton.getText().toString());

    }
*/



    public void SaveSalesmanId(){
        selectedSalesman= new Integer[selectedColours.size()];
        Log.e("size",String.valueOf(selectedSalesman.length));
        for(SalesmanListDomain pd:arraylistsaleslist) {
            Log.e("inside", "inside");
            Log.e("CompanyName", pd.getName());
            Log.e("colors2", String.valueOf(selectedColours));
          //  String[] Salesman = selectColoursButton.getText().toString().split(",");

            for (int j = 0; j < selectedColours.size(); j++) {

            //Log.e("selectedColours", cname);
            if (pd.getfullname().equals(selectedColours.get(j))) {
                Log.e("domaininside", "domaininside");
                int muserId = Integer.parseInt(pd.getDuserid());
                selectedSalesman[j] = muserId  ;
                /*if(selectedSalesman.equals("")){

                }else{
                    selectedSalesman[j] =selectedSalesman[j]+ "," +muserId  ;
                }*/


                //selectslaes= Integer.parseInt(selectedSalesman);
            }
        }
        }

        if(Constants.filter.equals("1")){

            Constants.fromDate=fromdate;
            Constants.toDate=todate;
            Constants.salesmans= new Integer[selectedColours.size()];
            Constants.salesmans=selectedSalesman;

            Intent to = new Intent(ExportActivity.this,DealerClientVisit.class);
            startActivity(to);
            finish();
            Constants.filter="";
        }else{
            Export();
        }
    }

    private void Export() {
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "";

            @Override
            protected void onPreExecute() {
                dialog = ProgressDialog.show(ExportActivity.this, "In Progress",
                        "Validating...", true, true);
            }

            @Override
            protected void onPostExecute(Void result) {
                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);
                    selectedSalesman= new Integer[0];
                   if (strStatus.equals("true")) {
                       strurl = JsonAccountObject.getString("url");
                       Log.e("strurl",strurl);


                       Boolean result1=isDownloadManagerAvailable(ExportActivity.this);
                       if (result1) {

                           downloadFile(Utils.strDownloadCsv+strurl,downloadFileName+".csv");
                          //Utils.urlPrefix+strurl
                       }

                    } else {

                       showAlertDialogToast(strMsg);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Log.e("loginActivityException", e.toString());
                }

                dialog.dismiss();
            }

            @Override
            protected Void doInBackground(Void... params) {
                dialog.show();

                JSONObject jsonObject = new JSONObject();
                JSONArray jsonarray1 = new JSONArray();
               // Log.e("colors", String.valueOf(selectslaes));
                try {

                    jsonObject.put("userid", Constants.USER_ID);
                    jsonObject.put("usertype",Constants.USER_TYPE);
                    jsonObject.put("fromdt",fromdate);
                    jsonObject.put("todt",todate);

for(int j=0;j<selectedSalesman.length;j++){
    jsonarray1.put(selectedSalesman[j]);
}

                    jsonObject.put("suseridlist",jsonarray1);
                  //  jsonObject.put("email",email);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                JsonServiceHandler = new JsonServiceHandler(
                        Utils.strExport, jsonObject.toString(),
                        ExportActivity.this);
                JsonAccountObject = JsonServiceHandler.ServiceData();
                return null;
            }
        }.execute(new Void[] {});

    }
    public boolean netCheckWithoutAlert() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            }

            else if (result == null) {
                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public boolean netCheck() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            }

            else if (result == null) {
                showAlertDialog(ExportActivity.this,
                        "No Internet Connection",
                        "Please Check Your internet connection.", false);
                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
    public void showAlertDialog(Context context, String title, String message,
                                Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        alertDialog.show();
    }
    public void showAlertDialogToast( String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void downloadFile(String documentSelected,String documentName){
        String DownloadUrl =documentSelected;
        Log.e("DownloadUrl", DownloadUrl);
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(DownloadUrl));
        request.setDescription("Downloading CASE CATALOG");   //appears the same in Notification bar while downloading
       //request.setTitle("amc.csv");
        request.setTitle(documentName);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            request.allowScanningByMediaScanner();
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
        }
        request.setDestinationInExternalFilesDir(getApplicationContext(), Environment.DIRECTORY_DOWNLOADS, documentName);
       //request.setDestinationInExternalPublicDir(File.separator + "Download" + File.separator + "amc.csv", Environment.DIRECTORY_DOWNLOADS);
        /*request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,
                File.separator + "Downloads" + File.separator + "amc.csv");*/
        // get download service and enqueue file
        Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
      request.setDestinationInExternalPublicDir(
                Environment.DIRECTORY_DOWNLOADS,
                "amc.csv");

        //long enqueue = manager.enqueue(request);
        DownloadManager manager=(DownloadManager) getApplicationContext().getSystemService(getApplicationContext().DOWNLOAD_SERVICE);
        manager.enqueue(request);



        showAlertDialogToast("Exported Successfully");
        //Toast.makeText(getApplicationContext(), "Downloading...", Toast.LENGTH_SHORT).show();
    }


    public static boolean isDownloadManagerAvailable(Context context) {
        try {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.GINGERBREAD) {
                return false;
            }
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
            intent.setClassName("com.android.providers.downloads.ui","com.android.providers.downloads.ui.DownloadList");
            List<ResolveInfo> list = context.getPackageManager().queryIntentActivities(intent,
                    PackageManager.MATCH_DEFAULT_ONLY);
            return list.size() > 0;
        } catch (Exception e) {
            return false;
        }
    }

    public void onBackPressed() {
        exitAlret();
    }

    private void exitAlret() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        localBuilder.setMessage("Do you want to Exit?");
        localBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {

                        finish();

                    }
                });
        localBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {
                        paramDialogInterface.cancel();
                    }
                });
        localBuilder.create().show();
    }
}
