package com.freshorders.freshorder.ui;
import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.activity.DistanceCalActivity;
import com.freshorders.freshorder.adapter.NippoAddMerchantAdapter;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.SpinnerDomain;
import com.freshorders.freshorder.model.HierarchyObjModel;
import com.freshorders.freshorder.popup.PopupTreeView;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.toonline.migration.PendingDataMigrationActivity;
import com.freshorders.freshorder.treeview.Pair;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.GPSTracker;
import com.freshorders.freshorder.utils.GPSTrackerActivity;
import com.freshorders.freshorder.utils.HierarchyUtil;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.freshorders.freshorder.service.PrefManager.KEY_IS_HIERARCHY_PLACE_FILE_LOADED;
import static com.freshorders.freshorder.service.PrefManager.KEY_PATH_HIERARCHY_PLACE_FILE;
import static com.google.android.gms.plus.PlusOneDummyView.TAG;

public class AddMerchantNew extends Activity implements NippoAddMerchantAdapter.ListAdapterListener {

	private String hierarchyText = "";
	private HierarchyObjModel selectedItem ;
	private String hierarchyobj = "";

	private double merchatGeolat = 0d, merchantGeolon = 0d;
    private ProgressDialog pDialog;

    private Location mLastLocation;
    private FusedLocationProviderClient mFusedLocationClient;
    private long locationUpdateInterval = 1000;
    private static int gpsCounter = 0;


    LocationManager locationManager;
    boolean gps_enabled = false;
    boolean network_enabled = false;
    boolean passive_enabled = false;

	private static int CURRENT_NEW_MERCHANT_ID = 0;
	private static String OFFLINE_MERCHANT_NO_OR_ORDER_NO = "";
	private static int curBeatRowId = 0;
	public static TextView textViewLocalMC1,textViewLocalMC2;
	TextView menuIcon, textViewAssetMenuProfile, textViewAssetMenuMyOrders,
			textViewAssetMenuMyDealers, textViewAssetMenuProducts,
			textViewAssetMenuPayment, textViewAssetMenuComplaint,
			textViewAssetMenuSignout,textViewAssetMenuCreateOrder,
			textViewAssetMenuClientVisit,textViewAssetMenuPaymentCollection,textViewClient,textViewAssetMenuRefresh,textViewAssetMenuDistanceCalculation,
			textViewAssetMenuStockEntry,textViewAssetMenuAddMerchant,textViewAssetMenuAcknowledge
			,textViewAssetMenusurveyuser,textViewAssetMenudownloadscheme,textViewAssetMenuMyDayPlan,textViewAssetMenuMySales,textViewAssetMenuDistStock,
			textViewAssetMenuStockOnly, textViewAssetMenuMigration, textViewAssetMenuStockAudit;

	AutoCompleteTextView textViewStockiest,textViewRoute,textViewAddress3,textViewAddress2;
	int menuCliccked;
	LinearLayout linearLayoutMenuParent,linearLayoutProfile, linearLayoutMyOrders,
			linearLayoutMyDealers, linearLayoutProducts, linearLayoutPayment,
			linearLayoutComplaint, linearLayoutSignout,linearLayoutEdit,linearLayoutDetails,
			linearLayoutCreateOrder,linearLayoutdump3,linearLayoutdump2,linearLayoutExport,linearLayoutClientVisit,linearLayoutPaymentCollection,linearLayoutRefresh,
			linearLayoutAcknowledge,linearLayoutStockEntry,linearLayoutAddMerchant,linearLayoutDistanceCalculation,
			linearLayoutsurveyuser,linearLayoutdownloadscheme,linearLayoutMyDayPlan,linearLayoutMySales,
			linearLayoutDistStock, linearLayoutStockOnly, linearLayoutMigration, linearLayoutStockAudit;

	private void showMenu(){

		MyApplication app = MyApplication.getInstance();

		if(!app.isCreateOrder()){
			linearLayoutCreateOrder.setVisibility(View.GONE);
		}
		if(!app.isProfile()){
			linearLayoutProfile.setVisibility(View.GONE);
		}
		if(!app.isMyDayPlan()){
			linearLayoutMyDayPlan.setVisibility(View.GONE);
		}
		if(!app.isMyOrders()){
			linearLayoutMyOrders.setVisibility(View.GONE);
		}
		if(!app.isAddMerchant()){
			linearLayoutAddMerchant.setVisibility(View.GONE);
		}
		if(!app.isMySales()){
			linearLayoutMySales.setVisibility(View.GONE);
		}
		if(!app.isPostNotes()){
			linearLayoutComplaint.setVisibility(View.GONE);
		}
		if(!app.isMyClientVisit()){
			linearLayoutClientVisit.setVisibility(View.GONE);
		}
		if(!app.isAcknowledge()){
			linearLayoutAcknowledge.setVisibility(View.GONE);
		}
		if(!app.isPaymentCollection()){
			linearLayoutPaymentCollection.setVisibility(View.GONE);
		}
		if(!app.isPkdDataCapture()){
			linearLayoutStockEntry.setVisibility(View.GONE);
		}
		if(!app.isDistributorStock()){
			linearLayoutDistStock.setVisibility(View.GONE);
		}
		if(!app.isSurveyUser()){
			linearLayoutsurveyuser.setVisibility(View.GONE);
		}
		if(!app.isDownLoadScheme()){
			linearLayoutdownloadscheme.setVisibility(View.GONE);
		}
		if(!app.isDistanceCalculation()){
			linearLayoutDistanceCalculation.setVisibility(View.GONE);
		}
		if(!app.isRefresh()){
			linearLayoutRefresh.setVisibility(View.GONE);
		}
		if(!app.isLogout()){
			linearLayoutSignout.setVisibility(View.GONE);
		}
		if(!app.isClosingStock()){
			linearLayoutStockOnly.setVisibility(View.GONE);
		}
		if(!app.isClosingStockAudit()){
			linearLayoutStockAudit.setVisibility(View.GONE);
		}
		if(!app.isPendingData()){
			linearLayoutMigration.setVisibility(View.GONE);
		}
	}

	ArrayList<String> arlsockiest=null;
	ArrayList<String> arlroute=null;
	ArrayList<String> arlcity=null;
	String strStockiest,duserid,strState,strRoute,fullname,companyName,email,address,mobile,validatemobile,address2,address3,tinnumber,pincode,stockiest,route,
			city="null",offbeatmuserid,selected_dealer,currentDate,userBeatModel ="",selectedbeat="",pan_no,customdata,
			strCustomValuesObj,
			selectedbeatId,distributorname,distributoruserType,distributorCompanyName,oflnordid;
	//JSONArray customdata;
	public static String newmerchantcmpnyname="null";
	String settingid,industryid,refkey,data_type,remarks,refvalue,userid,updateddt,status;
	EditText textViewName,textViewCompanyName,textViewMobile,textViewAddress,
			textViewEmail,textViewPinCode,textViewTin,textViewPan;
	View viewArCustomComponents[]=null;
	Button buttonsave;
	JSONArray jsonArCustomData=null;
	JSONObject JsonAccountObject ;
	JSONArray JsonAccountArray ;
	DatabaseHandler databaseHandler;
	JsonServiceHandler JsonServiceHandler;
	SQLiteDatabase db;
	GPSTracker gps;
	public static String MerchantCount,MerchantCount1;
	public static String PaymentStatus="NULL",moveto="NULL",orderno,pushstatus,productstatus,OfflineOrderNo,merchantRowid,
			beatrowid;
	String day_mon="N",day_tue="N",day_wed="N",day_thu="N",day_fri="N",day_sat="N",day_sun="N";
	double createordergeolat,createordergeolog;
	//Dynamic data load
	LinearLayout ll;
	LinearLayout .LayoutParams lp;
	//Nipon user values
	String customValue="",customID="";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(1);
		setContentView(R.layout.add_merchant_in_salesman);
		netCheckwithoutAlert();

        int permissionCheck = ContextCompat.checkSelfPermission(AddMerchantNew.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    AddMerchantNew.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.LOCATION_PERMISSION_REQUEST_CODE);
        } else {
            try {
                if (CheckGpsStatus()) {
                    startLocationUpdates();
                } else {
                    showSettingsAlert();
                }
            } catch (Exception e) {
                e.printStackTrace();
                showAlertDialogToast(getResources().getString(R.string.technical_fault) + " -- " + e.getMessage());
            }
        }


		databaseHandler = new DatabaseHandler(getApplicationContext());
		Cursor curs;
		curs = databaseHandler.getdealer();
		curs.moveToFirst();
		Constants.DUSER_ID = curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_Duserid));
		Log.e("Constants.DUSER_ID",Constants.DUSER_ID);
		//Initialization
		ll=(LinearLayout)findViewById(R.id.linearLayoutNibon);
		lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		//String strUrl=Utils.strCustomFieldMerchantURL+Constants.DUSER_ID;
		//loadCustomFields(strUrl);

        /*
        Kumaravel 21-07-2019

            if( Constants.jsonArCustomFieldsMerchant == null)
            {
                try {
                    String strMerchantCustomFields = databaseHandler.getConstantValue("MERCHANTCUSTOMFIELDS");
                    Constants.jsonArCustomFieldsMerchant = new JSONArray(strMerchantCustomFields);
                }catch (Exception ex)
                {
                    ex.printStackTrace();
                }
            }

         */


		loadUIComponents(Constants.jsonArCustomFieldsMerchant);
		gps = new GPSTracker(AddMerchantNew.this);
		Log.e("gps------------", String.valueOf(gps));
		databaseHandler = new DatabaseHandler(getApplicationContext());
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();
		//arlsockiest = new ArrayList<>();
		//arlroute = new ArrayList<>();
		db = getApplicationContext().openOrCreateDatabase("freshorders", 0, null);
		Cursor cur;
		cur = databaseHandler.getDetails();
		cur.moveToFirst();
		Log.e("Insertion Check",
				cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_username)));
		Log.e("Insertion Check",
				cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_password)));
		Log.e("KEY_paymentStatus",
				cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_paymentStatus)));
		PaymentStatus =cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_paymentStatus));

		Cursor cur1;
		cur1 = databaseHandler.getUserSetting();
		cur1.moveToFirst();


		userBeatModel = cur1.getString(cur1.getColumnIndex(DatabaseHandler.KEY_settingrefvalue));
		Log.e("userBeatModel",userBeatModel);
		if(userBeatModel.equalsIgnoreCase("C")){
			Cursor curr;
			curr = databaseHandler.getDetails();
			curr.moveToFirst();
			selectedbeat = curr.getString(curr.getColumnIndex(DatabaseHandler.KEY_selectedBeat));
			String selectedDate = curr.getString(curr.getColumnIndex(DatabaseHandler.KEY_selectedDate));
			String todayDate = new SimpleDateFormat(
					"yyyy-MM-dd").format(new java.util.Date());
			if(todayDate.equals(selectedDate) && !selectedbeat.equals("")){
				Cursor cur2;
				cur2 = databaseHandler.getBeatdetails(selectedbeat);
				cur2.moveToFirst();
				if(cur2!=null && cur2.getCount() > 0) {
					if (cur2.moveToFirst()) {

						selectedbeatId = cur2.getString(1);
						Log.e("selectedbeatId",selectedbeatId);
						distributorname = cur2.getString(19);
						Log.e("distributorname",distributorname);
						distributoruserType= cur2.getString(20);
						Log.e("distributoruserType",distributoruserType);
						distributorCompanyName = cur2.getString(21);
						Log.e("distributorCompanyName",distributorCompanyName);
					}
				}

			}else{
				Constants.beatassignscreen ="AddMerchant";
				Intent to = new Intent(AddMerchantNew.this, CompanyBeatActivity.class);
				startActivity(to);
				finish();
			}



		}
		currentDate= new SimpleDateFormat("yyyy-MM-dd").format(new java.util.Date());
		linearLayoutEdit = (LinearLayout) findViewById(R.id.linearLayoutEdit);
		linearLayoutDetails = (LinearLayout) findViewById(R.id.linearLayoutDetails);
		linearLayoutCreateOrder = (LinearLayout) findViewById(R.id.linearLayoutCreateOrder);
		linearLayoutProfile = (LinearLayout) findViewById(R.id.linearLayoutProfile);
		linearLayoutMyOrders = (LinearLayout) findViewById(R.id.linearLayoutMyOrders);
		linearLayoutMyDealers = (LinearLayout) findViewById(R.id.linearLayoutMyDealers);
		linearLayoutProducts = (LinearLayout) findViewById(R.id.linearLayoutProducts);
		linearLayoutPayment = (LinearLayout) findViewById(R.id.linearLayoutPayment);
		linearLayoutComplaint = (LinearLayout) findViewById(R.id.linearLayoutComplaint);
		linearLayoutSignout = (LinearLayout) findViewById(R.id.linearLayoutSignout);
		linearLayoutExport= (LinearLayout) findViewById(R.id.linearLayoutExport);
		linearLayoutClientVisit= (LinearLayout) findViewById(R.id.linearLayoutClientVisit);
		linearLayoutPaymentCollection= (LinearLayout) findViewById(R.id.linearLayoutPaymentCollection);
		linearLayoutRefresh= (LinearLayout) findViewById(R.id.linearLayoutRefresh);
		linearLayoutdump2= (LinearLayout) findViewById(R.id.linearLayoutdump2);
		linearLayoutdump3= (LinearLayout) findViewById(R.id.linearLayoutdump3);
		linearLayoutDistanceCalculation=(LinearLayout) findViewById(R.id.linearLayoutDistanceCalculation);
		linearLayoutAcknowledge= (LinearLayout) findViewById(R.id.linearLayoutAcknowledge);
		linearLayoutStockEntry= (LinearLayout) findViewById(R.id.linearLayoutStockEntry);
		linearLayoutAddMerchant = (LinearLayout) findViewById(R.id.linearLayoutAddMerchant);

		linearLayoutdownloadscheme= (LinearLayout) findViewById(R.id.linearLayoutdownloadscheme);
		linearLayoutsurveyuser= (LinearLayout) findViewById(R.id.linearLayoutsurveyuser);

		linearLayoutMyDayPlan = (LinearLayout) findViewById(R.id.linearLayoutMyDayPlan);
		linearLayoutMySales = (LinearLayout) findViewById(R.id.linearLayoutMySales);

		textViewAssetMenuRefresh = (TextView) findViewById(R.id.textViewAssetMenuRefresh);

		menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
		textViewAssetMenuCreateOrder= (TextView) findViewById(R.id.textViewAssetMenuCreateOrder);
		textViewAssetMenuProfile = (TextView) findViewById(R.id.textViewAssetMenuProfile);
		textViewAssetMenuMyOrders = (TextView) findViewById(R.id.textViewAssetMenuMyOrders);
		textViewAssetMenuMyDealers = (TextView) findViewById(R.id.textViewAssetMenuMyDealers);
		textViewAssetMenuProducts = (TextView) findViewById(R.id.textViewAssetMenuProducts);
		textViewAssetMenuPayment = (TextView) findViewById(R.id.textViewAssetMenuPayment);
		textViewAssetMenuComplaint = (TextView) findViewById(R.id.textViewAssetMenuComplaint);
		textViewAssetMenuSignout = (TextView) findViewById(R.id.textViewAssetMenuSignout);
		textViewAssetMenuDistanceCalculation= (TextView) findViewById(R.id.textViewAssetMenuDistanceCalculation);
		textViewAssetMenuClientVisit= (TextView) findViewById(R.id.textViewAssetMenuClientVisit);
		textViewAssetMenuPaymentCollection= (TextView) findViewById(R.id.textViewAssetMenuPaymentCollection);
		textViewClient= (TextView) findViewById(R.id.textViewClient);
		textViewLocalMC1 = (TextView) findViewById(R.id.textViewLocalMC1);
		textViewLocalMC2 = (TextView) findViewById(R.id.textViewLocalMC2);
		textViewAssetMenuAddMerchant = (TextView) findViewById(R.id.textViewAssetMenuAddMerchant);
		textViewAssetMenuAcknowledge = (TextView) findViewById(R.id.textViewAssetMenuAcknowledge);
		textViewAssetMenuStockEntry = (TextView) findViewById(R.id.textViewAssetMenuStockEntry);

		textViewAssetMenudownloadscheme= (TextView) findViewById(R.id.textViewAssetMenudownloadscheme);
		textViewAssetMenusurveyuser= (TextView) findViewById(R.id.textViewAssetMenusurveyuser);
		textViewAssetMenuMyDayPlan= (TextView) findViewById(R.id.textViewAssetMenuMyDayPlan);
		textViewAssetMenuMySales= (TextView) findViewById(R.id.textViewAssetMenuMySales);

		textViewName = (EditText) findViewById(R.id.textViewMerchantName);
		textViewCompanyName = (EditText) findViewById(R.id.textViewCompanyNmae);
		textViewMobile = (EditText) findViewById(R.id.textViewContactNo);
		textViewAddress = (EditText) findViewById(R.id.textViewAddress);
		textViewEmail = (EditText) findViewById(R.id.textViewEmail);
		//newly added field in Add Merchant/Outlet
		textViewAddress2 = (AutoCompleteTextView) findViewById(R.id.textViewAddress2);
		textViewAddress3 = (AutoCompleteTextView) findViewById(R.id.textViewAddress3);
		textViewPinCode = (EditText) findViewById(R.id.textViewPinCode);
		textViewTin = (EditText) findViewById(R.id.textViewTin);
		textViewPan = (EditText) findViewById(R.id.textViewPan);
		textViewStockiest = (AutoCompleteTextView) findViewById(R.id.textViewStockiest);

		textViewRoute = (AutoCompleteTextView) findViewById(R.id.textViewRoute);

		linearLayoutMenuParent = (LinearLayout) findViewById(R.id.linearLayoutMenuParent);
		Typeface font = Typeface.createFromAsset(getAssets(),
				"fontawesome-webfont.ttf");
		menuIcon.setTypeface(font);
		textViewAssetMenuCreateOrder.setTypeface(font);
		textViewAssetMenuProfile.setTypeface(font);
		textViewAssetMenuMyOrders.setTypeface(font);
		textViewAssetMenuMyDealers.setTypeface(font);
		textViewAssetMenuProducts.setTypeface(font);
		textViewAssetMenuPayment.setTypeface(font);
		textViewAssetMenuComplaint.setTypeface(font);
		textViewAssetMenuSignout.setTypeface(font);
		textViewAssetMenuDistanceCalculation.setTypeface(font);
		textViewAssetMenuClientVisit.setTypeface(font);
		textViewAssetMenuPaymentCollection.setTypeface(font);
		textViewAssetMenuRefresh.setTypeface(font);
		textViewAssetMenuAcknowledge.setTypeface(font);
		textViewAssetMenuStockEntry.setTypeface(font);
		textViewAssetMenuAddMerchant.setTypeface(font);
		textViewAssetMenusurveyuser.setTypeface(font);
		textViewAssetMenudownloadscheme.setTypeface(font);
		textViewAssetMenuMyDayPlan.setTypeface(font);
		textViewAssetMenuMySales.setTypeface(font);

		linearLayoutDistStock = (LinearLayout) findViewById(R.id.linearLayoutDistStock);
		textViewAssetMenuDistStock = (TextView) findViewById(R.id.textViewAssetMenuDistStock);
		textViewAssetMenuDistStock.setTypeface(font);

		textViewAssetMenuStockOnly = (TextView) findViewById(R.id.textViewAssetMenuStockOnly);
		textViewAssetMenuStockAudit = (TextView) findViewById(R.id.textViewAssetMenuStockAudit);
		textViewAssetMenuMigration = findViewById(R.id.textViewAssetMenuMigration);

		textViewAssetMenuStockOnly.setTypeface(font);
		textViewAssetMenuStockAudit.setTypeface(font);
		textViewAssetMenuMigration.setTypeface(font);

		linearLayoutStockOnly = (LinearLayout) findViewById(R.id.linearLayoutStockOnly);
		linearLayoutStockAudit = findViewById(R.id.linearLayoutStockAudit);
		linearLayoutMigration = (LinearLayout) findViewById(R.id.linearLayoutMigration);


		databaseHandler = new DatabaseHandler(getApplicationContext());
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();


		buttonsave=(Button)findViewById(R.id.buttonsave);
		databaseHandler = new DatabaseHandler(getApplicationContext());
		Cursor cursss;
		cursss = databaseHandler.getStockiestList();
		//Log.e("merchantcount stockiest1:", String.valueOf(cursss.getCount()));
		MerchantCount = String.valueOf(cursss.getCount());
		cursss.moveToFirst();
		arlsockiest = new ArrayList<>();
		while (cursss.isAfterLast() == false)
		{
			String strStockiest =cursss.getString(0);
			//  System.out.println("STR-->"+strStockiest);
			arlsockiest.add(strStockiest);
			cursss.moveToNext();
		}
		textViewStockiest.setAdapter(new ArrayAdapter<String>(AddMerchantNew.this, android.R.layout.select_dialog_item, arlsockiest));
		textViewStockiest.setThreshold(1);
		databaseHandler = new DatabaseHandler(getApplicationContext());
		Cursor curs5;
		curs5 = databaseHandler.getRouteList();
		Log.e("merchant strroute:", String.valueOf(curs5.getCount()));
		MerchantCount = String.valueOf(curs5.getCount());
		curs5.moveToFirst();
		arlroute = new ArrayList<>();
		while (curs5.isAfterLast() == false)
		{
			String strRoute=curs5.getString(0);
			System.out.println("STRR-->"+strRoute);
			arlroute.add(strRoute);
			curs5.moveToNext();
		}
		textViewRoute.setAdapter(new ArrayAdapter<String>(AddMerchantNew.this, android.R.layout.select_dialog_item, arlroute));
		textViewRoute.setThreshold(1);
		databaseHandler = new DatabaseHandler(getApplicationContext());
		Cursor curs6;
		curs6 = databaseHandler. getCityList();
		Log.e("merchant strcity:", String.valueOf(curs6.getCount()));
		MerchantCount = String.valueOf(curs6.getCount());
		curs6.moveToFirst();
		arlcity = new ArrayList<>();
		while (curs6.isAfterLast() == false)
		{
			String strCity=curs6.getString(0);
			System.out.println("STRC-->"+strCity);
			arlcity.add(strCity);
			curs6.moveToNext();
		}
		textViewAddress2.setAdapter(new ArrayAdapter<String>(AddMerchantNew.this, android.R.layout.select_dialog_item, arlcity));
		textViewAddress2.setThreshold(1);
		String[] arlstates = getResources().getStringArray(R.array.india_states);
		textViewAddress3.setAdapter(new ArrayAdapter<String>(AddMerchantNew.this, android.R.layout.select_dialog_item, arlstates));
		textViewAddress3.setThreshold(1);
		databaseHandler = new DatabaseHandler(getApplicationContext());
		linearLayoutMyDealers.setVisibility(View.GONE);

		/*  i think no need this By Kumaravel  26-09-2019
		int permissionCheck1 = ContextCompat.checkSelfPermission(AddMerchantNew.this, Manifest.permission.ACCESS_FINE_LOCATION);

		if (permissionCheck1 != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(
					AddMerchantNew.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.LOCATION_PERMISSION_REQUEST_CODE);
		}else{
			/*Intent intent = new Intent(this, GPSTrackerActivity.class);
			startActivityForResMerchantult(intent,1);*/ /*

			try {
				getcurrentlocation();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}  */

		if(netCheck()==true){
			onlineMerchant();
		}else {
			int integercount=0;
			String strValue=databaseHandler.getConstantValue("OUTLETCOUNT");
			if(strValue!=null){
			integercount = Integer.parseInt(strValue);
			}
			int intCount= databaseHandler.getMerchantCount() - integercount;
			System.out.println("A:"+databaseHandler.getMerchantCount()+"--B:"+integercount+"--C:"+intCount);
			textViewLocalMC1.setText("Online Count : " +databaseHandler.getConstantValue("OUTLETCOUNT"));
			textViewLocalMC2.setText("Offline Count : "+intCount);
		}
		textViewLocalMC1.setTypeface(null, Typeface.BOLD);
		textViewLocalMC2.setTypeface(null, Typeface.BOLD);


		/////////////////////////////////// place    new Functionality added  Kumaravel 10-09-2019
		final EditText eTxtPlace = findViewById(R.id.id_add_new_ETxt_hierarchy_place);
        boolean isFileLoaded = new PrefManager(AddMerchantNew.this).getBooleanDataByKey(KEY_IS_HIERARCHY_PLACE_FILE_LOADED);
        if(!isFileLoaded){
            eTxtPlace.setVisibility(View.GONE);
        }else {
            eTxtPlace.setVisibility(View.VISIBLE);
        }
		eTxtPlace.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				DisplayMetrics displayMetrics = new DisplayMetrics();
				getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
				hideKeyBoard();
				LinearLayout root = findViewById(R.id.id_LL_add_new_root);
				boolean isFileLoaded = new PrefManager(AddMerchantNew.this).getBooleanDataByKey(KEY_IS_HIERARCHY_PLACE_FILE_LOADED);
				if (isFileLoaded) {
					String filePath = new PrefManager(AddMerchantNew.this).getStringDataByKey(KEY_PATH_HIERARCHY_PLACE_FILE);
					PopupTreeView popup = new PopupTreeView(root,
							AddMerchantNew.this, filePath, eTxtPlace,
							displayMetrics, new PopupTreeView.OnDismissListener() {
						@Override
						public void onDismiss(HierarchyObjModel selectedItems) {
							if (selectedItems == null) {
								showAlertDialogToast(getResources().getString(R.string.not_retrieve_data));
							} else {
								selectedItem = selectedItems;
							}
						}
					});
					////popup.showPopup();
					popup.showPopupWindow();
				} else {
					showAlertDialogToast(getResources().getString(R.string.data_not_load_when_login));
				}
			}
		});
		//////////////////////////////////


		buttonsave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				strStockiest = textViewStockiest.getText()
						.toString().trim();

				strRoute = textViewRoute.getText()
						.toString().trim();

				fullname =textViewName.getText()
						.toString().trim();
				companyName = textViewCompanyName.getText()
						.toString().trim();
				email = textViewEmail.getText()
						.toString().trim();
				address = textViewAddress.getText()
						.toString().trim();
				mobile = textViewMobile.getText()
						.toString().trim();
				city = textViewAddress2.getText()
						.toString().trim();/// trim added by kumaravel 19/01/2019
				strState = textViewAddress3.getText()
						.toString().trim();
				pincode = textViewPinCode.getText()
						.toString().trim();
				tinnumber = textViewTin.getText()
						.toString().trim();
				pan_no = textViewPan.getText()
						.toString().trim();

				hierarchyText = eTxtPlace.getText().toString().trim();

				boolean isFileLoaded = new PrefManager(AddMerchantNew.this).getBooleanDataByKey(KEY_IS_HIERARCHY_PLACE_FILE_LOADED);

				if(isFileLoaded && hierarchyText.isEmpty()){
					showAlertDialogToast(getResources().getString(R.string.should_select_place));
					return;
				}
				if(isFileLoaded && selectedItem == null){
					showAlertDialogToast(getResources().getString(R.string.select_place_again));
					return;
				}else {
				    if(isFileLoaded) {
                        try {
                            Gson gson = new Gson();
                            String json = gson.toJson(selectedItem);
                            JSONObject obj = new JSONObject(json);
                            obj.put("children", new JSONArray());
                            if (obj.getInt("parentlevelid") == -1) {
                                obj.remove("parentlevelid");////////////// null can not add to integer so change field here to String empty
                                obj.put("parentlevelid", "");
                            }
                            JSONArray array = new JSONArray();
                            array.put(obj);
						/*
						JSONStringer json = new JSONStringer();
						json.array()
								.object()
								.key("levelid").value(Integer.parseInt(selectedItem.getChildId()))
								.key("name").value(selectedItem.getName())
								.key("parentlevelid").value(Integer.parseInt(selectedItem.getParentId()))
								.array().key("children").value("").endArray()
								.key("isExpanded").value(selectedItem.isExpanded())
								.key("isActive").value(selectedItem.isActive())
								.key("selected").value(selectedItem.isSelected())
								.endObject()
							.endArray();
						hierarchyobj = json.toString(); */
                            hierarchyobj = array.toString();
                            Log.e("hierarchyobj", "....Result....hierarchyobj::" + hierarchyobj);
                        } catch (Exception e) {
                            e.printStackTrace();
                            showAlertDialogToast(getResources().getString(R.string.technical_fault) + e.getMessage());
                            return;
                        }
                    }else {
                        hierarchyobj = null;////////////////
                        hierarchyText = "";
                    }
				}

				JSONArray jsonArCustomDataValues=null;
				try {
					jsonArCustomDataValues=new JSONArray();
					JSONObject jsonObject=null;
					jsonArCustomData=Constants.jsonArCustomFieldsMerchant;
					int childId=0;
					for(int i=0;i<jsonArCustomData.length();i++)
					{
						jsonObject=new JSONObject();
						JSONObject jsonCF=jsonArCustomData.getJSONObject(i);
						String strCFId=jsonCF.getString("custmfldid");
						jsonObject.put("custmfldid",""+strCFId);
						jsonObject.put("duserid",Constants.DUSER_ID);
						String strCustomValue=null;
						if(jsonCF.getString("uicomptype").equalsIgnoreCase("text")) {
							v=ll.getChildAt(childId++);
							strCustomValue= ""+((EditText)v).getText();
						}
						if(jsonCF.getString("uicomptype").equalsIgnoreCase("dropdown"))
						{
							v=ll.getChildAt(childId++);
							Spinner ms=(Spinner)v;
							strCustomValue= ""+ms.getSelectedItem();
						}
						if(jsonCF.getString("uicomptype").equalsIgnoreCase("multiselect"))
						{
							v=ll.getChildAt(childId++);
							strCustomValue= ""+((NippoAddMerchantAdapter)((Spinner)v).getAdapter()).getSelectedValuesString();
						}
						if(jsonCF.getString("uicomptype").equalsIgnoreCase("switch")){
							v=ll.getChildAt(++childId);
							RadioGroup rg=(RadioGroup)v;
							int intVal=rg.getCheckedRadioButtonId();
							for(int z=0;z<rg.getChildCount();z++)
							{
								RadioButton rb=(RadioButton)rg.getChildAt(z);
								if(rb.isChecked()){strCustomValue=""+rb.getText();}
							}
							childId++;
						}

						jsonObject.put("custmfldvalue",strCustomValue);
						jsonArCustomDataValues.put(jsonObject);
					}

				}catch (Exception ex)
				{
					Log.e("exp:",ex.toString());
				}

//
//             customdata="    {\n" +
//						"      \"custmfldid\": 31,\n" +
//						"      \"duserid\": 27205,\n" +
//						"      \"custmfldvalue\": \"Whole Seller\"\n" +
//						"    }]";
//
//
				customdata=""+jsonArCustomDataValues;


				Log.e("", "D USER  ID: "+Constants.DUSER_ID );
				databaseHandler.addCustomField(Constants.DUSER_ID,customID,customValue);

				//Check whether the data are inserted of not
				Cursor cursor=databaseHandler.getCutomDetail();
				if(cursor.getCount()>0)
				{
					JSONArray resultSet = new JSONArray();
					if (cursor.getCount() > 0) {
						cursor.moveToFirst();
						while (!cursor.isAfterLast()) {
							int totalColumn = cursor.getColumnCount();
							JSONObject rowObject = new JSONObject();
							for (int i = 0; i < totalColumn; i++) {
								if (cursor.getColumnName(i) != null) {
									try {
										if (cursor.getString(i) != null) {
											Log.e("TAG_NAME", cursor.getString(i));
											rowObject.put(cursor.getColumnName(i), cursor.getString(i));
										} else {
											rowObject.put(cursor.getColumnName(i), "");
										}
									} catch (Exception e) {
										Log.e("TAG_NAME", e.getMessage());
									}
								}
								resultSet.put(rowObject);
							}

							cursor.moveToNext();
						}
					}

					Log.d("json array", resultSet.toString());
				}else {
					//No data found insert error
				}
				cursor.close();
				/*JSONArray jsonArCustomData=new JSONArray();
				JSONObject jsonCustomFieldValue=new JSONObject();
				Log.e("jsonCustomFieldValue Merchant", String.valueOf(jsonCustomFieldValue));
				try {
					jsonCustomFieldValue.put("custmfldid", "31");
					jsonCustomFieldValue.put("duserid", Constants.DUSER_ID);
					jsonCustomFieldValue.put("custmfldvalue", "Whole Seller");
				}catch (Exception e)
				{
					Log.e("Exception :" , e.toString());
				}

				jsonArCustomData.put(jsonCustomFieldValue);
				Log.e("jsonCustomFieldValue Merchant", String.valueOf(jsonCustomFieldValue));*/
				if (!strStockiest.isEmpty() && !strStockiest.startsWith(" ")) {

					if (!strRoute.isEmpty() && !strRoute.startsWith(" ")) {

						if (!companyName.isEmpty() && !companyName.startsWith(" ")) {

							if (!fullname.isEmpty() && !fullname.startsWith(" ")) {

								if (!mobile.isEmpty() && !mobile.startsWith(" ")) {


									/*if (mobile.length()==10) {*/

									if (!address.isEmpty() && !address.startsWith(" ") ) {
										if (!city.isEmpty() && !city.startsWith(" ") ) {
											if (!strState.isEmpty() && !strState.startsWith(" ") ) {
										if (!pincode.isEmpty() && !pincode.startsWith(" ")&& pincode.length()==6) {

										/*if(!address2.isEmpty()){
											city=address2;
										}else if(!address3.isEmpty()){
											city=city+","+address3;
										}*/
											String strFieldName=getMandatoryMissingField();
											//System.out.println("HHH strFieldName:"+strFieldName);
											if(strFieldName!=null)
											{
												showAlertDialogToast("Please enter valid "+strFieldName);
											}
											else {
												Log.e("city", city);
												///////////Kumaravel 26-09-2019
                                                if(merchantGeolon > 0 && merchatGeolat > 0) {
                                                    mobilenovalidation();
                                                }else {
                                                    locationUpdateWithDelay();
                                                }
												//companynamevalidation();
											}

										} else {

											showAlertDialogToast("Please enter valid Zipcode");
											//toastDisplay("Please Enter the valid  address");
										}

									}
											else{

												showAlertDialogToast("State should not be empty");
												//toastDisplay("Please Enter the valid  address");
											}
							/*else{
								showAlertDialogToast("Please enter valid Mobile Number");
								//toastDisplay("Please Enter the company name");
							}*/
										}
										else{

											showAlertDialogToast("City should not be empty");
											//toastDisplay("Please Enter the valid  address");
										}
							/*else{
								showAlertDialogToast("Please enter valid Mobile Number");
								//toastDisplay("Please Enter the company name");
							}*/
									}
									else{

										showAlertDialogToast("Address should not be empty");
										//toastDisplay("Please Enter the valid  address");
									}
							/*else{
								showAlertDialogToast("Please enter valid Mobile Number");
								//toastDisplay("Please Enter the company name");
							}*/
								}

								else{
									showAlertDialogToast("Mobile Number should not be empty");
									//toastDisplay("Please Enter the company name");
								}
							}else{
								showAlertDialogToast("Proprietor Name should not be empty");
								//toastDisplay("Please Enter the company name");
							}
						}else{
							showAlertDialogToast("Store Name should not be empty");
							//toastDisplay("Please Enter the fullname");
						}
					}else{
						showAlertDialogToast("Route Name should not be empty");
						//toastDisplay("Please Enter the company name");
					}
				}else{
					showAlertDialogToast("Stockiest Name should not be empty");
					//toastDisplay("Please Enter the fullname");
				}
			}


		});


		menuIcon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (menuCliccked == 0) {
					linearLayoutMenuParent.setVisibility(View.VISIBLE);
					menuCliccked = 1;
				} else {
					linearLayoutMenuParent.setVisibility(View.GONE);
					menuCliccked = 0;
				}

			}
		});

		linearLayoutMenuParent.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
			}
		});

		linearLayoutProfile.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				Intent io = new Intent(AddMerchantNew.this, ProfileActivity.class);
				startActivity(io);
				finish();
			}
		});


		linearLayoutRefresh.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				Log.e("Clicked", "Clicked");

				if(netCheck()){

					final Dialog qtyDialog = new Dialog(AddMerchantNew.this);
					qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					qtyDialog.setContentView(R.layout.warning_dialog);

					final Button buttonUpdateOk, buttonUpdateCancel;

					final TextView textQtyValidate;

					buttonUpdateOk = (Button) qtyDialog
							.findViewById(R.id.buttonUpdateOk);
					buttonUpdateCancel = (Button) qtyDialog
							.findViewById(R.id.buttonUpdateCancel);

					textQtyValidate = (TextView) qtyDialog
							.findViewById(R.id.textQtyValidate);
					qtyDialog.show();


					buttonUpdateOk.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							databaseHandler.synTableDelete();
							Constants.refreshscreen="addmerchant";
							Intent io = new Intent(AddMerchantNew.this, RefreshActivity.class);
							startActivity(io);
							finish();
							qtyDialog.dismiss();
						}

					});

					buttonUpdateCancel
							.setOnClickListener(new OnClickListener() {

								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub

									qtyDialog.dismiss();

								}
							});

				}
			}

		});


		linearLayoutMyOrders.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				if(netCheck()) {
					getDetail("myorder");
				}else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment before place order");
					} else {
						Intent io = new Intent(AddMerchantNew.this,
								SalesManOrderActivity.class);
						io.putExtra("Key","menuclick");
						Constants.setimage = null;
						startActivity(io);
						finish();
						/*if (Constants.USER_TYPE.equals("D")) {
							Intent io = new Intent(AddMerchantNew.this,
									DealersOrderActivity.class);
							io.putExtra("Key","menuclick");
							startActivity(io);
							finish();
						} else if (Constants.USER_TYPE.equals("M")) {
							Intent io = new Intent(AddMerchantNew.this,
									MerchantOrderActivity.class);
							io.putExtra("Key","menuclick");
							Constants.setimage = null;
							startActivity(io);
							finish();
						} else {
							Intent io = new Intent(AddMerchantNew.this,
									SalesManOrderActivity.class);
							io.putExtra("Key","menuclick");
							Constants.setimage = null;
							startActivity(io);
							finish();
						}  */

					}
				}
			}
		});

		linearLayoutMyDealers.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				if(netCheck()==true) {
					getDetail("Mydealer");
				}else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment before place order");
					} else {
						Intent io = new Intent(AddMerchantNew.this,
								MyDealersActivity.class);
						io.putExtra("Key","menuclick");
						startActivity(io);
						finish();

					}
				}


			}
		});

		linearLayoutCreateOrder.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;

                MyApplication.getInstance().setTemplate8ForTemplate2(false);

				if(netCheckwithoutAlert()==true) {
					getDetail("createorder");
				}else{
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment before place order");
					}else {
						Intent io = new Intent(AddMerchantNew.this,
								CreateOrderActivity.class);
						startActivity(io);
						finish();
					}

				}

			}
		});

		linearLayoutPayment.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;


			}
		});

		linearLayoutComplaint.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				if( netCheckwithoutAlert()==true){
					getDetail("postnotes");
				}else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment before place order");
					} else {
						Intent io = new Intent(AddMerchantNew.this,
								MerchantComplaintActivity.class);
						startActivity(io);
						finish();
						/*if (Constants.USER_TYPE.equals("D")) {
							Intent io = new Intent(AddMerchantNew.this,
									DealersComplaintActivity.class);
							startActivity(io);
							finish();

						} else {
							Intent io = new Intent(AddMerchantNew.this,
									MerchantComplaintActivity.class);
							startActivity(io);
							finish();

						}  */
					}
				}
			}
		});

		linearLayoutClientVisit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				if(netCheck()==true) {
					getDetail("clientvisit");
				}else{
					if(PaymentStatus.equals("Pending")||PaymentStatus.equals(null) ||PaymentStatus.equals("")||PaymentStatus.equals("null")){
						showAlertDialogToast("Please make payment before place order");
					}else{
						Intent io = new Intent(AddMerchantNew.this,
								SMClientVisitHistory.class);
						startActivity(io);
						finish();
						/*if (Constants.USER_TYPE.equals("D")) {
							Intent io = new Intent(AddMerchantNew.this,
									DealerClientVisit.class);
							startActivity(io);
							finish();
						} else {
							Intent io = new Intent(AddMerchantNew.this,
									SMClientVisitHistory.class);
							startActivity(io);
							finish();
						} */
					}
				}


			}
		});
		linearLayoutPaymentCollection.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				if( netCheckwithoutAlert()==true){
					getDetail("paymentcoll");
				}else{
					showAlertDialogToast("Please Check Your internet connection");
				}
			}
		});
		linearLayoutStockEntry.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				if(netCheckwithoutAlert()){
					getDetail("stockentry");
				}else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment before place order");
					} else {
						Intent io = new Intent(AddMerchantNew.this,
								OutletStockEntry.class);
						startActivity(io);
						finish();
					}
				}
			}
		});

		linearLayoutDistStock.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				moveto = "diststock";
				if (netCheckwithoutAlert()) {
					getDetail("diststock");
				} else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment");
					} else {
						Intent io = new Intent(AddMerchantNew.this,
								DistributrStockActivity.class);
						startActivity(io);
						finish();
					}
				}
			}
		});

		linearLayoutAcknowledge.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				if( netCheckwithoutAlert()){
					getDetail("acknowledge");
				}else{
					showAlertDialogToast("Please Check Your internet connection");
				}
			}
		});
		linearLayoutAddMerchant.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;

			}
		});

		linearLayoutdownloadscheme.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(netCheck()){

					final Dialog qtyDialog = new Dialog(AddMerchantNew.this);
					qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					qtyDialog.setContentView(R.layout.warning_dialog);

					final Button buttonUpdateOk, buttonUpdateCancel;

					final TextView textQtyValidate;

					buttonUpdateOk = (Button) qtyDialog
							.findViewById(R.id.buttonUpdateOk);
					buttonUpdateCancel = (Button) qtyDialog
							.findViewById(R.id.buttonUpdateCancel);

					textQtyValidate = (TextView) qtyDialog
							.findViewById(R.id.textQtyValidate);
					qtyDialog.show();


					buttonUpdateOk.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							databaseHandler.deletescheme();
							Constants.downloadScheme = "addmerchant";
							Intent io = new Intent(AddMerchantNew.this, SchemeDownload.class);
							startActivity(io);
							finish();
							qtyDialog.dismiss();
						}

					});

					buttonUpdateCancel
							.setOnClickListener(new View.OnClickListener() {

								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub

									qtyDialog.dismiss();

								}
							});

				}else{
					showAlertDialogToast("Please Check Your internet connection");
				}
			}
		});
		linearLayoutsurveyuser.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

					linearLayoutMenuParent.setVisibility(View.GONE);
					menuCliccked = 0;
					if(netCheckwithoutAlert()) {
						getDetail("surveyuser");
					}else {
						if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
							showAlertDialogToast("Please make payment before place order");
						} else {
							Intent io = new Intent(AddMerchantNew.this,
									SurveyMerchant.class);
							startActivity(io);
							finish();
						}
					}


			}
		});
		linearLayoutDistanceCalculation.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				Intent to = new Intent(AddMerchantNew.this,
						DistanceCalActivity.class);
				startActivity(to);

			}
		});

		linearLayoutMyDayPlan.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				if(netCheckwithoutAlert()){
					getDetail("mydayplan");
				}else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment before place order");
					} else {
						Intent io = new Intent(AddMerchantNew.this,
								MyDayPlan.class);
						startActivity(io);
						finish();
					}
				}
			}
		});

		linearLayoutMySales.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				if( netCheckwithoutAlert()==true){
					getDetail("mysales");
				}else{
					showAlertDialogToast("Please Check Your internet connection");
				}
			}
		});

		linearLayoutSignout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				// TODO Auto-generated method stub
				databaseHandler.delete();
				Intent io = new Intent(AddMerchantNew.this, SigninActivity.class);
				startActivity(io);
				finish();
			}
		});

		linearLayoutMigration.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				Intent io = new Intent(AddMerchantNew.this,
						PendingDataMigrationActivity.class);
				startActivity(io);
			}
		});

		linearLayoutStockOnly.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				Constants.checkproduct = 0;
				Constants.orderid = "";
				Constants.SalesMerchant_Id = "";
				Constants.Merchantname = "";
				MyApplication.getInstance().setTemplate8ForTemplate2(true);
				if (netCheck()) {
					Intent io = new Intent(AddMerchantNew.this,
							ClosingStockDashBoardActivity.class);
					startActivity(io);
					finish();
				} else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals("") || PaymentStatus.equalsIgnoreCase("null")) {
						showAlertDialogToast("Please make payment before place order");
					} else {
						Intent io = new Intent(AddMerchantNew.this,
								ClosingStockDashBoardActivity.class);
						startActivity(io);
						finish();
					}
				}
			}
		});

		linearLayoutStockAudit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MyApplication.getInstance().setTemplate8ForTemplate2(false);
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				Constants.checkproduct = 0;
				Constants.orderid = "";
				Constants.SalesMerchant_Id = "";
				Constants.Merchantname = "";
				if (netCheck()) {
					Intent io = new Intent(AddMerchantNew.this,
							ClosingStockAudit.class);
					startActivity(io);
					finish();
				} else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals("") || PaymentStatus.equalsIgnoreCase("null")) {
						showAlertDialogToast("Please make payment before place order");
					} else {
						Intent io = new Intent(AddMerchantNew.this,
								ClosingStockAudit.class);
						startActivity(io);
						finish();
					}
				}
			}
		});

		/////////////////
		showMenu();
		///////////////
	}

	public String getMandatoryMissingField(){
		String str=null;



		try {
			JSONObject jsonObject=null;
			jsonArCustomData=Constants.jsonArCustomFieldsMerchant;
			int childId=0;
			View v;
			for(int i=0;i<jsonArCustomData.length();i++)
			{

				jsonObject=new JSONObject();
				JSONObject jsonCF=jsonArCustomData.getJSONObject(i);
				System.out.println("jsonCF:"+jsonCF);
				String strCFId=jsonCF.getString("custmfldid");
				jsonObject.put("custmfldid",""+strCFId);
				jsonObject.put("duserid",Constants.DUSER_ID);
				String strCustomValue=null;
				if(jsonCF.getString("uicomptype").equalsIgnoreCase("text")) {

					v=ll.getChildAt(childId++);
					strCustomValue= ""+((EditText)v).getText();
				}
				if(jsonCF.getString("uicomptype").equalsIgnoreCase("inputtext"))
				{
				strCustomValue= "Empty Data";
				}
				if(jsonCF.getString("uicomptype").equalsIgnoreCase("dropdown"))
				{
					v=ll.getChildAt(childId++);
					Spinner ms=(Spinner)v;
					strCustomValue= ""+ms.getSelectedItem();
				}
				if(jsonCF.getString("uicomptype").equalsIgnoreCase("multiselect"))
				{
					v=ll.getChildAt(childId++);
					strCustomValue= ""+((NippoAddMerchantAdapter)((Spinner)v).getAdapter()).getSelectedValuesString();
				}
				if(jsonCF.getString("uicomptype").equalsIgnoreCase("switch")){
					v=ll.getChildAt(++childId);
					RadioGroup rg=(RadioGroup)v;
					int intVal=rg.getCheckedRadioButtonId();
					for(int z=0;z<rg.getChildCount();z++)
					{
						RadioButton rb=(RadioButton)rg.getChildAt(z);
						if(rb.isChecked()){strCustomValue=""+rb.getText();}
					}
					childId++;
				}
				if(strCustomValue==null)
				{
					str=jsonCF.getString("reflabel");
					Log.e("str1",str);
				}
				else if(strCustomValue.equalsIgnoreCase(" ") || strCustomValue.equalsIgnoreCase(null)|| strCustomValue.isEmpty()||(strCustomValue.equalsIgnoreCase(jsonCF.getString("reflabel"))))
				{
					str=jsonCF.getString("reflabel");
					Log.e("str2",str);
				}

			}

		}catch (Exception ex)
		{
			Log.e("exp:",ex.toString());
		}






		return str;
	}

	public void getcurrentlocation() throws IOException {

		if (gps.canGetLocation() == true) {

               /* createordergeolat = gps.getLatitude();
            		createordergeolog = gps.getLongitude();

                    		Log.e("lat", String.valueOf(createordergeolat));
            			Log.e("log", String.valueOf(createordergeolog));
*/

			Intent intent = new Intent(this, GPSTrackerActivity.class);
			startActivityForResult(intent,1);

		}else {
			showSettingsAlert();

		}

	}

	@TargetApi(23)
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permission, @NonNull int[] grantResults) {

		Log.e("requestcode", String.valueOf(requestCode));
		Log.e("size", String.valueOf(grantResults.length));


		if (requestCode == Constants.LOCATION_PERMISSION_REQUEST_CODE) {
			Log.e("requestcode", String.valueOf(grantResults[0]));
			if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				Intent io=new Intent(AddMerchantNew.this,AddMerchantNew.class);
				startActivity(io);
				finish();
			} else {
				requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.LOCATION_PERMISSION_REQUEST_CODE);
			}


		}
	}

	public void showSettingsAlert(){
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(AddMerchantNew.this);

		// Setting Dialog Title
		alertDialog.setTitle("GPS settings");

		// Setting Dialog Message
		alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

		// Setting Icon to Dialog
		//alertDialog.setIcon(R.drawable.delete);

		// On pressing Settings button
		alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int which) {

				Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS );
				//startActivityForResult(intent, 2);  /// by vel 26-09-2019
                startActivity(intent);
                finish();


			}
		});

		// on pressing cancel button
		alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
				///////////by vel
                finish();
			}
		});

		// Showing Alert Message
		alertDialog.show();
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Log.e("inside", String.valueOf(resultCode));
		if (requestCode == 0) {
			switch (requestCode) {
				case 1:
					Log.e("inside", "inside");
					Intent io=new Intent(AddMerchantNew.this,AddMerchantNew.class);
					startActivity(io);
					finish();
					break;
			}
		}
		if(requestCode == 1){
			try {
				Bundle extras = data.getExtras();
				createordergeolog = extras.getDouble("Longitude");
				createordergeolat = extras.getDouble("Latitude");
			}catch (Exception e){
				e.printStackTrace();
			}
		}


		if(requestCode  == 2){
			Intent intent = new Intent(this, GPSTrackerActivity.class);
			startActivityForResult(intent,1);
		}


	}

	private void getDetail(final String moveto) {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog= ProgressDialog.show(AddMerchantNew.this, "",
						"Loading...", true, true);

			}

			@Override
			protected void onPostExecute(Void result) {

				try {

					PaymentStatus = (JsonAccountObject.getString("pymtstatus"));
					Log.e("PaymentStatus", PaymentStatus);

					if(PaymentStatus.equals("Pending")||PaymentStatus.equals(null) ||PaymentStatus.equals("")||PaymentStatus.equals("null")){
						showAlertDialogToast("Please make payment to place order");
					}else {
						if (moveto.equals("product")) {

							Intent to = new Intent(AddMerchantNew.this,
									ProductActivity.class);
							startActivity(to);
							finish();

						} else if (moveto.equals("export")) {


							Intent to = new Intent(AddMerchantNew.this,
									ExportActivity.class);

							startActivity(to);
							finish();

						} else if (moveto.equals("myorder")) {

							if (Constants.USER_TYPE.equals("D")) {
								Intent io = new Intent(AddMerchantNew.this,
										DealersOrderActivity.class);
								startActivity(io);
								finish();

							} else if (Constants.USER_TYPE.equals("M")){
								Intent io = new Intent(AddMerchantNew.this,
										MerchantOrderActivity.class);
								startActivity(io);
								finish();

							}else {
								Intent io = new Intent(AddMerchantNew.this,
										SalesManOrderActivity.class);
								startActivity(io);
								finish();

							}

						}else if(moveto.equals("createorder")){
							Intent io = new Intent(AddMerchantNew.this,
									CreateOrderActivity.class);
							startActivity(io);
							finish();

						}else if(moveto.equals("Mydealer")){
							Intent io = new Intent(AddMerchantNew.this,
									MyDealersActivity.class);
							io.putExtra("Key","MenuClick");
							startActivity(io);
							finish();

						}else if(moveto.equals("postnotes")){
							if (Constants.USER_TYPE.equals("D")) {
								Intent io = new Intent(AddMerchantNew.this,
										DealersComplaintActivity.class);
								startActivity(io);
								finish();

							} else {
								Intent io = new Intent(AddMerchantNew.this,
										MerchantComplaintActivity.class);
								startActivity(io);
								finish();

							}

						}

						else if(moveto.equals("clientvisit")){


							Intent io = new Intent(AddMerchantNew.this,
									SMClientVisitHistory.class);
							startActivity(io);
							finish();

						}

						else if(moveto.equals("paymentcoll")){

							Intent io = new Intent(AddMerchantNew.this,
									SalesmanPaymentCollectionActivity.class);
							startActivity(io);
							finish();

						}
						else if(moveto.equals("stockentry")){

							Intent io = new Intent(AddMerchantNew.this,
									OutletStockEntry.class);
							startActivity(io);
							finish();

						}
						else if(moveto.equals("acknowledge")){

							Intent io = new Intent(AddMerchantNew.this,
									SalesmanAcknowledgeActivity.class);
							startActivity(io);
							finish();

						}
						else if(moveto.equals("surveyuser")){

							Intent io = new Intent(AddMerchantNew.this,
									SurveyMerchant.class);
							startActivity(io);
							finish();

						}
						else if(moveto.equals("mydayplan")){

							Intent io = new Intent(AddMerchantNew.this,
									MyDayPlan.class);
							startActivity(io);
							finish();

						}
						else if(moveto.equals("mysales")){

							Intent io = new Intent(AddMerchantNew.this,
									MySalesActivity.class);
							startActivity(io);
							finish();

						} else if (moveto.equals("diststock")) {
							Intent io = new Intent(AddMerchantNew.this,
									DistributrStockActivity.class);
							startActivity(io);
							finish();
						}

					}

					dialog.dismiss();

				}catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
				}

			}

			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();


				JsonServiceHandler = new JsonServiceHandler(Utils.strGetProfileDetail+Constants.USER_ID, AddMerchantNew.this);
				JsonAccountObject = JsonServiceHandler.ServiceDataGet();
				return null;
			}
		}.execute(new Void[]{});
	}

	public void companynamevalidation(){
		Cursor cursor2;
		cursor2=db.rawQuery("SELECT * from merchanttable where companyname ="+"'"+companyName+"'", null);
		Log.e("CompanyNameCount", String.valueOf(cursor2.getCount()));

		if(cursor2.getCount()==0){
			mobilenovalidation();
		}else{
			showAlertDialogToast("Already registered with this Company Name");
		}
	}

	public void mobilenovalidation(){
		Cursor cursor2;
		cursor2=db.rawQuery("SELECT * from merchanttable where mobileno ="+"'"+mobile+"'", null);
		Log.e("MobileNoCount", String.valueOf(cursor2.getCount()));

		if(cursor2.getCount()==0){
			//Hide this line for new modification 10-09-2019 vel
			//databaseHandler.addmerchant(companyName,"New User", fullname, mobile, "R", email, address,city,pincode,tinnumber,"","",currentDate,pan_no,customdata,strStockiest,strRoute,strState);
			databaseHandler.addMerchantNew(companyName,"New User",
					fullname, mobile, "R", email, address,city,pincode,
					tinnumber,"","",currentDate,pan_no,customdata,strStockiest,
					strRoute,strState, hierarchyobj, hierarchyText, String.valueOf(merchatGeolat), String.valueOf(merchantGeolon));
			Cursor curs;
			curs = databaseHandler.getmerchantwithoutorder();
			Log.e("merchantcount", String.valueOf(curs.getCount()));
			if (curs != null && curs.getCount() > 0) {
				if (curs.moveToLast()) {

					do {
						Log.e("offbeatmuserid", curs.getString(0));
						Log.e("cpmnyname", curs.getString(1));
						Log.e("fullname", curs.getString(3));
						Log.e("userid", curs.getString(2));
						Log.e("mobile", curs.getString(4));
						Log.e("email", curs.getString(6));
						Log.e("address", curs.getString(7));
						Log.e("city", curs.getString(8));
						Log.e("pincode", curs.getString(9));
						Log.e("tin", curs.getString(10));
						Log.e("pan_no", curs.getString(10));
						Log.e("stockiest", curs.getString(16));
						Log.e("route", curs.getString(17));
						validatemobile = curs.getString(4);
						offbeatmuserid = curs.getString(0);
						CURRENT_NEW_MERCHANT_ID = curs.getInt(0);;
						newmerchantcmpnyname = curs.getString(1);

					} while (curs.moveToNext());


				}
				Log.e("outside", "outside");

				if (validatemobile.equals(mobile)) {

					textViewMobile.setText("");
					textViewAddress.setText("");
					textViewEmail.setText("");
					textViewName.setText("");
					textViewCompanyName.setText("");
					textViewTin.setText("");
					textViewPan.setText("");
					textViewPinCode.setText("");
					textViewAddress3.setText("");
					textViewAddress2.setText("");
					//move to survey screen if new user is added in local DB
					Cursor cursor;
					cursor = databaseHandler.getdealer();
					Log.e("count", String.valueOf(cursor.getCount()));
					if(cursor.getCount()>0){
						cursor.moveToFirst();
						selected_dealer = cursor.getString(cursor
								.getColumnIndex(DatabaseHandler.KEY_Duserid));
					}

					SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
					Date d = new Date();
					String dayofWeek = sdf.format(d);
					Log.e("dateday", dayofWeek);
					if(dayofWeek.equals("Monday")){
						day_mon="Y";
						Log.e("dbday","Monday");
					}else if(dayofWeek.equals("Tuesday")){
						day_tue="Y";
						Log.e("dbday","Tuesday");
					}else if(dayofWeek.equals("Wednesday")){
						day_wed="Y";
						Log.e("dbday","Wednesday");
					}else if(dayofWeek.equals("Thursday")){
						day_thu="Y";
						Log.e("dbday","Thursday");
					}else if(dayofWeek.equals("Friday")){
						day_fri="Y";
						Log.e("dbday","Friday");
					}else if(dayofWeek.equals("Saturday")){
						day_sat="Y";
						Log.e("dbday","Saturday");
					}else if(dayofWeek.equals("Sunday")){
						day_sun="Y";
						Log.e("dbday","Sunday");
					}
					Log.e("outside","outside");
					Log.e("day_wed",day_wed);

					String inputFormat1 = new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss").format(new Date());
					Log.e("inputFormat",inputFormat1);

					if(netCheck()==true) {

						if (userBeatModel.equalsIgnoreCase("C")) {
							databaseHandler.addCompanyModelbeat(selectedbeatId, "New User", Constants.USER_ID, selected_dealer, "N", "N",
									"N", "N", "N", "N", "N", "Active", inputFormat1, inputFormat1, "Inactive", offbeatmuserid, userBeatModel, selectedbeat, distributorname, distributoruserType, distributorCompanyName);

						} else {
							databaseHandler.addDistributorModelbeat("", "New User", Constants.USER_ID, selected_dealer, day_mon, day_tue,
									day_wed, day_thu, day_fri, day_sat, day_sun, "Active", inputFormat1, inputFormat1, "Inactive", offbeatmuserid, "D", "");

						}
					}else
					{
						if (userBeatModel.equalsIgnoreCase("C")) {
							databaseHandler.addCompanyModelbeat(selectedbeatId, "New User", Constants.USER_ID, selected_dealer, "N", "N",
									"N", "N", "N", "N", "N", "Active", inputFormat1, inputFormat1, "OffInactive", offbeatmuserid, userBeatModel, selectedbeat, distributorname, distributoruserType, distributorCompanyName);

						} else {
							databaseHandler.addDistributorModelbeat("", "New User", Constants.USER_ID, selected_dealer, day_mon, day_tue,
									day_wed, day_thu, day_fri, day_sat, day_sun, "Active", inputFormat1, inputFormat1, "OffInactive", offbeatmuserid, "D", "");

						}
					}
					Cursor curs1;
					curs1 = databaseHandler.getbeat();
					Log.e("beatCount", String.valueOf(curs1.getCount()));
					if(curs1.getCount() > 0){
						if(curs1.moveToLast()) {
							curBeatRowId = curs1.getInt(0);
						}else {
							curBeatRowId = 0;
						}
						curs1.close();
					}
					saveofflineheader();




				} else {

					showAlertDialogToast("Beat not assigned for this merchant");
				}
			}
		}else{
			showAlertDialogToast("Already registered with this Mobile Number");
		}
	}

	public  void saveofflineheader(){

		String inputFormat1 = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss").format(new java.util.Date());
		Log.e("inputFormat",inputFormat1);

		String inputPattern = "yyMMddhhmmssMs";

		SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
		String dt =inputFormat.format(new java.util.Date());

		String endtime = new SimpleDateFormat(
				"HH:mm:ss").format(new java.util.Date());
		Log.e("endtime",endtime);

		String startTime = new SimpleDateFormat(
				"HH:mm:ss").format(new java.util.Date());
		Log.e("endtime",endtime);

		if(Constants.USER_ID != null && !Constants.USER_ID.equalsIgnoreCase("null")){
			OFFLINE_MERCHANT_NO_OR_ORDER_NO = Constants.USER_ID + dt;
		}else {
			int random = Utils.getRandomNumberInRange(100, 10000);
			OFFLINE_MERCHANT_NO_OR_ORDER_NO = random + dt;
		}



		databaseHandler.addorderheader(Constants.USER_ID,"New User",companyName,"","Outlet", inputFormat1, "0",
				OFFLINE_MERCHANT_NO_OR_ORDER_NO, "", createordergeolat, createordergeolog,startTime,endtime,"");

		Cursor curs;
		curs = databaseHandler.getNewMerchantOrders();
		Log.e("orderHeaderCount", String.valueOf(curs.getCount()));

		if(curs!=null && curs.getCount() > 0) {

			if (curs.moveToLast()) {
				orderno = curs.getString(0);
				pushstatus=curs.getString(4);
				String date =curs.getString(5);
				Log.e("onlineorderno", curs.getString(6));
				Log.e("orderno", orderno);
				Log.e("Hdate", inputFormat1);
				databaseHandler.setMerchantOfflineId(OFFLINE_MERCHANT_NO_OR_ORDER_NO, CURRENT_NEW_MERCHANT_ID);
				databaseHandler.setBeatOfflineId(OFFLINE_MERCHANT_NO_OR_ORDER_NO, curBeatRowId);
				saveofflineDetail(orderno);
			}
		}
	}
	public void saveofflineDetail(String orderno) {
		Log.e("orde number", String.valueOf(orderno));
		productstatus = "Outlet";

		String inputFormat = new SimpleDateFormat(
				"yyyy-MM-dd").format(new java.util.Date());
		db = getApplicationContext().openOrCreateDatabase("freshorders", 0, null);
		Cursor curs1;
		curs1 = db.rawQuery("SELECT * from dealertable", null);
		Log.e("dEALER", String.valueOf(curs1.getCount()));
		if (curs1.getCount() > 0) {
			curs1.moveToFirst();
			selected_dealer = curs1.getString(curs1
					.getColumnIndex(DatabaseHandler.KEY_Duserid));
		}

		databaseHandler.addorderdetail(orderno, selected_dealer, "", "0", "1", "", productstatus, "", "", "",
				inputFormat, "", "", 0.0, "", "", "null", "null", "null", "", "", "", 0.0, "", "", "");

		Cursor curs;
		curs = databaseHandler.getorderdetail(orderno);

		Log.e("detailcount", String.valueOf(curs.getCount()));
		if (netCheckwithoutAlert() == true) {
			if (Constants.jsonArCustomFieldsMerchant != null) {
				convertjson();


			} else if (Constants.jsonArCustomFieldsMerchant == null) {

				try {
					if (netCheckwithoutAlert() == true) {
						convertjson();

					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}


		} else {
			if (Constants.jsonArCustomFieldsMerchant != null) {
				String cursettings;
				cursettings = databaseHandler.getSalesmanDataUserSetting("SCUSFIELD");
				System.out.println("cursettings:"+cursettings);
				if (cursettings.equalsIgnoreCase("Y")) {
					showAlertDialogToast1("Merchant details added successfully");
					Cursor cursorBeatDetails= db.rawQuery("select * from beat",null);

					Log.e("Off_BEATS:",""+cursorBeatDetails);
					if(cursorBeatDetails!=null) {
						Log.e("Off_BeatCount:", "" + cursorBeatDetails.getCount());
					}

				}else{ /// Kumaravel this is newly added so purpose only before they not show condition not met.
					showAlertDialogToast1("Merchant details added successfully");
				}
			} else if (Constants.jsonArCustomFieldsMerchant == null) {

				try {
					showAlertDialogToast2("Merchant details added successfully");

				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}

		}

	}

	public void loadUIComponents(JSONArray jsonArCustomComponents)
	{
		try {

			viewArCustomComponents = new View[jsonArCustomComponents.length()];

			for(int i=0;i<jsonArCustomComponents.length();i++)
			{
				setCustomViewComponent(viewArCustomComponents[i],jsonArCustomComponents.getJSONObject(i));
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}


	}


	public  void setCustomViewComponent(View v,JSONObject jsonCustomField)
	{
		try {

			if(jsonCustomField.getString("uicomptype").equalsIgnoreCase("dropdown")){
				final Spinner spinner = new Spinner(this);
				String[] strArSpinnerItems = (jsonCustomField.getString("reflabel")+","+jsonCustomField.getString("refvalue")).split(",");
				ArrayAdapter<String> adapterSpinnerCategory = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, strArSpinnerItems);
				spinner.setAdapter(adapterSpinnerCategory);
				v=spinner;
				ll.addView(v);
				customValue = spinner.getSelectedItem()
						.toString();

			}
			if(jsonCustomField.getString("uicomptype").equalsIgnoreCase("switch")){
				RadioGroup radioGroup = new RadioGroup(this);
				LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(
						LinearLayout.LayoutParams.FILL_PARENT,
						LinearLayout.LayoutParams.WRAP_CONTENT
				);
				v=radioGroup;
				TextView tvRGTitle=new TextView(this);
				tvRGTitle.setText(jsonCustomField.getString("reflabel"));
				ll.addView(tvRGTitle);
				ll.addView(v, p);
				String strRefValues=jsonCustomField.getString("refvalue");
				String strArRefValues[]=strRefValues.split(",");
				RadioButton radioButtonView[]=new RadioButton[strArRefValues.length];
				if(strArRefValues!=null && strArRefValues.length>0)
				{
					for(int rbc=0;rbc<strArRefValues.length;rbc++)
					{
						radioButtonView[rbc] = new RadioButton(this);
						radioButtonView[rbc].setText(strArRefValues[rbc]);
						//radioButtonView.setOnClickListener(this);
						radioGroup.addView(radioButtonView[rbc], p);
					}
				}
			}

			if(jsonCustomField.getString("uicomptype").equalsIgnoreCase("multiselect")){

				Spinner spinner = new Spinner(this);
				spinner.setPrompt(jsonCustomField.getString("reflabel"));
				String[] strArSpinnerCategory = (jsonCustomField.getString("reflabel")+","+jsonCustomField.getString("refvalue")).split(",");
				v=spinner;
				ll.addView(v);
				ArrayList<SpinnerDomain> listVOs4 = new ArrayList<>();
				for (int i = 0; i < strArSpinnerCategory.length; i++) {
					SpinnerDomain spinnerdomain = new SpinnerDomain();
					spinnerdomain.setTitle(strArSpinnerCategory[i]);
					spinnerdomain.setSelected(false);
					listVOs4.add(spinnerdomain);

				}
				NippoAddMerchantAdapter adapter = new NippoAddMerchantAdapter(AddMerchantNew.this, 0,
						listVOs4,this);
				spinner.setAdapter(adapter);



			}


		}catch (Exception ex)
		{
			ex.printStackTrace();
		}

	}

	@Override
	public void onClickButton(List<SpinnerDomain> list) {
		Log.e(TAG, "onClickButton: "+ " listener");
		for (SpinnerDomain data : list) {
			if(data.isSelected()) {
				Log.e(TAG, "onClickButton: " + data.getTitle());
			}
		}

	}

	public void convertjson(){
		final   JSONArray resultSet = new JSONArray();


		Cursor curs;
		curs = databaseHandler.getNewMerchantOrders();
		Log.e("HeaderCount", String.valueOf(curs.getCount()));
		curs.moveToFirst();
		while (curs.isAfterLast() == false) {

			int totalColumn = curs.getColumnCount();
			JSONObject rowObject = new JSONObject();

			for( int i=0 ;  i<totalColumn  ; i++ )
			{
				if( curs.getColumnName(i) != null  )
				{

					try
					{

						if( curs.getString(i) != null )
						{
							if(!curs.getString(i).equals("New User")){
								Log.d("TAG_NAME", curs.getString(i) );
								rowObject.put(curs.getColumnName(i) ,  curs.getString(i) );
							}

						}
						else
						{
							if(!curs.getColumnName(i).equals("muserid")){
								rowObject.put(curs.getColumnName(i), "");
							}

						}
					}
					catch( Exception e )
					{
						Log.d("TAG_NAME", e.getMessage()  );
					}
				}

			}

			try {
				//Kumaravel 13-07-2019
				if(Constants.distributorname == null || Constants.distributorname.isEmpty()){
					Constants.distributorname = new PrefManager(AddMerchantNew.this).getStringDataByKey(PrefManager.KEY_DISTRIBUTOR_NAME);
				}
				rowObject.put("distributorname",Constants.distributorname);
			} catch (Exception e) {
				Log.d("Exception in Dist. name", e.getMessage());
			}


			try{
				db=getApplicationContext().openOrCreateDatabase("freshorders", 0, null);

				Cursor curs1;
				curs1=db.rawQuery("SELECT * from dealertable", null);
				Log.e("dEALER", String.valueOf(curs1.getCount()));
				if(curs1.getCount()>0){
					curs1.moveToFirst();
					selected_dealer = curs1.getString(curs1
							.getColumnIndex(DatabaseHandler.KEY_Duserid));
				}

				Cursor cursor2;
				Constants.Merchantname =rowObject.getString("mname");

				cursor2=db.rawQuery("SELECT * from merchanttable where companyname ="+"'"+Constants.Merchantname+"'", null);

				cursor2.moveToFirst();
				while (cursor2.isAfterLast() == false) {

					int Columncount = cursor2.getColumnCount();
					Log.e("merchantColumn", String.valueOf(cursor2.getColumnCount()));
					JSONObject merchantdetail = new JSONObject();
					for( int j=0 ;  j<Columncount  ; j++ )
					{
						if( cursor2.getColumnName(j) != null  )
						{
							try
							{

								////////////////////////////////////added for hierarchy text
								String columnName = cursor2.getColumnName(j);
								if(columnName.equalsIgnoreCase("hierarchytext")){
									merchantdetail.put(cursor2.getColumnName(j), cursor2.getString(j));

								}else if(columnName.equalsIgnoreCase("hierarchyobj")){
									String jsonStr = cursor2.getString(j);
									Log.e("hierarchyobj","....UnmodifiedStr.." + jsonStr);
									JSONArray array = new JSONArray(jsonStr);
									merchantdetail.put(cursor2.getColumnName(j), array);
									Log.e("hierarchyobj","....modifiedStr.." + array.toString());

								} else {

									if (cursor2.getString(j) != null) {
										if (!cursor2.getString(j).equals("New User")) {

											merchantdetail.put(cursor2.getColumnName(j), cursor2.getString(j));

										}
									} else {
										if (!cursor2.getColumnName(j).equals("userid")) {
											merchantdetail.put(cursor2.getColumnName(j), "");
										}
									}
								}

							}
							catch( Exception e )
							{
								Log.d("Merchant_Exception", e.getMessage()  );
							}
						}

					}
					merchantdetail.put("usertype","M");
					merchantdetail.put("duserid",selected_dealer);
					rowObject.put("merchant",merchantdetail);
					cursor2.moveToNext();
				}

				cursor2.close();

				Log.e("Detailarray", rowObject.toString());

			} catch (JSONException e) {
				e.printStackTrace();
			}



			Log.e("jsonarray", rowObject.toString());

			JSONArray resultDetail 	= new JSONArray();
			try{

				Cursor cursor;
				cursor = databaseHandler.getorderdetail(rowObject.getString("oflnordid"));
				Log.e("DetailCount", String.valueOf(cursor.getCount()));
				cursor.moveToFirst();
				while (cursor.isAfterLast() == false) {

					int Columncount = cursor.getColumnCount();
					Log.e("Columncount", String.valueOf(Columncount));
					Log.e("Columncount", String.valueOf(cursor.getColumnCount()));
					JSONObject detailObject = new JSONObject();

					for( int i=0 ;  i<21 ; i++ ) //17
					{
						if( cursor.getColumnName(i) != null  )
						{

							try
							{

								if( cursor.getString(i) != null )
								{
									Log.d("TAG_NAME", cursor.getString(i) );
									detailObject.put(cursor.getColumnName(i), cursor.getString(i));
								}
								else
								{
									detailObject.put(cursor.getColumnName(i), "");
								}
							}
							catch( Exception e )
							{
								Log.d("TAG_NAME", e.getMessage()  );
							}
						}

					}

					resultDetail.put(detailObject);
					cursor.moveToNext();
				}

				cursor.close();

				Log.e("Detailarray", resultDetail.toString());

			} catch (JSONException e) {
				e.printStackTrace();
			}
			try{
				rowObject.put("orderdtls",resultDetail);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			resultSet.put(rowObject);
			curs.moveToNext();
		}
		curs.close();


		Log.e("SaveArray", resultSet.toString());



		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "",strOrderId ="";
			JSONObject jsonObject = new JSONObject();
			@Override
			protected void onPreExecute() {
				dialog= ProgressDialog.show(AddMerchantNew.this, "",
						"Loading...", true, true);
				//	dialog.setCancelable(false);
			}

			@Override
			protected void onPostExecute(Void result) {

				try {

					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);

					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);

					if(strStatus.equals("true")) {
						strOrderId = JsonAccountObject.getJSONObject("data").getString("ordid");
						Log.e("strOrderId", strOrderId);
						String muserid = JsonAccountObject.getJSONObject("data").getString("muserid");
						Log.e("muserid", muserid);
						String updatedFlag = jsonObject.getJSONObject("merchant").getString("cflag");
						Log.e("updatedFlag", updatedFlag);
						ContentValues contentValues1 = new ContentValues();
						contentValues1.put(DatabaseHandler.KEY_MOBILENUMER, jsonObject.getJSONObject("merchant").getString("mobileno"));
						contentValues1.put(DatabaseHandler.KEY_Muserid, muserid);
						contentValues1.put(DatabaseHandler.KEY_MFlag, updatedFlag);
						databaseHandler.updateMerchant(contentValues1);
						Log.e("updateMerchant", String.valueOf(contentValues1));
						Cursor curs1;
						curs1 = databaseHandler.getmerchantDetails(muserid);
						Log.e("Outlet/Merchant Count", String.valueOf(curs1.getCount()));

						if (curs1 != null && curs1.getCount() > 0) {

							if (curs1.moveToFirst()) {
								Log.e("merchant name", curs1.getString(0));
								Log.e("merchant id", curs1.getString(1));
								merchantRowid =curs1.getString(0);
							}
						}
						databaseHandler.updateBeatMercahant(muserid,merchantRowid);
						Cursor curs2;
						curs2 = databaseHandler.getbeat();
						Log.e("countbeat ", String.valueOf(curs2.getCount()));


						db.execSQL("DELETE FROM oredrdetail where ( ordstatus = 'Outlet' ) AND ( oflnordid = " + OfflineOrderNo + " ) ");

						Log.e(" oredrdetail","DELETE FROM oredrdetail where ( ordstatus = 'Outlet' ) AND ( oflnordid = " + OfflineOrderNo + ")");

						db.execSQL("DELETE FROM orderheader where ( pushstatus = 'Outlet' ) AND ( oflnordid = " + OfflineOrderNo + " ) ");

						Log.e(" orderheader","DELETE FROM orderheader where ( pushstatus = 'Outlet' ) AND ( oflnordid = " + OfflineOrderNo + " ) ");

						Cursor curs;
						curs = databaseHandler.getNoOrders();
						Log.e("Exist Count", String.valueOf(curs.getCount()));
						beatupdate();

						////////////////// what to update local count of merchant by one Kumaravel
						try {
							String strValue = databaseHandler.getConstantValue("OUTLETCOUNT");
							if (strValue != null && !strValue.isEmpty()) {
								int Count = Integer.parseInt(strValue);
								Count++;
								databaseHandler.setConstantValue("OUTLETCOUNT", String.valueOf(Count));
							}
						}catch (Exception e){
							e.printStackTrace();
						}

					}
					Constants.Merchantname="";
					Constants.SalesMerchant_Id="";


				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {

				}

				dialog.dismiss();
			}

			@Override
			protected Void doInBackground(Void... params) {

				try{
					for(int k=0;k<resultSet.length();k++){
						jsonObject = resultSet.getJSONObject(k);
						Log.e("jsonObjectOnline", String.valueOf(jsonObject));
						OfflineOrderNo =jsonObject.getString("oflnordid");
						JSONObject jsonMerchant= jsonObject.getJSONObject("merchant");
						Log.e("jsonMerchantOnline", String.valueOf(jsonMerchant));
						String strCustomData=jsonMerchant.getString("customdata");
						Log.e("strCustomDataOnline",strCustomData);
						JSONArray jsonCustomData=new JSONArray(jsonMerchant.getString("customdata"));
						Log.e("jsonCustomDataOnline", String.valueOf(jsonCustomData));
						jsonMerchant.put("customdata",jsonCustomData);
						jsonObject=new JSONObject(""+jsonObject);
						jsonMerchant.put("createdby",Constants.USER_ID);
						Log.e("createdbyOnlinde",Constants.USER_ID);
						//jsonMerchant.put("merchant",""+jsonMerchant);
						jsonObject.put("merchant",jsonMerchant);
						Log.e("merchantONline",""+jsonMerchant);
						Log.e("jsonObjectONline",jsonObject.toString());
						//jsonObject.put("merchant",jsonMerchant);
						JsonServiceHandler = new JsonServiceHandler(Utils.strsavemerchantOderdetail,jsonObject.toString(), AddMerchantNew.this);
						JsonAccountObject = JsonServiceHandler.ServiceData();
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				return null;

			}
		}.execute(new Void[]{});


	}
	public  void beatupdate() {

		Cursor cursor;
		cursor = db.rawQuery("SELECT * from beat where flag ='Inactive'", null);
		Log.e("beatCount", String.valueOf(cursor.getCount()));
		if (cursor != null && cursor.getCount() > 0) {
			cursor.moveToFirst();
			final JSONArray beatarray = new JSONArray();
			while (cursor.isAfterLast() == false) {

				int Columncount = cursor.getColumnCount();
				JSONObject beatobject = new JSONObject();


				for (int i = 0; i < Columncount; i++) {
					if (cursor.getColumnName(i) != null) {

						try {

							if (cursor.getString(i) != null) {
								if(cursor.getColumnName(i).equals("suserid") ){
									if(!cursor.getString(18).equalsIgnoreCase("C") ){
										beatobject.put(cursor.getColumnName(i), cursor.getString(i));
									}

								}else{
									beatobject.put(cursor.getColumnName(i), cursor.getString(i));
								}
								Log.d("TAG_NAME", cursor.getString(i));

							} else {
								beatobject.put(cursor.getColumnName(i), "");

							}
						} catch (Exception e) {
							Log.d("TAG_NAME", e.getMessage());
						}
					}

				}


				beatarray.put(beatobject);

				cursor.moveToNext();
			}

			cursor.close();

			Log.e("Detailarray", beatarray.toString());

			new AsyncTask<Void, Void, Void>() {
				String strStatus = "";
				String strMsg = "";
				ProgressDialog dialog;
				@Override
				protected void onPreExecute() {
					dialog= ProgressDialog.show(AddMerchantNew.this, "",
							"Loading...", true, true);
					//dialog.setCancelable(true);
				}

				@Override
				protected void onPostExecute(Void result) {
					try {

						strStatus = JsonAccountObject.getString("status");
						Log.e("return status", strStatus);
						strMsg = JsonAccountObject.getString("message");
						Log.e("return message", strMsg);

						if (strStatus.equals("true")) {

							Log.e("ïnside","ïnside");
							db.execSQL("UPDATE beat SET flag = 'Active' WHERE rowid =" + "'" + beatrowid + "'");
							Log.e("ïnside","outside");
							if(Constants.jsonArCustomFieldsMerchant!=null) {
								showAlertDialogToast1("Merchant details added successfully");
							}
							if(Constants.jsonArCustomFieldsMerchant==null) {
								showAlertDialogToast2("Merchant details added successfully");
							}

							dialog.dismiss();
						}else{
							dialog.dismiss();
						}


					} catch (JSONException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
					dialog.dismiss();
				}

				@Override
				protected Void doInBackground(Void... params) {
					JSONObject jsonObject = new JSONObject();
					try {

						for (int k = 0; k < beatarray.length(); k++) {
							jsonObject = beatarray.getJSONObject(k);
							beatrowid = jsonObject.getString("rowid");
							JsonServiceHandler = new JsonServiceHandler(Utils.strsavebeat, jsonObject.toString(), getApplicationContext());
							JsonAccountObject = JsonServiceHandler.ServiceData();


						}
					} catch (JSONException e) {
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
					return null;

				}
			}.execute(new Void[]{});

		}
	}
	public void onlineMerchant() {
		new AsyncTask<Void, Void, Void>() {
			String strStatus = "", strMsg;
			ProgressDialog dialog;
			String city;


			@Override
			protected void onPreExecute() {

			}
			@Override
			protected void onPostExecute(Void result) {

				try {
					Log.e("onPostExecute","onPostExecute");
					strStatus = JsonAccountObject.getString("status");
					//Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);
					if(strStatus.equalsIgnoreCase("false")){

						textViewLocalMC1.setText("Online Count:0");
						textViewLocalMC2.setText("Offline Count:0");

					}
					if(strStatus.equalsIgnoreCase("true")) {
						Constants.onLineCount = JsonAccountObject.getInt("count");
						databaseHandler.setConstantValue("OUTLETCOUNT",""+Constants.onLineCount);
						Log.e("count00000", String.valueOf(Constants.onLineCount));
						int intCount = databaseHandler.getMerchantCount() - (Constants.onLineCount);
						System.out.println("A:" + databaseHandler.getMerchantCount() + "--B:" + Constants.onLineCount + "--C:" + intCount);
						Log.e("in_count", String.valueOf(intCount));
						textViewLocalMC1.setText("Online Count: " + JsonAccountObject.getInt("count"));
						textViewLocalMC2.setText("Offline Count :" + intCount);

						Log.e("countMerchant", JsonAccountObject.getString("count"));
						Log.e("OfflineCount", String.valueOf(databaseHandler.getMerchantCount()));
					}

				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}


			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();
				try {
					jsonObject.put("userid", Constants.USER_ID);
					jsonObject.put("usertype", Constants.USER_TYPE);




				JsonServiceHandler = new JsonServiceHandler(Utils.strGetMerchantList, jsonObject.toString(), AddMerchantNew.this);
				JsonAccountObject = JsonServiceHandler.ServiceData();
				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
				return null;
			}
		}.execute(new Void[]{});
	}



	private void merchantCount() {
		Log.e("MerchantCount","MerchantCount");
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;

			@Override
			protected void onPreExecute() {
				dialog = ProgressDialog.show(AddMerchantNew.this, "",
						"Loading...", true, true);

			}

			@Override
			protected void onPostExecute(Void result) {

				try {
					Log.e("onPostExecute","onPostExecute");

					//textViewLocalMC.setText("Online Count: " +JsonAccountObject.getString("count")+"  "+"Offline Count :"+ databaseHandler.getMerchantCount());
					Log.e("countMerchant",JsonAccountObject.getString("count"));


					dialog.dismiss();
				} catch (JSONException e) {
					e.printStackTrace();

				}
				dialog.dismiss();
			}

			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();
				try {
					jsonObject.put("duserid",Constants.DUSER_ID);
					Log.e("duserid-----",Constants.DUSER_ID);
					jsonObject.put("usertype", "M");
					jsonObject.put("createdby",Constants.USER_ID);
					Log.e("createdby-----",Constants.USER_ID);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				JsonServiceHandler = new JsonServiceHandler(Utils.strMerchantCount+"?where=%7B%22createdby%22%3A%22"+ Constants.USER_ID +"%22%2C%22usertype%22%3A%22M%22%7D",AddMerchantNew.this);
				JsonAccountObject = JsonServiceHandler.ServiceDataGet();
				return null;
			}
		}.execute(new Void[]{});
	}

	protected void toastDisplay(String msg) {
		Toast toast = Toast.makeText(AddMerchantNew.this, msg,
				Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

	}
	public boolean netCheck() {
		// for network connection
		try {
			ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetwork = connectionManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			Object result = null;
			if (mWifi.isConnected() || mNetwork.isConnected()) {

				return true;
			} else if (result == null) {

             /*showAlertDialog(CreateOrderActivity.this,
                        "No Internet Connection",
                        "Please Check Your internet connection.", false);

Log.e("offline","offline");*/
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	public boolean netCheckwithoutAlert() {
		// for network connection
		try {
			ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetwork = connectionManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			Object result = null;
			if (mWifi.isConnected() || mNetwork.isConnected()) {
				return true;
			}

			else if (result == null) {

				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	public void showAlertDialog(Context context, String title, String message,
								Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
					}
				});
		alertDialog.show();
	}
	public void showAlertDialogToast( String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();

		alert11.show();

		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

	}
	public void showAlertDialogToast1( String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(false);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						onlineMerchant();
						Intent intentAddMerchant = new Intent(AddMerchantNew.this, AddMerchantNew.class);
						startActivity(intentAddMerchant);


					}
				});


		AlertDialog alert11 = builder1.create();

		alert11.show();

		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

	}
	public void showAlertDialogToast2( String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(false);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						Intent intentAddMerchant = new Intent(AddMerchantNew.this, SurveyMerchant.class);
						startActivity(intentAddMerchant);

					}
				});


		AlertDialog alert11 = builder1.create();

		alert11.show();

		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

	}

	public void SalesmanDataLink() {
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "", strMsg = "";

			@Override
			protected void onPreExecute() {


			}

			@Override
			protected void onPostExecute(Void result) {
				super.onPostExecute(result);
				try {


					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);

					if (strStatus.equals("true")) {
						dialog.cancel();
						//showAlertDialogToast("Merchant details added successfully");
						dialog.dismiss();
						JSONArray job1 = JsonAccountObject.getJSONArray("data");
						Log.e("job1NEW_______", String.valueOf(job1));
						for (int i = 0; i < job1.length(); i++) {

							JSONObject job = new JSONObject();
							job = job1.getJSONObject(i);
							Log.e("jobNEW_______", String.valueOf(job));
							settingid = job.getString("settingid");
							userid = job.getString("userid");
							industryid = job.getString("industryid");
							updateddt = job.getString("updateddt");
							refkey = job.getString("refkey");
							refvalue = job.getString("refvalue");
							Log.e("refvalue--", String.valueOf(refvalue));
							data_type = job.getString("data_type");
							remarks = job.getString("remarks");
							status = job.getString("status");
						}

						dialog.dismiss();
					} else{
						if(strStatus.equals("false")) {
							Intent io=new Intent(AddMerchantNew.this,SurveyMerchant.class);
							startActivity(io);
							dialog.cancel();
						}

					}


				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}

			}


			@SuppressLint("LongLogTag")
			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();
				JsonServiceHandler = new JsonServiceHandler(Utils.strsettingmerchantvali +Constants.DUSER_ID,AddMerchantNew.this);
				JsonAccountObject = JsonServiceHandler.ServiceDataGet();
				Log.e("JsonAccountObjectNEW____", String.valueOf(JsonAccountObject));
				return null;

			}

		}.execute(new Void[]{});

	}

	public void onBackPressed() {
		exitAlret();
	}

	private void exitAlret() {
		AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
		localBuilder.setCancelable(false);
		localBuilder.setMessage("Do you want to Exit?");
		localBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {

						finish();

					}
				});
		localBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {
						paramDialogInterface.cancel();
					}
				});
		localBuilder.create().show();
	}

	//set the dynamic view based on the response if ui filed is changed in Response change it according

	//set the Spinner with Items
	public  void dynamicView(String[]  items)
	{
		Spinner s = new Spinner(this);
		s.setLayoutParams(lp);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, items);
		s.setAdapter(adapter);
		ll.addView(s);
	}

	// set the Edittext with hint
	public  void dynamicView(String hint)
	{

		EditText editText  = new EditText(this);
		//set the Background
		//editText.setBackgroundColor();
		//Set the Max Lines
		editText.setMaxLines(2);
		editText.setTextSize(14);
		editText.setHint(hint);
		editText.setLayoutParams(lp);
		ll.addView(editText);
	}

	private void hideKeyBoard(){
		// Check if no view has focus:
		View view = this.getCurrentFocus();
		if (view != null) {
			InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			if (imm != null) {
				imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
			}
		}
	}

    private void locationUpdateWithDelay(){

        showD(Constants.GPS_LOCATION_FETCHING);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable(){
            @Override
            public void run(){
                gpsCounter ++ ;
                if(mLastLocation != null) {
                    updateCurrentLocation(mLastLocation);
                }else {
                    if(gpsCounter >= 2){
                        hideD();
                        if(CheckGpsStatus()){
                            showAlertMSG(getResources().getString(R.string.gps_accuracy_setting));
                            return;
                        }else {
                            showSettingsAlert();
                            return;
                        }
                    }else {
                        locationUpdateWithDelay();
                    }

                }
                hideD();
                mobilenovalidation();////////////
            }
        }, 6000);
    }

    private void showD(String msg){
        if(pDialog == null){
            pDialog = ProgressDialog.show(AddMerchantNew.this, "", msg, true, false);
        }else {
            if (!pDialog.isShowing()) {
                pDialog = ProgressDialog.show(AddMerchantNew.this, "", msg, true, false);
            }
        }
    }

    private void hideD(){
        if(pDialog != null){
            if(pDialog.isShowing()){
                pDialog.dismiss();
            }
        }
    }

    synchronized private void updateCurrentLocation(Location mLastLocation){
        merchatGeolat = mLastLocation.getLatitude();
        merchantGeolon = mLastLocation.getLongitude();
    }


    private boolean CheckGpsStatus(){
        final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );
        return manager != null && manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    // Trigger new location updates at interval
    @SuppressLint("MissingPermission")
    protected void startLocationUpdates() {

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations, this can be null.
                        if (location != null) {
                            updateCurrentLocation(location);
                            mLastLocation = location;
                        }
                    }
                });
        try {
            locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            //    mLastLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            passive_enabled = locationManager.isProviderEnabled(LocationManager.PASSIVE_PROVIDER);

            if (gps_enabled) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, locationUpdateInterval, 0,
                        locationListenerGps);
            }

            if (network_enabled) {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, locationUpdateInterval, 0,
                        locationListenerNetwork);
            }
            if (passive_enabled) {
                locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, locationUpdateInterval, 0,
                        locationListenerPassive);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    LocationListener locationListenerGps = new LocationListener() {
        public void onLocationChanged(Location location) {
            if (location != null) {
                mLastLocation = location;
                updateCurrentLocation(location);
            }
        }
        public void onProviderDisabled(String provider) { }
        public void onProviderEnabled(String provider) { }
        public void onStatusChanged(String provider, int status, Bundle extras) { }
    };


    LocationListener locationListenerPassive = new LocationListener() {
        public void onLocationChanged(Location location) {
            if (location != null) {
                mLastLocation = location;
                updateCurrentLocation(location);
            }
        }
        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) { }
        @Override
        public void onProviderEnabled(String provider) { }
        @Override
        public void onProviderDisabled(String provider) { }

    };

    LocationListener locationListenerNetwork = new LocationListener() {
        public void onLocationChanged(Location location) {
            if (location != null) {
                mLastLocation = location;
                updateCurrentLocation(location);
            }
        }
        public void onProviderDisabled(String provider) { }
        public void onProviderEnabled(String provider) { }
        public void onStatusChanged(String provider, int status, Bundle extras) { }
    };


    public void showAlertMSG(String message) {

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setMessage(message);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        ////finish();//////////// 26-09-2019
                    }
                });

        builder.setNegativeButton("ALLOW WITHOUT LOCATION",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        ////finish();//////////// 26-09-2019
                        mobilenovalidation();////////
                    }
                });


        android.app.AlertDialog alert = builder.create();
        alert.show();

        Button buttonbackground = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground.setBackgroundColor(Color.parseColor("#A9E2F3"));

        Button negativeBtnBG = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        negativeBtnBG.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mFusedLocationClient == null) {
            startLocationUpdates();
        }
    }


}
