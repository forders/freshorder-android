package com.freshorders.freshorder.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.OutletStockViewAdapter;
import com.freshorders.freshorder.adapter.OutleyStockEntryAdapter;
import com.freshorders.freshorder.domain.MerchantOrderListDomain;
import com.freshorders.freshorder.domain.OutletStockEntryDetailDomain;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;
import com.leavjenn.smoothdaterangepicker.date.SmoothDateRangePickerFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Jayesh on 15/05/2017.
 */

public class OutletStockView extends Activity {

    TextView textViewProdName, textViewMerchName, textViewNoData, textViewcalendericon, textViewCalendar, textViewSatrtDate, textViewEndDate, textViewAssetMenu;
    EditText editTextFromDate, editTextToDate;
    ListView ListViewOutlet;

    JsonServiceHandler JsonServiceHandler;
    JSONObject JsonAccountObject = null;
    JSONArray JsonAccountArray = null;

    ArrayList<OutletStockEntryDetailDomain> arrayliststockdetail;
    Date dateStr = null, manufdateStr = null;
    String str = null, manufstr = null;
    OutletStockViewAdapter adapter;
    private int year, hr, sec, mins, day, mnth;
    public static Date dateStr11 = null, dateStr1 = null;

    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.outlet_stock_view);

        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();

        textViewMerchName = (TextView) findViewById(R.id.textViewMerchName);
        textViewProdName = (TextView) findViewById(R.id.textViewProdName);
        textViewNoData = (TextView) findViewById(R.id.textViewNoData);
        textViewCalendar = (TextView) findViewById(R.id.textViewCalendar);
        textViewSatrtDate = (TextView) findViewById(R.id.textViewSatrtDate);
        textViewEndDate = (TextView) findViewById(R.id.textViewEndDate);
        textViewAssetMenu = (TextView) findViewById(R.id.textViewAssetMenu);

        ListViewOutlet = (ListView) findViewById(R.id.ListViewOutlet);

        Typeface font = Typeface.createFromAsset(getAssets(),
                "fontawesome-webfont.ttf");
        textViewAssetMenu.setTypeface(font);
        textViewCalendar.setTypeface(font);
        showCurrentDateOnView();

        textViewAssetMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent io = new Intent(OutletStockView.this, OutletStockEntry.class);
                Constants.setorder = 1;
                startActivity(io);
                finish();
            }
        });

        textViewCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SmoothDateRangePickerFragment smoothDateRangePickerFragment =
                        SmoothDateRangePickerFragment
                                .newInstance(new SmoothDateRangePickerFragment.OnDateRangeSetListener() {
                                    @Override
                                    public void onDateRangeSet(SmoothDateRangePickerFragment view,
                                                               int yearStart, int monthStart,
                                                               int dayStart, int yearEnd,
                                                               int monthEnd, int dayEnd) {
                                        String date = "You picked the following date range: \n"
                                                + "From " + dayStart + "/" + (++monthStart)
                                                + "/" + yearStart + " To " + dayEnd + "/"
                                                + (++monthEnd) + "/" + yearEnd;

                                        String startDate = dayStart + "-" + (monthStart) + "-" + yearStart;
                                        String endDate = dayEnd + "-" + (monthEnd) + "-" + yearEnd;

                                        String inputPattern = "dd-MM-yyyy";
                                        String outputPattern = "dd-MMM-yyyy";
                                        SimpleDateFormat inputFormat = new SimpleDateFormat(
                                                inputPattern);
                                        SimpleDateFormat outputFormat = new SimpleDateFormat(
                                                outputPattern);

                                        String str = null, str1 = null;
                                        try {
                                            dateStr11 = inputFormat.parse(startDate);
                                            str = outputFormat.format(dateStr11);
                                            textViewSatrtDate.setText(str);

                                            dateStr1 = inputFormat.parse(endDate);
                                            str1 = outputFormat.format(dateStr1);
                                            textViewEndDate.setText(str1);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }

                                        if(netCheck()) {
                                            stockdetail(textViewSatrtDate.getText().toString(), textViewEndDate.getText().toString());
                                        }
                                    }

                                });

                smoothDateRangePickerFragment.show(getFragmentManager(), "Datepickerdialog");
            }


        });


    }

    public void stockdetail(final String startdate, final String enddate) {

        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "", strMsg = "";

            @Override
            protected void onPreExecute() {
                dialog = ProgressDialog.show(OutletStockView.this, "",
                        "Loading...", true, true);

            }

            @Override
            protected void onPostExecute(Void result) {

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {
                        ListViewOutlet.setVisibility(View.VISIBLE);
                        textViewNoData.setVisibility(View.GONE);
                        JSONArray job1 = JsonAccountObject.getJSONArray("data");

                        arrayliststockdetail = new ArrayList<OutletStockEntryDetailDomain>();
                        for (int i = 0; i < job1.length(); i++) {
                            JSONObject job = new JSONObject();
                            job = job1.getJSONObject(i);
                            OutletStockEntryDetailDomain dod = new OutletStockEntryDetailDomain();
                            dod.setprodcode(job.getString("prodcode"));
                            dod.setproductname(job.getString("prodname"));
                            dod.setstock(job.getString("qty"));

                            String time = job.getString("updateddt");
                            Log.e("time", time);

                            String[] timespilt = time.split("\\s+");
                            Log.e("time[0]", timespilt[0]);
                            Log.e("time[1]", timespilt[1]);

                            String manufdate = job.getString("manufdt");
                            Log.e("manufdate", manufdate);

                            String inputPattern = "yyyy-MM-dd";
                            String outputPattern = "dd-MMM-yyyy ";
                            SimpleDateFormat inputFormat = new SimpleDateFormat(
                                    inputPattern);
                            SimpleDateFormat outputFormat = new SimpleDateFormat(
                                    outputPattern);

                            try {
                                dateStr = inputFormat.parse(timespilt[0]);
                                manufdateStr = inputFormat.parse(manufdate);
                                str = outputFormat.format(dateStr);
                                manufstr = outputFormat.format(manufdateStr);

                                Log.e("str", str);
                                Log.e("manufstr", manufstr);

                                dod.setOrderdate(str);
                                dod.setmanufdt(manufstr);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            Log.e("inside", "inside");
                            arrayliststockdetail.add(dod);
                        }
                        setadap();

                    } else {
                        ListViewOutlet.setVisibility(View.GONE);
                        textViewNoData.setVisibility(View.VISIBLE);
                       // textViewNoData.setText(strMsg);
                    }
                    textViewMerchName.setText(OutletStockEntry.selectedmechantname);
                    textViewProdName.setText(OutletStockEntry.companyname + " [ " + OutletStockEntry.fullname + " ]");
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    //Log.e("ProductActivityException", e.toString());
                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();
                JSONArray jsonmuser = new JSONArray();
                JSONArray jsonprodid = new JSONArray();

                String inputPattern = "dd-MMM-yyyy";
                String outputPattern = "yyyy-MM-dd";
                SimpleDateFormat inputFormat = new SimpleDateFormat(
                        inputPattern);
                SimpleDateFormat outputFormat = new SimpleDateFormat(
                        outputPattern);

                String str = null, manufstr = null;
                try {
                    dateStr = inputFormat.parse(startdate);
                    manufdateStr = inputFormat.parse(enddate);
                    str = outputFormat.format(dateStr);
                    manufstr = outputFormat.format(manufdateStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    jsonObject.put("suserid", Constants.USER_ID);
                    jsonObject.put("usertype", Constants.USER_TYPE);
                    jsonObject.put("fromdt", str);
                    jsonObject.put("todt", manufstr);
                    jsonObject.put("reporttype", "STOCK");
                    jsonmuser.put(OutletStockEntry.merchantid);
                    jsonprodid.put(OutletStockEntry.prodid);
                    jsonObject.put("muserid", jsonmuser);//
                    jsonObject.put("prodid", jsonprodid);//

                } catch (JSONException e) {
                    e.printStackTrace();
                }


                JsonServiceHandler = new JsonServiceHandler(
                        Utils.strstockdetail, jsonObject.toString(),
                        OutletStockView.this);
                JsonAccountObject = JsonServiceHandler.ServiceData();
                return null;
            }

        }.execute(new Void[]{});
    }


    public void setadap() {
        // TODO Auto-generated method stub

        Log.e("size", String.valueOf(arrayliststockdetail.size()));
        adapter = new OutletStockViewAdapter(OutletStockView.this,
                R.layout.item_outlet_stock_view, arrayliststockdetail);
        ListViewOutlet.setAdapter(adapter);

    }


    private void showCurrentDateOnView() {
        // TODO Auto-generated method stub


        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        mnth = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        hr = c.get(Calendar.HOUR_OF_DAY);
        mins = c.get(Calendar.MINUTE);
        sec = c.get(Calendar.SECOND);

        String monthLength = String.valueOf(mnth + 1);
        if (monthLength.length() == 1) {
            monthLength = "0" + monthLength;
        }
        String dayLength = String.valueOf(day);
        if (dayLength.length() == 1) {
            dayLength = "0" + dayLength;
        }


        textViewSatrtDate.setText(new StringBuilder()
                .append(year).append("-").append(monthLength).append("-").append(dayLength)
        );


        String time = textViewSatrtDate.getText().toString();

        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd-MMM-yyyy ";
        SimpleDateFormat inputFormat = new SimpleDateFormat(
                inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(
                outputPattern);

        String str = null;
        try {
            dateStr = inputFormat.parse(time);
            str = outputFormat.format(dateStr);
            textViewSatrtDate.setText(str);
            textViewEndDate.setText(str);
        } catch (Exception e) {
            e.printStackTrace();
        }


        if(netCheck()) {
            stockdetail(textViewSatrtDate.getText().toString(), textViewEndDate.getText().toString());
        }
    }

    public boolean netCheck() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            }

            else if (result == null) {
                showAlertDialog(OutletStockView.this,
                        "No Internet Connection",
                        "Please Check Your internet connection.", false);
                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public void showAlertDialog(Context context, String title, String message,
                                Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        alertDialog.show();
    }


    public void onBackPressed() {
        exitAlret();
    }

    private void exitAlret() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        localBuilder.setMessage("Do you want to Exit?");
        localBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {

                        finish();

                    }
                });
        localBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {
                        paramDialogInterface.cancel();
                    }
                });
        localBuilder.create().show();
    }
}
