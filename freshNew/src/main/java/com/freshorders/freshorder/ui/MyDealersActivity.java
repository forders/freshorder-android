package com.freshorders.freshorder.ui;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.DealerOrderListAdapter;
import com.freshorders.freshorder.adapter.MerchantOrderDetailAdapter;
import com.freshorders.freshorder.adapter.MerchantOrderListAdapter;
import com.freshorders.freshorder.adapter.MyDealerListAdapter;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.DealerOrderListDomain;
import com.freshorders.freshorder.domain.MDealerCompDropdownDomain;
import com.freshorders.freshorder.domain.MerchantOrderDetailDomain;
import com.freshorders.freshorder.domain.MerchantOrderListDomain;
import com.freshorders.freshorder.domain.MyDealerListDomain;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.UploadFileToServer;
import com.freshorders.freshorder.utils.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MyDealersActivity extends Activity {

	public static TextView menuIcon, textViewAssetMenuProfile, textViewAssetMenuMyOrders,
			textViewAssetMenuMyDealers, textViewAssetMenuProducts,
			textViewAssetMenuPayment, textViewAssetMenuComplaint,
			textViewAssetMenuSignout,textViewAssetMenuClientVisit,textViewAssetMenuPaymentCollection,
			textViewAssetMenuCreateOrder,textViewAssetSearch,textViewHeader,textViewCross,backbutton,textViewAssetMenuRefresh;

	public static TextView textViewNodata;
	public static	int menuCliccked;
	public int i=0;
	public static LinearLayout linearLayoutMenuParent,linearLayoutProfile, linearLayoutMyOrders,
			linearLayoutMyDealers, linearLayoutProducts, linearLayoutPayment,linearLayoutClientVisit,
			linearLayoutComplaint, linearLayoutSignout,linearLayoutCreateOrder,
            linearlayoutSearchIcon,linearLayoutPaymentCollection,linearLayoutButtonConfirm,linearLayoutRefresh,linearlayoutdump1;

	public static ArrayList<MerchantOrderDetailDomain> arraylistMerchantOrderDetailList;
	public static EditText editTextSearchField;
	public static JSONObject JsonAccountObject = null;
	public static JSONArray JsonAccountArray = null;
	public static DatabaseHandler databaseHandler;
	public static JsonServiceHandler JsonServiceHandler ;
	public static ArrayList<MyDealerListDomain> arraylistDealerList;
	public static ArrayList<MyDealerListDomain> arraylistDealerSearchList;
	public static ListView listViewDealerList;
	public static Button buttonAddDealer,Imageconfirmorder;
	public static String Mfullname,Muserid,Mcmpnyname,mflag,stockiest,route;
	public static String Dfullname,Duserid,Dcompanyname,UDID,Daddress,isread,merchantmobileno,memail,maddress,pan_no,customdata,strStockiest,strState,strRoute;
	int count=0;
	public static String searchclick="0",PaymentStatus="null";
	public static String strMsg = "";
	public static MyDealerListAdapter adapter;
	public static String prodid,prodname,prodcode,userid,date,companyname,Porderdate,qty,fqty,Dorderdate,
			prodprice,prodtax,mfgdate,expdate,batchno,uom,prodimage;

	public static String usertype,key;
	public String imagepath1="",imagepath2="",imagepath3="",imagepath4="",imagepath5="",imagepath6="",
			imagepath7="",imagepath8="",imagepath9="",imagepath10="",imagepath11="",imagepath12="";
	public static String MerchantCount,DealerCount,ProductCount;
	public static Context context;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(1);
		setContentView(R.layout.my_dealers_screen);
		Cursor cur;
		netCheck();

		databaseHandler = new DatabaseHandler(getApplicationContext());
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();
		context = MyDealersActivity.this;
		Intent intent = getIntent();
		key = intent.getStringExtra("Key");

		cur = databaseHandler.getDetails();
		cur.moveToFirst();
		Log.e("Insertion Check",
				cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_username)));
		Log.e("Insertion Check",
                cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_password)));
		Log.e("Insertion Check",
				cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_usertype)));
		usertype =cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_usertype));
        PaymentStatus =cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_paymentStatus));

		buttonAddDealer = (Button) findViewById(R.id.buttonAddDealer);
		Imageconfirmorder = (Button) findViewById(R.id.Imageconfirmorder);
		linearLayoutCreateOrder = (LinearLayout) findViewById(R.id.linearLayoutCreateOrder);
		listViewDealerList = (ListView) findViewById(R.id.listViewDealerList);
		linearLayoutProfile = (LinearLayout) findViewById(R.id.linearLayoutProfile);
		linearLayoutMyOrders = (LinearLayout) findViewById(R.id.linearLayoutMyOrders);
		linearLayoutMyDealers = (LinearLayout) findViewById(R.id.linearLayoutMyDealers);
		linearLayoutProducts = (LinearLayout) findViewById(R.id.linearLayoutProducts);
		linearLayoutPayment = (LinearLayout) findViewById(R.id.linearLayoutPayment);
		linearLayoutComplaint = (LinearLayout) findViewById(R.id.linearLayoutComplaint);
		linearLayoutClientVisit= (LinearLayout) findViewById(R.id.linearLayoutClientVisit);
		linearLayoutPaymentCollection= (LinearLayout) findViewById(R.id.linearLayoutPaymentCollection);
		linearLayoutSignout = (LinearLayout) findViewById(R.id.linearLayoutSignout);
		linearlayoutSearchIcon=(LinearLayout) findViewById(R.id.linearlayoutSearchIcon);
		editTextSearchField=(EditText) findViewById(R.id.editTextSearchField);
        linearLayoutButtonConfirm=(LinearLayout) findViewById(R.id.linearLayoutButtonConfirm);
        linearLayoutRefresh= (LinearLayout) findViewById(R.id.linearLayoutRefresh);
        textViewAssetMenuRefresh = (TextView) findViewById(R.id.textViewAssetMenuRefresh);
		linearlayoutdump1= (LinearLayout) findViewById(R.id.linearlayoutdump1);
		//Imageconfirmorder.setVisibility(Button.GONE);
		//
		// buttonAddDealer.setVisibility(Button.VISIBLE);
		Log.e("usertypt", Constants.USER_TYPE);

		if (Constants.USER_TYPE.equals("D")) {
			linearLayoutPaymentCollection.setVisibility(View.GONE);
			linearLayoutMyDealers.setVisibility(View.GONE);
			linearLayoutProducts.setVisibility(View.VISIBLE);
			linearLayoutCreateOrder.setVisibility(View.GONE);
		} else if (Constants.USER_TYPE.equals("M")){
			linearLayoutPaymentCollection.setVisibility(View.GONE);
			linearLayoutMyDealers.setVisibility(View.VISIBLE);
			linearLayoutProducts.setVisibility(View.GONE);
			linearLayoutCreateOrder.setVisibility(View.GONE);
            linearLayoutRefresh.setVisibility(View.GONE);
			linearlayoutdump1.setVisibility(View.VISIBLE);
		}else {
			linearLayoutMyDealers.setVisibility(View.VISIBLE);
			linearLayoutProducts.setVisibility(View.GONE);
			linearLayoutPayment.setVisibility(LinearLayout.GONE);
            linearLayoutRefresh.setVisibility(View.VISIBLE);
		}

		/*if(netCheck()==false){
			buttonAddDealer.setVisibility(Button.GONE);
		}*/
		menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
		backbutton=(TextView) findViewById(R.id.backbutton);

		if(Constants.USER_TYPE.equals("M")){
			linearLayoutButtonConfirm.setVisibility(LinearLayout.VISIBLE);
			Imageconfirmorder.setVisibility(Button.GONE);
			buttonAddDealer.setVisibility(Button.VISIBLE);
		}else{
			linearLayoutButtonConfirm.setVisibility(LinearLayout.GONE);
			Imageconfirmorder.setVisibility(Button.GONE);
			buttonAddDealer.setVisibility(Button.VISIBLE);
		}
		if( key.equals("ImageOrder")){
            linearLayoutButtonConfirm.setVisibility(LinearLayout.VISIBLE);
			Imageconfirmorder.setVisibility(Button.VISIBLE);
			buttonAddDealer.setVisibility(Button.GONE);
			menuIcon.setVisibility(View.GONE);
			backbutton.setVisibility(View.VISIBLE);
		}
		backbutton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Constants.adapterset=1;
				Intent io=new Intent(MyDealersActivity.this,CameraPhotoCapture.class);
				startActivity(io);
				finish();
			}
		});
		buttonAddDealer.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutProducts.setVisibility(View.GONE);
				linearLayoutCreateOrder.setVisibility(View.VISIBLE);
				// TODO Auto-generated method stub
				Intent io=new Intent(MyDealersActivity.this,AddDealerActivityNew.class);
				startActivity(io);
				finish();
			}
		});

		textViewAssetMenuCreateOrder= (TextView) findViewById(R.id.textViewAssetMenuCreateOrder);
		textViewAssetMenuProfile = (TextView) findViewById(R.id.textViewAssetMenuProfile);
		textViewAssetMenuMyOrders = (TextView) findViewById(R.id.textViewAssetMenuMyOrders);
		textViewAssetMenuMyDealers = (TextView) findViewById(R.id.textViewAssetMenuMyDealers);
		textViewAssetMenuProducts = (TextView) findViewById(R.id.textViewAssetMenuProducts);
		textViewAssetMenuPayment = (TextView) findViewById(R.id.textViewAssetMenuPayment);
		textViewAssetMenuComplaint = (TextView) findViewById(R.id.textViewAssetMenuComplaint);
		textViewAssetMenuClientVisit= (TextView) findViewById(R.id.textViewAssetMenuClientVisit);
		textViewAssetMenuSignout = (TextView) findViewById(R.id.textViewAssetMenuSignout);
		textViewAssetSearch =(TextView) findViewById(R.id.textViewAssetSearch);
		textViewHeader=(TextView) findViewById(R.id.textViewHeader);
		textViewCross=(TextView) findViewById(R.id.textViewCross);
		textViewNodata=(TextView) findViewById(R.id.textViewNodata);
		textViewAssetMenuPaymentCollection=(TextView) findViewById(R.id.textViewAssetMenuPaymentCollection);


		linearLayoutMenuParent = (LinearLayout) findViewById(R.id.linearLayoutMenuParent);
		Typeface font = Typeface.createFromAsset(getAssets(),
				"fontawesome-webfont.ttf");
		menuIcon.setTypeface(font);
		backbutton.setTypeface(font);
		textViewAssetMenuCreateOrder.setTypeface(font);
		textViewAssetMenuProfile.setTypeface(font);
		textViewAssetMenuMyOrders.setTypeface(font);
		textViewAssetMenuMyDealers.setTypeface(font);
		textViewAssetMenuProducts.setTypeface(font);
		textViewAssetMenuPayment.setTypeface(font);
		textViewAssetMenuComplaint.setTypeface(font);
		textViewAssetMenuSignout.setTypeface(font);
		textViewAssetMenuClientVisit.setTypeface(font);
		textViewAssetSearch.setTypeface(font);
		textViewCross.setTypeface(font);
		textViewAssetMenuPaymentCollection.setTypeface(font);
        textViewAssetMenuRefresh.setTypeface(font);

		if(cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_usertype)).equals("M")){

			linearLayoutClientVisit.setVisibility(View.GONE);
		}

		if(key.equals("AddDealer") && usertype.equals("S")){

			databaseHandler.synTableDelete();

			offlineDealers();
		}else if(usertype.equals("M")){
			MyDealers();
		}else{

			Cursor curr;
			curr = databaseHandler.getdealer();
			if(curr.getCount()!=0){

				setofflinDealer();
			}else{

				listViewDealerList.setVisibility(View.GONE);
				textViewNodata.setVisibility(View.VISIBLE);
				textViewNodata.setText("Please add new dealer");
				linearlayoutSearchIcon.setClickable(false);
				textViewAssetSearch.setVisibility(TextView.VISIBLE);


			}
		}
        linearLayoutRefresh.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("Clicked", "Clicked");

                if(netCheck()){

                    final Dialog qtyDialog = new Dialog(MyDealersActivity.this);
                    qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    qtyDialog.setContentView(R.layout.warning_dialog);

                    final Button buttonUpdateOk, buttonUpdateCancel;

                    final TextView textQtyValidate;

                    buttonUpdateOk = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateOk);
                    buttonUpdateCancel = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateCancel);

                    textQtyValidate = (TextView) qtyDialog
                            .findViewById(R.id.textQtyValidate);
                    qtyDialog.show();


                    buttonUpdateOk.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            databaseHandler.synTableDelete();
                            Constants.refreshscreen="mydealer";
                            Intent io = new Intent(MyDealersActivity.this, RefreshActivity.class);
                            startActivity(io);
                            finish();
                            qtyDialog.dismiss();
                        }

                    });

                    buttonUpdateCancel
                            .setOnClickListener(new OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub

                                    qtyDialog.dismiss();

                                }
                            });

                }else{
                    showAlertDialogToast("Please Check Your internet connection");
                }
            }

        });



        listViewDealerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Constants.Imageduserid = arraylistDealerList.get(position).getDuserid();
				String ImageDuserCompany = arraylistDealerList.get(position).getComName();
				Log.e("ImageDuserCompany", ImageDuserCompany);

				if (key.equals("ImageOrder")) {
					showAlertDialogToast(ImageDuserCompany + " Selected");
				}

			}
		});

		Imageconfirmorder.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (!Constants.Imageduserid.equals("")) {
					paymentcheck();
				} else {
					showAlertDialogToast("Please select the dealer");
				}

			}
		});
		menuIcon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (menuCliccked == 0) {
					linearLayoutMenuParent.setVisibility(View.VISIBLE);
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(menuIcon.getWindowToken(), 0);
					editTextSearchField.setVisibility(EditText.GONE);
					textViewHeader.setVisibility(TextView.VISIBLE);
					textViewCross.setVisibility(TextView.GONE);
					menuCliccked = 1;
				} else {
					linearLayoutMenuParent.setVisibility(View.GONE);
					textViewHeader.setVisibility(TextView.VISIBLE);
					editTextSearchField.setVisibility(EditText.GONE);
					textViewCross.setVisibility(TextView.GONE);
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(linearlayoutSearchIcon.getWindowToken(), 0);
					menuCliccked = 0;
				}

			}
		});
		linearlayoutSearchIcon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (searchclick == "0") {
					textViewHeader.setVisibility(TextView.GONE);
					editTextSearchField.setVisibility(EditText.VISIBLE);
					editTextSearchField.setFocusable(true);
					editTextSearchField.requestFocus();
					textViewCross.setVisibility(TextView.VISIBLE);

					linearLayoutMenuParent.setVisibility(View.GONE);
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
					Log.e("Keyboard", "Show");
					searchclick = "1";
				} else {
					textViewHeader.setVisibility(TextView.VISIBLE);
					editTextSearchField.setVisibility(EditText.GONE);
					textViewCross.setVisibility(TextView.GONE);
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(linearlayoutSearchIcon.getWindowToken(), 0);
					searchclick = "0";
				}
			}
		});
		editTextSearchField.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				textViewCross.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						editTextSearchField.setText("");
						if (editTextSearchField.equals("")) {
							InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
							imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
						}
					}
				});
			}

			@Override
			public void afterTextChanged(Editable s) {
				arraylistDealerList = new ArrayList<MyDealerListDomain>();
				String searchText1 = editTextSearchField.getText().toString()
						.toLowerCase(Locale.getDefault());
				for (MyDealerListDomain pd : arraylistDealerSearchList) {
					if (pd.getComName().toLowerCase(Locale.getDefault())
							.contains(searchText1) || pd.getDuserid().toLowerCase(Locale.getDefault())
							.contains(searchText1)) {
						arraylistDealerList.add(pd);
					}


				}
				MyDealersActivity.this.runOnUiThread(new Runnable() {

					@Override
					public void run() {
						adapter = new MyDealerListAdapter(
								MyDealersActivity.this,
								R.layout.item_my_dealer,
								arraylistDealerList);
						listViewDealerList.setAdapter(adapter);
					}
				});
			}
		});


		linearLayoutMenuParent.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
			}
		});

		linearLayoutProfile.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				// TODO Auto-generated method stub

				if(netCheck()==true){
					linearLayoutMenuParent.setVisibility(View.GONE);
					menuCliccked = 0;
					getDetails("profile");
				}else{
					showAlertDialogToast("Please Check Your internet connection");
				}

			}
		});

		linearLayoutMyOrders.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(netCheck()==true) {
					Constants.orderstatus = "Success";
					linearLayoutMenuParent.setVisibility(View.GONE);
					menuCliccked = 0;
					getDetails("myorders");
				}else{
					if(PaymentStatus.equals("Pending")||PaymentStatus.equals(null) ||PaymentStatus.equals("")||PaymentStatus.equals("null")){
						showAlertDialogToast("Please make payment before place order");
					}else{
                        if (Constants.USER_TYPE.equals("D")) {
                            Intent io = new Intent(MyDealersActivity.this,
                                    DealersOrderActivity.class);
                            startActivity(io);
                            finish();
                        } else if (Constants.USER_TYPE.equals("M")) {
                            Intent io = new Intent(MyDealersActivity.this,
                                    MerchantOrderActivity.class);
                            startActivity(io);
                            finish();
                        } else {
                            Intent io = new Intent(MyDealersActivity.this,
                                    SalesManOrderActivity.class);
                            startActivity(io);
                            finish();
                        }
					}
				}


			}
		});

		linearLayoutMyDealers.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				// TODO Auto-generated method stub


			}
		});


		linearLayoutCreateOrder.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
                if(netCheck()==true){
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    Constants.checkproduct =0;
                    menuCliccked = 0;
                    getDetails("createorder");
                }else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    }else{
                        Intent io = new Intent(MyDealersActivity.this,
                                CreateOrderActivity.class);
                        startActivity(io);
                        finish();
                    }
                }


			}
		});

		linearLayoutPayment.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
                if(netCheck()==true){
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Intent to = new Intent(MyDealersActivity.this,
                            PaymentActivity.class);
                    startActivity(to);
                    finish();
                }else{
                    showAlertDialogToast("Please Check Your internet connection");
                }


			}
		});


		linearLayoutComplaint.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (netCheck() == true) {
					linearLayoutMenuParent.setVisibility(View.GONE);
					menuCliccked = 0;
					getDetails("postnotes");
				}else {
					showAlertDialogToast("Please Check Your internet connection");
				}
			}
		});
		linearLayoutClientVisit.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (netCheck() == true) {
					linearLayoutMenuParent.setVisibility(View.GONE);
					menuCliccked = 0;
					// TODO Auto-generated method stub

					getDetails("clientvisit");
				}else {
					if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
						showAlertDialogToast("Please make payment before place order");
					}else{
						Intent to = new Intent(MyDealersActivity.this,
								SMClientVisitHistory.class);
						startActivity(to);
						finish();
					}
				}

			}
		});
		linearLayoutPaymentCollection.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (netCheck() == true) {
					linearLayoutMenuParent.setVisibility(View.GONE);
					menuCliccked = 0;
					getDetails("paymentcoll");
				}else {
					showAlertDialogToast("Please Check Your internet connection");
				}


			}
		});
		linearLayoutSignout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				linearLayoutMenuParent.setVisibility(View.GONE);
				menuCliccked = 0;
				// TODO Auto-generated method stub
				databaseHandler.delete();
				Intent io = new Intent(MyDealersActivity.this, SigninActivity.class);
				startActivity(io);
				finish();
			}
		});

	}

	public void paymentcheck() {

		final Dialog qtyDialog = new Dialog(MyDealersActivity.this);
		qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		qtyDialog.setContentView(R.layout.paymenttype);
		qtyDialog.show();
		Button buttonUpdateOk, buttonUpdateCancel;

		buttonUpdateOk = (Button) qtyDialog
				.findViewById(R.id.buttonUpdateOk);
		buttonUpdateCancel = (Button) qtyDialog
				.findViewById(R.id.buttonUpdateCancel);


		buttonUpdateOk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {


				RadioGroup radioPayment;
				RadioButton radioCash;
				radioPayment = (RadioGroup) qtyDialog.findViewById(R.id.radioPayment);
				int selectedId = radioPayment.getCheckedRadioButtonId();

				// find the radiobutton by returned id
				radioCash = (RadioButton) qtyDialog.findViewById(selectedId);
				Constants.PaymentType = radioCash.getText().toString();

				qtyDialog.dismiss();

				saveOrder();
			}

		});

		buttonUpdateCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				qtyDialog.dismiss();
			}
		});
	}

	private void getDetails(final String paymentcheck) {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog= ProgressDialog.show(MyDealersActivity.this, "",
						"Loading...", true, true);

			}

			@Override
			protected void onPostExecute(Void result) {

				try {

					PaymentStatus=(JsonAccountObject.getString("pymtstatus"));
					Log.e("PaymentStatus",PaymentStatus);


					if(PaymentStatus.equals("Pending")||PaymentStatus.equals(null) ||PaymentStatus.equals("")||PaymentStatus.equals("null")){
						showAlertDialogToast("Please make payment before place order");
					}else{
						Log.e("paymentcheck", paymentcheck);

						if(paymentcheck.equals("profile")){
							Intent io = new Intent(MyDealersActivity.this,
									ProfileActivity.class);
							startActivity(io);
							finish();

						}else if(paymentcheck.equals("myorders")){

							if (Constants.USER_TYPE.equals("D")) {
								Intent io = new Intent(MyDealersActivity.this,
										DealersOrderActivity.class);
								startActivity(io);
								finish();
							} else if (Constants.USER_TYPE.equals("M")) {
								Intent io = new Intent(MyDealersActivity.this,
										MerchantOrderActivity.class);
								startActivity(io);
								finish();
							} else {
								Intent io = new Intent(MyDealersActivity.this,
										SalesManOrderActivity.class);
								startActivity(io);
								finish();
							}

						}else if(paymentcheck.equals("postnotes")){

							if (Constants.USER_TYPE.equals("D")) {
								Intent io = new Intent(MyDealersActivity.this,
										DealersComplaintActivity.class);
								startActivity(io);
								finish();
							} else {
								Intent io = new Intent(MyDealersActivity.this,
										MerchantComplaintActivity.class);
								startActivity(io);
								finish();
							}

						}
						else if(paymentcheck.equals("createorder")){

								Intent io = new Intent(MyDealersActivity.this,
										CreateOrderActivity.class);
								startActivity(io);
								finish();
						}
						else if(paymentcheck.equals("clientvisit")){


								Intent io = new Intent(MyDealersActivity.this,
										SMClientVisitHistory.class);
								startActivity(io);
								finish();


						}
						else if(paymentcheck.equals("paymentcoll")){
							Intent io = new Intent(MyDealersActivity.this,
									SalesmanPaymentCollectionActivity.class);
							startActivity(io);
							finish();
						}

					}

					dialog.dismiss();


				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
				}

			}

			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();


				JsonServiceHandler = new JsonServiceHandler(Utils.strGetProfileDetail+Constants.USER_ID, MyDealersActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceDataGet();
				return null;
			}
		}.execute(new Void[]{});
	}

	private void saveOrder() {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "",strOrderId;

			@Override
			protected void onPreExecute() {
				dialog= ProgressDialog.show(MyDealersActivity.this, "Loading Images...",
						"Loading...", true, true);
			}


			@Override
			protected void onPostExecute(Void result) {

				try {

					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);
					if(strStatus.equals("true")){
						strOrderId = JsonAccountObject.getJSONObject("data").getString("ordid");
						Log.e("strOrderId", strOrderId);


						Log.e("Camera", String.valueOf(MerchantOrderActivity.arraylistimagepath.size()));
						for(int k=0;k<MerchantOrderActivity.arraylistimagepath.size();k++){
							Log.e("inside", "inside");

							if(k==0){
								imagepath1=MerchantOrderActivity.arraylistimagepath.get(0).toString();
							}else if(k==1){
								imagepath2=MerchantOrderActivity.arraylistimagepath.get(1).toString();
							}else if(k==2){
								imagepath3=MerchantOrderActivity.arraylistimagepath.get(2).toString();
							}else if(k==3){
								imagepath4=MerchantOrderActivity.arraylistimagepath.get(3).toString();
							}else if(k==4){
								imagepath5=MerchantOrderActivity.arraylistimagepath.get(4).toString();
							}else if(k==5){
								imagepath6=MerchantOrderActivity.arraylistimagepath.get(5).toString();
							}else if(k==6){
								imagepath7=MerchantOrderActivity.arraylistimagepath.get(6).toString();
							}else if(k==7){
								imagepath8=MerchantOrderActivity.arraylistimagepath.get(7).toString();
							}else if(k==8){
								imagepath9=MerchantOrderActivity.arraylistimagepath.get(8).toString();
							}else if(k==9){
								imagepath10=MerchantOrderActivity.arraylistimagepath.get(9).toString();
							}else if(k==10){
								imagepath11=MerchantOrderActivity.arraylistimagepath.get(10).toString();
							}else if(k==11){
								imagepath12=MerchantOrderActivity.arraylistimagepath.get(11).toString();
							}

							Log.e("path", String.valueOf(MerchantOrderActivity.arraylistimagepath.get(k)));

						}

						UploadFileToServer ufs = new UploadFileToServer(
								MyDealersActivity.this, imagepath1,imagepath2,imagepath3,imagepath4,
								imagepath5,imagepath6,imagepath7,imagepath8,
								imagepath9,imagepath10,imagepath11,imagepath12,strOrderId);

                        ufs.execute();
                      /* Intent to = new Intent (CameraPhotoCapture.this, MerchantOrderConfirmation.class);
                        to.putExtra("strOrderId",strOrderId);
                        startActivity(to);
                        finish();*/


					}else{
						showAlertDialogToast(strMsg);
					}



				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					//Log.e("ProductActivityException", e.toString());
				}
				dialog.dismiss();
			}

			@Override
			protected Void doInBackground(Void... params) {
				JSONObject jsonObject = new JSONObject();
				try {
					JSONArray jsonArray = new JSONArray();
					JSONObject jsonObject1,jsonObject2;

					String  inputFormat= new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss").format(new java.util.Date());


					jsonObject1 = new JSONObject();
					jsonObject2 = new JSONObject();
					try {

						isread=Constants.USER_ID+","+Constants.Imageduserid;
                        Log.e("isread",isread);
						jsonObject2.put("image", "Y");
						jsonObject2.put("duserid", Constants.Imageduserid);
						jsonObject2.put("ordslno", "0");
						jsonObject2.put("ordstatus", "Pending");
						jsonObject2.put("deliverydt",inputFormat);
						jsonObject2.put("isread",isread);
						jsonArray.put(jsonObject2);
						jsonObject.put("orderdtls", jsonArray);




					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}






					jsonObject.put("muserid", Constants.USER_ID);
					jsonObject.put("usertype", Constants.USER_TYPE);
					jsonObject.put("orderdt",inputFormat);


					if(Constants.PaymentType.equals("Cash")){
						jsonObject.put("iscash","C");
					}else if(Constants.PaymentType.equals("Credit")){
						jsonObject.put("iscash", "CR");
					}

					Log.e("payment type", Constants.PaymentType);

					jsonObject.put("orderdtls", jsonArray);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				JsonServiceHandler = new JsonServiceHandler(Utils.strsavemerchantOderdetail,jsonObject.toString(), MyDealersActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceData();
				return null;
			}
		}.execute(new Void[]{});
	}

//new offline merchant


	//new mydealeroffline
	public  void offlineDealers() {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			String strStatus = "";
			ProgressDialog dialog;
			@Override
			protected void onPreExecute() {
				dialog= ProgressDialog.show(MyDealersActivity.this, "",
						"Initializing Dealer table ...", true, true);
				dialog.setCancelable(false);
			}


			@Override
			protected void onPostExecute(Void result) {

				try {

					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);

					if (strStatus.equals("true")) {


						JSONArray job1 = JsonAccountObject.getJSONArray("data");


						for (int i = 0; i < job1.length(); i++) {

							JSONObject job = new JSONObject();
							job = job1.getJSONObject(i);
							UDID=job.getString("usrdlrid");
							Duserid = job.getJSONObject("D").getString("userid");
							Dfullname =job.getJSONObject("D").getString("fullname");
							Dcompanyname=job.getJSONObject("D").getString("companyname");
							Daddress=job.getJSONObject("D").getString("address");


							String inputFormat = new SimpleDateFormat(
									"yyyy-MM-dd HH:mm:ss").format(new Date());
							Log.e("inputFormat",inputFormat);

							databaseHandler.adddealer(Dcompanyname, UDID, Duserid, Dfullname, Daddress,"");


						}
						Cursor cur;
						cur = databaseHandler.getdealer();
						Log.e("dealercount", String.valueOf(cur.getCount()));
						DealerCount=String.valueOf(cur.getCount());
						if(cur!=null && cur.getCount() > 0) {
							if (cur.moveToLast()) {

								Log.e("Ddate", String.valueOf(cur.getString(6)));
							}
						}
						setofflinDealer();
						OfflineProducts();


					} else {

						listViewDealerList.setVisibility(View.GONE);
						textViewNodata.setVisibility(View.VISIBLE);
						textViewNodata.setText(strMsg);
						linearlayoutSearchIcon.setClickable(false);
						textViewAssetSearch.setVisibility(TextView.VISIBLE);

					}
					dialog.dismiss();


				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {

				}

			}

			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();


				JsonServiceHandler = new JsonServiceHandler(Utils.strofflineDealerList + Constants.USER_ID,MyDealersActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceDataGet();
				return null;
			}
		}.execute(new Void[]{});

	}

	public  void setofflinDealer(){
		Log.e("inside","inside");
		arraylistDealerList = new ArrayList<MyDealerListDomain>();
		arraylistDealerSearchList=new ArrayList<MyDealerListDomain>();
		arraylistDealerList.clear();
		arraylistDealerSearchList.clear();
		Cursor curs;
		curs = databaseHandler.getdealer();
		Log.e("dealerCount", String.valueOf(curs.getCount()));

		if(curs!=null && curs.getCount() > 0) {
			if (curs.moveToFirst()) {

				do{
					Log.e("inside", "inside");
					MyDealerListDomain dod = new MyDealerListDomain();
					dod.setserialno(String.valueOf(i+1));
					dod.setComName(curs.getString(1));
					dod.setID(curs.getString(2));
					dod.setDuserid(curs.getString(3));
					dod.setName(curs.getString(4));
					dod.setAddress(curs.getString(5));
					arraylistDealerList.add(dod);
					arraylistDealerSearchList.add(dod);
i++;
				} while (curs.moveToNext());


			}
			Log.e("outside", "outside");
		}
		setoffadp();
	}

	public  void setoffadp() {
		// TODO Auto-generated method stub


		adapter = new MyDealerListAdapter(
				MyDealersActivity.this,
				R.layout.item_my_dealer,
				arraylistDealerList);
		listViewDealerList.setVisibility(ListView.VISIBLE);
		listViewDealerList.setAdapter(adapter);

		textViewNodata.setVisibility(TextView.GONE);
	}


	public  void OfflineProducts(){
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";


			@Override
			protected void onPreExecute() {

				dialog = ProgressDialog.show(MyDealersActivity.this, "",
						"Initializing Products table ...", true,true);
				dialog.setCancelable(false);



			}

			@Override
			protected void onPostExecute(Void result) {

				super.onPostExecute(result);
				if (!dialog.isShowing()){
					offlineMerchant();
				}




			}

			@Override
			protected Void doInBackground(Void... params) {

				//Do something...
				JSONObject jsonObject = new JSONObject();
				try {
					jsonObject.put("userid", Constants.USER_ID);
					jsonObject.put("status","Active");

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				JsonServiceHandler = new JsonServiceHandler(Utils.strofflineproduct, jsonObject.toString(), MyDealersActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceData();

				try {


					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);

					if (strStatus.equals("true")) {


						JSONArray job1 = JsonAccountObject.getJSONArray("data");
						System.out.println("job1SERVER:"+ String.valueOf(job1));
						count=job1.length();
						int i;
						for ( i = 0; i < job1.length(); i++) {
							JSONObject job = new JSONObject();
							job = job1.getJSONObject(i);
							prodid=job.getString("prodid");
							prodname=job.getString("prodname");
							Log.e("prodnameSERVER",prodname);
							prodcode=job.getString("prodcode");
							userid=job.getString("userid");
							qty="";
							fqty="";
							date=job.getString("updateddt");
							companyname=job.getString("companyname");
							prodprice = job.getString("prodprice");
							prodtax = job.getString("prodtax");
                            mfgdate = job.getString("manufdt");
                            expdate = job.getString("expirydt");
                            batchno = job.getString("batchno");
							uom = job.getString("uom");

							if(uom.equals(null)|| uom.isEmpty()||uom.equals("null")){
								uom="";
							}
							//currentdate="2016-07-04";
							String inputFormat = new SimpleDateFormat(
									"yyyy-MM-dd HH:mm:ss").format(new Date());

							databaseHandler.addProd(userid, companyname, prodcode, prodid, prodname, qty, fqty, date,
                                    inputFormat,prodprice,prodtax,batchno,mfgdate,expdate,uom,"","","",prodimage,"");




						}
						Cursor cur;
						cur = databaseHandler.getProduct();
						ProductCount= String.valueOf(cur.getCount());
						Log.e("productCount", String.valueOf(cur.getCount()));

						if(i==count){
							dialog.dismiss();

						}


					}else{
						dialog.dismiss();
					}



				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {

				}

				return null;

			}
		}.execute(new Void[]{});

	}
	public  void offlineMerchant() {
		new AsyncTask<Void, Void, Void>() {
			String city;
			ProgressDialog dialog;
			String strStatus="";
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				dialog= ProgressDialog.show(MyDealersActivity.this, "",
						"Initializing Merchant table ...", true,true);
				dialog.setCancelable(false);
			}

			@Override
			protected void onPostExecute(Void result) {
				super.onPostExecute(result);
				if (!dialog.isShowing()){
					showAlertDialogToast("Stockiest/Dealer Count :" + DealerCount + "\n" + "Product Count :" + ProductCount + "\n" + "Outlet/Merchant Count :" + MerchantCount + "\n");

					Constants.syn = 1;

				}
			}


			@Override
			protected Void doInBackground(Void... params){
				{
					JSONObject jsonObject = new JSONObject();

					try {
						jsonObject.put("userid", Constants.USER_ID);
						jsonObject.put("usertype", Constants.USER_TYPE);


					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					JsonServiceHandler = new JsonServiceHandler(Utils.strGetMerchantList, jsonObject.toString(),MyDealersActivity.this);
					JsonAccountObject = JsonServiceHandler.ServiceData();
					try {
						strStatus = JsonAccountObject.getString("status");
						Log.e("return status", strStatus);
						strMsg = JsonAccountObject.getString("message");
						Log.e("return message", strMsg);
						if(strStatus.equals("true")) {
							MDealerCompDropdownDomain cd;

							JSONArray JsonArray = JsonAccountObject.getJSONArray("data");
							int merchntcount=JsonArray.length();
							int i;
							for ( i= 0; i < JsonArray.length(); i++) {
								cd = new MDealerCompDropdownDomain();
								JsonAccountObject = JsonArray.getJSONObject(i);
								Mfullname = JsonAccountObject.getString("fullname");
								Mcmpnyname = JsonAccountObject.getString("companyname");
								Muserid = JsonAccountObject.getString("userid");
								merchantmobileno= JsonAccountObject.getString("mobileno");
								mflag=JsonAccountObject.getString("cflag");
								memail=JsonAccountObject.getString("emailid");
								maddress=JsonAccountObject.getString("address");
								city=JsonAccountObject.getString("city");
								strStockiest=JsonAccountObject.getString("stockiest");
								strRoute=JsonAccountObject.getString("route");
								// String day=JsonAccountObject.getString("address");

								databaseHandler.addmerchant(Mcmpnyname, Muserid, Mfullname,merchantmobileno,mflag,memail,maddress,city,"","","","Monday","",pan_no,customdata,strStockiest,strRoute,strState);


							}

							Cursor cur;
							cur = databaseHandler.getmerchant();
							Log.e("merchantcount", String.valueOf(cur.getCount()));
							MerchantCount = String.valueOf(cur.getCount());

							if(i==merchntcount) {
								dialog.dismiss();
							}
						}else{
							dialog.dismiss();
						}

					} catch (JSONException e) {
						Log.e("JSONException", e.toString());

					} catch (Exception e) {
						Log.e("Exception", e.toString());


					}

					return null;
				}
			}
		}.execute(new Void[] {});
	}

	public static void MyDealers() {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";


			@Override
			protected void onPreExecute() {
				dialog = ProgressDialog.show(context, "",
						"Loading...", true, true);

			}

			@Override
			protected void onPostExecute(Void result) {

				try {

					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);

					if (strStatus.equals("true")) {


						JSONArray job1 = JsonAccountObject.getJSONArray("data");
						arraylistDealerList = new ArrayList<MyDealerListDomain>();
						arraylistDealerSearchList=new ArrayList<MyDealerListDomain>();

						for (int i = 0; i < job1.length(); i++) {
							try {
								JSONObject job = new JSONObject();
								job = job1.getJSONObject(i);
								MyDealerListDomain dod = new MyDealerListDomain();
								dod.setID(job.getString("usrdlrid"));
								dod.setDuserid(job.getJSONObject("D").getString("userid"));
								dod.setName(job.getJSONObject("D").getString("fullname"));
								dod.setComName(job.getJSONObject("D").getString("companyname"));
								dod.setAddress(job.getJSONObject("D").getString("address"));
								dod.setserialno(String.valueOf(i+1));
								arraylistDealerList.add(dod);
								arraylistDealerSearchList.add(dod);
							}catch (JSONException e) {

							}
						}
						setadap();

					} else {

						listViewDealerList.setVisibility(View.GONE);
						textViewNodata.setVisibility(View.VISIBLE);
						textViewNodata.setText(strMsg);
						linearlayoutSearchIcon.setClickable(false);
						textViewAssetSearch.setVisibility(TextView.VISIBLE);

					}
					dialog.dismiss();


				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {

				}

			}

			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();


				JsonServiceHandler = new JsonServiceHandler(Utils.strDealerList + Constants.USER_ID+"&filter[where][dstatus]=Active", context);
				JsonAccountObject = JsonServiceHandler.ServiceDataGet();
				return null;
			}
		}.execute(new Void[]{});

	}
	public  static void setadap() {
		// TODO Auto-generated method stub


		adapter = new MyDealerListAdapter(
				context,
				R.layout.item_my_dealer,
				arraylistDealerList);
		listViewDealerList.setVisibility(View.VISIBLE);
		listViewDealerList.setAdapter(adapter);

		textViewNodata.setVisibility(View.GONE);
	}

	protected void toastDisplay(String msg) {
		Toast toast = Toast.makeText(MyDealersActivity.this, msg,
				Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

	}// http://52.77.252.149:2001/api/users/merchantlist
	//{"userid":"2690","usertype":"S"}
	public boolean netCheck() {
		// for network connection
		try {
			ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetwork = connectionManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			Object result = null;
			if (mWifi.isConnected() || mNetwork.isConnected()) {
				//setofflinDealer();
				return true;
			} else if (result == null) {
				//setofflinDealer();
						/*showAlertDialog(MyDealersActivity.this,
								"No Internet Connection",
								"Please Check Your internet connection.", false);*/
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public void showAlertDialog(Context context, String title, String message,
								Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
					}
				});
		alertDialog.show();
	}

	public void showAlertDialogToast(String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();

		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

	}

	public void onBackPressed() {
		exitAlret();
	}

	private void exitAlret() {
		AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
		localBuilder.setCancelable(false);
		localBuilder.setMessage("Do you want to Exit?");
		localBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {

						finish();

					}
				});
		localBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {
						paramDialogInterface.cancel();
					}
				});
		localBuilder.create().show();
	}
}
