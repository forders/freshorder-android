package com.freshorders.freshorder.ui;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Karthika on 08/04/2018.
 */

public class  AddSalesmanDataActivity extends Activity  {
    TextView menuIcon, textViewAssetMenuProfile, textViewAssetMenuMyOrders,
            textViewAssetMenuMyDealers, textViewAssetMenuProducts,
            textViewAssetMenuPayment, textViewAssetMenuComplaint,
            textViewAssetMenuSignout, textViewAssetMenuCreateOrder,
            textViewAssetMenuClientVisit, textViewAssetMenuPaymentCollection, textViewClient, textViewAssetMenuRefresh, textViewAssetMenuDistanceCalculation,
            textViewAssetMenuStockEntry, textViewAssetMenuAddMerchant, textViewAssetMenuAcknowledge, textViewAssetMenusurveyuser, textViewAssetMenudownloadscheme, textViewAssetMenuMyDayPlan, textViewAssetMenuMySales,
            textViewAssetMenuDistStock;

    int menuCliccked;
    public static String PaymentStatus = "NULL", moveto = "NULL", suserid, duserid = null;
    LinearLayout linearLayoutMenuParent, linearLayoutProfile, linearLayoutMyOrders,
            linearLayoutMyDealers, linearLayoutProducts, linearLayoutPayment,
            linearLayoutComplaint, linearLayoutSignout, linearLayoutEdit, linearLayoutDetails,
            linearLayoutCreateOrder, linearLayoutExport, linearLayoutClientVisit, linearLayoutPaymentCollection, linearLayoutRefresh,
            linearLayoutAcknowledge, linearLayoutStockEntry, linearLayoutAddMerchant, linearLayoutDistanceCalculation,
            linearLayoutsurveyuser, linearLayoutdownloadscheme, linearLayoutMyDayPlan, linearLayoutMySales, linearLayoutDistStock;

    JSONObject JsonAccountObject;
    JSONArray JsonAccountArray;
    DatabaseHandler databaseHandler;
    public static String status = "Active", usertype = "S";
    com.freshorders.freshorder.utils.JsonServiceHandler JsonServiceHandler;
    SQLiteDatabase db;
    static String refvalue;
    static String amname;
    static String soname;
    static String cityname;
    static String areahqname;
    static String sohqname;
    static String srhqname;
    static String regionId,strUserDetailsId;
    static String stateId;
    static String depotId;
    static String citytownId;
    static String areamanagerId;
    static String soId;
    static String SRId;
    static String countryRegion,countryState,countryDepot,countryCityTime,countryAMHQ,countrySOHQ,countrySRHQ;
    Button buttonSave, buttonEdit;
    EditText textVieweditSOName, spinRegion,textVieweditAreaMN, textViewAreaMN, textViewSOName,spinState, spincitytown, spinDepot, spinareamanagerHQ, spinsomanagerHq, spinSRmanagerHq;;

    Spinner spineditRegion;
    AutoCompleteTextView  spineditState, spineditcitytown, spineditDepot, spineditsomanagerHq, spineditSRmanagerHq,
            spineditareamanagerHQ;
    int j,k = 0;
    ArrayList<String> arlregion;
    ArrayList<String> arlstate;
    ArrayList<String> arldepot;
    ArrayList<String> arlcitytown;
    ArrayList<String> arlamhq;
    ArrayList<String> arlsohq;
    ArrayList<String> arlsrhq;
    HashMap<Integer,String> spinnerMap;
    HashMap<String,Object> AutocompletetextviewStateMap ;
    HashMap<String,Object> AutocompletetextviewDepotMap;
    HashMap<String,Object> AutocompletetextviewcitytownMap;
    HashMap<String,Object> AutocompletetextviewareamanagerHQMap;
    HashMap<String,Object> AutocompletetextviewSOHQMap;
    HashMap<String,Object> AutocompletetextviewSRHQMap;
    String strURLRegion="";
    String strURLState="";
    String strURLDepot="";
    String strURLCityTown="";
    String strURLAMHQ="";
    String strURLSOHQ="";
    String strURLSRHQ="";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.activity_salesman_data);
        databaseHandler = new DatabaseHandler(getApplicationContext());
        Cursor curs;
        curs = databaseHandler.getdealer();
        curs.moveToFirst();
        Constants.DUSER_ID = curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_Duserid));
        Log.e("Constants.DUSER_ID",Constants.DUSER_ID);
        strURLRegion=Utils.strCustomFieldURL+"&filter[where][refkey]=REGION";
        strURLState=Utils.strCustomFieldURL+"&filter[where][duserid]="+Constants.DUSER_ID+"&filter[where][refkey]=STATE&filter[where][basecustmfldid]=";
        strURLDepot=Utils.strCustomFieldURL+"&filter[where][duserid]="+Constants.DUSER_ID+"&filter[where][refkey]=DEPOT&filter[where][basecustmfldid]=";
        strURLCityTown=Utils.strCustomFieldURL+"&filter[where][duserid]="+Constants.DUSER_ID+"&filter[where][refkey]=CITYTOWN&filter[where][basecustmfldid]=";
        strURLAMHQ=Utils.strCustomFieldURL+"&filter[where][duserid]="+Constants.DUSER_ID+"&filter[where][refkey]=AMHQ&filter[where][basecustmfldid]=";
        strURLSOHQ=Utils.strCustomFieldURL+"&filter[where][duserid]="+Constants.DUSER_ID+"&filter[where][refkey]=SOHQ&filter[where][basecustmfldid]=";
        strURLSRHQ=Utils.strCustomFieldURL+"&filter[where][duserid]="+Constants.DUSER_ID+"&filter[where][refkey]=SRHQ&filter[where][basecustmfldid]=";
        linearLayoutCreateOrder = (LinearLayout) findViewById(R.id.linearLayoutCreateOrder);
        linearLayoutProfile = (LinearLayout) findViewById(R.id.linearLayoutProfile);
        linearLayoutMyOrders = (LinearLayout) findViewById(R.id.linearLayoutMyOrders);
        linearLayoutMyDealers = (LinearLayout) findViewById(R.id.linearLayoutMyDealers);
        linearLayoutProducts = (LinearLayout) findViewById(R.id.linearLayoutProducts);
        linearLayoutPayment = (LinearLayout) findViewById(R.id.linearLayoutPayment);
        linearLayoutComplaint = (LinearLayout) findViewById(R.id.linearLayoutComplaint);
        linearLayoutSignout = (LinearLayout) findViewById(R.id.linearLayoutSignout);
        linearLayoutExport = (LinearLayout) findViewById(R.id.linearLayoutExport);
        linearLayoutClientVisit = (LinearLayout) findViewById(R.id.linearLayoutClientVisit);
        linearLayoutPaymentCollection = (LinearLayout) findViewById(R.id.linearLayoutPaymentCollection);
        linearLayoutRefresh = (LinearLayout) findViewById(R.id.linearLayoutRefresh);
        linearLayoutDistanceCalculation = (LinearLayout) findViewById(R.id.linearLayoutDistanceCalculation);
        linearLayoutEdit = (LinearLayout) findViewById(R.id.linearLayoutEdit);
        linearLayoutDetails = (LinearLayout) findViewById(R.id.linearLayoutDetails);
        linearLayoutAcknowledge = (LinearLayout) findViewById(R.id.linearLayoutAcknowledge);
        linearLayoutStockEntry = (LinearLayout) findViewById(R.id.linearLayoutStockEntry);
        linearLayoutAddMerchant = (LinearLayout) findViewById(R.id.linearLayoutAddMerchant);
        linearLayoutdownloadscheme = (LinearLayout) findViewById(R.id.linearLayoutdownloadscheme);
        linearLayoutsurveyuser = (LinearLayout) findViewById(R.id.linearLayoutsurveyuser);
        linearLayoutMyDayPlan = (LinearLayout) findViewById(R.id.linearLayoutMyDayPlan);
        linearLayoutMySales = (LinearLayout) findViewById(R.id.linearLayoutMySales);
        textViewAssetMenuRefresh = (TextView) findViewById(R.id.textViewAssetMenuRefresh);
        //List Save field
        spinRegion = (EditText) findViewById(R.id.spinRegion);
        spinState = (EditText) findViewById(R.id.spinState);
        spincitytown = (EditText) findViewById(R.id.spincitytown);
        spinDepot= (EditText) findViewById(R.id.spinDepot);
        spinareamanagerHQ = (EditText) findViewById(R.id.spinareamanagerHQ);
        spinsomanagerHq = (EditText) findViewById(R.id.spinsomanagerHq);
        spinSRmanagerHq = (EditText) findViewById(R.id.spinSRmanagerHq);
        buttonSave = (Button) findViewById(R.id.buttonSave);
        textViewAreaMN = (EditText) findViewById(R.id.textViewAreaMN);
        textViewSOName = (EditText) findViewById(R.id.textViewSOName);
        //edit field
        spineditRegion = (Spinner) findViewById(R.id.spineditRegion);
        spineditState = (AutoCompleteTextView) findViewById(R.id.spineditState);
        spineditDepot = (AutoCompleteTextView) findViewById(R.id.spineditDepot);
        spineditcitytown= (AutoCompleteTextView) findViewById(R.id.spineditcitytown);
        spineditareamanagerHQ = (AutoCompleteTextView) findViewById(R.id.spineditareamanagerHQ);
        spineditsomanagerHq = (AutoCompleteTextView) findViewById(R.id.spineditsomanagerHq);
        spineditSRmanagerHq = (AutoCompleteTextView) findViewById(R.id.spineditSRmanagerHq);
        textVieweditAreaMN = (EditText) findViewById(R.id.textVieweditAreaMN);
        textVieweditSOName = (EditText) findViewById(R.id.textVieweditSOName);
        buttonEdit = (Button) findViewById(R.id.buttonEdit);

        if (Constants.USER_TYPE.equals("D")) {
            linearLayoutPaymentCollection.setVisibility(View.GONE);
            linearLayoutMyDealers.setVisibility(View.GONE);
            linearLayoutProducts.setVisibility(View.VISIBLE);
            linearLayoutExport.setVisibility(View.VISIBLE);
            linearLayoutCreateOrder.setVisibility(View.GONE);
            linearLayoutRefresh.setVisibility(View.GONE);
            linearLayoutAcknowledge.setVisibility(View.GONE);
            linearLayoutAddMerchant.setVisibility(View.GONE);
            linearLayoutStockEntry.setVisibility(View.GONE);
            linearLayoutdownloadscheme.setVisibility(View.GONE);
            linearLayoutsurveyuser.setVisibility(View.GONE);
            linearLayoutDistanceCalculation.setVisibility(View.GONE);
            linearLayoutMySales.setVisibility(View.GONE);
        } else if (Constants.USER_TYPE.equals("M")) {
            linearLayoutPaymentCollection.setVisibility(View.GONE);
            linearLayoutClientVisit.setVisibility(View.GONE);
            linearLayoutMyDealers.setVisibility(View.VISIBLE);
            linearLayoutProducts.setVisibility(View.GONE);
            linearLayoutExport.setVisibility(View.GONE);
            linearLayoutCreateOrder.setVisibility(View.GONE);
            linearLayoutRefresh.setVisibility(View.GONE);
            linearLayoutAcknowledge.setVisibility(View.GONE);
            linearLayoutAddMerchant.setVisibility(View.GONE);
            linearLayoutStockEntry.setVisibility(View.GONE);
            linearLayoutDistanceCalculation.setVisibility(View.GONE);
            linearLayoutdownloadscheme.setVisibility(View.GONE);
            linearLayoutsurveyuser.setVisibility(View.GONE);
            linearLayoutMySales.setVisibility(View.GONE);
        } else {
            linearLayoutMyDealers.setVisibility(View.VISIBLE);
            linearLayoutProducts.setVisibility(View.GONE);
            linearLayoutExport.setVisibility(View.GONE);
            linearLayoutCreateOrder.setVisibility(View.VISIBLE);
            linearLayoutPayment.setVisibility(View.GONE);
            linearLayoutRefresh.setVisibility(View.VISIBLE);
            linearLayoutDistanceCalculation.setVisibility(View.VISIBLE);
            linearLayoutMyDealers.setVisibility(View.GONE);
            linearLayoutMyDayPlan.setVisibility(View.VISIBLE);

        }
        menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
        textViewAssetMenuCreateOrder = (TextView) findViewById(R.id.textViewAssetMenuCreateOrder);
        textViewAssetMenuProfile = (TextView) findViewById(R.id.textViewAssetMenuProfile);
        textViewAssetMenuMyOrders = (TextView) findViewById(R.id.textViewAssetMenuMyOrders);
        textViewAssetMenuMyDealers = (TextView) findViewById(R.id.textViewAssetMenuMyDealers);
        textViewAssetMenuProducts = (TextView) findViewById(R.id.textViewAssetMenuProducts);
        textViewAssetMenuPayment = (TextView) findViewById(R.id.textViewAssetMenuPayment);
        textViewAssetMenuComplaint = (TextView) findViewById(R.id.textViewAssetMenuComplaint);
        textViewAssetMenuSignout = (TextView) findViewById(R.id.textViewAssetMenuSignout);
        textViewAssetMenuDistanceCalculation = (TextView) findViewById(R.id.textViewAssetMenuDistanceCalculation);
        textViewAssetMenuClientVisit = (TextView) findViewById(R.id.textViewAssetMenuClientVisit);
        textViewAssetMenuPaymentCollection = (TextView) findViewById(R.id.textViewAssetMenuPaymentCollection);
        textViewClient = (TextView) findViewById(R.id.textViewClient);
        textViewAssetMenuAddMerchant = (TextView) findViewById(R.id.textViewAssetMenuAddMerchant);
        textViewAssetMenuAcknowledge = (TextView) findViewById(R.id.textViewAssetMenuAcknowledge);
        textViewAssetMenuStockEntry = (TextView) findViewById(R.id.textViewAssetMenuStockEntry);
        textViewAssetMenudownloadscheme = (TextView) findViewById(R.id.textViewAssetMenudownloadscheme);
        textViewAssetMenusurveyuser = (TextView) findViewById(R.id.textViewAssetMenusurveyuser);
        textViewAssetMenuMyDayPlan = (TextView) findViewById(R.id.textViewAssetMenuMyDayPlan);
        textViewAssetMenuMySales = (TextView) findViewById(R.id.textViewAssetMenuMySales);
        linearLayoutMenuParent = (LinearLayout) findViewById(R.id.linearLayoutMenuParent);

        Typeface font = Typeface.createFromAsset(getAssets(),
                "fontawesome-webfont.ttf");
        menuIcon.setTypeface(font);
        textViewAssetMenuCreateOrder.setTypeface(font);
        textViewAssetMenuProfile.setTypeface(font);
        textViewAssetMenuMyOrders.setTypeface(font);
        textViewAssetMenuMyDealers.setTypeface(font);
        textViewAssetMenuProducts.setTypeface(font);
        textViewAssetMenuPayment.setTypeface(font);
        textViewAssetMenuComplaint.setTypeface(font);
        textViewAssetMenuSignout.setTypeface(font);
        textViewAssetMenuDistanceCalculation.setTypeface(font);
        textViewAssetMenuClientVisit.setTypeface(font);
        textViewAssetMenuPaymentCollection.setTypeface(font);
        textViewAssetMenuRefresh.setTypeface(font);

        textViewAssetMenuAcknowledge.setTypeface(font);
        textViewAssetMenuStockEntry.setTypeface(font);
        textViewAssetMenuAddMerchant.setTypeface(font);

        textViewAssetMenusurveyuser.setTypeface(font);
        textViewAssetMenudownloadscheme.setTypeface(font);
        textViewAssetMenuMyDayPlan.setTypeface(font);
        textViewAssetMenuMySales.setTypeface(font);

        linearLayoutDistStock = (LinearLayout) findViewById(R.id.linearLayoutDistStock);
        textViewAssetMenuDistStock = (TextView) findViewById(R.id.textViewAssetMenuDistStock);
        textViewAssetMenuDistStock.setTypeface(font);

        arlregion = new ArrayList<>();
        arlstate = new ArrayList<>();
        arldepot = new ArrayList<>();
        arlcitytown = new ArrayList<>();
        arlamhq = new ArrayList<>();
        arlsohq = new ArrayList<>();
        arlsrhq = new ArrayList<>();
        int permissionCheck1 = ContextCompat.checkSelfPermission(AddSalesmanDataActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);

        if (permissionCheck1 != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    AddSalesmanDataActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.LOCATION_PERMISSION_REQUEST_CODE);
        }

        menuIcon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (menuCliccked == 0) {
                    linearLayoutMenuParent.setVisibility(View.VISIBLE);
                    menuCliccked = 1;
                } else {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                }

            }
        });


        if (netCheck() == true) {
            getdata();
            getDetails();
        } else {
            buttonEdit.setVisibility(Button.GONE);
        }

        if (Constants.USER_TYPE.equals("D")) {
            textViewClient.setText("Client Visit");
        } else {
            textViewClient.setText("My Client Visit");
        }

        buttonEdit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutEdit.setVisibility(View.VISIBLE);
                linearLayoutDetails.setVisibility(View.GONE);
                spineditRegion.setSelection(getSelectedRegionIndex(""+spinRegion.getText(),arlregion));
                if(spinRegion.getText()==null || (""+spinRegion.getText()).equalsIgnoreCase(""))
                {
                    spineditRegion.setSelection(0);
                }


            }
        });

        linearLayoutMenuParent.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
            }
        });


        linearLayoutProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Intent io = new Intent(AddSalesmanDataActivity.this, ProfileActivity.class);
                startActivity(io);
                finish();
            }
        });


        linearLayoutRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("Clicked", "Clicked");

                if (netCheck()) {

                    final Dialog qtyDialog = new Dialog(AddSalesmanDataActivity.this);
                    qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    qtyDialog.setContentView(R.layout.warning_dialog);

                    final Button buttonUpdateOk, buttonUpdateCancel;

                    final TextView textQtyValidate;

                    buttonUpdateOk = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateOk);
                    buttonUpdateCancel = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateCancel);

                    textQtyValidate = (TextView) qtyDialog
                            .findViewById(R.id.textQtyValidate);
                    qtyDialog.show();


                    buttonUpdateOk.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            databaseHandler.synTableDelete();
                            Constants.refreshscreen = "addmerchant";
                            Intent io = new Intent(AddSalesmanDataActivity.this, RefreshActivity.class);
                            startActivity(io);
                            finish();
                            qtyDialog.dismiss();
                        }

                    });

                    buttonUpdateCancel
                            .setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub

                                    qtyDialog.dismiss();

                                }
                            });

                }
            }

        });


        linearLayoutMyOrders.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);

                if (netCheck() == true) {

                    getDetail("myorder");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(AddSalesmanDataActivity.this,
                                MyDealersActivity.class);
                        io.putExtra("Key", "menuclick");
                        startActivity(io);
                        finish();

                    }
                }


            }
        });

        linearLayoutMyDealers.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheck() == true) {

                    getDetail("Mydealer");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(AddSalesmanDataActivity.this,
                                MyDealersActivity.class);
                        io.putExtra("Key", "menuclick");
                        startActivity(io);
                        finish();

                    }
                }


            }
        });


        linearLayoutCreateOrder.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Constants.checkproduct = 0;
                Constants.orderid = "";
                Constants.SalesMerchant_Id = "";
                Constants.Merchantname = "";
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;

                if (netCheckwithoutAlert()) {
                    getDetail("createorder");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(AddSalesmanDataActivity.this,
                                CreateOrderActivity.class);
                        startActivity(io);
                        finish();
                    }
                }

            }
        });

        linearLayoutPayment.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;


            }
        });

        linearLayoutComplaint.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheckwithoutAlert() == true) {
                    getDetail("postnotes");
                } else {

                    showAlertDialogToast("Please Check Your internet connection");
                }
            }
        });

        linearLayoutClientVisit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheck() == true) {
                    getDetail("clientvisit");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(AddSalesmanDataActivity.this,
                                SalesManOrderActivity.class);
                        startActivity(io);
                        finish();
                    }
                }


            }
        });
        linearLayoutPaymentCollection.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheckwithoutAlert() == true) {
                    getDetail("paymentcoll");
                } else {

                    showAlertDialogToast("Please Check Your internet connection");
                }
            }
        });
        linearLayoutStockEntry.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheckwithoutAlert() == true) {
                    getDetail("stockentry");
                } else {

                    showAlertDialogToast("Please Check Your internet connection");
                }
            }
        });
        linearLayoutDistStock.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                //if (netCheckwithoutAlert() == true) {
                    getDetail("diststock");
                //} else {

                //    showAlertDialogToast("Please Check Your internet connection");
                //}
            }
        });
        linearLayoutAcknowledge.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheckwithoutAlert() == true) {
                    getDetail("acknowledge");
                } else {

                    showAlertDialogToast("Please Check Your internet connection");
                }
            }
        });
        linearLayoutAddMerchant.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheckwithoutAlert() == true) {
                    Intent io = new Intent(AddSalesmanDataActivity.this,
                            AddMerchantNew.class);
                    startActivity(io);
                    finish();

                } else {

                    showAlertDialogToast("Please Check Your internet connection");
                }
            }
        });

        linearLayoutdownloadscheme.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck()) {

                    final Dialog qtyDialog = new Dialog(AddSalesmanDataActivity.this);
                    qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    qtyDialog.setContentView(R.layout.warning_dialog);

                    final Button buttonUpdateOk, buttonUpdateCancel;

                    final TextView textQtyValidate;

                    buttonUpdateOk = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateOk);
                    buttonUpdateCancel = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateCancel);

                    textQtyValidate = (TextView) qtyDialog
                            .findViewById(R.id.textQtyValidate);
                    qtyDialog.show();


                    buttonUpdateOk.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            databaseHandler.deletescheme();
                            Constants.downloadScheme = "addmerchant";
                            Intent io = new Intent(AddSalesmanDataActivity.this, SchemeDownload.class);
                            startActivity(io);
                            finish();
                            qtyDialog.dismiss();
                        }

                    });

                    buttonUpdateCancel
                            .setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub

                                    qtyDialog.dismiss();

                                }
                            });

                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }
            }
        });
        linearLayoutsurveyuser.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //if (netCheck() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    getDetail("surveyuser");
                //} else {
                //    showAlertDialogToast("Please Check Your internet connection");
                //}

            }
        });
        linearLayoutDistanceCalculation.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Intent to = new Intent(AddSalesmanDataActivity.this,
                            DistanceCalculationActivity.class);
                    startActivity(to);

            }
        });

        linearLayoutMyDayPlan.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheckwithoutAlert() == true) {
                    getDetail("mydayplan");
                } else {

                    showAlertDialogToast("Please Check Your internet connection");
                }
            }
        });

        linearLayoutMySales.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheckwithoutAlert() == true) {
                    getDetail("mysales");
                } else {

                    showAlertDialogToast("Please Check Your internet connection");
                }
            }
        });

        linearLayoutSignout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                // TODO Auto-generated method stub

                Intent io = new Intent(AddSalesmanDataActivity.this, SigninActivity.class);
                startActivity(io);
                finish();
            }
        });
        buttonSave.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                amname = textVieweditAreaMN.getText()
                        .toString();
                soname = textVieweditSOName.getText()
                        .toString();
                cityname = spineditcitytown.getText()
                        .toString();
                Log.e("citynamebuttonSave",cityname);
                areahqname = spineditareamanagerHQ.getText()
                        .toString();
                Log.e("areahqnamebuttonSave ",areahqname);
                sohqname = spineditsomanagerHq.getText()
                        .toString();
                Log.e("sohqnamebuttonSave",sohqname);
                srhqname = spineditSRmanagerHq.getText()
                        .toString();
                Log.e("srhqnamebuttonSave",srhqname);
                refvalue = spineditRegion.getSelectedItem()
                        .toString();
                if (!amname.isEmpty() && !amname.startsWith(" ")) {

                    if (!soname.isEmpty() && !soname.startsWith(" ")) {

                        senddata();
                    } else {
                        showAlertDialogToast("Please Enter the soname name");
                        //toastDisplay("Please Enter the company name");
                    }
                } else {
                    showAlertDialogToast("Please Enter the amname");
                    //toastDisplay("Please Enter the fullname");
                }

            }


        });



        spineditRegion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                String country=spineditRegion.getItemAtPosition(spineditRegion.getSelectedItemPosition()).toString();
                Log.e("country TestNewActivity",country);
                regionId = spinnerMap.get(spineditRegion.getSelectedItemPosition());
                Log.e("ID for Region--",regionId);
                loadStateData(strURLState+regionId);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // DO Nothing here
                // Log.e("country TestNewActivity",country);
            }
        });
        spineditState.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                try {
                    Object selectedItem = spineditState.getAdapter().getItem(position);
                    JSONObject json =(JSONObject) AutocompletetextviewStateMap.get(selectedItem);
                    stateId = "" + json.get("custmfldid");
                    loadDepotData(strURLDepot+stateId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        spineditDepot.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                try {
                    Object selectedItem = spineditDepot.getAdapter().getItem(position);
                    JSONObject json =(JSONObject) AutocompletetextviewDepotMap.get(selectedItem);
                    depotId = "" + json.get("custmfldid");
                    loadTownCityData(strURLCityTown+depotId);
                    Log.e("depotId----",depotId);
                    Log.e("ID for depot--", selectedItem.toString());
                    loadAreaManagerHQData(strURLAMHQ+depotId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        spineditcitytown.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                try {
                    Object selectedItem = spineditcitytown.getAdapter().getItem(position);
                    JSONObject json =(JSONObject) AutocompletetextviewcitytownMap.get(selectedItem);
                    citytownId = "" + json.get("custmfldid");
                    //loadAreaManagerHQData(strURLAMHQ+citytownId);
                    Log.e("citytownId----",citytownId);
                    Log.e("ID for citytownId--", selectedItem.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        spineditareamanagerHQ.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                try {
                    Object selectedItem = spineditareamanagerHQ.getAdapter().getItem(position);
                    JSONObject json =(JSONObject) AutocompletetextviewareamanagerHQMap.get(selectedItem);
                    areamanagerId = "" + json.get("custmfldid");
                    loadSoManagerHQData(strURLSOHQ+areamanagerId);
                    Log.e("areamanagerId----",areamanagerId);
                    Log.e("ID for areamanagerId--", selectedItem.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        spineditsomanagerHq.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                try {
                    Object selectedItem = spineditsomanagerHq.getAdapter().getItem(position);
                    JSONObject json =(JSONObject) AutocompletetextviewSOHQMap.get(selectedItem);
                    soId = "" + json.get("custmfldid");
                    SRManagerHQData(strURLSRHQ+soId);
                    Log.e("soId----",soId);
                    Log.e("ID for soId--", selectedItem.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        spineditSRmanagerHq.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                try {
                    Object selectedItem = spineditSRmanagerHq.getAdapter().getItem(position);
                    JSONObject json =(JSONObject) AutocompletetextviewSRHQMap.get(selectedItem);
                    SRId = "" + json.get("custmfldid");
                    // loadDepotData(strURLSOHQ+soId);
                    Log.e("SRId----",SRId);
                    Log.e("ID for SRId--", selectedItem.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    public int getSelectedRegionIndex(String strItem, ArrayList<String> arl)
    {   int intRegionIndex=3;
        for(int i=0;i<arl.size();i++)
        {
           if( arl.get(i).equalsIgnoreCase(strItem))
           {
               intRegionIndex= i;
               break;
           }
        }
        return intRegionIndex;
    }
    private void getdata() {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "";

            @Override
            protected void onPreExecute() {
                dialog = ProgressDialog.show(AddSalesmanDataActivity.this, "",
                        "Loading...", true, true);

            }

            @Override
            protected void onPostExecute(Void result) {

                try {

                    strStatus = JsonAccountObject.getString("status");
                    if(strStatus.equals("true")){
                        JSONArray job1 = JsonAccountObject.getJSONArray("data");
                        for (int i = 0; i < job1.length(); i++) {
                            JSONObject job = new JSONObject();
                            job = job1.getJSONObject(i);
                            if(job.getJSONObject("userdetails") != null) {
                                JSONObject jsonArr = job.getJSONObject("userdetails");
                                Log.e("jsonArr  Data", String.valueOf(jsonArr));
                                strUserDetailsId = jsonArr.getString("userdetailsid");
                                textViewSOName.setText(jsonArr.getString("soname"));
                                textVieweditSOName.setText(jsonArr.getString("soname"));
                                textVieweditAreaMN.setText(jsonArr.getString("amname"));
                                textViewAreaMN.setText(jsonArr.getString("amname"));
                                JSONObject jsonArr1 = jsonArr.getJSONObject("region");
                                regionId = jsonArr1.getString("custmfldid");
                                Log.e("regionId  Data", String.valueOf(regionId));
                                spinRegion.setText(jsonArr1.getString("refvalue"));
                                int intInex = getSelectedRegionIndex(jsonArr1.getString("refvalue"), arlregion);
                                spineditRegion.setSelection(intInex);
                                JSONObject jsonArr2 = jsonArr.getJSONObject("state");
                                stateId = jsonArr1.getString("custmfldid");
                                Log.e("stateId  Data", String.valueOf(stateId));
                                spinState.setText(jsonArr2.getString("refvalue"));
                                spineditState.setText(jsonArr2.getString("refvalue"));
                                JSONObject jsonArr3 = jsonArr.getJSONObject("city");
                              //  citytownId = jsonArr3.getString("custmfldid");
                               // Log.e("citytownId  Data", String.valueOf(citytownId));
                                spincitytown.setText(jsonArr3.getString("refvalue"));
                                spineditcitytown.setText(jsonArr3.getString("refvalue"));
                                JSONObject jsonArr4 = jsonArr.getJSONObject("depot");
                               // depotId = jsonArr4.getString("custmfldid");
                               // Log.e("depotId  Data", String.valueOf(depotId));
                                spinDepot.setText(jsonArr4.getString("refvalue"));
                                spineditDepot.setText(jsonArr4.getString("refvalue"));
                                JSONObject jsonArr5 = jsonArr.getJSONObject("areahq");
                               // areamanagerId = jsonArr5.getString("custmfldid");
                               // Log.e("areamanagerId  Data", String.valueOf(areamanagerId));
                                spinareamanagerHQ.setText(jsonArr5.getString("refvalue"));
                                spineditareamanagerHQ.setText(jsonArr5.getString("refvalue"));
                                JSONObject jsonArr6 = jsonArr.getJSONObject("sohq");
                               // soId = jsonArr6.getString("custmfldid");
                               // Log.e("soId  Data", String.valueOf(soId));
                                spinsomanagerHq.setText(jsonArr6.getString("refvalue"));
                                spineditsomanagerHq.setText(jsonArr6.getString("refvalue"));
                                JSONObject jsonArr7 = jsonArr.getJSONObject("srhq");
                               // SRId = jsonArr7.getString("custmfldid");
                               // Log.e("SRId  Data", String.valueOf(SRId));
                                spinSRmanagerHq.setText(jsonArr7.getString("refvalue"));
                                spineditSRmanagerHq.setText(jsonArr7.getString("refvalue"));
                            }

                        }
                    }else{

                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                }
                dialog.dismiss();
            }


            @Override
            protected Void doInBackground(Void... params) {



                JsonServiceHandler = new JsonServiceHandler(Utils.strSalesData + Constants.USER_ID, AddSalesmanDataActivity.this);
                JsonAccountObject = JsonServiceHandler.ServiceDataGet();
                return null;
            }
        }.execute(new Void[]{});
    }

    private void senddata() {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "",strOrderId;

            @Override
            protected void onPreExecute() {
                dialog= ProgressDialog.show(AddSalesmanDataActivity.this, "",
                        "Loading...", true, true);
            }


            @Override
            protected void onPostExecute(Void result) {

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);
                    if(strStatus.equals("true")){
                        showAlertDialogToast(strMsg);
                        getdata();
                        getDetails();
                        linearLayoutEdit.setVisibility(View.GONE);
                        linearLayoutDetails.setVisibility(View.VISIBLE);
                    }else{

                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject=new JSONObject();
                Log.e("jsonObject:", jsonObject.toString());

                try {
                    jsonObject.put("duserid",Constants.DUSER_ID);
                    jsonObject.put("userid",Constants.USER_ID);
                    jsonObject.put("usertype",Constants.USER_TYPE);
                    JSONObject jsonSalemanAdlData=new JSONObject();
                    jsonSalemanAdlData.put("userdetailsid",strUserDetailsId);
                    jsonSalemanAdlData.put("userid",Constants.USER_ID);
                    jsonSalemanAdlData.put("regionid",regionId);
                 //  stateId= ((JSONObject)AutocompletetextviewStateMap.get(""+spineditState.getText())).getString("custmfldid");
                    jsonSalemanAdlData.put("stateid",stateId);
                    jsonSalemanAdlData.put("depotid",depotId);
                    jsonSalemanAdlData.put("cityname",cityname);
                    jsonSalemanAdlData.put("areahqname",areahqname);
                    jsonSalemanAdlData.put("amname",amname);
                    jsonSalemanAdlData.put("sohqname",sohqname);
                    jsonSalemanAdlData.put("soname",soname);
                    jsonSalemanAdlData.put("srhqname",srhqname);
                    jsonObject.put("userdetails",jsonSalemanAdlData);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.e("UD OBJ:", jsonObject.toString());
                JsonServiceHandler = new JsonServiceHandler(Utils.strSalesmanSaveURL+Constants.USER_ID, jsonObject.toString(),AddSalesmanDataActivity.this);
                JsonAccountObject = JsonServiceHandler.ServiceData();
                Log.e("JsonAccountObject SAVE", String.valueOf(JsonAccountObject));
                return null;
            }
        }.execute(new Void[]{});
    }

    private void getDetail(final String moveto) {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "";

            @Override
            protected void onPreExecute() {
                dialog= ProgressDialog.show(AddSalesmanDataActivity.this, "",
                        "Loading...", true, true);

            }

            @Override
            protected void onPostExecute(Void result) {

                try {

                    PaymentStatus = (JsonAccountObject.getString("pymtstatus"));
                    Log.e("PaymentStatus", PaymentStatus);

                    if(PaymentStatus.equals("Pending")||PaymentStatus.equals(null) ||PaymentStatus.equals("")||PaymentStatus.equals("null")){
                        showAlertDialogToast("Please make payment to place order");
                    }else {
                        if (moveto.equals("product")) {

                            Intent to = new Intent(AddSalesmanDataActivity.this,
                                    ProductActivity.class);
                            startActivity(to);
                            finish();

                        } else if (moveto.equals("export")) {


                            Intent to = new Intent(AddSalesmanDataActivity.this,
                                    ExportActivity.class);

                            startActivity(to);
                            finish();

                        } else if (moveto.equals("myorder")) {

                            if (Constants.USER_TYPE.equals("D")) {
                                Intent io = new Intent(AddSalesmanDataActivity.this,
                                        DealersOrderActivity.class);
                                startActivity(io);
                                finish();

                            } else if (Constants.USER_TYPE.equals("M")){
                                Intent io = new Intent(AddSalesmanDataActivity.this,
                                        MerchantOrderActivity.class);
                                startActivity(io);
                                finish();

                            }else {
                                Intent io = new Intent(AddSalesmanDataActivity.this,
                                        SalesManOrderActivity.class);
                                startActivity(io);
                                finish();

                            }

                        }else if(moveto.equals("createorder")){
                            Intent io = new Intent(AddSalesmanDataActivity.this,
                                    CreateOrderActivity.class);
                            startActivity(io);
                            finish();

                        }else if(moveto.equals("Mydealer")){
                            Intent io = new Intent(AddSalesmanDataActivity.this,
                                    MyDealersActivity.class);
                            io.putExtra("Key","MenuClick");
                            startActivity(io);
                            finish();

                        }else if(moveto.equals("postnotes")){
                            if (Constants.USER_TYPE.equals("D")) {
                                Intent io = new Intent(AddSalesmanDataActivity.this,
                                        DealersComplaintActivity.class);
                                startActivity(io);
                                finish();

                            } else {
                                Intent io = new Intent(AddSalesmanDataActivity.this,
                                        MerchantComplaintActivity.class);
                                startActivity(io);
                                finish();

                            }

                        }

                        else if(moveto.equals("clientvisit")){


                            Intent io = new Intent(AddSalesmanDataActivity.this,
                                    SMClientVisitHistory.class);
                            startActivity(io);
                            finish();

                        }

                        else if(moveto.equals("paymentcoll")){

                            Intent io = new Intent(AddSalesmanDataActivity.this,
                                    SalesmanPaymentCollectionActivity.class);
                            startActivity(io);
                            finish();

                        }
                        else if(moveto.equals("stockentry")){

                            Intent io = new Intent(AddSalesmanDataActivity.this,
                                    OutletStockEntry.class);
                            startActivity(io);
                            finish();

                        }
                        else if(moveto.equals("acknowledge")){

                            Intent io = new Intent(AddSalesmanDataActivity.this,
                                    SalesmanAcknowledgeActivity.class);
                            startActivity(io);
                            finish();

                        }
                        else if(moveto.equals("surveyuser")){

                            Intent io = new Intent(AddSalesmanDataActivity.this,
                                    SurveyMerchant.class);
                            startActivity(io);
                            finish();

                        }
                        else if(moveto.equals("mydayplan")){

                            Intent io = new Intent(AddSalesmanDataActivity.this,
                                    MyDayPlan.class);
                            startActivity(io);
                            finish();

                        }
                        else if(moveto.equals("mysales")){

                            Intent io = new Intent(AddSalesmanDataActivity.this,
                                    MySalesActivity.class);
                            startActivity(io);
                            finish();

                        } else if (moveto.equals("diststock")) {
                            Intent io = new Intent(AddSalesmanDataActivity.this,
                                    DistributrStockActivity.class);
                            startActivity(io);
                            finish();
                        }

                    }

                    dialog.dismiss();

                }catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();


                JsonServiceHandler = new JsonServiceHandler(Utils.strGetProfileDetail+Constants.USER_ID, AddSalesmanDataActivity.this);
                JsonAccountObject = JsonServiceHandler.ServiceDataGet();
                return null;
            }
        }.execute(new Void[]{});
    }
    protected void toastDisplay(String msg) {
        Toast toast = Toast.makeText(AddSalesmanDataActivity.this, msg,
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();

    }
    public boolean netCheck() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            }

            else if (result == null) {
                showAlertDialog(AddSalesmanDataActivity.this,
                        "No Internet Connection",
                        "Please Check Your internet connection.", false);
                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public boolean netCheckwithoutAlert() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            }

            else if (result == null) {

                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
    public void showAlertDialog(Context context, String title, String message,
                                Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        alertDialog.show();
    }
    public void showAlertDialogToast( String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();

        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }
    public void showAlertDialogToast1( String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setCancelable(false);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent io=new Intent(AddSalesmanDataActivity.this,SurveyMerchant.class);
                        startActivity(io);
                        finish();
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();

        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    public void onBackPressed() {
        exitAlret();
    }

    private void exitAlret() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        localBuilder.setMessage("Do you want to Exit?");
        localBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {

                        finish();

                    }
                });
        localBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {
                        paramDialogInterface.cancel();
                    }
                });
        localBuilder.create().show();
    }
    @SuppressLint("LongLogTag")
    public void getDetails(){
        loadRegionData(strURLRegion);
        loadStateData(strURLState+regionId);
        Log.e(" loadStateData(strURLState+regionId)",(strURLState+regionId));
        loadDepotData(strURLDepot+stateId);
        loadTownCityData(strURLCityTown+depotId);
        loadAreaManagerHQData(strURLAMHQ+depotId);
        loadSoManagerHQData(strURLSOHQ+areamanagerId);
        SRManagerHQData(strURLSRHQ+soId);
    }

    private void loadRegionData(String url) {
        arlregion.clear();
        RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject jsonObject= null;
                    try {
                        jsonObject = new JSONObject(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    spinnerMap = new HashMap<Integer, String>();
                    if(jsonObject.getString("status").equalsIgnoreCase("true")){
                        JSONArray jsonArray=jsonObject.getJSONArray("data");
                        arlregion.add("Select Region");
                        spinnerMap.put(0,"");
                        for(int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            countryRegion=jsonObject1.getString("refvalue");
                            spinnerMap.put(i+1,""+jsonObject1.getInt("custmfldid"));
                            arlregion.add(countryRegion);
                        }
                    }
                    spineditRegion.setPrompt(



                            "Selecct Region");
                    spineditRegion.setTag("Selecct Region");
                    spineditRegion.setAdapter(new ArrayAdapter<String>(AddSalesmanDataActivity.this, android.R.layout.simple_dropdown_item_1line, arlregion));

                }catch (JSONException e){e.printStackTrace();}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }
    private void loadStateData(String url) {
        arlstate.clear();
        RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject jsonObject= null;
                    try {
                        jsonObject = new JSONObject(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    AutocompletetextviewStateMap = new HashMap<String, Object>();

                    if(jsonObject.getString("status").equalsIgnoreCase("true")){
                        JSONArray jsonArray=jsonObject.getJSONArray("data");
                        for(int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            countryState=jsonObject1.getString("refvalue");
                            AutocompletetextviewStateMap.put(countryState, jsonObject1);

                            arlstate.add(countryState);
                        }
                    }
                    spineditState.setAdapter(new ArrayAdapter<String>(AddSalesmanDataActivity.this, android.R.layout.select_dialog_item, arlstate));
                    spineditState.setThreshold(1);
                }catch (JSONException e){e.printStackTrace();}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void loadDepotData(String url) {
        arldepot.clear();
        RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject jsonObject= null;
                    try {
                        jsonObject = new JSONObject(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    AutocompletetextviewDepotMap = new HashMap<String, Object>();

                    if(jsonObject.getString("status").equalsIgnoreCase("true")){
                        JSONArray jsonArray=jsonObject.getJSONArray("data");
                        for(int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            countryDepot=jsonObject1.getString("refvalue");
                            AutocompletetextviewDepotMap.put(countryDepot, jsonObject1);

                            arldepot.add(countryDepot);
                        }
                    }
                    spineditDepot.setAdapter(new ArrayAdapter<String>(AddSalesmanDataActivity.this, android.R.layout.select_dialog_item, arldepot));
                    spineditDepot.setThreshold(1);
                }catch (JSONException e){e.printStackTrace();}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }
    private void loadTownCityData(String url) {
        arlcitytown.clear();
        RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject jsonObject= null;
                    try {
                        jsonObject = new JSONObject(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    AutocompletetextviewcitytownMap = new HashMap<String, Object>();

                    if(jsonObject.getString("status").equalsIgnoreCase("true")){
                        JSONArray jsonArray=jsonObject.getJSONArray("data");
                        for(int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            countryCityTime=jsonObject1.getString("refvalue");
                            AutocompletetextviewcitytownMap.put(countryCityTime, jsonObject1);

                            arlcitytown.add(countryCityTime);
                        }
                    }
                    spineditcitytown.setAdapter(new ArrayAdapter<String>(AddSalesmanDataActivity.this, android.R.layout.select_dialog_item, arlcitytown));
                    spineditcitytown.setThreshold(1);
                }catch (JSONException e){e.printStackTrace();}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }

    private void loadAreaManagerHQData(String url) {
        arlamhq.clear();
        RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject jsonObject= null;
                    try {
                        jsonObject = new JSONObject(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    AutocompletetextviewareamanagerHQMap = new HashMap<String, Object>();
                    if(jsonObject.getString("status").equalsIgnoreCase("true")){
                        JSONArray jsonArray=jsonObject.getJSONArray("data");
                        for(int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            countryAMHQ=jsonObject1.getString("refvalue");
                            AutocompletetextviewareamanagerHQMap.put(countryAMHQ, jsonObject1);
                            arlamhq.add(countryAMHQ);
                        }
                    }
                    spineditareamanagerHQ.setAdapter(new ArrayAdapter<String>(AddSalesmanDataActivity.this, android.R.layout.select_dialog_item, arlamhq));
                    spineditareamanagerHQ.setThreshold(1);
                }catch (JSONException e){e.printStackTrace();}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }
    private void loadSoManagerHQData(String url) {
        arlsohq.clear();
        RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject jsonObject= null;
                    try {
                        jsonObject = new JSONObject(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    AutocompletetextviewSOHQMap = new HashMap<String, Object>();
                    if(jsonObject.getString("status").equalsIgnoreCase("true")){
                        JSONArray jsonArray=jsonObject.getJSONArray("data");
                        for(int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            countrySOHQ=jsonObject1.getString("refvalue");
                            AutocompletetextviewSOHQMap.put(countrySOHQ, jsonObject1);
                            arlsohq.add(countrySOHQ);
                        }
                    }
                    spineditsomanagerHq.setAdapter(new ArrayAdapter<String>(AddSalesmanDataActivity.this, android.R.layout.select_dialog_item, arlsohq));
                    spineditsomanagerHq.setThreshold(1);
                }catch (JSONException e){e.printStackTrace();}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }
    private void SRManagerHQData(String url) {
        arlsrhq.clear();
        RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
        StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject jsonObject= null;
                    try {
                        jsonObject = new JSONObject(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    AutocompletetextviewSRHQMap = new HashMap<String, Object>();
                    if(jsonObject.getString("status").equalsIgnoreCase("true")){
                        JSONArray jsonArray=jsonObject.getJSONArray("data");
                        for(int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject1=jsonArray.getJSONObject(i);
                            countrySRHQ=jsonObject1.getString("refvalue");
                            AutocompletetextviewSRHQMap.put(countrySRHQ, jsonObject1);
                            arlsrhq.add(countrySRHQ);
                        }
                    }
                    spineditSRmanagerHq.setAdapter(new ArrayAdapter<String>(AddSalesmanDataActivity.this, android.R.layout.select_dialog_item, arlsrhq));
                    spineditSRmanagerHq.setThreshold(1);
                }catch (JSONException e){e.printStackTrace();}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
        int socketTimeout = 30000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        stringRequest.setRetryPolicy(policy);
        requestQueue.add(stringRequest);
    }
}

