package com.freshorders.freshorder.ui;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.MerchantDropDownAdapter;
import com.freshorders.freshorder.adapter.DistributrStockDetailAdapter;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.MDealerCompDropdownDomain;
import com.freshorders.freshorder.domain.MerchantDropdownDomain;
import com.freshorders.freshorder.domain.MerchantOrderDetailDomain;
import com.freshorders.freshorder.model.DistributorStockListModel;
import com.freshorders.freshorder.model.DistributorStockModel;
import com.freshorders.freshorder.toonline.migration.PendingDataMigrationActivity;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.GPSTracker;
import com.freshorders.freshorder.utils.GPSTrackerActivity;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class DistributrStockActivity extends Activity {

    private static String TAG = DistributrStockActivity.class.getSimpleName();

    TextView menuIcon, textViewAssetMenuProfile, textViewAssetMenuMyOrders,
            textViewAssetMenuMyDealers, textViewAssetMenuProducts,
            textViewAssetMenuPayment, textViewAssetMenuComplaint,
            textViewAssetMenuSignout, textViewAssetMenuClientVisit, textViewAssetMenuPaymentCollection, textView2,
            textViewNodata, textViewAssetMenuCreateOrder, textViewcross_1, textViewAssetMenuRefresh, textViewAssetMenuStockEntry, textViewAssetMenuAddMerchant, textViewAssetMenuAcknowledge, textViewAssetMenusurveyuser, textViewAssetMenudownloadscheme,
            textViewAssetMenuMyDayPlan, textViewAssetMenuMySales, textViewAssetMenuStockOnly, textViewAssetMenuMigration, textViewAssetMenuStockAudit;

    ListView listViewProducts;

    public static int menuCliccked;

    LinearLayout linearLayoutMenuParent, linearLayoutProfile,
            linearLayoutMyOrders, linearLayoutMyDealers, linearLayoutProducts,
            linearLayoutPayment, linearLayoutPaymentCollection, linearLayoutComplaint, linearLayoutSignout,
            linearlayoutSearchIcon, linearLayoutCreateOrder, linearLayoutClientVisit, linearLayoutRefresh,
            linearLayoutAcknowledge, linearLayoutStockEntry, linearLayoutAddMerchant,
            linearLayoutsurveyuser, linearLayoutdownloadscheme, linearLayoutMyDayPlan, linearLayoutMySales,
            linearLayoutDistStock, linearLayoutStockOnly, linearLayoutMigration, linearLayoutStockAudit;

    private void showMenu(){

        MyApplication app = MyApplication.getInstance();

        if(!app.isCreateOrder()){
            linearLayoutCreateOrder.setVisibility(View.GONE);
        }
        if(!app.isProfile()){
            linearLayoutProfile.setVisibility(View.GONE);
        }
        if(!app.isMyDayPlan()){
            linearLayoutMyDayPlan.setVisibility(View.GONE);
        }
        if(!app.isMyOrders()){
            linearLayoutMyOrders.setVisibility(View.GONE);
        }
        if(!app.isAddMerchant()){
            linearLayoutAddMerchant.setVisibility(View.GONE);
        }
        if(!app.isMySales()){
            linearLayoutMySales.setVisibility(View.GONE);
        }
        if(!app.isPostNotes()){
            linearLayoutComplaint.setVisibility(View.GONE);
        }
        if(!app.isMyClientVisit()){
            linearLayoutClientVisit.setVisibility(View.GONE);
        }
        if(!app.isAcknowledge()){
            linearLayoutAcknowledge.setVisibility(View.GONE);
        }
        if(!app.isPaymentCollection()){
            linearLayoutPaymentCollection.setVisibility(View.GONE);
        }
        if(!app.isPkdDataCapture()){
            linearLayoutStockEntry.setVisibility(View.GONE);
        }
        if(!app.isDistributorStock()){
            linearLayoutDistStock.setVisibility(View.GONE);
        }
        if(!app.isSurveyUser()){
            linearLayoutsurveyuser.setVisibility(View.GONE);
        }
        if(!app.isDownLoadScheme()){
            linearLayoutdownloadscheme.setVisibility(View.GONE);
        }
        if(!app.isDistanceCalculation()){
            //linearLayoutDistanceCalculation.setVisibility(View.GONE);
        }
        if(!app.isRefresh()){
            linearLayoutRefresh.setVisibility(View.GONE);
        }
        if(!app.isLogout()){
            linearLayoutSignout.setVisibility(View.GONE);
        }
        if(!app.isClosingStock()){
            linearLayoutStockOnly.setVisibility(View.GONE);
        }
        if(!app.isClosingStockAudit()){
            linearLayoutStockAudit.setVisibility(View.GONE);
        }
        if(!app.isPendingData()){
            linearLayoutMigration.setVisibility(View.GONE);
        }
    }

    JSONObject JsonAccountObject = null;
    JSONArray JsonAccountArray = null;
    DatabaseHandler databaseHandler;
    JsonServiceHandler JsonServiceHandler;

    public static ArrayList<MerchantOrderDetailDomain> arraylistMerchantOrderDetailList, arraylistMerchantOrderDetailListSelected;

    public static String companyname, fullname, date, address, orderId, selected_dealer;
    public static String Duserid = "NULL", userid = "NULL", prodcode = "NULL", prodid = "NULL", prodname = "NULL", merchantRowid, beatrowid;
    Date dateStr = null;

    public static String time, searchclick = "0", strMsg = "", PaymentStatus = "null", Orderno;
    public static EditText editTextSearchField;
    public static TextView textViewHeader, textViewAssetSearch, textViewCross;

    int size = 0;
    public static AutoCompleteTextView autoCompleteDistributor;
    ArrayList<MDealerCompDropdownDomain> arrayListDealerDomain, arrayListSearch, arrayListmerchant;
    ArrayList<MerchantDropdownDomain> arraylistmerchantdropdown, searchresult;
    ArrayList<String> arraylistDistributor;

    String muserId;
    Button buttonSaveDistStock;
    public static DistributrStockDetailAdapter adapter;
    public static int merchantselected = 0;
    public static MerchantDropDownAdapter dropdown;
    double createordergeolat, createordergeolog;
    public static String pushstatus, startTime, userBeatModel = "";
    public SQLiteDatabase db;

    GPSTracker gps;

    public List<DistributorStockListModel> stockList = new ArrayList<DistributorStockListModel>();
    public List<DistributorStockListModel> arrayListSearchResults = new ArrayList<DistributorStockListModel>();

    public void hideSoftKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
    }

    public static void hideKeyboard(Activity activity) {
        View view = activity.findViewById(android.R.id.content);
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void showKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.distributr_stock);

        netCheck();
        databaseHandler = new DatabaseHandler(getApplicationContext());
        autoCompleteDistributor = (AutoCompleteTextView) findViewById(R.id.autoCompleteDistributor);

        gps = new GPSTracker(DistributrStockActivity.this);
        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();

        Cursor curPay;
        curPay = databaseHandler.getDetails();
        if(curPay != null && curPay.getCount() > 0){
            curPay.moveToFirst();
            PaymentStatus = curPay.getString(curPay.getColumnIndex(DatabaseHandler.KEY_paymentStatus));
            curPay.close();
        }

        ///Kumaravel /// For Offline Count
        TextView tvFailedCount = (TextView) findViewById(R.id.id_D_stock_failedCount);
        TextView tvOfflineCount = (TextView) findViewById(R.id.id_d_stock_offlineCount);
        String strFailedCount = tvFailedCount.getText().toString();
        String strOfflineCount = tvOfflineCount.getText().toString();

        Cursor cur_count = databaseHandler.getDistributorStockPending();
        if(cur_count != null && cur_count.getCount() > 0){
            tvOfflineCount.setText((strOfflineCount+cur_count.getCount()));
            cur_count.close();
        }else {
            tvOfflineCount.setText((strOfflineCount+0));
        }
        Cursor cur_failed_count = databaseHandler.getDistributorStockFailed();
        if(cur_failed_count != null && cur_failed_count.getCount() > 0){
            tvFailedCount.setText((strFailedCount+cur_failed_count.getCount()));
            cur_failed_count.close();
        }else {
            tvFailedCount.setText((strFailedCount+0));
        }

        Cursor cur1;
        cur1 = databaseHandler.getUserSetting();
        cur1.moveToFirst();
        userBeatModel = cur1.getString(cur1.getColumnIndex(DatabaseHandler.KEY_settingrefvalue));
        Log.e("userBeatModel", userBeatModel);
        linearLayoutRefresh = (LinearLayout) findViewById(R.id.linearLayoutRefresh);
        textViewAssetMenuRefresh = (TextView) findViewById(R.id.textViewAssetMenuRefresh);


        linearLayoutCreateOrder = (LinearLayout) findViewById(R.id.linearLayoutCreateOrder);
        listViewProducts = (ListView) findViewById(R.id.listViewProducts);
        linearLayoutProfile = (LinearLayout) findViewById(R.id.linearLayoutProfile);
        linearLayoutMyOrders = (LinearLayout) findViewById(R.id.linearLayoutMyOrders);
        linearLayoutMyDealers = (LinearLayout) findViewById(R.id.linearLayoutMyDealers);
        linearLayoutProducts = (LinearLayout) findViewById(R.id.linearLayoutProducts);
        linearLayoutPayment = (LinearLayout) findViewById(R.id.linearLayoutPayment);
        linearLayoutComplaint = (LinearLayout) findViewById(R.id.linearLayoutComplaint);
        linearLayoutSignout = (LinearLayout) findViewById(R.id.linearLayoutSignout);
        linearlayoutSearchIcon = (LinearLayout) findViewById(R.id.linearlayoutSearchIcon);
        linearLayoutClientVisit = (LinearLayout) findViewById(R.id.linearLayoutClientVisit);
        linearLayoutPaymentCollection = (LinearLayout) findViewById(R.id.linearLayoutPaymentCollection);

        linearLayoutAcknowledge = (LinearLayout) findViewById(R.id.linearLayoutAcknowledge);
        linearLayoutStockEntry = (LinearLayout) findViewById(R.id.linearLayoutStockEntry);
        linearLayoutAddMerchant = (LinearLayout) findViewById(R.id.linearLayoutAddMerchant);

        linearLayoutdownloadscheme = (LinearLayout) findViewById(R.id.linearLayoutdownloadscheme);
        linearLayoutsurveyuser = (LinearLayout) findViewById(R.id.linearLayoutsurveyuser);
        linearLayoutMyDayPlan = (LinearLayout) findViewById(R.id.linearLayoutMyDayPlan);
        linearLayoutMySales = (LinearLayout) findViewById(R.id.linearLayoutMySales);
        linearLayoutDistStock = (LinearLayout) findViewById(R.id.linearLayoutDistStock);

        linearLayoutStockOnly = (LinearLayout) findViewById(R.id.linearLayoutStockOnly);
        linearLayoutStockAudit = findViewById(R.id.linearLayoutStockAudit);
        linearLayoutMigration = (LinearLayout) findViewById(R.id.linearLayoutMigration);

        buttonSaveDistStock = (Button) findViewById(R.id.buttonSaveStock);
        textViewNodata = (TextView) findViewById(R.id.textViewNodata);
        textView2 = (TextView) findViewById(R.id.textView2);

        arrayListDealerDomain = new ArrayList<MDealerCompDropdownDomain>();
        arrayListmerchant = new ArrayList<MDealerCompDropdownDomain>();
        arrayListSearch = new ArrayList<MDealerCompDropdownDomain>();
        arraylistmerchantdropdown = new ArrayList<MerchantDropdownDomain>();
        searchresult = new ArrayList<MerchantDropdownDomain>();
        arraylistMerchantOrderDetailListSelected = new ArrayList<>();

        linearLayoutMyDealers.setVisibility(View.GONE);
        linearLayoutProducts.setVisibility(View.GONE);
        linearLayoutCreateOrder.setVisibility(View.VISIBLE);

        int permissionCheck1 = ContextCompat.checkSelfPermission(DistributrStockActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);

        if (permissionCheck1 != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    DistributrStockActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.LOCATION_PERMISSION_REQUEST_CODE);
        } else {
            try {
                getcurrentlocation();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (userBeatModel.equalsIgnoreCase("C")) {
            Log.e("userModel C", "userModel C");
            Cursor curr;
            curr = databaseHandler.getDetails();
            curr.moveToFirst();
            String selectedbeat = curr.getString(curr.getColumnIndex(DatabaseHandler.KEY_selectedBeat));

            if (selectedbeat.equals("") || selectedbeat.isEmpty()) {
                Constants.beatassignscreen = "DistStock";
                Intent to = new Intent(DistributrStockActivity.this, CompanyBeatActivity.class);
                startActivity(to);
                finish();
            }
            //getBeatMerchant(selectedbeat);
            getDistributors(databaseHandler);

        } else {
            Log.e("userModel D", "userModel D");
        }

        autoCompleteDistributor.setThreshold(1);

        autoCompleteDistributor.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                autoCompleteDistributor.showDropDown();
                textViewHeader.setVisibility(View.VISIBLE);
                textViewcross_1.setVisibility(View.VISIBLE);
                editTextSearchField.setVisibility(View.GONE);
                textViewCross.setVisibility(View.GONE);
                textViewcross_1.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        autoCompleteDistributor.setText("");
                        Constants.distributorname_stock = "";
                        Constants.distributorId = "";
                        arraylistMerchantOrderDetailList = null;
                        stockList.clear();
                        arrayListSearchResults.clear();
                        notifyAdapter();
                        //hideSoftKeyboard(autoCompleteDistributor);
                        hideKeyboard(DistributrStockActivity.this);
                        merchantselected = 0;
                        if (autoCompleteDistributor.equals("")) {
                            //InputMethodManager iaa = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            //iaa.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                        }

                    }
                });
                return false;
            }
        });
        autoCompleteDistributor
                .setOnItemClickListener(new OnItemClickListener() {
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {

                        startTime = new SimpleDateFormat(
                                "HH:mm:ss").format(new Date());
                        Constants.startTime = startTime;
                        Log.e("datestart", Constants.startTime);
                        Constants.distributorname_stock = autoCompleteDistributor.getText().toString();
                        Log.e("distname",Constants.distributorname_stock);

                        if(!Constants.distributorname_stock.isEmpty() && Constants.distributorname_stock != null) {
                            if (netCheck()) {
                                getDistProducts(textView2);
                            } else {
                                final Cursor c = databaseHandler.getStockEntryData();
                                if (c != null && c.getCount() > 0) {
                                    if (c.moveToFirst()) {
                                        String entryData = c.getString(1);
                                        getStockEntryList(entryData);
                                    }
                                    c.close();
                                    notifyAdapter();
                                } else {
                                    textView2.setText("No Products available");
                                }
                            }
                            merchantselected = 1;

                            if (merchantselected == 1) {
                                InputMethodManager iaa = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                iaa.hideSoftInputFromWindow(linearlayoutSearchIcon.getWindowToken(), 0);

                                textViewcross_1.setVisibility(View.GONE);

                            }
                            //hideKeyboard(DistributrStockActivity.this);
                            Log.e("keyboard","...........hide focus lost...........");
                        }else {
                            showAlertDialogToast("Please Select Distributor Name");
                        }


                    }
                });

        menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
        textViewAssetSearch = (TextView) findViewById(R.id.textViewAssetSearch);
        textViewHeader = (TextView) findViewById(R.id.textViewHeader);

        textViewAssetMenuCreateOrder = (TextView) findViewById(R.id.textViewAssetMenuCreateOrder);
        textViewAssetMenuProfile = (TextView) findViewById(R.id.textViewAssetMenuProfile);
        textViewAssetMenuMyOrders = (TextView) findViewById(R.id.textViewAssetMenuMyOrders);
        textViewAssetMenuMyDealers = (TextView) findViewById(R.id.textViewAssetMenuMyDealers);
        textViewAssetMenuProducts = (TextView) findViewById(R.id.textViewAssetMenuProducts);
        textViewAssetMenuPayment = (TextView) findViewById(R.id.textViewAssetMenuPayment);
        textViewAssetMenuComplaint = (TextView) findViewById(R.id.textViewAssetMenuComplaint);
        textViewAssetMenuSignout = (TextView) findViewById(R.id.textViewAssetMenuSignout);
        textViewAssetMenuClientVisit = (TextView) findViewById(R.id.textViewAssetMenuClientVisit);
        textViewAssetMenuAddMerchant = (TextView) findViewById(R.id.textViewAssetMenuAddMerchant);
        textViewAssetMenuAcknowledge = (TextView) findViewById(R.id.textViewAssetMenuAcknowledge);
        textViewAssetMenuStockEntry = (TextView) findViewById(R.id.textViewAssetMenuStockEntry);
        textViewAssetMenudownloadscheme = (TextView) findViewById(R.id.textViewAssetMenudownloadscheme);
        textViewAssetMenusurveyuser = (TextView) findViewById(R.id.textViewAssetMenusurveyuser);
        textViewAssetMenuMyDayPlan = (TextView) findViewById(R.id.textViewAssetMenuMyDayPlan);
        textViewAssetMenuMySales = (TextView) findViewById(R.id.textViewAssetMenuMySales);
        textViewAssetMenuPaymentCollection = (TextView) findViewById(R.id.textViewAssetMenuPaymentCollection);

        textViewAssetMenuStockOnly = (TextView) findViewById(R.id.textViewAssetMenuStockOnly);
        textViewAssetMenuStockAudit = (TextView) findViewById(R.id.textViewAssetMenuStockAudit);
        textViewAssetMenuMigration = findViewById(R.id.textViewAssetMenuMigration);



        linearLayoutMenuParent = (LinearLayout) findViewById(R.id.linearLayoutMenuParent);

        textViewCross = (TextView) findViewById(R.id.textViewCross);
        textViewcross_1 = (TextView) findViewById(R.id.textViewcross_1);
        editTextSearchField = (EditText) findViewById(R.id.editTextSearchField);

        Typeface font = Typeface.createFromAsset(getAssets(),
                "fontawesome-webfont.ttf");
        menuIcon.setTypeface(font);
        textViewAssetMenuCreateOrder.setTypeface(font);
        textViewAssetSearch.setTypeface(font);
        textViewAssetMenuProfile.setTypeface(font);
        textViewAssetMenuMyOrders.setTypeface(font);
        textViewAssetMenuMyDealers.setTypeface(font);
        textViewAssetMenuProducts.setTypeface(font);
        textViewAssetMenuPayment.setTypeface(font);
        textViewAssetMenuComplaint.setTypeface(font);
        textViewAssetMenuSignout.setTypeface(font);
        textViewAssetMenuClientVisit.setTypeface(font);
        textViewCross.setTypeface(font);
        textViewcross_1.setTypeface(font);
        textViewAssetMenuPaymentCollection.setTypeface(font);
        textViewAssetMenuRefresh.setTypeface(font);

        textViewAssetMenuAcknowledge.setTypeface(font);
        textViewAssetMenuStockEntry.setTypeface(font);
        textViewAssetMenuAddMerchant.setTypeface(font);
        textViewAssetMenusurveyuser.setTypeface(font);
        textViewAssetMenudownloadscheme.setTypeface(font);
        textViewAssetMenuMyDayPlan.setTypeface(font);
        textViewAssetMenuMySales.setTypeface(font);

        textViewAssetMenuStockOnly.setTypeface(font);
        textViewAssetMenuStockAudit.setTypeface(font);
        textViewAssetMenuMigration.setTypeface(font);

        buttonSaveDistStock.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Constants.checkproduct = 1;
                Log.e("clicksavedist",Constants.distributorname_stock);
                if (Constants.distributorname_stock.length() > 0 ) {
                    size = adapter.getSelectedStockList().size();
                    Log.e("size", String.valueOf(size));
                    if (size == 0) {
                        showAlertDialogToast("No stocks entered to save");

                    } else {
                        if(netCheck()){
                            pushStock();
                        }else {
                            ContentValues cv = new ContentValues();
                            cv.put(DatabaseHandler.KEY_DISTRIBUTOR_STOCK_URL, Utils.strSaveDistriStock);
                            cv.put(DatabaseHandler.KEY_DISTRIBUTOR_STOCK_SYNC_DATA, syncData());
                            cv.put(DatabaseHandler.KEY_DISTRIBUTOR_STOCK_FAILED_REASON, Constants.EMPTY);
                            cv.put(DatabaseHandler.KEY_DISTRIBUTOR_STOCK_MIGRATION_STATUS, 1);
                            databaseHandler.loadDistributorStock(cv);
                            //databaseHandler.insertSyncData(Utils.strSaveDistriStock, syncData());
                            //arraylistMerchantOrderDetailList.clear();
                            //arraylistMerchantOrderDetailListSelected.clear();
                            stockList.clear();
                            adapter.getSelectedStockList().clear();
                            showAlertDialogToast1("Saved Successfully");
                            //notifyAdapter();
                            //// don't want to see that blink after recreate() method

                        }
                    }

                } else {
                    showAlertDialogToast("Please choose a Distributor");
                }

            }
        });

        linearlayoutSearchIcon.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub
                if (searchclick == "0") {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    textViewHeader.setVisibility(TextView.GONE);
                    editTextSearchField.setVisibility(EditText.VISIBLE);
                    editTextSearchField.setFocusable(true);
                    textViewcross_1.setVisibility(View.GONE);
                    editTextSearchField.requestFocus();
                    textViewCross.setVisibility(TextView.VISIBLE);
                    InputMethodManager iaa = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    iaa.hideSoftInputFromWindow(linearlayoutSearchIcon.getWindowToken(), 0);
                    InputMethodManager iss = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    iss.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                    Log.e("Keyboard", "Show");
                    searchclick = "1";
                } else {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    textViewHeader.setVisibility(TextView.VISIBLE);
                    editTextSearchField.setVisibility(EditText.GONE);
                    textViewCross.setVisibility(TextView.GONE);
                    textViewcross_1.setVisibility(View.GONE);
                    InputMethodManager iss = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    iss.hideSoftInputFromWindow(linearlayoutSearchIcon.getWindowToken(), 0);
                    searchclick = "0";
                }

            }
        });

        editTextSearchField.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

                //arraylistMerchantOrderDetailList = new ArrayList<MerchantOrderDetailDomain>();
                stockList.clear();
                String searchText = editTextSearchField.getText().toString().toLowerCase(Locale.getDefault());

                for (DistributorStockListModel pd : arrayListSearchResults) {
                    if (pd.getProdName().toLowerCase(Locale.getDefault()).contains(searchText) || pd.getProdCode().toLowerCase(Locale.getDefault()).contains(searchText)) {

                        stockList.add(pd);
                    }
                }
                DistributrStockActivity.this.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        adapter = new DistributrStockDetailAdapter(
                                DistributrStockActivity.this,
                                R.layout.item_dist_stock_details,
                                stockList);
                        listViewProducts.setAdapter(adapter);
                    }
                });


            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {

            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

                textViewCross.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        editTextSearchField.setText("");

                        if (editTextSearchField.equals("")) {
                            InputMethodManager iss = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            iss.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                        }
                    }
                });
            }
        });

        menuIcon.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (menuCliccked == 0) {
                    linearLayoutMenuParent.setVisibility(View.VISIBLE);

                    editTextSearchField.setVisibility(EditText.GONE);
                    textViewHeader.setVisibility(TextView.VISIBLE);
                    textViewCross.setVisibility(TextView.GONE);
                    textViewcross_1.setVisibility(View.GONE);
                    InputMethodManager iss = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    iss.hideSoftInputFromWindow(menuIcon.getWindowToken(), 0);
                    menuCliccked = 1;
                } else {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    textViewHeader.setVisibility(TextView.VISIBLE);
                    editTextSearchField.setVisibility(EditText.GONE);
                    textViewCross.setVisibility(TextView.GONE);
                    InputMethodManager iss = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    iss.hideSoftInputFromWindow(menuIcon.getWindowToken(), 0);
                    menuCliccked = 0;
                }

            }
        });

        // All menus and actions stuff

        linearLayoutMenuParent.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);

                menuCliccked = 0;
            }
        });

        linearLayoutDistStock.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
            }
        });

        linearLayoutCreateOrder.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                MyApplication.getInstance().setTemplate8ForTemplate2(false);
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Constants.Merchantname = "";
                Constants.SalesMerchant_Id = "";
                merchantselected = 0;
                getDetails("createorder");
            }
        });

        linearLayoutProfile.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                // TODO Auto-generated method stub

                if (netCheck() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    merchantselected = 0;
                    getDetails("profile");
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });

        linearLayoutMyOrders.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheck() == true) {
                    Constants.orderstatus = "Success";

                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    merchantselected = 0;
                    getDetails("myorders");
                } else {
                    Constants.orderstatus = "Success";
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    merchantselected = 0;

                        Intent io = new Intent(DistributrStockActivity.this,
                                SalesManOrderActivity.class);
                        startActivity(io);
                        finish();
                }


            }
        });

        linearLayoutRefresh.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.e("Clicked", "Clicked");

                if (netCheck()) {

                    final Dialog qtyDialog = new Dialog(DistributrStockActivity.this);
                    qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    qtyDialog.setContentView(R.layout.warning_dialog);

                    final Button buttonUpdateOk, buttonUpdateCancel;

                    final TextView textQtyValidate;

                    buttonUpdateOk = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateOk);
                    buttonUpdateCancel = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateCancel);

                    textQtyValidate = (TextView) qtyDialog
                            .findViewById(R.id.textQtyValidate);
                    qtyDialog.show();


                    buttonUpdateOk.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            databaseHandler.synTableDelete();
                            Constants.refreshscreen = "createorder";
                            Intent io = new Intent(DistributrStockActivity.this, RefreshActivity.class);
                            startActivity(io);
                            finish();
                            qtyDialog.dismiss();
                        }

                    });

                    buttonUpdateCancel
                            .setOnClickListener(new OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub

                                    qtyDialog.dismiss();

                                }
                            });

                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }
            }

        });


        linearLayoutMyDealers.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheck() == true) {
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    merchantselected = 0;
                    getDetails("mydealer");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Constants.Merchantname = "";
                        Constants.SalesMerchant_Id = "";
                        merchantselected = 0;
                        Intent io = new Intent(DistributrStockActivity.this,
                                MyDealersActivity.class);
                        io.putExtra("Key", "menuclick");
                        startActivity(io);
                        finish();

                    }
                }

            }
        });


        linearLayoutComplaint.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheck()) {
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    merchantselected = 0;
                    getDetails("postnotes");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment");
                    } else {
                        Constants.Merchantname = "";
                        Constants.SalesMerchant_Id = "";
                        merchantselected = 0;
                        Intent io = new Intent(DistributrStockActivity.this,
                                MerchantComplaintActivity.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });
        linearLayoutClientVisit.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheck() == true) {
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    merchantselected = 0;
                    getDetails("clientvisit");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Constants.Merchantname = "";
                        Constants.SalesMerchant_Id = "";
                        merchantselected = 0;
                        Intent to = new Intent(DistributrStockActivity.this,
                                SMClientVisitHistory.class);
                        startActivity(to);
                        finish();
                    }
                }

            }
        });

        linearLayoutPaymentCollection.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    merchantselected = 0;
                    getDetails("paymentcoll");
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }


            }
        });

        linearLayoutAcknowledge.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    merchantselected = 0;
                    getDetails("acknowledge");
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }


            }
        });
        linearLayoutStockEntry.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheck() == true) {
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    merchantselected = 0;
                    getDetails("stockentry");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment");
                    }else {
                        menuCliccked = 0;
                        Constants.Merchantname = "";
                        Constants.SalesMerchant_Id = "";
                        merchantselected = 0;
                        Intent io = new Intent(DistributrStockActivity.this,
                                OutletStockEntry.class);
                        startActivity(io);
                        finish();
                    }
                }


            }
        });


        linearLayoutAddMerchant.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheck() == true) {
                    Constants.orderstatus = "Success";
                    Constants.Merchantname = "";
                    Constants.SalesMerchant_Id = "";
                    merchantselected = 0;
                    getDetails("addmerchant");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Constants.orderstatus = "Success";
                        Constants.Merchantname = "";
                        Constants.SalesMerchant_Id = "";
                        merchantselected = 0;

                        Intent io = new Intent(DistributrStockActivity.this,
                                AddMerchantNew.class);
                        startActivity(io);
                        finish();
                    }
                }


            }
        });
        linearLayoutdownloadscheme.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck()) {

                    final Dialog qtyDialog = new Dialog(DistributrStockActivity.this);
                    qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    qtyDialog.setContentView(R.layout.warning_dialog);

                    final Button buttonUpdateOk, buttonUpdateCancel;

                    final TextView textQtyValidate;

                    buttonUpdateOk = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateOk);
                    buttonUpdateCancel = (Button) qtyDialog
                            .findViewById(R.id.buttonUpdateCancel);

                    textQtyValidate = (TextView) qtyDialog
                            .findViewById(R.id.textQtyValidate);
                    qtyDialog.show();


                    buttonUpdateOk.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            databaseHandler.deletescheme();
                            Constants.downloadScheme = "createorder";
                            Intent io = new Intent(DistributrStockActivity.this, SchemeDownload.class);
                            startActivity(io);
                            finish();
                            qtyDialog.dismiss();
                        }

                    });

                    buttonUpdateCancel
                            .setOnClickListener(new OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub

                                    qtyDialog.dismiss();

                                }
                            });

                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });
        linearLayoutsurveyuser.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheck() == true) {
                    getDetails("surveyuser");
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals(null) || PaymentStatus.equals("") || PaymentStatus.equals("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent to = new Intent(DistributrStockActivity.this, SurveyMerchant.class);
                        startActivity(to);
                        finish();
                    }
                }
            }
        });

        linearLayoutMyDayPlan.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    getDetails("mydayplan");
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }
            }
        });

        linearLayoutMySales.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (netCheck() == true) {
                    linearLayoutMenuParent.setVisibility(View.GONE);
                    menuCliccked = 0;
                    getDetails("mysales");
                } else {
                    showAlertDialogToast("Please Check Your internet connection");
                }
            }
        });

        linearLayoutSignout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                // TODO Auto-generated method stub
                Constants.Merchantname = "";
                Constants.SalesMerchant_Id = "";
                merchantselected = 0;

                databaseHandler.delete();
                Intent io = new Intent(DistributrStockActivity.this,
                        SigninActivity.class);
                startActivity(io);
                finish();
            }
        });

        linearLayoutMigration.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent io = new Intent(DistributrStockActivity.this,
                        PendingDataMigrationActivity.class);
                startActivity(io);
            }
        });

        linearLayoutStockOnly.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                MyApplication.getInstance().setTemplate8ForTemplate2(true);
                if (netCheck()) {
                    Intent io = new Intent(DistributrStockActivity.this,
                            ClosingStockDashBoardActivity.class);
                    startActivity(io);
                    finish();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals("") || PaymentStatus.equalsIgnoreCase("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(DistributrStockActivity.this,
                                ClosingStockDashBoardActivity.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });

        linearLayoutStockAudit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.getInstance().setTemplate8ForTemplate2(false);
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if (netCheck()) {
                    Intent io = new Intent(DistributrStockActivity.this,
                            ClosingStockAudit.class);
                    startActivity(io);
                    finish();
                } else {
                    if (PaymentStatus.equals("Pending") || PaymentStatus.equals("") || PaymentStatus.equalsIgnoreCase("null")) {
                        showAlertDialogToast("Please make payment before place order");
                    } else {
                        Intent io = new Intent(DistributrStockActivity.this,
                                ClosingStockAudit.class);
                        startActivity(io);
                        finish();
                    }
                }
            }
        });

        ////////////
        showMenu();
        //////////////
    }

    // End of all menus and actions stuff


    private void getDistributors(DatabaseHandler dbHandler) {

        Cursor curs;
        Log.e("calling dist", "getDistributors()");
        curs = dbHandler.getDistributor();
        Log.e("distCount", String.valueOf(curs.getCount()));
        arraylistDistributor = new ArrayList<String>();

        if(curs!=null && curs.getCount() > 0) {
            if (curs.moveToFirst()) {

                do{
                    Log.e("inside", "inside");
                    arraylistDistributor.add( curs.getString(0));


                } while (curs.moveToNext());


            }
            Log.e("outside", "outside");
        }
        Log.e("Distributor count",String.valueOf(arraylistDistributor.size()));
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                DistributrStockActivity.this, R.layout.textview, R.id.textView,
                arraylistDistributor);

        autoCompleteDistributor.setAdapter(adapter);

    }

    public void getcurrentlocation() throws IOException {

        if (gps.canGetLocation() == true) {
            Intent intent = new Intent(this, GPSTrackerActivity.class);
            startActivityForResult(intent, 1);

        } else {
            showSettingsAlert();

        }

    }


    @TargetApi(23)
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permission, @NonNull int[] grantResults) {

        Log.e("requestcode", String.valueOf(requestCode));
        Log.e("size", String.valueOf(grantResults.length));

        if (requestCode == Constants.LOCATION_PERMISSION_REQUEST_CODE) {
            Log.e("requestcode", String.valueOf(grantResults[0]));
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                Intent io = new Intent(DistributrStockActivity.this, DistributrStockActivity.class);
                startActivity(io);
                finish();
            } else {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, Constants.LOCATION_PERMISSION_REQUEST_CODE);
            }
        }
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(DistributrStockActivity.this);

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.delete);

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, 2);

            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("inside", String.valueOf(resultCode));
        if (resultCode == 0) {
            switch (requestCode) {
                case 1:
                    Log.e("inside", "inside");
                    Intent io = new Intent(DistributrStockActivity.this, DistributrStockActivity.class);
                    startActivity(io);
                    finish();
                    break;
            }
        }
        if (requestCode == 1) {
            try {
                Bundle extras = data.getExtras();
                createordergeolog = extras.getDouble("Longitude");
                Log.e("createordergeolog", String.valueOf(createordergeolog));
                createordergeolat = extras.getDouble("Latitude");
                Log.e("createordergeolat", String.valueOf(createordergeolat));
            }catch (Exception e){
                e.printStackTrace();
            }

        }

        if (requestCode == 2) {
            Intent intent = new Intent(this, GPSTrackerActivity.class);
            startActivityForResult(intent, 1);
        }
    }

    private void getDetails(final String paymentcheck) {
        if (paymentcheck.equals("profile")) {
            Intent io = new Intent(DistributrStockActivity.this,
                    ProfileActivity.class);
            startActivity(io);
            finish();

        } else if (paymentcheck.equals("myorders")) {
            Intent io = new Intent(DistributrStockActivity.this,
                    SalesManOrderActivity.class);
            startActivity(io);
            finish();

        } else if (paymentcheck.equals("mydealer")) {
            Intent io = new Intent(DistributrStockActivity.this,
                    MyDealersActivity.class);
            io.putExtra("Key", "menuclick");
            startActivity(io);
            finish();

        } else if (paymentcheck.equals("postnotes")) {
            Intent io = new Intent(DistributrStockActivity.this,
                    MerchantComplaintActivity.class);
            startActivity(io);
            finish();

        } else if (paymentcheck.equals("placeorder")) {
            Constants.imagesetcheckout = 1;
            Intent to = new Intent(DistributrStockActivity.this,
                    SalesManOrderCheckoutActivity.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("clientvisit")) {
            Intent to = new Intent(DistributrStockActivity.this,
                    SMClientVisitHistory.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("paymentcoll")) {
            Intent to = new Intent(DistributrStockActivity.this,
                    SalesmanPaymentCollectionActivity.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("addmerchant")) {
            Intent to = new Intent(DistributrStockActivity.this,
                    AddMerchantNew.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("acknowledge")) {
            Intent to = new Intent(DistributrStockActivity.this,
                    SalesmanAcknowledgeActivity.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("stockentry")) {
            Intent to = new Intent(DistributrStockActivity.this,
                    OutletStockEntry.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("surveyuser")) {
            Intent to = new Intent(DistributrStockActivity.this,
                    SurveyMerchant.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("mydayplan")) {
            Intent to = new Intent(DistributrStockActivity.this,
                    MyDayPlan.class);
            startActivity(to);
            finish();
        } else if (paymentcheck.equals("mysales")) {
            Intent to = new Intent(DistributrStockActivity.this,
                    MySalesActivity.class);
            startActivity(to);
            finish();
        }else if(paymentcheck.equals("createorder")) {
            Intent io = new Intent(DistributrStockActivity.this,
                    CreateOrderActivity.class);
            startActivity(io);
            finish();
        }
    }

    public void setadap() {

        Log.e("arraylist", String.valueOf(arraylistMerchantOrderDetailList.size()));
        adapter = new DistributrStockDetailAdapter(
                DistributrStockActivity.this,
                R.layout.item_dist_stock_details,
                stockList);
        listViewProducts.setAdapter(adapter);

    }

    protected void toastDisplay(String msg) {
        Toast toast = Toast.makeText(DistributrStockActivity.this, msg,
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();

    }

    public boolean netCheck() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {

                return true;
            } else if (result == null) {

                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public void showAlertDialog(Context context, String title, String message,
                                Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        alertDialog.show();
    }

    public void showAlertDialogToast(String message) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setMessage(message);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert = builder.create();
        alert.show();

        Button buttonbackground = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    public void showAlertDialogToast1(String message) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setMessage(message);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        autoCompleteDistributor.setText(Constants.EMPTY);
                        finish();
                        overridePendingTransition(0, 0);
                        startActivity(getIntent());
                        overridePendingTransition(0, 0);
                    }
                });


        AlertDialog alert = builder.create();
        alert.show();

        Button buttonbackground = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    public void onBackPressed() {
        exitAlret();
    }

    private void exitAlret() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        localBuilder.setMessage("Do you want to Exit?");
        localBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {

                        finish();

                    }
                });
        localBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {
                        paramDialogInterface.cancel();
                    }
                });
        localBuilder.create().show();
    }

    private void setAdapter(){
        adapter = new DistributrStockDetailAdapter(
                DistributrStockActivity.this,
                R.layout.item_dist_stock_details,
                stockList);
        listViewProducts.setAdapter(adapter);
    }

    private void getStockEntryList(final String dataObj){
        JSONArray data = null;
        try {
            final JSONObject obj = new JSONObject(dataObj);
            data = obj.getJSONArray("data");
            stockList.clear();//////
            arrayListSearchResults.clear();
            Log.e("JSONArrayDist",data.toString());

            for (int i=0;i<data.length();i++) {
                DistributorStockListModel currentStockList = new DistributorStockListModel();

                JSONObject jsonObjectProduct= data.getJSONObject(i);

                currentStockList.setSerialNo(Integer.toString((i+1))); ;
                currentStockList.setProdCode(jsonObjectProduct.getString("prodcode"));
                currentStockList.setProdId(jsonObjectProduct.getString("prodid"));
                currentStockList.setProdName(jsonObjectProduct.getString("prodname"));
                currentStockList.setOpening(jsonObjectProduct.getString("opening"));
                currentStockList.setClosing(jsonObjectProduct.getString("closing"));
                currentStockList.setUomId(jsonObjectProduct.getString("uomid"));
                stockList.add(currentStockList);
            }
            arrayListSearchResults.addAll(stockList);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void getDistProducts(final TextView textView2)
    {
        new AsyncTask<Void, Void, Void>() {
            String strStatus = "", strMsg;
            ProgressDialog dialog;

            @Override
            protected void onPreExecute() {
                dialog= ProgressDialog.show(DistributrStockActivity.this, "",
                        "Loading...", true, true);
            }
            @Override
            protected void onPostExecute(Void result) {
                dialog.dismiss();
                try {
                    Log.e("onPostExecute","dist products");
                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("returnmessage", strMsg);
                    if(strStatus.equalsIgnoreCase("false")){
                        textView2.setText("No Products available");
                    }
                    else {

                            JSONArray data = JsonAccountObject.getJSONArray("data");

                            Log.e("JSONArrayDist",data.toString());
                            stockList.clear();//////
                            arrayListSearchResults.clear();
                            for (int i=0;i<data.length();i++) {
                                //MerchantOrderDetailDomain merchantOrderDetailDomain = new MerchantOrderDetailDomain();
                                DistributorStockListModel currentStockList = new DistributorStockListModel();

                                JSONObject jsonObjectProduct= data.getJSONObject(i);

                                currentStockList.setSerialNo(Integer.toString((i+1))); ;
                                currentStockList.setProdCode(jsonObjectProduct.getString("prodcode"));
                                currentStockList.setProdId(jsonObjectProduct.getString("prodid"));
                                currentStockList.setProdName(jsonObjectProduct.getString("prodname"));
                                currentStockList.setOpening(jsonObjectProduct.getString("opening"));
                                currentStockList.setClosing(jsonObjectProduct.getString("closing"));
                                currentStockList.setUomId(jsonObjectProduct.getString("uomid"));
                                stockList.add(currentStockList);
                            }
                        arrayListSearchResults.addAll(stockList);

                        DistributrStockActivity.this.runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                adapter = new DistributrStockDetailAdapter(
                                        DistributrStockActivity.this,
                                        R.layout.item_dist_stock_details,
                                        stockList);
                                listViewProducts.setAdapter(adapter);
                            }
                        });
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("distributorname", Constants.distributorname_stock);

                    Log.e("Products list",jsonObject.toString());

                    JsonServiceHandler = new JsonServiceHandler(Utils.strDistriProd, jsonObject.toString(), DistributrStockActivity.this);
                    JsonAccountObject = JsonServiceHandler.ServiceData();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute(new Void[]{});
    }


    private void notifyAdapter(){
        adapter = new DistributrStockDetailAdapter(
                DistributrStockActivity.this,
                R.layout.item_dist_stock_details,
                stockList);
        listViewProducts.setAdapter(adapter);
    }
    private String syncData(){
        JSONObject jsonObject = new JSONObject();
        JSONObject jsonDtlObject;

        try {
            JSONArray jsonArray = new JSONArray();
            Map<Integer, DistributorStockModel> selectedMapList = adapter.getSelectedStockList();

            for (Map.Entry<Integer, DistributorStockModel> entry: selectedMapList.entrySet()) {
                jsonDtlObject = new JSONObject();
                Integer key = entry.getKey();
                DistributorStockModel item = entry.getValue();
                Log.e(TAG, "......key(serialNo), ... Value()" + key + ".....value" + item.toString());
                try {
                    Log.e("Stk json","null "+item.getProdId());
                    jsonDtlObject.put("prodid", item.getProdId());
                    jsonDtlObject.put("purchaseqty", item.getPurchaseQty());
                    jsonDtlObject.put("salesqty", item.getSalesQty());
                    jsonDtlObject.put("uomid", item.getItemUOM());

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                jsonArray.put(jsonDtlObject);

            }

            jsonObject.put("distributorname", Constants.distributorname_stock);
            jsonObject.put("suserid",Constants.USER_ID);
            jsonObject.put("stocklist", jsonArray);
            jsonObject.put("starttime",Constants.startTime);
            jsonObject.put("endtime",new SimpleDateFormat(
                    "HH:mm:ss").format(new Date()));

            Log.i("DistStockJSON",jsonObject.toString());

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
      return jsonObject.toString();
    }
    private void pushStock() {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "", strOrderId = "";

            @Override
            protected void onPreExecute() {
                dialog = ProgressDialog.show(DistributrStockActivity.this, "",
                        "Loading...", true, true);
            }

            @Override
            protected void onPostExecute(Void result) {

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    if (strStatus.equals("true")) {

                        stockList.clear();
                        adapter.getSelectedStockList().clear();
                        showAlertDialogToast1("Saved Successfully");


                    } else {
                        showAlertDialogToast1("Failed! Please Contact Support");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    //Log.e("ProductActivityException", e.toString());
                }
                dialog.dismiss();
            }

            @Override
            protected Void doInBackground(Void... params) {
                JsonServiceHandler = new JsonServiceHandler(Utils.strSaveDistriStock, syncData(), DistributrStockActivity.this);
                JsonAccountObject = JsonServiceHandler.ServiceData();
                return null;
            }
        }.execute(new Void[]{});
    }
}
