package com.freshorders.freshorder.ui;

import java.io.InputStream;
import java.util.List;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.utils.DocUploadFileToServer1;
import com.freshorders.freshorder.utils.Utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ImportActivity extends Activity {
	public static TextView textViewAssetBack,textViewFileName;
	Button buttonPickFile,buttonDownload,buttonImport;
	public static String docPath1 ="";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(1);
		setContentView(R.layout.import_screen);
		
		netCheck();
		textViewAssetBack = (TextView) findViewById(R.id.textViewAssetBack);
		textViewFileName = (TextView) findViewById(R.id.textViewFileName);
		buttonPickFile = (Button) findViewById(R.id.buttonPickFile);
		buttonDownload = (Button) findViewById(R.id.buttonDownload);
		buttonImport = (Button) findViewById(R.id.buttonImport);
		
		Typeface font = Typeface.createFromAsset(getAssets(),
				"fontawesome-webfont.ttf");
		textViewAssetBack.setTypeface(font);
		buttonPickFile.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
		

				Intent i = new Intent(Intent.ACTION_GET_CONTENT);
				i.setType("file/*");
				startActivityForResult(i, 1);
			}

		});
		buttonDownload.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
		
				  Boolean result=isDownloadManagerAvailable(ImportActivity.this);
			        if (result) {
			           
			           
			            downloadFile(Utils.strDownloadUrl,"ImportProduct.csv");

			        }

			}

		});
		buttonImport.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
		

				DocUploadFileToServer1 ufs = new DocUploadFileToServer1(ImportActivity.this,
						 docPath1);//,jobi
				ufs.execute();
			}

		});
		
		
		textViewAssetBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent intent = new Intent(ImportActivity.this,
						ProductActivity.class);
				startActivity(intent);
				finish();
			}

		});
		
		
		
		
		
		
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == 1) {
				Uri selectedPath = data.getData();
				docPath1 = selectedPath.getPath();
				String filename = selectedPath.toString();
				String filenameArray[] = filename.split("\\.");
				String filenameArray1[] = filename.split("\\/");
				String extension = filenameArray[filenameArray.length - 1];
				String extension1 = filenameArray1[filenameArray1.length - 1];
				if (extension.equals("csv")
						|| extension.equals("txt")) {
					showAlertDialogToast("File Selected");
					//toast("File Selected");
					textViewFileName.setText(extension1);

				} else {
					showAlertDialogToast("File Selected");
					//toast("Unsupported format");
					docPath1="";
				}
			}

		}
	}
	
	

	public void toast(String msg) {
		Toast toast = Toast.makeText(ImportActivity.this, msg, Toast.LENGTH_SHORT);
		// toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
	}
	public boolean netCheck() {
		// for network connection
		try {
			ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetwork = connectionManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			Object result = null;
			if (mWifi.isConnected() || mNetwork.isConnected()) {
				return true;
			}

			else if (result == null) {
				showAlertDialog(ImportActivity.this,
						"No Internet Connection",
						"Please Check Your internet connection.", false);
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	public void showAlertDialog(Context context, String title, String message,
			Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
					}
				});
		alertDialog.show();
	}
	  @TargetApi(Build.VERSION_CODES.HONEYCOMB)
	    public void downloadFile(String documentSelected,String documentName){
	        String DownloadUrl =documentSelected;
	        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(DownloadUrl));
	        request.setDescription("Downloading CASE CATALOG");   //appears the same in Notification bar while downloading
	        request.setTitle(documentName);
	        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
	            request.allowScanningByMediaScanner();
	            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
	        }
	        request.setDestinationInExternalFilesDir(getApplicationContext(), Environment.DIRECTORY_DOWNLOADS, documentName);

	        // get download service and enqueue file
	        DownloadManager manager=(DownloadManager) getApplicationContext().getSystemService(getApplicationContext().DOWNLOAD_SERVICE);
	        manager.enqueue(request);
		  showAlertDialogToast("Downloading");
		  //Toast.makeText(getApplicationContext(), "Downloading...", Toast.LENGTH_SHORT).show();
	    }


	    public static boolean isDownloadManagerAvailable(Context context) {
	        try {
	            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.GINGERBREAD) {
	                return false;
	            }
	            Intent intent = new Intent(Intent.ACTION_MAIN);
	            intent.addCategory(Intent.CATEGORY_LAUNCHER);
	            intent.setClassName("com.android.providers.downloads.ui","com.android.providers.downloads.ui.DownloadList");
	            List<ResolveInfo> list = context.getPackageManager().queryIntentActivities(intent,
	                    PackageManager.MATCH_DEFAULT_ONLY);
	            return list.size() > 0;
	        } catch (Exception e) {
	            return false;
	        }
	    }


	public void showAlertDialogToast( String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();

		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

	}

	public void onBackPressed() {
		exitAlret();
	}

	private void exitAlret() {
		AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
		localBuilder.setCancelable(false);
		localBuilder.setMessage("Do you want to Exit?");
		localBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {

						finish();

					}
				});
		localBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {
						paramDialogInterface.cancel();
					}
				});
		localBuilder.create().show();
	}
}
