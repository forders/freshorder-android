package com.freshorders.freshorder.ui;

import java.security.PublicKey;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.DealerOrderDetailAdapter;
import com.freshorders.freshorder.adapter.MerchantOrderCheckoutAdapter;
import com.freshorders.freshorder.adapter.MerchantOrderDetailAdapter;
import com.freshorders.freshorder.domain.DealerOrderDetailDomain;
import com.freshorders.freshorder.domain.MerchantOrderDetailDomain;
import com.freshorders.freshorder.domain.MerchantOrderDomainSelected;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MerchantOrderCheckoutActivity extends Activity {
	TextView menuIcon;
	JSONObject JsonAccountObject = null;
	JSONArray JsonAccountArray = null;
	JsonServiceHandler JsonServiceHandler;
	public static ListView listViewOrders;
	LinearLayout linearLayoutBack;
	public static Context context;

	public static String[] orderdtid, userid,prodid,qty,date,status,freeQty,paymentType;
	public static String flag,orderdt;
	public static int DeliveryDate,year,month,day,hr,mins,sec;
	//int index;
	Button buttonAddPlaceOrder;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(1);
		setContentView(R.layout.merchant_my_order_checkout);
		netCheck();
		buttonAddPlaceOrder = (Button) findViewById(R.id.buttonAddPlaceOrder);
		menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
		listViewOrders = (ListView) findViewById(R.id.listViewOrders);
		linearLayoutBack = (LinearLayout) findViewById(R.id.linearLayoutBack);
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();
		context = MerchantOrderCheckoutActivity.this;

		Typeface font = Typeface.createFromAsset(getAssets(),
				"fontawesome-webfont.ttf");
		menuIcon.setTypeface(font);

		menuIcon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});

		buttonAddPlaceOrder.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!String.valueOf(MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.size()).equals("0")){
					paymentcheck();
				}else{
					showAlertDialogToast("Please Select the Products");

				}
			}
		});


		final Calendar c = Calendar.getInstance();
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);
		hr= c.get(Calendar.HOUR_OF_DAY);
		mins= c.get(Calendar.MINUTE);
		sec= c.get(Calendar.SECOND);

		DeliveryDate= year+(month + 1)+day;
		orderdt=year+"-"+(month + 1)+"-"+day+" "+hr+":"+mins+":"+sec;

		menuIcon .setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent io=new Intent(MerchantOrderCheckoutActivity.this,MerchantOrderDetailActivity.class);
				startActivity(io);
				finish();
			}
		});



		MerchantOrderCheckoutAdapter adapter = new MerchantOrderCheckoutAdapter(
				MerchantOrderCheckoutActivity.this,
				R.layout.item_merchant_order_details,
				MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected);
		listViewOrders.setAdapter(adapter);

	}


	public static void removeid(String id) {

		for (MerchantOrderDetailDomain pd : MerchantOrderDetailActivity.arraylistMerchantOrderDetailList) {
			if (pd.getprodid().equals(id)) {
				pd.setqty("");
				pd.setFreeQty("");
				Constants.checkproduct = 1;
				// CreateOrderActivity.arraylistMerchantOrderDetailList.add(pd);
			}
		}

	}

	private void saveOrder() {
		// TODO Auto-generated method stub
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "",strOrderId;

			@Override
			protected void onPreExecute() {
				dialog= ProgressDialog.show(MerchantOrderCheckoutActivity.this, "",
						"Loading...", true, true);
			}


			@Override
			protected void onPostExecute(Void result) {

				try {

					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);
					if(strStatus.equals("true")){
						strOrderId = JsonAccountObject.getJSONObject("data").getString("ordid");
						Log.e("strOrderId", strOrderId);
						MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.clear();
						Intent to = new Intent (MerchantOrderCheckoutActivity.this, MerchantOrderConfirmation.class);
						to.putExtra("strOrderId",strOrderId);
						startActivity(to);
						finish();


					}else{
						showAlertDialogToast(strMsg);
					}



				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					//Log.e("ProductActivityException", e.toString());
				}
				dialog.dismiss();
			}

			@Override
			protected Void doInBackground(Void... params) {
				JSONObject jsonObject = new JSONObject();
				try {
					JSONArray jsonArray = new JSONArray();
					JSONObject jsonObject1,jsonObject2;
					userid = new String[MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.size()];
					prodid = new String[MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.size()];
					qty = new String[MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.size()];
					freeQty= new String[MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.size()];


					for (int i = 0; i < MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.size(); i++) {
						userid[i] =MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.get(i).getUserid();
						prodid[i] =MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.get(i).getprodid();
						qty[i]=MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.get(i).getqty();
						freeQty[i]=MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.get(i).getFreeQty();

					}
					for (int i = 0; i < MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.size(); i++) {
						jsonObject1 = new JSONObject();
						jsonObject2 = new JSONObject();
						double frqty = Float.parseFloat(freeQty[i]);
						double qtty = Float.parseFloat(qty[i]);
						String[] fread=new String[MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.size()];
						String[] qread=new String[MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.size()];
						try {

							if(qtty>0){
								jsonObject1.put("qty", qty[i]);
								jsonObject1.put("isfree", "NF");
								jsonObject1.put("prodid",prodid[i]);
								jsonObject1.put("duserid", userid[i]);
								//jsonObject1.put("ordslno", i);
								//jsonObject1.put("ordid",Constants.orderid);
								//jsonObject1.put("duserid", Constants.USER_ID);
								qread[i]=userid[i]+","+Constants.USER_ID;
								Log.e("inside",qread[i]);
								jsonObject1.put("isread",qread[i]);
								jsonObject1.put("ordstatus", "Pending");
								jsonObject1.put("ordslno", i);
								jsonObject1.put("deliverydt",orderdt);

								Log.e("date",orderdt);

								jsonArray.put(jsonObject1);
								jsonObject.put("orderdtls", jsonArray);
							}
							if(frqty>0){
								jsonObject2.put("qty", freeQty[i]);
								jsonObject2.put("isfree", "F");
								jsonObject2.put("prodid",prodid[i]);
								jsonObject2.put("duserid", userid[i]);
								//jsonObject2.put("ordslno", i);
								jsonObject2.put("ordstatus", "Pending");
								//jsonObject2.put("ordid",Constants.orderid);
								//jsonObject2.put("duserid", Constants.USER_ID);
								fread[i]=userid[i]+","+Constants.USER_ID;
								Log.e("outside",fread[i]);
								jsonObject2.put("isread",fread[i]);
								jsonObject2.put("deliverydt",orderdt);
								jsonObject2.put("ordslno",i);
								jsonArray.put(jsonObject2);
								jsonObject.put("orderdtls", jsonArray);
							}



						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}



					}


					jsonObject.put("muserid", Constants.USER_ID);
					jsonObject.put("usertype", Constants.USER_TYPE);
					jsonObject.put("orderdt",orderdt);


					if(Constants.PaymentType.equals("Cash")){
						jsonObject.put("iscash","C");
					}else if(Constants.PaymentType.equals("Credit")){
						jsonObject.put("iscash", "CR");
					}

					Log.e("payment type",Constants.PaymentType);

					jsonObject.put("orderdtls", jsonArray);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				JsonServiceHandler = new JsonServiceHandler(Utils.strsavemerchantOderdetail,jsonObject.toString(), MerchantOrderCheckoutActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceData();
				return null;
			}
		}.execute(new Void[] {});
	}


	public void paymentcheck() {

		final Dialog qtyDialog = new Dialog(MerchantOrderCheckoutActivity.this);
		qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		qtyDialog.setContentView(R.layout.paymenttype);

		Button buttonUpdateOk, buttonUpdateCancel;

		buttonUpdateOk = (Button) qtyDialog
				.findViewById(R.id.buttonUpdateOk);
		buttonUpdateCancel = (Button) qtyDialog
				.findViewById(R.id.buttonUpdateCancel);

		userid = new String[MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.size()];
		prodid = new String[MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.size()];
		qty = new String[MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.size()];
		freeQty= new String[MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.size()];


		for (int i = 0; i < MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.size(); i++) {
			userid[i] =MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.get(i).getusrdlrid();
			prodid[i] =MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.get(i).getprodid();
			qty[i]=MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.get(i).getqty();
			freeQty[i]=MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.get(i).getFreeQty();
			//index[i]=MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.get(i);
		}

		if(Constants.orderid!=""){


			updateorder();
		}else{
			qtyDialog.show();


		}

		buttonUpdateOk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {


				RadioGroup radioPayment;
				RadioButton radioCash;
				radioPayment = (RadioGroup) qtyDialog.findViewById(R.id.radioPayment);
				int selectedId = radioPayment.getCheckedRadioButtonId();

				// find the radiobutton by returned id
				radioCash = (RadioButton) qtyDialog.findViewById(selectedId);
				Constants.PaymentType = radioCash.getText().toString();
				//dod.setpaymentType(item.getPaymentType());

				Log.e("payment_type", Constants.PaymentType);
				qtyDialog.dismiss();
				saveOrder();

			}

		});

		buttonUpdateCancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				qtyDialog.dismiss();
			}
		});
	}

	public void updateorder(){
		new AsyncTask<Void, Void, Void>() {
			ProgressDialog dialog;
			String strStatus = "";
			String strMsg = "";

			@Override
			protected void onPreExecute() {
				dialog= ProgressDialog.show(MerchantOrderCheckoutActivity.this, "",
						"Loading...", true, true);
			}

			@Override
			protected void onPostExecute(Void result) {

				try {

					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					strMsg = JsonAccountObject.getString("message");
					Log.e("return message", strMsg);
					showAlertDialogToast(strMsg);
					if(strStatus.equals("true")){
						Constants.adapterset=0;
						Intent to = new Intent(MerchantOrderCheckoutActivity.this,
								MerchantSingleDetailActivity.class);
						startActivity(to);
						finish();
					}else{
						showAlertDialogToast(strMsg);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					//Log.e("ProductActivityException", e.toString());
				}
				dialog.dismiss();
			}

			@Override
			protected Void doInBackground(Void... params) {
				JSONObject jsonObject = new JSONObject();
				// try {
				JSONArray jsonArray = new JSONArray();
				JSONObject jsonObject1;
				JSONObject jsonObject2;


				for (int i = 0; i < MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.size(); i++) {
					jsonObject1 = new JSONObject();
					jsonObject2 = new JSONObject();
					try {

						Log.e("fqty",freeQty[i]);
						Log.e("qty",qty[i]);

						double frqty = Float.parseFloat(freeQty[i]);
						double qtty = Float.parseFloat(qty[i]);

						int serialno=Integer.parseInt(Constants.serialno);
						serialno=serialno+1;
						Constants.serialno=String.valueOf(serialno);

						if(frqty>0){
							flag="F";
							Log.e("Freeqty", freeQty[i]);
							jsonObject1.put("qty", freeQty[i]);
							jsonObject1.put("isfree", flag);
							jsonObject1.put("ordid",Constants.orderid);
							jsonObject1.put("prodid",prodid[i]);
							jsonObject1.put("ordstatus", "Pending");
							jsonObject1.put("duserid", userid[i]); //Constants.dealerid
							jsonObject1.put("ordslno", serialno);
							jsonObject1.put("deliverydt",  orderdt);

							jsonArray.put(jsonObject1);
							jsonObject.put("orderdtls", jsonArray);
						}

						if(qtty>0){
							flag= "NF";
							Log.e("quantity", qty[i]);
							jsonObject2.put("qty", qty[i]);
							jsonObject2.put("isfree", flag);
							jsonObject2.put("ordid",Constants.orderid);
							jsonObject2.put("prodid",prodid[i]);
							jsonObject2.put("ordstatus", "Pending");
							jsonObject2.put("duserid", userid[i]);
							jsonObject2.put("ordslno", serialno);
							jsonObject2.put("deliverydt",  orderdt);
							jsonArray.put(jsonObject2);
							jsonObject.put("orderdtls", jsonArray);
						}



					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//jsonArray.put(jsonObject1);
				}


               /* } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }*/
				JsonServiceHandler = new JsonServiceHandler(Utils.strsavedealerOderdetail,jsonObject.toString(), MerchantOrderCheckoutActivity.this);
				JsonAccountObject = JsonServiceHandler.ServiceData();
				return null;
			}
		}.execute(new Void[] {});
	}
	public boolean netCheck() {
		// for network connection
		try {
			ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connManager
					.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetwork = connectionManager
					.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			Object result = null;
			if (mWifi.isConnected() || mNetwork.isConnected()) {
				return true;
			}

			else if (result == null) {
				showAlertDialog(MerchantOrderCheckoutActivity.this,
						"No Internet Connection",
						"Please Check Your internet connection.", false);
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	public void showAlertDialog(Context context, String title, String message,
								Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
					}
				});
		alertDialog.show();
	}

	protected void toastDisplay(String msg) {
		Toast toast = Toast.makeText(MerchantOrderCheckoutActivity.this, msg,
				Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

	}

	public void showAlertDialogToast( String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();

		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

	}

	public void onBackPressed() {
		exitAlret();
	}

	private void exitAlret() {
		AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
		localBuilder.setCancelable(false);
		localBuilder.setMessage("Do you want to Exit?");
		localBuilder.setPositiveButton("Yes",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {

						finish();

					}
				});
		localBuilder.setNegativeButton("No",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface paramDialogInterface,
										int paramInt) {
						paramDialogInterface.cancel();
					}
				});
		localBuilder.create().show();
	}



}
