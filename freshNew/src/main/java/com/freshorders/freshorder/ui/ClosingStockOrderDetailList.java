package com.freshorders.freshorder.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.ClosingStockOrderDetailAdapter;
import com.freshorders.freshorder.adapter.SalesmanCameraAdapter;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.DealerOrderDetailDomain;
import com.freshorders.freshorder.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

public class ClosingStockOrderDetailList extends AppCompatActivity {

    TextView textViewName,textViewAddress,textViewDate,menuIcon,textViewdelord,textViewQty;
    LinearLayout linearLayoutBack;
    JSONObject JsonAccountObject = null;
    JSONArray JsonAccountArray = null;
    com.freshorders.freshorder.utils.JsonServiceHandler JsonServiceHandler ;
    public static ArrayList<DealerOrderDetailDomain> arraylistDealerOrderDetailList;
    ListView listViewOrders;
    Date dateStr = null;
    public static String time;
    Button buttonUpdate,buttonAddNewProducts,buttonAddproductsecond,buttonUpdatesecond;
    DatabaseHandler databaseHandler;
    public static String[] orderdtid, orderid,prodid,prodPrice,qty,date,status,freeQty,duserid,flag,
            defaultuom,selectedUom,deliverydate,deliverymode,deliverytime,pvalue,defaultuomtotal;
    String[] ordtlId;
    public static String pushStatus, OrderId;
    ArrayList<String> Orderdtlist ;
    SQLiteDatabase db;
    public static int popup=0;
    LinearLayout linearlayoutsecond,linearlayoutmain,linearlayoutmaingallery;
    Gallery gridViewcamera;
    double approximateTotal=0.0;
    String appr="";
    private boolean isOrderProcessed = false;  // kumaravel need to check if online order is delivered or partial delivered
    //public static ArrayList<Bitmap> arrayListimagebitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_closing_stock_order_detail_list);
        //arrayListimagebitmap = new ArrayList<Bitmap>();
        databaseHandler = new DatabaseHandler(getApplicationContext());
        db=getApplicationContext().openOrCreateDatabase("freshorders", 0, null);
        textViewName = (TextView) findViewById(R.id.textViewName);
        textViewAddress = (TextView) findViewById(R.id.textViewAddress);
        textViewDate = (TextView) findViewById(R.id.textViewDate);
        textViewdelord = (TextView) findViewById(R.id.textViewdelord);
        textViewQty = (TextView) findViewById(R.id.textViewQty);
        menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
        listViewOrders = (ListView) findViewById(R.id.listViewOrders);
        buttonUpdate = (Button) findViewById(R.id.buttonUpdate);
        buttonAddNewProducts= (Button) findViewById(R.id.buttonAddproduct);
        buttonAddproductsecond= (Button) findViewById(R.id.buttonAddproductsecond);
        buttonUpdatesecond= (Button) findViewById(R.id.buttonUpdatesecond);
        gridViewcamera= (Gallery) findViewById(R.id.gridViewcamera);
        linearlayoutsecond= (LinearLayout) findViewById(R.id.linearlayoutsecond);
        linearlayoutmain= (LinearLayout) findViewById(R.id.linearlayoutmain);
        linearlayoutmaingallery= (LinearLayout) findViewById(R.id.linearlayoutmaingallery);
        Orderdtlist =new  ArrayList<String>();
        linearLayoutBack = (LinearLayout) findViewById(R.id.linearLayoutBack);
        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();

        gridViewcamera.setSelection(1);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) gridViewcamera.getLayoutParams();
        mlp.setMargins(-450, 0, 0, 0);

        Typeface font = Typeface.createFromAsset(getAssets(),
                "fontawesome-webfont.ttf");
        menuIcon.setTypeface(font);
        textViewdelord.setTypeface(font);
        //textViewName.setText(SalesManOrderActivity.fullname);
        /*
        //Kumaravel 14/01/2019
        Cursor curOrderStatus;
        try{
            curOrderStatus = databaseHandler.getOrderStatus(Integer.parseInt(SalesManOrderActivity.orderId));
            if(curOrderStatus != null && curOrderStatus.getCount() > 0){
                curOrderStatus.moveToFirst();
                isOrderProcessed = curOrderStatus.getInt(0) > 0;
                curOrderStatus.close();
            }
        }catch (Exception e){
            e.printStackTrace();
            isOrderProcessed = true; // Suppose Exception catched we don't know process status so we block
        }  */

        textViewName.setText(ClosingStockDashBoardActivity.fullname);
        String orderId = "OrderId-" + ClosingStockDashBoardActivity.orderId;
        textViewAddress.setText(orderId);


        textViewDate.setText(ClosingStockDashBoardActivity.date);
        orderstatus();
        products();////////////////////////////////////

        gridViewcamera.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent i = new Intent(getApplicationContext(), SM_FullGalleryView1.class);
                i.putExtra("position", position);
                i.putExtra("CLOSE", true);
                startActivity(i);
                ClosingStockOrderDetailList.this.finish();
            }
        });
        Log.e("size", String.valueOf(ClosingStockDashBoardActivity.image.length));
        if(ClosingStockDashBoardActivity.image.length==0) {

            linearlayoutmaingallery.setVisibility(View.GONE);

        }else {

            ClosingStockDashBoardActivity.arrayListimagebitmap.clear();
            for (int i = 0; i < ClosingStockDashBoardActivity.image.length; i++) {

                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                Bitmap bitmap = BitmapFactory.decodeFile(ClosingStockDashBoardActivity.image[i], bmOptions);
                bitmap = Bitmap.createScaledBitmap(bitmap, 350, 250, true);
                ClosingStockDashBoardActivity.arrayListimagebitmap.add(bitmap);
            }

            gridViewcamera.setAdapter(new SalesmanCameraAdapter(this));

        }

        linearLayoutBack.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if(pushStatus.equals("Success")){
                    Constants.orderstatus="Success";
                }else{
                    Constants.orderstatus="Pending";
                }
                Intent to = new Intent(ClosingStockOrderDetailList.this, ClosingStockDashBoardActivity.class);
                startActivity(to);
                finish();
            }
        });

    }

    public void orderstatus(){
        Cursor curs;
        curs = databaseHandler.getStockOrderHeaderStatus(ClosingStockDashBoardActivity.orderId);
        Log.e("HeaderCount", String.valueOf(curs.getCount()));

        if(curs!=null && curs.getCount() > 0) {
            if (curs.moveToFirst()) {
                do{
                    Log.e("pushstatus", curs.getString(0));
                    pushStatus =curs.getString(0);

                } while (curs.moveToNext());
            }
            Log.e("outside", "outside");
        }
    }

    private void products(){

        Cursor curs;
        curs = databaseHandler.getStockOrderDetail(ClosingStockDashBoardActivity.orderId);
        Log.e("ListCount", String.valueOf(curs.getCount()));
        arraylistDealerOrderDetailList=new ArrayList<DealerOrderDetailDomain>();

        if(curs!=null && curs.getCount() > 0) {
            if (curs.moveToFirst()) {
                do{
                    DealerOrderDetailDomain dod=new DealerOrderDetailDomain();
                    dod.setproductname(curs.getString(10));
                    dod.setSelcetedUOM(curs.getString(23));
                    dod.setCurrStock(curs.getString(27));
                    arraylistDealerOrderDetailList.add(dod);
                }while (curs.moveToNext());
            }
        }

        ClosingStockOrderDetailAdapter adapter = new
                ClosingStockOrderDetailAdapter(ClosingStockOrderDetailList.this,
                R.layout.item_closing_stock_ordered_list, arraylistDealerOrderDetailList);
        listViewOrders.setAdapter(adapter);
    }

    public void onBackPressed() {
        exitAlret();
    }

    private void exitAlret() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        localBuilder.setMessage("Do you want to Exit?");
        localBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {

                        finish();

                    }
                });
        localBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {
                        paramDialogInterface.cancel();
                    }
                });
        localBuilder.create().show();
    }
}
