package com.freshorders.freshorder.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.DealerOrderListAdapter;
import com.freshorders.freshorder.adapter.DealerVisitAdapter;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.DealerVisitDomain;
import com.freshorders.freshorder.domain.MDealerCompDropdownDomain;
import com.freshorders.freshorder.domain.SalesmanListDomain;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Pavithra on 19-10-2016.
 */

public class DealerClientVisit extends Activity {

    TextView textViewName,textViewAddress,textViewDate,menuIcon,textViewdelord,textViewFilterMenu,textViewNodata;
    LinearLayout linearLayoutBack,linearLayoutButton,linearLayoutHeading,linearLayoutFilter;
    JSONObject JsonAccountObject = null;
    JSONArray JsonAccountArray = null;
    com.freshorders.freshorder.utils.JsonServiceHandler JsonServiceHandler ;
    public static int menuCliccked;
    ListView listViewClientVisit;
    Date dateStr = null;
    public static ArrayList<DealerVisitDomain> arraylistDealerVisitDomain;
    public static String time,sname,mname,date,desc,lat,lng,PaymentStatus="NULL";
    DealerVisitAdapter adapter;

    public static  DealerVisitDomain domainSelected;
    TextView  textViewAssetMenuProfile, textViewAssetMenuMyOrders,
            textViewAssetMenuMyDealers, textViewAssetMenuProducts,
            textViewAssetMenuPayment, textViewAssetMenuComplaint,
            textViewAssetMenuSignout,textViewAssetMenuCreateOrder,
            textViewAssetMenuExport,textViewAssetClientVisit;
    LinearLayout linearLayoutMenuParent, linearLayoutProfile,
            linearLayoutMyOrders, linearLayoutMyDealers, linearLayoutProducts,
            linearLayoutPayment, linearLayoutComplaint, linearLayoutSignout,
            linearLayoutCreateOrder,linearLayoutExport,linearLayoutClientVisit;
    DatabaseHandler databaseHandler;
    ArrayList<String> arrayListSalesman;

    DatePicker datePicker;
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        requestWindowFeature(1);
        setContentView(R.layout.dealer_client_visit);
        //netCheck();
        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();
        datePicker= (DatePicker) findViewById(R.id.datePicker1);
        databaseHandler = new DatabaseHandler(getApplicationContext());
        textViewName = (TextView) findViewById(R.id.textViewName);
        textViewAddress = (TextView) findViewById(R.id.textViewAddress);
        textViewDate = (TextView) findViewById(R.id.textViewDate);
        textViewdelord = (TextView) findViewById(R.id.textViewdelord);
        textViewFilterMenu = (TextView) findViewById(R.id.textViewFilterMenu);
        textViewAssetClientVisit= (TextView) findViewById(R.id.textViewAssetClientVisit);
        menuIcon = (TextView) findViewById(R.id.textViewAssetMenu);
        listViewClientVisit = (ListView) findViewById(R.id.listViewClientVisit);
        linearLayoutMenuParent = (LinearLayout) findViewById(R.id.linearLayoutMenuParent);
        linearLayoutFilter = (LinearLayout) findViewById(R.id.linearLayoutFilter);
        linearLayoutBack = (LinearLayout) findViewById(R.id.linearLayoutBack);
        linearLayoutButton = (LinearLayout) findViewById(R.id.linearLayoutButton);
        linearLayoutHeading = (LinearLayout) findViewById(R.id.linearLayoutHeading);
        JsonAccountObject = new JSONObject();
        arrayListSalesman = new ArrayList<String>();

        linearLayoutCreateOrder = (LinearLayout) findViewById(R.id.linearLayoutCreateOrder);
        linearLayoutProfile = (LinearLayout) findViewById(R.id.linearLayoutProfile);
        linearLayoutMyOrders = (LinearLayout) findViewById(R.id.linearLayoutMyOrders);
        linearLayoutMyDealers = (LinearLayout) findViewById(R.id.linearLayoutMyDealers);
        linearLayoutProducts = (LinearLayout) findViewById(R.id.linearLayoutProducts);
        linearLayoutPayment = (LinearLayout) findViewById(R.id.linearLayoutPayment);
        linearLayoutComplaint = (LinearLayout) findViewById(R.id.linearLayoutComplaint);
        linearLayoutSignout = (LinearLayout) findViewById(R.id.linearLayoutSignout);
        linearLayoutExport= (LinearLayout) findViewById(R.id.linearLayoutExport);
        linearLayoutClientVisit= (LinearLayout) findViewById(R.id.linearLayoutClientVisit);
        if (Constants.USER_TYPE.equals("D")) {
            linearLayoutMyDealers.setVisibility(View.GONE);
            linearLayoutProducts.setVisibility(View.VISIBLE);
            linearLayoutCreateOrder.setVisibility(View.GONE);
            linearLayoutExport.setVisibility(View.VISIBLE);
        } else if (Constants.USER_TYPE.equals("M")){
            linearLayoutMyDealers.setVisibility(View.VISIBLE);
            linearLayoutProducts.setVisibility(View.GONE);
            linearLayoutCreateOrder.setVisibility(View.GONE);
            linearLayoutExport.setVisibility(View.GONE);
        }else {
            linearLayoutMyDealers.setVisibility(View.GONE);
            linearLayoutProducts.setVisibility(View.GONE);
            linearLayoutCreateOrder.setVisibility(View.VISIBLE);
            linearLayoutExport.setVisibility(View.GONE);
        }
        textViewAssetMenuCreateOrder= (TextView) findViewById(R.id.textViewAssetMenuCreateOrder);
        textViewAssetMenuProfile = (TextView) findViewById(R.id.textViewAssetMenuProfile);
        textViewAssetMenuMyOrders = (TextView) findViewById(R.id.textViewAssetMenuMyOrders);
        textViewAssetMenuMyDealers = (TextView) findViewById(R.id.textViewAssetMenuMyDealers);
        textViewAssetMenuProducts = (TextView) findViewById(R.id.textViewAssetMenuProducts);
        textViewAssetMenuPayment = (TextView) findViewById(R.id.textViewAssetMenuPayment);
        textViewAssetMenuComplaint = (TextView) findViewById(R.id.textViewAssetMenuComplaint);
        textViewAssetMenuSignout= (TextView) findViewById(R.id.textViewAssetMenuSignout);
        textViewAssetMenuExport= (TextView) findViewById(R.id.textViewAssetMenuExport);
        textViewNodata= (TextView) findViewById(R.id.textViewNodata);
        linearLayoutMenuParent = (LinearLayout) findViewById(R.id.linearLayoutMenuParent);
        linearLayoutMenuParent = (LinearLayout) findViewById(R.id.linearLayoutMenuParent);

        Typeface font = Typeface.createFromAsset(getAssets(),
                "fontawesome-webfont.ttf");
        menuIcon.setTypeface(font);
        textViewFilterMenu.setTypeface(font);
        textViewAssetMenuProfile.setTypeface(font);
        textViewAssetMenuMyOrders.setTypeface(font);
        textViewAssetMenuMyDealers.setTypeface(font);
        textViewAssetMenuProducts.setTypeface(font);
        textViewAssetMenuPayment.setTypeface(font);
        textViewAssetMenuComplaint.setTypeface(font);
        textViewAssetMenuSignout.setTypeface(font);
        textViewAssetMenuExport.setTypeface(font);
        textViewAssetClientVisit.setTypeface(font);
        if(Constants.fromDate.equals("")){
            myVisit();
        }else{
            myVisitFilter();
        }

        listViewClientVisit.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub
                domainSelected = arraylistDealerVisitDomain.get(position);
                date = arraylistDealerVisitDomain.get(position).getDate();
                sname = arraylistDealerVisitDomain.get(position).getSname();
                mname = arraylistDealerVisitDomain.get(position).getMname();
                lat = arraylistDealerVisitDomain.get(position).getGeoLat();
                lng = arraylistDealerVisitDomain.get(position).getGeoLng();
                desc = arraylistDealerVisitDomain.get(position).getNotes();

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(listViewClientVisit.getWindowToken(), 0);
                Intent to = new Intent(DealerClientVisit.this, DealerClientVisitDetailActivity.class);
                startActivity(to);
                finish();

            }
        });
        linearLayoutFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.filter = "1";
                Intent to = new Intent(DealerClientVisit.this, ExportActivity.class);
                startActivity(to);
                finish();

                //ExportActivity.textViewHeader.setText("Client Visit Filter");
            }
        });
        menuIcon.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub


                    if (menuCliccked == 0) {
                        linearLayoutMenuParent.setVisibility(View.VISIBLE);
                        Constants.fromDate="";
                        menuCliccked = 1;
                    } else {
                        linearLayoutMenuParent.setVisibility(View.GONE);
                        menuCliccked = 0;
                    }


            }
        });

        linearLayoutMenuParent.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
            }
        });

        linearLayoutClientVisit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                myVisit();

            }
        });

        linearLayoutProfile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if( netCheckwithoutAlert()==true){
                    getDetails("profile");
                }else{

                    showAlertDialogToast("Please Check Your internet connection");
                }
            }
        });

        linearLayoutExport.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                Constants.filter="0";
                if( netCheckwithoutAlert()==true){
                    getDetails("export");
                }else{

                    showAlertDialogToast("Please Check Your internet connection");
                }
            }
        });


        linearLayoutMyOrders.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                // TODO Auto-generated method stub
                if( netCheckwithoutAlert()==true){
                    getDetails("myorders");
                }else{

                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });

        linearLayoutMyDealers.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                // TODO Auto-generated method stub
                Intent io = new Intent(DealerClientVisit.this,
                        MyDealersActivity.class);
                Constants.fromDate="";
                io.putExtra("Key","MenuClick");
                startActivity(io);
                finish();
            }
        });

        linearLayoutProducts.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                // TODO Auto-generated method stub
                if( netCheckwithoutAlert()==true){
                    getDetails("product");
                }else{

                    showAlertDialogToast("Please Check Your internet connection");
                }
            }
        });

        linearLayoutCreateOrder.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                // TODO Auto-generated method stub
                Intent io = new Intent(DealerClientVisit.this,
                        CreateOrderActivity.class);
                Constants.fromDate="";
                startActivity(io);
                finish();
            }
        });

        linearLayoutPayment.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                if( netCheckwithoutAlert()==true){
                    Intent io = new Intent(DealerClientVisit.this,
                            DealerPaymentActivity.class);
                    Constants.fromDate="";
                    startActivity(io);
                    finish();
                }else{

                    showAlertDialogToast("Please Check Your internet connection");
                }

            }
        });

        linearLayoutComplaint.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                //if( netCheckwithoutAlert()==true){
                    getDetails("postnotes");
                //}else{

                 //   showAlertDialogToast("Please Check Your internet connection");
                //}

            }
        });

        linearLayoutSignout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                linearLayoutMenuParent.setVisibility(View.GONE);
                menuCliccked = 0;
                // TODO Auto-generated method stub
                databaseHandler.delete();
                Intent io = new Intent(DealerClientVisit.this,
                        SigninActivity.class);
                Constants.fromDate="";
                startActivity(io);
                finish();
            }
        });

    }

    private void getDetails(final String paymentcheck) {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "";

            @Override
            protected void onPreExecute() {
                dialog= ProgressDialog.show(DealerClientVisit.this, "",
                        "Loading...", true, true);

            }

            @Override
            protected void onPostExecute(Void result) {

                try {

                    PaymentStatus=(JsonAccountObject.getString("pymtstatus"));
                    Log.e("PaymentStatus",PaymentStatus);


                    if(PaymentStatus.equals("Pending")||PaymentStatus.equals(null) ||PaymentStatus.equals("")||PaymentStatus.equals("null")){
                        showAlertDialogToast("Please make payment before place order");
                    }else{
                        Log.e("paymentcheck", paymentcheck);

                        if(paymentcheck.equals("profile")){
                            Intent io = new Intent(DealerClientVisit.this,
                                    ProfileActivity.class);
                            Constants.fromDate="";
                            startActivity(io);
                            finish();

                        }else if(paymentcheck.equals("postnotes")){
                            Intent io = new Intent(DealerClientVisit.this,
                                    DealersComplaintActivity.class);
                            Constants.fromDate="";
                            startActivity(io);
                            finish();

                        }else if(paymentcheck.equals("export")){
                            Intent io = new Intent(DealerClientVisit.this,
                                    ExportActivity.class);
                            Constants.fromDate="";
                            startActivity(io);
                            finish();

                        }else if(paymentcheck.equals("myorders")){
                            Intent to = new Intent(DealerClientVisit.this,
                                    DealersOrderActivity.class);
                            Constants.fromDate="";
                            startActivity(to);
                            finish();

                        }else if(paymentcheck.equals("product")){

                            Intent io = new Intent(DealerClientVisit.this,
                                    ProductActivity.class);
                            startActivity(io);
                            finish();

                        }

                    }

                    dialog.dismiss();


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();


                JsonServiceHandler = new JsonServiceHandler(Utils.strGetProfileDetail+Constants.USER_ID, DealerClientVisit.this);
                JsonAccountObject = JsonServiceHandler.ServiceDataGet();
                return null;
            }
        }.execute(new Void[]{});
    }

    private void myVisit() {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "",strMsg ="";


            @Override
            protected void onPreExecute() {
                dialog= ProgressDialog.show(DealerClientVisit.this, "",
                        "Loading...", true, true);

            }

            @Override
            protected void onPostExecute(Void result) {

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);
                    int j=1;
                    if (strStatus.equals("true")) {

                        arraylistDealerVisitDomain=new ArrayList<DealerVisitDomain>();
                        textViewNodata.setVisibility(View.GONE);
                        listViewClientVisit.setVisibility(View.VISIBLE);
                        JSONArray job1 = JsonAccountObject.getJSONArray("data");

                        for (int i = 0; i < job1.length(); i++) {
                            JSONObject job = new JSONObject();
                            job=job1.getJSONObject(i);
                            DealerVisitDomain dod=new DealerVisitDomain();

                            if(!job.getString("muserid").equals("0")){
                                if(j==1){
                                    dod.setSerialNo(String.valueOf(j));
                                    j++;
                                }else{
                                    dod.setSerialNo(String.valueOf(j));
                                    j++;
                                }

                                Log.e("inside","inside");
                                dod.setSname(job.getJSONObject("suser").getString("fullname"));

                                dod.setMname(job.getJSONObject("muser").getString("companyname"));

                                if(job.has("planid")){
                                    dod.setPlanId(job.getString("planid"));
                                }else{
                                    dod.setPlanId(job.getString(""));
                                }

                                if(job.has("geolat")){
                                    dod.setGeoLat(job.getString("geolat"));
                                }else{
                                    dod.setGeoLat(job.getString(""));
                                }

                                if(job.has("geolong")){
                                    dod.setGeoLng(job.getString("geolong"));
                                }else{
                                    dod.setGeoLng(job.getString(""));
                                }

                                if(job.has("remarks")){
                                    dod.setNotes(job.getString("remarks"));
                                }else{
                                    dod.setNotes(job.getString(""));
                                }

                                if(job.has("orderplndt")){
                                    time=job.getString("orderplndt");
                                }else{
                                    time=job.getString("");
                                }
                                String inputPattern = "yyyy-MM-dd";
                                String outputPattern = "dd-MMM-yyyy";
                                SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
                                SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

                                String str = null;
                                Log.e("setadap","setadap");
                                try {
                                    dateStr = inputFormat.parse(time);
                                    str = outputFormat.format(dateStr);
                                    dod.setDate(str);
                                    //Log.e("str", str);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                Log.e("setadap","setadap");
                                arraylistDealerVisitDomain.add(dod);

                            }
                        }
                        setadap();
                    }else{
                        listViewClientVisit.setVisibility(View.GONE);
                        textViewNodata.setText(strMsg);
                        textViewNodata.setVisibility(View.VISIBLE);
                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {

                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();

                JsonServiceHandler = new JsonServiceHandler(Utils.strgetdealerVisit+Constants.USER_ID, DealerClientVisit.this);
                JsonAccountObject = JsonServiceHandler.ServiceDataGet();
                return null;
            }
        }.execute(new Void[] {});
    }




    private void myVisitFilter() {
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "",strMsg ="";


            @Override
            protected void onPreExecute() {
                dialog= ProgressDialog.show(DealerClientVisit.this, "",
                        "Loading...", true, true);

            }

            @Override
            protected void onPostExecute(Void result) {

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {


                        arraylistDealerVisitDomain=new ArrayList<DealerVisitDomain>();
                        JSONArray job1 = JsonAccountObject.getJSONArray("data");

                        for (int i = 0; i < job1.length(); i++) {
                            JSONObject job = new JSONObject();
                            job=job1.getJSONObject(i);
                            DealerVisitDomain dod=new DealerVisitDomain();

                            dod.setSname(job.getString("sname"));
                            dod.setMname(job.getString("mcompanyname"));
                            dod.setPlanId(job.getString("planid"));
                            dod.setGeoLat(job.getString("geolat"));
                            dod.setGeoLng(job.getString("geolong"));
                            dod.setSerialNo(String.valueOf(i + 1));
                            dod.setNotes(job.getString("remarks"));
                            time=job.getString("orderplndt");

                            String inputPattern = "yyyy-MM-dd HH:mm:ss";
                            String outputPattern = "dd-MMM-yyyy HH:mm:ss";
                            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
                            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

                            String str = null;

                            try {
                                dateStr = inputFormat.parse(time);
                                str = outputFormat.format(dateStr);
                                dod.setDate(str);
                                //Log.e("str", str);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            arraylistDealerVisitDomain.add(dod);


                        }

                        setadap();
                    }else{
                        listViewClientVisit.setVisibility(View.GONE);

                    }
                    dialog.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {

                }

            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();
                JSONArray jsonarray1 = new JSONArray();
                try {
                    jsonObject.put("duserid", Constants.USER_ID);
                    jsonObject.put("usertype", Constants.USER_TYPE);
                    jsonObject.put("mode","MOB");
                    jsonObject.put("fromplndt",Constants.fromDate);
                    jsonObject.put("toplndt",Constants.toDate);

                    Log.e("Length", String.valueOf(ExportActivity.selectedSalesman.length));
                    Log.e("size",String.valueOf(ExportActivity.size));
                if(ExportActivity.selectedSalesman.length!=0){
                        for(int j=0;j<ExportActivity.selectedSalesman.length;j++){
                            jsonarray1.put(ExportActivity.selectedSalesman[j]);
                        }

                        jsonObject.put("saleslist",jsonarray1);
                    }


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                JsonServiceHandler = new JsonServiceHandler(Utils.strclientVisitFilter,jsonObject.toString(), DealerClientVisit.this);
                JsonAccountObject = JsonServiceHandler.ServiceData();
                return null;
            }
        }.execute(new Void[] {});
    }
    public  void setadap() {
        // TODO Auto-generated method stub

        adapter=new DealerVisitAdapter(DealerClientVisit.this, R.layout.item_dealer_visit_page, arraylistDealerVisitDomain);
        listViewClientVisit.setVisibility(View.VISIBLE);
        listViewClientVisit.setAdapter(adapter);

    }

    protected void toastDisplay(String msg) {
        Toast toast = Toast.makeText(DealerClientVisit.this, msg,
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();

    }
    public boolean netCheckwithoutAlert() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            }

            else if (result == null) {

                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
    public boolean netCheck() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            }

            else if (result == null) {
                showAlertDialog(DealerClientVisit.this,
                        "No Internet Connection",
                        "Please Check Your internet connection.", false);
                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }
    public void showAlertDialog(Context context, String title, String message,
                                Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        alertDialog.show();
    }

    public void showAlertDialogToast( String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }
    public void onBackPressed() {
        exitAlret();
    }

    private void exitAlret() {
        AlertDialog.Builder localBuilder = new AlertDialog.Builder(this);
        localBuilder.setCancelable(false);
        localBuilder.setMessage("Do you want to Exit?");
        localBuilder.setPositiveButton("Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {

                        finish();

                    }
                });
        localBuilder.setNegativeButton("No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface paramDialogInterface,
                                        int paramInt) {
                        paramDialogInterface.cancel();
                    }
                });
        localBuilder.create().show();
    }
}
