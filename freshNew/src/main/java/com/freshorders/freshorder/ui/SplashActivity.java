package com.freshorders.freshorder.ui;
import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.accuracy.location.track.LocationTrackForegroundService;
import com.freshorders.freshorder.activity.ManagerActivity;
import com.freshorders.freshorder.crashreport.AppCrashConstants;
import com.freshorders.freshorder.crashreport.AppCrashReporter;
import com.freshorders.freshorder.crashreport.AppCrashUtil;
import com.freshorders.freshorder.crashreport.ExternalStorageUtil;
import com.freshorders.freshorder.crashreport.SendCrashReportToServer;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.model.CommonResponseModel;
import com.freshorders.freshorder.receiver.StartTrackingBroadCast;
import com.freshorders.freshorder.receiver.StopTrackingBroadCast;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.Dialogs;
import com.freshorders.freshorder.utils.MenuSetting;
import com.freshorders.freshorder.utils.TemplateSetting;
import com.freshorders.freshorder.volleylib.APIClient;
import com.freshorders.freshorder.volleylib.APIInterface;
import com.freshorders.freshorder.worker.FCMTokenUploadWorker;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import org.joda.time.DateTime;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.freshorders.freshorder.db.DatabaseHandler.KEY_TEMPLATE_VALUE;
import static com.freshorders.freshorder.service.PrefManager.FCM_TOKEN;
import static com.freshorders.freshorder.service.PrefManager.IS_DELIVERED_FCM_TOKEN;
import static com.freshorders.freshorder.service.PrefManager.KEY_IS_CRASH_HAPPENED;
import static com.freshorders.freshorder.utils.Constants.BR_REQUEST_CODE_START_LIVE_TRACK;
import static com.freshorders.freshorder.utils.Constants.BR_REQUEST_CODE_STOP_LIVE_TRACK;
import static com.freshorders.freshorder.utils.Constants.MENU_DISTANCE_CALCULATION_CODE;
import static com.freshorders.freshorder.utils.Constants.SELF_START_TIME_LIVE_TRACK;
import static com.freshorders.freshorder.utils.Constants.SELF_STOP_TIME_LIVE_TRACK;
import static com.freshorders.freshorder.utils.Utils.strFCMSendURL;

public class SplashActivity extends Activity {

	private static String TAG = SplashActivity.class.getSimpleName();
	DatabaseHandler databaseHandler;
	private static Context mContext;

	private String dUserId = "";

	// Kumaravel 03/03/2019

	private int timeoutMillis       = 5000;
	private long startTimeMillis     = 0;
	private Type CLASSNAME;

	/** The code used when requesting permissions */
	private static final int    PERMISSIONS_REQUEST = 123456;
	public int getTimeoutMillis() {
		return timeoutMillis;
	}
	@SuppressWarnings("rawtypes")
	public Class getNextActivityClass() {
		return CLASSNAME.getClass();
	};

	private String[] appPermissions ={
			Manifest.permission.WRITE_EXTERNAL_STORAGE,
			Manifest.permission.ACCESS_FINE_LOCATION,
			Manifest.permission.READ_CALENDAR,

	};

	private boolean checkPermissions() {
		List<String> listRequiredPermissions = new ArrayList<>();
		for(String curPermission : appPermissions){
			if(ContextCompat.checkSelfPermission(this, curPermission) != PackageManager.PERMISSION_GRANTED){
				listRequiredPermissions.add(curPermission);
			}
		}

		if(!listRequiredPermissions.isEmpty()){
			ActivityCompat.requestPermissions(this, listRequiredPermissions.toArray(new String[listRequiredPermissions.size()]), PERMISSIONS_REQUEST);
			return false;
		}
		return true;
	}

	public void setNextClass(Type nextClass){
		this.CLASSNAME = nextClass;
	}

	public String[] getRequiredPermissions() {
		String[] permissions = null;
		try {
			permissions = getPackageManager().getPackageInfo(getPackageName(),
					PackageManager.GET_PERMISSIONS).requestedPermissions;
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}
		if (permissions == null) {
			return new String[0];
		} else {
			return permissions.clone();
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
		if (requestCode == PERMISSIONS_REQUEST) {
			HashMap<String, Integer> perGrantedList = new HashMap<>();
			int deniedCount = 0;

			for(int i = 0; i < grantResults.length; i++){
				if(grantResults[i] == PackageManager.PERMISSION_DENIED){
					deniedCount ++;
					perGrantedList.put(permissions[i], grantResults[i]);
				}
			}

			if(deniedCount == 0){
				startNextActivity();
			}else {
				for(Map.Entry<String, Integer> entry: perGrantedList.entrySet()){
					String perName = entry.getKey();
					Integer perResult = entry.getValue();

					if(ActivityCompat.shouldShowRequestPermissionRationale(this, perName)){
						new Dialogs(this).showDialogOKCancel(
								"Warning",
								"This App needs storage location, Calendar permissions to work properly",
								"YES, Allow Permissions",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										dialog.dismiss();
										checkPermissions();/////
									}
								},
								"NO, App Close",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										dialog.dismiss();
										finish();
									}
								},
								false
						);
					}else {
						new Dialogs(this).showDialogOKCancel("Follow",
								"You Have Denied Some Permissions, Go To [Setting] -> [Permissions]",
								"Go To Setting",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
									dialog.dismiss();
									Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
											Uri.fromParts("package",getPackageName(), null));
									intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
									startActivity(intent);
									finish();
									}
								},
								"Exit App",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										dialog.dismiss();
										finish();
									}
								},
								false
						);
					break;
					}
				}
			}
		}
	}

	@TargetApi(23)
	private String[] requiredPermissionsStillNeeded() {

		Set<String> permissions = new HashSet<String>();
		for (String permission : getRequiredPermissions()) {
			permissions.add(permission);
		}
		for (Iterator<String> i = permissions.iterator(); i.hasNext();) {
			String permission = i.next();
			if (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED) {
				Log.d(SplashActivity.class.getSimpleName(),
						"Permission: " + permission + " already granted.");
				i.remove();
			} else {
				Log.d(SplashActivity.class.getSimpleName(),
						"Permission: " + permission + " not yet granted.");
			}
		}
		return permissions.toArray(new String[permissions.size()]);
	}

	private void startNextActivity(){

		String crashPath = ExternalStorageUtil.createOrGetCrashFolderFile(mContext.getApplicationContext());
		AppCrashReporter.setCrashFilePath(crashPath);
		MyApplication.getInstance().setCrashReporterPath(crashPath);
		new PrefManager(mContext.getApplicationContext()).setStringDataByKey(PrefManager.KEY_BUG_FILE_PATH,crashPath);
		Log.e(TAG,"Crash File Path...........:" + crashPath);


		String migPath = ExternalStorageUtil.createOrGetMigrationFolderFile(mContext.getApplicationContext());
		MyApplication.getInstance().setMigPath(migPath);
		Log.e(TAG,"Migration File Path...........:" + migPath);



		if(new PrefManager(mContext.getApplicationContext()).getBooleanDataByKey(KEY_IS_CRASH_HAPPENED)){
			new SendCrashReportToServer(mContext, crashPath).sendCrashFile();
		}

		final StringBuilder firstLine = new StringBuilder();
		databaseHandler = new DatabaseHandler(SplashActivity.this.getApplicationContext());
		final Cursor csr = databaseHandler.getDetails();

		try {
			if (csr.getCount() > 0) {
				csr.moveToFirst();
				Constants.USER_TYPE = csr.getString(csr.getColumnIndex(DatabaseHandler.KEY_usertype));
				if (!(Constants.USER_TYPE.equals("D") || Constants.USER_TYPE.equals("U"))) {
					new MenuSetting().loadMenu(databaseHandler);
				}
			}
		}catch (Exception e){
			e.printStackTrace();
			showAlertDialogToast("Menu setting failed App should re-open");
			return;
		}

		CountDownTimer ct = new CountDownTimer(4000, 2000) {

			@Override
			public void onTick(long millisUntilFinished) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onFinish() {
				// Validation for intro screen...
				if (csr.getCount() > 0) {
					csr.moveToFirst();
					Constants.USER_TYPE = csr.getString(csr
							.getColumnIndex(DatabaseHandler.KEY_usertype));
					Constants.USER_ID = csr.getString(csr
							.getColumnIndex(DatabaseHandler.KEY_id));
					Constants.USER_NAME = csr.getString(csr
							.getColumnIndex(DatabaseHandler.KEY_name));
					Constants.PAYMENT = csr.getString(csr
							.getColumnIndex(DatabaseHandler.KEY_payment));

					dUserId = csr.getString(csr
							.getColumnIndex(DatabaseHandler.KEY_clientdealid));

					Log.e("Insertion Check", csr.getString(csr
							.getColumnIndex(DatabaseHandler.KEY_username)));
					Log.e("Insertion Check", csr.getString(csr
							.getColumnIndex(DatabaseHandler.KEY_password)));
					Log.e("Insertion Check", csr.getString(csr
							.getColumnIndex(DatabaseHandler.KEY_usertype)));
					Log.e("Insertion Check", csr.getString(csr
							.getColumnIndex(DatabaseHandler.KEY_name)));


					try {
						Cursor curTemplate = databaseHandler.getLastTemplateType();
						if(curTemplate != null && curTemplate.getCount() > 0) {
							curTemplate.moveToFirst();
							String templateNo = curTemplate.getString(curTemplate.getColumnIndex(KEY_TEMPLATE_VALUE));
							TemplateSetting.templateSetting(templateNo);
						}else {
							TemplateSetting.templateSetting(Constants.DEFAULT_TEMPLATE_NO);
						}
					}catch (Exception e){
						e.printStackTrace();
					}
			/*		Intent intent = new Intent(SplashActivity.this,
							DealersOrderActivity.class);

					startActivity(intent);
					overridePendingTransition(R.anim.slide_in_left,
							R.anim.slide_out_left);	*/
					if (Constants.USER_NAME.equals("null")) {
						Intent intent = new Intent(SplashActivity.this,
								ProfileActivityInitial.class);

						startActivity(intent);
						overridePendingTransition(R.anim.slide_in_left,
								R.anim.slide_out_left);
						finish();
					} else {
						firstLine.append("UserName : "); firstLine.append(Constants.USER_NAME);
						firstLine.append("\tUserID : "); firstLine.append(Constants.USER_ID);
						firstLine.append("\tUserType : "); firstLine.append(Constants.USER_TYPE);
						firstLine.append("\tKEY_username : "); firstLine.append(csr.getString(csr.getColumnIndex(DatabaseHandler.KEY_username)));
						firstLine.append("\tKEY_password : "); firstLine.append(csr.getString(csr.getColumnIndex(DatabaseHandler.KEY_password)));
						firstLine.append("\n");
						AppCrashConstants.USER_DETAILS = firstLine.toString();

						AppCrashUtil.initialCrashCustomerInfo(AppCrashConstants.USER_DETAILS);

						if (Constants.USER_TYPE.equals("D") || Constants.USER_TYPE.equals("U")) {
							if(Constants.USER_TYPE.equals("U")){
								Constants.DUSER_ID = dUserId;
							}else {
								Constants.DUSER_ID = Constants.USER_ID;
							}
							//Intent intent = new Intent(SplashActivity.this, DealersOrderActivity.class);

							Intent intent = new Intent(SplashActivity.this, ManagerActivity.class);

							startActivity(intent);
							overridePendingTransition(R.anim.slide_in_left,
									R.anim.slide_out_left);
							finish();
						} else  if(Constants.USER_TYPE.equals("M")){
							Intent intent = new Intent(SplashActivity.this,
									MerchantOrderActivity.class);

							startActivity(intent);
							overridePendingTransition(R.anim.slide_in_left,
									R.anim.slide_out_left);
							finish();
						}else {
							///////////////////////27-09-2019 vel
							startAndStopTrackingService();
							////////////////////////////////////
							sendingFCMTokenToServer(); ////26-11-2019
							Intent intent = new Intent(SplashActivity.this,
									SalesManOrderActivity.class);

							startActivity(intent);
							overridePendingTransition(R.anim.slide_in_left,
									R.anim.slide_out_left);
							finish();
						}
					}

				} else {

					Intent intent = new Intent(SplashActivity.this,
							SigninActivity.class);
					SplashActivity.this.startActivity(intent);
					overridePendingTransition(R.anim.slide_in_left,
							R.anim.slide_out_left);
					finish();

				}
			}
		};
		ct.start();
	}

	private void sendingFCMTokenToServer(){

        if(isTrackMenuFound()) {
            PrefManager pre = new PrefManager(SplashActivity.this);
            boolean isDeliveredFCMToken = pre.getBooleanDataByKey(IS_DELIVERED_FCM_TOKEN);
            String token = pre.getStringDataByKey(FCM_TOKEN);
            if(!isDeliveredFCMToken){
                scheduleFCMWorker();
            }
        }else {
            Log.e(TAG, "Wont Tracking ...User userId: " + Constants.USER_ID);
        }
	}

	private void scheduleFCMWorker(){
        Constraints constraints = new Constraints.Builder().setRequiredNetworkType
                (NetworkType.CONNECTED).build();

        /*Data inputData = new Data.Builder()
                .putDouble(LocationUploadWorker.LOCATION_LAT, location.getLatitude())
                .putDouble(LocationUploadWorker.LOCATION_LONG, location.getLongitude())
                .putLong(LocationUploadWorker.LOCATION_TIME, location.getTime())
                .build(); */

        //worker itself
        OneTimeWorkRequest uploadWork = new OneTimeWorkRequest.Builder
                (FCMTokenUploadWorker.class).setConstraints(constraints)/*.setInputData
                (inputData) */.build();
        //Schedule it
        WorkManager.getInstance().enqueue(uploadWork);
    }

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(1);
		setContentView(R.layout.splash_screen);
		mContext = this; //static context
		// Kumaravel  resetting Migration check flag
		new PrefManager(this.getApplicationContext()).setBooleanDataByKey(PrefManager.KEY_IS_MIGRATION_SET, false);
		Animation animation = AnimationUtils.loadAnimation(
				getApplicationContext(), R.anim.clk);
	//	ImageView image1 = (ImageView) findViewById(R.id.imageView1);
	//	image1.startAnimation(animation);

	//	ImageView image2 = (ImageView) findViewById(R.id.imageView2);
	//	image2.startAnimation(animation);
		Constants.Merchantname="";
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			if(checkPermissions()){
				startNextActivity();
			}
		}else {
			startNextActivity();
		}

	}
	public void showAlertDialogToast( String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
		builder1.setCancelable(true);
		builder1.setMessage(message);
		builder1.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();

		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

	}

	private void startAndStopTrackingService(){


			final long delay;
			final long delayStop;

			Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(System.currentTimeMillis());
			calendar.set(Calendar.HOUR_OF_DAY, SELF_START_TIME_LIVE_TRACK);

			AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

			Intent intent = new Intent(this, StartTrackingBroadCast.class);
			PendingIntent pendingIntent = PendingIntent.getBroadcast(this.getApplicationContext(), BR_REQUEST_CODE_START_LIVE_TRACK, intent,
					PendingIntent.FLAG_UPDATE_CURRENT);

			delay = AlarmManager.INTERVAL_DAY;
		/*
		if (DateTime.now().getHourOfDay() < SELF_START_TIME_LIVE_TRACK) {
			delay = new Duration(DateTime.now() , DateTime.now().withTimeAtStartOfDay().plusHours(SELF_START_TIME_LIVE_TRACK)).getStandardMinutes();
		} else {
			delay = new Duration(DateTime.now() , DateTime.now().withTimeAtStartOfDay().plusDays(1).plusHours(SELF_START_TIME_LIVE_TRACK)).getStandardMinutes();
		} */

			alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), delay, pendingIntent);  //AlarmManager.INTERVAL_FIFTEEN_MINUTES

			/////////////////////////////////////////////////////
			Intent intentStop = new Intent(this, StopTrackingBroadCast.class);
			PendingIntent piStop = PendingIntent.getBroadcast(this.getApplicationContext(), BR_REQUEST_CODE_STOP_LIVE_TRACK, intentStop,
					PendingIntent.FLAG_UPDATE_CURRENT);
			delayStop = AlarmManager.INTERVAL_DAY;
		/*
		if (DateTime.now().getHourOfDay() < SELF_START_TIME_LIVE_TRACK) {
			delayStop = new Duration(DateTime.now() , DateTime.now().withTimeAtStartOfDay().plusHours(SELF_START_TIME_LIVE_TRACK)).getStandardMinutes();
		} else {
			delayStop = new Duration(DateTime.now() , DateTime.now().withTimeAtStartOfDay().plusDays(1).plusHours(SELF_START_TIME_LIVE_TRACK)).getStandardMinutes();
		} */

			AlarmManager alarmManager1 = (AlarmManager) getSystemService(ALARM_SERVICE);
			Calendar calendar1 = Calendar.getInstance();
			calendar1.setTimeInMillis(System.currentTimeMillis());
			calendar1.set(Calendar.HOUR_OF_DAY, SELF_STOP_TIME_LIVE_TRACK);


			alarmManager1.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar1.getTimeInMillis(), delayStop, piStop);  //AlarmManager.INTERVAL_FIFTEEN_MINUTES

			//startTrackingOnAppStart();

	}

	private void startTrackingOnAppStart(){

			if (isTrackMenuFound()) {
				if (!PrefManager.getStatus(mContext)) {
					if(isTimeLiesBetween(SELF_START_TIME_LIVE_TRACK, SELF_STOP_TIME_LIVE_TRACK)) {
						try {
							PrefManager.setStatus(mContext, true);
							if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
								mContext.startForegroundService(new Intent(mContext, LocationTrackForegroundService.class));
							} else {
								mContext.startService(new Intent(mContext, LocationTrackForegroundService.class));
							}
						} catch (Exception e) {
							e.printStackTrace();
							new com.freshorders.freshorder.utils.Log(mContext).ee(TAG, "...Error Produced in Auto Start Tracking........" + e.getMessage());
						}
					}else {
						new com.freshorders.freshorder.utils.Log(mContext).ee(TAG, "...Time not between  9 am to 7 pm......to start tracking..");
					}
				} else {
					new com.freshorders.freshorder.utils.Log(mContext).ee(TAG, "...Already Tracking Started........");
				}
			} else {
				new com.freshorders.freshorder.utils.Log(mContext).ee(TAG, "...Tracking Menu Not Found........");
			}
	}

	private boolean isTrackMenuFound(){
		DatabaseHandler db = new DatabaseHandler(mContext);
		Cursor menuCur = db.getMenuItems();
		if(menuCur != null && menuCur.getCount() > 0){
			menuCur.moveToFirst();
			for(int i = 0; i < menuCur.getCount(); i++) {
				int currentVisibility = menuCur.getInt(menuCur.getColumnIndex(DatabaseHandler.KEY_MENU_IS_VISIBLE));
				String currentMenuCode = menuCur.getString(menuCur.getColumnIndex(DatabaseHandler.KEY_MENU_CODE));
				if (MENU_DISTANCE_CALCULATION_CODE.equalsIgnoreCase(currentMenuCode)) {
					if (currentVisibility == 1) {
						return true;
					}
				}
				menuCur.moveToNext();
			}
		}
		return false;
	}

	private boolean isTimeLiesBetween(int startTime , int endTime){

		DateTime dateTime = new DateTime();
		if( !(dateTime.getDayOfWeek() == 7/* Sunday*/) ) {
			DateTime todayStart = dateTime.withTimeAtStartOfDay();
			DateTime start = todayStart.plusHours(startTime);
			DateTime end = todayStart.plusHours(endTime);
			return dateTime.isAfter(start) && dateTime.isBefore(end);
		}else {
			return false;
		}
	}
}
