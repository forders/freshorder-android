package com.freshorders.freshorder;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;

import androidx.multidex.MultiDex;
import android.widget.Toast;

import com.freshorders.freshorder.crashreport.AppCrashReporter;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.MerchantOrderDomainSelected;
import com.freshorders.freshorder.receiver.NetworkConnectReceiver;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.Log;
import com.freshorders.freshorder.utils.MenuSetting;
import com.freshorders.freshorder.utils.TemplateSetting;
import com.google.gson.Gson;
/*
import com.simplymadeapps.quickperiodicjobscheduler.PeriodicJob;
import com.simplymadeapps.quickperiodicjobscheduler.QuickJobFinishedCallback;
import com.simplymadeapps.quickperiodicjobscheduler.QuickPeriodicJob;
import com.simplymadeapps.quickperiodicjobscheduler.QuickPeriodicJobCollection;
import com.simplymadeapps.quickperiodicjobscheduler.QuickPeriodicJobScheduler;  */

import org.joda.time.DateTime;
import org.joda.time.Duration;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import io.reactivex.disposables.Disposable;

import static com.freshorders.freshorder.db.DatabaseHandler.KEY_TEMPLATE_VALUE;

public class MyApplication extends Application {

    private static MyApplication mInstance;
    private String crashReporterPath = Constants.EMPTY;
    private String MigPath = Constants.EMPTY;
    private DatabaseHandler databaseHandler;


    ////////////////////////////////////////////////////////////////////////////////////////////
    // flag for GPS status
    boolean isGPSEnabled = false;
    public static int minutes =1;
    // flag for GPS status
    public static boolean canGetLocation = false;

    int notiID =1001;
    int accuracy = 500;

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 5; // 10 meters
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 30 * 1; // 1 minute
    // Declaring a Location Manager
    protected LocationManager locationManager;
    //  private FusedLocationProviderClient fusedLocationClient;
    public static String NOTIFICATION_CHANNEL_ID = "Channel_location1";
    public static Disposable disposable;

    ///////////////////////////////////////////////////////////////////////////////////////////

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        Log.setEnableDebugLog(true);
        new PrefManager(mInstance).setBooleanDataByKey(PrefManager.KEY_IS_FULL_MIGRATION_SET, false);///need to reset
        //For Crash Report
        //AppCrashUtil.getInitialDefaultPath(mInstance);
        ////String crashReporterPath = Environment.getExternalStorageDirectory() + File.separator + AppCrashConstants.CRASH_REPORT_DIR;
        android.util.Log.e("CrashPath","...............path :" + crashReporterPath);
        AppCrashReporter.initialize(this, crashReporterPath);

        loadInitialSetting(getApplicationContext());/////////////
    }

    //Kumaravel 21-07-2019

    private void loadInitialSetting(Context context){
        DatabaseHandler databaseHandler = new DatabaseHandler(context);
        try {
            Cursor curTemplate = databaseHandler.getLastTemplateType();
            if(curTemplate != null && curTemplate.getCount() > 0) {
                curTemplate.moveToFirst();
                String templateNo = curTemplate.getString(curTemplate.getColumnIndex(KEY_TEMPLATE_VALUE));
                TemplateSetting.templateSetting(templateNo);
            }else {
                TemplateSetting.templateSetting(Constants.DEFAULT_TEMPLATE_NO);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        try {
            Cursor csr = databaseHandler.getDetails();
            if (csr.getCount() > 0) {
                csr.moveToFirst();
                Constants.USER_TYPE = csr.getString(csr.getColumnIndex(DatabaseHandler.KEY_usertype));
                if (!Constants.USER_TYPE.equals("D")) {
                    new MenuSetting().loadMenu(databaseHandler);
                }
                csr.close();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    /// Added By Kumaravel 14-12-2018
    private boolean template1 = false;
    private boolean template2 = false;
    private boolean template3 = false;
    private boolean template4 = false;
    private boolean template5 = false;
    private boolean template6 = false;  /// for milk
    private boolean template7 = false;
    private boolean template8 = false;

    private boolean template9 = false;
    private boolean template10 = false;

    private boolean isTemplate8ForTemplate2 = false;

    private boolean template1PairMissing = false;


    public static Map<Integer, MerchantOrderDomainSelected> previousSelected = new LinkedHashMap<>();

    public static Map<String, MerchantOrderDomainSelected> previousSelectedForSearch = new LinkedHashMap<>();

    public static Map<String, Integer> productIdPositionMap = new LinkedHashMap<>();

    private String errorMSg = "";


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    public void setConnectivityListener(NetworkConnectReceiver.ConnectivityReceiverListener listener) {
        NetworkConnectReceiver.connectivityReceiverListener = listener;
    }

    // Gloabl declaration of variable to use in whole app

    public static boolean activityVisible; // Variable that will check the
    // current activity state

    public static boolean isActivityVisible() {
        return activityVisible; // return true or false
    }

    public static void activityResumed() {
        activityVisible = true;// this will set true when activity resumed

    }

    public static void activityPaused() {
        activityVisible = false;// this will set false when activity paused

    }

    public boolean isTemplate1() {
        return template1;
    }

    public void setTemplate1(boolean template1) {
        this.template1 = template1;
    }

    public boolean isTemplate2() {
        return template2;
    }

    public void setTemplate2(boolean template2) {
        this.template2 = template2;
    }

    public boolean isTemplate3() {
        return template3;
    }

    public void setTemplate3(boolean template3) {
        this.template3 = template3;
    }

    public boolean isTemplate4() {
        return template4;
    }

    public void setTemplate4(boolean template4) {
        this.template4 = template4;
    }

    public boolean isTemplate5() {
        return template5;
    }

    public void setTemplate5(boolean template5) {
        this.template5 = template5;
    }

    public boolean isTemplate1PairMissing() {
        return template1PairMissing;
    }

    public void setTemplate1PairMissing(boolean template1PairMissing) {
        this.template1PairMissing = template1PairMissing;
    }

    public String getErrorMSg() {
        return errorMSg;
    }

    public void setErrorMSg(String errorMSg) {
        this.errorMSg = errorMSg;
    }

    public boolean isTemplate6() {
        return template6;
    }

    public void setTemplate6(boolean template6) {
        this.template6 = template6;
    }

    public boolean isTemplate7() {
        return template7;
    }

    public void setTemplate7(boolean template7) {
        this.template7 = template7;
    }

    public boolean isTemplate8() {
        return template8;
    }

    public void setTemplate8(boolean template8) {
        this.template8 = template8;
    }

    public boolean isTemplate9() {
        return template9;
    }

    public void setTemplate9(boolean template9) {
        this.template9 = template9;
    }

    public boolean isTemplate10() {
        return template10;
    }

    public void setTemplate10(boolean template10) {
        this.template10 = template10;
    }


    public boolean isTemplate8ForTemplate2() {
        return isTemplate8ForTemplate2;
    }

    public void setTemplate8ForTemplate2(boolean template8ForTemplate2) {
        isTemplate8ForTemplate2 = template8ForTemplate2;
    }

    public String getCrashReporterPath() {
        return crashReporterPath;
    }

    public void setCrashReporterPath(String crashReporterPath) {
        this.crashReporterPath = crashReporterPath;
    }

    public String getMigPath() {
        return MigPath;
    }

    public void setMigPath(String migPath) {
        MigPath = migPath;
    }



    public String formatDateTime(java.util.Date date) {
        SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault());
        if (date == null) {
            return "";
        } else {
            return DATE_TIME_FORMAT.format(date);
        }
    }

    public static void showSettingsAlert(Context context){
        Toast.makeText(context,"GPS IS DISABLE",Toast.LENGTH_LONG).show();
    }

    public interface ActivityLifecycleCallbacks {
        void onActivityCreated(Activity activity, Bundle savedInstanceState);
        void onActivityStarted(Activity activity);
        void onActivityResumed(Activity activity);
        void onActivityPaused(Activity activity);
        void onActivityStopped(Activity activity);
        void onActivitySaveInstanceState(Activity activity, Bundle outState);
        void onActivityDestroyed(Activity activity);
    }


    private boolean profile = false;
    private boolean myDayPlan = false;
    private boolean myOrders = false;
    private boolean addMerchant = false;

    private boolean mySales = false;
    private boolean postNotes = false;
    private boolean myClientVisit = false;
    private boolean acknowledge = false;

    private boolean paymentCollection = false;
    private boolean pkdDataCapture = false;
    private boolean distributorStock = false;
    private boolean surveyUser = false;

    private boolean distanceCalculation = false;
    private boolean refresh = false;
    private boolean logout = false;
    private boolean closingStock = false;

    private boolean closingStockAudit = false;
    private boolean pendingData = false;
    private boolean downLoadScheme = false;
    private boolean createOrder = false;

    public boolean isCreateOrder() {
        return createOrder;
    }

    public void setCreateOrder(boolean createOrder) {
        this.createOrder = createOrder;
    }


    public boolean isProfile() {
        return profile;
    }

    public void setProfile(boolean profile) {
        this.profile = profile;
    }

    public boolean isMyDayPlan() {
        return myDayPlan;
    }

    public void setMyDayPlan(boolean myDayPlan) {
        this.myDayPlan = myDayPlan;
    }

    public boolean isMyOrders() {
        return myOrders;
    }

    public void setMyOrders(boolean myOrders) {
        this.myOrders = myOrders;
    }

    public boolean isAddMerchant() {
        return addMerchant;
    }

    public void setAddMerchant(boolean addMerchant) {
        this.addMerchant = addMerchant;
    }

    public boolean isMySales() {
        return mySales;
    }

    public void setMySales(boolean mySales) {
        this.mySales = mySales;
    }

    public boolean isPostNotes() {
        return postNotes;
    }

    public void setPostNotes(boolean postNotes) {
        this.postNotes = postNotes;
    }

    public boolean isMyClientVisit() {
        return myClientVisit;
    }

    public void setMyClientVisit(boolean myClientVisit) {
        this.myClientVisit = myClientVisit;
    }

    public boolean isAcknowledge() {
        return acknowledge;
    }

    public void setAcknowledge(boolean acknowledge) {
        this.acknowledge = acknowledge;
    }

    public boolean isPaymentCollection() {
        return paymentCollection;
    }

    public void setPaymentCollection(boolean paymentCollection) {
        this.paymentCollection = paymentCollection;
    }

    public boolean isPkdDataCapture() {
        return pkdDataCapture;
    }

    public void setPkdDataCapture(boolean pkdDataCapture) {
        this.pkdDataCapture = pkdDataCapture;
    }

    public boolean isDistributorStock() {
        return distributorStock;
    }

    public void setDistributorStock(boolean distributorStock) {
        this.distributorStock = distributorStock;
    }

    public boolean isSurveyUser() {
        return surveyUser;
    }

    public void setSurveyUser(boolean surveyUser) {
        this.surveyUser = surveyUser;
    }

    public boolean isDistanceCalculation() {
        return distanceCalculation;
    }

    public void setDistanceCalculation(boolean distanceCalculation) {
        this.distanceCalculation = distanceCalculation;
    }

    public boolean isRefresh() {
        return refresh;
    }

    public void setRefresh(boolean refresh) {
        this.refresh = refresh;
    }

    public boolean isLogout() {
        return logout;
    }

    public void setLogout(boolean logout) {
        this.logout = logout;
    }

    public boolean isClosingStock() {
        return closingStock;
    }

    public void setClosingStock(boolean closingStock) {
        this.closingStock = closingStock;
    }

    public boolean isClosingStockAudit() {
        return closingStockAudit;
    }

    public void setClosingStockAudit(boolean closingStockAudit) {
        this.closingStockAudit = closingStockAudit;
    }

    public boolean isPendingData() {
        return pendingData;
    }

    public void setPendingData(boolean pendingData) {
        this.pendingData = pendingData;
    }

    public boolean isDownLoadScheme() {
        return downLoadScheme;
    }

    public void setDownLoadScheme(boolean downLoadScheme) {
        this.downLoadScheme = downLoadScheme;
    }
}

