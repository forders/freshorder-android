package com.freshorders.freshorder.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.MerchantOrderDetailDomain;
import com.freshorders.freshorder.model.DistributorStockListModel;
import com.freshorders.freshorder.model.DistributorStockModel;
import com.freshorders.freshorder.ui.DistributrStockActivity;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class DistributrStockDetailAdapter extends ArrayAdapter<DistributorStockListModel> {
	Context context;
	int resource;
	ArrayList<MerchantOrderDetailDomain> arraylistMerchantOrderDetailList;
	ArrayList<MerchantOrderDetailDomain>arrayListSearchResults;
	public static ArrayList<MerchantOrderDetailDomain> arraylistMerchantOrderDetailListSelected, arraylistsavearray;
	JSONObject JsonAccountObject = null;
	JSONArray JsonAccountArray = null;
	JsonServiceHandler JsonServiceHandler;
	String buyQty = "", soldQty = "", closeStock = "";
	public static String prodid,selecteddeliverymode="",monthLength="",dayLength="";

	public static EditText editTextSearchField;
	public static TextView textViewHeader, textQtyValidate;
	public static MerchantOrderDetailDomain dod;
	DatabaseHandler databaseHandler;

	//Kumaravel 06/02/2019
	public List<DistributorStockListModel> stockList;
	private Map<Integer, DistributorStockModel> merchantStockList ;
	private Map<Integer, String> purchaseList;
	private Map<Integer, String> salesList;
	private Map<Integer, String> openingStockList;
	private Map<Integer, String> closingStockList;

	public DistributrStockDetailAdapter(
			Context context,
			int resource,
			List<DistributorStockListModel> stockList) {
				super(context, resource, stockList);

				this.context = context;
				this.resource = resource;
				this.stockList = stockList;
				JsonAccountObject = new JSONObject();
				JsonAccountArray = new JSONArray();
				databaseHandler = new DatabaseHandler(context);

		merchantStockList = new LinkedHashMap<>(stockList.size());
		purchaseList = new HashMap<>(stockList.size());
		salesList = new HashMap<>(stockList.size());
		openingStockList = new HashMap<>(stockList.size());
		closingStockList = new HashMap<>(stockList.size());
	}

	public Map<Integer, DistributorStockModel> getSelectedStockList(){
	    return merchantStockList;
    }

	@Override
	public int getCount() {
		return stockList.size();
	}

	@Override
	public boolean isEnabled(int position) {
		return true;
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// Log.e("Adapter position",String.valueOf(position));
		View row = convertView;
		final HomeHolder2 homeHolder;

		if (row == null) {
			// Log.e("Adapter Inner position",String.valueOf(position));
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(resource, parent, false);
			homeHolder = new HomeHolder2();
			homeHolder.textProdName = (TextView) row
					.findViewById(R.id.textProdName);
			homeHolder.editQtyRecd = (EditText) row
					.findViewById(R.id.editQtyRecd);
			homeHolder.textOpenStk = (TextView) row
					.findViewById(R.id.textOpenStk);
			homeHolder.textCloseStk = (TextView) row
					.findViewById(R.id.textCloseStk);
			homeHolder.serialno = (TextView) row
					.findViewById(R.id.serialno);
			homeHolder.editQtySold = (EditText) row
					.findViewById(R.id.editQtySold);

			homeHolder.editQtyRecd.addTextChangedListener(new TextWatcher() {

			  @Override
				public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

				}

				@Override
				public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

				}

				@Override
				public void afterTextChanged(Editable editable) {
					String currentQyt = editable.toString();
					int position = (int) homeHolder.editQtyRecd.getTag();
					if(!currentQyt.isEmpty()){
						purchaseList.put(position, currentQyt);
						DistributorStockListModel item = stockList.get(position);
						if(!merchantStockList.containsKey(position)) {
							DistributorStockModel model = new DistributorStockModel();
							model.setItemUOM(item.getUomId());
							model.setProdId(item.getProdId());
							model.setPurchaseQty(currentQyt);
							model.setSalesQty("");
							merchantStockList.put(position, model);
							if (item.getClosing().isEmpty())
								closeStock = Integer.toString (Integer.parseInt(currentQyt));
							else
								closeStock = Integer.toString (Integer.parseInt(item.getClosing()) + Integer.parseInt(currentQyt));
							String closing = context.getResources().getString(R.string.closing) + closeStock;
							homeHolder.textCloseStk.setText(closing);
							closingStockList.put(position, closing);
						}else {
							DistributorStockModel model = merchantStockList.get(position);
							model.setPurchaseQty(currentQyt);
							merchantStockList.put(position, model);
							if(!model.getSalesQty().isEmpty()) {
								if (item.getClosing().isEmpty()){
									closeStock = String.valueOf(Integer.parseInt(currentQyt) - Integer.parseInt(model.getSalesQty()));
								}else {
									closeStock = Integer.toString(Integer.parseInt(item.getClosing()) +
											Integer.parseInt(currentQyt) - Integer.parseInt(model.getSalesQty()));
								}
							}else {
								if (item.getClosing().isEmpty())
									closeStock = Integer.toString (Integer.parseInt(currentQyt));
								else
									closeStock = Integer.toString (Integer.parseInt(item.getClosing()) + Integer.parseInt(currentQyt));
							}
							String closing = context.getResources().getString(R.string.closing) + closeStock;
							homeHolder.textCloseStk.setText(closing);
							closingStockList.put(position, closing);
						}
					}else {
						purchaseList.put(position, Constants.EMPTY);
						if(merchantStockList.get(position) != null) {
							DistributorStockListModel item = stockList.get(position);
							DistributorStockModel model = merchantStockList.get(position);
							if(model.getSalesQty().isEmpty()) {
								merchantStockList.remove(position);
								closeStock = item.getClosing();
							}else {
								model.setPurchaseQty("");
								merchantStockList.put(position, model);
								if(item.getClosing().isEmpty())
									closeStock = Integer.toString (0 - Integer.parseInt(model.getSalesQty()));
								else
									closeStock = Integer.toString (Integer.parseInt(item.getClosing()) - Integer.parseInt(model.getSalesQty()));
							}
							String closing = context.getResources().getString(R.string.closing) + closeStock;
							homeHolder.textCloseStk.setText(closing);
							closingStockList.put(position, closing);
						}
					}
				}
		 	});

			homeHolder.editQtySold.addTextChangedListener(new TextWatcher() {

				@Override
				public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

				}

				@Override
				public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

				}

				@Override
				public void afterTextChanged(Editable editable) {
					String currentQyt = editable.toString();
					int position = (int) homeHolder.editQtySold.getTag();
					if(!currentQyt.isEmpty()){
						salesList.put(position, currentQyt);
						DistributorStockListModel item = stockList.get(position);
						if(!merchantStockList.containsKey(position)) {
							DistributorStockModel model = new DistributorStockModel();
							model.setItemUOM(item.getUomId());
							model.setProdId(item.getProdId());
							model.setPurchaseQty("");
							model.setSalesQty(currentQyt);
							merchantStockList.put(position, model);
							if (item.getClosing().isEmpty())
								closeStock = Integer.toString (0 - Integer.parseInt(currentQyt));
							else
								closeStock = Integer.toString (Integer.parseInt(item.getClosing()) - Integer.parseInt(currentQyt));
							String closing = context.getResources().getString(R.string.closing) + closeStock;
							homeHolder.textCloseStk.setText(closing);
							closingStockList.put(position, closing);
						}else {
							DistributorStockModel model = merchantStockList.get(position);
							model.setSalesQty(currentQyt);
							merchantStockList.put(position, model);
							if(!model.getPurchaseQty().isEmpty()) {
								if (item.getClosing().isEmpty()){
									closeStock = String.valueOf(Integer.parseInt(model.getPurchaseQty()) - Integer.parseInt(currentQyt));
								}else {
									closeStock = Integer.toString(Integer.parseInt(item.getClosing()) +
											Integer.parseInt(model.getPurchaseQty()) - Integer.parseInt(currentQyt));
								}
							}else {
								if (item.getClosing().isEmpty())
									closeStock = Integer.toString (Integer.parseInt(currentQyt));
								else
									closeStock = Integer.toString (Integer.parseInt(item.getClosing()) + Integer.parseInt(currentQyt));
							}
							String closing = context.getResources().getString(R.string.closing) + closeStock;
							homeHolder.textCloseStk.setText(closing);
							closingStockList.put(position, closing);
						}
					}else {
						salesList.put(position, Constants.EMPTY);
						if(merchantStockList.get(position) != null) {
							DistributorStockListModel item = stockList.get(position);
							DistributorStockModel model = merchantStockList.get(position);
							if(model.getPurchaseQty().isEmpty()) {
								merchantStockList.remove(position);
								closeStock = item.getClosing();
							}else {
								model.setSalesQty("");
								merchantStockList.put(position, model);
								if(item.getClosing().isEmpty())
									closeStock = Integer.toString (Integer.parseInt(model.getPurchaseQty()));
								else
									closeStock = Integer.toString (Integer.parseInt(item.getClosing()) + Integer.parseInt(model.getPurchaseQty()));
							}
							String closing = context.getResources().getString(R.string.closing) + closeStock;
							homeHolder.textCloseStk.setText(closing);
							closingStockList.put(position, closing);
						}
					}
				}
			});


			row.setTag(homeHolder);
		} else {
			homeHolder = (HomeHolder2) row.getTag();
		}
		final DistributorStockListModel item = stockList
				.get(position);

		homeHolder.textProdName.setText(Html.fromHtml(item.getProdName() + "  ["
				+ item.getProdCode() + "]"));
		String opening = context.getResources().getString(R.string.opening) + item.getOpening();
		String serialNo = item.getSerialNo() + Constants.DOT;
		homeHolder.textOpenStk.setText(opening);

		homeHolder.serialno.setText(serialNo);

		homeHolder.editQtyRecd.setTag(position);
		homeHolder.editQtySold.setTag(position);
		homeHolder.textOpenStk.setTag(position);
		homeHolder.textCloseStk.setTag(position);

		if(closingStockList.containsKey(position)){
			String closing = context.getResources().getString(R.string.closing) + closingStockList.get(position);
			homeHolder.textCloseStk.setText(closing);
			Log.e("closing","IFIFIF............"+closing);
		}else {
			String closing = context.getResources().getString(R.string.closing) + item.getClosing();
			homeHolder.textCloseStk.setText(closing);
			Log.e("closing","first else............"+closing);
		}

		if(purchaseList.containsKey(position)){
			homeHolder.editQtyRecd.setText(purchaseList.get(position));
		}else {
			homeHolder.editQtyRecd.setText("");
		}

		if(salesList.containsKey(position)){
			homeHolder.editQtySold.setText(salesList.get(position));
		}else {
			homeHolder.editQtySold.setText("");
		}

		/*if(!item.getprodprice().equals("")){
			homeHolder.editQtyRecd.setText(item.getprodprice());
		}else{
			homeHolder.editQtyRecd.setText("0");
		}
		if(!item.getprodtax().equals("")){
			homeHolder.editQtySold.setText(item.getprodtax());
		}else{
			homeHolder.editQtySold.setText("0");
		}

		homeHolder.editQtyRecd.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.e("StkDtlAda","setOnClickListener ");

				final Dialog qtyDialog = new Dialog(context);
				qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				qtyDialog.setContentView(R.layout.dist_stock_entry_dialog);

				final Button buttonUpdateOk, buttonUpdateCancel;
				InputMethodManager iss = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
				iss.hideSoftInputFromWindow(homeHolder.editQtyRecd.getWindowToken(), 0);
				InputMethodManager inn = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
				inn.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
				arrayListSearchResults = new ArrayList<MerchantOrderDetailDomain>();

				final EditText editTextBuyQty,editTextSellQty;

                buttonUpdateOk = (Button) qtyDialog
						.findViewById(R.id.buttonUpdateOk);
				buttonUpdateCancel = (Button) qtyDialog
						.findViewById(R.id.buttonUpdateCancel);

				editTextBuyQty = (EditText) qtyDialog
						.findViewById(R.id.editTextBuyQty);
				editTextSellQty= (EditText) qtyDialog
						.findViewById(R.id.editTextSellQty);

				textQtyValidate= (TextView) qtyDialog
						.findViewById(R.id.textQtyValidate);

				editTextBuyQty.setSelection(editTextSellQty.getText().length());
				editTextSellQty.setSelection(editTextSellQty.getText().length());

				qtyDialog.show();
				buttonUpdateOk.setOnClickListener(new OnClickListener() {

													  @Override
													  public void onClick(View v) {
														  dod= new MerchantOrderDetailDomain();
														  arraylistMerchantOrderDetailListSelected = new ArrayList<MerchantOrderDetailDomain>();

														  buyQty = editTextBuyQty.getText().toString();

                                                          Log.e( "Stock buyQty: ",buyQty );
														  soldQty = editTextSellQty.getText().toString();

														  Log.e( "Stock soldQty: ",soldQty +", "+closeStock);

														  if (buyQty.isEmpty() || soldQty.isEmpty()) {
															  textQtyValidate.setText("Please enter Purchase and Sale quantity");
														  } else {
														  	if (item.getFreeQty().isEmpty())
																closeStock = Integer.toString (Integer.parseInt(buyQty) - Integer.parseInt(soldQty));
															else
																closeStock = Integer.toString (Integer.parseInt(item.getFreeQty()) + Integer.parseInt(buyQty) - Integer.parseInt(soldQty));

															item.setprodprice(buyQty);
															item.setprodtax(soldQty);
															dod.setprodcode(item.getprodcode());
															dod.setprodid(item.getprodid());
															dod.setFreeQty(item.getFreeQty()); // closing
															dod.setqty(item.getqty()); // opening
															dod.setprodprice(buyQty); // Purchase
															dod.setprodtax(soldQty); // Sales
															dod.setUOM(item.getUOM());

															DistributrStockActivity.arraylistMerchantOrderDetailListSelected.add(dod);

															homeHolder.editQtyRecd.setText(buyQty);
															homeHolder.editQtySold.setText(soldQty);
															homeHolder.textCloseStk.setText("Closing: "+closeStock);

															qtyDialog.dismiss();
														  }

													  }
												  }

				);

				buttonUpdateCancel
						.setOnClickListener(new OnClickListener()

											{

												@Override
												public void onClick(View v) {
													// TODO Auto-generated method stub

													qtyDialog.dismiss();
												}


											}

						);
			}
		});  */

		return row;
	}



	class HomeHolder2 {
		RelativeLayout relativeLayoutAddQuantity;
		TextView textProdName, textOpenStk,serialno,textCloseStk;
		EditText editQtyRecd;
		EditText editQtySold;

	}

}