package com.freshorders.freshorder.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.domain.OutletStockEntryDetailDomain;
import com.freshorders.freshorder.domain.OutletStockEntryDomain;
import com.freshorders.freshorder.utils.JsonServiceHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Jayesh on 16/05/2017.
 */

public class OutletStockViewAdapter extends ArrayAdapter<OutletStockEntryDetailDomain> {

    Context context;
    int resource;
    ArrayList<OutletStockEntryDetailDomain> arrayListServiceDomainObjects;

    JSONObject JsonAccountObject = null;
    JSONArray JsonAccountArray = null;
    JsonServiceHandler JsonServiceHandler;


    public OutletStockViewAdapter(Context context, int resource,
                                  ArrayList<OutletStockEntryDetailDomain> arrayListServiceDomainObjects) {
        super(context, resource, arrayListServiceDomainObjects);
        this.context = context;
        this.resource = resource;
        this.arrayListServiceDomainObjects = arrayListServiceDomainObjects;
        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();
    }

    @Override
    public int getCount() {
        return arrayListServiceDomainObjects.size();
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Log.e("Adapter position",String.valueOf(position));
        View row = convertView;
        final OutletStockViewAdapter.HomeHolder homeHolder;
        if (row == null) {
            // Log.e("Adapter Inner position",String.valueOf(position));
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(resource, parent, false);
            homeHolder = new OutletStockViewAdapter.HomeHolder();

            homeHolder.textVieworderdt = (TextView) row
                    .findViewById(R.id.textVieworderdt);

            homeHolder.textViewstock = (TextView) row
                    .findViewById(R.id.textViewstock);

            homeHolder.textViewmanuf = (TextView) row
                    .findViewById(R.id.textViewmanuf);

            row.setTag(homeHolder);
        } else {
            homeHolder = (OutletStockViewAdapter.HomeHolder) row.getTag();
        }

        final OutletStockEntryDetailDomain item = arrayListServiceDomainObjects
                .get(position);

        homeHolder.textVieworderdt.setText(item.getOrderdate());
        homeHolder.textViewstock.setText(item.getstock());
        homeHolder.textViewmanuf.setText(item.getmanufdt());

        return row;

    }

    class HomeHolder {

        TextView textVieworderdt, textViewstock, textViewmanuf;
    }

}
