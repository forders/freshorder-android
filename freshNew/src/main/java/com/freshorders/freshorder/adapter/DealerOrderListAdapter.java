package com.freshorders.freshorder.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.domain.DealerOrderListDomain;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class DealerOrderListAdapter extends ArrayAdapter<DealerOrderListDomain> {
	Context context;
	int resource;
	ArrayList<DealerOrderListDomain> arrayListServiceDomainObjects;

	JSONObject JsonAccountObject = null;
	JSONArray JsonAccountArray = null;
	JsonServiceHandler JsonServiceHandler;

	public DealerOrderListAdapter(Context context, int resource,
			ArrayList<DealerOrderListDomain> arrayListServiceDomainObjects) {
		super(context, resource, arrayListServiceDomainObjects);
		this.context = context;
		this.resource = resource;
		this.arrayListServiceDomainObjects = arrayListServiceDomainObjects;
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();
	}




	@Override
	public int getCount() {
		return arrayListServiceDomainObjects.size();
	}

	@Override
	public boolean isEnabled(int position) {
		return true;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// Log.e("Adapter position",String.valueOf(position));
		View row = convertView;
		final HomeHolder homeHolder;
		if (row == null) {
			// Log.e("Adapter Inner position",String.valueOf(position));
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(resource, parent, false);
			homeHolder = new HomeHolder();

			homeHolder.textViewName = (TextView) row
					.findViewById(R.id.textViewName);
			homeHolder.textViewAddress = (TextView) row
					.findViewById(R.id.textViewAddress);
			homeHolder.textViewDate = (TextView) row
					.findViewById(R.id.textViewDate);
			homeHolder.serialnonew = (TextView) row
					.findViewById(R.id.serialnonew);
			homeHolder.textviewdate1 = (TextView) row
					.findViewById(R.id.textviewdate1);
			row.setTag(homeHolder);
		} else {
			homeHolder = (HomeHolder) row.getTag();
		}
		DealerOrderListDomain item = arrayListServiceDomainObjects
				.get(position);

		String [] read=item.getisread().split(",");
//		Log.e("userid",Constants.USER_ID);
		int readint=0;
		for(int i=0;i<read.length;i++) {

			Log.e("read[0]", read[i]);

			if (read[i].equals(Constants.USER_ID)) {
				readint = 1;
			}
		}
		if(readint==1){
			homeHolder.serialnonew.setTypeface(null, Typeface.BOLD);
			homeHolder.textViewName.setTypeface(null, Typeface.BOLD);
			homeHolder.textViewDate.setTypeface(null, Typeface.BOLD);
			homeHolder.textViewAddress.setTypeface(null, Typeface.BOLD);
			homeHolder.textviewdate1.setTypeface(null, Typeface.BOLD);

		}else{
			homeHolder.serialnonew.setTypeface(null, Typeface.NORMAL);
			homeHolder.textViewName.setTypeface(null, Typeface.NORMAL);
			homeHolder.textViewDate.setTypeface(null, Typeface.NORMAL);
			homeHolder.textViewAddress.setTypeface(null, Typeface.NORMAL);
			homeHolder.textviewdate1.setTypeface(null, Typeface.NORMAL);

		}
		homeHolder.serialnonew.setText(item.getserialnonew() +".");
		homeHolder.textViewName.setText((item.getComName()));
		homeHolder.textViewAddress.setText("Order id : " +item.getAddress() +""+"\t\t"+"Dated : "+item.getDate());
		//homeHolder.textViewDate.setText(item.getDate());
		

		return row;
	}

	class HomeHolder {

		TextView textViewName, textViewAddress,textViewDate,serialnonew,textviewdate1;
	}

}