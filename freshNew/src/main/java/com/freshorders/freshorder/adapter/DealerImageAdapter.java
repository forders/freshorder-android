package com.freshorders.freshorder.adapter;

/**
 * Created by ragul on 24/08/16.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ParseException;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.GridView;
import android.widget.ImageView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.DealerOrderDetailDomain;
import com.freshorders.freshorder.domain.ViewPagerDomain;
import com.freshorders.freshorder.ui.DealerOrderDetailsActivity;
import com.freshorders.freshorder.ui.DealersOrderActivity;
import com.freshorders.freshorder.ui.MerchantOrderActivity;
import com.freshorders.freshorder.ui.MerchantSingleDetailActivity;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class DealerImageAdapter extends BaseAdapter {
    private Context mContext;
    int imageTotal;
    JSONObject JsonAccountObject = null;
    JSONArray JsonAccountArray = null;
    DatabaseHandler databaseHandler;
    JsonServiceHandler JsonServiceHandler;


    public DealerImageAdapter(Context c) {
        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();


        mContext = c;

        // myOrders();
    }

    public int getCount() {
        if (Constants.sendimageproduct == 1) {
            return DealersOrderActivity.arraylistimage.size();
        }else {
        return DealersOrderActivity.arraylistPdoductImages.size();
    }
    }

    @Override
    public String getItem(int position) {
        if (Constants.sendimageproduct == 1) {
            return DealerOrderDetailsActivity.imagesetarray[position];
        }else {
            return DealerOrderDetailsActivity.mThumbIds[position];
        }

    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        //Displaying images with prorducts and without products in salesman order detail screen

        if(Constants.sendimageproduct==1){

            Log.e("inside","inside");
            ImageView imageView;
            if (convertView == null) {
                imageView = new ImageView(mContext);
                //imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                imageView.setLayoutParams(new Gallery.LayoutParams(200, 200));
                imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

            } else {
                imageView = (ImageView) convertView;
            }
            String url = getItem(position);
            Picasso.with(mContext)
                    .load(url)
                    .placeholder(
                            R.drawable.loader)
                    .fit()
                    .centerCrop().into(imageView);
            return imageView;

        }else{
            Log.e("outside","ouside");
            ImageView imageView;
            if (convertView == null) {
                imageView = new ImageView(mContext);
                imageView.setLayoutParams(new GridView.LayoutParams(400, 400));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imageView.setPadding(15, 15, 15, 15);
            } else {
                imageView = (ImageView) convertView;
            }
            String url = getItem(position);
            Picasso.with(mContext)
                    .load(url)
                    .placeholder(
                            R.drawable.loader)
                    .fit()
                    .centerCrop().into(imageView);
            return imageView;
        }

    }



}