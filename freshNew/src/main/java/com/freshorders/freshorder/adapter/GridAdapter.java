package com.freshorders.freshorder.adapter;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.domain.GalleryDomain;
import com.freshorders.freshorder.ui.GalleryActivity;
import com.freshorders.freshorder.utils.ImageLoader;
import com.freshorders.freshorder.utils.JsonServiceHandler;

@SuppressLint("ViewHolder")
public class GridAdapter extends ArrayAdapter<GalleryDomain> {
	Context context;
	
	int resource;
	ArrayList<GalleryDomain> arraylistPdoductImages;
	public ImageLoader imageLoader;
	GalleryDomain GalleryDomainSelected;
	JSONObject JsonAccountObject = null;
	JSONArray JsonAccountArray = null;
	com.freshorders.freshorder.utils.JsonServiceHandler JsonServiceHandler;
	String ProdId;
	String strdata = "";

	public GridAdapter(Context context, int resource,
			ArrayList<GalleryDomain> arraylistPdoductImages) {
		super(context, resource, arraylistPdoductImages);
		this.context = context;
		this.resource = resource;
		this.arraylistPdoductImages = arraylistPdoductImages;
		imageLoader = new ImageLoader(context.getApplicationContext());
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();
		
		
	}

	@Override
	public int getCount() {
		return arraylistPdoductImages.size();
	}
	/*@Override
	public boolean isEnabled(int position)
	{
	return true;
	}
	*/
	public GalleryDomain getItem(int postion) {
		Log.e("position",String.valueOf(arraylistPdoductImages.get(postion)));
		return arraylistPdoductImages.get(postion);
	}
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		
		View row = convertView;
		final HomeHolder homeHolder;
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(resource, parent, false);
			homeHolder = new HomeHolder();
			homeHolder.imageViewProductItemImage = (ImageView) row.findViewById(R.id.imageView1);
		    homeHolder.textViewProductItemName = (TextView) row.findViewById(R.id.textView1);
			homeHolder.textViewDate = (TextView) row.findViewById(R.id.textView2);
			homeHolder.relativeLayoutGrid = (RelativeLayout) row.findViewById(R.id.relativeLayoutGrid);
			
	
			row.setTag(homeHolder);
	
		final GalleryDomain item = arraylistPdoductImages.get(position);


	try {
		homeHolder.imageViewProductItemImage.setTag(position);
		homeHolder.imageViewProductItemImage.setImageResource(R.drawable.ic_launcher);
		homeHolder.imageViewProductItemImage.setScaleType(ImageView.ScaleType.FIT_XY);
		Log.e("image", item.getImages());

		//for(int i=0;i<arraylistPdoductImages.size();i++){}
				imageLoader.DisplayImage(arraylistPdoductImages.get(position).getImages(), homeHolder.imageViewProductItemImage);


		} catch (Exception e) {
			// TODO Auto-generated catch block
			Log.e("ProductListAdapter","exception1 "+e);
		}
		/*homeHolder.textViewProductItemName.setText(item.getGalleryTitle());
		homeHolder.textViewDate.setText(item.getUpdatedDate());*/
	
		/*homeHolder.relativeLayoutGrid.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				GalleryActivity.gridposition =Integer.valueOf(position);
				//GalleryActivity.moveto();
				//Log.e("position", String.valueOf(Integer.valueOf(position)));
				
			}
		});*/
		
		return row;
	}

	class HomeHolder {
		ImageView imageViewProductItemImage;
		TextView textViewProductItemName,textViewDate,textViewProductCode;
		RelativeLayout relativeLayoutGrid;
		JSONObject JsonAccountObject;
		JSONArray JsonAccountArray;
		JsonServiceHandler JsonServiceHandler;
		ArrayList<GalleryDomain> arraylistPdoductImages;

		GalleryDomain GalleryDomainSelected;
		public  String ProdId, Mrp,discountprnt,discountvalue,uomid;
	}

	
}
  