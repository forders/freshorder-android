package com.freshorders.freshorder.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.domain.SpinnerDomain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Karthika on 16/04/2018.
 */

public class NippoAddMerchantAdapter extends ArrayAdapter<SpinnerDomain> {
    private Context mContext;
    private ArrayList<SpinnerDomain> listState;
    private NippoAddMerchantAdapter myAdapter;
    private boolean isFromView = false;
    private ListAdapterListener mListener;
    private ArrayList<String> arlSelectedValues;

    public interface ListAdapterListener { // create an interface
        void onClickButton(List<SpinnerDomain> list); // create callback function
    }

    public NippoAddMerchantAdapter(Context context, int resource, List<SpinnerDomain> objects, ListAdapterListener mListener) {
        super(context, resource, objects);
        this.mContext = context;
        this.listState = (ArrayList<SpinnerDomain>) objects;
        this.myAdapter = this;
        this.mListener = mListener; // receive mListener from Fragment (or Activity)
        this.arlSelectedValues=new ArrayList<String>();
    }

    public ArrayList<String> getSelectedValues()
    {
        return arlSelectedValues;
    }

    public String  getSelectedValuesString()
    {
        String strSelectedValues=""+this.getSelectedValues();
        strSelectedValues=strSelectedValues.substring(1,strSelectedValues.length()-1);
        strSelectedValues=strSelectedValues.replace(", ",",");
        return strSelectedValues;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {

        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView,
                              ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            LayoutInflater layoutInflator = LayoutInflater.from(mContext);
            convertView = layoutInflator.inflate(R.layout.spinner_check, null);
            holder = new ViewHolder();
            holder.mTextView = (TextView) convertView
                    .findViewById(R.id.text);
            holder.mCheckBox = (CheckBox) convertView
                    .findViewById(R.id.checkbox);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.mTextView.setText(listState.get(position).getTitle());
        if ((position == 0)) {
            holder.mCheckBox.setVisibility(View.INVISIBLE);
        } else {
            holder.mCheckBox.setVisibility(View.VISIBLE);
        }
        holder.mCheckBox.setTag(position);
        // To check weather checked event fire from getview() or user input
        isFromView = true;
        holder.mCheckBox.setChecked(listState.get(position).isSelected());
        holder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int getPosition = (Integer) buttonView.getTag();
                if (isChecked) {
                    listState.get(getPosition).setSelected(true);
                } else {
                    listState.get(getPosition).setSelected(false);
                }
//this line make the tricks to add the selected list
                arlSelectedValues.clear();
                for (SpinnerDomain data : listState) {
                    if(data.isSelected()) {
                        arlSelectedValues.add(data.getTitle());
                    }
                }
                mListener.onClickButton(listState);

            }
        });
        isFromView = false;

        return convertView;
    }

    private class ViewHolder {
        private TextView mTextView;
        private CheckBox mCheckBox;
    }
}