package com.freshorders.freshorder.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.model.StockAuditDisplayModel;
import com.freshorders.freshorder.utils.Constants;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class ClosingStockAuditAdapter extends
                            RecyclerView.Adapter<ClosingStockAuditAdapter.ViewHolder>
                                                                        implements Filterable {

    private List<StockAuditDisplayModel> mArrayList;
    private List<StockAuditDisplayModel> mFilteredList;
    private LinkedHashMap<Integer,Integer> enteredList;
    private LinkedHashMap<Integer,StockAuditDisplayModel> enteredListPair;

    public ClosingStockAuditAdapter(List<StockAuditDisplayModel> arrayList,
                                    LinkedHashMap<Integer,Integer> enteredLists,
                                    LinkedHashMap<Integer,StockAuditDisplayModel> enteredListPair) {
        mArrayList = arrayList;
        mFilteredList = arrayList;
        enteredList = enteredLists;
        this.enteredListPair = enteredListPair;

    }

    @NonNull
    @Override
    public ClosingStockAuditAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_closing_stock_audit, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ClosingStockAuditAdapter.ViewHolder viewHolder, int position) {

        StockAuditDisplayModel currentItem = mFilteredList.get(position);
        int id = currentItem.getStkauditid();
        int sno = position + 1;
        String stock = String.valueOf(currentItem.getStockqty());
        viewHolder.tv_sno.setText(String.valueOf(sno));
        viewHolder.tv_product_name.setText(currentItem.getProduct().getProdname());
        viewHolder.tv_stock.setText(stock);

        if(enteredList.get(id) != null){
            String item = enteredList.get(id).toString();
            viewHolder.et_audit_stock.setText(item);
        }else {
            viewHolder.et_audit_stock.setText(Constants.EMPTY);
        }

        viewHolder.et_audit_stock.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int pos = viewHolder.getAdapterPosition();
                StockAuditDisplayModel currentItem = mFilteredList.get(pos);
                int id = currentItem.getStkauditid();
                String stock = s.toString().trim();
                //old entry
                if(enteredList.get(id) != null){
                    if(stock.length() > 0){
                        enteredList.put(id, Integer.valueOf(stock));
                        enteredListPair.put(id, currentItem);
                    }else {
                        enteredList.remove(id);
                        enteredListPair.remove(id);
                    }
                } else if (enteredList.get(id) == null) {
                    if(stock.length() > 0){
                        enteredList.put(id, Integer.valueOf(stock));
                        enteredListPair.put(id, currentItem);
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if(mFilteredList == null){
            return 0;
        }
        return mFilteredList.size();
    }


    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    mFilteredList = mArrayList;
                } else {

                    List<StockAuditDisplayModel> filteredList = new ArrayList<>();

                    for (StockAuditDisplayModel item : mArrayList) {

                        if (String.valueOf(item.getProduct()).toLowerCase().contains(charString.toLowerCase())
                                /*|| androidVersion.getName().toLowerCase().contains(charString)
                                || androidVersion.getVer().toLowerCase().contains(charString)*/) {

                            filteredList.add(item);
                        }
                    }

                    mFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<StockAuditDisplayModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_sno,tv_product_name,tv_stock;
        EditText et_audit_stock;
        public ViewHolder(View view) {
            super(view);

            tv_sno = (TextView)view.findViewById(R.id.id_audit_TV_sno);
            tv_product_name = (TextView)view.findViewById(R.id.id_audit_TV_product_name);
            tv_stock = (TextView)view.findViewById(R.id.id_audit_TV_stock);
            et_audit_stock = view.findViewById(R.id.id_audit_ET_audit_stock);

        }
    }
}
