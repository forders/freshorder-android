package com.freshorders.freshorder.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.domain.OutletStockEntryDomain;
import com.freshorders.freshorder.utils.JsonServiceHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class MySalesAdapter extends ArrayAdapter<OutletStockEntryDomain> {
	Context context;
	int resource;
	ArrayList<OutletStockEntryDomain> arrayListServiceDomainObjects;

	JSONObject JsonAccountObject = null;
	JSONArray JsonAccountArray = null;
	JsonServiceHandler JsonServiceHandler;


	public MySalesAdapter(Context context, int resource,
						  ArrayList<OutletStockEntryDomain> arrayListServiceDomainObjects) {
		super(context, resource, arrayListServiceDomainObjects);
		this.context = context;
		this.resource = resource;
		this.arrayListServiceDomainObjects = arrayListServiceDomainObjects;
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();
	}

	@Override
	public int getCount() {
		return arrayListServiceDomainObjects.size();
	}

	@Override
	public boolean isEnabled(int position) {
		return true;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		final HomeHolder homeHolder;
		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(resource, parent, false);
			homeHolder = new HomeHolder();

			homeHolder.textViewProdName = (TextView) row
					.findViewById(R.id.textViewProdName);

			homeHolder.textViewProdUOM = (TextView) row
					.findViewById(R.id.textViewProdUOM);

			homeHolder.textViewQTY = (TextView) row
					.findViewById(R.id.textViewQTY);

			homeHolder.textViewValue = (TextView) row
					.findViewById(R.id.textViewValue);

			homeHolder.serialnonew = (TextView) row
					.findViewById(R.id.serialnonew);

			row.setTag(homeHolder);
		} else {
			homeHolder = (HomeHolder) row.getTag();
		}

		final OutletStockEntryDomain item = arrayListServiceDomainObjects
				.get(position);

		homeHolder.textViewProdName.setText(item.getprodname() + "  ["
				+ item.getprodcode() + "]");
		homeHolder.textViewProdUOM.setText("UOM : "+item.getUOM());
        homeHolder.textViewQTY.setText(item.getqty());
		homeHolder.serialnonew.setText(item.getserialno() +".");
		homeHolder.textViewValue.setText(item.getPvalue());
		return row;

	}

	class HomeHolder {

		TextView textViewProdName, textViewProdUOM,textViewQTY,serialnonew,textViewValue;
	}

}