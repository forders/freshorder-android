package com.freshorders.freshorder.adapter;

import android.app.Activity;
import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.domain.DealerVisitDomain;
import com.freshorders.freshorder.utils.Constants;


import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by dhanapriya on 21/10/16.
 */
public class DealerVisitAdapter extends ArrayAdapter<DealerVisitDomain> {
    Context context;
    int resource;
    ArrayList<DealerVisitDomain> arrayListServiceDomainObjects;

    JSONObject JsonAccountObject = null;
    JSONArray JsonAccountArray = null;
    com.freshorders.freshorder.utils.JsonServiceHandler JsonServiceHandler;

    public DealerVisitAdapter(Context context, int resource,
                                  ArrayList<DealerVisitDomain> arrayListServiceDomainObjects) {
        super(context, resource, arrayListServiceDomainObjects);
        this.context = context;
        this.resource = resource;
        this.arrayListServiceDomainObjects = arrayListServiceDomainObjects;
        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();
    }
    @Override
    public int getCount() {
        return arrayListServiceDomainObjects.size();
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Log.e("Adapter position",String.valueOf(position));
        View row = convertView;
        final HomeHolder homeHolder;
        if (row == null) {
            // Log.e("Adapter Inner position",String.valueOf(position));
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(resource, parent, false);
            homeHolder = new HomeHolder();

            homeHolder.textViewMerchant = (TextView) row
                    .findViewById(R.id.textViewMerchant);
            homeHolder.textViewSalesman = (TextView) row
                    .findViewById(R.id.textViewSalesman);
            homeHolder.textViewDate = (TextView) row
                    .findViewById(R.id.textViewDate);
            homeHolder.serialnonew = (TextView) row
                    .findViewById(R.id.serialnonew);

            row.setTag(homeHolder);
        } else {
            homeHolder = (HomeHolder) row.getTag();
        }
        DealerVisitDomain item = arrayListServiceDomainObjects
                .get(position);


        homeHolder.serialnonew.setText(item.getSerialNo() +".");
        homeHolder.textViewMerchant.setText(item.getMname());
        if(Constants.USER_TYPE.equals("D")){
            homeHolder.textViewSalesman.setVisibility(TextView.VISIBLE);
            homeHolder.textViewSalesman.setText(item.getSname());
        }else{
            homeHolder.textViewSalesman.setVisibility(TextView.GONE);
        }

        homeHolder.textViewDate.setText(item.getDate());


        return row;
    }

    class HomeHolder {

        TextView serialnonew, textViewMerchant,textViewSalesman,textViewDate;
    }

}
