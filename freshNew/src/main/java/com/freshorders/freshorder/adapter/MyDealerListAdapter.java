package com.freshorders.freshorder.adapter;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.domain.MyDealerListDomain;
import com.freshorders.freshorder.domain.MyDealerListDomain;
import com.freshorders.freshorder.ui.MyDealersActivity;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

public class MyDealerListAdapter extends ArrayAdapter<MyDealerListDomain> {
	Context context;
	int resource;
	ArrayList<MyDealerListDomain> arrayListServiceDomainObjects;

	JSONObject JsonAccountObject = null;
	JSONArray JsonAccountArray = null;
	JsonServiceHandler JsonServiceHandler;
	public static String id="";

	public MyDealerListAdapter(Context context, int resource,
			ArrayList<MyDealerListDomain> arrayListServiceDomainObjects) {
		super(context, resource, arrayListServiceDomainObjects);
		this.context = context;
		this.resource = resource;
		this.arrayListServiceDomainObjects = arrayListServiceDomainObjects;
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();
	}

	@Override
	public int getCount() {
		return arrayListServiceDomainObjects.size();
	}

	@Override
	public boolean isEnabled(int position) {
		return true;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// Log.e("Adapter position",String.valueOf(position));
		View row = convertView;
		final HomeHolder homeHolder;
		if (row == null) {

			// Log.e("Adapter Inner position",String.valueOf(position));
			LayoutInflater inflater1 = ((Activity) context).getLayoutInflater();
			row = inflater1.inflate(resource, parent, false);
			homeHolder = new HomeHolder();

			homeHolder.textViewName = (TextView) row
					.findViewById(R.id.textViewName);
			homeHolder.serialno = (TextView) row
					.findViewById(R.id.serialno);
			homeHolder.deleteicon = (TextView) row
					.findViewById(R.id.deleteicon);

			Typeface font = Typeface.createFromAsset(context.getAssets(),
					"fontawesome-webfont.ttf");
			homeHolder.deleteicon.setText(context.getString(R.string.delete_icon));
			homeHolder.deleteicon.setTextColor(Color.parseColor("#03acec"));
			homeHolder.deleteicon.setTypeface(font);
			row.setTag(homeHolder);
		} else {
			homeHolder = (HomeHolder) row.getTag();
		}
		final MyDealerListDomain item = arrayListServiceDomainObjects
				.get(position);

		if(Constants.USER_TYPE.equals("M")){
			homeHolder.deleteicon.setVisibility(TextView.VISIBLE);
		}else{
			homeHolder.deleteicon.setVisibility(TextView.GONE);
		}
		homeHolder.textViewName.setText(item.getComName());
		homeHolder.serialno.setText(item.getserialno()+".");
		Log.e("textviewname", item.getID());
homeHolder.deleteicon.setOnClickListener(new View.OnClickListener() {
	@Override
	public void onClick(View v) {
		id=arrayListServiceDomainObjects.get(position).getID();
		arrayListServiceDomainObjects.remove(position);

		removedealer(id);
	}
});
		return row;
	}
public void removedealer(final String id){
	// TODO Auto-generated method stub
	new AsyncTask<Void, Void, Void>() {
		ProgressDialog dialog;
		String strStatus = "";
		String strMsg = "";


		@Override
		protected void onPreExecute() {
			dialog = ProgressDialog.show(context, "",
					"Loading...", true, true);

		}

		@Override
		protected void onPostExecute(Void result) {

			try {

				strStatus = JsonAccountObject.getString("status");
				Log.e("return status", strStatus);
				strMsg = JsonAccountObject.getString("message");
				Log.e("return message", strMsg);

				showAlertDialogToast(strMsg);
				dialog.dismiss();


			} catch (JSONException e) {
				e.printStackTrace();
			} catch (Exception e) {

			}

		}

		@Override
		protected Void doInBackground(Void... params) {

			JSONObject jsonObject = new JSONObject();
			try {
				jsonObject.put("dstatus","Inactive");

			} catch (JSONException e) {
				e.printStackTrace();
			}

			JsonServiceHandler = new JsonServiceHandler(Utils.strdeletedealer+id,jsonObject.toString(), context.getApplicationContext());
			JsonAccountObject = JsonServiceHandler.ServiceData();
			return null;
		}
	}.execute(new Void[]{});

}
	public void showAlertDialogToast( String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
		builder1.setCancelable(true);
		builder1.setMessage(message);
		builder1.setPositiveButton("Ok",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						MyDealersActivity.MyDealers();
						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();

		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

	}

	class HomeHolder {

		TextView textViewName, textViewAddress,textViewDate,serialno,deleteicon;

	}

}