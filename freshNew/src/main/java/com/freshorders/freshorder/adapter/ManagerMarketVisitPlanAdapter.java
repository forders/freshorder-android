package com.freshorders.freshorder.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.model.MarketVisitPlanModel;

import java.util.List;

public class ManagerMarketVisitPlanAdapter extends RecyclerView.Adapter<ManagerMarketVisitPlanAdapter.ItemViewHolder> {

    public static final String TAG = "MarketVisitAdapter";

    private Context mContext;
    private List<MarketVisitPlanModel> listItems;
    private View viewFragment;

    public ManagerMarketVisitPlanAdapter(Context mContext,
                                  List<MarketVisitPlanModel> listItems,
                                  View viewFragment) {
        this.mContext = mContext;
        this.listItems = listItems;
        this.viewFragment = viewFragment;
    }

    @NonNull
    @Override
    public ManagerMarketVisitPlanAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View myInflatedView = inflater.inflate(R.layout.adapter_manager_market_visit_plan, parent,false);  ////////////parent --- Null(can be)
        //RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //myInflatedView.setLayoutParams(lp);
        return new ManagerMarketVisitPlanAdapter.ItemViewHolder(myInflatedView);
    }

    @Override
    public void onBindViewHolder(@NonNull ManagerMarketVisitPlanAdapter.ItemViewHolder holder, int position) {
        MarketVisitPlanModel currentItems = listItems.get(position);
        String sNo = String.valueOf((position + 1));
        String merchantName = currentItems.getName();
        String remarks = currentItems.getRemarks();
        String workType = currentItems.getWorkType();

        holder.tvSno.setText(sNo);
        holder.tvMerchantName.setText(merchantName);
        holder.tvWorkType.setText(workType);
        holder.tvRemarks.setText(remarks);

    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }




    class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView tvSno, tvMerchantName, tvWorkType, tvRemarks;

        ItemViewHolder(View itemView) {
            super(itemView);
            tvSno = itemView.findViewById(R.id.id_manager_market_TV_sno);
            tvMerchantName = itemView.findViewById(R.id.id_manager_market_TV_merchant_name);
            tvWorkType = itemView.findViewById(R.id.id_manager_market_TV_work_type);
            tvRemarks = itemView.findViewById(R.id.id_manager_market_TV_remarks);

        }
    }
}
