package com.freshorders.freshorder.adapter;
import java.util.ArrayList;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.domain.ViewPagerDomain;
import com.freshorders.freshorder.utils.ImageLoader;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


@SuppressLint("NewApi")
public class ViewPagerAdapter extends PagerAdapter {
	Activity context;
	int resource;
	TextView tv;
	public ImageLoader imageLoader;
	public  ArrayList<ViewPagerDomain> arraylistPdoductImages;
	int i=0;

	public ViewPagerAdapter(Activity context, int resource, ArrayList<ViewPagerDomain> arraylistPdoductImages) {
		this.context = context;
		this.resource = resource;
		imageLoader=new ImageLoader(context.getApplicationContext());
		this.arraylistPdoductImages=arraylistPdoductImages;
		
	}

	@Override
	public int getCount() {
//Log.e("getCount",String.valueOf(arraylistPdoductImages.size()));
		return arraylistPdoductImages.size();  //arraylistPdoductImages.size()
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {

		return view == ((View) object);
	}

	@SuppressLint("NewApi")
	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	@Override
	public Object instantiateItem(ViewGroup container, int position) {

		View row=container;
		HomeHolder1 homeHolder1 = new HomeHolder1();
		//if (row == null) {
		LayoutInflater inflater = context.getLayoutInflater();
		row = inflater.inflate(resource, container, false);
		
		homeHolder1.imageView = (ImageView) row
				.findViewById(R.id.imageViewGallery);
		homeHolder1.textViewImageDate =(TextView) row.findViewById(R.id.textViewImageDate);
		homeHolder1.textViewImageStatus=(TextView) row.findViewById(R.id.textViewImageStatus);
		homeHolder1.linearLayoutImage = (LinearLayout) row.findViewById(R.id.linearLayoutImage);
		
		row.setTag(homeHolder1);
		
			final ViewPagerDomain filename=arraylistPdoductImages.get(position);
			  try {
				  Log.e("imageLoader",filename.getImage());
	 				imageLoader.DisplayImage(filename.getImage(), homeHolder1.imageView);
				  Log.e("imageLoader",filename.getImage());
			/*	if (android.os.Build.VERSION.SDK_INT > 9) {
					        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

					        StrictMode.setThreadPolicy(policy); 
					        }
					
					URL url = null;
				  url = new URL(filename.getImage());
					Bitmap bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
				  BitmapDrawable background = new BitmapDrawable(bmp);
				  homeHolder1.linearLayoutImage .setBackgroundDrawable(background);*/

	    
	    } catch (Exception e) {
	 		
	 				e.printStackTrace();
	 			}
	       
	    homeHolder1.textViewImageDate.setText(filename.getDate());
		//Log.e("date", filename.getDate());
	    homeHolder1.textViewImageStatus.setText(filename.getStatus());
		//Log.e("status", filename.getStatus());
	    
  ((ViewPager) container).addView(row);
		
		return row;
	}

	
	
	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		((ViewPager) container).removeView((View) object);
	}
	
	
}

class HomeHolder1 {
	ImageView imageView;
	TextView textViewImageDate,textViewImageStatus;
	LinearLayout linearLayoutImage;
}