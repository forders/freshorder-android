package com.freshorders.freshorder.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.domain.CollectionDomain;
import com.freshorders.freshorder.domain.MerchantOrderListDomain;
import com.freshorders.freshorder.utils.JsonServiceHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class SalesmanPaymentCollectionAdapter extends ArrayAdapter<CollectionDomain> {
	Context context;
	int resource;
	ArrayList<CollectionDomain> arrayListServiceDomainObjects;

	JSONObject JsonAccountObject = null;
	JSONArray JsonAccountArray = null;
	JsonServiceHandler JsonServiceHandler;


	public SalesmanPaymentCollectionAdapter(Context context, int resource,
											ArrayList<CollectionDomain> arrayListServiceDomainObjects) {
		super(context, resource, arrayListServiceDomainObjects);
		this.context = context;
		this.resource = resource;
		this.arrayListServiceDomainObjects = arrayListServiceDomainObjects;
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();
	}

	@Override
	public int getCount() {
		return arrayListServiceDomainObjects.size();
	}

	@Override
	public boolean isEnabled(int position) {
		return true;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// Log.e("Adapter position",String.valueOf(position));
		View row = convertView;
		final HomeHolder homeHolder;
		if (row == null) {
			// Log.e("Adapter Inner position",String.valueOf(position));
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(resource, parent, false);
			homeHolder = new HomeHolder();

			homeHolder.textViewbillno = (TextView) row
					.findViewById(R.id.textViewbillno);

			homeHolder.textViewAmount = (TextView) row
					.findViewById(R.id.textViewAmount);

			homeHolder.textViewbalamt = (TextView) row
					.findViewById(R.id.textViewbalamt);

			homeHolder.textViewdate = (TextView) row
					.findViewById(R.id.textViewdate);

			row.setTag(homeHolder);
		} else {
			homeHolder = (HomeHolder) row.getTag();
		}
		CollectionDomain item = arrayListServiceDomainObjects
				.get(position);


		homeHolder.textViewbillno.setText(item.getBillno());
		homeHolder.textViewAmount.setText(item.getBillAmount());


		double finalresult = new Double(item.getBalncAmount());
		double value=Double.parseDouble(new DecimalFormat("##.##").format(finalresult));
		homeHolder.textViewbalamt.setText(String.valueOf(value));
		homeHolder.textViewdate.setText(item.getBillDate());



		return row;

	}

	class HomeHolder {

		TextView textViewbillno,textViewAmount,textViewdate,textViewbalamt;
	}

}