package com.freshorders.freshorder.adapter;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.android.special.ui.InputSenderDialog;
import com.freshorders.freshorder.model.StockAuditDisplayModel;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;

public class ClosingStockAuditDetailAdapter extends
            RecyclerView.Adapter<ClosingStockAuditDetailAdapter.ViewHolder>{

    private static String TAG = "AuditDetail";

    private List<StockAuditDisplayModel> mArrayList;
    private LinkedHashMap<Integer,Integer> enteredList;
    private LinkedHashMap<Integer,Integer> enteredStockList;//////////////need to remove only temp
    private LinkedHashMap<Integer,StockAuditDisplayModel> enteredListPair;
    List<Integer> keys;
    private Context context;

    public ClosingStockAuditDetailAdapter(List<StockAuditDisplayModel> arrayList,
                                                        LinkedHashMap<Integer,Integer> enteredLists,
                                                        LinkedHashMap<Integer,Integer> enteredStockList,
                                                        LinkedHashMap<Integer,StockAuditDisplayModel> enteredListPairs) {
        mArrayList = arrayList;
        enteredList = enteredLists;
        this.enteredStockList = enteredStockList;
        enteredListPair = enteredListPairs;
        keys = new ArrayList<>(enteredList.keySet());
    }

    @NonNull
    @Override
    public ClosingStockAuditDetailAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_closing_stock_audit_detail_view, viewGroup, false);
        context = viewGroup.getContext();
        return new ClosingStockAuditDetailAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ClosingStockAuditDetailAdapter.ViewHolder viewHolder, int position) {

        viewHolder.tv_sno.setText(String.valueOf(position+1));
        final int currentKey = keys.get(position);
        StockAuditDisplayModel currentItem = enteredListPair.get(currentKey);
        viewHolder.tv_product_name.setText(currentItem.getProduct().getProdname());

        if(enteredStockList.get(currentKey) != null){
            viewHolder.tv_stock.setText(String.valueOf(enteredStockList.get(currentKey)));
        }else {
            viewHolder.tv_stock.setText(String.valueOf(currentItem.getStockqty()));
        }

        viewHolder.et_audit_stock.setText(String.valueOf(enteredList.get(currentKey)));

        viewHolder.iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enteredList.remove(currentKey);
                enteredStockList.remove(currentKey);/////////////
                enteredListPair.remove(currentKey);
                keys.remove(viewHolder.getAdapterPosition());
                notifyItemRemoved(viewHolder.getAdapterPosition());
                notifyItemRangeChanged(viewHolder.getAdapterPosition(), enteredList.size());
            }
        });

        viewHolder.iv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertCustomizedLayout(context, viewHolder, currentKey);
                /*new InputSenderDialog(v.getContext(), new InputSenderDialog.InputSenderDialogListener() {
                    @Override
                    public void onOK(final String number) {
                        Log.d(TAG, "The user tapped OK, number is "+number);
                        try {
                            if (number.length() > 0) {
                                viewHolder.et_audit_stock.setText(number);
                                enteredList.put(currentKey, Integer.valueOf(number));
                            }
                        }catch (NumberFormatException e){
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onCancel(String number) {
                        Log.d(TAG, "The user tapped Cancel, number is "+number);
                    }
                }).setNumber(String.valueOf(enteredList.get(currentKey))).show();  */
            }
        });

    }

    public void alertCustomizedLayout(Context context,
                                      final ClosingStockAuditDetailAdapter.ViewHolder viewHolder,
                                      final int currentKey){

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        // get the layout inflater
        LayoutInflater inflater = LayoutInflater.from(context);

        // inflate and set the layout for the dialog
        // pass null as the parent view because its going in the dialog layout
        final View view = inflater.inflate(R.layout.closing_stock_audit_edit_temp, null);
        final EditText etAudit = view.findViewById(R.id.id_audit_audit_stk);
        final EditText etClosingStk = view.findViewById(R.id.id_audit_closing_stk);

        String audit = Objects.requireNonNull(enteredList.get(currentKey)).toString();
        etAudit.setText(audit);
        String stock = "";

        if(enteredStockList.get(currentKey) != null){
            stock = Objects.requireNonNull(enteredStockList.get(currentKey)).toString();
            etClosingStk.setText(stock);
        }else {
            StockAuditDisplayModel cur = enteredListPair.get(currentKey);
            if(cur != null) {
                etClosingStk.setText(cur.getStockqty());
            }
        }



        builder.setView(view)
                // action buttons
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // your sign in code here
                        String audit = etAudit.getText().toString().trim();
                        String stock = etClosingStk.getText().toString().trim();
                        if(!audit.isEmpty()){
                            viewHolder.et_audit_stock.setText(audit);
                            enteredList.put(currentKey, Integer.valueOf(audit));
                        }else {
                            viewHolder.et_audit_stock.setText("0");
                            enteredList.put(currentKey, 0);
                        }

                        if(!stock.isEmpty()){
                            viewHolder.tv_stock.setText(stock);
                            enteredStockList.put(currentKey, Integer.valueOf(stock));
                        }else {
                            viewHolder.tv_stock.setText("0");
                            enteredStockList.put(currentKey, 0);
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // remove the dialog from the screen
                        dialog.dismiss();
                    }
                })
                .show();

    }

    @Override
    public int getItemCount() {
        if(enteredList == null){
            return 0;
        }
        return enteredList.size();
    }




    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView tv_sno,tv_product_name,tv_stock;
        TextView et_audit_stock;
        ImageView iv_edit, iv_delete;
        public ViewHolder(View view) {
            super(view);

            tv_sno = (TextView)view.findViewById(R.id.id_audit_TV_sno);
            tv_product_name = (TextView)view.findViewById(R.id.id_audit_TV_product_name);
            tv_stock = (TextView)view.findViewById(R.id.id_audit_TV_stock);
            et_audit_stock = view.findViewById(R.id.id_audit_ET_audit_stock);
            iv_edit = view.findViewById(R.id.id_audit_IV_edit);
            iv_delete = view.findViewById(R.id.id_audit_IV_delete);

        }
    }
}
