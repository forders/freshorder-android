package com.freshorders.freshorder.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.model.ManagerClientVisitAdapterModel;

import java.util.List;

public class ManagerClientVisitAdapter extends RecyclerView.Adapter<ManagerClientVisitAdapter.ItemViewHolder> {

    private static final String TAG = ManagerClientVisitAdapter.class.getSimpleName();

    private Context mContext;
    private List<ManagerClientVisitAdapterModel> listItems;
    private View viewFragment;

    public ManagerClientVisitAdapter(Context mContext,
                                     List<ManagerClientVisitAdapterModel> listItems,
                                     View viewFragment) {
        this.mContext = mContext;
        this.listItems = listItems;
        this.viewFragment = viewFragment;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View myInflatedView = inflater.inflate(R.layout.adapter_manager_client_visit, parent,false);  ////////////parent --- Null(can be)
        //RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //myInflatedView.setLayoutParams(lp);
        return new ItemViewHolder(myInflatedView);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        ManagerClientVisitAdapterModel currentItems = listItems.get(position);
        String sNo = String.valueOf((position + 1));
        String merchantName = currentItems.getName();
        String product = currentItems.getRemarks();
        String time = currentItems.getTime();

        holder.tvSno.setText(sNo);
        holder.tvMerchantName.setText(merchantName);
        holder.tvTime.setText(time);
        holder.tvProduct.setText(product);

    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView tvSno, tvMerchantName, tvProduct, tvTime;


        ItemViewHolder(View itemView) {
            super(itemView);
            tvSno = itemView.findViewById(R.id.id_manager_client_tv_sno);
            tvMerchantName = itemView.findViewById(R.id.id_manager_client_tv_merchant_name);
            tvTime = itemView.findViewById(R.id.id_manager_client_tv_time);
            tvProduct = itemView.findViewById(R.id.id_manager_client_tv_remarks);

        }
    }
}
