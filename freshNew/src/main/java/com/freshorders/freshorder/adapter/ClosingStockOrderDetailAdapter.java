package com.freshorders.freshorder.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.DealerOrderDetailDomain;
import com.freshorders.freshorder.domain.MerchantOrderDetailDomain;
import com.freshorders.freshorder.domain.ProductUomDomain;
import com.freshorders.freshorder.popup.Template1QuantityUpdatePopup;
import com.freshorders.freshorder.popup.Template2QuantityUpdatePopup;
import com.freshorders.freshorder.popup.Template3QuantityUpdatePopup;
import com.freshorders.freshorder.popup.Template4QuantityUpdatePopup;
import com.freshorders.freshorder.ui.DealerOrderDetailsActivity;
import com.freshorders.freshorder.ui.SalesManOrderActivity;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.freshorders.freshorder.utils.Constants.QUANTITY_TEXT_FOR_TEMPLATE;

public class ClosingStockOrderDetailAdapter extends
        ArrayAdapter<DealerOrderDetailDomain> {
    Context context;
    int resource;
    ArrayList<DealerOrderDetailDomain> arraylistDealerOrderDetailList;
    ArrayList<MerchantOrderDetailDomain> arraylistMerchantOrderDetailList;
    Context textViewDate;
    JSONObject JsonAccountObject = null;
    JSONArray JsonAccountArray = null;
    com.freshorders.freshorder.utils.JsonServiceHandler JsonServiceHandler;
    DatePicker datePicker;
    TimePicker timePicker;
    public static String time,selecteddeliverymode="",monthLength="",dayLength="";
    private int year;
    private int month, date;
    private int day, today;
    String[] items;
    public static Date dateStr = null;
    String qty="0",prodPrice="",freeQty="0",multipleUomCount="",deliverydate="",deliverytime="",deliverymode="",orderuom,freeqtyuom;
    Float oldQty =0.0f;
    DatabaseHandler databaseHandler;
    public static int mYear, mMonth, mDay, mHour, mMinute,mSeconds;
    String[] array;

    private boolean isOrderProcessedIn = false;  // kumaravel need to check if online order is delivered or partial delivered
    //Kumaravel 14/01/2019
    private Cursor curOrderStatus;
    private void getOrderStatusAdapter() {
        try {
            curOrderStatus = databaseHandler.getOrderStatus(Integer.parseInt(SalesManOrderActivity.orderId));
            if (curOrderStatus != null && curOrderStatus.getCount() > 0) {
                curOrderStatus.moveToFirst();
                isOrderProcessedIn = curOrderStatus.getInt(0) > 0;
                curOrderStatus.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            isOrderProcessedIn = true; // Suppose Exception catched we don't know process status so we block
        }
    }

    public ClosingStockOrderDetailAdapter(Context context, int resource,
                                    ArrayList<DealerOrderDetailDomain> arraylistDealerOrderDetailList) {
        super(context, resource, arraylistDealerOrderDetailList);
        this.context = context;
        this.resource = resource;
        this.arraylistDealerOrderDetailList = arraylistDealerOrderDetailList;
        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();
        databaseHandler = new DatabaseHandler(context);
        ///////////////////////        getOrderStatusAdapter();
    }

    @Override
    public int getCount() {
        return arraylistDealerOrderDetailList.size();
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Log.e("Adapter position",String.valueOf(position));
        View row = convertView;
        final ClosingStockOrderDetailAdapter.HomeHolder homeHolder;
        if (row == null) {
            // Log.e("Adapter Inner position",String.valueOf(position));
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(resource, parent, false);
            homeHolder = new ClosingStockOrderDetailAdapter.HomeHolder();

            homeHolder.textViewProductName = (TextView) row
                    .findViewById(R.id.textViewProductName);
            homeHolder.textViewUOMName= (TextView) row
                    .findViewById(R.id.textViewUOMName);

            homeHolder.textViewClosingStock = (TextView) row
                    .findViewById(R.id.textViewClosingStock);

            row.setTag(homeHolder);
        } else {
            homeHolder = (ClosingStockOrderDetailAdapter.HomeHolder) row.getTag();
        }
        final DealerOrderDetailDomain item = arraylistDealerOrderDetailList
                .get(position);

        homeHolder.textViewProductName.setText(item.getproductname());
        homeHolder.textViewClosingStock.setText(item.getCurrStock());

        homeHolder.textViewUOMName.setText("UOM : "+item.getSelcetedUOM());
        return  row;
    }


    class HomeHolder {
        RelativeLayout relativeLayoutCalender, relativeLayoutStatus;
        Spinner spinnerStatus;

        TextView textViewProductName, textViewQty, textViewDate,
                textViewStatus, textViewCalendar, textViewSpinner, textViewClosingStock;
        TextView textViewName, textViewSubname, textViewPlus,textViewUOMName;
        EditText editTextQuantity;
        LinearLayout llRoot;
    }

    //Kumaravel
    public void showAlertDialogToast(String message) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(true);
        builder.setMessage(message);
        builder.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert = builder.create();
        alert.show();

        Button buttonbackground = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

}
