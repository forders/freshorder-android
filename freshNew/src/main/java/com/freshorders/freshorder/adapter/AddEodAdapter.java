package com.freshorders.freshorder.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.domain.AddEodDomain;

import java.util.ArrayList;


/**
 * Created by karthika on 21/8/17.
 */

public class AddEodAdapter extends RecyclerView.Adapter<AddEodAdapter.ViewHolder> {

    private ArrayList<AddEodDomain> arrayList;
    private Context Activity;


    public AddEodAdapter(ArrayList<AddEodDomain> arrayListServiceDomainObjects, android.app.Activity activity) {
        this.arrayList = arrayListServiceDomainObjects;
        this.Activity = activity;

          }

    @Override
    public AddEodAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view;

            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_eod, viewGroup, false);

          return new AddEodAdapter.ViewHolder(view);

    }


    @Override
    public void onBindViewHolder(AddEodAdapter.ViewHolder viewHolder, final int i) {
        AddEodDomain item = arrayList.get(i);
        try {
            viewHolder.textViewprodcode.setText(item.getProdcode());
            viewHolder.textViewprodname.setText(item.getProdname());
            viewHolder.textViewqtotal.setText(item.getQtotal());
            viewHolder.textViewuom.setText(item.getUom());


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {

        return arrayList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {


        TextView textViewprodcode, textViewprodname,textViewqtotal,textViewuom, currentDate;


        ViewHolder(View view) {
            super(view);
            textViewprodcode = (TextView) view.findViewById(R.id.textViewprodcode);
            textViewprodname = (TextView) view.findViewById(R.id.textViewprodname);
            textViewqtotal = (TextView) view.findViewById(R.id.textViewqtotal);
            textViewuom = (TextView) view.findViewById(R.id.textViewuom);

        }
    }


}