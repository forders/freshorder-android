package com.freshorders.freshorder.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.domain.AppPaymentHistoryDomain;
import com.freshorders.freshorder.domain.DealerOrderDetailDomain;
import com.freshorders.freshorder.domain.MerchantOrderDetailDomain;
import com.freshorders.freshorder.domain.MerchantOrderListDomain;
import com.freshorders.freshorder.ui.MerchantOrderActivity;
import com.freshorders.freshorder.utils.JsonServiceHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by ragul on 03/10/16.
 */
public class AppPaymentHsitoryAdapter extends
        ArrayAdapter<AppPaymentHistoryDomain> {

    Context context;
    int resource;
    ArrayList<AppPaymentHistoryDomain> arraylistpaymenthistory;


    JSONObject JsonAccountObject = null;
    JSONArray JsonAccountArray = null;
    JsonServiceHandler JsonServiceHandler;

    public AppPaymentHsitoryAdapter(Context context, int resource,
                                    ArrayList<AppPaymentHistoryDomain> arraylistpaymenthistory) {
        super(context, resource, arraylistpaymenthistory);
        this.context = context;
        this.resource = resource;
        this.arraylistpaymenthistory = arraylistpaymenthistory;
        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();
    }
    public int getCount() {

        return arraylistpaymenthistory.size();

    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Log.e("Adapter position",String.valueOf(position));
        View row = convertView;
        final HomeHolder homeHolder;
        if (row == null) {
            // Log.e("Adapter Inner position",String.valueOf(position));
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(resource, parent, false);
            homeHolder = new HomeHolder();

            homeHolder.textViewpaymntStatus = (TextView) row
                    .findViewById(R.id.textViewpaymntStatus);

            homeHolder.textViewpaymntamt = (TextView) row
                    .findViewById(R.id.textViewpaymntamt);
            homeHolder.textViewpaymntdate = (TextView) row
                    .findViewById(R.id.textViewpaymntdate);

            row.setTag(homeHolder);
        } else {
            homeHolder = (HomeHolder) row.getTag();
        }
        AppPaymentHistoryDomain item = arraylistpaymenthistory
                .get(position);
        homeHolder.textViewpaymntamt.setText(item.getpymtamount() );
        homeHolder.textViewpaymntdate.setText(item.getpymtdate());
        //	homeHolder.textViewAddress.setText(item.getAddress());
        homeHolder.textViewpaymntStatus.setText(item.getpymtstatus());

        Log.e("item.getComName()","RS. "+item.getpymtamount());
        Log.e("item.getDate()",item.getpymtdate());


        return row;

    }

    class HomeHolder {

        TextView textViewpaymntStatus,textViewpaymntamt,textViewpaymntdate;
    }
}
