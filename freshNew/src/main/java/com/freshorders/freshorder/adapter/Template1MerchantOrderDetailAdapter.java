package com.freshorders.freshorder.adapter;


import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.MerchantOrderDetailDomain;
import com.freshorders.freshorder.domain.MerchantOrderDomainSelected;
import com.freshorders.freshorder.model.Template2Model;
import com.freshorders.freshorder.ui.CreateOrderActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.freshorders.freshorder.MyApplication.previousSelected;
import static com.freshorders.freshorder.MyApplication.previousSelectedForSearch;
import static com.freshorders.freshorder.utils.Constants.CHAR_DOT;
import static com.freshorders.freshorder.utils.Constants.DOT;

public class Template1MerchantOrderDetailAdapter extends ArrayAdapter<MerchantOrderDetailDomain> {
    Context context;
    int resource;

    ArrayList<MerchantOrderDetailDomain> arraylistMerchantOrderDetailList;
    ArrayList<MerchantOrderDetailDomain>arrayListSearchResults;
    public static ArrayList<MerchantOrderDomainSelected> arraylistMerchantOrderDetailListSelected, arraylistsavearray;
    JSONObject JsonAccountObject = null;
    JSONArray JsonAccountArray = null;
    com.freshorders.freshorder.utils.JsonServiceHandler JsonServiceHandler;
    String qty = "", freeQty = "", orderuom = "",price = "", defaultuom = "", freeqtyuom = "",deliverydate="",deliverytime="",deliverymode="",currstock="";
    public static String prodid, multipleUomCount = "",selecteddeliverymode="",monthLength="",dayLength="";
    //int index=1;
    public static EditText editTextSearchField;
    public static TextView textViewHeader, textQtyValidat;
    public static MerchantOrderDomainSelected dod;
    DatabaseHandler databaseHandler;
    public static int mYear, mMonth, mDay, mHour, mMinute,mSeconds;
    private boolean netcheck;

    public Map<Integer, Template2Model> merchantOrderDetailList= new LinkedHashMap<>();
    Map<Integer, String> stock;
    Map<Integer, String> quantityList;
    //public static String searchclick="0";
    public Template1MerchantOrderDetailAdapter(
            Context context,
            int resource,
            ArrayList<MerchantOrderDetailDomain> arraylistMerchantOrderDetailList, Boolean netcheck) {
        super(context, resource, arraylistMerchantOrderDetailList);
        Log.e("MerchantOrdDtlAdapter",netcheck.toString());
        this.context = context;
        this.resource = resource;
        this.arraylistMerchantOrderDetailList = arraylistMerchantOrderDetailList;
        this.netcheck = netcheck;
        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();
        databaseHandler = new DatabaseHandler(context);
        stock = new LinkedHashMap<>(arraylistMerchantOrderDetailList.size());
        quantityList = new LinkedHashMap<>(arraylistMerchantOrderDetailList.size());

        for(int i = 0; i < arraylistMerchantOrderDetailList.size(); i++) {
            String productId = arraylistMerchantOrderDetailList.get(i).getprodid();
            if(previousSelectedForSearch.get(productId) != null){///////////////////for Search
                MerchantOrderDomainSelected item = previousSelectedForSearch.get(productId);
                quantityList.put(i, item.getqty());
                stock.put(i, item.getCurrStock());
            }else {
                stock.put(i, "");
                quantityList.put(i, "");
            }
        }

        previousSelected.clear(); // need to clear



    }

    @Override
    public int getCount() {
        return arraylistMerchantOrderDetailList.size();
    }




    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Log.e("Adapter position",String.valueOf(position));
        View row = convertView;
        final Template1MerchantOrderDetailAdapter.HomeHolder2 homeHolder;

        if (row == null) {
            // Log.e("Adapter Inner position",String.valueOf(position));
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(resource, parent, false);
            homeHolder = new Template1MerchantOrderDetailAdapter.HomeHolder2();
            homeHolder.textViewName = (TextView) row
                    .findViewById(R.id.textViewName);
            homeHolder.editTextClosingStock = (EditText) row
                    .findViewById(R.id.editTextClosingStock);
            homeHolder.editTextOrderQuantity = (EditText) row.findViewById(R.id.editTextOrderQuantity);
            homeHolder.serialno = (TextView) row
                    .findViewById(R.id.serialno);


            homeHolder.editTextClosingStock.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }
                @Override
                public void afterTextChanged(Editable editable) {

                    int position = (int) homeHolder.editTextOrderQuantity.getTag();

                    String productId = arraylistMerchantOrderDetailList.get(position).getprodid();//////Search

                    String closeStock = editable.toString().trim();
                    Template2Model template2Model = merchantOrderDetailList.get(position);
                    if(template2Model != null){
                        String qty = template2Model.getOrderQuantity();
                        //String closeStock = template2Model.getClosingStock();
                        if(closeStock.isEmpty() && qty.isEmpty()){
                            merchantOrderDetailList.remove(position);
                            if(previousSelectedForSearch.get(productId) != null){///////Search
                                previousSelectedForSearch.remove(productId);
                            }
                        }else if(closeStock.isEmpty() && (qty.length() == 1 && qty.startsWith("."))){
                            merchantOrderDetailList.remove(position);
                            if(previousSelectedForSearch.get(productId) != null){///////Search
                                previousSelectedForSearch.remove(productId);
                            }
                        } else if(!closeStock.isEmpty()){  /// && qty.length() > 0
                            Template2Model temp = merchantOrderDetailList.get(position);
                            temp.setClosingStock(closeStock);
                            temp.setProductId(productId);//////Search
                            merchantOrderDetailList.put(position, temp);
                            stock.put(position,closeStock);
                        }else if(closeStock.isEmpty()) {  /// && qty.length() > 0
                            Template2Model temp = merchantOrderDetailList.get(position);
                            temp.setClosingStock(closeStock);
                            temp.setProductId(productId);//////Search
                            merchantOrderDetailList.put(position, temp);
                            stock.put(position, closeStock);
                        }


                    }else {  ///// this part is focused for first entry
                        if(!closeStock.isEmpty()) {
                            if(!(closeStock.length() == 1 && closeStock.startsWith("."))) {
                                Template2Model newModel = new Template2Model();
                                newModel.setClosingStock(closeStock);
                                newModel.setProductId(productId);//////Search
                                merchantOrderDetailList.put(position, newModel);
                                stock.put(position,closeStock);
                            }
                        }
                    }
                }

            });

            homeHolder.editTextOrderQuantity.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }
                @Override
                public void afterTextChanged(Editable editable) {

                    int position = (int) homeHolder.editTextClosingStock.getTag();

                    String productId = arraylistMerchantOrderDetailList.get(position).getprodid();//////Search

                    String quantity = editable.toString().trim();
                    Template2Model template2Model = merchantOrderDetailList.get(position);
                    if(template2Model != null){
                        String qty = template2Model.getOrderQuantity();
                        String closeStock = template2Model.getClosingStock();
                        if(quantity.isEmpty() && closeStock.isEmpty()){
                            merchantOrderDetailList.remove(position);
                            if(previousSelectedForSearch.get(productId) != null){///////Search
                                previousSelectedForSearch.remove(productId);
                            }
                        }else if(quantity.isEmpty() && (closeStock.length() == 1 && closeStock.startsWith("."))){
                            merchantOrderDetailList.remove(position);
                            if(previousSelectedForSearch.get(productId) != null){///////Search
                                previousSelectedForSearch.remove(productId);
                            }
                        } else if(!quantity.isEmpty()){  ////&& closeStock.length() > 0
                            Template2Model temp = merchantOrderDetailList.get(position);
                            temp.setOrderQuantity(quantity);
                            temp.setProductId(productId);//////Search
                            merchantOrderDetailList.put(position, temp);
                            quantityList.put(position, quantity);
                        }else if(quantity.isEmpty()){
                            Template2Model temp = merchantOrderDetailList.get(position);
                            temp.setOrderQuantity(quantity);
                            temp.setProductId(productId);//////Search
                            merchantOrderDetailList.put(position, temp);
                            quantityList.put(position, quantity);
                        }



                    }else {  ///// this part is focused for first entry
                        if(!quantity.isEmpty()) {
                            if(!(quantity.length() == 1 && quantity.startsWith("."))) {
                                Template2Model newModel = new Template2Model();
                                newModel.setOrderQuantity(quantity);
                                newModel.setProductId(productId);//////Search
                                merchantOrderDetailList.put(position, newModel);
                                quantityList.put(position, quantity);
                            }
                        }
                    }
                }
            });


            row.setTag(homeHolder);
        } else {
            homeHolder = (Template1MerchantOrderDetailAdapter.HomeHolder2) row.getTag();
        }

        final MerchantOrderDetailDomain item = arraylistMerchantOrderDetailList
                .get(position);

        arraylistsavearray = new ArrayList<MerchantOrderDomainSelected>();
        homeHolder.textViewName.setText(Html.fromHtml(item.getprodname() + "  ["
                + item.getprodcode() + "]"));

        homeHolder.serialno.setText(item.getserialno() +".");

        homeHolder.editTextOrderQuantity.setTag(position);
        homeHolder.editTextClosingStock.setTag(position);


        homeHolder.editTextOrderQuantity.setText(quantityList.get(position));
        homeHolder.editTextClosingStock.setText(stock.get(position));



        Log.e("position","~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`"+position);

        return row;
    }

    public Map<Integer, Template2Model> getSelectedMerchantOrders(){
        return merchantOrderDetailList;
    }

    public ArrayList<MerchantOrderDetailDomain> getArrayListMerchantOrderDetailList(){
        return arraylistMerchantOrderDetailList;
    }

    public boolean stock_quantity_pairCheck(){
        for(Map.Entry entry: merchantOrderDetailList.entrySet()){
            int i = (int) entry.getKey();
            String strQuantity = merchantOrderDetailList.get(i).getOrderQuantity();
            String strStock = merchantOrderDetailList.get(i).getClosingStock();

            if(strQuantity.length() > 0) {
                if (strQuantity.charAt(strQuantity.length() - 1) == CHAR_DOT) {
                    Template2Model temp = merchantOrderDetailList.get(i);
                    temp.setOrderQuantity(strQuantity.substring(0, strQuantity.lastIndexOf(CHAR_DOT)));
                    merchantOrderDetailList.put(i, temp);
                }
            }

            if(strStock.length() > 0) {
                if (strStock.charAt(strStock.length() - 1) == CHAR_DOT) {
                    Template2Model temp = merchantOrderDetailList.get(i);
                    temp.setClosingStock(strStock.substring(0, strStock.lastIndexOf(CHAR_DOT)));
                    merchantOrderDetailList.put(i, temp);
                }
            }

            if(strStock.isEmpty()){
                if(quantityList.get(i).length() > 0){
                    return false;
                }
            }
            if(strQuantity.isEmpty()){
                if(strStock.length() > 0){
                    return false;
                }
            }

            if(strQuantity.length() == 1 && strQuantity.equals(DOT)){
                return false;
            }
            if(strStock.length() == 1 && strStock.equals(DOT)){
                return false;
            }
        }
        return true;
    }

    class HomeHolder2 {
        //RelativeLayout relativeLayoutAddQuantity;
        TextView textViewName, serialno;

        //ImageView imageView;
        EditText editTextClosingStock, editTextOrderQuantity;

    }

}