package com.freshorders.freshorder.adapter;

import java.util.ArrayList;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONObject;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.MerchantOrderDetailDomain;
import com.freshorders.freshorder.domain.MerchantOrderDomainSelected;
import com.freshorders.freshorder.ui.CreateOrderActivity;
import com.freshorders.freshorder.ui.MerchantOrderCheckoutActivity;
import com.freshorders.freshorder.ui.MerchantOrderDetailActivity;
import com.freshorders.freshorder.ui.SalesManOrderCheckoutActivity;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

public class MerchantOrderCheckoutAdapter extends ArrayAdapter<MerchantOrderDomainSelected> {
	Context context;
	int resource;
	ArrayList<MerchantOrderDomainSelected> arraylistMerchantOrderDetailList;
	JSONObject JsonAccountObject = null;
	JSONArray JsonAccountArray = null;
	public static MerchantOrderDomainSelected dod;
	JsonServiceHandler JsonServiceHandler;
	String qty,price,freeQty,multipleUomCount ="",freeqtyuom,deliverydate="",deliverytime="",deliverymode="",selecteddeliverymode="",monthLength="",dayLength="",orderuom;
	DatabaseHandler databaseHandler;
	public static EditText editTextSearchField;
	public static TextView textViewHeader, textQtyValidate;
	public static int mYear, mMonth, mDay, mHour, mMinute,mSeconds;

	public MerchantOrderCheckoutAdapter(Context context, int resource,
			ArrayList<MerchantOrderDomainSelected> arraylistMerchantOrderDetailList) {
		super(context, resource, arraylistMerchantOrderDetailList);

		this.context = context;
		this.resource = resource;
		this.arraylistMerchantOrderDetailList = arraylistMerchantOrderDetailList;
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();
		databaseHandler = new DatabaseHandler(context);
	}

	@Override
	public int getCount() {
		return arraylistMerchantOrderDetailList.size();
	}

	@Override
	public boolean isEnabled(int position) {
		return true;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// Log.e("Adapter position",String.valueOf(position));
		View row = convertView;
		final HomeHolder homeHolder;
		if (row == null) {
			// Log.e("Adapter Inner position",String.valueOf(position));
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(resource, parent, false);
			homeHolder = new HomeHolder();

			homeHolder.textViewName = (TextView) row
					.findViewById(R.id.textViewName);
			homeHolder.textViewSubname = (TextView) row
					.findViewById(R.id.textViewSubname);
			homeHolder.textViewPlus = (TextView) row
					.findViewById(R.id.textViewPlus);
			homeHolder.serialno = (TextView) row
					.findViewById(R.id.serialno);
			homeHolder.editTextQuantity = (EditText) row.findViewById(R.id.editTextClosingStock);
			homeHolder.textViewUomName= (TextView) row.findViewById(R.id.textViewUomName);
			homeHolder.relativeLayoutAddQuantity = (RelativeLayout) row.findViewById(R.id.relativeLayoutAddQuantity);

			Typeface font = Typeface.createFromAsset(context.getAssets(),
					"fontawesome-webfont.ttf");
			homeHolder.textViewPlus.setText(context.getString(R.string.delete_icon));
			homeHolder.textViewPlus.setTypeface(font);
			homeHolder.textViewPlus.setTextColor(Color.parseColor("#03acec"));
			homeHolder.textViewPlus.setVisibility(View.VISIBLE);
		
			row.setTag(homeHolder);
		} else {
			homeHolder = (HomeHolder) row.getTag();
		}
		final MerchantOrderDomainSelected item = arraylistMerchantOrderDetailList
				.get(position);
		homeHolder.textViewName.setText(item.getprodname()+"  ["+item.getprodcode()+"]");
		homeHolder.serialno.setText(item.getserialno()+".");
		//homeHolder.textViewPlus.setText(item.getindex());
		homeHolder.textViewUomName.setText("UOM : "+item.getorderuom());
		homeHolder.textViewSubname.setText(item.getCompanyname()+"  ["+item.getUserid()+"]");
		if(item.getqty().equals("")){
			item.setqty("0");

		}
		if(item.getFreeQty().equals("")){
			item.setFreeQty("0");
		}
		/*if(!item.getUOM().isEmpty()) {
			homeHolder.editTextQuantity.setText("NF:" + "[" + item.getqty() + "]" + "\n" + "F:" + "["+item.getFreeQty()+"]"+" "+item.getUOM());
		}else{
			homeHolder.editTextQuantity.setText("NF:" + "[" + item.getqty() + "]" + "\n" + "F:" + "["+item.getFreeQty()+"]");
		}
*/

		homeHolder.editTextQuantity.setText("NF:" + "[" + item.getqty() + "]" + "\n" + "F:" + "["+item.getFreeQty()+"]");
		homeHolder.textViewPlus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.e("Clicked", "Clicked");
				if (Constants.USER_TYPE.equals("M")) {
					MerchantOrderCheckoutActivity.removeid(arraylistMerchantOrderDetailList.get(position).getprodid());
				} else {
					SalesManOrderCheckoutActivity.removeid(arraylistMerchantOrderDetailList.get(position).getprodid());
				}

				arraylistMerchantOrderDetailList.remove(position);

				MerchantOrderDomainSelected  dod = new MerchantOrderDomainSelected();
				for (int i = 0; i < arraylistMerchantOrderDetailList.size(); i++) {
					//dod.setserialno(String.valueOf(i+1));
                    arraylistMerchantOrderDetailList.get(i).setserialno(String.valueOf(i+1));
					Log.e("i value", String.valueOf(i + 1));
				}


			if (Constants.USER_TYPE.equals("M")) {
					Intent io = new Intent(getContext(), MerchantOrderCheckoutActivity.class);
					getContext().startActivity(io);
					((Activity) context).finish();
				} else {
					Intent io = new Intent(getContext(), SalesManOrderCheckoutActivity.class);
					getContext().startActivity(io);
					((Activity) context).finish();
				}


			}
		});
		homeHolder.editTextQuantity.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				final Dialog qtyDialog = new Dialog(context);
				qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				qtyDialog.setContentView(R.layout.refkey_dialogue);

				final Button buttonUpdateOk, buttonUpdateCancel;
				InputMethodManager iss = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
				iss.hideSoftInputFromWindow(homeHolder.editTextQuantity.getWindowToken(), 0);
				InputMethodManager inn = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
				inn.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);


				final EditText editTextRefKey, editTextFreeQty, editTextRefKeyprice,editTextfqtyuom, editTextTime, editTextDate;
				final TextView textQtyValidate;
				final AutoCompleteTextView textviewuom, textviewfreeuom,textviewdelivery;
				final LinearLayout linearlayoutdatetime,linearlayoutdeliverymode;

				buttonUpdateOk = (Button) qtyDialog
						.findViewById(R.id.buttonUpdateOk);
				buttonUpdateCancel = (Button) qtyDialog
						.findViewById(R.id.buttonUpdateCancel);
				textviewuom= (AutoCompleteTextView) qtyDialog
						.findViewById(R.id.textviewuom);
				editTextRefKey = (EditText) qtyDialog
						.findViewById(R.id.editTextRefKey);
				editTextRefKeyprice = (EditText) qtyDialog
						.findViewById(R.id.editTextRefKeyprice);
				editTextFreeQty = (EditText) qtyDialog
						.findViewById(R.id.editTextFreeQty);
				textQtyValidate=(TextView) qtyDialog.findViewById(R.id.textQtyValidate);
				textviewfreeuom = (AutoCompleteTextView) qtyDialog
						.findViewById(R.id.textviewfreeuom);
				editTextTime = (EditText) qtyDialog
						.findViewById(R.id.editTextTime);
				editTextDate = (EditText) qtyDialog
						.findViewById(R.id.editTextDate);
				textviewdelivery = (AutoCompleteTextView) qtyDialog
						.findViewById(R.id.textviewdelivery);
				linearlayoutdatetime = (LinearLayout) qtyDialog
						.findViewById(R.id.linearlayoutdatetime);
				linearlayoutdeliverymode = (LinearLayout) qtyDialog
						.findViewById(R.id.linearlayoutdeliverymode);

				if (Constants.USER_TYPE.equals("M")) {
					linearlayoutdatetime.setVisibility(linearlayoutdatetime.GONE);
					linearlayoutdeliverymode.setVisibility(linearlayoutdeliverymode.GONE);
					textviewuom.setVisibility(textviewuom.GONE);
					textviewfreeuom.setVisibility(textviewfreeuom.GONE);
				}

				if(textviewdelivery.equals("")){
					textviewdelivery.setText("Standard");
				}
				String prodiduom=item.getprodid();
				Cursor cur;
				cur = databaseHandler.getUOMforproduct(prodiduom);
				multipleUomCount =String.valueOf(cur.getCount());

				String[] array = new String[cur.getCount()];
				cur.moveToFirst();
				for(int i=0;i<cur.getCount();i++)
				{
					//array[0] = item.getUOM();
					array[i] = cur.getString(cur.getColumnIndex("uomname"));
					cur.moveToNext();
				}

				ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, array);
				textviewuom.setAdapter(adapter);
				textviewfreeuom.setAdapter(adapter);
				textviewuom.setHint(item.getorderuom());
				textviewfreeuom.setHint(item.getorderfreeuom());
				textviewuom.setOnTouchListener(new View.OnTouchListener() {
					@Override
					public boolean onTouch(View v, MotionEvent event) {
						if(!multipleUomCount.equals("0")){
							textviewuom.showDropDown();
						}
						return false;
					}
				});

				textviewfreeuom.setOnTouchListener(new View.OnTouchListener() {
					@Override
					public boolean onTouch(View v, MotionEvent event) {
						if(!multipleUomCount.equals("0")){
							textviewfreeuom.showDropDown();
						}

						return false;
					}
				});

				if(item.getFreeQty().equals("")){

					editTextFreeQty.setText("");
				}else {
					Log.e("freeqty",item.getFreeQty());
					editTextFreeQty.setText(item.getFreeQty());

				}
				if(item.getqty().equals("")){

					editTextRefKey.setText("");
				}else {
					Log.e("freeqty",item.getqty());
					editTextRefKey.setText(item.getqty());

				}
				if (item.getprice().equals("")) {

					editTextRefKeyprice.setText("");
				} else {
					editTextRefKeyprice.setText(item.getprice());

				}
				if (item.getDeliverymode().equals("")) {
					Log.e("ins", "ins");
					ArrayAdapter<String> deliverydapt = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, Constants.deliverymode);
					textviewdelivery.setAdapter(deliverydapt);

				} else {
					Log.e("getDeliverymode", item.getDeliverymode());
					if (item.getDeliverymode().equals("Standard")) {
						linearlayoutdatetime.setVisibility(View.GONE);
						textviewdelivery.setText(item.getDeliverymode());
						editTextTime.setText("");
						editTextDate.setText("");
					} else if (item.getDeliverymode().equals("Scheduled")) {

						Log.e("deliverytime", item.getDeliverytime());
						Log.e("deliverydate", item.getDeliverydate());
						textviewdelivery.setText(item.getDeliverymode());
						linearlayoutdatetime.setVisibility(View.VISIBLE);
						editTextTime.setText(item.getDeliverytime());
						editTextDate.setText(item.getDeliverydate());
					}
				}

				editTextFreeQty.setSelection(editTextFreeQty.getText().length());
				editTextRefKey.setSelection(editTextRefKey.getText().length());
				editTextRefKeyprice.setSelection(editTextRefKeyprice.getText().length());
				editTextTime.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						final int mHour, mMinute,mSeconds;
						final Calendar c = Calendar.getInstance();
						mHour = c.get(Calendar.HOUR_OF_DAY);
						mMinute = c.get(Calendar.MINUTE);
						mSeconds = c.get(Calendar.SECOND);

						// Launch Time Picker Dialog
						TimePickerDialog timePickerDialog = new TimePickerDialog(context,
								new TimePickerDialog.OnTimeSetListener() {

									@Override
									public void onTimeSet(TimePicker view, int hourOfDay,
														  int minute) {
										String minLength = String.valueOf(minute);
										if (minLength.length() == 1) {
											minLength = "0" + minLength;
										}
										editTextTime.setText(hourOfDay + ":" + minLength);
									}
								}, mHour, mMinute, false);
						timePickerDialog.show();
					}
				});

				editTextDate.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// Get Current Date
						int mYear, mMonth, mDay;
						final Calendar c = Calendar.getInstance();
						mYear = c.get(Calendar.YEAR);
						mMonth = c.get(Calendar.MONTH);
						mDay = c.get(Calendar.DAY_OF_MONTH);

						DatePickerDialog datePickerDialog = new DatePickerDialog(context,
								new DatePickerDialog.OnDateSetListener() {

									@Override
									public void onDateSet(DatePicker view, int year,
														  int monthOfYear, int dayOfMonth) {

										String monthLength = String.valueOf(monthOfYear);
										if (monthLength.length() == 1) {
											monthLength = "0" + monthLength;
										}
										String dayLength = String.valueOf(dayOfMonth);
										if (dayLength.length() == 1) {
											dayLength = "0" + dayLength;
										}
										editTextDate.setText(dayLength + "-" + (Integer.parseInt(monthLength) + 1) + "-" + year);

									}
								}, mYear, mMonth, mDay);
						datePickerDialog.show();
					}
				});

				textviewdelivery.setOnTouchListener(new View.OnTouchListener() {
					@Override
					public boolean onTouch(View v, MotionEvent event) {
						textviewdelivery.setText("");
						ArrayAdapter<String> deliverydapt = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, Constants.deliverymode);
						textviewdelivery.setAdapter(deliverydapt);
						textviewdelivery.showDropDown();
						InputMethodManager iss = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
						iss.hideSoftInputFromWindow(textviewdelivery.getWindowToken(), 0);
						return false;
					}
				});

				textviewdelivery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
						selecteddeliverymode=textviewdelivery.getText().toString();
						if (selecteddeliverymode.equals("Scheduled")) {
							linearlayoutdatetime.setVisibility(View.VISIBLE);
							final Calendar c = Calendar.getInstance();
							mYear = c.get(Calendar.YEAR);
							mMonth = c.get(Calendar.MONTH);
							mDay = c.get(Calendar.DAY_OF_MONTH);
							mHour = c.get(Calendar.HOUR_OF_DAY);
							mMinute = c.get(Calendar.MINUTE);
							mSeconds= c.get(Calendar.SECOND);

							String minuteLength = String.valueOf(mMinute);
							if (minuteLength.length() == 1) {
								minuteLength = "0" + minuteLength;
							}

							monthLength = String.valueOf(mMonth + 1);
							if (monthLength.length() == 1) {
								monthLength = "0" + monthLength;
							}
							dayLength = String.valueOf(mDay);
							if (dayLength.length() == 1) {
								dayLength = "0" + dayLength;
							}

							// set current date into textview
							editTextDate.setText(new StringBuilder()
									// Month is 0 based, just add 1
									.append(dayLength).append("-").append(monthLength).append("-").append(mYear)
							);

							editTextTime.setText(new StringBuilder()
									// Month is 0 based, just add 1
									.append(mHour).append(":").append(minuteLength)
							);
						} else {
							linearlayoutdatetime.setVisibility(View.GONE);
							editTextTime.setText("");
							editTextDate.setText("");
						}

					}
				});
				qtyDialog.show();

				buttonUpdateOk.setOnClickListener(new OnClickListener() {

													  @Override
													  public void onClick(View v) {
														  dod = new MerchantOrderDomainSelected();
														  qty = editTextRefKey.getText().toString();
														  freeQty = editTextFreeQty.getText().toString();
														  price=editTextRefKeyprice.getText().toString();
														  freeqtyuom = textviewfreeuom.getText().toString();

														  orderuom = textviewuom.getText().toString();
                                                          Log.e("Order UOM 1>>",orderuom);
														  selecteddeliverymode=textviewdelivery.getText().toString();

														   if(Constants.USER_TYPE.equals("S")){
															  deliverydate = editTextDate.getText().toString();
															  deliverymode = selecteddeliverymode;
															  deliverytime = editTextTime.getText().toString();
														  }else  if(Constants.USER_TYPE.equals("M")){
															  deliverydate ="";
															  deliverymode = "";
															  deliverytime = "";
														  }
														  editTextRefKeyprice.setOnTouchListener(new View.OnTouchListener() {
															  @Override
															  public boolean onTouch(View v, MotionEvent event) {
																  editTextRefKeyprice.setCursorVisible(true);
																  return false;
															  }
														  });
														  //uom
                                                          if(!textviewuom.getText().toString().equals("")){
                                                              item.setorderuom(textviewuom.getText().toString());
                                                              homeHolder.textViewUomName.setText("UOM : "+item.getorderuom());
															  Cursor curs;
                                                              Log.e("Product ID >>",item.getprodid());
                                                              Log.e("Order UOM 2>>",orderuom);
															  curs = databaseHandler.getUomUnit(item.getprodid(),orderuom);
															  curs.moveToFirst();
															  if(curs!=null && curs.getCount() > 0) {
                                                                  Log.e("Selected UOM >>",curs.getString(0));
																  item.setSelecetdQtyUomUnit(curs.getString(0));;
																  Log.e("checkorderuom 1",orderuom);
																  Log.e("checkQtyUomunit 1",curs.getString(0));
															  }
														  }/*else{
															  item.setorderuom(item.getUOM());
															  item.setSelecetdQtyUomUnit(item.getUnitPerUom());

														  }*/
														  if( !textviewfreeuom.getText().toString().equals("")){
															  item.setselectedfreeUOM(textviewfreeuom.getText().toString());
															  // homeHolder.textViewUomName.setText("UOM : "+item.getSlectedUOM());
															  Cursor curs1;
															  curs1 = databaseHandler.getUomUnit(item.getprodid(),freeqtyuom);
															  curs1.moveToFirst();
															  if(curs1!=null && curs1.getCount() > 0) {
																  item.setSelecetdFreeQtyUomUnit(curs1.getString(0));;
															  }
														  }/*else{
															  item.setselectedfreeUOM(item.getUOM());
															  item.setSelecetdFreeQtyUomUnit(item.getUnitPerUom());
														  }*/

														  InputMethodManager inn = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
														  inn.hideSoftInputFromWindow(buttonUpdateOk.getWindowToken(), 0);
														  InputMethodManager iss = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
														  iss.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

														  //deliverymode set
														  if(selecteddeliverymode.equals("Standard")){
															  item.setDeliverymode(selecteddeliverymode);
															  item.setDeliverydate("");
															  item.setDeliverytime("");
														  }else if(selecteddeliverymode.equals("Scheduled")){
															  item.setDeliverymode(selecteddeliverymode);
															  item.setDeliverydate(editTextDate.getText().toString());
															  item.setDeliverytime(editTextTime.getText().toString());
														  }else {
															  item.setDeliverydate("");
															  item.setDeliverytime("");
															  item.setDeliverymode("");
														  }


														  if (qty.isEmpty() && freeQty.isEmpty() || qty.equals("0") && freeQty.equals("0")|| qty.equals("0")&&freeQty.isEmpty()||
																  qty.isEmpty()&&freeQty.equals("0")) {
															  textQtyValidate.setText("Please enter valid quantity");
														  }else if(selecteddeliverymode.equals("")){
															  textQtyValidate.setText("Please select Delivery Mode");
														  } else {
															  if (qty.isEmpty() || qty.equals(".")) {
																  item.setqty("0");
															  } else {
																  item.setqty(qty);
															  }
															  if (freeQty.isEmpty() || freeQty.equals(".")) {
																  item.setFreeQty("0");
															  } else {
																  item.setFreeQty(freeQty);
															  }
															  if (price.isEmpty() || price.equals(".")) {
																  item.setprice(price);
															  } else {
																  item.setprice(price);
															  }
															  homeHolder.editTextQuantity.setText("NF: [" + item.getqty() + "]" + "\n" + "F: [" + item.getFreeQty() + "]"); //+" "+item.getUOM()

															  if (Constants.USER_TYPE.equals("M")) {
																  for (MerchantOrderDetailDomain pd : MerchantOrderDetailActivity.arraylistMerchantOrderDetailList) {
																	  if (pd.getprodid().equals(item.getprodid())) {
																		  pd.setqty(item.getqty());
																		  pd.setFreeQty(item.getFreeQty());
																		  pd.setSelectedUOM(item.getorderuom());
																		  pd.setprodprice(item.getprice());
																		  pd.setselectedfreeUOM(item.getorderfreeuom());
																		  pd.setSelecetdFreeQtyUomUnit(item.getSelectedFreeQtyUomUnit());
																		  pd.setSelecetdQtyUomUnit(item.getSelectedQtyUomUnit());
																		  pd.setDeliverydate("");
																		  pd.setDeliverymode("");
																		  pd.setDeliverytime("");
																	  }
																  }
															  } else {

																	  for (MerchantOrderDetailDomain pd : CreateOrderActivity.arraylistMerchantOrderDetailList) {
																		  if (pd.getprodid().equals(item.getprodid())) {
																			  pd.setqty(item.getqty());
																			  pd.setFreeQty(item.getFreeQty());
																			  pd.setSelectedUOM(item.getorderuom());
																			  pd.setprodprice(item.getprice());
																			  pd.setselectedfreeUOM(item.getorderfreeuom());
																			  pd.setDeliverydate(item.getDeliverydate());
																			  pd.setDeliverymode(item.getDeliverymode());
																			  pd.setDeliverytime(item.getDeliverytime());
																			  pd.setSelecetdFreeQtyUomUnit(item.getSelectedFreeQtyUomUnit());
																			  pd.setSelecetdQtyUomUnit(item.getSelectedQtyUomUnit());

																		  }
																	  }

															  }
															  qtyDialog.dismiss();
														  }

													  }
												  }

				);

					buttonUpdateCancel
							.setOnClickListener(new OnClickListener()

												{

													@Override
													public void onClick(View v) {
														// TODO Auto-generated method stub
														InputMethodManager inn = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
														inn.hideSoftInputFromWindow(buttonUpdateOk.getWindowToken(), 0);
														InputMethodManager iss = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
														iss.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
														textviewdelivery.setText("Standard");
														qtyDialog.dismiss();
													}


												}

							);
				}
			});

		return row;
	}


	class HomeHolder {
      RelativeLayout relativeLayoutAddQuantity;
   		TextView textViewName, textViewSubname,textViewPlus,serialno,textViewUomName;
   		EditText editTextQuantity;
	}

}