package com.freshorders.freshorder.adapter;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.domain.DealerProductDomain;
import com.freshorders.freshorder.ui.AddDealerActivityNew;
import com.freshorders.freshorder.ui.ProductActivity;
import com.freshorders.freshorder.utils.JsonServiceHandler;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class DealerProductListAdapter extends ArrayAdapter<DealerProductDomain> {
	Context context;
	int resource;
	ArrayList<DealerProductDomain> arrayListServiceDomainObjects;

	JSONObject JsonAccountObject = null;
	JSONArray JsonAccountArray = null;
	JsonServiceHandler JsonServiceHandler;

	public DealerProductListAdapter(Context context, int resource,
			ArrayList<DealerProductDomain> arrayListServiceDomainObjects) {
		super(context, resource, arrayListServiceDomainObjects);
		this.context = context;
		this.resource = resource;
		this.arrayListServiceDomainObjects = arrayListServiceDomainObjects;
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();
	}

	@Override
	public int getCount() {
		return arrayListServiceDomainObjects.size();
	}

	@Override
	public boolean isEnabled(int position) {
		return true;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// Log.e("Adapter position",String.valueOf(position));
		View row = convertView;
		final HomeHolder homeHolder;
		if (row == null) {
			// Log.e("Adapter Inner position",String.valueOf(position));
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(resource, parent, false);
			homeHolder = new HomeHolder();

			homeHolder.textViewProductName = (TextView) row
					.findViewById(R.id.textViewProductName);
			homeHolder.textViewNextIcon = (TextView) row
					.findViewById(R.id.textViewNextIcon);
			homeHolder.serialnonew = (TextView) row
					.findViewById(R.id.serialnonew);
			homeHolder.textViewNextIcon.setTypeface(ProductActivity.font);
		

			row.setTag(homeHolder);
		} else {
			homeHolder = (HomeHolder) row.getTag();
		}
		DealerProductDomain item = arrayListServiceDomainObjects
				.get(position);
		homeHolder.textViewProductName.setText(item.getProductName()+"  ["+item.getProductCode()+"]");
		homeHolder.serialnonew.setText(item.getserialno()+".");
		homeHolder.textViewNextIcon.setText(Html.fromHtml("&#xf105;"));
		

		return row;
	}

	class HomeHolder {

		TextView textViewProductName, textViewAddress,textViewNextIcon,serialnonew;
	}

}