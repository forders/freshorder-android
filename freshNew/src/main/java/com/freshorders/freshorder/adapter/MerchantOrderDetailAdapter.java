package com.freshorders.freshorder.adapter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.MerchantOrderDetailDomain;
import com.freshorders.freshorder.domain.MerchantOrderDomainSelected;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.ui.CreateOrderActivity;
import com.freshorders.freshorder.ui.MerchantOrderDetailActivity;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;
import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

public class MerchantOrderDetailAdapter extends ArrayAdapter<MerchantOrderDetailDomain> {
	Context context;
	int resource;
	ArrayList<MerchantOrderDetailDomain> arraylistMerchantOrderDetailList;
	ArrayList<MerchantOrderDetailDomain>arrayListSearchResults;
	public static ArrayList<MerchantOrderDomainSelected> arraylistMerchantOrderDetailListSelected, arraylistsavearray;
	JSONObject JsonAccountObject = null;
	JSONArray JsonAccountArray = null;
	JsonServiceHandler JsonServiceHandler;
	String qty = "", freeQty = "", orderuom = "",price = "", defaultuom = "", freeqtyuom = "",deliverydate="",deliverytime="",deliverymode="",currstock="";
	public static String prodid, multipleUomCount = "",selecteddeliverymode="",monthLength="",dayLength="";
	//int index=1;
	public static EditText editTextSearchField;
	public static TextView textViewHeader, textQtyValidat;
	public static MerchantOrderDomainSelected dod;
	DatabaseHandler databaseHandler;
	public static int mYear, mMonth, mDay, mHour, mMinute,mSeconds;
	private boolean netcheck;

	//public static String searchclick="0";
	public MerchantOrderDetailAdapter(
			Context context,
			int resource,
			ArrayList<MerchantOrderDetailDomain> arraylistMerchantOrderDetailList, Boolean netcheck) {
		super(context, resource, arraylistMerchantOrderDetailList);
		Log.e("MerchantOrdDtlAdapter",netcheck.toString());
        this.context = context;
		this.resource = resource;
		this.arraylistMerchantOrderDetailList = arraylistMerchantOrderDetailList;
		this.netcheck = netcheck;
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();
		databaseHandler = new DatabaseHandler(context);

}

	@Override
	public int getCount() {
		return arraylistMerchantOrderDetailList.size();
	}

	@Override
	public boolean isEnabled(int position) {
		return true;
	}

	public void onlineStockValue(final TextView textViewOpenStkVal, final TextView textViewDistStkVal, final String prodId)
    {
		if(Constants.distributorname == null || Constants.distributorname.isEmpty()){
			Constants.distributorname = new PrefManager(context).getStringDataByKey(PrefManager.KEY_DISTRIBUTOR_NAME);
		}


		new AsyncTask<Void, Void, Void>() {
			String strStatus = "", strMsg;
			ProgressDialog dialog;
			String city;


			@Override
			protected void onPreExecute() {

			}
			@Override
			protected void onPostExecute(Void result) {

				try {
					Log.e("onPostExecute","onlineStockValue");
					strStatus = JsonAccountObject.getString("status");
					Log.e("return status", strStatus);
					 strMsg = JsonAccountObject.getString("message");
					Log.e("returnmessage", strMsg);
					if(strStatus.equalsIgnoreCase("false")){
						textViewOpenStkVal.setText("Stock Position not available");
                    }
                    else {
						textViewOpenStkVal.setText(JsonAccountObject.getString("mclosing"));
						textViewDistStkVal.setText(JsonAccountObject.getString("dclosing"));
					}

				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}


			@Override
			protected Void doInBackground(Void... params) {

				JSONObject jsonObject = new JSONObject();
				try {
					jsonObject.put("muserid", Constants.SalesMerchant_Id);
					jsonObject.put("prodid", prodId);
					jsonObject.put("distributorname", Constants.distributorname);

					Log.e("OutletStkRqst",jsonObject.toString());

					JsonServiceHandler = new JsonServiceHandler(Utils.strMerchantStockPosition, jsonObject.toString(), getContext());
					JsonAccountObject = JsonServiceHandler.ServiceData();
				} catch (JSONException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
				return null;
			}
		}.execute(new Void[]{});
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// Log.e("Adapter position",String.valueOf(position));
		View row = convertView;
		final HomeHolder2 homeHolder;

		if (row == null) {
			// Log.e("Adapter Inner position",String.valueOf(position));
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(resource, parent, false);
			homeHolder = new HomeHolder2();
			homeHolder.textViewName = (TextView) row
					.findViewById(R.id.textViewName);
			homeHolder.imageView = (ImageView) row
					.findViewById(R.id.imageView);
			homeHolder.textViewSubname = (TextView) row
					.findViewById(R.id.textViewSubname);
			homeHolder.editTextQuantity = (EditText) row
					.findViewById(R.id.editTextClosingStock);
			homeHolder.serialno = (TextView) row
					.findViewById(R.id.serialno);
			homeHolder.textViewUomName = (TextView) row
					.findViewById(R.id.textViewUomName);

			row.setTag(homeHolder);
		} else {
			homeHolder = (HomeHolder2) row.getTag();
		}
		final MerchantOrderDetailDomain item = arraylistMerchantOrderDetailList
				.get(position);

		arraylistsavearray = new ArrayList<MerchantOrderDomainSelected>();
		homeHolder.textViewName.setText(Html.fromHtml(item.getprodname() + "  ["
				+ item.getprodcode() + "]"));
		homeHolder.textViewSubname.setText(item.getCompanyname() + "  [" + item.getDuserid() + "]");
		homeHolder.textViewUomName.setText("UOM : "+item.getSlectedUOM());
		if(item.getProdimage().equalsIgnoreCase("null")){
			homeHolder.imageView.setImageResource(R.drawable.product);
		}else {
			if (item != null) {
				Picasso.with(this.getContext())
						.load(item.getProdimage())
						.into(homeHolder.imageView);
			}
		}
		homeHolder.serialno.setText(item.getserialno() +".");


		if(!item.getqty().equals("")){
			homeHolder.editTextQuantity.setText("NF: ["+item.getqty() +"]"+"\n" + "F: [" + item.getFreeQty() + "]");

           /* if(!item.getUOM().isEmpty()){
                homeHolder.editTextQuantity.setText("NF: ["+item.getqty() +"]"+"\n" + "F: [" + item.getFreeQty() + "]" + " " + item.getUOM());
            }else{
                homeHolder.editTextQuantity.setText("NF: ["+item.getqty() +"]"+"\n" + "F: [" + item.getFreeQty() + "]");

            }*/
		}else{
            /*if(!item.getUOM().isEmpty()) {
                homeHolder.editTextQuantity.setText( item.getqty() +"\n"+ item.getFreeQty()+" "+item.getUOM());
            }*/
			homeHolder.editTextQuantity.setText( item.getqty() +"\n"+ item.getFreeQty());

		}


		homeHolder.editTextQuantity.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				final Dialog qtyDialog = new Dialog(context);
				qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				qtyDialog.setContentView(R.layout.refkey_dialogue);

				final Button buttonUpdateOk, buttonUpdateCancel;
				InputMethodManager iss = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
				iss.hideSoftInputFromWindow(homeHolder.editTextQuantity.getWindowToken(), 0);
				InputMethodManager inn = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
				inn.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
				arrayListSearchResults = new ArrayList<MerchantOrderDetailDomain>();

				final EditText editTextRefKey,editTextRefKeyprice, editTextFreeQty, editTextfqtyuom, editTextTime, editTextDate, editTextCurrStock;
				final TextView textQtyValidate, textViewOpenStkVal, textViewDistStkVal;
				final AutoCompleteTextView textviewuom, textviewfreeuom,textviewdelivery;
				final LinearLayout linearlayoutdatetime,linearlayoutdeliverymode,linearlayoutrefkey;


                buttonUpdateOk = (Button) qtyDialog
						.findViewById(R.id.buttonUpdateOk);
				buttonUpdateCancel = (Button) qtyDialog
						.findViewById(R.id.buttonUpdateCancel);

                textViewOpenStkVal = (TextView) qtyDialog.findViewById(R.id.textViewOpenStkVal);
				textViewDistStkVal = (TextView) qtyDialog.findViewById(R.id.textViewDistStkVal);

				if (netcheck) {
					onlineStockValue(textViewOpenStkVal, textViewDistStkVal, item.getprodid());
				}
				else {
					textViewOpenStkVal.setText("Outlet stock not available");
					textViewDistStkVal.setText("Distributor stock not available");

				}
				editTextRefKey = (EditText) qtyDialog
						.findViewById(R.id.editTextRefKey);
				editTextRefKeyprice= (EditText) qtyDialog
						.findViewById(R.id.editTextRefKeyprice);
				editTextFreeQty = (EditText) qtyDialog
						.findViewById(R.id.editTextFreeQty);
				editTextTime = (EditText) qtyDialog
						.findViewById(R.id.editTextTime);
				editTextDate = (EditText) qtyDialog
						.findViewById(R.id.editTextDate);
				textviewdelivery = (AutoCompleteTextView) qtyDialog
						.findViewById(R.id.textviewdelivery);
				linearlayoutdatetime = (LinearLayout) qtyDialog
						.findViewById(R.id.linearlayoutdatetime);
				linearlayoutdeliverymode= (LinearLayout) qtyDialog
						.findViewById(R.id.linearlayoutdeliverymode);
				//textViewHeader = (TextView) findViewById(R.id.textViewHeader);
				textQtyValidate = (TextView) qtyDialog.findViewById(R.id.textQtyValidate);
				textviewuom = (AutoCompleteTextView) qtyDialog.findViewById(R.id.textviewuom);
				textviewfreeuom = (AutoCompleteTextView) qtyDialog.findViewById(R.id.textviewfreeuom);
				editTextCurrStock = (EditText) qtyDialog
						.findViewById(R.id.editTextCurrStock);

				if (Constants.USER_TYPE.equals("M")) {
					linearlayoutdatetime.setVisibility(linearlayoutdatetime.GONE);
					linearlayoutdeliverymode.setVisibility(linearlayoutdeliverymode.GONE);
					textviewuom.setVisibility(textviewuom.GONE);
					textviewfreeuom.setVisibility(textviewfreeuom.GONE);
					editTextRefKeyprice.setVisibility((editTextRefKeyprice.GONE));
				}

				if(textviewdelivery.equals("")){
					textviewdelivery.setText("Standard");
				}
				if (item.getFreeQty().equals("")) {
					editTextFreeQty.setText("");
				} else {
					editTextFreeQty.setText(item.getFreeQty());

				}
				if (item.getCurrStock().equals("")) {
					editTextCurrStock.setText("");
				} else {
					editTextCurrStock.setText(item.getCurrStock());

				}
				if (item.getqty().equals("")) {

					editTextRefKey.setText("");
				} else {
					editTextRefKey.setText(item.getqty());

				}
				if (item.getprodprice().equals("")) {

					editTextRefKeyprice.setText("");
				} else {
					editTextRefKeyprice.setText(item.getprodprice());

				}
				editTextFreeQty.setSelection(editTextFreeQty.getText().length());
				editTextRefKey.setSelection(editTextRefKey.getText().length());
				editTextRefKeyprice.setSelection(editTextRefKeyprice.getText().length());
				try {
					if (item.getDeliverymode().equals("")) {
						textviewdelivery.setText("Standard");
						ArrayAdapter<String> deliverydapt = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, Constants.deliverymode);
						textviewdelivery.setAdapter(deliverydapt);

					} else {
						if (item.getDeliverymode().equals("Standard")) {
							textviewdelivery.setText(item.getDeliverymode());
							linearlayoutdatetime.setVisibility(View.GONE);
							editTextTime.setText("");
							editTextDate.setText("");
						} else if (item.getDeliverymode().equals("Scheduled")){
							textviewdelivery.setText(item.getDeliverymode());
							linearlayoutdatetime.setVisibility(View.VISIBLE);
							editTextTime.setText(item.getDeliverytime());
							editTextDate.setText(item.getDeliverydate());
						}
					}
				}catch (Exception e){

				}
				String prodiduom=item.getprodid();
				Cursor cur;
				cur = databaseHandler.getUOMforproduct(prodiduom);
				multipleUomCount =String.valueOf(cur.getCount());
				Log.e("multipleUomCount",multipleUomCount);
				String[] array = new String[cur.getCount()];
				cur.moveToFirst();
				//8-9-2017
				for(int i=0;i<cur.getCount();i++)
				{
					//array[0] = item.getUOM();
					array[i] = cur.getString(cur.getColumnIndex("uomname"));
					Log.e("array[i+1]",array[i]);
					cur.moveToNext();
				}


				ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, array);
				textviewuom.setAdapter(adapter);
				textviewfreeuom.setAdapter(adapter);


				if(item.getSlectedUOM().equals("")){
					textviewuom.setHint(item.getUOM());
				}else{
					textviewuom.setHint(item.getSlectedUOM());
				}

				if(item.getselectedfreeUOM().equals("")){
					textviewfreeuom.setHint(item.getUOM());

				}else{
					textviewfreeuom.setHint(item.getselectedfreeUOM());
				}

				defaultuom=item.getUOM();
				textviewuom.setOnTouchListener(new View.OnTouchListener() {
					@Override
					public boolean onTouch(View v, MotionEvent event) {
						if(!multipleUomCount.equals("0")){
							textviewuom.showDropDown();
						}

						return false;
					}
				});

				editTextRefKeyprice.setOnTouchListener(new View.OnTouchListener() {
					@Override
					public boolean onTouch(View v, MotionEvent event) {

						editTextRefKeyprice.setCursorVisible(true);
						return false;
					}
				});

				textviewfreeuom.setOnTouchListener(new View.OnTouchListener() {
					@Override
					public boolean onTouch(View v, MotionEvent event) {
						if(!multipleUomCount.equals("0")){
							textviewfreeuom.showDropDown();
						}

						return false;
					}
				});

				editTextTime.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						final int mHour, mMinute,mSeconds;
						final Calendar c = Calendar.getInstance();
						mHour = c.get(Calendar.HOUR_OF_DAY);
						mMinute = c.get(Calendar.MINUTE);
						mSeconds = c.get(Calendar.SECOND);
						// Launch Time Picker Dialog
						TimePickerDialog timePickerDialog = new TimePickerDialog(context,
								new TimePickerDialog.OnTimeSetListener() {
									@Override
									public void onTimeSet(TimePicker view, int hourOfDay,
														  int minute) {
										String minLength = String.valueOf(minute);
										if (minLength.length() == 1) {
											minLength = "0" + minLength;
										}
										editTextTime.setText(hourOfDay + ":" + minLength);
									}
								}, mHour, mMinute, false);
						timePickerDialog.show();
					}
				});

				editTextDate.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// Get Current Date
						int mYear, mMonth, mDay;
						final Calendar c = Calendar.getInstance();
						mYear = c.get(Calendar.YEAR);
						mMonth = c.get(Calendar.MONTH);
						mDay = c.get(Calendar.DAY_OF_MONTH);

						DatePickerDialog datePickerDialog = new DatePickerDialog(context,
								new DatePickerDialog.OnDateSetListener() {

									@Override
									public void onDateSet(DatePicker view, int year,
														  int monthOfYear, int dayOfMonth) {

										String monthLength = String.valueOf(monthOfYear);
										if (monthLength.length() == 1) {
											monthLength = "0" + monthLength;
										}
										String dayLength = String.valueOf(dayOfMonth);
										if (dayLength.length() == 1) {
											dayLength = "0" + dayLength;
										}
										editTextDate.setText(dayLength + "-" + (Integer.parseInt(monthLength) + 1) + "-" + year);

									}
								}, mYear, mMonth, mDay);
						datePickerDialog.show();
					}
				});

				textviewdelivery.setOnItemClickListener( new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
						selecteddeliverymode=textviewdelivery.getText().toString();
						if (selecteddeliverymode.equals("Scheduled")) {
							linearlayoutdatetime.setVisibility(View.VISIBLE);
							final Calendar c = Calendar.getInstance();
							mYear = c.get(Calendar.YEAR);
							mMonth = c.get(Calendar.MONTH);
							mDay = c.get(Calendar.DAY_OF_MONTH);
							mHour = c.get(Calendar.HOUR_OF_DAY);
							mMinute = c.get(Calendar.MINUTE);
							mSeconds= c.get(Calendar.SECOND);
							monthLength = String.valueOf(mMonth + 1);
							if (monthLength.length() == 1) {
								monthLength = "0" + monthLength;
							}
							dayLength = String.valueOf(mDay);
							if (dayLength.length() == 1) {
								dayLength = "0" + dayLength;
							}

							String minuteLength = String.valueOf(mMinute);
							if (minuteLength.length() == 1) {
								minuteLength = "0" + minuteLength;
							}

							String AM_PM ;
							if(mHour < 12) {
								AM_PM = "AM";
							} else {
								AM_PM = "PM";
							}
							// set current date into textview
							editTextDate.setText(new StringBuilder()
									// Month is 0 based, just add 1
									.append(dayLength).append("-").append(monthLength).append("-").append(mYear)
							);

							editTextTime.setText(new StringBuilder()
									// Month is 0 based, just add 1
									.append(mHour).append(":").append(minuteLength)
							);

						} else {
							linearlayoutdatetime.setVisibility(View.GONE);
							editTextTime.setText("");
							editTextDate.setText("");
						}

					}
				});

				textviewdelivery.setOnTouchListener(new View.OnTouchListener() {
					@Override
					public boolean onTouch(View v, MotionEvent event) {
						textviewdelivery.setText("");
						ArrayAdapter<String> deliverydapt = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, Constants.deliverymode);
						textviewdelivery.setAdapter(deliverydapt);
						textviewdelivery.showDropDown();
						InputMethodManager iss = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
						iss.hideSoftInputFromWindow(textviewdelivery.getWindowToken(), 0);
						return false;
					}
				});

				qtyDialog.show();
				buttonUpdateOk.setOnClickListener(new OnClickListener() {

													  @Override
													  public void onClick(View v) {
														  dod= new MerchantOrderDomainSelected();
														  //dod = new MerchantOrderDetailDomain();
														  qty = editTextRefKey.getText().toString();
														  price = editTextRefKeyprice.getText().toString();
														  currstock = editTextCurrStock.getText().toString();

                                                          Log.e( "Order prodprice: ",price );
														  freeQty = editTextFreeQty.getText().toString();
														  orderuom = textviewuom.getText().toString();
														  Log.e( "Order UOM111111: ",orderuom );
														  freeqtyuom = textviewfreeuom.getText().toString();
														  selecteddeliverymode= textviewdelivery.getText().toString();
														  if(Constants.USER_TYPE.equals("S")){
															  deliverydate = editTextDate.getText().toString();
															  deliverymode = textviewdelivery.getText().toString();
															  deliverytime = editTextTime.getText().toString();
														  }else if(Constants.USER_TYPE.equals("M")){
															  deliverydate ="";
															  deliverymode = "";
															  deliverytime = "";
															  price="";
														  }

														  if( !textviewuom.getText().toString().equals("")){
															  item.setSelectedUOM(textviewuom.getText().toString());
															  homeHolder.textViewUomName.setText("UOM : "+item.getSlectedUOM());
															  Cursor curs;
															  Log.e( "Databaese : ","" );
															  Log.e( "Order UOM: ",orderuom );
															  curs = databaseHandler.getUomUnit(item.getprodid(),orderuom);
															  curs.moveToFirst();
															  if(curs!=null && curs.getCount() > 0) {
																  item.setSelecetdQtyUomUnit(curs.getString(0));;
																  Log.e("QtyUomunit",curs.getString(0));
																  Log.e("orderuom",orderuom);
															  }

														  }else{
															  item.setSelectedUOM(item.getUOM());
															  homeHolder.textViewUomName.setText("UOM : "+item.getUOM());
															  Cursor curs;
															  Log.e( "Database : ","" );
															  Log.e( "Order UOM: ",orderuom );
															  curs = databaseHandler.getUomUnit(item.getprodid(),defaultuom); //// findUOMUnit
															  curs.moveToFirst();
															  if(curs!=null && curs.getCount() > 0) {
																  item.setSelecetdQtyUomUnit(curs.getString(0));
																  //item.setSelecetdQtyUomUnit(item.getUnitPerUom());
																  Log.e("else   selectedUOM", item.getUOM());
																  Log.e("else   QtyUomUnit", item.getUnitPerUom());
																  Log.e("else   Orderuom", orderuom);
															  }

														  }

														  if( !textviewfreeuom.getText().toString().equals("")){
															  item.setselectedfreeUOM(textviewfreeuom.getText().toString());
															  homeHolder.textViewUomName.setText("UOM : "+item.getSlectedUOM());
															  Cursor curs1;
															  curs1 = databaseHandler.getUomUnit(item.getprodid(),freeqtyuom);
															  curs1.moveToFirst();
															  if(curs1!=null && curs1.getCount() > 0) {
																  item.setSelecetdFreeQtyUomUnit(curs1.getString(0));;
															  }
														  }else{
															  item.setselectedfreeUOM(item.getUOM());
															  homeHolder.textViewUomName.setText("UOM : "+item.getUOM());
															  Cursor curs1;
															  curs1 = databaseHandler.getUomUnit(item.getprodid(),defaultuom);
															  curs1.moveToFirst();
															  if(curs1!=null && curs1.getCount() > 0) {
																  item.setSelecetdFreeQtyUomUnit(curs1.getString(0));;
															  }
															  //item.setSelecetdFreeQtyUomUnit(item.getUnitPerUom());
														  }
														  if(selecteddeliverymode.equals("Standard")){
															  item.setDeliverymode(selecteddeliverymode);
															  item.setDeliverydate("");
															  item.setDeliverytime("");
														  }else if(selecteddeliverymode.equals("Scheduled")){
															  item.setDeliverymode(selecteddeliverymode);
															  item.setDeliverydate(editTextDate.getText().toString());
															  item.setDeliverytime(editTextTime.getText().toString());
														  }else {
															  item.setDeliverydate("");
															  item.setDeliverytime("");
															  item.setDeliverymode("");
														  }
														  arraylistMerchantOrderDetailListSelected = new ArrayList<MerchantOrderDomainSelected>();

														  if (Constants.USER_TYPE.equals("M")) {
															  MerchantOrderDetailActivity.textViewHeader.setVisibility(TextView.GONE);
															  MerchantOrderDetailActivity.editTextSearchField.setVisibility(EditText.VISIBLE);
															  MerchantOrderDetailActivity.editTextSearchField.setFocusable(true);
															  MerchantOrderDetailActivity.editTextSearchField.requestFocus();
															  MerchantOrderDetailActivity.textViewCross.setVisibility(TextView.VISIBLE);
														  } else {
														  	  CreateOrderActivity.textViewHeader.setVisibility(TextView.GONE);
															  CreateOrderActivity.editTextSearchField.setVisibility(EditText.VISIBLE);
															  CreateOrderActivity.editTextSearchField.setFocusable(true);
															  CreateOrderActivity.editTextSearchField.requestFocus();
															  CreateOrderActivity.textViewCross.setVisibility(TextView.VISIBLE);
														  }

														  InputMethodManager inn = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
														  inn.hideSoftInputFromWindow(buttonUpdateOk.getWindowToken(), 0);
														  InputMethodManager iss = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
														  iss.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

														  if (qty.isEmpty()&&freeQty.isEmpty() || qty.equals("0")&&freeQty.equals("0")|| qty.equals("0")&&freeQty.isEmpty()||
																  qty.isEmpty()&&freeQty.equals("0")) {
															  textQtyValidate.setText("Please enter valid quantity");
														  } else if(currstock.equals("") || currstock.isEmpty()){
															  textQtyValidate.setText("Please enter Current stock");
														  } else if(selecteddeliverymode.equals("")){
															  textQtyValidate.setText("Please select Delivery Mode");
														  } else {
															  if (qty.isEmpty() || qty.equals(".")) {
																  item.setqty("0");
															  } else {
																  item.setqty(qty);
															  }
															  if (freeQty.isEmpty() || freeQty.equals(".")) {
																  item.setFreeQty("0");
															  } else {
																  item.setFreeQty(freeQty);
															  }
															  if (price.isEmpty() || price.equals(".")) {
																  item.setprodprice(price);
															  } else {
																  item.setprodprice(price);

															  }
															  item.setCurrStock(currstock);

															  dod.setCurrStock(currstock);
															  dod.setFreeQty(item.getFreeQty());
															  dod.setDate(item.getDate());
															  dod.setprodcode(item.getprodcode());
															  dod.setprodid(item.getprodid());
															  dod.setprodname(item.getprodname());
															  dod.setqty(item.getqty());
															  dod.setOutstock("");
															  dod.setCompanyname(item.getCompanyname());
															  dod.setUserid(item.getDuserid());
															  dod.setprice(item.getprodprice());
															  dod.setptax(item.getprodtax());
															  // dod.setindex(item.getindex());
															  dod.setusrdlrid(item.getusrdlrid());
															  dod.setBatchno(item.getBatchno());
															  dod.setExpdate(item.getExpdate());
															  dod.setMgfDate(item.getMgfDate());
															  dod.setUOM(item.getUOM());
															  dod.setorderuom(item.getSlectedUOM());
															  dod.setorderfreeuom(item.getselectedfreeUOM());
															  dod.setUnitGram(item.getUnitGram());
															  dod.setUnitPerUom(item.getUnitPerUom());
															  dod.setDeliverydate(item.getDeliverydate());
															  dod.setDeliverymode(item.getDeliverymode());
															  dod.setDeliverytime(item.getDeliverytime());
															  dod.setSelecetdQtyUomUnit(item.getSelectedQtyUomUnit());
															  dod.setSelecetdFreeQtyUomUnit(item.getSelectedFreeQtyUomUnit());
															  if (Constants.USER_TYPE.equals("M")) {
																  prodid = item.getprodid();

																  if (updateQty() == 0) {
																	  MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.add(dod);
																	  for (int i = 0; i < MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.size(); i++) {
																		  dod.setserialno(String.valueOf(i+1));
																	  }
																  }

															  } else {
																  prodid = item.getprodid();
																  if (updateQty() == 0) {
																	  CreateOrderActivity.arraylistMerchantOrderDetailListSelected.add(dod);
																	  for (int i = 0; i < CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size(); i++) {
																		  dod.setserialno(String.valueOf(i+1));
																	  }
																  }

															  }


															 homeHolder.editTextQuantity.setText("NF: [" + item.getqty() + "]" + "\n" + "F: [" + item.getFreeQty() + "]"); //+" "+item.getUOM()


															  qtyDialog.dismiss();
														  }

													  }
												  }

				);

				buttonUpdateCancel
						.setOnClickListener(new View.OnClickListener()

											{

												@Override
												public void onClick(View v) {
													// TODO Auto-generated method stub
													if (Constants.USER_TYPE.equals("M")) {

														MerchantOrderDetailActivity.textViewHeader.setVisibility(TextView.GONE);
														MerchantOrderDetailActivity.editTextSearchField.setVisibility(EditText.VISIBLE);
														MerchantOrderDetailActivity.editTextSearchField.setFocusable(true);
														MerchantOrderDetailActivity.editTextSearchField.requestFocus();
														MerchantOrderDetailActivity.textViewCross.setVisibility(TextView.VISIBLE);
													} else {
														CreateOrderActivity.textViewHeader.setVisibility(TextView.GONE);
														CreateOrderActivity.editTextSearchField.setVisibility(EditText.VISIBLE);
														CreateOrderActivity.editTextSearchField.setFocusable(true);
														CreateOrderActivity.editTextSearchField.requestFocus();
														CreateOrderActivity.textViewCross.setVisibility(TextView.VISIBLE);
													}

													textviewdelivery.setText("Standard");
													InputMethodManager inn = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
													inn.hideSoftInputFromWindow(buttonUpdateCancel.getWindowToken(), 0);
													InputMethodManager iss = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
													iss.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

													qtyDialog.dismiss();
												}


											}

						);
			}
		});

		return row;
	}
	private boolean validation(String qty,String freeQty) {
		Pattern pattern = Pattern.compile("(^[1-9][0-9]*)+((\\.\\d)?)");

		Matcher matcherq = pattern.matcher(qty);
		Matcher matcherf = pattern.matcher(freeQty);
		if (matcherq.matches()){
			return matcherq.matches();
		} else if (matcherf.matches()){
			return matcherf.matches();
		}else{
			return false;
		}
	}

	public int updateQty()
	{
		if(Constants.USER_TYPE.equals("M")){
			for (int j = 0; j < MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.size(); j++) {
				Log.e("outsideprodid",prodid);
				Log.e("merchantprodid",MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.get(j).getprodid());
				if (MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.get(j).getprodid().equals(prodid)) {
					MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.get(j).setprice(price);
					MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.get(j).setFreeQty(freeQty);
					MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.get(j).setqty(qty);
					MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.get(j).setDeliverydate("");
					MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.get(j).setDeliverymode("");
					MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.get(j).setDeliverytime("");
					if (orderuom.toString().equals("")) {
						MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.get(j).setorderuom(defaultuom);

					}else{
						MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.get(j).setorderuom(orderuom);


					}
					if(freeqtyuom.toString().equals("")){
						CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(j).setorderfreeuom(defaultuom);

					}else{
						CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(j).setorderfreeuom(freeqtyuom);
					}
					Log.e("updatesize", String.valueOf(MerchantOrderDetailActivity.arraylistMerchantOrderDetailListSelected.size()));
					return 1;

				}
				Log.e("j", String.valueOf(j));

			}
			return 0;
		}else{
			for (int j = 0; j < CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size(); j++) {

				if (CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(j).getprodid().equals(prodid)) {
					CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(j).setCurrStock(currstock);
					CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(j).setFreeQty(freeQty);
					CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(j).setprice(price);
					CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(j).setqty(qty);
					CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(j).setDeliverydate(deliverydate);
					CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(j).setDeliverymode(deliverymode);
					CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(j).setDeliverytime(deliverytime);
					if (orderuom.toString().equals("")) {
						CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(j).setorderuom(defaultuom);

					}else{
						CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(j).setorderuom(orderuom);
					}

					if(freeqtyuom.toString().equals("")){
						CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(j).setorderfreeuom(defaultuom);

					}else{
						CreateOrderActivity.arraylistMerchantOrderDetailListSelected.get(j).setorderfreeuom(freeqtyuom);
					}

					Log.e("updatesize", String.valueOf(CreateOrderActivity.arraylistMerchantOrderDetailListSelected.size()));
					return 1;
				}
				Log.e("j", String.valueOf(j));

			}
			return 0;
		}


	}

	class HomeHolder2 {
		RelativeLayout relativeLayoutAddQuantity;
		TextView textViewName, textViewSubname,serialno,textViewUomName;
		ImageView imageView;
		EditText editTextQuantity;

	}

}