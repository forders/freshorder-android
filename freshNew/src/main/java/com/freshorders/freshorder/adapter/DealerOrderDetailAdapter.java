package com.freshorders.freshorder.adapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONObject;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.DealerOrderDetailDomain;
import com.freshorders.freshorder.domain.MerchantOrderDetailDomain;
import com.freshorders.freshorder.domain.ProductUomDomain;
import com.freshorders.freshorder.popup.Template1QuantityUpdatePopup;
import com.freshorders.freshorder.popup.Template2QuantityUpdatePopup;
import com.freshorders.freshorder.popup.Template3QuantityUpdatePopup;
import com.freshorders.freshorder.popup.Template4QuantityUpdatePopup;
import com.freshorders.freshorder.ui.DealerOrderDetailsActivity;
import com.freshorders.freshorder.ui.SalesManOrderActivity;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import static com.freshorders.freshorder.utils.Constants.QUANTITY_TEXT_FOR_TEMPLATE;

public class DealerOrderDetailAdapter extends
		ArrayAdapter<DealerOrderDetailDomain> {
	Context context;
	int resource;
	ArrayList<DealerOrderDetailDomain> arraylistDealerOrderDetailList;
	ArrayList<MerchantOrderDetailDomain> arraylistMerchantOrderDetailList;
	Context textViewDate;
	JSONObject JsonAccountObject = null;
	JSONArray JsonAccountArray = null;
	JsonServiceHandler JsonServiceHandler;
	DatePicker datePicker;
	TimePicker timePicker;
	public static String time,selecteddeliverymode="",monthLength="",dayLength="";
	private int year;
	private int month, date;
	private int day, today;
	String[] items;
	public static Date dateStr = null;
	String qty="0",prodPrice="",freeQty="0",multipleUomCount="",deliverydate="",deliverytime="",deliverymode="",orderuom,freeqtyuom;
    Float oldQty =0.0f;
    DatabaseHandler databaseHandler;
	public static int mYear, mMonth, mDay, mHour, mMinute,mSeconds;
	String[] array;

	private boolean isOrderProcessedIn = false;  // kumaravel need to check if online order is delivered or partial delivered
	//Kumaravel 14/01/2019
	private Cursor curOrderStatus;
	private void getOrderStatusAdapter() {
		try {
			curOrderStatus = databaseHandler.getOrderStatus(Integer.parseInt(SalesManOrderActivity.orderId));
			if (curOrderStatus != null && curOrderStatus.getCount() > 0) {
				curOrderStatus.moveToFirst();
				isOrderProcessedIn = curOrderStatus.getInt(0) > 0;
				curOrderStatus.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			isOrderProcessedIn = true; // Suppose Exception catched we don't know process status so we block
		}
	}

	public DealerOrderDetailAdapter(Context context, int resource,
			ArrayList<DealerOrderDetailDomain> arraylistDealerOrderDetailList) {
		super(context, resource, arraylistDealerOrderDetailList);
		this.context = context;
		this.resource = resource;
		this.arraylistDealerOrderDetailList = arraylistDealerOrderDetailList;
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();
        databaseHandler = new DatabaseHandler(context);
		getOrderStatusAdapter();
	}

	@Override
	public int getCount() {
		return arraylistDealerOrderDetailList.size();
	}

	@Override
	public boolean isEnabled(int position) {
		return true;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// Log.e("Adapter position",String.valueOf(position));
		View row = convertView;
		final HomeHolder homeHolder;
		if (row == null) {
			// Log.e("Adapter Inner position",String.valueOf(position));
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(resource, parent, false);
			homeHolder = new HomeHolder();

			homeHolder.textViewProductName = (TextView) row
					.findViewById(R.id.textViewProductName);
			homeHolder.textViewQty = (TextView) row
					.findViewById(R.id.textViewQty);
			homeHolder.textViewDate = (TextView) row
					.findViewById(R.id.textViewDate);
			homeHolder.textViewStatus = (TextView) row
					.findViewById(R.id.textViewStatus);
			homeHolder.textViewCalendar = (TextView) row
					.findViewById(R.id.textViewCalendar);
			homeHolder.textViewSpinner = (TextView) row
					.findViewById(R.id.textViewSpinner);
			homeHolder.relativeLayoutCalender = (RelativeLayout) row
					.findViewById(R.id.relativeLayoutCalender);
			homeHolder.spinnerStatus = (Spinner) row
					.findViewById(R.id.spinnerStatus);
			homeHolder.relativeLayoutStatus = (RelativeLayout) row
					.findViewById(R.id.relativeLayoutStatus);
			homeHolder.textViewUOMName= (TextView) row
					.findViewById(R.id.textViewUOMName);

			homeHolder.textViewPlus = (TextView) row
					.findViewById(R.id.textViewPlus);

			homeHolder.editTextQuantity = (EditText) row
					.findViewById(R.id.editTextClosingStock);
			Typeface font = Typeface.createFromAsset(context.getAssets(),
					"fontawesome-webfont.ttf");
			homeHolder.textViewCalendar.setTypeface(font);
			homeHolder.textViewSpinner.setTypeface(font);
			homeHolder.llRoot = (LinearLayout) row.findViewById(R.id.id_salesman_order_detail_root);
			homeHolder.ll_LV_stk = row.findViewById(R.id.ll_LV_stk);
			homeHolder.TV_stk = row.findViewById(R.id.TV_stk);
			row.setTag(homeHolder);
		} else {
			homeHolder = (HomeHolder) row.getTag();
		}
		final DealerOrderDetailDomain item = arraylistDealerOrderDetailList
				.get(position);

		homeHolder.textViewProductName.setText(item.getproductname());
		homeHolder.textViewStatus.setText(item.getordstatus());
	/*	if(!item.getUOM().isEmpty()){
            homeHolder.textViewQty.setText(item.getqty()+" ["+item.getFlag()+"]"+ " "+item.getUOM());
		}else{
			homeHolder.setText(item.getqty()+" ["+item.getFlag()+"]");
		}*/

		if(Constants.USER_TYPE.equals("D")){
			if(item.getFlag().equals("NF")){
				homeHolder.textViewQty.setText(item.getqty()+" ["+item.getFlag()+"]");
			}else {
				homeHolder.textViewQty.setText(item.getFreeQty() + " [" + item.getFlag() + "]");
			}

		}else{
			homeHolder.textViewQty.setText(item.getqty()+" ["+item.getFlag()+"]");

		}

		if(MyApplication.getInstance().isTemplate4() ||
				MyApplication.getInstance().isTemplate3() ||
				MyApplication.getInstance().isTemplate2() ||
				MyApplication.getInstance().isTemplate1()){
			//homeHolder.textViewQty.setText(item.getqty()+ QUANTITY_TEXT_FOR_TEMPLATE);
			homeHolder.textViewQty.setText(item.getqty());
		}

		if(MyApplication.getInstance().isTemplate2() ||
				MyApplication.getInstance().isTemplate1()){
			homeHolder.ll_LV_stk.setVisibility(View.VISIBLE);
			homeHolder.TV_stk.setText(item.getCurrStock());
		}else {
			homeHolder.ll_LV_stk.setVisibility(View.GONE);
		}


		homeHolder.textViewDate.setText(item.getDate());
        homeHolder.textViewUOMName.setText("UOM : "+item.getSelcetedUOM());
        /*Log.e("selected UOM",item.getSelcetedUOM());
        Log.e("default UOM",item.getUOM());*/

		if(Constants.USER_TYPE.equals("D")){
			homeHolder.spinnerStatus.setEnabled(true);
			homeHolder.relativeLayoutCalender.setEnabled(true);
			
		}else{
			homeHolder.spinnerStatus.setEnabled(false);
			homeHolder.relativeLayoutCalender.setEnabled(false);
		}


		  if(item.getordstatus().equals("Pending")){
			  if (Constants.USER_TYPE.equals("D")) {
					homeHolder.spinnerStatus.setEnabled(true);
					homeHolder.relativeLayoutCalender.setEnabled(true);
					items = new String[] { "Pending", "Inprogress", "Approved",
							"Cancelled" };
					ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
							android.R.layout.simple_spinner_dropdown_item, items);
					homeHolder.spinnerStatus.setAdapter(adapter);


				} else {
					String[] items1 = new String[] { "Pending",
							"Cancelled" };
					ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(context,
							android.R.layout.simple_spinner_dropdown_item, items1);
					homeHolder.spinnerStatus.setAdapter(adapter1);
					homeHolder.spinnerStatus.setEnabled(false);
					homeHolder.relativeLayoutCalender.setEnabled(false);
				}
	        	homeHolder.spinnerStatus.setSelection(0);
	        	homeHolder.spinnerStatus.setEnabled(true);
				homeHolder.relativeLayoutCalender.setEnabled(true);
	        }else if(item.getordstatus().equals("Approved")){
	        	if (Constants.USER_TYPE.equals("D")) {
					homeHolder.spinnerStatus.setEnabled(true);
					homeHolder.relativeLayoutCalender.setEnabled(true);
					items = new String[] { "Pending", "Inprogress", "Approved",
							"Cancelled" };
					ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
							android.R.layout.simple_spinner_dropdown_item, items);
					homeHolder.spinnerStatus.setAdapter(adapter);

				} else {
					String[] items1 = new String[] { "Pending", "Inprogress", "Approved",
							"Cancelled" };
					ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(context,
							android.R.layout.simple_spinner_dropdown_item, items1);
					homeHolder.spinnerStatus.setAdapter(adapter1);
					homeHolder.spinnerStatus.setEnabled(false);
					homeHolder.relativeLayoutCalender.setEnabled(false);
				}
	        	homeHolder.spinnerStatus.setSelection(2);
	        }else if(item.getordstatus().equals("Inprogress")){
	        	if (Constants.USER_TYPE.equals("D")) {
					homeHolder.spinnerStatus.setEnabled(true);
					homeHolder.relativeLayoutCalender.setEnabled(true);
					items = new String[] { "Pending", "Inprogress", "Approved",
							"Cancelled" };
					ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
							android.R.layout.simple_spinner_dropdown_item, items);
					homeHolder.spinnerStatus.setAdapter(adapter);

				} else {
					String[] items1 = new String[] { "Pending", "Inprogress", "Approved",
							"Cancelled" };
					ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(context,
							android.R.layout.simple_spinner_dropdown_item, items1);
					homeHolder.spinnerStatus.setAdapter(adapter1);
					homeHolder.spinnerStatus.setEnabled(false);
					homeHolder.relativeLayoutCalender.setEnabled(false);
				}
	        	homeHolder.spinnerStatus.setSelection(1);
	        }else if(item.getordstatus().equals("Cancelled")){
	        	if (Constants.USER_TYPE.equals("D")) {
					homeHolder.spinnerStatus.setEnabled(true);
					homeHolder.relativeLayoutCalender.setEnabled(true);
					items = new String[] { "Pending", "Inprogress", "Approved",
							"Cancelled" };
					ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
							android.R.layout.simple_spinner_dropdown_item, items);
					homeHolder.spinnerStatus.setAdapter(adapter);

				} else {
					String[] items = new String[] { "Pending","Inprogress", "Approved",
							"Cancelled" };
					ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(context,
							android.R.layout.simple_spinner_dropdown_item, items);
					homeHolder.spinnerStatus.setAdapter(adapter1);
					homeHolder.spinnerStatus.setEnabled(false);
					homeHolder.relativeLayoutCalender.setEnabled(false);
				}
	        	homeHolder.spinnerStatus.setSelection(3);
	        	homeHolder.spinnerStatus.setEnabled(false);
				homeHolder.relativeLayoutCalender.setEnabled(false);
	        }

		final Calendar c = Calendar.getInstance();
		year = c.get(Calendar.YEAR);
		month = c.get(Calendar.MONTH);
		day = c.get(Calendar.DAY_OF_MONTH);


		homeHolder.textViewQty.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.e("Clicked", "Clicked");

				if(isOrderProcessedIn){
					showAlertDialogToast("This Order Is Taken For Deliver Not Given Permission To Edit Items");
					return;
				}

				if (MyApplication.getInstance().isTemplate5()) {

					final Dialog qtyDialog = new Dialog(context);
					qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
					qtyDialog.setContentView(R.layout.refkey_dialogue);


					final Button buttonUpdateOk, buttonUpdateCancel;

					final EditText editTextRefKey, editTextFreeQty, editTextRefKeyprice, editTextfqtyuom, editTextTime, editTextDate;
					final TextView textQtyValidate;
					final AutoCompleteTextView textviewuom, textviewfreeuom, textviewdelivery;
					final LinearLayout linearlayoutdatetime, linearlayoutdeliverymode;

					InputMethodManager imm = (InputMethodManager) context
							.getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
					buttonUpdateOk = (Button) qtyDialog
							.findViewById(R.id.buttonUpdateOk);
					buttonUpdateCancel = (Button) qtyDialog
							.findViewById(R.id.buttonUpdateCancel);
					editTextRefKey = (EditText) qtyDialog
							.findViewById(R.id.editTextRefKey);
					editTextRefKeyprice = (EditText) qtyDialog
							.findViewById(R.id.editTextRefKeyprice);
					editTextFreeQty = (EditText) qtyDialog
							.findViewById(R.id.editTextFreeQty);
					textQtyValidate = (TextView) qtyDialog
							.findViewById(R.id.textQtyValidate);
					textviewuom = (AutoCompleteTextView) qtyDialog
							.findViewById(R.id.textviewuom);
					textviewfreeuom = (AutoCompleteTextView) qtyDialog
							.findViewById(R.id.textviewfreeuom);
					editTextTime = (EditText) qtyDialog
							.findViewById(R.id.editTextTime);
					editTextDate = (EditText) qtyDialog
							.findViewById(R.id.editTextDate);
					textviewdelivery = (AutoCompleteTextView) qtyDialog
							.findViewById(R.id.textviewdelivery);
					linearlayoutdatetime = (LinearLayout) qtyDialog
							.findViewById(R.id.linearlayoutdatetime);
					linearlayoutdeliverymode = (LinearLayout) qtyDialog
							.findViewById(R.id.linearlayoutdeliverymode);
					Log.e("USER_TYPE", Constants.USER_TYPE);
					if (Constants.USER_TYPE.equals("D")) {
						textviewuom.setVisibility(AutoCompleteTextView.GONE);
						textviewfreeuom.setVisibility(AutoCompleteTextView.GONE);
					} else if (Constants.USER_TYPE.equals("M")) {
						linearlayoutdeliverymode.setVisibility(View.GONE);
						linearlayoutdatetime.setVisibility(View.GONE);
						textviewuom.setVisibility(AutoCompleteTextView.GONE);
						textviewfreeuom.setVisibility(AutoCompleteTextView.GONE);
					}
					if (textviewdelivery.equals("")) {
						textviewdelivery.setText("Standard");
					}
					String prodiduom = item.getprodid();
					Cursor cur;
					cur = databaseHandler.getUOMforproduct(prodiduom);
					multipleUomCount = String.valueOf(cur.getCount());

					array = new String[cur.getCount()];
					cur.moveToFirst();
					for (int i = 0; i < cur.getCount(); i++) {
						//array[0] = item.getUOM();
						array[i] = cur.getString(cur.getColumnIndex("uomname"));
						cur.moveToNext();
					}


					textviewuom.setOnTouchListener(new View.OnTouchListener() {
						@Override
						public boolean onTouch(View v, MotionEvent event) {
							if (!multipleUomCount.equals("0")) {
								textviewuom.setText("");
								ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, array);
								textviewuom.setAdapter(adapter);
								textviewuom.showDropDown();
							}
							return false;
						}
					});

					textviewfreeuom.setOnTouchListener(new View.OnTouchListener() {
						@Override
						public boolean onTouch(View v, MotionEvent event) {
							if (!multipleUomCount.equals("0")) {
								textviewfreeuom.setText("");
								ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, array);
								textviewfreeuom.setAdapter(adapter);
								textviewfreeuom.showDropDown();
							}
							return false;
						}
					});
					editTextRefKeyprice.setOnTouchListener(new View.OnTouchListener() {
						@Override
						public boolean onTouch(View v, MotionEvent event) {
							editTextRefKeyprice.setCursorVisible(true);
							return false;
						}
					});

					//adapter set, date and time picker
					editTextTime.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							final int mHour, mMinute, mSeconds;
							final Calendar c = Calendar.getInstance();
							mHour = c.get(Calendar.HOUR_OF_DAY);
							mMinute = c.get(Calendar.MINUTE);
							mSeconds = c.get(Calendar.SECOND);
							// Launch Time Picker Dialog
							TimePickerDialog timePickerDialog = new TimePickerDialog(context,
									new TimePickerDialog.OnTimeSetListener() {

										@Override
										public void onTimeSet(TimePicker view, int hourOfDay,
															  int minute) {
											String minLength = String.valueOf(minute);
											if (minLength.length() == 1) {
												minLength = "0" + minLength;
											}
											editTextTime.setText(hourOfDay + ":" + minLength);
										}
									}, mHour, mMinute, false);
							timePickerDialog.show();
						}
					});

					editTextDate.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							// Get Current Date
							int mYear, mMonth, mDay;
							final Calendar c = Calendar.getInstance();
							mYear = c.get(Calendar.YEAR);
							mMonth = c.get(Calendar.MONTH);
							mDay = c.get(Calendar.DAY_OF_MONTH);

						DatePickerDialog datePickerDialog = new DatePickerDialog(context, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT,
								new DatePickerDialog.OnDateSetListener() {

										@Override
										public void onDateSet(DatePicker view, int year,
															  int monthOfYear, int dayOfMonth) {

											String monthLength = String.valueOf(monthOfYear);
											if (monthLength.length() == 1) {
												monthLength = "0" + monthLength;
											}
											String dayLength = String.valueOf(dayOfMonth);
											if (dayLength.length() == 1) {
												dayLength = "0" + dayLength;
											}
											editTextDate.setText(dayLength + "-" + (Integer.parseInt(monthLength) + 1) + "-" + year);

										}
									}, mYear, mMonth, mDay);
							datePickerDialog.show();
						}
					});

					textviewdelivery.setOnTouchListener(new View.OnTouchListener() {
						@Override
						public boolean onTouch(View v, MotionEvent event) {
							textviewdelivery.setText("");
							ArrayAdapter<String> deliverydapt = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, Constants.deliverymode);
							textviewdelivery.setAdapter(deliverydapt);
							textviewdelivery.showDropDown();
							InputMethodManager iss = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
							iss.hideSoftInputFromWindow(textviewdelivery.getWindowToken(), 0);
							return false;
						}
					});

					textviewdelivery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
							selecteddeliverymode = textviewdelivery.getText().toString();
							if (selecteddeliverymode.equals("Scheduled")) {
								linearlayoutdatetime.setVisibility(View.VISIBLE);
								final Calendar c = Calendar.getInstance();
								mYear = c.get(Calendar.YEAR);
								mMonth = c.get(Calendar.MONTH);
								mDay = c.get(Calendar.DAY_OF_MONTH);
								mHour = c.get(Calendar.HOUR_OF_DAY);
								mMinute = c.get(Calendar.MINUTE);
								mSeconds = c.get(Calendar.SECOND);

								monthLength = String.valueOf(mMonth + 1);
								if (monthLength.length() == 1) {
									monthLength = "0" + monthLength;
								}
								dayLength = String.valueOf(mDay);
								if (dayLength.length() == 1) {
									dayLength = "0" + dayLength;
								}
								String minuteLength = String.valueOf(mMinute);
								if (minuteLength.length() == 1) {
									minuteLength = "0" + minuteLength;
								}
								// set current date into textview
								editTextDate.setText(new StringBuilder()
										// Month is 0 based, just add 1
										.append(dayLength).append("-").append(monthLength).append("-").append(mYear)
								);

								editTextTime.setText(new StringBuilder()
										// Month is 0 based, just add 1
										.append(mHour).append(":").append(minuteLength)
								);
							} else {
								linearlayoutdatetime.setVisibility(View.GONE);
								editTextTime.setText("");
								editTextDate.setText("");
							}

						}
					});

					if (item.getFlag().equals("F")) {
						editTextFreeQty.setVisibility(EditText.VISIBLE);
						editTextRefKey.setVisibility(EditText.GONE);
						editTextRefKeyprice.setVisibility(EditText.GONE);
						textviewuom.setVisibility(EditText.GONE);

						if (Constants.USER_TYPE.equals("D")) {
							textviewfreeuom.setText(item.getSelcetedUOM());

						} else {
							textviewfreeuom.setHint(item.getSelcetedUOM());

						}

						if (item.getFreeQty().equals("0")) {

							editTextFreeQty.setText(item.getqty());

							oldQty = Float.parseFloat(item.getqty());
						} else {
							editTextFreeQty.setText(item.getFreeQty());
							oldQty = Float.parseFloat(item.getFreeQty());
						}

						//setting deliverymode details
						Log.e("Delivery", item.getDeliverymode());
						if (item.getDeliverymode().equals("Standard")) {
							textviewdelivery.setText(item.getDeliverymode());
							linearlayoutdatetime.setVisibility(View.GONE);
							editTextTime.setText("");
							editTextDate.setText("");
						} else if (item.getDeliverymode().equals("Scheduled")) {

							Log.e("deliverytime", item.getDeliverytime());
							Log.e("deliverydate", item.getDeliverydate());
							String deldate = "";
							if (!item.getDeliverydate().equals("")) {
								SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
								Date date = null;
								try {
									date = fmt.parse(item.getDeliverydate());
								} catch (java.text.ParseException e) {
									e.printStackTrace();
								}

								SimpleDateFormat fmtOut = new SimpleDateFormat("dd-MM-yyyy");
								deldate = fmtOut.format(date);
								Log.e("deliverydate", deldate);
							}
							textviewdelivery.setText(item.getDeliverymode());
							linearlayoutdatetime.setVisibility(View.VISIBLE);
							editTextTime.setText(item.getDeliverytime());
							editTextDate.setText(deldate);
						}

						editTextFreeQty.setSelection(editTextFreeQty.getText().length());
						editTextRefKey.setSelection(editTextRefKey.getText().length());
						editTextRefKeyprice.setSelection(editTextRefKeyprice.getText().length());
						qtyDialog.show();

					} else if (item.getFlag().equals("NF")) {
						editTextFreeQty.setVisibility(EditText.GONE);
						editTextRefKey.setVisibility(EditText.VISIBLE);
						//  editTextRefKeyprice.setVisibility(EditText.GONE);
						textviewfreeuom.setVisibility(EditText.GONE);
						editTextRefKey.setText(item.getqty());

						if (Constants.USER_TYPE.equals("D")) {
							textviewuom.setText(item.getSelcetedUOM());
						} else {
							textviewuom.setHint(item.getSelcetedUOM());
						}
						//setting deliverymode details
						if (!item.getDeliverymode().equals("")) {
							if (item.getDeliverymode().equals("Standard")) {
								textviewdelivery.setText(item.getDeliverymode());
								linearlayoutdatetime.setVisibility(View.GONE);
								editTextTime.setText("");
								editTextDate.setText("");
							} else if (item.getDeliverymode().equals("Scheduled")) {
								Log.e("deliverytimeNF", item.getDeliverytime());
								Log.e("deliverydateNF", item.getDeliverydate());
								String deldate = "";
								if (!item.getDeliverydate().equals("")) {
									SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
									Date date = null;
									try {
										date = fmt.parse(item.getDeliverydate());
									} catch (java.text.ParseException e) {
										e.printStackTrace();
									}

									SimpleDateFormat fmtOut = new SimpleDateFormat("dd-MM-yyyy");
									deldate = fmtOut.format(date);
									Log.e("deliverydate", deldate);
								}
								textviewdelivery.setText(item.getDeliverymode());
								linearlayoutdatetime.setVisibility(View.VISIBLE);
								editTextTime.setText(item.getDeliverytime());
								editTextDate.setText(deldate);
							}
						}
						editTextFreeQty.setSelection(editTextFreeQty.getText().length());
						editTextRefKey.setSelection(editTextRefKey.getText().length());
						editTextRefKeyprice.setSelection(editTextRefKeyprice.getText().length());
						qtyDialog.show();
					} else {
						editTextFreeQty.setSelection(editTextFreeQty.getText().length());
						editTextRefKey.setSelection(editTextRefKey.getText().length());
						editTextRefKeyprice.setSelection(editTextRefKeyprice.getText().length());
						qtyDialog.show();
					}

					if (item.getProdprice().equals("")) {

						editTextRefKeyprice.setText("");
					} else {
						editTextRefKeyprice.setText(item.getProdprice());

					}

					buttonUpdateOk.setOnClickListener(new OnClickListener() {

						@Override
						public void onClick(View v) {
							qty = editTextRefKey.getText().toString();
							freeQty = editTextFreeQty.getText().toString();
							prodPrice = editTextRefKeyprice.getText().toString();
							Log.e("Order prodprice---: ", prodPrice);
							deliverydate = editTextDate.getText().toString();
							deliverymode = textviewdelivery.getText().toString();
							deliverytime = editTextTime.getText().toString();
							String deldate = "";
							if (!deliverydate.equals("")) {
								SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
								Date date = null;
								try {
									date = fmt.parse(deliverydate);
								} catch (java.text.ParseException e) {
									e.printStackTrace();
								}

								SimpleDateFormat fmtOut = new SimpleDateFormat("yyyy-MM-dd");
								deldate = fmtOut.format(date);
								Log.e("deliverydate", deldate);
							}
							InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(buttonUpdateOk.getWindowToken(), 0);

						/*if (qty.isEmpty()&&freeQty.isEmpty() || qty.equals("0")&&freeQty.equals("0")) {
							textQtyValidate.setText("Please enter valid quantity");
						} else {*/
							if (selecteddeliverymode.equals("")) {
								textQtyValidate.setText("Please select Delivery Mode");
							} else if (qty.isEmpty() || qty.equals(".")) {
								item.setqty("0");
								qty = "0";
							} else {

								item.setqty(qty);
							}
							if (freeQty.isEmpty() || freeQty.equals(".")) {
								item.setFreeQty("0");
								freeQty = "0";
							} else {
								item.setFreeQty(freeQty);
							}

							if (prodPrice.isEmpty() || prodPrice.equals(".")) {
								item.setProdprice(prodPrice);
							} else {
								item.setProdprice(prodPrice);
							}
							if (item.getFlag().equals("F")) {

								if (!textviewfreeuom.getText().toString().equals("")) {
									if (textviewfreeuom.getText().toString().equals(item.getUOM())) {
										item.setSelcetedUOM(textviewfreeuom.getText().toString());
										Cursor curs;
										curs = databaseHandler.getProductUnits(item.getprodid());
										Log.e("Product Unit count", String.valueOf(curs.getCount()));
										curs.moveToFirst();
										if (curs != null && curs.getCount() > 0) {
											item.setSelecetdFreeQtyUomUnit(curs.getString(1));
										}
									} else {
										item.setSelcetedUOM(textviewfreeuom.getText().toString());
										homeHolder.textViewUOMName.setText("UOM : " + item.getSelcetedUOM());
										Log.e("USERTYPE", Constants.USER_TYPE);
										if (Constants.USER_TYPE.equals("D")) {
											Log.e("SelecetdQty", "SelecetdQty");
											Log.e("flag", item.getFlag());
											Log.e("UOMDESC", item.getSelcetedUOM());
											Log.e("size", String.valueOf(DealerOrderDetailsActivity.arraylistproductUOM.size()));

											for (ProductUomDomain fpd : DealerOrderDetailsActivity.arraylistproductUOM) {
												Log.e("flag", item.getFlag());
												Log.e("uomm", fpd.getUomdesc());
												if (fpd.getUomdesc().equals(item.getSelcetedUOM()) && fpd.getFlag().equals(item.getFlag())) {
													Log.e("SelecetdQty", fpd.getUomvalue());
													Log.e("Selecetdprodid", fpd.getProdid());
													Log.e("Selecetduom", fpd.getUomdesc());
													item.setSelecetdFreeQtyUomUnit(fpd.getUomvalue());
												}
											}

										} else {
											Cursor curs1;
											curs1 = databaseHandler.getUomUnit(item.getprodid(), item.getSelcetedUOM());
											curs1.moveToFirst();
											Log.e("count", String.valueOf(curs1.getCount()));
											if (curs1 != null && curs1.getCount() > 0) {
												item.setSelecetdFreeQtyUomUnit(curs1.getString(0));
												;
												Log.e("FreeQty Uom unit", curs1.getString(0));
											}
										}
									}
								}
								Log.e("SelfreeUOM", item.getSelectedFreeQtyUomUnit());
								Log.e("freeQty", freeQty);
								if (Float.parseFloat(freeQty) == 0) {
									//textQtyValidate.setText("Please enter valid quantity");
									item.setFreeQty(freeQty);
								} else {
									item.setFreeQty(freeQty);
									item.setqty("0");
									if (item.getFreeQty().equals("")) {
										homeHolder.textViewQty.setText("0" + "[" + item.getFlag() + "]"); //+" "+item.getUOM()
									} else {
										homeHolder.textViewQty.setText(item.getFreeQty() + "[" + item.getFlag() + "]"); //+" "+item.getUOM()
									}
									if (null == item.getProductPrice()) {
										item.setProductPrice("0");
									}
									if (null == item.getProductTax()) {
										item.setProductTax("0");
									}

									Log.e("freeQtyins", freeQty);

									Float totalunit = Float.parseFloat(freeQty) * (Float.parseFloat(item.getSelectedFreeQtyUomUnit()));
									Double updtpval = totalunit * (Double.parseDouble(item.getProductPrice()) + (Double.parseDouble(item.getProductPrice()) * (Double.parseDouble(item.getProductTax()) / 100)));
									item.setPvalue(String.valueOf(updtpval));

									//Double defaultuomTotal =( Double.parseDouble(freeQty) *(Double.parseDouble(item.getUnitGram()))*(Double.parseDouble(item.getSelectedFreeQtyUomUnit())))/1000;
									Double defaultuomTotal = (Double.parseDouble(freeQty) * (Double.parseDouble(item.getSelectedFreeQtyUomUnit())));
									Log.e("defaultuomTotalfreeQty", String.valueOf((Double.parseDouble(freeQty) * (Double.parseDouble(item.getSelectedFreeQtyUomUnit())))));
									item.setDefaultUomTotal(String.valueOf(defaultuomTotal));
									Log.e("freeQtyins", freeQty);
								}


								//set delivery mode values after clicking ok in pop up for free
								if (deliverymode.equals("Standard")) {
									item.setDeliverymode(deliverymode);
									item.setDeliverydate("");
									item.setDeliverytime("");
								} else if (deliverymode.equals("Scheduled")) {
									item.setDeliverymode(deliverymode);
									item.setDeliverydate(deldate);
									item.setDeliverytime(editTextTime.getText().toString());
								}

							} else if (item.getFlag().equals("NF")) {



					/*	Log.e("count","count");
							Log.e("defaultuom",item.getUOM());
							Log.e("textviewuom",textviewuom.getText().toString());
							if(!textviewuom.getText().toString().equals("")) {

								if (textviewuom.getText().toString().equals(item.getUOM())) {
									item.setSelcetedUOM(textviewuom.getText().toString());
									Cursor curs;
									curs = databaseHandler.getProductUnits(item.getprodid());
									Log.e("Product Unit count", String.valueOf(curs.getCount()));
									curs.moveToFirst();
									if (curs != null && curs.getCount() > 0) {
										item.setSelecetdQtyUomUnit(curs.getString(1));
									}
								} else {

									item.setSelcetedUOM(textviewuom.getText().toString());
									homeHolder.textViewUOMName.setText("UOM : " + item.getSelcetedUOM());
									Log.e("USERTYPE",Constants.USER_TYPE);
									if(Constants.USER_TYPE.equals("D")){
										Log.e("SelecetdQty","SelecetdQty");
										for(ProductUomDomain pd : DealerOrderDetailsActivity.arraylistproductUOM){
											Log.e("uomm",pd.getUomdesc());
											if(pd.getUomdesc().equals(item.getSelcetedUOM())&&pd.getFlag().equals(item.getFlag())){
												Log.e("SelecetdQty",pd.getUomvalue());
												Log.e("Selecetdprodid",pd.getProdid());
												Log.e("Selecetduom",pd.getUomdesc());
												item.setSelecetdQtyUomUnit(pd.getUomvalue());
											}
										}

									}else {
										Cursor curs;
										curs = databaseHandler.getUomUnit(item.getprodid(), item.getSelcetedUOM());
										Log.e("count", String.valueOf(curs.getCount()));
										Log.e("prodid", item.getprodid());
										Log.e("item.getSelcetedUOM()", item.getSelcetedUOM());
										curs.moveToFirst();

										if (curs != null && curs.getCount() > 0) {
											item.setSelecetdQtyUomUnit(curs.getString(0));
											;
											Log.e("Qty Uom unit", curs.getString(0));
										}
									}
								}
							}

							if (Double.parseDouble(qty) == 0 ) {
								textQtyValidate.setText("Please enter valid quantity");
							} else {

								item.setqty(qty);
								item.setFreeQty("0");
								if (item.getqty().equals("")) {
									homeHolder.textViewQty.setText("0" + "[" + item.getFlag() + "]"); //+" "+item.getUOM()
								} else {
									homeHolder.textViewQty.setText(item.getqty() + "[" + item.getFlag() + "]"); //+" "+item.getUOM()
								}

								Log.e("qty",qty);
								Log.e("fqty",item.getFreeQty());
								Log.e("uomunit",item.getSelectedQtyUomUnit());

								Float totalunit=   Float.parseFloat(qty) *(Float.parseFloat(item.getSelectedQtyUomUnit()));
								Double  updtpval  = totalunit*(Double.parseDouble(item.getProductPrice()) + (Double.parseDouble(item.getProductPrice())*(Double.parseDouble(item.getProductTax())/100)));

								item.setPvalue(String.valueOf(updtpval));

								Double defaultuomTotal =( Double.parseDouble(qty) *(Double.parseDouble(item.getUnitGram()))*(Double.parseDouble(item.getSelectedQtyUomUnit())))/1000;

								item.setDefaultUomTotal(String.valueOf(defaultuomTotal));
							}

								//set delivery mode values after clicking ok in pop up for non free
							if(deliverymode.equals("Standard")){
								item.setDeliverymode(deliverymode);
								item.setDeliverydate("");
								item.setDeliverytime("");
							}else if(deliverymode.equals("Scheduled")){
								item.setDeliverymode(deliverymode);
								item.setDeliverydate(deldate);
								item.setDeliverytime(editTextTime.getText().toString());
							}
*/
								if (!textviewuom.getText().toString().equals("")) {
									if (textviewuom.getText().toString().equals(item.getUOM())) {
										item.setSelcetedUOM(textviewuom.getText().toString());
										Cursor curs;
										curs = databaseHandler.getProductUnits(item.getprodid());
										Log.e("Product Unit count", String.valueOf(curs.getCount()));
										curs.moveToFirst();
										if (curs != null && curs.getCount() > 0) {
											item.setSelecetdQtyUomUnit(curs.getString(1));
										}
									} else {
										item.setSelcetedUOM(textviewuom.getText().toString());
										homeHolder.textViewUOMName.setText("UOM : " + item.getSelcetedUOM());
										Log.e("USERTYPE", Constants.USER_TYPE);
										if (Constants.USER_TYPE.equals("D")) {
											Log.e("SelecetdQty", "SelecetdQty");
											Log.e("flag", item.getFlag());
											Log.e("UOMDESC", item.getSelcetedUOM());
											Log.e("size", String.valueOf(DealerOrderDetailsActivity.arraylistproductUOM.size()));

											for (ProductUomDomain fpd : DealerOrderDetailsActivity.arraylistproductUOM) {
												Log.e("flag", item.getFlag());
												Log.e("uomm", fpd.getUomdesc());
												if (fpd.getUomdesc().equals(item.getSelcetedUOM()) && fpd.getFlag().equals(item.getFlag())) {
													Log.e("SelecetdQty", fpd.getUomvalue());
													Log.e("Selecetdprodid", fpd.getProdid());
													Log.e("Selecetduom", fpd.getUomdesc());
													item.setSelecetdQtyUomUnit(fpd.getUomvalue());
												}
											}

										} else {
											Cursor curs1;
											curs1 = databaseHandler.getUomUnit(item.getprodid(), item.getSelcetedUOM());
											curs1.moveToFirst();
											Log.e("count", String.valueOf(curs1.getCount()));
											if (curs1 != null && curs1.getCount() > 0) {
												item.setSelecetdQtyUomUnit(curs1.getString(0));
												Log.e("FreeQty Uom unit", curs1.getString(0));
											}
										}
									}
								}
								Log.e("SelfreeUOM", item.getSelectedQtyUomUnit());
								Log.e("freeQty", qty);
								if (null == qty || qty.length() < 1) {
									qty = "0";
								}
								if (Float.parseFloat(qty) == 0) {
									//textQtyValidate.setText("Please enter valid quantity");
									item.setqty(qty);
								} else {
									item.setFreeQty("0");
									item.setqty(qty);
									if (item.getqty().equals("")) {
										homeHolder.textViewQty.setText("0" + "[" + item.getFlag() + "]"); //+" "+item.getUOM()
									} else {
										homeHolder.textViewQty.setText(item.getqty() + "[" + item.getFlag() + "]"); //+" "+item.getUOM()
									}
									if (null == item.getProductPrice()) {
										item.setProductPrice("0");
									}
									if (null == item.getProductTax()) {
										item.setProductTax("0");
									}


									Log.e("prodPrice--2", prodPrice);
									Log.e("Qtyins", qty);
									Log.e("Qtyins", item.getProductTax());

									Float totalunit = Float.parseFloat(qty) * (Float.parseFloat(item.getSelectedQtyUomUnit()));
									Log.e("totalunit", String.valueOf(totalunit));
									Double updtpval = totalunit * (Double.parseDouble(item.getProductPrice()) + (Double.parseDouble(item.getProductPrice()) * (Double.parseDouble(item.getProductTax()) / 100)));

									item.setPvalue(String.valueOf(updtpval));


									//Double defaultuomTotal =( Double.parseDouble(qty) *(Double.parseDouble(item.getUnitGram()))*(Double.parseDouble(item.getSelectedQtyUomUnit())))/1000;
									Log.e("Qtyins", qty);
									Log.e("SelectedFreeQtyUomUnit", item.getSelectedQtyUomUnit());
									Double defaultuomTotal = (Double.parseDouble(qty) * (Double.parseDouble(item.getSelectedQtyUomUnit())));
									item.setDefaultUomTotal(String.valueOf(defaultuomTotal));
								}
								//set delivery mode values after clicking ok in pop up for free
								if (deliverymode.equals("Standard")) {
									item.setDeliverymode(deliverymode);
									item.setDeliverydate("");
									item.setDeliverytime("");
								} else if (deliverymode.equals("Scheduled")) {
									item.setDeliverymode(deliverymode);
									item.setDeliverydate(deldate);
									item.setDeliverytime(editTextTime.getText().toString());
								}
							}
							qtyDialog.dismiss();
						}

					});

					buttonUpdateCancel
							.setOnClickListener(new OnClickListener() {

								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub
									InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
									imm.hideSoftInputFromWindow(buttonUpdateCancel.getWindowToken(), 0);
									textviewdelivery.setText("Standard");
									qtyDialog.dismiss();

								}
							});
				}

				if(MyApplication.getInstance().isTemplate1()){
					Template1QuantityUpdatePopup popup = new
							Template1QuantityUpdatePopup(context,
							item.getCurrStock(), item.getqty(),homeHolder.llRoot, position);
					popup.showPopup();
				}

				if(MyApplication.getInstance().isTemplate2()){
					Template2QuantityUpdatePopup popup = new
							Template2QuantityUpdatePopup(context,
							item.getCurrStock(), item.getqty(),homeHolder.llRoot, position);
					popup.showPopup();
				}

				if(MyApplication.getInstance().isTemplate3()){
					Template3QuantityUpdatePopup popup = new
							Template3QuantityUpdatePopup(context,
							item.getCurrStock(), item.getqty(),homeHolder.llRoot, position);
					popup.showPopup();
				}

				if(MyApplication.getInstance().isTemplate4()){
					Template4QuantityUpdatePopup popup = new
							Template4QuantityUpdatePopup(context,
							item.getCurrStock(), item.getqty(),homeHolder.llRoot, position);
					popup.showPopup();
				}

			}
		});


		homeHolder.relativeLayoutCalender
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						Log.e("Clicked", "Clicked");


						DatePickerDialog mdiDialog = new DatePickerDialog(
								getContext(), AlertDialog.THEME_DEVICE_DEFAULT_LIGHT,
								new DatePickerDialog.OnDateSetListener() {
									@Override
									public void onDateSet(DatePicker view,
											int year, int month,
											int dayOfMonth) {
										// Toast.makeText(getContext(),year+
										// " "+monthOfYear+" "+dayOfMonth,Toast.LENGTH_LONG).show();
										Log.e("date", year + "-" + (month + 1)
												+ "-" + dayOfMonth);
										item.setDate(year + "-" + (month+1)
												+ "-" + dayOfMonth);

										time = item.getDate();

										String inputPattern = "yyyy-MM-dd";
										String outputPattern = "dd-MMM-yyyy ";
										SimpleDateFormat inputFormat = new SimpleDateFormat(
												inputPattern);
										SimpleDateFormat outputFormat = new SimpleDateFormat(
												outputPattern);

										String str = null;
										try {
											dateStr = inputFormat.parse(time);
											str = outputFormat.format(dateStr);
											item.setDate(str);
											homeHolder.textViewDate.setText(str);
											// Log.e("str", str);
										} catch (Exception  e) {
											e.printStackTrace();
										}

									}
								}, year, month + 1, day);
						mdiDialog.show();

					}
				});

		homeHolder.spinnerStatus
				.setOnItemSelectedListener(new OnItemSelectedListener() {
					@Override
					public void onItemSelected(AdapterView<?> parentView,
							View selectedItemView, int position, long id) {

						Log.e("selected", homeHolder.spinnerStatus.getSelectedItem().toString());
						item.setordstatus(homeHolder.spinnerStatus.getSelectedItem().toString());
						Log.e("status", item.getordstatus());
					}

					@Override
					public void onNothingSelected(AdapterView<?> parentView) {
					}

				});

		return row;
	}

	private boolean validation(String qty,String freeQty) {
		Pattern pattern = Pattern.compile("(^[1-9][0-9]*)+((\\.\\d)?)");

		Matcher matcherq = pattern.matcher(qty);
		Matcher matcherf = pattern.matcher(freeQty);
		if (matcherq.matches()){
			return matcherq.matches();
		} else if (matcherf.matches()){
			return matcherf.matches();
		}else{
			return false;
		}
	}


	class HomeHolder {
		RelativeLayout relativeLayoutCalender, relativeLayoutStatus;
		Spinner spinnerStatus;

		TextView textViewProductName, textViewQty, textViewDate,
				textViewStatus, textViewCalendar, textViewSpinner;
		TextView textViewName, textViewSubname, textViewPlus,textViewUOMName,TV_stk;
		EditText editTextQuantity;
		LinearLayout llRoot, ll_LV_stk;
	}

	//Kumaravel
	public void showAlertDialogToast(String message) {

		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setCancelable(true);
		builder.setMessage(message);
		builder.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});


		AlertDialog alert = builder.create();
		alert.show();

		Button buttonbackground = alert.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground.setBackgroundColor(Color.parseColor("#A9E2F3"));

	}

}
