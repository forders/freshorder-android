package com.freshorders.freshorder.adapter;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.domain.MerchantOrderListDomain;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MerchantOrderListAdapter extends ArrayAdapter<MerchantOrderListDomain> {
	Context context;
	int resource;
	ArrayList<MerchantOrderListDomain> arrayListServiceDomainObjects;

	JSONObject JsonAccountObject = null;
	JSONArray JsonAccountArray = null;
	JsonServiceHandler JsonServiceHandler;


	public MerchantOrderListAdapter(Context context, int resource,
			ArrayList<MerchantOrderListDomain> arrayListServiceDomainObjects) {
		super(context, resource, arrayListServiceDomainObjects);
		this.context = context;
		this.resource = resource;
		this.arrayListServiceDomainObjects = arrayListServiceDomainObjects;
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();
	}

	@Override
	public int getCount() {
		return arrayListServiceDomainObjects.size();
	}

	@Override
	public boolean isEnabled(int position) {
		return true;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// Log.e("Adapter position",String.valueOf(position));
		View row = convertView;
		final HomeHolder homeHolder;
		if (row == null) {
			// Log.e("Adapter Inner position",String.valueOf(position));
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(resource, parent, false);
			homeHolder = new HomeHolder();

			homeHolder.textViewName = (TextView) row
					.findViewById(R.id.textViewName);

			homeHolder.textViewDate = (TextView) row
					.findViewById(R.id.textViewDate);
			homeHolder.textViewId = (TextView) row
					.findViewById(R.id.textViewId);
			homeHolder.serialno = (TextView) row
					.findViewById(R.id.serialno);
			homeHolder.textviewdate1 = (TextView) row
					.findViewById(R.id.textviewdate1);
			homeHolder.textvieworderid = (TextView) row
					.findViewById(R.id.textvieworderid);
			row.setTag(homeHolder);
		} else {
			homeHolder = (HomeHolder) row.getTag();
		}
		MerchantOrderListDomain item = arrayListServiceDomainObjects
				.get(position);

		String [] read=item.getisread().split(",");
		int readint=0;
		for(int i=0;i<read.length;i++){
			if(read[i].equals(Constants.USER_ID)){
				readint = 1;

			}
		}
		if(readint==1){
			homeHolder.serialno.setTypeface(null, Typeface.BOLD);
			homeHolder.textViewName.setTypeface(null, Typeface.BOLD);
			homeHolder.textViewDate.setTypeface(null, Typeface.BOLD);
			homeHolder.textViewId.setTypeface(null, Typeface.BOLD);
			homeHolder.textviewdate1.setTypeface(null, Typeface.BOLD);
			homeHolder.textvieworderid.setTypeface(null, Typeface.BOLD);
		}else{
			homeHolder.serialno.setTypeface(null, Typeface.NORMAL);
			homeHolder.textViewName.setTypeface(null, Typeface.NORMAL);
			homeHolder.textViewDate.setTypeface(null, Typeface.NORMAL);
			homeHolder.textViewId.setTypeface(null, Typeface.NORMAL);
			homeHolder.textviewdate1.setTypeface(null, Typeface.NORMAL);
			homeHolder.textvieworderid.setTypeface(null, Typeface.NORMAL);
		}
		homeHolder.serialno.setText(item.getserialno() +".");
		homeHolder.textViewName.setText(item.getComName());
		homeHolder.textViewDate.setText(item.getDate());
		homeHolder.textViewId.setText(item.getOrderid() + ",");



		return row;

	}

	class HomeHolder {

		TextView textViewName,textViewDate,textViewId,serialno,textvieworderid,textviewdate1;
	}

}