package com.freshorders.freshorder.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.activity.MyTaskActivity;
import com.freshorders.freshorder.activity.TeamStatusActivity;
import com.freshorders.freshorder.model.TeamStatusModel;

import java.util.List;

public class TeamStatusAdapter extends RecyclerView.Adapter<TeamStatusAdapter.ItemViewHolder>{

    private static final String TAG = TeamStatusAdapter.class.getSimpleName();

    private Context mContext;
    private List<TeamStatusModel> listItems;
    private View rootView;
    private TeamStatusActivity.MyBSheet listener;


    public TeamStatusAdapter(Context mContext,
                     List<TeamStatusModel> listItems,
                     View rootView, TeamStatusActivity.MyBSheet listener) {
        this.mContext = mContext;
        this.listItems = listItems;
        this.rootView = rootView;
        this.listener = listener;
    }

    @NonNull
    @Override
    public TeamStatusAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View myInflatedView = inflater.inflate(R.layout.item_team_status, parent, false);
        return new TeamStatusAdapter.ItemViewHolder(myInflatedView);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {

        final TeamStatusModel currentItem = listItems.get(position);
        String strWorkName = currentItem.getWorkerName();
        String strWorkCompleted = currentItem.getPercentageOfWorkCompleted();
        if(isNumeric(strWorkCompleted)){
            int workComplete = Integer.parseInt(strWorkCompleted);
            setBackgroundColor1(workComplete, holder.progressBar);
            setProgressUpdate(workComplete, holder.progressBar);
            //setBackgroundColor(workComplete, holder.tvWorkStatus, holder.progressBar);
        }
        holder.tvName.setText(strWorkName);
        holder.tvWorkStatus.setText((strWorkCompleted + " %"));

        holder.llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, MyTaskActivity.class);
                intent.putExtra("MyTask", currentItem);
                mContext.startActivity(intent);
            }
        });
    }

    private void setProgressUpdate(final int value, final ProgressBar progressBar){

        new Handler().post(new Runnable() {
            public void run() {
                //Drawable progressDrawable = progressBar.getProgressDrawable().mutate();
                //////setBackgroundColor(value, progressBar);
                progressBar.setProgress(value);
                progressBar.invalidate();
                Log.e(TAG,"........................progress updated...........");
            }
        });
    }

    private void setBackgroundColor1(int work, final ProgressBar progressBar){

        LayerDrawable layerDrawable = (LayerDrawable) progressBar.getProgressDrawable();
        Drawable progressDrawable = layerDrawable.findDrawableByLayerId(android.R.id.progress);

        if( work <= 25){
            //bg.setBackgroundColor(ContextCompat.getColor(mContext, R.color.red_bellow_25));
            //bg.setTextColor(ContextCompat.getColor(mContext, R.color.red_bellow_25));
            progressDrawable.setColorFilter(ContextCompat.getColor(mContext, R.color.red_bellow_25), android.graphics.PorterDuff.Mode.SRC_IN);
            //progressBar.setProgress(work);
        }else if( work <= 50){
            //bg.setBackgroundColor(ContextCompat.getColor(mContext, R.color.yellow_bellow_50));
            //bg.setTextColor(ContextCompat.getColor(mContext, R.color.yellow_bellow_50));
            progressDrawable.setColorFilter(ContextCompat.getColor(mContext, R.color.yellow_bellow_50), android.graphics.PorterDuff.Mode.SRC_IN);
            //progressBar.setProgress(work);
        } else if(work <= 99){
            //bg.setBackgroundColor(ContextCompat.getColor(mContext, R.color.blue_above_50));
            //bg.setTextColor(ContextCompat.getColor(mContext, R.color.blue_above_50));
            progressDrawable.setColorFilter(ContextCompat.getColor(mContext, R.color.blue_above_50), android.graphics.PorterDuff.Mode.SRC_IN);
            //progressBar.setProgress(work);
        } else if(work >= 100){
            //bg.setBackgroundColor(ContextCompat.getColor(mContext, R.color.green_complete_100));
            //bg.setTextColor(ContextCompat.getColor(mContext, R.color.green_complete_100));
            progressDrawable.setColorFilter(ContextCompat.getColor(mContext, R.color.green_complete_100), android.graphics.PorterDuff.Mode.SRC_IN);
            //progressBar.setProgress(work);
        }

        progressBar.setProgressDrawable(progressDrawable);
    }

    private void setBackgroundColor(int work, final ProgressBar progressBar){

        Drawable progressDrawable = progressBar.getProgressDrawable().mutate();

        if( work <= 25){
            //bg.setBackgroundColor(ContextCompat.getColor(mContext, R.color.red_bellow_25));
            //bg.setTextColor(ContextCompat.getColor(mContext, R.color.red_bellow_25));
            progressDrawable.setColorFilter(ContextCompat.getColor(mContext, R.color.red_bellow_25), android.graphics.PorterDuff.Mode.SRC_IN);
            //progressBar.setProgress(work);
        }else if( work <= 50){
            //bg.setBackgroundColor(ContextCompat.getColor(mContext, R.color.yellow_bellow_50));
            //bg.setTextColor(ContextCompat.getColor(mContext, R.color.yellow_bellow_50));
            progressDrawable.setColorFilter(ContextCompat.getColor(mContext, R.color.yellow_bellow_50), android.graphics.PorterDuff.Mode.SRC_IN);
            //progressBar.setProgress(work);
        } else if(work <= 99){
            //bg.setBackgroundColor(ContextCompat.getColor(mContext, R.color.blue_above_50));
            //bg.setTextColor(ContextCompat.getColor(mContext, R.color.blue_above_50));
            progressDrawable.setColorFilter(ContextCompat.getColor(mContext, R.color.blue_above_50), android.graphics.PorterDuff.Mode.SRC_IN);
            //progressBar.setProgress(work);
        } else if(work >= 100){
            //bg.setBackgroundColor(ContextCompat.getColor(mContext, R.color.green_complete_100));
            //bg.setTextColor(ContextCompat.getColor(mContext, R.color.green_complete_100));
            progressDrawable.setColorFilter(ContextCompat.getColor(mContext, R.color.green_complete_100), android.graphics.PorterDuff.Mode.SRC_IN);
            //progressBar.setProgress(work);
        }

        progressBar.setProgressDrawable(progressDrawable);
    }

    private static boolean isNumeric(String strNum) {
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }



    @Override
    public int getItemCount() {
        return listItems.size();
    }


    class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView tvName, tvWorkStatus;
        LinearLayout llItem;
        ProgressBar progressBar;


        ItemViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.id_team_tv_worker_name);
            tvWorkStatus = itemView.findViewById(R.id.id_team_work_status);
            llItem = itemView.findViewById(R.id.id_team_ll_root);
            progressBar = itemView.findViewById(R.id.id_team_work_status_progress);
        }
    }

    private void showBottomSheet(String msg){
        if(listener != null){
            listener.show(msg);
        }
    }

    private void show(){
        if(listener != null){
            listener.show(mContext.getResources().getString(R.string.wait_data_fetching));
        }
    }

    private void hide(){
        listener.hide();
    }
}
