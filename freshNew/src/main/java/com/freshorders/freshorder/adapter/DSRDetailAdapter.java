package com.freshorders.freshorder.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.model.DSRDetailModel;
import com.freshorders.freshorder.model.DSRModel;
import com.freshorders.freshorder.utils.CurrencyFormat;

import java.util.List;

public class DSRDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = DSRAdapter.class.getSimpleName();

    private static final int FOOTER_VIEW = 1;

    private Context mContext;
    private List<DSRDetailModel> listItems;
    private View viewFragment;
    private String totalAmount;

    public DSRDetailAdapter(Context mContext,
                                List<DSRDetailModel> listItems,
                                View viewFragment,
                                String totalAmount) {
        this.mContext = mContext;
        this.listItems = listItems;
        this.viewFragment = viewFragment;
        this.totalAmount = totalAmount;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View myInflatedView;
        if (viewType == FOOTER_VIEW) {
            myInflatedView = inflater.inflate(R.layout.item_dsr_detail_footer, parent, false);
            return new DSRDetailAdapter.FooterViewHolder(myInflatedView);
        }
        myInflatedView = inflater.inflate(R.layout.item_dsr_detail, parent, false);  ////////////parent --- Null(can be)
        //RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //myInflatedView.setLayoutParams(lp);
        return new DSRDetailAdapter.ItemViewHolder(myInflatedView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        try {
            if (viewHolder instanceof FooterViewHolder) {
                FooterViewHolder holder = (FooterViewHolder) viewHolder;
                if(listItems.size() > 0) {
                    holder.tvTotalSales.setText(CurrencyFormat.getFormattedAmounts_Rs(totalAmount));
                }

            }else if (viewHolder instanceof ItemViewHolder) {
                ItemViewHolder holder = (ItemViewHolder) viewHolder;

                DSRDetailModel currentItems = listItems.get(position);
                String sNo = String.valueOf((position + 1));
                String brName = currentItems.getProdname();
                String qty = currentItems.getQtotal();
                /////////////String sales = CurrencyFormat.getFormattedAmounts(currentItems.getValue());
                String sales =currentItems.getValue();

                holder.tvSno.setText(sNo);
                holder.tvProductName.setText(brName);
                holder.tvQty.setText(qty);
                holder.tvSales.setText(sales);
            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    public class FooterViewHolder extends RecyclerView.ViewHolder {

        TextView tvTotalSales;

        public FooterViewHolder(View itemView) {
            super(itemView);
            tvTotalSales = itemView.findViewById(R.id.id_dsr_dt_total_amount);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Do whatever you want on clicking the item
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return listItems.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == listItems.size()) {
            // This is where we'll add footer.
            return FOOTER_VIEW;
        }
        return super.getItemViewType(position);
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView tvSno, tvProductName, tvSales, tvQty;


        ItemViewHolder(View itemView) {
            super(itemView);
            tvSno = itemView.findViewById(R.id.id_der_dt_s_no);
            tvProductName = itemView.findViewById(R.id.id_der_dt_product_name);
            tvQty = itemView.findViewById(R.id.id_der_dt_qty);
            tvSales = itemView.findViewById(R.id.id_der_dt_sales);

        }
    }
}
