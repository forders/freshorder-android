package com.freshorders.freshorder.adapter;

import android.app.Activity;
import android.content.Context;
import androidx.annotation.NonNull;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.MerchantOrderDetailDomain;
import com.freshorders.freshorder.domain.MerchantOrderDomainSelected;
import com.freshorders.freshorder.model.Template6Model;
import com.freshorders.freshorder.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.freshorders.freshorder.utils.Constants.DOT;

public class Template6MilkOrderDetailAdapter extends ArrayAdapter<MerchantOrderDetailDomain> {
    Context context;
    int resource;

    ArrayList<MerchantOrderDetailDomain> arraylistMerchantOrderDetailList;
    ArrayList<MerchantOrderDetailDomain>arrayListSearchResults;
    public static ArrayList<MerchantOrderDomainSelected> arraylistMerchantOrderDetailListSelected, arraylistsavearray;
    JSONObject JsonAccountObject = null;
    JSONArray JsonAccountArray = null;
    com.freshorders.freshorder.utils.JsonServiceHandler JsonServiceHandler;
    String qty = "", freeQty = "", orderuom = "",price = "", defaultuom = "", freeqtyuom = "",deliverydate="",deliverytime="",deliverymode="",currstock="";
    public static String prodid, multipleUomCount = "",selecteddeliverymode="",monthLength="",dayLength="";
    //int index=1;
    public static EditText editTextSearchField;
    public static TextView textViewHeader, textQtyValidat;
    public static MerchantOrderDomainSelected dod;
    DatabaseHandler databaseHandler;
    public static int mYear, mMonth, mDay, mHour, mMinute,mSeconds;
    private boolean netcheck;

    public Map<Integer, Template6Model> merchantOrderDetailList = new LinkedHashMap<>();
    public Map<String, String> pairCheck = new HashMap<>();

    //public static String searchclick="0";
    public Template6MilkOrderDetailAdapter(
            Context context,
            int resource,
            ArrayList<MerchantOrderDetailDomain> arraylistMerchantOrderDetailList, Boolean netcheck) {
        super(context, resource, arraylistMerchantOrderDetailList);
        Log.e("MerchantOrdDtlAdapter",netcheck.toString());
        this.context = context;
        this.resource = resource;
        this.arraylistMerchantOrderDetailList = arraylistMerchantOrderDetailList;
        this.netcheck = netcheck;
        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();
        databaseHandler = new DatabaseHandler(context);


    }

    @Override
    public int getCount() {
        return arraylistMerchantOrderDetailList.size();
    }




    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Log.e("Adapter position",String.valueOf(position));
        View row = convertView;
        final Template6MilkOrderDetailAdapter.HomeHolder2 homeHolder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(resource, parent, false);
            homeHolder = new Template6MilkOrderDetailAdapter.HomeHolder2();
            homeHolder.textViewName = (TextView) row
                    .findViewById(R.id.textViewName);
            homeHolder.serialno = (TextView) row
                    .findViewById(R.id.serialno);
            homeHolder.tvUOM = (TextView) row.findViewById(R.id.id_milk_uom_type);
            homeHolder.eTxtValue = (EditText) row.findViewById(R.id.id_milk_value);

            homeHolder.eTxtValue.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }
                @Override
                public void afterTextChanged(Editable editable) {
                    String strValue = editable.toString().trim();
                    int position = (int) homeHolder.eTxtValue.getTag();
                    MerchantOrderDetailDomain item = arraylistMerchantOrderDetailList.get(position);
                    Template6Model temp = new Template6Model();
                    temp.setProductCode(item.getprodcode());
                    temp.setQuantity(strValue);
                    merchantOrderDetailList.put(position, temp);
                    pairCheck.put(item.getprodcode(),strValue);
                }
            });

            row.setTag(homeHolder);
        } else {
            homeHolder = (Template6MilkOrderDetailAdapter.HomeHolder2) row.getTag();
        }

        final MerchantOrderDetailDomain item = arraylistMerchantOrderDetailList
                .get(position);

        homeHolder.textViewName.setText(Html.fromHtml(item.getprodname() + "  ["
                + item.getprodcode() + "]"));
        homeHolder.serialno.setText(item.getserialno() +".");
        homeHolder.tvUOM.setText(item.getSlectedUOM());

        homeHolder.eTxtValue.setTag(position);
        //homeHolder.editTextOrderQuantity.setText(quantityList.get(position));
        return row;
    }

    public Map<Integer, Template6Model> getSelectedMerchantOrders(){
        return merchantOrderDetailList;
    }
    public Map<String, String> getPairCheck(){
        return pairCheck;
    }

    public boolean requirementCheck() {

        boolean milk, fat, snf, temperature, lrvalue;
        milk = fat = snf = temperature = lrvalue = false;
        for (Map.Entry entry : pairCheck.entrySet()) {
            String code = String.valueOf(entry.getKey());
            String value = String.valueOf(entry.getValue());
            switch (code) {
                case Constants.MILK:
                    milk = (value.length() > 0 && (!(value.length() == 1 && value.startsWith(DOT))));
                    break;
                case Constants.FAT:
                    fat = (value.length() > 0 && (!(value.length() == 1 && value.startsWith(DOT))));
                    break;
                case Constants.SNF:
                    snf = (value.length() > 0 && (!(value.length() == 1 && value.startsWith(DOT))));
                    break;
                case Constants.TEMPERATURE:
                    temperature = (value.length() > 0 && (!(value.length() == 1 && value.startsWith(DOT))));
                    break;
                case Constants.LRVALUE:
                    lrvalue = (value.length() > 0 && (!(value.length() == 1 && value.startsWith(DOT))));
                    break;
            }
        }
        return milk && (fat && snf || fat && temperature && lrvalue);
    }

    class HomeHolder2 {
        //RelativeLayout relativeLayoutAddQuantity;
        TextView textViewName, serialno, tvUOM ;

        //ImageView imageView;
        EditText eTxtValue;


    }

}

