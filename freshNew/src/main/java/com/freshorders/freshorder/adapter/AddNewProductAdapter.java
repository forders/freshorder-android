package com.freshorders.freshorder.adapter;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONObject;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.domain.DealerAddProductDomain;
import com.freshorders.freshorder.domain.DealerComplaintDomain;
import com.freshorders.freshorder.domain.DealerProductDomain;
import com.freshorders.freshorder.domain.MerchantOrderDetailDomain;
import com.freshorders.freshorder.domain.MerchantOrderDomainSelected;
import com.freshorders.freshorder.ui.CreateOrderActivity;
import com.freshorders.freshorder.ui.DealerAddNewProduct;
import com.freshorders.freshorder.ui.DealerAddProductActivity;
import com.freshorders.freshorder.ui.DealerOrderDetailsActivity;
import com.freshorders.freshorder.ui.MerchantOrderDetailActivity;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;

import android.R.integer;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import static com.freshorders.freshorder.R.layout.refkey_dialogue;

public class AddNewProductAdapter extends
        ArrayAdapter<DealerAddProductDomain> {
    Context context;
    int resource;
    ArrayList<DealerAddProductDomain> arraylistDealerProductList,arraylistMProductListSelected;

    JSONObject JsonAccountObject = null;
    JSONArray JsonAccountArray = null;
    JsonServiceHandler JsonServiceHandler;
    String qty="",freeQty="";
    public static String prodid,selecteddeliverymode="",monthLength="",dayLength="",deliverydate="",deliverytime="",deliverymode="";
    //int index=1;
    public static EditText editTextSearchField;
    public static TextView textViewHeader,textQtyValidate;
    public static DealerAddProductDomain dod;
    public static int mYear, mMonth, mDay, mHour, mMinute,mSeconds;
    //public static String searchclick="0";
    public AddNewProductAdapter(
            Context context,
            int resource,
            ArrayList<DealerAddProductDomain> arraylistDealerProductList) {
        super(context, resource, arraylistDealerProductList);
        this.context = context;
        this.resource = resource;
        this.arraylistDealerProductList = arraylistDealerProductList;
        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();
    }

    @Override
    public int getCount() {
        return arraylistDealerProductList.size();
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Log.e("Adapter position",String.valueOf(position));
        View row = convertView;
        final HomeHolder2 homeHolder;

        if (row == null) {
            // Log.e("Adapter Inner position",String.valueOf(position));
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(resource, parent, false);
            homeHolder = new HomeHolder2();
//
            homeHolder.textViewName = (TextView) row
                    .findViewById(R.id.textViewName);
            homeHolder.textViewserialno = (TextView) row
                    .findViewById(R.id.textViewserialno);
            homeHolder.textViewSubname = (TextView) row
                    .findViewById(R.id.textViewSubname);


            homeHolder.editTextQuantity = (EditText) row
                    .findViewById(R.id.editTextClosingStock);



            row.setTag(homeHolder);
        } else {
            homeHolder = (HomeHolder2) row.getTag();
        }
        final DealerAddProductDomain item = arraylistDealerProductList
                .get(position);
       // arraylistsavearray= new ArrayList<MerchantOrderDomainSelected>();
        homeHolder.textViewName.setText(item.getProductName());
        homeHolder.textViewSubname.setText(item.getProductCode() );
        homeHolder.textViewserialno.setText(item.getserialno()+ "." );


        //homeHolder.textViewPlus.setText(item.getindex());

        //homeHolder.editTextQuantity.setText("NF: ["+item.getqty() +"]"+"\n"+"F: ["+ item.getFreeQty() +"]");
        if(!item.getDqty().equals("")){
            homeHolder.editTextQuantity.setText("NF: ["+item.getDqty() +"]"+"\n"+"F: ["+ item.getDfqty() +"]");
        }else{
            homeHolder.editTextQuantity.setText( item.getDqty() +"\n"+ item.getDfqty());
        }



        homeHolder.editTextQuantity.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                final Dialog qtyDialog = new Dialog(context);
                qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                qtyDialog.setContentView(refkey_dialogue);

                final Button buttonUpdateOk, buttonUpdateCancel;
                InputMethodManager iss = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                iss.hideSoftInputFromWindow(homeHolder.editTextQuantity.getWindowToken(), 0);
                InputMethodManager inn = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                inn.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

                final EditText editTextRefKey, editTextFreeQty, editTextTime, editTextDate;
                final TextView textQtyValidate;
                final AutoCompleteTextView textviewuom,textviewfreeuom,textviewdelivery;
                final LinearLayout linearlayoutdatetime,linearlayoutdeliverymode,linearlayoutrefkey;

                buttonUpdateOk = (Button) qtyDialog
                        .findViewById(R.id.buttonUpdateOk);
                buttonUpdateCancel = (Button) qtyDialog
                        .findViewById(R.id.buttonUpdateCancel);

                editTextRefKey = (EditText) qtyDialog
                        .findViewById(R.id.editTextRefKey);
                editTextFreeQty = (EditText) qtyDialog
                        .findViewById(R.id.editTextFreeQty);
                //textViewHeader = (TextView) findViewById(R.id.textViewHeader);
                textQtyValidate=(TextView) qtyDialog.findViewById(R.id.textQtyValidate);

                textviewuom = (AutoCompleteTextView) qtyDialog
                        .findViewById(R.id.textviewuom);
                textviewfreeuom = (AutoCompleteTextView) qtyDialog
                        .findViewById(R.id.textviewfreeuom);
                editTextTime = (EditText) qtyDialog
                        .findViewById(R.id.editTextTime);
                editTextDate = (EditText) qtyDialog
                        .findViewById(R.id.editTextDate);
                textviewdelivery = (AutoCompleteTextView) qtyDialog
                        .findViewById(R.id.textviewdelivery);
                linearlayoutdatetime = (LinearLayout) qtyDialog
                        .findViewById(R.id.linearlayoutdatetime);
                linearlayoutdeliverymode= (LinearLayout) qtyDialog
                        .findViewById(R.id.linearlayoutdeliverymode);

                textviewuom.setVisibility(View.GONE);
                textviewfreeuom.setVisibility(View.GONE);

if(item.getDfqty().equals("")){

    editTextFreeQty.setText("");
}else {
    Log.e("freeqty",item.getDfqty());
    editTextFreeQty.setText(item.getDfqty());

}
                if(item.getDqty().equals("")){

                    editTextRefKey.setText("");
                }else {
                    Log.e("freeqty",item.getDqty());
                    editTextRefKey.setText(item.getDqty());

                }
                editTextFreeQty.setSelection(editTextFreeQty.getText().length());
                editTextRefKey.setSelection(editTextRefKey.getText().length());
                try {
                    if (item.getDeliverymode().equals("")) {
                        Log.e("ins", "ins");
                        textviewdelivery.setText("Standard");
                        ArrayAdapter<String> deliverydapt = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, Constants.deliverymode);
                        textviewdelivery.setAdapter(deliverydapt);

                    } else {
                        Log.e("getDeliverymode", item.getDeliverymode());
                        if (item.getDeliverymode().equals("Standard")) {
                            textviewdelivery.setText(item.getDeliverymode());
                            linearlayoutdatetime.setVisibility(View.GONE);
                            editTextTime.setText("");
                            editTextDate.setText("");
                        } else if (item.getDeliverymode().equals("Scheduled")){
                            textviewdelivery.setText(item.getDeliverymode());
                            Log.e("deliverytime", item.getDeliverytime());
                            Log.e("deliverydate", item.getDeliverydate());
                            linearlayoutdatetime.setVisibility(View.VISIBLE);
                            editTextTime.setText(item.getDeliverytime());
                            editTextDate.setText(item.getDeliverydate());
                        }
                    }
                }catch (Exception e){

                }
                editTextTime.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final int mHour, mMinute,mSeconds;
                        final Calendar c = Calendar.getInstance();
                        mHour = c.get(Calendar.HOUR_OF_DAY);
                        mMinute = c.get(Calendar.MINUTE);
                        mSeconds = c.get(Calendar.SECOND);

                        // Launch Time Picker Dialog
                        TimePickerDialog timePickerDialog = new TimePickerDialog(context,
                                new TimePickerDialog.OnTimeSetListener() {

                                    @Override
                                    public void onTimeSet(TimePicker view, int hourOfDay,
                                                          int minute) {
                                        String minLength = String.valueOf(minute);
                                        if (minLength.length() == 1) {
                                            minLength = "0" + minLength;
                                        }
                                        editTextTime.setText(hourOfDay + ":" + minLength);
                                    }
                                }, mHour, mMinute, false);
                        timePickerDialog.show();
                    }
                });

                editTextDate.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Get Current Date
                        int mYear, mMonth, mDay;
                        final Calendar c = Calendar.getInstance();
                        mYear = c.get(Calendar.YEAR);
                        mMonth = c.get(Calendar.MONTH);
                        mDay = c.get(Calendar.DAY_OF_MONTH);

                        DatePickerDialog datePickerDialog = new DatePickerDialog(context, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT,
                                new DatePickerDialog.OnDateSetListener() {

                                    @Override
                                    public void onDateSet(DatePicker view, int year,
                                                          int monthOfYear, int dayOfMonth) {

                                        String monthLength = String.valueOf(monthOfYear);
                                        if (monthLength.length() == 1) {
                                            monthLength = "0" + monthLength;
                                        }
                                        String dayLength = String.valueOf(dayOfMonth);
                                        if (dayLength.length() == 1) {
                                            dayLength = "0" + dayLength;
                                        }
                                        editTextDate.setText(dayLength + "-" + (Integer.parseInt(monthLength) + 1) + "-" + year);

                                    }
                                }, mYear, mMonth, mDay);
                        datePickerDialog.show();
                    }
                });

                textviewdelivery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        selecteddeliverymode=textviewdelivery.getText().toString();
                        if (selecteddeliverymode.equals("Scheduled")) {
                            linearlayoutdatetime.setVisibility(View.VISIBLE);
                            final Calendar c = Calendar.getInstance();
                            mYear = c.get(Calendar.YEAR);
                            mMonth = c.get(Calendar.MONTH);
                            mDay = c.get(Calendar.DAY_OF_MONTH);
                            mHour = c.get(Calendar.HOUR_OF_DAY);
                            mMinute = c.get(Calendar.MINUTE);
                            mSeconds= c.get(Calendar.SECOND);

                            monthLength = String.valueOf(mMonth + 1);
                            if (monthLength.length() == 1) {
                                monthLength = "0" + monthLength;
                            }
                            dayLength = String.valueOf(mDay);
                            if (dayLength.length() == 1) {
                                dayLength = "0" + dayLength;
                            }

                            String minuteLength = String.valueOf(mMinute);
                            if (minuteLength.length() == 1) {
                                minuteLength = "0" + minuteLength;
                            }

                            String AM_PM ;
                            if(mHour < 12) {
                                AM_PM = "AM";
                            } else {
                                AM_PM = "PM";
                            }
                            Log.e("AM_PM",AM_PM);
                            // set current date into textview
                            editTextDate.setText(new StringBuilder()
                                    // Month is 0 based, just add 1
                                    .append(dayLength).append("-").append(monthLength).append("-").append(mYear)
                            );

                            editTextTime.setText(new StringBuilder()
                                    // Month is 0 based, just add 1
                                    .append(mHour).append(":").append(minuteLength)
                            );

                        } else {
                            linearlayoutdatetime.setVisibility(View.GONE);
                            editTextTime.setText("");
                            editTextDate.setText("");
                        }

                    }
                });

                textviewdelivery.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        textviewdelivery.setText("");
                        ArrayAdapter<String> deliverydapt = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, Constants.deliverymode);
                        textviewdelivery.setAdapter(deliverydapt);
                        textviewdelivery.showDropDown();
                        InputMethodManager iss = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        iss.hideSoftInputFromWindow(textviewdelivery.getWindowToken(), 0);
                        return false;
                    }
                });
                qtyDialog.show();


                buttonUpdateOk.setOnClickListener(new OnClickListener() {

                                                      @Override
                                                      public void onClick(View v) {
                                                          dod = new DealerAddProductDomain();
                                                          qty = editTextRefKey.getText().toString();
                                                          freeQty = editTextFreeQty.getText().toString();
                                                          selecteddeliverymode= textviewdelivery.getText().toString();

                                                          InputMethodManager inn = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                                                          inn.hideSoftInputFromWindow(buttonUpdateOk.getWindowToken(), 0);
                                                         /* InputMethodManager iss = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                                                          iss.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);*/

                                                          if (!validation(qty, freeQty)) {
                                                             /* if (qty.startsWith(".") || freeQty.startsWith(".")) {
                                                                  textQtyValidate.setText("Please enter zero before Dot");
                                                              } else {
                                                                  textQtyValidate.setText("Please enter valid quantity");
                                                              }*/
                                                              textQtyValidate.setText("Please enter valid quantity");
                                                          } else {
                                                              if (qty.isEmpty() || qty.equals(".")) {
                                                                  item.setDqty("0");
                                                              } else {
                                                                  item.setDqty(qty);
                                                              }
                                                              if (freeQty.isEmpty() || freeQty.equals(".")) {
                                                                  item.setDfqty("0");
                                                              } else {
                                                                  item.setDfqty(freeQty);
                                                              }

                                                              if(selecteddeliverymode.equals("Standard")){
                                                                  Log.e("Standard","Standard");
                                                                  item.setDeliverymode(selecteddeliverymode);
                                                                  item.setDeliverydate("");
                                                                  item.setDeliverytime("");
                                                              }else if(selecteddeliverymode.equals("Scheduled")){
                                                                  Log.e("Scheduled","Scheduled");
                                                                  item.setDeliverymode(selecteddeliverymode);
                                                                  item.setDeliverydate(editTextDate.getText().toString());
                                                                  item.setDeliverytime(editTextTime.getText().toString());
                                                              }else {
                                                                  item.setDeliverydate("");
                                                                  item.setDeliverytime("");
                                                                  item.setDeliverymode("");
                                                              }

                                                              deliverydate = editTextDate.getText().toString();
                                                              deliverymode = textviewdelivery.getText().toString();
                                                              deliverytime = editTextTime.getText().toString();
                                                              dod.setDfqty(item.getDfqty());

                                                              dod.setProductCode(item.getProductCode());
                                                              dod.setProductID(item.getProductID());
                                                              dod.setProductName(item.getProductName());
                                                              dod.setDqty(item.getDqty());
                                                              dod.setDeliverydate(item.getDeliverydate());
                                                              dod.setDeliverymode(item.getDeliverymode());
                                                              dod.setDeliverytime(item.getDeliverytime());
                                                              dod.setUnitGram(item.getUnitGram());
                                                              dod.setUnitPerUom(item.getUnitPerUom());
                                                              dod.setUom(item.getUom());
                                                              dod.setPrice(item.getPrice());
                                                              dod.setTax(item.getTax());
                                                              prodid = item.getProductID();

                                                              if (updateQty() == 0) {
                                                                  DealerOrderDetailsActivity.arraylistMProductListSelected.add(dod);

                                                              }


                                                              homeHolder.editTextQuantity.setText("NF: [" + item.getDqty() + "]" + "\n" + "F: [" + item.getDfqty() + "]");
                                                              qtyDialog.dismiss();
                                                          }


                                                      }

                                                  }

                );

                buttonUpdateCancel
                        .setOnClickListener(new View.OnClickListener()

                                            {

                                                @Override
                                                public void onClick(View v) {
                                                    // TODO Auto-generated method stub

                                                    InputMethodManager inn = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                                                    inn.hideSoftInputFromWindow(buttonUpdateCancel.getWindowToken(), 0);
                                                    /*InputMethodManager iss = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                                                    iss.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);*/

//textViewHeader.setVisibility(View.INVISIBLE);
								/*InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
								imm.hideSoftInputFromWindow(buttonUpdateCancel.getWindowToken(), 0);*/
                                                    qtyDialog.dismiss();
                                                }


                                            }

                        );
            }
        });

        return row;
    }
    private boolean validation(String qty,String freeQty) {
        Pattern pattern = Pattern.compile("(^[1-9][0-9]*)+((\\.\\d)?)");//(^[1-9][0-9]*)+((\.\d)?)//(^[1-9][0-9]*)+((^\d+\.?\d*)?)

        Matcher matcherq = pattern.matcher(qty);
        Matcher matcherf = pattern.matcher(freeQty);
        if (matcherq.matches()){
            return matcherq.matches();
        } else if (matcherf.matches()){
            return matcherf.matches();
        }else{
            return false;
        }
    }

    public int updateQty() {

            for (int j = 0; j < DealerOrderDetailsActivity.arraylistMProductListSelected.size(); j++) {
                Log.e("outsideprodid", prodid);
                Log.e("merchantprodid", DealerOrderDetailsActivity.arraylistMProductListSelected.get(j).getProductID());
                if (DealerOrderDetailsActivity.arraylistMProductListSelected.get(j).getProductID().equals(prodid)) {

                    DealerOrderDetailsActivity.arraylistMProductListSelected.get(j).setDfqty(freeQty);
                    DealerOrderDetailsActivity.arraylistMProductListSelected.get(j).setDqty(qty);
                    DealerOrderDetailsActivity.arraylistMProductListSelected.get(j).setDeliverydate(deliverydate);
                    DealerOrderDetailsActivity.arraylistMProductListSelected.get(j).setDeliverymode(deliverymode);
                    DealerOrderDetailsActivity.arraylistMProductListSelected.get(j).setDeliverytime(deliverytime);
                    Log.e("updatesize", String.valueOf(DealerOrderDetailsActivity.arraylistMProductListSelected.size()));
                    return 1;

                }
                Log.e("j", String.valueOf(j));

            }
            return 0;

    }
    class HomeHolder2 {

        TextView textViewName, textViewSubname,textViewserialno;
        EditText editTextQuantity;
    }

}
