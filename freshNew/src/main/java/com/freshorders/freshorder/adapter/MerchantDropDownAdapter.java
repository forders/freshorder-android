package com.freshorders.freshorder.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.domain.MerchantDropdownDomain;
import com.freshorders.freshorder.ui.CreateOrderActivity;
import com.freshorders.freshorder.utils.JsonServiceHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class MerchantDropDownAdapter extends ArrayAdapter<MerchantDropdownDomain> {
	Context context;
	int resource;
	ArrayList<MerchantDropdownDomain> arrayListServiceDomainObjects,tempItems,suggestions;

	JSONObject JsonAccountObject = null;
	JSONArray JsonAccountArray = null;
	JsonServiceHandler JsonServiceHandler;

	public MerchantDropDownAdapter(Context context, int resource,
								   ArrayList<MerchantDropdownDomain> arrayListServiceDomainObjects) {
		super(context, resource, arrayListServiceDomainObjects);
		this.context = context;
		this.resource = resource;
		this.arrayListServiceDomainObjects = arrayListServiceDomainObjects;
		tempItems = new ArrayList<MerchantDropdownDomain>(arrayListServiceDomainObjects); // this makes the difference.
		suggestions = new ArrayList<MerchantDropdownDomain>();
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();
	}




	@Override
	public int getCount() {
		return arrayListServiceDomainObjects.size();
	}

	@Override
	public boolean isEnabled(int position) {
		return true;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// Log.e("Adapter position",String.valueOf(position));
		View row = convertView;
		final HomeHolder homeHolder;
		if (row == null) {
			// Log.e("Adapter Inner position",String.valueOf(position));
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(resource, parent, false);
			homeHolder = new HomeHolder();

			homeHolder.textViewnew = (TextView) row
					.findViewById(R.id.textViewnew);
			homeHolder.linearlayoutnew = (LinearLayout) row
					.findViewById(R.id.linearlayoutnew);

			row.setTag(homeHolder);
		} else {
			homeHolder = (HomeHolder) row.getTag();
		}
		MerchantDropdownDomain item = arrayListServiceDomainObjects
				.get(position);

		if(item.getmflag().equals("R")){
			homeHolder.textViewnew.setText(item.getComName());
			homeHolder.textViewnew.setTextColor(Color.parseColor("#F78181"));
			homeHolder.linearlayoutnew.setBackgroundColor(Color.parseColor("#000000"));
		}else if(item.getmflag().equals("G")){
			homeHolder.textViewnew.setText(item.getComName());
			homeHolder.textViewnew.setTextColor(Color.parseColor("#04B404"));
			homeHolder.linearlayoutnew.setBackgroundColor(Color.parseColor("#000000"));

		}else{
			homeHolder.textViewnew.setText(item.getComName());
			homeHolder.textViewnew.setTextColor(Color.parseColor("#ffffff"));
			homeHolder.linearlayoutnew.setBackgroundColor(Color.parseColor("#000000"));
		}
		return row;
	}

	@Override
	public Filter getFilter() {

		return nameFilter;
	}

	/**
	 * Custom Filter implementation for custom suggestions we provide.
	 */
	Filter nameFilter = new Filter() {
		@Override
		public CharSequence convertResultToString(Object resultValue) {

			String str = ((MerchantDropdownDomain) resultValue).getComName();
			return str;
		}

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			if (constraint != null) {
				suggestions.clear();
				for (MerchantDropdownDomain people : tempItems) {
					if (people.getComName().toLowerCase().contains(constraint.toString().toLowerCase())) {
						suggestions.add(people);
					}
				}
				FilterResults filterResults = new FilterResults();
				filterResults.values = suggestions;
				filterResults.count = suggestions.size();
				return filterResults;
			} else {
				return new FilterResults();
			}
		}

		@Override
		protected void publishResults(CharSequence constraint, FilterResults results) {
			ArrayList<MerchantDropdownDomain> filterList = (ArrayList<MerchantDropdownDomain>) results.values;
			if (results != null && results.count > 0) {
				clear();
				for (MerchantDropdownDomain people : filterList) {
					add(people);
					notifyDataSetChanged();
				}
			}

		}
	};
	class HomeHolder {

		TextView textViewnew;
		LinearLayout linearlayoutnew;
	}

}