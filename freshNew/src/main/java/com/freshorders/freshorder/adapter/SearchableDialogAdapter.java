package com.freshorders.freshorder.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.freshorders.freshorder.R;

import java.util.ArrayList;
import java.util.List;

public class SearchableDialogAdapter extends
        RecyclerView.Adapter<SearchableDialogAdapter.ItemViewHolder> implements Filterable {

    @Override
    public Filter getFilter() {
        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String newText = charSequence.toString();
                if (newText.isEmpty()) {
                    filteredListItems = listItems;
                } else {
                    final List<String> resultItemList = new ArrayList<>();
                    for (String item : listItems) {
                        if (item.toLowerCase().contains(newText.toLowerCase()) || item.contains(newText)) {
                            resultItemList.add(item);
                        }
                    }
                    filteredListItems = resultItemList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredListItems;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredListItems = (ArrayList<String>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface OnItemClickListener {
        void onItemClick(String item);
    }

    private final OnItemClickListener listener;
    private Context mContext;
    private List<String> listItems;
    private List<String> filteredListItems;

    public SearchableDialogAdapter(Context mContext, List<String> listItems, OnItemClickListener listener) {
        this.mContext = mContext;
        this.listItems = listItems;
        this.listener = listener;
        this.filteredListItems = listItems;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_search_view, viewGroup, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        String itemName = listItems.get(position);
        holder.tvItemName.setText(itemName);
        holder.tvItemName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String item = ((TextView) view).getText().toString();
                listener.onItemClick(item);
            }
        });
    }

    @Override
    public int getItemCount() {
        return filteredListItems.size();
    }


    public static class ItemViewHolder extends RecyclerView.ViewHolder{

        TextView tvItemName;
        public ItemViewHolder(View itemView) {
            super(itemView);

            tvItemName = (TextView) itemView.findViewById(R.id.id_Search_TV_item_name);
        }
    }
}

