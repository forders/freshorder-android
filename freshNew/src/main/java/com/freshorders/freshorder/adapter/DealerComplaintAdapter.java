package com.freshorders.freshorder.adapter;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.domain.DealerComplaintDomain;
import com.freshorders.freshorder.utils.JsonServiceHandler;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class DealerComplaintAdapter extends ArrayAdapter<DealerComplaintDomain> {
	Context context;
	int resource;
	ArrayList<DealerComplaintDomain> arrayListServiceDomainObjects;

	JSONObject JsonAccountObject = null;
	JSONArray JsonAccountArray = null;
	JsonServiceHandler JsonServiceHandler;

	public DealerComplaintAdapter(Context context, int resource,
			ArrayList<DealerComplaintDomain> arrayListServiceDomainObjects) {
		super(context, resource, arrayListServiceDomainObjects);
		this.context = context;
		this.resource = resource;
		this.arrayListServiceDomainObjects = arrayListServiceDomainObjects;
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();
	}

	@Override
	public int getCount() {
		return arrayListServiceDomainObjects.size();
	}

	@Override
	public boolean isEnabled(int position) {
		return true;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// Log.e("Adapter position",String.valueOf(position));
		View row = convertView;
		final HomeHolder homeHolder;
		if (row == null) {
			// Log.e("Adapter Inner position",String.valueOf(position));
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(resource, parent, false);
			homeHolder = new HomeHolder();

			homeHolder.textViewName = (TextView) row
					.findViewById(R.id.textViewName);
			homeHolder.textViewAddress = (TextView) row
					.findViewById(R.id.textViewAddress);
			
		

			row.setTag(homeHolder);
		} else {
			homeHolder = (HomeHolder) row.getTag();
		}
		DealerComplaintDomain item = arrayListServiceDomainObjects
				.get(position);
		homeHolder.textViewName.setText(item.getCompanyName()+"  ["+item.getMuserId()+"]");
		homeHolder.textViewAddress.setText(item.getAddress());
		

		return row;
	}

	class HomeHolder {

		TextView textViewName, textViewAddress,textViewDate;
	}

}