package com.freshorders.freshorder.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.domain.OutletStockEntryDetailDomain;
import com.freshorders.freshorder.utils.JsonServiceHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OutletStockDetailAdapter extends ArrayAdapter<OutletStockEntryDetailDomain> {
	Context context;
	int resource;
	ArrayList<OutletStockEntryDetailDomain> arraylistDealerOrderDetailList;
	JSONObject JsonAccountObject = null;
	JSONArray JsonAccountArray = null;
	JsonServiceHandler JsonServiceHandler;
	DatePicker datePicker;
	TimePicker timePicker;
public static String time,currentdate;
	private int year;
	private int month, date;
	private int day, today,oldQty =0;
	String[] items;
	public static Date dateStr = null;
	String stock="",batchno="",fromdate="";
	public int monthnew,yearnew,daynew;


	public OutletStockDetailAdapter(Context context, int resource, ArrayList<OutletStockEntryDetailDomain> arraylistDealerOrderDetailList) {
		super(context,resource,arraylistDealerOrderDetailList);
		this.context = context;
		this.resource = resource;
		this.arraylistDealerOrderDetailList = arraylistDealerOrderDetailList;
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();
	}

	@Override
	public int getCount() {
		return arraylistDealerOrderDetailList.size();
	}

	@Override
	public boolean isEnabled(int position) {
		return true;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// Log.e("Adapter position",String.valueOf(position));
		View row = convertView;
		final HomeHolder homeHolder;
		if (row == null) {
			// Log.e("Adapter Inner position",String.valueOf(position));
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(resource, parent, false);
			homeHolder = new HomeHolder();

			homeHolder.textViewProductName = (TextView) row
					.findViewById(R.id.textViewProductName);
			homeHolder.textViewQty = (TextView) row
					.findViewById(R.id.textViewQty);
			homeHolder.textViewDate = (TextView) row
					.findViewById(R.id.textViewDate);

			homeHolder.textviewexpand = (TextView) row
					.findViewById(R.id.textviewexpand);

			homeHolder.edittextStock = (EditText) row
					.findViewById(R.id.edittextStock);

			Typeface font = Typeface.createFromAsset(context.getAssets(),
					"fontawesome-webfont.ttf");
			homeHolder.textviewexpand.setTypeface(font);


			row.setTag(homeHolder);
		} else {
			homeHolder = (HomeHolder) row.getTag();
		}
		final OutletStockEntryDetailDomain item = arraylistDealerOrderDetailList
				.get(position);



		homeHolder.textViewProductName.setText(item.getproductname()+" ["+item.getFlag()+"]");
		homeHolder.textViewQty.setText(item.getqty()+" ["+item.getFlag()+"]");

		Log.e("stock",item.getstock());
		if(item.getstock().equals("0")||item.getstock().isEmpty()||item.getstock().equals("null")){
			item.setstock("");


		}

		if(item.getbatchno().equals("null")||item.getbatchno().isEmpty()){
			item.setbatchno("");
		}

		if(item.getmanufdt().equals("null")||item.getmanufdt().isEmpty()){
			item.setmanufdt("");
		}
		homeHolder.edittextStock.setText(item.getstock());


		homeHolder.edittextStock.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				Log.e("Clicked", "Clicked");
				final Dialog qtyDialog = new Dialog(context);
				qtyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				qtyDialog.setContentView(R.layout.outlet_dialog);
				final DatePicker datePicker;

				final Button buttonUpdateOk, buttonUpdateCancel;

				final EditText editTextBatchNo, editTextstock,EditTextFromDate;
				final TextView textQtyValidate;


				InputMethodManager imm = (InputMethodManager) context
						.getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
				buttonUpdateOk = (Button) qtyDialog
						.findViewById(R.id.buttonUpdateOk);
				buttonUpdateCancel = (Button) qtyDialog
						.findViewById(R.id.buttonUpdateCancel);

				editTextstock = (EditText) qtyDialog
						.findViewById(R.id.editTextstock);
				editTextBatchNo = (EditText) qtyDialog
						.findViewById(R.id.editTextBatchNo);
				textQtyValidate = (TextView) qtyDialog
						.findViewById(R.id.textQtyValidate);
				datePicker= (DatePicker) qtyDialog
						.findViewById(R.id.datePicker1);

				EditTextFromDate = (EditText) qtyDialog
						.findViewById(R.id.EditTextFromDate);

				editTextstock.setSelection(editTextstock.getText().length());
				editTextBatchNo.setSelection(editTextBatchNo.getText().length());

				//textViewmanufdtdate.setText("Manuf Date : " + item.getmanufdt());

				EditTextFromDate.setText(item.getmanufdt());
				editTextstock.setText(homeHolder.edittextStock.getText().toString());
				editTextBatchNo.setText(item.getbatchno());

				//date picker shows current date
				final Calendar c = Calendar.getInstance();

				yearnew = c.get(Calendar.YEAR);
				monthnew=c.get(Calendar.MONTH);
				daynew = c.get(Calendar.DAY_OF_MONTH);



				String monthLength =String.valueOf(monthnew+1);
				if(monthLength.length()==1){
					monthLength = "0"+monthLength;
				}
				String dayLength =String.valueOf(daynew);
				if(dayLength.length()==1){
					dayLength = "0"+dayLength;
				}

				Log.e("monthLength",monthLength);

				Log.e("dayLength",dayLength);
				// set current date into textview

				currentdate = year + "-" + (month + 1)
						+ "-" + daynew;

				Log.e("date",currentdate);

				String inputPattern = "yyyy-MM-dd";
				String outputPattern = "yyyy-MM-dd";
				SimpleDateFormat inputFormat = new SimpleDateFormat(
						inputPattern);
				SimpleDateFormat outputFormat = new SimpleDateFormat(
						outputPattern);

				String str = null;
				try {
					dateStr = inputFormat.parse(currentdate);
					str = outputFormat.format(dateStr);

					/*if(item.getmanufdt().equals("null"))
					{
					EditTextFromDate.setText(str);
					}*/
					// Log.e("str", str);
				} catch (Exception  e) {
					e.printStackTrace();
				}


				// set current date into datepicker
				datePicker.init(year, monthnew, day, null);



				EditTextFromDate.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View view) {
						DatePickerDialog mdiDialog = new DatePickerDialog(
								context, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT,
								new DatePickerDialog.OnDateSetListener() {
									@Override
									public void onDateSet(DatePicker view,
														  int year, int month,
														  int dayOfMonth) {
										Log.e("date1", year + "-" + (month)
												+ "-" + day);

										Log.e("date", year + "-" + (month + 1)
												+ "-" + dayOfMonth);
										String time = year + "-" + (month + 1)
												+ "-" + dayOfMonth;

										String monthLength = String.valueOf(month + 1);
										Log.e("date", monthLength);
										if (monthLength.length() == 1) {
											monthLength = "0" + monthLength;
										}
										String dayLength = String.valueOf(dayOfMonth);
										if (dayLength.length() == 1) {
											dayLength = "0" + dayLength;
										}
										fromdate = year + "-" + monthLength
												+ "-" + dayLength;
										Log.e("date", fromdate);
										monthnew = month;
										yearnew = year;
										daynew = dayOfMonth;
										Log.e("date3", String.valueOf(month));
										Log.e("date2", String.valueOf(monthnew));
										String inputPattern = "yyyy-MM-dd";
										String outputPattern = "dd-MM-yyyy";
										SimpleDateFormat inputFormat = new SimpleDateFormat(
												inputPattern);
										SimpleDateFormat outputFormat = new SimpleDateFormat(
												outputPattern);

										String str = null;
										try {
											dateStr = inputFormat.parse(time);
											str = outputFormat.format(dateStr);
											EditTextFromDate.setText(str);
											// Log.e("str", str);
										} catch (Exception e) {
											e.printStackTrace();
										}

									}
								}, yearnew, monthnew, daynew);
						mdiDialog.show();


					}
				});


				qtyDialog.show();





				buttonUpdateOk.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {

						stock = editTextstock.getText().toString();
						batchno = editTextBatchNo.getText().toString();


						if (stock.isEmpty() || stock.equals(".")||stock.equals("")) {

							textQtyValidate.setText("Please enter stock");
						}else{



						if (batchno.isEmpty() || batchno.equals(".")) {
							item.setbatchno("");
						}
						item.setmanufdt(EditTextFromDate.getText().toString());
						homeHolder.edittextStock.setText(stock);
						item.setbatchno(batchno);
						item.setstock(stock);


						Log.e("stock", stock);
						Log.e("batch no", batchno);
						Log.e("datenew", fromdate);
						InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(buttonUpdateOk.getWindowToken(), 0);


						qtyDialog.dismiss();

						}
					}

				});

				buttonUpdateCancel
						.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
								imm.hideSoftInputFromWindow(buttonUpdateCancel.getWindowToken(), 0);
								qtyDialog.dismiss();

							}
						});
			}
		});





		return row;
	}

	private void showCurrentDateOnView() {
		// TODO Auto-generated method stub



	}


	class HomeHolder {
		RelativeLayout relativeLayoutCalender, relativeLayoutStatus;


		TextView textViewProductName, textViewQty, textViewDate
		;
		TextView textViewName, textviewexpand, textViewPlus;
		EditText edittextStock;
	}

}
