package com.freshorders.freshorder.adapter;


import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.MerchantOrderDetailDomain;
import com.freshorders.freshorder.domain.MerchantOrderDomainSelected;
import com.freshorders.freshorder.model.Template3Model;
import com.freshorders.freshorder.ui.CreateOrderActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.freshorders.freshorder.MyApplication.previousSelected;
import static com.freshorders.freshorder.MyApplication.previousSelectedForSearch;
import static com.freshorders.freshorder.utils.Constants.CHAR_DOT;
import static com.freshorders.freshorder.utils.Constants.DOT;

public class Template4MerchantOrderDetailAdapter extends ArrayAdapter<MerchantOrderDetailDomain> {
    Context context;
    int resource;

    ArrayList<MerchantOrderDetailDomain> arraylistMerchantOrderDetailList;
    ArrayList<MerchantOrderDetailDomain>arrayListSearchResults;
    public static ArrayList<MerchantOrderDomainSelected> arraylistMerchantOrderDetailListSelected, arraylistsavearray;
    JSONObject JsonAccountObject = null;
    JSONArray JsonAccountArray = null;
    com.freshorders.freshorder.utils.JsonServiceHandler JsonServiceHandler;
    String qty = "", freeQty = "", orderuom = "",price = "", defaultuom = "", freeqtyuom = "",deliverydate="",deliverytime="",deliverymode="",currstock="";
    public static String prodid, multipleUomCount = "",selecteddeliverymode="",monthLength="",dayLength="";
    //int index=1;
    public static EditText editTextSearchField;
    public static TextView textViewHeader, textQtyValidat;
    public static MerchantOrderDomainSelected dod;
    DatabaseHandler databaseHandler;
    public static int mYear, mMonth, mDay, mHour, mMinute,mSeconds;
    private boolean netcheck;

    public Map<Integer, Template3Model> merchantOrderDetailList = new LinkedHashMap<>();
    Map<Integer, String> priceList;
    Map<Integer, String> quantityList;

    Map<Integer, String> changedUOM = new LinkedHashMap<>();
    Map<Integer, Integer> spinRetain = new LinkedHashMap<>();

    //public static String searchclick="0";
    public Template4MerchantOrderDetailAdapter(
            Context context,
            int resource,
            ArrayList<MerchantOrderDetailDomain> arraylistMerchantOrderDetailList, Boolean netcheck) {
        super(context, resource, arraylistMerchantOrderDetailList);
        Log.e("MerchantOrdDtlAdapter",netcheck.toString());
        this.context = context;
        this.resource = resource;
        this.arraylistMerchantOrderDetailList = arraylistMerchantOrderDetailList;
        this.netcheck = netcheck;
        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();
        databaseHandler = new DatabaseHandler(context);
        priceList = new LinkedHashMap<>(arraylistMerchantOrderDetailList.size());
        quantityList = new LinkedHashMap<>(arraylistMerchantOrderDetailList.size());
        Log.e("Template4",".........previousSelected..."+previousSelected.size());
        for(int i = 0; i < arraylistMerchantOrderDetailList.size(); i++) {
            String productId = arraylistMerchantOrderDetailList.get(i).getprodid();
            if(previousSelectedForSearch.get(productId) != null){///////////////////for Search
                MerchantOrderDomainSelected item = previousSelectedForSearch.get(productId);
                priceList.put(i, item.getprice());
                quantityList.put(i, item.getqty());
                changedUOM.put(i, item.getorderuom());
                //changedUOM.put(i, item.getorderuom());
            }else {
                priceList.put(i, arraylistMerchantOrderDetailList.get(i).getprodprice());
                quantityList.put(i, "");
            }
        }
        previousSelected.clear(); // need to clear

    }

    @Override
    public int getCount() {
        return arraylistMerchantOrderDetailList.size();
    }




    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Log.e("Adapter position",String.valueOf(position));
        View row = convertView;
        final Template4MerchantOrderDetailAdapter.HomeHolder2 homeHolder;

        if (row == null) {
            // Log.e("Adapter Inner position",String.valueOf(position));
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(resource, parent, false);
            homeHolder = new Template4MerchantOrderDetailAdapter.HomeHolder2();
            homeHolder.textViewName = (TextView) row
                    .findViewById(R.id.textViewName);
            homeHolder.tvItemPrice = (TextView) row
                    .findViewById(R.id.editTextItemPrice);
            homeHolder.editTextOrderQuantity = (EditText) row.findViewById(R.id.editTextOrderQuantity);
            homeHolder.serialno = (TextView) row
                    .findViewById(R.id.serialno);
            homeHolder.tvCurrentStock = (TextView)row.findViewById(R.id.id_template2_tv_stock);
            homeHolder.atvUOM = (Spinner) row.findViewById(R.id.id_template2_atv_uom);

            homeHolder.atvUOM.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    int position = (int) homeHolder.atvUOM.getTag();
                    changedUOM.put(position, ((TextView) view).getText().toString());/////////
                    spinRetain.put(position, i);
                }
                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });


            homeHolder.editTextOrderQuantity.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }
                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }
                @Override
                public void afterTextChanged(Editable editable) {

                    int position = (int) homeHolder.tvItemPrice.getTag();

                    String productId = arraylistMerchantOrderDetailList.get(position).getprodid();//////Search

                    String quantity = editable.toString().trim();
                    Template3Model template3Model = merchantOrderDetailList.get(position);
                    String price = homeHolder.tvItemPrice.getText().toString().trim();
                    if(template3Model != null){
                        String itemPrice = template3Model.getItemPrice();
                        if(quantity.isEmpty() && itemPrice.isEmpty()){
                            merchantOrderDetailList.remove(position);
                            quantityList.remove(position);
                            if(previousSelectedForSearch.get(productId) != null){///////Search
                                previousSelectedForSearch.remove(productId);
                            }
                        } else if(!quantity.isEmpty()){  ////&& closeStock.length() > 0
                            Template3Model temp = merchantOrderDetailList.get(position);
                            temp.setOrderQuantity(quantity);
                            temp.setItemPrice(price);
                            temp.setProductId(productId);//////Search
                            merchantOrderDetailList.put(position, temp);
                            quantityList.put(position, quantity);
                        } else if(quantity.isEmpty()){
                            merchantOrderDetailList.remove(position);
                            quantityList.remove(position);
                            if(previousSelectedForSearch.get(productId) != null){///////Search
                                previousSelectedForSearch.remove(productId);
                            }
                        }
                    }else {  ///// this part is focused for first entry
                        if(!quantity.isEmpty()) {
                            if(!(quantity.length() == 1 && quantity.startsWith("."))) {
                                Template3Model newModel = new Template3Model();
                                // this is special case need to set price also
                                if(price.length() > 0 && !(price.length() == 1 && price.startsWith("."))){
                                    newModel.setOrderQuantity(quantity);
                                    newModel.setItemPrice(price);
                                    newModel.setProductId(productId);//////Search
                                    merchantOrderDetailList.put(position, newModel);
                                    quantityList.put(position, quantity);
                                }
                            }
                        }
                    }
                }
            });

            row.setTag(homeHolder);
        } else {
            homeHolder = (Template4MerchantOrderDetailAdapter.HomeHolder2) row.getTag();
        }

        final MerchantOrderDetailDomain item = arraylistMerchantOrderDetailList
                .get(position);

        arraylistsavearray = new ArrayList<MerchantOrderDomainSelected>();
        homeHolder.textViewName.setText(Html.fromHtml(item.getprodname() + "  ["
                + item.getprodcode() + "]"));
        homeHolder.tvCurrentStock.setText(item.getStockPosition()); /////////////////////
        homeHolder.serialno.setText(item.getserialno() +".");

        String[] array = CreateOrderActivity.itemListUOM.get(item.getprodid());
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_dropdown_item, array);
        homeHolder.atvUOM.setAdapter(adapter);


        homeHolder.editTextOrderQuantity.setTag(position);
        homeHolder.tvItemPrice.setTag(position);
        homeHolder.atvUOM.setTag(position);


        int spinnerPosition = -1;
        if(!changedUOM.containsKey(position)) {
            spinnerPosition = adapter.getPosition(item.getSlectedUOM()); ///// default UOM Setting
            homeHolder.atvUOM.setSelection(spinnerPosition);
        }else {
            if(spinRetain.get(position) != null) {
                homeHolder.atvUOM.setSelection(spinRetain.get(position));
            }else {
                spinnerPosition = adapter.getPosition(changedUOM.get(position)); ///// Previous(back button) UOM Setting
                homeHolder.atvUOM.setSelection(spinnerPosition);
            }
        }
        homeHolder.tvItemPrice.setText(priceList.get(position));
        homeHolder.editTextOrderQuantity.setText(quantityList.get(position));


        Log.e("position","~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~`"+position);

        return row;
    }

    public Map<Integer, Template3Model> getSelectedMerchantOrders(){
        return merchantOrderDetailList;
    }

    public ArrayList<MerchantOrderDetailDomain> getArrayListMerchantOrderDetailList(){
        return arraylistMerchantOrderDetailList;
    }

    public Map<Integer, String> getChangedUOM(){
        return changedUOM;
    }

    public boolean price_quantity_pairCheck(){

        for(Map.Entry entry: merchantOrderDetailList.entrySet()){

            int i = (int) entry.getKey();
            String strQuantity = merchantOrderDetailList.get(i).getOrderQuantity();

            if(strQuantity.length() > 0) {
                // Trim Last Dot
                if (strQuantity.charAt(strQuantity.length() - 1) == CHAR_DOT) {
                    Template3Model temp = merchantOrderDetailList.get(i);
                    temp.setOrderQuantity(strQuantity.substring(0, strQuantity.lastIndexOf(CHAR_DOT)));
                    merchantOrderDetailList.put(i, temp);
                }
            }
            //String price = priceList.get(i);
            String strPrice = merchantOrderDetailList.get(i).getItemPrice();
            if(strQuantity.isEmpty() || strPrice.isEmpty()){
                return false;
            }
            if(strQuantity.length() == 1 && strQuantity.equals(DOT)){
                return false;
            }
            if(strQuantity.length() == 1 && strQuantity.equals(DOT)){
                return false;
            }
            /*
            for(int count = 0; count < quantity.length(); count++){  /// Multiple DOT occurrence test for quantity
                int occurrence = 0;
                if (quantity.charAt(i) == CHAR_DOT){
                    occurrence++;
                    if(occurrence > 1){
                        return false;
                    }
                }
            }
            for(int count = 0; count < quantity.length(); count++){ /// Multiple DOT occurrence test for price
                int occurrence = 0;
                if (price.charAt(i) == CHAR_DOT){
                    occurrence++;
                    if(occurrence > 1){
                        return false;
                    }
                }
            } */  // NO NEEd ............

        }
        return true;
    }


    class HomeHolder2 {
        //RelativeLayout relativeLayoutAddQuantity;
        TextView textViewName, serialno, tvCurrentStock, tvItemPrice;

        //ImageView imageView;
        EditText editTextOrderQuantity;
        Spinner atvUOM;

    }

}
