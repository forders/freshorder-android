package com.freshorders.freshorder.adapter;
import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.domain.DistanceCalculationDomain;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Karthika on 01/03/2018.
 */

public class DistanceCalculationAdapterUnSync extends ArrayAdapter<DistanceCalculationDomain> {
    Context context;
    int resource;
    ArrayList<DistanceCalculationDomain> arrayListObjects;
    public DistanceCalculationAdapterUnSync(Context context, int resource,
                                      ArrayList<DistanceCalculationDomain> arrayListObjects) {
        super(context, resource, arrayListObjects);
        this.context = context;
        this.resource = resource;
        this.arrayListObjects = arrayListObjects;
    }


    @Override
    public int getCount() {
        return arrayListObjects.size();
    }


    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        final DistanceCalculationAdapterUnSync.HomeHolder homeHolder;
        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(resource, parent, false);
            homeHolder = new DistanceCalculationAdapterUnSync.HomeHolder();

            homeHolder.distancedate = (TextView) row
                    .findViewById(R.id.distancedate);

            homeHolder.distanceamount = (TextView) row
                    .findViewById(R.id.distanceamount);

            homeHolder.distance = (TextView) row
                    .findViewById(R.id.distance);

            row.setTag(homeHolder);
        } else {
            homeHolder = (DistanceCalculationAdapterUnSync.HomeHolder) row.getTag();
        }

        final DistanceCalculationDomain item = arrayListObjects
                .get(position);
        try
        {
            homeHolder.distancedate.setText(new SimpleDateFormat("dd-MMM-yyyy").format(new SimpleDateFormat("EEE MMM dd HH:mm:ss ZZZZ yyyy").parse(item.getLogtime())));
            Log.e("UnSyncDatatry","UnSyncDatatry");
            //homeHolder.distancedate.setText(item.getLogtime());
            DecimalFormat format = new DecimalFormat("##.00");
            homeHolder.distanceamount.setText(format.format(Double.parseDouble(item.getAmount())));
            Log.e("UnSyncDatatry--","UnSyncDatatry--");
            homeHolder.distance.setText(format.format(Double.parseDouble(item.getTraveldistance())));
            Log.e("UnSyncDatatry--","UnSyncDatatry--");

        }catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return row;

    }

    static class HomeHolder {

        TextView distancedate, distanceamount,distance;
    }


}
