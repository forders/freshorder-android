package com.freshorders.freshorder.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.model.GRN;

import java.util.List;

public class GrnAdapter extends ArrayAdapter<GRN> {

    private Context context;
    private int layoutResourceId;
    private List<GRN> data = null;

    public GrnAdapter(@NonNull Context context, int resource, List<GRN> objects) {
        super(context, resource, objects);
        this.layoutResourceId = resource;
        this.context = context;
        this.data = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        ViewHolder viewHolder;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(layoutResourceId, viewGroup, false);
            viewHolder.itemNo = (TextView) convertView.findViewById(R.id.itemNoTxtView);
            viewHolder.orderDate = (TextView) convertView.findViewById(R.id.orderDateTxtView);
            viewHolder.value = (TextView) convertView.findViewById(R.id.valueTxtView);
            viewHolder.status = (TextView) convertView.findViewById(R.id.statusTxtView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        GRN grn = data.get(position);
        viewHolder.itemNo.setText(grn.getOrdid());
        viewHolder.orderDate.setText(grn.getOrderdt());
        viewHolder.value.setText(grn.getAprxordval());
        viewHolder.status.setText(grn.getOrdstatus());

        return convertView;
    }

    private static class ViewHolder {
        TextView itemNo;
        TextView orderDate;
        TextView value;
        TextView status;
    }
}
