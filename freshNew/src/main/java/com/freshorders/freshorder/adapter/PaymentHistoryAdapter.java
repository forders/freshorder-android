package com.freshorders.freshorder.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.domain.DealerOrderListDomain;
import com.freshorders.freshorder.domain.PaymentHistoryDomain;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by ragul on 29/06/16.
 */
public class PaymentHistoryAdapter extends ArrayAdapter<PaymentHistoryDomain> {
    Context context;
    int resource;
    ArrayList<PaymentHistoryDomain> arrayListPaymentHistoryDomain;

    public PaymentHistoryAdapter(Context context, int resource, ArrayList<PaymentHistoryDomain> arrayListPaymentHistoryDomain) {
        super(context, resource, arrayListPaymentHistoryDomain);
        this.context=context;
        this.resource=resource;
        this.arrayListPaymentHistoryDomain=arrayListPaymentHistoryDomain;
    }

    @Override
    public int getCount(){
        return arrayListPaymentHistoryDomain.size();
    }
    @Override
    public boolean isEnabled(int position){
        return true;
    }

    @Override
    public View getView(int position,View convertView,ViewGroup parent){
        View row = convertView;
        final HomeHolder homeHolder;
        if (row == null) {
            // Log.e("Adapter Inner position",String.valueOf(position));
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(resource, parent, false);
            homeHolder = new HomeHolder();

            homeHolder.textViewName = (TextView) row
                    .findViewById(R.id.textViewName);
            homeHolder.textViewAddress = (TextView) row
                    .findViewById(R.id.textViewAddress);
            homeHolder.textViewDate = (TextView) row
                    .findViewById(R.id.textViewDate);

            row.setTag(homeHolder);
        } else {
            homeHolder = (HomeHolder) row.getTag();
        }
        PaymentHistoryDomain item = arrayListPaymentHistoryDomain
                .get(position);
        homeHolder.textViewName.setText("Reference No"+"  ["+item.getPaymentId()+"]");
        homeHolder.textViewAddress.setText(item.getStatus());
        homeHolder.textViewDate.setText(item.getDate());


        return row;
    }

    private class HomeHolder {
        TextView textViewName, textViewAddress,textViewDate;
    }
}
