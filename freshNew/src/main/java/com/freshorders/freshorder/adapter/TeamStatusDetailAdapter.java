package com.freshorders.freshorder.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.activity.MyTaskActivity;
import com.freshorders.freshorder.activity.TeamStatusActivity;
import com.freshorders.freshorder.model.MyTeamStatusDetailModel;
import com.freshorders.freshorder.model.TeamStatusModel;

import java.util.List;

public class TeamStatusDetailAdapter extends RecyclerView.Adapter<TeamStatusDetailAdapter.ItemViewHolder>{

    private static final String TAG = TeamStatusAdapter.class.getSimpleName();

    private Context mContext;
    private TeamStatusModel currentItems;
    private View rootView;
    private MyTaskActivity.MyBSheet listener;
    private List<MyTeamStatusDetailModel> listItems ;

    public TeamStatusDetailAdapter(Context mContext,
                                    TeamStatusModel currentItems,
                                    List<MyTeamStatusDetailModel> listItems,
                                    View rootView, MyTaskActivity.MyBSheet listener) {
        this.mContext = mContext;
        this.currentItems = currentItems;
        this.rootView = rootView;
        this.listener = listener;
        this.listItems = listItems;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View myInflatedView = inflater.inflate(R.layout.item_team_status, parent, false);
        return new TeamStatusDetailAdapter.ItemViewHolder(myInflatedView);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        final MyTeamStatusDetailModel currentItem = listItems.get(position);
        String strWorkName = currentItem.getWorkName();
        String strWorkCompleted = String.valueOf(currentItem.getWorkCompletionPercentage());//currentItem.getWorkCompletion();
        if(isNumeric(strWorkCompleted)){
            int workComplete = Integer.parseInt(strWorkCompleted);
            setBackgroundColor1(workComplete, holder.progressBar);
            setProgressUpdate(workComplete, holder.progressBar);
        }
        holder.tvName.setText(strWorkName);
        holder.tvWorkStatus.setText((strWorkCompleted + " %"));

        holder.llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, MyTaskActivity.class);
                intent.putExtra("MyTask", currentItem);
                mContext.startActivity(intent);
            }
        });
    }

    private void setProgressUpdate(final int value, final ProgressBar progressBar){

        new Handler().post(new Runnable() {
            public void run() {
                //Drawable progressDrawable = progressBar.getProgressDrawable().mutate();
                //setBackgroundColor(value, progressBar);
                progressBar.setProgress(value);
                progressBar.invalidate();
                Log.e(TAG,"........................progress updated...........");
            }
        });
    }

    private void setBackgroundColor1(int work, final ProgressBar progressBar){

        LayerDrawable layerDrawable = (LayerDrawable) progressBar.getProgressDrawable();
        Drawable progressDrawable = layerDrawable.findDrawableByLayerId(android.R.id.progress);

        if( work <= 25){
            //bg.setBackgroundColor(ContextCompat.getColor(mContext, R.color.red_bellow_25));
            //bg.setTextColor(ContextCompat.getColor(mContext, R.color.red_bellow_25));
            progressDrawable.setColorFilter(ContextCompat.getColor(mContext, R.color.red_bellow_25), android.graphics.PorterDuff.Mode.SRC_IN);
            //progressBar.setProgress(work);
        }else if( work <= 50){
            //bg.setBackgroundColor(ContextCompat.getColor(mContext, R.color.yellow_bellow_50));
            //bg.setTextColor(ContextCompat.getColor(mContext, R.color.yellow_bellow_50));
            progressDrawable.setColorFilter(ContextCompat.getColor(mContext, R.color.yellow_bellow_50), android.graphics.PorterDuff.Mode.SRC_IN);
            //progressBar.setProgress(work);
        } else if(work <= 99){
            //bg.setBackgroundColor(ContextCompat.getColor(mContext, R.color.blue_above_50));
            //bg.setTextColor(ContextCompat.getColor(mContext, R.color.blue_above_50));
            progressDrawable.setColorFilter(ContextCompat.getColor(mContext, R.color.blue_above_50), android.graphics.PorterDuff.Mode.SRC_IN);
            //progressBar.setProgress(work);
        } else if(work >= 100){
            //bg.setBackgroundColor(ContextCompat.getColor(mContext, R.color.green_complete_100));
            //bg.setTextColor(ContextCompat.getColor(mContext, R.color.green_complete_100));
            progressDrawable.setColorFilter(ContextCompat.getColor(mContext, R.color.green_complete_100), android.graphics.PorterDuff.Mode.SRC_IN);
            //progressBar.setProgress(work);
        }

        progressBar.setProgressDrawable(progressDrawable);
    }

    private void setBackgroundColor(int work, TextView bg){

        if( work <= 25){
            bg.setBackgroundColor(ContextCompat.getColor(mContext, R.color.red_bellow_25));
            //bg.setTextColor(ContextCompat.getColor(mContext, R.color.red_bellow_25));
        }else if( work <= 50){
            bg.setBackgroundColor(ContextCompat.getColor(mContext, R.color.yellow_bellow_50));
            //bg.setTextColor(ContextCompat.getColor(mContext, R.color.yellow_bellow_50));
        } else if(work <= 99){
            bg.setBackgroundColor(ContextCompat.getColor(mContext, R.color.blue_above_50));
            //bg.setTextColor(ContextCompat.getColor(mContext, R.color.blue_above_50));
        } else if(work >= 100){
            bg.setBackgroundColor(ContextCompat.getColor(mContext, R.color.green_complete_100));
            //bg.setTextColor(ContextCompat.getColor(mContext, R.color.green_complete_100));
        }
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    private static boolean isNumeric(String strNum) {
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView tvName, tvWorkStatus;
        LinearLayout llItem;
        ProgressBar progressBar;


        ItemViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.id_team_tv_worker_name);
            tvWorkStatus = itemView.findViewById(R.id.id_team_work_status);
            llItem = itemView.findViewById(R.id.id_team_ll_root);
            progressBar = itemView.findViewById(R.id.id_team_work_status_progress);

        }
    }
}
