package com.freshorders.freshorder.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.domain.OutletStockEntryDomain;
import com.freshorders.freshorder.utils.JsonServiceHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class OutleyStockEntryAdapter extends ArrayAdapter<OutletStockEntryDomain> {
	Context context;
	int resource;
	ArrayList<OutletStockEntryDomain> arrayListServiceDomainObjects;

	JSONObject JsonAccountObject = null;
	JSONArray JsonAccountArray = null;
	JsonServiceHandler JsonServiceHandler;


	public OutleyStockEntryAdapter(Context context, int resource,
								   ArrayList<OutletStockEntryDomain> arrayListServiceDomainObjects) {
		super(context, resource, arrayListServiceDomainObjects);
		this.context = context;
		this.resource = resource;
		this.arrayListServiceDomainObjects = arrayListServiceDomainObjects;
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();
	}

	@Override
	public int getCount() {
		return arrayListServiceDomainObjects.size();
	}

	@Override
	public boolean isEnabled(int position) {
		return true;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// Log.e("Adapter position",String.valueOf(position));
		View row = convertView;
		final HomeHolder homeHolder;
		if (row == null) {
			// Log.e("Adapter Inner position",String.valueOf(position));
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(resource, parent, false);
			homeHolder = new HomeHolder();

			homeHolder.textViewName = (TextView) row
					.findViewById(R.id.textViewName);

			homeHolder.textViewSubname = (TextView) row
					.findViewById(R.id.textViewSubname);

			homeHolder.serialno = (TextView) row
					.findViewById(R.id.serialno);

			row.setTag(homeHolder);
		} else {
			homeHolder = (HomeHolder) row.getTag();
		}

		final OutletStockEntryDomain item = arrayListServiceDomainObjects
				.get(position);

		homeHolder.textViewName.setText(item.getprodname() + "  ["
				+ item.getprodcode() + "]");
		homeHolder.textViewSubname.setText(item.getCompanyname() + "  [" + item.getDuserid() + "]");
		homeHolder.serialno.setText(item.getserialno() +".");

		Log.e("adap","adap");
		return row;

	}

	class HomeHolder {

		TextView textViewName, textViewSubname,serialno;
	}

}