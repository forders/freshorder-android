package com.freshorders.freshorder.adapter;

/**
 * Created by ragul on 24/08/16.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ParseException;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.DealerOrderDetailDomain;
import com.freshorders.freshorder.domain.ViewPagerDomain;
import com.freshorders.freshorder.ui.MerchantOrderActivity;
import com.freshorders.freshorder.ui.MerchantSingleDetailActivity;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class ImageAdapter extends BaseAdapter {
    private Context mContext;
    int imageTotal;
    JSONObject JsonAccountObject = null;
    JSONArray JsonAccountArray = null;
    DatabaseHandler databaseHandler;
    JsonServiceHandler JsonServiceHandler;
    //  public static String[] mThumbIds ;

   /* public static String[] mThumbIds = {
            "http://27.250.1.52:3003/api/files/orders/download/1561-99.jpg",
            "http://27.250.1.52:3003/api/files/orders/download/6539-47.jpg",
            "http://27.250.1.52:3003/api/files/orders/download/0744-77.jpg",
            "http://27.250.1.52:3003/api/files/orders/download/1561-99.jpg",
            "http://27.250.1.52:3003/api/files/orders/download/6539-47.jpg",
            "http://27.250.1.52:3003/api/files/orders/download/0744-77.jpg",
            "http://27.250.1.52:3003/api/files/orders/download/0744-77.jpg",
    };*/

    public ImageAdapter(Context c) {
        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();


        mContext = c;

        // myOrders();
    }

    public int getCount() {
        return MerchantOrderActivity.arraylistPdoductImages.size();
    }

    @Override
    public String getItem(int position) {
        return MerchantSingleDetailActivity.mThumbIds[position];
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(400, 400));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(15, 15, 15, 15);
        } else {
            imageView = (ImageView) convertView;
        }
        String url = getItem(position);
        //Log.e("url",url);
        Picasso.with(mContext)
                .load(url)
                .placeholder(
                        R.drawable.loader)
                .fit()
                .centerCrop().into(imageView);
        return imageView;
    }



}