package com.freshorders.freshorder.adapter;

import android.content.Context;
import androidx.annotation.NonNull;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.model.OrderDetail;
import com.freshorders.freshorder.model.Product;

import java.util.ArrayList;

public class GRNOrderAdapter extends ArrayAdapter<OrderDetail> {

    private Context context;
    private int layoutResourceId;
    private ArrayList<OrderDetail> data = null;

    public GRNOrderAdapter(@NonNull Context context, int resource, ArrayList<OrderDetail> objects ) {
        super(context, resource, objects);
        this.layoutResourceId = resource;
        this.context = context;
        this.data = objects;
    }

    public ArrayList<OrderDetail> getUpdatedList(){
        return data;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(layoutResourceId, parent, false);
            viewHolder.prodName = (TextView) convertView.findViewById(R.id.prodNameTxtView);
            viewHolder.qty = (TextView) convertView.findViewById(R.id.qtyTxtView);
            viewHolder.shippedQty = (TextView) convertView.findViewById(R.id.shippedQtyTxtView);
            viewHolder.deliverQty = (EditText) convertView.findViewById(R.id.deliverQtyEdtView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        OrderDetail orderDetail = data.get(position);
        final Product product = orderDetail.getProduct();
        viewHolder.prodName.setText(product.getProdname());
        viewHolder.qty.setText(orderDetail.getQty());
        viewHolder.shippedQty.setText(orderDetail.getShippedqty());
        viewHolder.deliverQty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                data.get(position).setReceivedQty(s.toString());

            }
        });

        return convertView;
    }

    private static class ViewHolder {
        TextView prodName;
        TextView qty;
        TextView shippedQty;
        EditText deliverQty;
    }
}
