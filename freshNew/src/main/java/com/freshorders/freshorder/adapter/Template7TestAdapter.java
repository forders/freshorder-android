package com.freshorders.freshorder.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.model.Template7Model;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Template7TestAdapter extends RecyclerView.Adapter<Template7TestAdapter.ItemViewHolder> implements Filterable {

    private List<Template7Model> testDataList;
    private Context mContext;

    Map<Integer, String> quantityList;

    public Template7TestAdapter(List<Template7Model> testDataList, Context mContext) {
        this.testDataList = testDataList;
        this.mContext = mContext;
        quantityList = new LinkedHashMap<>(testDataList.size());
        valueListReset();
    }

    public void valueListReset(){
        for(int i = 0 ;i < testDataList.size(); i++){
            quantityList.put(i, "");
        }
    }


    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_template7, viewGroup, false);
        return new Template7TestAdapter.ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, int position) {
        Template7Model currentTest = testDataList.get(holder.getAdapterPosition());
        holder.tvTestName.setText(currentTest.getTestName());
        holder.tvSerialNo.setText(String.valueOf(position+1));

        holder.eTxtTestValue.setText(quantityList.get(holder.getAdapterPosition()));

        holder.eTxtTestValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String value = editable.toString();
                quantityList.put(holder.getAdapterPosition(), value);
            }
        });
    }

    @Override
    public int getItemCount() {
        return testDataList.size();
    }

    @Override
    public Filter getFilter() {
        return null;
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{
        TextView tvTestName, tvSerialNo;
        EditText eTxtTestValue;
        public ItemViewHolder(View itemView) {
            super(itemView);
            tvTestName = (TextView) itemView.findViewById(R.id.tvTestName);
            eTxtTestValue = (EditText) itemView.findViewById(R.id.id_temp7_test_value);
            tvSerialNo = (TextView) itemView.findViewById(R.id.temp7_serialno);
        }
    }
}
