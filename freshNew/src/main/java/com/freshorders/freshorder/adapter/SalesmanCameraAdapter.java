package com.freshorders.freshorder.adapter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

import com.freshorders.freshorder.ui.SalesManOrderActivity;

import java.util.ArrayList;

/**
 * Created by dhanapriya on 26/10/16.
 */
public class SalesmanCameraAdapter extends BaseAdapter {

    private Context mContext;
    public ArrayList<String> images;

    public SalesmanCameraAdapter(Context c) {


        mContext = c;
        // this.images = CameraPhotoCapture.arraylistimagepath;
        // myOrders();
    }
    @Override
    public int getCount() {

        return SalesManOrderActivity.arrayListimagebitmap.size();

    }

    @Override
    public Object getItem(int position) {
        return SalesManOrderActivity.arrayListimagebitmap.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        ImageView picturesView;
        if (convertView == null) {
            picturesView = new ImageView(mContext);
            picturesView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            picturesView.setLayoutParams(new Gallery.LayoutParams(200, 250));
            picturesView.setScaleType(ImageView.ScaleType.FIT_XY);
            picturesView.setPadding(10, 10, 10, 10);

        } else {
            picturesView = (ImageView) convertView;
        }
        Log.e("pathadp", String.valueOf(SalesManOrderActivity.arrayListimagebitmap.get(position)));
        picturesView.setImageBitmap(SalesManOrderActivity.arrayListimagebitmap.get(position));
        return picturesView;
    }
}
