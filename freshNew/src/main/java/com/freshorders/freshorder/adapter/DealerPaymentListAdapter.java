package com.freshorders.freshorder.adapter;

import android.app.Activity;
import android.content.Context;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.freshorders.freshorder.R;

import com.freshorders.freshorder.domain.SalesmanListDomain;
import com.freshorders.freshorder.ui.DealerPaymentActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Pavithra on 10-11-2016.
 */
public class DealerPaymentListAdapter extends ArrayAdapter<SalesmanListDomain> {
    Context context;
    int resource;
    ArrayList<SalesmanListDomain> arrayListServiceDomainObjects;

    JSONObject JsonAccountObject = null;
    JSONArray JsonAccountArray = null;
    com.freshorders.freshorder.utils.JsonServiceHandler JsonServiceHandler;

    public DealerPaymentListAdapter(Context context, int resource,
                                  ArrayList<SalesmanListDomain> arrayListServiceDomainObjects) {
        super(context, resource, arrayListServiceDomainObjects);
        this.context = context;
        this.resource = resource;
        this.arrayListServiceDomainObjects = arrayListServiceDomainObjects;
        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();

    }

    @Override
    public int getCount() {
        return arrayListServiceDomainObjects.size();
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        final HomeHolder homeHolder;
        if (row == null) {

            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(resource, parent, false);
            homeHolder = new HomeHolder();

            homeHolder.textViewName = (TextView) row
                    .findViewById(R.id.textViewName);

            homeHolder.checkBoxsalesman = (CheckBox) row
                    .findViewById(R.id.checkBoxsalesman);


            row.setTag(homeHolder);
        } else {
            homeHolder = (HomeHolder) row.getTag();
        }
        final SalesmanListDomain item = arrayListServiceDomainObjects
                .get(position);


        homeHolder.textViewName.setText((item.getfullname()));

        homeHolder.checkBoxsalesman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( homeHolder.checkBoxsalesman.isChecked()){
                    DealerPaymentActivity.arraylistSalesmanId.add(item.getDuserid());
                    Log.e("enabled salesman",item.getDuserid());

                    if(DealerPaymentActivity.arraylistSalesmanId.size()>=1){
                      String amount=  DealerPaymentActivity.paymentAmountInitial;
                        int TotalAmnt = Integer.parseInt(amount)*Integer.parseInt(String.valueOf(DealerPaymentActivity.arraylistSalesmanId.size()));
                        Log.e("TotalAmnt",String.valueOf(TotalAmnt));
                        DealerPaymentActivity.EditTextAmount.setText(String.valueOf(TotalAmnt));
                    }
                }else{
for(int i=0;i<DealerPaymentActivity.arraylistSalesmanId.size();i++){
    if(item.getDuserid().equals(DealerPaymentActivity.arraylistSalesmanId.get(i).toString())){
        DealerPaymentActivity.arraylistSalesmanId.remove(i);
    }
}
                        String amount=  DealerPaymentActivity.paymentAmountInitial;
                        int TotalAmnt = Integer.parseInt(amount)*Integer.parseInt(String.valueOf(DealerPaymentActivity.arraylistSalesmanId.size()));
                        Log.e("TotalAmnt",String.valueOf(TotalAmnt));
                        DealerPaymentActivity.EditTextAmount.setText(String.valueOf(TotalAmnt));

                }
            }
        });


        return row;
    }

    class HomeHolder {

        TextView textViewName;
        CheckBox checkBoxsalesman;
    }

}
