package com.freshorders.freshorder.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.model.ManagerSalesmanAdapterModel;

import java.util.List;

public class ManagerSalesmanAdapter extends RecyclerView.Adapter<ManagerSalesmanAdapter.ItemViewHolder> {

    private static final String TAG = ManagerSalesmanAdapter.class.getSimpleName();

    private Context mContext;
    private List<ManagerSalesmanAdapterModel> listItems;
    private View viewFragment;

    public ManagerSalesmanAdapter(Context mContext,
                                     List<ManagerSalesmanAdapterModel> listItems,
                                     View viewFragment) {
        this.mContext = mContext;
        this.listItems = listItems;
        this.viewFragment = viewFragment;
    }

    @NonNull
    @Override
    public ManagerSalesmanAdapter.ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View myInflatedView = inflater.inflate(R.layout.adapter_manager_salesman, parent,false);  ////////////parent --- Null(can be)
        //RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //myInflatedView.setLayoutParams(lp);
        return new ManagerSalesmanAdapter.ItemViewHolder(myInflatedView);
    }

    @Override
    public void onBindViewHolder(@NonNull ManagerSalesmanAdapter.ItemViewHolder holder, int position) {
        ManagerSalesmanAdapterModel currentItems = listItems.get(position);
        String sNo = String.valueOf((position + 1));
        String merchantName = currentItems.getStorename();
        String inTime = currentItems.getStart_time();
        String outTime = currentItems.getEnd_time();

        holder.tvSno.setText(sNo);
        holder.tvMerchantName.setText(merchantName);
        holder.tvInTime.setText(inTime);
        holder.tvOutTime.setText(outTime);

    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView tvSno, tvMerchantName, tvInTime, tvOutTime;


        ItemViewHolder(View itemView) {
            super(itemView);
            tvSno = itemView.findViewById(R.id.id_manager_salesman_tv_sno);
            tvMerchantName = itemView.findViewById(R.id.id_manager_salesman_tv_merchant_name);
            tvInTime = itemView.findViewById(R.id.id_manager_salesman_tv_in_time);
            tvOutTime = itemView.findViewById(R.id.id_manager_salesman_tv_out_time);

        }
    }
}
