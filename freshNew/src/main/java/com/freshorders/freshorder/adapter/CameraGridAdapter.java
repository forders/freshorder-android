package com.freshorders.freshorder.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.freshorders.freshorder.ui.MerchantOrderActivity;

import java.util.ArrayList;

/**
 * Created by ragul on 02/09/16.
 */
public class CameraGridAdapter extends BaseAdapter {
    private Context mContext;
    public ArrayList<String> images;
    Bitmap bitmap;
    public CameraGridAdapter(Context c) {


        mContext = c;
      //  this.images = CameraPhotoCapture.arraylistimagepath;
        // myOrders();
    }

    @Override
    public int getCount() {
        return MerchantOrderActivity.arrayListimagebitmap.size();
    }

    public Object getItem(int position) {
        return MerchantOrderActivity.arrayListimagebitmap.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView,
                        ViewGroup parent) {
        ImageView picturesView;
        if (convertView == null) {
            picturesView = new ImageView(mContext);
            picturesView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            picturesView
                    .setLayoutParams(new GridView.LayoutParams(400, 400));
            picturesView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            picturesView.setPadding(15, 15, 15, 15);

        } else {
            picturesView = (ImageView) convertView;
        }
Log.e("pathadp", String.valueOf(MerchantOrderActivity.arrayListimagebitmap.get(position)));
        picturesView.setImageBitmap(MerchantOrderActivity.arrayListimagebitmap.get(position));
        /*ByteArrayOutputStream baos = new ByteArrayOutputStream();
        MerchantOrderActivity.arrayListimagebitmap.get(position).compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();

        String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);*/
        return picturesView;
    }
}
