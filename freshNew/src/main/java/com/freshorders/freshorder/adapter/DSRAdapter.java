package com.freshorders.freshorder.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.activity.DailySalesReportActivity;
import com.freshorders.freshorder.model.DSRDetailInputModel;
import com.freshorders.freshorder.model.DSRDetailModel;
import com.freshorders.freshorder.model.DSRFullDetailModel;
import com.freshorders.freshorder.model.DSRFullResponseModel;
import com.freshorders.freshorder.model.DSRModel;
import com.freshorders.freshorder.model.DSRResponseModel;
import com.freshorders.freshorder.popup.PopupDSRDetail;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.CurrencyFormat;
import com.freshorders.freshorder.utils.MobileNet;
import com.freshorders.freshorder.utils.Utils;
import com.freshorders.freshorder.volleylib.APIClient;
import com.freshorders.freshorder.volleylib.APIInterface;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DSRAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = DSRAdapter.class.getSimpleName();

    private static final int FOOTER_VIEW = 1;

    private Context mContext;
    private List<DSRModel> listItems;
    private View rootView;

    private List<DSRDetailModel>  detailModelList;

    private String totalSales;

    private DailySalesReportActivity.MyBSheet listener;

    public DSRAdapter(Context mContext,
                      List<DSRModel> listItems,
                      View rootView,
                      DailySalesReportActivity.MyBSheet listener) {
        this.mContext = mContext;
        this.listItems = listItems;
        this.rootView = rootView;
        detailModelList = new ArrayList<>();
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View myInflatedView;

        if (viewType == FOOTER_VIEW) {
            myInflatedView = inflater.inflate(R.layout.item_dsr_table_footer, parent, false);
            return new DSRAdapter.FooterViewHolder(myInflatedView);
        }

        myInflatedView = inflater.inflate(R.layout.item_dsr_table, parent, false);  ////////////parent --- Null(can be)
        //RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //myInflatedView.setLayoutParams(lp);
        return new DSRAdapter.ItemViewHolder(myInflatedView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        try {

            if (viewHolder instanceof FooterViewHolder) {
                FooterViewHolder holder = (FooterViewHolder) viewHolder;
                if(listItems.size() > 0) {
                    totalSales = listItems.get(listItems.size() - 1).getTotalSales();
                    holder.tvTotalSales.setText(CurrencyFormat.getFormattedAmounts_Rs(totalSales));
                }

            }else if(viewHolder instanceof ItemViewHolder){
                ItemViewHolder holder = (ItemViewHolder) viewHolder;

                DSRModel currentItems = listItems.get(position);
                String sNo = String.valueOf((position + 1));
                final String brName = currentItems.getBranch_salesManName();
                final String sales = CurrencyFormat.getFormattedAmounts(currentItems.getSalesValue());
                final String suserid = currentItems.getSuserid();
                final String selectedDate = currentItems.getSelectedDate();

                holder.tvSno.setText(sNo);
                holder.tvBranchName.setText(brName);
                holder.tvSales.setText(sales);

                holder.llItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        //Getting current Branch Detail
                        if (MobileNet.isNetAvail(mContext)) {
                            try {
                                detailModelList.clear();
                        /*JSONObject jsonObject = new JSONObject();

                        jsonObject.put("suserid", suserid);
                        jsonObject.put("reporttype", "SALES");
                        jsonObject.put("usertype", "S");
                        jsonObject.put("fromdt", selectedDate);
                        jsonObject.put("todt", selectedDate); */

                                DSRDetailInputModel out = new DSRDetailInputModel(suserid, "SALES", "S", selectedDate, selectedDate);

                                getSalesDetail(out, brName, selectedDate,sales);

                            } catch (Exception e) {
                                e.printStackTrace();
                                showBottomSheet("----------" + e.getMessage());
                            }
                        } else {
                            showBottomSheet(mContext.getResources().getString(R.string.no_active_network_avail));
                        }


                    }
                });
            }
        }catch (Exception e){
            e.printStackTrace();
            showBottomSheet("-------" +  e.getMessage());
        }

    }

    private void showPopup(String brName, String date, String totalSales){
        PopupDSRDetail popup = new PopupDSRDetail(rootView,mContext,detailModelList, brName, date, totalSales);
        popup.showPopup();
    }

    @Override
    public int getItemCount() {
        return listItems.size() + 1; /// add one for footer
    }

    @Override
    public int getItemViewType(int position) {
        if (position == listItems.size()) {
            // This is where we'll add footer.
            return FOOTER_VIEW;
        }

        return super.getItemViewType(position);
    }

    public class FooterViewHolder extends RecyclerView.ViewHolder {

        TextView tvTotalSales;

        public FooterViewHolder(View itemView) {
            super(itemView);
            tvTotalSales = itemView.findViewById(R.id.id_dsr_total_sales);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Do whatever you want on clicking the item
                }
            });
        }
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView tvSno, tvBranchName, tvSales;
        LinearLayout llItem;


        ItemViewHolder(View itemView) {
            super(itemView);
            tvSno = itemView.findViewById(R.id.id_dsr_s_no);
            tvBranchName = itemView.findViewById(R.id.id_dsr_br_name);
            tvSales = itemView.findViewById(R.id.id_dsr_sales);
            llItem = itemView.findViewById(R.id.id_dsr_ll_root);

        }
    }


    private void getSalesDetail(DSRDetailInputModel out, final String brName, final String date, final String totalSales) {

        show();

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        final Call<DSRFullDetailModel> call = apiInterface.getDSRFullDetail(out);

        call.enqueue(new Callback<DSRFullDetailModel>() {
            @Override
            public void onResponse(Call<DSRFullDetailModel> call, Response<DSRFullDetailModel> response) {
                Log.e(TAG,"......................response");
                hide();
                try {
                    if (response != null) {
                        if (response.isSuccessful()) {
                            Log.e(TAG, "......................Success");
                            DSRFullDetailModel res = response.body();
                            if (res != null) {
                                String status = res.getStatus();
                                if (status.equalsIgnoreCase("true")) {
                                    List<DSRDetailModel> list = res.getData();
                                    if (list != null && list.size() > 0) {
                                        //DSRDetailModel model = new DSRDetailModel();
                                        detailModelList.addAll(list);
                                        showPopup(brName, date, totalSales);
                                    } else {
                                        showBottomSheet(mContext.getResources().getString(R.string.no_data_avail));
                                    }
                                } else {
                                    showBottomSheet(mContext.getResources().getString(R.string.no_data_avail));
                                }
                            } else {
                                showBottomSheet(mContext.getResources().getString(R.string.no_res_server));
                            }
                        } else {

                            // error case
                            switch (response.code()) {
                                case 404:
                                    //Toast.makeText(ErrorHandlingActivity.this, "not found", Toast.LENGTH_SHORT).show();
                                    showBottomSheet(mContext.getResources().getString(R.string.response_code_404));
                                    break;
                                case 500:
                                    //Toast.makeText(ErrorHandlingActivity.this, "server broken", Toast.LENGTH_SHORT).show();
                                    showBottomSheet(mContext.getResources().getString(R.string.response_code_500));
                                    break;
                                default:
                                    //Toast.makeText(ErrorHandlingActivity.this, "unknown error", Toast.LENGTH_SHORT).show();
                                    showBottomSheet(mContext.getResources().getString(R.string.unknown_error) + "_" + response.code());
                                    break;
                            }

                        }
                    } else {
                        showBottomSheet(mContext.getResources().getString(R.string.no_res_server));
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    showBottomSheet(mContext.getResources().getString(R.string.technical_fault)+e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<DSRFullDetailModel> call, Throwable t) {
                hide();
                Log.e(TAG,"......................Fail");
                Log.e(TAG,"......................Fail" + t.getMessage());
                showBottomSheet("-------" +  t.getMessage());
            }
        });

    }

    private void showBottomSheet(String msg){
        if(listener != null){
            listener.show(msg);
        }
    }

    private void show(){
        if(listener != null){
            listener.show(mContext.getResources().getString(R.string.wait_data_fetching));
        }
    }

    private void hide(){
        listener.hide();
    }

}
