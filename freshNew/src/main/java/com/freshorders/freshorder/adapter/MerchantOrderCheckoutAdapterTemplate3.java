package com.freshorders.freshorder.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.MerchantOrderDomainSelected;
import com.freshorders.freshorder.popup.Template2QuantityEditPopup;
import com.freshorders.freshorder.popup.Template3QuantityEditPopup;
import com.freshorders.freshorder.ui.CreateOrderActivity;
import com.freshorders.freshorder.ui.MerchantOrderCheckoutActivity;
import com.freshorders.freshorder.ui.SalesManOrderCheckoutActivity;
import com.freshorders.freshorder.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MerchantOrderCheckoutAdapterTemplate3 extends ArrayAdapter<MerchantOrderDomainSelected> {
    Context context;
    int resource;
    ArrayList<MerchantOrderDomainSelected> arraylistMerchantOrderDetailList;
    JSONObject JsonAccountObject = null;
    JSONArray JsonAccountArray = null;
    public static MerchantOrderDomainSelected dod;
    com.freshorders.freshorder.utils.JsonServiceHandler JsonServiceHandler;
    String qty,price,freeQty,multipleUomCount ="",freeqtyuom,deliverydate="",deliverytime="",deliverymode="",selecteddeliverymode="",monthLength="",dayLength="",orderuom;
    DatabaseHandler databaseHandler;
    public static EditText editTextSearchField;
    public static TextView textViewHeader, textQtyValidate;
    public static int mYear, mMonth, mDay, mHour, mMinute,mSeconds;
    public HashMap<String, String[]> itemListUOM;
    public MerchantOrderCheckoutAdapterTemplate3(Context context, int resource,
                                                 ArrayList<MerchantOrderDomainSelected> arraylistMerchantOrderDetailList,
                                                 HashMap<String, String[]> itemListUOM) {
        super(context, resource, arraylistMerchantOrderDetailList);

        this.context = context;
        this.resource = resource;
        this.arraylistMerchantOrderDetailList = arraylistMerchantOrderDetailList;
        this.itemListUOM = itemListUOM;
        JsonAccountObject = new JSONObject();
        JsonAccountArray = new JSONArray();
        databaseHandler = new DatabaseHandler(context);
    }

    @Override
    public int getCount() {
        return arraylistMerchantOrderDetailList.size();
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Log.e("Adapter position",String.valueOf(position));
        View row = convertView;
        final MerchantOrderCheckoutAdapterTemplate3.HomeHolder homeHolder;
        if (row == null) {
            // Log.e("Adapter Inner position",String.valueOf(position));
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(resource, parent, false);
            homeHolder = new MerchantOrderCheckoutAdapterTemplate3.HomeHolder();

            homeHolder.textViewName = (TextView) row
                    .findViewById(R.id.textViewName);

            homeHolder.textViewPlus = (TextView) row
                    .findViewById(R.id.textViewPlus);
            homeHolder.serialno = (TextView) row
                    .findViewById(R.id.serialno);
            homeHolder.textViewEdit = (TextView) row.findViewById(R.id.textViewEdit);
            homeHolder.textViewQuantity = (TextView) row.findViewById(R.id.textViewQuantity);
            homeHolder.textViewPrice = (TextView) row.findViewById(R.id.textViewPrice);
            homeHolder.merchantOrderDetailRoot = (LinearLayout) row.findViewById(R.id.id_merchant_order_detail_root);
            homeHolder.tvOrderUOM = (TextView) row.findViewById(R.id.id_template2_uom);

            Typeface font = Typeface.createFromAsset(context.getAssets(),
                    "fontawesome-webfont.ttf");
            homeHolder.textViewPlus.setText(context.getString(R.string.delete_icon));
            homeHolder.textViewPlus.setTypeface(font);
            homeHolder.textViewPlus.setTextColor(Color.parseColor("#03acec"));
            homeHolder.textViewPlus.setVisibility(View.VISIBLE);

            Typeface fontEdit = Typeface.createFromAsset(context.getAssets(),
                    "fontawesome-webfont.ttf");
            homeHolder.textViewEdit.setText(context.getString(R.string.editicon));
            homeHolder.textViewEdit.setTypeface(fontEdit);
            homeHolder.textViewEdit.setTextColor(Color.parseColor("#03acec"));
            homeHolder.textViewEdit.setVisibility(View.VISIBLE);

            row.setTag(homeHolder);
        } else {
            homeHolder = (MerchantOrderCheckoutAdapterTemplate3.HomeHolder) row.getTag();
        }
        final MerchantOrderDomainSelected item = arraylistMerchantOrderDetailList
                .get(position);
        homeHolder.textViewName.setText(item.getprodname()+"  ["+item.getprodcode()+"]");
        homeHolder.serialno.setText((position + 1)+".");
        ///homeHolder.serialno.setText(item.getserialno()+"."); ///hide by vel for wrong serial no
        homeHolder.textViewQuantity.setText(item.getqty());
        homeHolder.textViewPrice.setText(item.getprice());
        homeHolder.tvOrderUOM.setText(item.getorderuom());
        //homeHolder.textViewPlus.setText(item.getindex());

        if(item.getqty().equals("")){
            item.setqty("0");

        }
        if(item.getFreeQty().equals("")){
            item.setFreeQty("0");
        }
		/*if(!item.getUOM().isEmpty()) {
			homeHolder.editTextQuantity.setText("NF:" + "[" + item.getqty() + "]" + "\n" + "F:" + "["+item.getFreeQty()+"]"+" "+item.getUOM());
		}else{
			homeHolder.editTextQuantity.setText("NF:" + "[" + item.getqty() + "]" + "\n" + "F:" + "["+item.getFreeQty()+"]");
		}
*/


        homeHolder.textViewPlus.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Log.e("Clicked", "Clicked");

                deleteEntry(arraylistMerchantOrderDetailList.get(position).getprodid()); /// delete for backward process

                if (Constants.USER_TYPE.equals("M")) {
                    MerchantOrderCheckoutActivity.removeid(arraylistMerchantOrderDetailList.get(position).getprodid());
                } else {
                    SalesManOrderCheckoutActivity.removeid(arraylistMerchantOrderDetailList.get(position).getprodid());
                }

                //arraylistMerchantOrderDetailList.remove(position);
                CreateOrderActivity.arraylistMerchantOrderDetailListSelected.remove(position);

                MerchantOrderDomainSelected  dod = new MerchantOrderDomainSelected();
                for (int i = 0; i < arraylistMerchantOrderDetailList.size(); i++) {
                    //dod.setserialno(String.valueOf(i+1));
                    arraylistMerchantOrderDetailList.get(i).setserialno(String.valueOf(i+1));
                    Log.e("i value", String.valueOf(i + 1));
                }


                if (Constants.USER_TYPE.equals("M")) {
                    Intent io = new Intent(getContext(), MerchantOrderCheckoutActivity.class);
                    getContext().startActivity(io);
                    ((Activity) context).finish();
                } else {
                    Intent io = new Intent(getContext(), SalesManOrderCheckoutActivity.class);
                    getContext().startActivity(io);
                    ((Activity) context).finish();
                }

            }
        });

        homeHolder.textViewEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                View parentRow = (View) view.getParent();
                LinearLayout listViewRoot = (LinearLayout) parentRow.getParent();
                TextView tvPrice = (TextView) listViewRoot.findViewById(R.id.textViewPrice);
                TextView orderQuantity = (TextView) listViewRoot.findViewById(R.id.textViewQuantity);
                String prodid = arraylistMerchantOrderDetailList.get(position).getprodid();
                final Template3QuantityEditPopup popup =
                        new Template3QuantityEditPopup(context,
                                tvPrice.getText().toString().trim(),
                                orderQuantity.getText().toString().trim(),
                                listViewRoot, position, prodid,
                                arraylistMerchantOrderDetailList,
                                itemListUOM,
                                arraylistMerchantOrderDetailList.get(position).getorderuom());

                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        popup.showPopup();
                    }
                }, 50);
            }
        });


        return row;
    }

    private synchronized void  deleteEntry(String prodid){  // to maintain back key pressed event
        /*for(Map.Entry entry: MyApplication.previousSelected.entrySet()){
            MerchantOrderDomainSelected item = (MerchantOrderDomainSelected) entry.getValue();
            int key = (int) entry.getKey();
            if(item.getprodid().equals(prodid)){
                Log.e("deleteEntry",".............itemremoveCalled");
                MyApplication.previousSelected.remove(key);
                break;
            }
        }  */
        MyApplication.previousSelectedForSearch.remove(prodid);
    }


    class HomeHolder {
        LinearLayout merchantOrderDetailRoot;
        TextView textViewName, textViewPlus,serialno,
                textViewEdit, textViewQuantity,
                textViewPrice, tvOrderUOM;
    }

}

