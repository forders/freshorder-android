package com.freshorders.freshorder.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.model.OrderDetail;
import com.freshorders.freshorder.model.Product;

import java.util.ArrayList;

public class OrderAcceptanceDetailAdapter extends ArrayAdapter<OrderDetail> {

    //http://lalit3686.blogspot.com/2012/06/today-i-am-going-to-show-how-to-deal.html

    private Context context;
    private int layoutResourceId;
    private ArrayList<OrderDetail> data = null;

    public OrderAcceptanceDetailAdapter(@NonNull Context context, int resource, ArrayList<OrderDetail> objects) {
        super(context, resource, objects);
        this.layoutResourceId = resource;
        this.context = context;
        this.data = objects;
    }

    public ArrayList<OrderDetail> getUpdatedList(){
        return data;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(layoutResourceId, parent, false);
            viewHolder.item = (TextView) convertView.findViewById(R.id.itemTxtView);
            viewHolder.qty = (TextView) convertView.findViewById(R.id.qtyTxtView);
            viewHolder.value = (TextView) convertView.findViewById(R.id.valTxtView);
            viewHolder.acceptanceBox = (CheckBox) convertView.findViewById(R.id.acceptChxBox);
            viewHolder.acceptanceBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    int getPosition = (Integer) buttonView.getTag();  // Here we get the position that we have set for the checkbox using setTag.
                    data.get(getPosition).setAccepted(buttonView.isChecked()); // Set the value of checkbox to maintain its state.
                }
            });
            convertView.setTag(viewHolder);
            convertView.setTag(R.id.acceptChxBox, viewHolder.acceptanceBox);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.acceptanceBox.setTag(position); // This line is important.
        OrderDetail orderDetail = data.get(position);
        final Product product = orderDetail.getProduct();
        viewHolder.item.setText(product.getProdname());
        viewHolder.qty.setText(orderDetail.getQty());
        viewHolder.value.setText(orderDetail.getPvalue());
        viewHolder.acceptanceBox.setChecked(orderDetail.isAccepted());
        return convertView;
    }

    private static class ViewHolder {
        TextView item;
        TextView qty;
        TextView value;
        CheckBox acceptanceBox;
    }
}
