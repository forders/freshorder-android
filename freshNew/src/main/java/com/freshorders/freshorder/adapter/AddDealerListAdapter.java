package com.freshorders.freshorder.adapter;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.domain.AddDealerListDomain;
import com.freshorders.freshorder.domain.AddDealerListDomain;
import com.freshorders.freshorder.ui.AddDealerActivityNew;
import com.freshorders.freshorder.ui.MyDealersActivity;
import com.freshorders.freshorder.ui.SigninActivity;
import com.freshorders.freshorder.ui.AddDealerActivityNew.IMethodCaller;

import com.freshorders.freshorder.ui.ProductActivity;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class AddDealerListAdapter extends ArrayAdapter<AddDealerListDomain> {
	Context context;
	int resource;
	ArrayList<AddDealerListDomain> arrayListServiceDomainObjects;

	JSONObject JsonAccountObject = null;
	JSONArray JsonAccountArray = null;
	JsonServiceHandler JsonServiceHandler;
	String duserid;

	public AddDealerListAdapter(Context context, int resource,
			ArrayList<AddDealerListDomain> arrayListServiceDomainObjects) {
		super(context, resource, arrayListServiceDomainObjects);
		this.context = context;
		this.resource = resource;
		this.arrayListServiceDomainObjects = arrayListServiceDomainObjects;
		JsonAccountObject = new JSONObject();
		JsonAccountArray = new JSONArray();
	}

	@Override
	public int getCount() {
		return arrayListServiceDomainObjects.size();
	}

	@Override
	public boolean isEnabled(int position) {
		return true;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// Log.e("Adapter position",String.valueOf(position));
		View row = convertView;
		final HomeHolder homeHolder;
		if (row == null) {
			// Log.e("Adapter Inner position",String.valueOf(position));
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(resource, parent, false);
			homeHolder = new HomeHolder();

			homeHolder.textViewName = (TextView) row
					.findViewById(R.id.textViewName);
			homeHolder.textViewAddress = (TextView) row
					.findViewById(R.id.textViewAddress);
			homeHolder.textViewAddIcon = (TextView) row
					.findViewById(R.id.textViewAddIcon);
			homeHolder.textViewAddIcon.setTypeface(AddDealerActivityNew.font);

			homeHolder.liearLayoutAdd= (LinearLayout) row
					.findViewById(R.id.liearLayoutAdd);
		

			row.setTag(homeHolder);
		} else {
			homeHolder = (HomeHolder) row.getTag();
		}
		final AddDealerListDomain item = arrayListServiceDomainObjects
				.get(position);
	
	//	homeHolder.logo.setText(Html.fromHtml(item.getImage()));
		try {
			
			homeHolder.textViewAddIcon.setText(Html.fromHtml("&#xf067;"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		homeHolder.textViewName.setText(item.getComName());
		homeHolder.textViewAddress.setText(item.getAddress());

		homeHolder.liearLayoutAdd.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				duserid=item.getID();
				new AsyncTask<Void, Void, Void>() {
					ProgressDialog dialog;
					String strStatus = "";
					String strMsg = "";

					@Override
					protected void onPreExecute() {
						dialog= ProgressDialog.show(getContext(), "",
								"Loading...", true, true);

					}

					@Override
					protected void onPostExecute(Void result) {

						try {

							strStatus = JsonAccountObject.getString("status");
							Log.e("return status", strStatus);
							strMsg = JsonAccountObject.getString("message");
							Log.e("return message", strMsg);

							if (strStatus.equals("true")) {


								showAlertDialogToast(strMsg);

								//toastDisplay(strMsg);
								Intent io = new Intent(getContext(),
										MyDealersActivity.class);
								io.putExtra("Key","AddDealer");
								getContext().startActivity(io);
								((Activity)context).finish();

							
								 if(context instanceof IMethodCaller){
							            ((IMethodCaller)context).MyDealers();

							        }

							//	AddDealerActivity.MyDealers();
							}else{
								showAlertDialogToast(strMsg);
								//toastDisplay(strMsg);
							
							}
							dialog.dismiss();
						} catch (JSONException e) {
							e.printStackTrace();
						} catch (Exception e) {
							//Log.e("ProductActivityException", e.toString());
						}

					}

					@Override
					protected Void doInBackground(Void... params) {

						JSONObject jsonObject = new JSONObject();
						
						try {
							jsonObject.put("muserid", Constants.USER_ID);
							jsonObject.put("duserid", duserid);//"dstatus":"Active"
							jsonObject.put("dstatus", "Active");
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						JsonServiceHandler = new JsonServiceHandler(Utils.strAddDealers,jsonObject.toString(),getContext());
						JsonAccountObject = JsonServiceHandler.ServiceData();
						return null;
					}
				}.execute(new Void[] {});
				
				
			}
		});
		

		return row;
	}

	public void showAlertDialogToast( String message) {

		AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
		builder1.setCancelable(true);
		builder1.setMessage(message);
		builder1.setPositiveButton("Ok",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});


		AlertDialog alert11 = builder1.create();
		alert11.show();

		Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

	}

	protected void toastDisplay(String msg) {
		Toast toast = Toast.makeText(getContext(), msg,
				Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();

	}
	class HomeHolder {

		TextView textViewName, textViewAddress,textViewDate,textViewAddIcon;
		LinearLayout liearLayoutAdd;
	}

}