package com.freshorders.freshorder.utils;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.work.ListenableWorker;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.model.CommonResponseModel;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.volleylib.APIClient;
import com.freshorders.freshorder.volleylib.APIInterface;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.freshorders.freshorder.service.PrefManager.FCM_TOKEN;
import static com.freshorders.freshorder.utils.Constants.MENU_DISTANCE_CALCULATION_CODE;
import static com.freshorders.freshorder.utils.Utils.strFCMSendURL;

public class SendFCMTokenUtil {

    public interface ITokenUploadListener {
        ListenableWorker.Result response(boolean isSuccess);
    }

    private Context mContext;
    private final static String TAG = SendFCMTokenUtil.class.getSimpleName();
    private DatabaseHandler db;
    public ITokenUploadListener listener;

    public SendFCMTokenUtil(Context mContext, ITokenUploadListener listener) {
        this.mContext = mContext;
        db = new DatabaseHandler(mContext);
        this.listener = listener;
    }

    public void getToken(){

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            listener.response(false);
                            return;
                        }

                        // Get new Instance ID token
                        String token = "";
                        if(task.getResult() != null){
                            token = task.getResult().getToken();
                        }


                        // Log and toast
                        String msg = mContext.getString(R.string.msg_token_fmt, token);
                        Log.d(TAG, msg);
                        Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
                        NotificationUtil.showMyNotification(mContext,R.string.fcm_message, token, 9001);

                        final PrefManager pre = new PrefManager(mContext);
                        pre.setStringDataByKey(FCM_TOKEN, token);

                        sendFCMToken();
                    }
                });
    }


    public void sendFCMToken(){
        final PrefManager pre = new PrefManager(mContext);
        String token = pre.getStringDataByKey(FCM_TOKEN);
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        String userId;
        Cursor cur;
        cur = db.getDetails();
        if(cur != null && cur.getCount() > 0) {
            cur.moveToFirst();
            userId = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_id));
            cur.close();
        }else {
            return;
        }

        if (apiInterface != null) {
            final Call<CommonResponseModel> call = apiInterface.sendFCMToken(strFCMSendURL+userId, token);
            call.enqueue(new Callback<CommonResponseModel>() {
                @Override
                public void onResponse(Call<CommonResponseModel> call, Response<CommonResponseModel> response) {

                    if(response.isSuccessful()) {
                        CommonResponseModel res = response.body();
                        if(res != null) {
                            if(res.isStatus()) {
                                pre.setBooleanDataByKey(PrefManager.IS_DELIVERED_FCM_TOKEN, true);
                                listener.response(true);
                            }
                        }
                    }else {
                        Log.e(TAG,"response failed.........FCM Token Submitting");
                        pre.setBooleanDataByKey(PrefManager.IS_DELIVERED_FCM_TOKEN, false);
                        listener.response(false);
                    }
                }

                @Override
                public void onFailure(Call<CommonResponseModel> call, Throwable t) {
                    pre.setBooleanDataByKey(PrefManager.IS_DELIVERED_FCM_TOKEN, false);
                    listener.response(false);

                }
            });
        }
    }

    private boolean isTrackMenuFound(){

        Cursor menuCur = db.getMenuItems();
        if(menuCur != null && menuCur.getCount() > 0){
            menuCur.moveToFirst();
            for(int i = 0; i < menuCur.getCount(); i++) {
                int currentVisibility = menuCur.getInt(menuCur.getColumnIndex(DatabaseHandler.KEY_MENU_IS_VISIBLE));
                String currentMenuCode = menuCur.getString(menuCur.getColumnIndex(DatabaseHandler.KEY_MENU_CODE));
                if (MENU_DISTANCE_CALCULATION_CODE.equalsIgnoreCase(currentMenuCode)) {
                    if (currentVisibility == 1) {
                        return true;
                    }
                }
                menuCur.moveToNext();
            }
        }
        return false;
    }
}
