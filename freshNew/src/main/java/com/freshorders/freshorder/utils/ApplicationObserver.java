package com.freshorders.freshorder.utils;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

public class ApplicationObserver implements LifecycleObserver {

    private static String TAG = ApplicationObserver.class.getSimpleName();

    public static boolean fromBackground = false;
    public static boolean isBackground = false;

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void onForeground(){
        android.util.Log.i(TAG," App goes to foreground");
        fromBackground = true;
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onBackground(){
        android.util.Log.i(TAG," App goes to background");
        isBackground = true;
    }
}
