package com.freshorders.freshorder.utils;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

/**
 * Created by dhanapriya on 9/11/16.
 */
public class SM_uploadToServerCheckout_NCR extends AsyncTask<Void, Integer, String> {

    long totalSize = 0;
    String  path1="",path2="",path3="",path4="", path5="",path6="",path7="",path8="",path9="",path10="",path11="",path12="",strOrderId;

BroadcastReceiver broadcastreceiver;
    private Context mContext;

    public SM_uploadToServerCheckout_NCR(String path1,String path2,String path3,String path4, String path5,String path6,String path7,String path8, String path9,String path10,String path11,String path12,String strOrderId){
        this.broadcastreceiver=broadcastreceiver;
        this.path1=path1;
        this.path2=path2;
        this.path3=path3;
        this.path4=path4;
        this.path5=path5;
        this.path6=path6;
        this.path7=path7;
        this.path8=path8;
        this.path9=path9;
        this.path10=path10;
        this.path11=path11;
        this.path12=path12;
        this.strOrderId=strOrderId;
        Log.e("orderid", strOrderId);



    }

    public SM_uploadToServerCheckout_NCR (Context context){
        mContext = context;
    }
    @Override
    protected String doInBackground(Void... voids) {
        return uploadFile();
    }

    @SuppressLint("NewApi")

    private String uploadFile() {
        String responseString = null;
        Log.e("uploadFile1", "uploadFile1");
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(Utils.Fileupload+strOrderId);
        Log.e("httppost", Utils.Fileupload + strOrderId);

        try {
            AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                    new AndroidMultiPartEntity.ProgressListener() {

                        @Override
                        public void transferred(long num) {
                            publishProgress((int) ((num / (float) totalSize) * 100));
                        }
                    });

            if(!path1.isEmpty()){
                if(path1.length()>0){
                    File sourceFile1 = new File(path1);
                    entity.addPart("file[0]", new FileBody(sourceFile1));
                }
            }
            if(!path2.isEmpty()){
                if(!path2.isEmpty()){
                    if(path2.length()>0){
                        File sourceFile2 = new File(path2);
                        entity.addPart("file[1]", new FileBody(sourceFile2));}
                }
            }
            if(!path3.isEmpty()){
                if(path3.length()>0){
                    File sourceFile3 = new File(path3);
                    entity.addPart("file[2]", new FileBody(sourceFile3));}}
            if(!path4.isEmpty()){
                if(path4.length()>0){
                    File sourceFile4 = new File(path4);
                    entity.addPart("file[3]", new FileBody(sourceFile4));}}
            if(!path5.isEmpty()){
                if(path5.length()>0){
                    File sourceFile5 = new File(path5);
                    entity.addPart("file[4]", new FileBody(sourceFile5));}}
            if(!path6.isEmpty()){
                if(path6.length()>0){
                    File sourceFile6 = new File(path6);
                    entity.addPart("file[5]", new FileBody(sourceFile6));}}
            if(!path7.isEmpty()){
                if(path7.length()>0){
                    File sourceFile7 = new File(path7);
                    entity.addPart("file[6]", new FileBody(sourceFile7));}}
            if(!path8.isEmpty()){
                if(path8.length()>0){
                    File sourceFile8 = new File(path8);
                    entity.addPart("file[7]", new FileBody(sourceFile8));}}
            if(!path9.isEmpty()){
                if(path9.length()>0){
                    File sourceFile9 = new File(path9);
                    entity.addPart("file[8]", new FileBody(sourceFile9));}}
            if(!path10.isEmpty()){
                if(path10.length()>0){
                    File sourceFile10 = new File(path10);
                    entity.addPart("file[9]", new FileBody(sourceFile10));}}
            if(!path11.isEmpty()){
                if(path11.length()>0){
                    File sourceFile11 = new File(path11);
                    entity.addPart("file[10]", new FileBody(sourceFile11));}}
            if(!path12.isEmpty()){
                if(path12.length()>0){
                    File sourceFile12 = new File(path12);
                    entity.addPart("file[11]", new FileBody(sourceFile12));}}
            // Adding file data to http body
            //entity.addPart("image", new FileBody(sourceFile));
            //entity.addPart("image1", new FileBody(sourceFile1));
            //entity.addPart("image2", new FileBody(sourceFile2));
            // Extra parameters if you want to pass to server

            Charset chars = Charset.forName("UTF-8");

            Log.e("uploadFile2", "uploadFile2");
            //entity.addPart("orderid", new StringBody("1296"));



            totalSize = entity.getContentLength();
            httppost.setEntity(entity);

            // Making server call
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity r_entity = response.getEntity();

            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                // Server response
                Log.e("uploadFile3", "uploadFile3");
                responseString = EntityUtils.toString(r_entity);
NetworkChangeReceiver.arraylistimagepath.clear();
            } else {
                responseString = "Error occurred! Http Status Code: "
                        + statusCode;
                NetworkChangeReceiver.arraylistimagepath.clear();
            }




        } catch (ClientProtocolException e) {
            responseString = e.toString();
        } catch (IOException e) {
            responseString = e.toString();
        }

        return responseString;

    }


    @Override
    protected void onPostExecute(String result) {
        Log.e("uploadclass", "Response from server: " + result);
        int duration = Toast.LENGTH_SHORT;

       /* Toast toast = Toast.makeText(mContext, result, Toast.LENGTH_SHORT);
        toast.show()*/
        super.onPostExecute(result);
    }


}
