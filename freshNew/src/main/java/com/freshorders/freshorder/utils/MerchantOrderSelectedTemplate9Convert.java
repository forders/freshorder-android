package com.freshorders.freshorder.utils;

import android.content.Context;
import android.util.Log;

import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.MerchantOrderDetailDomain;
import com.freshorders.freshorder.domain.MerchantOrderDomainSelected;
import com.freshorders.freshorder.model.Template3Model;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.freshorders.freshorder.MyApplication.previousSelected;
import static com.freshorders.freshorder.MyApplication.previousSelectedForSearch;
import static com.freshorders.freshorder.MyApplication.productIdPositionMap;

public class MerchantOrderSelectedTemplate9Convert {
    private Context mContext;
    private Map<Integer, String> changedUOM;
    private boolean isTemplate9 = false;
    private DatabaseHandler databaseHandler;

    public MerchantOrderSelectedTemplate9Convert(Context mContext) {
        this.mContext = mContext;
    }

    public MerchantOrderSelectedTemplate9Convert(Context mContext, Map<Integer, String> changedUOM, boolean isTemplate9) {
        this.mContext = mContext;
        this.isTemplate9 = isTemplate9;
        this.changedUOM = changedUOM;
        databaseHandler = new DatabaseHandler(mContext);
    }

    public ArrayList<MerchantOrderDomainSelected> getMerchantSelectedFromTemplate9(Map<Integer, Template3Model> selectedOrders,
                                                                                   ArrayList<MerchantOrderDetailDomain> availItems){
        ArrayList<MerchantOrderDomainSelected> out = new ArrayList<>();
        Map<String, Integer> productIdPositionLocal = new LinkedHashMap<>();

        android.util.Log.e("PreviousCount","................................" + previousSelectedForSearch.size());
        int index = 0;
        for(Map.Entry entry: previousSelectedForSearch.entrySet()){
            String productId = (String) entry.getKey();
            MerchantOrderDomainSelected currentSelected = (MerchantOrderDomainSelected) entry.getValue();
            android.util.Log.e("productId","................................////////////////..........." + productId);
            int position = productIdPositionMap.get(productId);
            out.add(currentSelected);
            previousSelected.put(position,currentSelected); ////////// this is used for back arrow selection
            productIdPositionLocal.put(productId, index);
            index++;
        }


        android.util.Log.e("SelectedCount","................................" + selectedOrders.size());
        for(Map.Entry entry: selectedOrders.entrySet()){

            MerchantOrderDomainSelected currentSelected = new MerchantOrderDomainSelected();
            int position = (int) entry.getKey();
            android.util.Log.e("Convert",".........................................."+ position);
            Template3Model template3Model = (Template3Model) entry.getValue();
            android.util.Log.e("template2Model","....................................price."+ template3Model.getItemPrice() + "  qty " + template3Model.getOrderQuantity());
            MerchantOrderDetailDomain currentAvailItem = availItems.get(position);
            String quantity = template3Model.getOrderQuantity();
            ///////////String price =  template3Model.getItemPrice();
            int productValue = 0;
            if(!quantity.isEmpty() ){
                productValue = (int) (Float.parseFloat(template3Model.getOrderQuantity()) * Float.parseFloat(currentAvailItem.getprodprice()));
            }

            String productId = currentAvailItem.getprodid();

            android.util.Log.e("Price","..............price:" + template3Model.getItemPrice() + " qty: " +template3Model.getOrderQuantity());

            currentSelected.setqty(template3Model.getOrderQuantity());
            currentSelected.setprice(currentAvailItem.getprodprice());//////////////////////////////
            currentSelected.setOutstock(currentAvailItem.getCurrStock());
            currentSelected.setCurrStock(currentAvailItem.getCurrStock()); //////////////////  saravan and nambi said crrent and out stock is same
            currentSelected.setDate("");//////////////////////////////////////
            currentSelected.setusrdlrid(currentAvailItem.getusrdlrid());
            currentSelected.setprodid(currentAvailItem.getprodid());
            currentSelected.setprodcode(currentAvailItem.getprodcode());
            currentSelected.setprodname(currentAvailItem.getprodname());
            currentSelected.setUserid(currentAvailItem.getDuserid()); //// saravanan told to set this id (because in web order not showing)
            currentSelected.setCompanyname(currentAvailItem.getCompanyname());
            currentSelected.setFreeQty(currentAvailItem.getFreeQty());
            currentSelected.setserialno(currentAvailItem.getserialno());
            currentSelected.setindex(currentAvailItem.getindex());
            currentSelected.setptax(currentAvailItem.getprodtax());
            currentSelected.setpvalue(productValue);
            currentSelected.setpaymentType(currentAvailItem.getPaymentType());
            currentSelected.setpaymentType(currentAvailItem.getPaymentType());
            currentSelected.setBatchno(currentAvailItem.getBatchno());
            currentSelected.setExpdate(currentAvailItem.getExpdate());
            currentSelected.setMgfDate(currentAvailItem.getMgfDate());
            currentSelected.setUOM(currentAvailItem.getUOM());
            currentSelected.setorderfreeuom(""); ////////

            if(changedUOM.containsKey(position)) {
                currentSelected.setorderuom(changedUOM.get(position));
            }else {
                currentSelected.setorderuom(currentAvailItem.getSlectedUOM());
            }
            currentSelected.setselectedfreeUOM(currentAvailItem.getselectedfreeUOM());
            currentSelected.setUnitGram(currentAvailItem.getUnitGram());
            currentSelected.setUnitPerUom(currentAvailItem.getUnitPerUom());
            currentSelected.setSelecetdQtyUomUnit(currentAvailItem.getSelectedQtyUomUnit());
            currentSelected.setSelecetdFreeQtyUomUnit(currentAvailItem.getSelectedFreeQtyUomUnit());
            currentSelected.setDeliverymode(currentAvailItem.getDeliverymode());
            /////currentSelected.setDeliverydate(currentAvailItem.getDeliverydate());
            currentSelected.setDeliverydate("");
            currentSelected.setDeliverytime(currentAvailItem.getDeliverytime());

            int newPosition = productIdPositionMap.get(productId);
            if(previousSelectedForSearch.get(productId) != null){
                out.set(productIdPositionLocal.get(productId),currentSelected);
                previousSelected.put(newPosition, currentSelected); ////////// this is used for back arrow selection
            }else {
                out.add(currentSelected);
                previousSelected.put(newPosition, currentSelected); ////////// this is used for back arrow selection
                previousSelectedForSearch.put(productId, currentSelected);////////////
            }
        }
        return out;
    }

    public ArrayList<MerchantOrderDomainSelected> getMerchantSelectedFromTemplate9ForSearch(Map<Integer, Template3Model> selectedOrders,
                                                                                            ArrayList<MerchantOrderDetailDomain> availItems,
                                                                                            ArrayList<MerchantOrderDetailDomain> totalLoadedList){
        ArrayList<MerchantOrderDomainSelected> out = new ArrayList<>();
        for(Map.Entry entry: selectedOrders.entrySet()){
            MerchantOrderDomainSelected currentSelected = new MerchantOrderDomainSelected();
            int position = (int) entry.getKey();
            Template3Model template3Model = (Template3Model) entry.getValue();
            String pId = template3Model.getProductId();
            android.util.Log.e("CurrentProductId","............................................."+ pId);
            int originalPosition = productIdPositionMap.get(pId);
            MerchantOrderDetailDomain currentAvailItem = totalLoadedList.get(originalPosition); ///////////////here see difference
            String quantity = template3Model.getOrderQuantity();
            /////////////String price =  template3Model.getItemPrice();

            String productId = currentAvailItem.getprodid();

            int productValue = 0;
            if(!quantity.isEmpty() ){
                productValue = (int) (Float.parseFloat(template3Model.getOrderQuantity()) * Float.parseFloat(currentAvailItem.getprodprice()));
            }

            Log.e("Price","..............price:" +
                    template3Model.getItemPrice() + " qty: " +template3Model.getOrderQuantity() +
                    "...originalPosition.."+ originalPosition);

            currentSelected.setqty(template3Model.getOrderQuantity());
            currentSelected.setprice(currentAvailItem.getprodprice());//////////////////////
            currentSelected.setOutstock(currentAvailItem.getCurrStock());
            currentSelected.setCurrStock(currentAvailItem.getCurrStock()); //////////////////  saravan and nambi said crrent and out stock is same
            currentSelected.setDate("");//////////////////////////////////////
            currentSelected.setusrdlrid(currentAvailItem.getusrdlrid());
            currentSelected.setprodid(currentAvailItem.getprodid());
            currentSelected.setprodcode(currentAvailItem.getprodcode());
            currentSelected.setprodname(currentAvailItem.getprodname());
            currentSelected.setUserid(currentAvailItem.getDuserid()); //// saravanan told to set this id (because in web order not showing)
            currentSelected.setCompanyname(currentAvailItem.getCompanyname());
            currentSelected.setFreeQty(currentAvailItem.getFreeQty());
            currentSelected.setserialno(currentAvailItem.getserialno());
            currentSelected.setindex(currentAvailItem.getindex());
            currentSelected.setptax(currentAvailItem.getprodtax());
            currentSelected.setpvalue(productValue);
            currentSelected.setpaymentType(currentAvailItem.getPaymentType());
            currentSelected.setpaymentType(currentAvailItem.getPaymentType());
            currentSelected.setBatchno(currentAvailItem.getBatchno());
            currentSelected.setExpdate(currentAvailItem.getExpdate());
            currentSelected.setMgfDate(currentAvailItem.getMgfDate());
            currentSelected.setUOM(currentAvailItem.getUOM());
            currentSelected.setorderfreeuom(""); ////////

            if(changedUOM.containsKey(position)) {
                currentSelected.setorderuom(changedUOM.get(position));
            }else {
                currentSelected.setorderuom(currentAvailItem.getSlectedUOM());
            }
            currentSelected.setselectedfreeUOM(currentAvailItem.getselectedfreeUOM());
            currentSelected.setUnitGram(currentAvailItem.getUnitGram());
            currentSelected.setUnitPerUom(currentAvailItem.getUnitPerUom());
            currentSelected.setSelecetdQtyUomUnit(currentAvailItem.getSelectedQtyUomUnit());
            currentSelected.setSelecetdFreeQtyUomUnit(currentAvailItem.getSelectedFreeQtyUomUnit());
            currentSelected.setDeliverymode(currentAvailItem.getDeliverymode());
            /////currentSelected.setDeliverydate(currentAvailItem.getDeliverydate());
            currentSelected.setDeliverydate("");
            currentSelected.setDeliverytime(currentAvailItem.getDeliverytime());

            out.add(currentSelected);
            previousSelectedForSearch.put(productId,currentSelected); ////////// this is used for search Operation
        }
        //assign current list for search operation
        //previousLoadedItems.clear();
        //previousLoadedItems.addAll(availItems);
        return out;


    }


}

