package com.freshorders.freshorder.utils;

import android.content.Context;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.crashreport.AppCrashUtil;
import com.freshorders.freshorder.service.PrefManager;

public class Log {

    private static boolean ENABLE_DEBUG_LOG = true;

    private static String migPath = "";
    private static String bugPath = "";

    private Context mContext;

    public Log() {
    }

    public Log(Context mContext) {
        this.mContext = mContext;
        bugPath = new PrefManager(mContext).getStringDataByKey(PrefManager.KEY_BUG_FILE_PATH);
    }

    public Log(String bugPaths) {
        bugPath = bugPaths;
    }


    public static void i(String tag, String msg) {
        android.util.Log.i(tag, msg);
    }

    public static void e(String tag, String msg) {
        if (ENABLE_DEBUG_LOG) {
            android.util.Log.e(tag, msg);
            AppCrashUtil.writeStringToBugFile(bugPath, tag + "     :::" + msg);////////////////
        }

    }

    public synchronized void ee(String tag, String msg) {
        if (ENABLE_DEBUG_LOG) {
            android.util.Log.e(tag, msg);
            AppCrashUtil.writeStringToBugFile(bugPath, tag + "     :::" + msg);////////////////
        }

    }

    public static synchronized void p(String path, String tag, String msg) {
        if (ENABLE_DEBUG_LOG) {
            android.util.Log.e(tag, msg);
            AppCrashUtil.writeStringToBugFile(path, tag + "     :::" + msg);////////////////
        }

    }

    public static void e(String tag, String msg, Throwable e) {
        if (ENABLE_DEBUG_LOG) {
            android.util.Log.e(tag, msg, e);
            //AppCrashUtil.writeStringToBugFile(migPath, tag + "     :::" + msg);////////////////
        }

    }

    public static void d(String tag, String msg) {
        if (ENABLE_DEBUG_LOG) {
            android.util.Log.d(tag, msg);
        }

    }

    public static void d(String tag, String msg, Throwable e) {
        if (ENABLE_DEBUG_LOG) {
            android.util.Log.d(tag, msg, e);
        }

    }

    public static void v(String tag, String msg) {
        if (ENABLE_DEBUG_LOG) {
            android.util.Log.v(tag, msg);
        }

    }

    public static void w(String tag, String msg) {
        if (ENABLE_DEBUG_LOG) {
            android.util.Log.w(tag, msg);
        }

    }

    public static void w(String tag, String msg, Throwable e) {
        if (ENABLE_DEBUG_LOG) {
            android.util.Log.w(tag, msg, e);
        }

    }

    public static void setEnableDebugLog(boolean islogenabled) {
        ENABLE_DEBUG_LOG = islogenabled;
    }

    public static String getMigPath() {
        return migPath;
    }

    public static void setMigPath(String migPath) {
        Log.migPath = migPath;
    }
}

