package com.freshorders.freshorder.utils;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;


public class JsonServiceHandler 
{
	String URL = null;
    static InputStream is = null;
    static JSONArray jary=null;
    static JSONObject Jobj=null;
	String json = "";
    String parameter="";
	//Kumaravel 11/01/2019
	public JsonServiceHandler(Context context) {
		this.context = context;
	}

	Context context;
    
    public JsonServiceHandler(String url,Context context)
    {
        this.URL = url;
        this.context=context;
    }

    public JsonServiceHandler(String url, String parameter,Context context)
    {
        this.URL = url;
        this.parameter=parameter;
        this.context=context;
        
    }

	public String getJson() {  /// this for response string
		return json;
	}

    // Kumaravel Add For Migration
	public void setParams(String parameter){
		this.parameter = parameter;
	}
	// Kumaravel Add For Daily Order List Showing
	public  void setURL(String url){
		this.URL = url;
	}

    public JSONObject ServiceData ()
	{
    	is = null;
        jary=new JSONArray();
        Jobj=new JSONObject();
        json = "";
        try
		{

			// newly added by Kumaravel may be remove
			HttpParams httpParams = new BasicHttpParams();
			ConnManagerParams.setTimeout(httpParams, 10000);
			HttpConnectionParams.setConnectionTimeout(httpParams,50000);
			HttpConnectionParams.setSoTimeout(httpParams, 50000);

        		Log.e("param service handler",parameter);
        		Log.e("URL service handler",URL);
                DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);/// here empty constructor
                HttpPost httpPost = new HttpPost(URL);
                StringEntity input = new StringEntity(parameter);

               
                httpPost.setHeader("Content-Type","application/json");
                httpPost.setEntity(input);
                HttpResponse httpResponse = httpClient.execute(httpPost);
                Log.e("httpResponse","res"+httpResponse);
                int responseCode=httpResponse.getStatusLine().getStatusCode();
				
				Log.e("asdss", String.valueOf(httpResponse.getStatusLine().getStatusCode()) +"URL : " + URL);
				if(responseCode==200){
					HttpEntity httpEntity = httpResponse.getEntity();
	                is = httpEntity.getContent();
	                Log.e("inputStream", is.toString());
	                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
	                Log.e("reader", reader.toString());
	                StringBuilder sb = new StringBuilder();
	                String line = null;
	                while ((line = reader.readLine()) != null)
	                {
	                    sb.append(line + "\n");
	                }
	                is.close();
	                json = sb.toString();
	                Log.e("json",json);
	                System.out.println("----"+URL+"JSON--->"+json);
	                 //jary=new JSONArray(json);
	                 Jobj=new JSONObject(json);
					
				}else {
					if(responseCode==404){
						((Activity) context).runOnUiThread(new Runnable(){

							@Override
							public void run() {
								Toast toast = Toast.makeText(context,
										"Requested resource not found",
										Toast.LENGTH_SHORT);
								toast.setGravity(Gravity.CENTER, 0, 0);
								toast.show();
								
							}
						});
								
					}else if(responseCode==500){
						((Activity) context).runOnUiThread(new Runnable(){

							@Override
							public void run() {
							Toast toast = Toast.makeText(context,
									"Something went wrong at server end",
									Toast.LENGTH_SHORT);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();
}
							
						});
					}else {
						((Activity) context).runOnUiThread(new Runnable(){

							@Override
							public void run() {
					
								Toast toast = Toast.makeText(context,
										"Unexpected Error occcured! [Most common Error: Device might not be connected to Internet]",
										Toast.LENGTH_SHORT);
								toast.setGravity(Gravity.CENTER, 0, 0);
								toast.show();
						}
						
					});
					}
				}
                

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
			Log.e("JSON Service Handler", "Error parsing data " + e.toString());
			//Toast.makeText(SyncDetail.context, "Data Not Inserted", Toast.LENGTH_SHORT ).show();
		}catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        return Jobj;
        //return jary;
    }

    public JSONObject ServiceDataGet ()
    {
    	is = null;
        jary=new JSONArray();
        Jobj=new JSONObject();
        json = "";
        try
        {
        		Log.e("param handler set",parameter);
        		Log.e("URL service handler get",URL);
                DefaultHttpClient httpClient = new DefaultHttpClient();
                
                HttpGet httpget = new HttpGet(URL);
           //   StringEntity input = new StringEntity(parameter);
               
            // httpget.setEntity(input);
                Log.e("URL service handler get",URL);
                HttpResponse httpResponse = httpClient.execute(httpget);
                Log.e("httpResponse","res"+httpResponse);
                int responseCode=httpResponse.getStatusLine().getStatusCode();

				
				Log.e("asdss", String.valueOf(httpResponse.getStatusLine().getStatusCode()));
				if(responseCode==200){
					HttpEntity httpEntity = httpResponse.getEntity();
	                is = httpEntity.getContent();
	                Log.e("inputStream", is.toString());
	                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
	                Log.e("reader", reader.toString());
	                StringBuilder sb = new StringBuilder();
	                String line = null;
	                while ((line = reader.readLine()) != null)
	                {
	                    sb.append(line + "\n");
	                }
	                is.close();
	                json = sb.toString();
	                Log.e("json",json);
	                System.out.println("----"+URL+"JSON--->"+json);
	                 //jary=new JSONArray(json);
	                 Jobj=new JSONObject(json);
					
				}else {
					if(responseCode==404){
						((Activity) context).runOnUiThread(new Runnable(){

							@Override
							public void run() {
								Toast toast = Toast.makeText(context,
										"Requested resource not found",
										Toast.LENGTH_SHORT);
								toast.setGravity(Gravity.CENTER, 0, 0);
								toast.show();
								
							}
						});
								
					}else if(responseCode==500){
						((Activity) context).runOnUiThread(new Runnable(){

							@Override
							public void run() {
							Toast toast = Toast.makeText(context,
									"Something went wrong at server end",
									Toast.LENGTH_SHORT);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();
}
							
						});
					}else {
						((Activity) context).runOnUiThread(new Runnable(){

							@Override
							public void run() {
					
								Toast toast = Toast.makeText(context,
										"Unexpected Error occcured! [Most common Error: Device might not be connected to Internet]",
										Toast.LENGTH_SHORT);
								toast.setGravity(Gravity.CENTER, 0, 0);
								toast.show();
						}
						
					});
					}
				}
                

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            Log.e("JSON Service Handler", "Error parsing data " + e.toString());
            //Toast.makeText(SyncDetail.context, "Data Not Inserted", Toast.LENGTH_SHORT ).show();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        return Jobj;
        //return jary;
    }


	public JSONObject ServiceDataModifiedGet ()
	{
		is = null;
		jary=new JSONArray();
		Jobj=new JSONObject();
		json = "";
		try
		{
			// newly added by Kumaravel may be remove
			HttpParams httpParams = new BasicHttpParams();
			ConnManagerParams.setTimeout(httpParams, 10000);
			HttpConnectionParams.setConnectionTimeout(httpParams,50000);
			HttpConnectionParams.setSoTimeout(httpParams, 50000);

			Log.e("param handler set",parameter);
			Log.e("URL service handler get",URL);

			//String paramString= URLEncodedUtils.format(parameter,"utf-8");
			String paramString = URLEncoder.encode(parameter, "UTF-8");
			URL = URL + paramString;
			DefaultHttpClient httpClient = new DefaultHttpClient(httpParams);
			HttpGet httpget = new HttpGet(URL);

			//httpget.setHeader("Content-Type","application/json");



			//   StringEntity input = new StringEntity(parameter);

			// httpget.setEntity(input);
			Log.e("URL service handler get",URL);
			HttpResponse httpResponse = httpClient.execute(httpget);
			Log.e("httpResponse","res"+httpResponse);
			int responseCode=httpResponse.getStatusLine().getStatusCode();


			Log.e("asdss", String.valueOf(httpResponse.getStatusLine().getStatusCode()));
			if(responseCode==200){
				HttpEntity httpEntity = httpResponse.getEntity();
				is = httpEntity.getContent();
				Log.e("inputStream", is.toString());
				BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
				Log.e("reader", reader.toString());
				StringBuilder sb = new StringBuilder();
				String line = null;
				while ((line = reader.readLine()) != null)
				{
					sb.append(line + "\n");
				}
				is.close();
				json = sb.toString();
				Log.e("json",json);
				System.out.println("----"+URL+"JSON--->"+json);
				//jary=new JSONArray(json);
				Jobj=new JSONObject(json);

			}else {
				if(responseCode==404){
					((Activity) context).runOnUiThread(new Runnable(){

						@Override
						public void run() {
							Toast toast = Toast.makeText(context,
									"Requested resource not found",
									Toast.LENGTH_SHORT);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();

						}
					});

				}else if(responseCode==500){
					((Activity) context).runOnUiThread(new Runnable(){

						@Override
						public void run() {
							Toast toast = Toast.makeText(context,
									"Something went wrong at server end",
									Toast.LENGTH_SHORT);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();
						}

					});
				}else {
					((Activity) context).runOnUiThread(new Runnable(){

						@Override
						public void run() {

							Toast toast = Toast.makeText(context,
									"Unexpected Error occcured! [Most common Error: Device might not be connected to Internet]",
									Toast.LENGTH_SHORT);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();
						}

					});
				}
			}


		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			Log.e("JSON Service Handler", "Error parsing data " + e.toString());
			//Toast.makeText(SyncDetail.context, "Data Not Inserted", Toast.LENGTH_SHORT ).show();
		} catch (Exception e) {
			Log.e("Buffer Error", "Error converting result " + e.toString());
		}
		return Jobj;
		//return jary;
	}
}