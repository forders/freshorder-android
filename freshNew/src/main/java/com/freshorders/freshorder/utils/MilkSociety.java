package com.freshorders.freshorder.utils;

import android.content.ContentValues;
import android.os.AsyncTask;
import android.util.Log;

import com.freshorders.freshorder.db.DatabaseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.freshorders.freshorder.db.DatabaseHandler.KEY_CREATED_BY;
import static com.freshorders.freshorder.db.DatabaseHandler.KEY_CREATED_DATE;
import static com.freshorders.freshorder.db.DatabaseHandler.KEY_FAT;
import static com.freshorders.freshorder.db.DatabaseHandler.KEY_FAT_SNF_COLUMN_COUNT;
import static com.freshorders.freshorder.db.DatabaseHandler.KEY_FAT_SNF_RATE_ID;
import static com.freshorders.freshorder.db.DatabaseHandler.KEY_IS_FAT_SNF_TBL_LOADED;
import static com.freshorders.freshorder.db.DatabaseHandler.KEY_IS_SNF_CAL_TBL_LOADED;
import static com.freshorders.freshorder.db.DatabaseHandler.KEY_LAST_UPDATED_BY;
import static com.freshorders.freshorder.db.DatabaseHandler.KEY_LAST_UPDATED_DATE;
import static com.freshorders.freshorder.db.DatabaseHandler.KEY_MILK_CHECK_ID;
import static com.freshorders.freshorder.db.DatabaseHandler.KEY_RATE;
import static com.freshorders.freshorder.db.DatabaseHandler.KEY_SNF_CALC__ID;
import static com.freshorders.freshorder.db.DatabaseHandler.KEY_SNF_CAL_COLUMN_COUNT;
import static com.freshorders.freshorder.db.DatabaseHandler.KEY_SNF_CAL_CREATED_BY;
import static com.freshorders.freshorder.db.DatabaseHandler.KEY_SNF_CAL_CREATED_DATE;
import static com.freshorders.freshorder.db.DatabaseHandler.KEY_SNF_CAL_IR_VALUE;
import static com.freshorders.freshorder.db.DatabaseHandler.KEY_SNF_CAL_LAST_UPDATED_BY;
import static com.freshorders.freshorder.db.DatabaseHandler.KEY_SNF_CAL_LAST_UPDATED_DATE;
import static com.freshorders.freshorder.db.DatabaseHandler.KEY_SNF_CAL_SNF_VALUE;
import static com.freshorders.freshorder.db.DatabaseHandler.KEY_SNF_CAL_STATUS;
import static com.freshorders.freshorder.db.DatabaseHandler.KEY_SNF_CAL_TEMPERATURE;
import static com.freshorders.freshorder.db.DatabaseHandler.KEY_SNF_CAL_USR_ID;
import static com.freshorders.freshorder.db.DatabaseHandler.KEY_SNF_VALUE;
import static com.freshorders.freshorder.db.DatabaseHandler.KEY_STATUS;
import static com.freshorders.freshorder.db.DatabaseHandler.KEY_USR_ID;
import static com.freshorders.freshorder.utils.Constants.MILK_CHECK_TBL_PRIMARY_KEY_VALUE;

public class MilkSociety {


    public interface SNFCalMigrationListener{
        void snfCalculationLoadFinished(boolean result, String statusMSG, Long inputLength, Long storedLength);
    }
    public interface MilkTableFetchListener{
        void milkRateFetchFinished(boolean result, String statusMSG, Long inputLength, Long storedLength);
    }


    public static class MilkRateTableFetchAsyncTask extends AsyncTask<Void, Void,JSONObject> {

        private JsonServiceHandler fatSnfTblService;
        private MilkSociety.MilkTableFetchListener listener;
        private DatabaseHandler databaseHandler;
        private boolean isFirstTime;

        public MilkRateTableFetchAsyncTask(boolean isFirstTime, DatabaseHandler databaseHandler, JsonServiceHandler fatSnfTblService,
                                           MilkTableFetchListener listener) {
            this.fatSnfTblService = fatSnfTblService;
            this.listener = listener;
            this.databaseHandler = databaseHandler;
            this.isFirstTime = isFirstTime;
        }

        @Override
        protected JSONObject doInBackground(Void... voids) {
            try {
                return fatSnfTblService.ServiceDataGet();
            }catch (Exception e){
                e.printStackTrace();
                listener.milkRateFetchFinished(false,e.getMessage(),0L, 0L);
                return null; //// consider
            }
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            try {
                if(result != null && result.length() > 0) {
                    String strStatus = result.getString("status");
                    if(strStatus.equals("true")) {
                        new MilkSociety.FATSNFAsyncTask(isFirstTime,databaseHandler, result.getJSONArray("data"),listener).execute();
                    }else {
                        listener.milkRateFetchFinished(false,"NO VALUE RETURN FROM SERVER",0L, 0L);
                    }
                    Log.e("fatsnfJSONObject", ".........." + result.toString());
                }else {
                    listener.milkRateFetchFinished(false,"NO VALUE RETURN FROM SERVER",0L, 0L);///need to consider
                }
            }catch (Exception e){
                listener.milkRateFetchFinished(false,e.getMessage(),0L, 0L);
            }
        }
    }



    public static class FATSNFAsyncTask extends AsyncTask<Void, Void,Boolean> {

        private MilkSociety.MilkTableFetchListener listener;
        private JSONArray fatSnfArray;
        private DatabaseHandler databaseHandler;
        private boolean isFirstTime;
        private Long migratedCount = 0L;
        private Long receivedCount = 0L;


        public FATSNFAsyncTask(boolean isFirstTime, DatabaseHandler databaseHandler,
                               JSONArray fatSnfArray,
                               MilkTableFetchListener listener) {
            this.listener = listener;
            this.fatSnfArray = fatSnfArray;
            this.databaseHandler = databaseHandler;
            this.isFirstTime = isFirstTime;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            List<ContentValues> valuesList = new ArrayList<>();
            try {

                int size = fatSnfArray.length();
                receivedCount = (long) size;
                for(int i = 0; i < size; i++){
                    JSONObject job = fatSnfArray.getJSONObject(i);
                    ContentValues values = new ContentValues();
                    values.put(KEY_FAT_SNF_RATE_ID, job.getInt("fatsnfrateid"));
                    values.put(KEY_USR_ID, job.getInt("userid"));
                    values.put(KEY_FAT, job.getDouble("fat"));
                    values.put(KEY_SNF_VALUE, job.getDouble("snfvalue"));
                    values.put(KEY_RATE, job.getDouble("rate"));
                    values.put(KEY_STATUS, job.getString("status"));
                    values.put(KEY_CREATED_BY, job.getString("createdby"));
                    values.put(KEY_CREATED_DATE, job.getString("createddt"));
                    values.put(KEY_LAST_UPDATED_BY, job.getString("lastupdatedby"));
                    values.put(KEY_LAST_UPDATED_DATE, job.getString("lastupdateddt"));
                    valuesList.add(values);
                }
                migratedCount = databaseHandler.addBulkFatSNFTblValues(valuesList);
                if(migratedCount.equals(receivedCount)){
                    if(isFirstTime){
                        ContentValues cv = new ContentValues();
                        cv.put(KEY_MILK_CHECK_ID,MILK_CHECK_TBL_PRIMARY_KEY_VALUE);
                        cv.put(KEY_FAT_SNF_COLUMN_COUNT,receivedCount);
                        cv.put(KEY_SNF_CAL_COLUMN_COUNT,0L);
                        cv.put(KEY_IS_FAT_SNF_TBL_LOADED,true);
                        cv.put(KEY_IS_SNF_CAL_TBL_LOADED,false);
                        databaseHandler.addDairyDataCheckTbl(cv);
                    }else {
                        databaseHandler.updateDairyDataCheckFatSnf(receivedCount, true);
                    }
                    listener.milkRateFetchFinished(true,"Success Loading Data", receivedCount, migratedCount);
                    return true;
                }else {
                    databaseHandler.deleteBulkFatSNFTblValues(); //////////// delete unfinished records
                    listener.milkRateFetchFinished(false,"Server Data Is Not Full Load", receivedCount, migratedCount);
                }
            }catch (Exception e){
                listener.milkRateFetchFinished(false,e.getMessage(),receivedCount, migratedCount);
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean result) {
            Log.e("FatSnfResult","................................"+result);
        }
    }


    public static class SNFCalculationAsyncTask extends AsyncTask<Void, Void,JSONObject> {

        MilkSociety.SNFCalMigrationListener listener;
        private JsonServiceHandler SnfCalTblHTTPService;
        private DatabaseHandler databaseHandler;

        public SNFCalculationAsyncTask(JsonServiceHandler SnfCalTblHTTPService,
                                       DatabaseHandler databaseHandler,
                                       MilkSociety.SNFCalMigrationListener listener) {
            this.listener = listener;
            this.SnfCalTblHTTPService = SnfCalTblHTTPService;
            this.databaseHandler = databaseHandler;
        }

        @Override
        protected JSONObject doInBackground(Void... voids) {
            try {
                JSONObject resultObj =  SnfCalTblHTTPService.ServiceDataGet();
                Log.e("resultObj",".........................resultObj"+resultObj.toString());
                return resultObj;
            }catch (Exception e){
                e.printStackTrace();
                listener.snfCalculationLoadFinished(false,e.getMessage(),0L, 0L);
                return null;
            }
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            try {
                if(result != null && result.length() > 0) {
                    String strStatus = result.getString("status");
                    if(strStatus.equals("true")) {
                        new MilkSociety.FatCalTblLoadAsyncTask(listener, result.getJSONArray("data"),databaseHandler).execute();
                    }else {
                        listener.snfCalculationLoadFinished(false,"NO VALUE RETURN FROM SERVER",0L, 0L);
                    }
                    Log.e("snfCalJSONObject", ".........." + result.toString());
                }else {
                    listener.snfCalculationLoadFinished(false,"NO VALUE RETURN FROM SERVER",0L, 0L);
                }
            }catch (Exception e){
                listener.snfCalculationLoadFinished(false,e.getMessage(),0L, 0L);
            }
        }
    }

    private static class FatCalTblLoadAsyncTask extends AsyncTask<Void, Void, Boolean>{

        private MilkSociety.SNFCalMigrationListener listener;
        private JSONArray SnfCalArray;
        private DatabaseHandler databaseHandler;
        private Long migratedCount = 0L;
        private Long receivedCount = 0L;

        public FatCalTblLoadAsyncTask(SNFCalMigrationListener listener, JSONArray SnfCalArray, DatabaseHandler databaseHandler) {
            this.listener = listener;
            this.SnfCalArray = SnfCalArray;
            this.databaseHandler = databaseHandler;
        }


        @Override
        protected Boolean doInBackground(Void... voids) {
            List<ContentValues> valuesList = new ArrayList<>();
            try {
                int size = SnfCalArray.length();
                receivedCount = (long) size;
                for(int i = 0; i < size; i++){
                    JSONObject job = SnfCalArray.getJSONObject(i);
                    ContentValues values = new ContentValues();
                    values.put(KEY_SNF_CALC__ID, job.getInt("snfcalcid"));
                    values.put(KEY_SNF_CAL_USR_ID, job.getInt("userid"));
                    values.put(KEY_SNF_CAL_TEMPERATURE, job.getDouble("temperature"));
                    values.put(KEY_SNF_CAL_IR_VALUE, job.getDouble("lrvalue"));
                    values.put(KEY_SNF_CAL_SNF_VALUE, job.getDouble("snfvalue"));
                    values.put(KEY_SNF_CAL_STATUS, job.getString("status"));
                    values.put(KEY_SNF_CAL_CREATED_BY, job.getString("createdby"));
                    values.put(KEY_SNF_CAL_CREATED_DATE, job.getString("createddt"));
                    values.put(KEY_SNF_CAL_LAST_UPDATED_BY, job.getString("lastupdatedby"));
                    values.put(KEY_SNF_CAL_LAST_UPDATED_DATE, job.getString("lastupdateddt"));
                    valuesList.add(values);
                }
                migratedCount = databaseHandler.addBulkSNFCalTblValues(valuesList);//
                if(migratedCount.equals(receivedCount)){
                    databaseHandler.updateDairyDataCheckSnfCal(receivedCount, true);
                    listener.snfCalculationLoadFinished(true,"Success Loading Data", receivedCount, migratedCount);
                    return true;
                }else {
                    databaseHandler.deleteBulkSNFCalTblValues(); //////////// delete unfinished records
                    listener.snfCalculationLoadFinished(false,"Server Data Is Not Full Load", receivedCount, migratedCount);
                }
            }catch (Exception e){
                listener.snfCalculationLoadFinished(false,e.getMessage(),receivedCount, migratedCount);
            }
            return false;
        }
                @Override
        protected void onPostExecute(Boolean result) {
            Log.e("SnfCalResult","................................"+result);
        }
    }


}

