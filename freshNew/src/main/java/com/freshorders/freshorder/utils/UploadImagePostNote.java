package com.freshorders.freshorder.utils;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ProgressBar;

import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.ui.MerchantComplaintActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

import static com.freshorders.freshorder.service.PrefManager.KEY_IS_CRASH_HAPPENED;


public class UploadImagePostNote extends AsyncTask<Void, Integer, String> {
    long totalSize = 0;
    private ProgressBar progressBar;
    Activity activity;
    String  path1="",Duserid="",Discription="",userid="";
    ProgressDialog dialog;
    Context context;
    private Context curContext;

    private String bugFileLoading = "";

    public UploadImagePostNote(Activity activity,String path1,String Discription,String Duserid,String userid){
        this.activity=activity;
        this.path1=path1;
        this.Duserid=Duserid;
        this.Discription=Discription;
        this.userid=userid;

        context =activity;

    }
    //Kumaravel for sending bug report

    public UploadImagePostNote(Context current, Context context,String path1,String Discription,String Duserid,String userid, String bugFileLoading){
        this.path1 = path1;
        this.Duserid = Duserid;
        this.Discription = Discription;
        this.userid = userid;
        this.context = context;
        this.bugFileLoading = bugFileLoading;
        this.curContext = current;
    }

    @Override
    protected void onPreExecute() {
        // setting progress bar to zero
        if(activity != null) {
            dialog = ProgressDialog.show(context, "",
                    "Uploading...", true, true);
            dialog.setCancelable(false);
            dialog.show();
        }else {
            //dialog = ProgressDialog.show(curContext, "", bugFileLoading, true, true);
        }


        super.onPreExecute();
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {

    }

    @Override
    protected String doInBackground(Void... params) {
        //dialog.show();
        //Log.e("doInBackground", "doInBackground");
        return uploadFile();

    }

    @SuppressLint("NewApi")
    @SuppressWarnings({ "deprecation", "resource" })
    private String uploadFile() {
        String responseString = null;
        Log.e("uploadFile", "uploadFile");
        HttpClient httpclient = new DefaultHttpClient();
        String url=Utils.strAddComplaint+userid+"&duserid="+Duserid+"&particular="+Discription;
        url=url.replaceAll(" ", "%20");
        HttpPost httppost = new HttpPost(url);


        try {
            AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                    new AndroidMultiPartEntity.ProgressListener() {

                        @Override
                        public void transferred(long num) {
                            publishProgress((int) ((num / (float) totalSize) * 100));
                        }
                    });

            //for(int i=0;i<arrayListPath.size();i++){
            //File sourceFile = new File(arrayListPath.get(i));
            //entity.addPart("image[]", new FileBody(sourceFile));
            //}


            //File sourceFile1 = new File(Environment.getExternalStorageDirectory()+"/a1.jpg");
            //File sourceFile2 = new File(Environment.getExternalStorageDirectory()+"/a2.jpg");
            Log.e("path",path1);
            Log.e("userid",userid);
            Log.e("Duserid",Duserid);
            Log.e("discription", Discription);
            if(!path1.isEmpty()){
                if(path1.length()>0){
                    File sourceFile1 = new File(path1);
                    entity.addPart("file", new FileBody(sourceFile1));
                }
            }
            /*entity.addPart("muserid", new StringBody(userid));
            entity.addPart("duserid", new StringBody(Duserid));
            entity.addPart("particular", new StringBody(Discription));*/

            // Adding file data to http body
            //entity.addPart("image", new FileBody(sourceFile));
            //entity.addPart("image1", new FileBody(sourceFile1));
            //entity.addPart("image2", new FileBody(sourceFile2));
            // Extra parameters if you want to pass to server

            Charset chars = Charset.forName("UTF-8");


            //entity.addPart("orderid", new StringBody("1296"));



            totalSize = entity.getContentLength();
            httppost.setEntity(entity);

            // Making server call
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity r_entity = response.getEntity();

            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                // Server response
                responseString = EntityUtils.toString(r_entity);
                //Changed ------requirement 05-12-2019
                responseString = "Notes Posted Successfully";
            } else {
                responseString = "Error occurred! Http Status Code: "
                        + statusCode;
            }




        } catch (ClientProtocolException e) {
            responseString = e.toString();
        } catch (IOException e) {
            responseString = e.toString();
        }

        return responseString;

    }

    @Override
    protected void onPostExecute(String result) {
        Log.e("uploadclass", "Response from server: " + result);
        //Report.result=result;


        //showing the server response in an alert dialog

        if(activity != null) {
            dialog.dismiss();
            showAlert(result);
        }else {
            //Kumaravel
            new PrefManager(context.getApplicationContext()).setBooleanDataByKey(KEY_IS_CRASH_HAPPENED, false);
        }


        super.onPostExecute(result);
    }


    /**
     * Method to show alert dialog
     * */
    private void showAlert(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        /////builder.setMessage("Your query raised to dealer").setTitle("Response from Servers") 05-12-2019
        builder.setMessage(message).setTitle("Response from Servers")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Constants.setimage=null;
                        Constants.setdiscription="";
                        Constants.setdealerpostnts="";
                        Constants.setduserid="";
                        Intent to = new Intent (context, MerchantComplaintActivity.class);
                        context.startActivity(to);
                        activity.finish();

                        // do nothing
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

}