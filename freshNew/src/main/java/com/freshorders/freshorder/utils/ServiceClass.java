package com.freshorders.freshorder.utils;

import android.app.ActivityManager;
import android.content.Context;

import com.freshorders.freshorder.service.PrefManager;

import java.util.List;

public class ServiceClass {

    public static boolean isServiceRunningInForeground(Context context, Class<?> serviceClass, String TAG) throws Exception {

        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                if (service.foreground) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean isServiceRunning(Context context,String serviceName) {
        boolean serviceRunning = false;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> l = am.getRunningServices(Integer.MAX_VALUE);
        for (ActivityManager.RunningServiceInfo runningServiceInfo : l) {
            if (runningServiceInfo.service.getClassName().equals(serviceName)) {
                //serviceRunning = true;

                if (runningServiceInfo.foreground) {
                    //service run in foreground
                    return true;
                }
            }
        }
        return serviceRunning;
    }
}
