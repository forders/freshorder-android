package com.freshorders.freshorder.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.freshorders.freshorder.ui.SalesmanAcknowledgeActivity;
import com.freshorders.freshorder.utils.AndroidMultiPartEntity.ProgressListener;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.File;
import java.io.IOException;

public class CsvFileUploadToServer extends AsyncTask<Void, Integer, String> {
	long totalSize = 0;
	Activity activity;
	String docPath1 = "",duserid="",salesmanid="",description="",designation="",traders="",date="",time="";
	ProgressDialog dialog;
	Context context;
	//String jobid
	public CsvFileUploadToServer(Activity activity, String docPath1,String salesmanid,String duserid,String date,String time,String traders,String designation,String description) {
		this.activity = activity;
		this.docPath1 = docPath1;
		this.duserid = duserid;
		this.salesmanid = salesmanid;
		this.description = description;
		this.designation = designation;
		this.traders = traders;
		this.date = date;
		this.time=time;

		context=activity;
	Log.e("file",docPath1);
	}

	@Override
	protected void onPreExecute() {
		dialog = ProgressDialog.show(context, "",
				"Loading...", true, true);
		dialog.setCancelable(false);
		dialog.show();
		super.onPreExecute();
	}

	@Override
	protected void onProgressUpdate(Integer... progress) {
	
	}

	@Override
	protected String doInBackground(Void... params) {
		// dialog.show();
		return uploadFile();
	}

	@SuppressWarnings("deprecation")
	private String uploadFile() {
		String responseString = null;
		designation.replaceAll(" ", "%20");
		description.replaceAll(" ", "%20");
		traders.replaceAll(" ", "%20");
		String url=Utils.strclientvisitsales+"suserid="+salesmanid+"&duserid="+duserid+"&orderplndt="+date+"%20"+time+"&remarks="+description+"&status="+"Submitted"+"&updateddt="+date+"%20"+time+"&staffname="+traders+"&reporteedesignatn="+designation;
		url=url.replaceAll(" ", "%20");
		Log.e("url",url);
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(url);

		//http://192.168.1.74:2001/api/plans?suserid=1412&duserid=2639&orderplndt=2016-12-24&remarks=Finished the day&status=Submitted&updateddt=2016-12-24&staffname=Shakthi
		try {
			AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
					new ProgressListener() {

						@Override
						public void transferred(long num) {
							publishProgress((int) ((num / (float) totalSize) * 100));
						}
					});
			
			

		if (docPath1.length() > 0) {
				File sourceFile1 = new File(docPath1);
			Log.e("inside", String.valueOf(sourceFile1));
				entity.addPart("file", new FileBody(sourceFile1));
			}  

			totalSize = entity.getContentLength();
			
			httppost.setEntity(entity);


			HttpResponse response = httpclient.execute(httppost);
			HttpEntity r_entity = response.getEntity();

			int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode == 200) {
				// Server response

				responseString = "Uploaded successfully";
			} else {

				responseString = "Error occurred! Http Status Code: "
						+ statusCode;
			}

		} catch (ClientProtocolException e) {
			responseString = e.toString();
		} catch (IOException e) {
			responseString = e.toString();
		}

		return responseString;

	}

	@Override
	protected void onPostExecute(String result) {
		Log.e("uploadclass", "Response from server: " + result);
		dialog.dismiss();
		showAlert(result);
		
		super.onPostExecute(result);
	}

	public void toast(String msg) {
		Toast toast = Toast.makeText(activity, msg, Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
		
	}

	private void showAlert(String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setMessage(message).setTitle("")
				.setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						SalesmanAcknowledgeActivity.textviewdesignation.setText("");
						SalesmanAcknowledgeActivity.textViewUploadedFile.setText("");
						SalesmanAcknowledgeActivity.textviewtrader.setText("");
						SalesmanAcknowledgeActivity.EditTextViewDescription.setText("");
						Intent io= new Intent(activity,SalesmanAcknowledgeActivity.class);
						activity.startActivity(io);
						activity.finish();
					}
				});

		AlertDialog alert = builder.create();
		alert.show();
		Button buttonbackground= alert.getButton(DialogInterface.BUTTON_POSITIVE);
		buttonbackground.setBackgroundColor(Color.parseColor("#A9E2F3"));
	}

}