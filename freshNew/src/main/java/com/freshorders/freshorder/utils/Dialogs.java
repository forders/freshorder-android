package com.freshorders.freshorder.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class Dialogs {

    private Context mContext;
    public Dialogs(Context mContext) {
        this.mContext = mContext;
    }

    public AlertDialog showDialogOKCancel(String title,
                                          String msg,
                                          String positiveLabel,
                                          DialogInterface.OnClickListener positiveOnClick,
                                          String negativeLabel,
                                          DialogInterface.OnClickListener negativeOnClick,
                                          boolean isCancelable){

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(title);
        builder.setMessage(msg);
        builder.setPositiveButton(positiveLabel, positiveOnClick);
        builder.setNegativeButton(negativeLabel, negativeOnClick);
        builder.setCancelable(isCancelable);

        AlertDialog alert = builder.create();
        alert.show();
        return alert;
    }
}
