package com.freshorders.freshorder.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.freshorders.freshorder.ui.ImportActivity;
import com.freshorders.freshorder.utils.AndroidMultiPartEntity.ProgressListener;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;

public class DocUploadFileToServer1 extends AsyncTask<Void, Integer, String> {
	long totalSize = 0;
	Activity activity;
	String docPath1 = "";
	
	//String jobid
	public DocUploadFileToServer1(Activity activity, String docPath1) {
		this.activity = activity;
		this.docPath1 = docPath1;
		
	
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	@Override
	protected void onProgressUpdate(Integer... progress) {
	
	}

	@Override
	protected String doInBackground(Void... params) {
		// dialog.show();
		return uploadFile();
	}

	@SuppressWarnings("deprecation")
	private String uploadFile() {
		String responseString = null;

		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(Utils.strImportFile);

		try {
			AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
					new ProgressListener() {

						@Override
						public void transferred(long num) {
							publishProgress((int) ((num / (float) totalSize) * 100));
						}
					});
			
			

		if (docPath1.length() > 0) {
				File sourceFile1 = new File(docPath1);
				entity.addPart("file", new FileBody(sourceFile1));
			}  
			
		
			


			totalSize = entity.getContentLength();
			
			httppost.setEntity(entity);   
	
			

		
		
			// Making server call
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity r_entity = response.getEntity();

			int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode == 200) {
				// Server response
				responseString = EntityUtils.toString(r_entity);
			} else {
				responseString = "Error occurred! Http Status Code: "
						+ statusCode;
			}

		} catch (ClientProtocolException e) {
			responseString = e.toString();
		} catch (IOException e) {
			responseString = e.toString();
		}

		return responseString;

	}

	@Override
	protected void onPostExecute(String result) {
		Log.e("uploadclass", "Response from server: " + result);
		showAlert(result);
		
		super.onPostExecute(result);
	}

	public void toast(String msg) {
		Toast toast = Toast.makeText(activity, msg, Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
		
	}

	private void showAlert(String message) {
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setMessage(message).setTitle("")
				.setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// do nothing
						ImportActivity.textViewFileName.setText("");
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}

}