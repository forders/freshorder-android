package com.freshorders.freshorder.utils;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Network;
import android.os.AsyncTask;
import android.util.Log;

import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.ui.AddMerchantNew;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by ragul on 14/08/16.
 */
public class NetworkChangeReceiver extends BroadcastReceiver {

    public static DatabaseHandler databaseHandler;
    public static JsonServiceHandler JsonServiceHandler;
    public static JSONObject JsonAccountObject = null;
    public static JSONArray JsonAccountArray = null;
    public static String payment_type, orderdate, Orderno, flag, productStatus, mode, paymenttype, pushstauts, OfflineOrderNo, rowid, beatrowid;
    public static int year, month, day, hr, mins, sec, deliverydt;
    Context context;
    public String imagepath1 = "", imagepath2 = "", imagepath3 = "", imagepath4 = "", imagepath5 = "", imagepath6 = "",
            imagepath7 = "", imagepath8 = "", imagepath9 = "", imagepath10 = "", imagepath11 = "", imagepath12 = "", selected_dealer;
    SQLiteDatabase db, sp;
    public static ArrayList<String> arraylistimagepath;
    private static String DBNAME = "freshorders.db";
    private static String Table = "orderheader";
    String[] image;
    public int orderimage = 0;


    @Override
    public void onReceive(final Context context, final Intent intent) {
        Log.e("onReceive_INSIDE", "onReceive");

    }
}