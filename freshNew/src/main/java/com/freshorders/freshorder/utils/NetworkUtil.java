package com.freshorders.freshorder.utils;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Button;

import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.ui.MerchantOrderConfirmation;
import com.freshorders.freshorder.ui.SalesManOrderCheckoutActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by ragul on 14/08/16.
 */
public class NetworkUtil {

    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;
    public static  DatabaseHandler databaseHandler;
    public static JsonServiceHandler JsonServiceHandler;
    public static JSONObject JsonAccountObject = null;
    public static JSONArray JsonAccountArray = null;
    public static String payment_type,orderdate,Orderno,flag,productStatus,mode,paymenttype,pushstauts,OfflineOrderNo;
    public static int year,month,day,hr,mins,sec,deliverydt;
    public static Context mcontext;
    public static int getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;

            if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }
    public static String getConnectivityStatusString(Context context) {
        int conn = NetworkUtil.getConnectivityStatus(context);
        String status = null;
        if (conn == NetworkUtil.TYPE_WIFI) {
            status = "Wifi enabled";
            Log.e("Wifi enabled","Wifi enabled");

        } else if (conn == NetworkUtil.TYPE_MOBILE) {
            status = "Mobile data enabled";
            Log.e("Mobile data enabled","Mobile data enabled");
        } else if (conn == NetworkUtil.TYPE_NOT_CONNECTED) {
            status = "Not connected to Internet";
            Log.e("NotconnectedtoInternet","Not connected to Internet");
        }
        return status;
    }

}
