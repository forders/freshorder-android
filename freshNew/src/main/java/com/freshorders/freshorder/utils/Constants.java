package com.freshorders.freshorder.utils;

import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;

import com.freshorders.freshorder.BuildConfig;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

public class Constants {

	public static String CLOSING_STOCK_QUANTITY = "7777777";
	public static String PRODUCT_QUANTITY = "6666666";

	public static String OUTLET = "Outlet";
	public static String SUCCESS = "success";
	public static String FAIL = "fail";
	public static long CURRENT_MILLIS = 0;
	public static String OFFLINE_MERCHANT_ID ="";  //this key used for to fetch merchant id and to set order then send to server as offline to online
	//public static final int WRITE_EXTERNAL_STORAGE= Integer.parseInt(null);  // Kumaravel  USER_TYPE this to USER_TYPE = "";
	public static String USER_TYPE = "",USER_ID,USER_EMAIL,USER_FULLNAME,USER_NAME,DUSER_ID,DUSER_ID1;
	public static String STOCKIEST;
	public static String PAYMENT;
	public static int onLineCount=0;
	public static String Url="http://cdn2.stylecraze.com/wp-content/uploads/2017/08/Oriflame-Beauty-And-Skin-Care-Products-Top-15.jpg";
	public static double refvalue=1.5;
	public static String SalesMerchant_Id ="null";
	public static String PaymentType ="null";
	public static String basecustmfldid ="";
	public static String Merchantname="";
	public static String paymentmerchname="null";
	public static String paymentmerchant_id ="null";
	public static String clientvisitmerchname="";
	public static String clientvisitmerchant_id ="null";
	public static String serialno="";
	public static String orderid="";
	public static int TRACK_INTERVAL=30;
	public static int GEO_RECORDS_UPLOAD_LIMIT=30;
	public static int syn;
	public static int checkproduct;
	public static String orderstatus="Success";
	public static String removeid="";
	public static String Imageduserid="";
	public static String dealerid="";
	public static int adapterset=0,setorder=0;
	public static Bitmap setimage=null;
	public static String setattachmntimage="";
	public static String setdealerpostnts="";
	public static String setdiscription="",setduserid="",isread="";

	public static String clientdealerid="";
	public static final int WRITE_EXTERNAL_STORAGE = 100;
	public static final int READ_EXTERNAL_LOCATION = 0;
	public static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
	public static String fromDate="";
	public static String toDate="";
	public static Integer salesmans[];
	public static String refreshscreen="";
	public static String filter="",ClientStatus="Success";
	public static Integer sendimageproduct=0,imagesetcheckout=0;
	public static double approximate=0;
	public static String downloadScheme="";
	public static String totalorders="";
	public static String startTime="";
	public static String beatassignscreen="";

	public  static String[] deliverymode={"Standard","Scheduled"};

	public static JSONArray jsonArCustomFieldsMerchant= null;

	// Added by SK for pushing distributorname
	public static String distributorname ="";
	public static String distributorId="";
	// End of SK Changes
    public static String distributorname_stock ="";
	public static final char CHAR_DOT = '.';
	public static final String DOT = ".";
	public static final String TEMPLATE1_STATUS_MESSAGE = "online_status_no_need";
	public static final String TEMPLATE1_PAIR_MISSING_TITLE = "Input Missing";
	public static final String TEMPLATE1_PAIR_MISSING_MESSAGE = "Please Enter Missing Closing Stock Or Quantity Or Check More Than One DOT";
	public static final String TEMPLATE3_PAIR_MISSING_MESSAGE = "Please Enter Missing Price Or Quantity Or Check More Than One DOT";
	public static final String TEMPLATE10_PAIR_MISSING_MESSAGE = "Please Enter Missing Sales Value Or Quantity";
	public static final String SHOULD_NOT_EMPTY_DATA = "Should Not Empty data Please Fill Both";
	public static final String SHOULD_NOT_ONLY_DOT = "Any One Input Contains Only Dot Symbol(.)";
	public static final String QUANTITY_TEXT_FOR_TEMPLATE = " Qty ";
	public static final String CLOSING_STOCK_TEXT_FOR_TEMPLATE = "stk";
	public static final String STR_CURRENT_STOCK = "Curr Stock ";
	public static final String CURRENT_STOCK_NOT_AVAIL = "NA";
	public static final String SERVER_SEND_NO_STOCK_RESULT = "Server Send No Stock Result";
	public static final String EMPTY = "";
	public static final String SINGLE_SPACE = " ";
	public static final String TEMPLATE2_NO_STOCK_INFO = "offline NA";
	public static final String DISTRIBUTOR_STOCK = "Distributor Stock : ";
	public static final String COMPANY_STOCK = "Company Stock : ";

	public static final String MILK = "MILK";
	public static final String FAT = "FAT";
	public static final String SNF = "SNF";
	public static final String TEMPERATURE = "TEMPERATURE";
	public static final String LRVALUE = "LRVALUE";

	public static String TEMPLATE_NO = "";
	public static final int MILK_CHECK_TBL_PRIMARY_KEY_VALUE = 1000;

	public static final int MY_SOCKET_TIMEOUT_MS = 60*1000;

	public static final String TEMPLATE1 = "1";
	public static final String TEMPLATE2 = "2";
	public static final String TEMPLATE3 = "3";
	public static final String TEMPLATE4 = "4";
	public static final String TEMPLATE5 = "5";  /// tHIS IS DEFAULT TEMPLATE
    public static final String DEFAULT_TEMPLATE_NO = TEMPLATE5;
	public static final String TEMPLATE6 = "6";
	public static final String MILK_TEMPLATE_NO = TEMPLATE6;
	public static final String TEMPLATE7 = "7";
	public static final String TESTING_TEMPLATE = TEMPLATE7;
	public static final String TEMPLATE8 = "8";
	public static final String TEMPLATE9 = "9";
	public static final String TEMPLATE10 = "10";

	public static final String DATA_MIGRATION_TO_SERVER = "Pending Order Move To Server. It May Take Little Long Time...";
	public static final String MIGRATION_SUCCESS = "Pending Order Moved To Server Successfully";
	public static final String MIGRATION_FAILED = "Pending Order Moving Temporary Failed";
	public static final String NEW_CLIENT_FAILED = "New Client Moving Temporary Failed";
	public static final String TODAY_ORDER_LOADING = "Loading Order From Server....";
	public static final String GETTING_LOCATION = "Getting Location....";

	public static final int ALERT_CHANNEL_ID = 1002;
	public static final String STR_DEFAULT_FLOAT_VALUE = "0.00";
	public static final String ALERT_MIGRATION_TITLE = "Bending Order Available";
	public static final String ALERT_MIGRATION_MESSAGE = "Should Move Order To Server Click Me";
	public static final String MILK_REQUIREMENT_NOTIFICATION = "----------You Should Give Milk value With SNF OR Milk, Temperature, LR Values---------";
	public static final String MILK_INPUT_NOT_MATCHING = "-------You Entered Value Not Matching With Prent Table Value Should Enter Correct Value------";
	public static final String STATUS_PENDING = "Pending";
	public static final String OFFLINE_COUNT = "Offline Count : ";
	public static final String FAILED_COUNT = "Failed Count : ";

	public static final String ORDER_ALREADY_EXIST = "Order Already exist";
	public static final String ORDER_SAVE_FAILED = "Order details not saved. Please try again";

	// Error msg

	public static final String TIME_OUT_ERROR = "TimeoutError";
	public static final String AUTH_FAILURE_ERROR = "AuthFailureError";
	public static final String SERVER_ERROR = "ServerError";
	public static final String NETWORK_ERROR = "NetworkError";
	public static final String PARSE_ERROR = "ParseError";


	public static int MIGRATION_SERVICE_ID = 5000;
	public static int NOTIFICATION_MIG_ID = 5001;
	public final static int BOOT_COMPLETED_AM_ID = 5002;
	//////////////////////////////////////////////
	public final static int DATE_TIME_SUNDAY = 8;
	public final static int SELF_START_TIME_LIVE_TRACK = 9;
	public final static int SELF_STOP_TIME_LIVE_TRACK = 19;
	public final static int BR_REQUEST_CODE_START_LIVE_TRACK = 1111;
	public final static int BR_REQUEST_CODE_STOP_LIVE_TRACK = 1112;
	public final static int BR_REQUEST_CODE_START_LIVE_TRACK_By_My_DAY_PLAN = 1113;
	public final static int BR_REQUEST_CODE_START_LIVE_TRACK_BY_ALARM = 1114;

	public final static String DATE_PATTERN_FOR_APP = "dd-MM-yyyy";

	public static int MERCHANT_PRIMARY_KEY_FOR_PKD = 0;
	public static int MERCHANT_PRIMARY_KEY_FOR_SURVEY = 0;



	public static final String BACKUP_FOLDER = "FreshOrdersDBBackup";
	public static final String DATABASE = "freshorders";
	public static final String IMPORT_FOLDER = "ImportFrom";

	public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 5000;

	public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
			UPDATE_INTERVAL_IN_MILLISECONDS / 2;

	public static final String GPS_LOCATION_FETCHING = "GPS location fetching...";

	public static final String EXTRA_LOCATION ="location_move_to_check_out";



	public static final String MENU_PROFILE_CODE = "001";
	public static final String MENU_MY_DAY_PLAN_CODE = "002";
	public static final String MENU_MY_ORDERS_CODE = "003";
	public static final String MENU_ADD_MERCHANT_CODE = "004";

	public static final String MENU_CREATE_ORDER_CODE = "005";
	public static final String MENU_MY_SALES_CODE = "006";
	public static final String MENU_POST_NOTES_CODE = "007";
	public static final String MENU_MY_CLIENT_VISIT_CODE = "008";

	public static final String MENU_ACKNOWLEDGE_CODE = "009";
	public static final String MENU_PAYMENT_COLLECTION_CODE = "010";
	public static final String MENU_PKD_DATA_CAPTURE_CODE = "011";
	public static final String MENU_DISTRIBUTOR_STOCK_CODE = "012";

	public static final String MENU_SURVEY_USER_CODE = "013";
	public static final String MENU_DOWNLOAD_SCHEME_CODE = "014";
	public static final String MENU_DISTANCE_CALCULATION_CODE = "015";
	public static final String MENU_REFRESH_CODE = "016";

	public static final String MENU_SIGN_OUT_CODE = "017";
	public static final String MENU_CLOSING_STOCK_CODE = "018";
	public static final String MENU_CLOSING_STOCK_AUDIT_CODE = "019";
	public static final String MENU_PENDING_DATA_CODE = "020";

	public static final String MOBILE_MENUS = "MOBILE_MENUS";

	public static final String DSR_NO_DATA_FOUND = "No report found for selected date";


	public  static final long DETECTION_INTERVAL_IN_MILLISECONDS = 30 * 1000;
	public static final int CONFIDENCE = 70;
	public static final int USER_ACTIVITY_RQ_CODE = 6000;

	public static final String PLACE_HIERARCHY_FILE_JSON = "place_hierarchy_file.json";

	public static final int INTENT_KEY_DATE_RESULT_CODE = 123;

	public static final String MY_DAY_PLAN_LEAVE = "Leave";













	public static final List<Intent> POWERMANAGER_INTENTS = Arrays.asList(
			new Intent().setComponent(new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity")),
			new Intent().setComponent(new ComponentName("com.letv.android.letvsafe", "com.letv.android.letvsafe.AutobootManageActivity")),
			new Intent().setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.optimize.process.ProtectActivity")),
			new Intent().setComponent(new ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.appcontrol.activity.StartupAppControlActivity")),
			new Intent().setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity")),
			new Intent().setComponent(new ComponentName("com.coloros.safecenter", "com.coloros.safecenter.startupapp.StartupAppListActivity")).setData(Uri.fromParts("package", "com.location.locationfinder", null)),
			new Intent().setComponent(new ComponentName("com.oppo.safe", "com.oppo.safe.permission.startup.StartupAppListActivity")),
			new Intent().setComponent(new ComponentName("com.iqoo.secure", "com.iqoo.secure.ui.phoneoptimize.AddWhiteListActivity")),
			new Intent().setComponent(new ComponentName("com.iqoo.secure", "com.iqoo.secure.ui.phoneoptimize.BgStartUpManager")),
			new Intent().setComponent(new ComponentName("com.vivo.permissionmanager", "com.vivo.permissionmanager.activity.BgStartUpManagerActivity")),
			new Intent().setComponent(new ComponentName("com.asus.mobilemanager", "com.asus.mobilemanager.entry.FunctionActivity")),
			new Intent().setComponent(new ComponentName("com.asus.mobilemanager", "com.asus.mobilemanager.autostart.AutoStartActivity")),
			new Intent().setComponent(new ComponentName("com.letv.android.letvsafe", "com.letv.android.letvsafe.AutobootManageActivity"))
					.setData(android.net.Uri.parse("mobilemanager://function/entry/AutoStart")),
			new Intent().setComponent(new ComponentName("com.meizu.safe", "com.meizu.safe.security.SHOW_APPSEC")).addCategory(Intent.CATEGORY_DEFAULT).putExtra("packageName", BuildConfig.APPLICATION_ID),
			new Intent().setComponent(new ComponentName("com.samsung.android.lool",
					"com.samsung.android.sm.ui.battery.BatteryActivity"))
	);

}
