package com.freshorders.freshorder.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;

import android.text.TextUtils;

import com.freshorders.freshorder.model.GRN;
import com.freshorders.freshorder.model.OrderDetail;
import com.freshorders.freshorder.model.Product;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class Utils {
    //public static String urlPrefix="http://52.77.252.149:2001/api/"; -old
 /*   public static String urlPrefix="http://27.250.1.52:2001/api/";
      public static String strDownloadCsv= "http://27.250.1.52:2001";*/
    //public static String urlPrefix="http://27.250.1.54:2001/api/";
  /*  public static String urlPrefix="http://27.250.1.58:2001/api/";
      public static String strDownloadCsv= "http://27.250.1.58:2001";*
    /*public static String urlPrefix = "http://192.168.1.112:2001/api/";
      public static String strDownloadCsv = "http://192.168.1.112:2001";*/

  //LOCAL
 /*   public static String urlPrefix = "http://192.168.0.105:2001/api/";
      public static String strDownloadCsv = "http://192.168.0.105:2001";*/
//Dev
/*
  public static String urlPrefix = "http://13.233.72.134:2001/api/";
  public static String strDownloadCsv = "http://13.233.72.134:2001";
*/
    //Kumaravel testing
  //public static String urlPrefix = "http://13.233.72.134:2001/api/";
  //public static String strDownloadCsv = "http://13.233.72.134:2001";  http://139.59.65.102:2005/

                                                    public static String urlPrefix = "http://139.59.65.102:2005/api/";
                                                    public static String strDownloadCsv = "http://139.59.65.102:2005/";


    // production  now using 23/2/2019   and  02/07/2019 and 22-07-2019
    //public static String urlPrefix = "https://api.freshorders.in/api/";










    //public static String strDownloadCsv = "https://api.freshorders.in";





    // production

    //public static String urlPrefix = "http://13.126.0.169:2001/api/";
    //public static String strDownloadCsv = "http://13.126.0.169:2001";


    //kumaravel
    public static String urlFatSnfTbl = urlPrefix + "fatsnfrate?filter[where][status]=Active";
    public static String urlSnfCalTbl = urlPrefix + "snfcalc?filter[where][status]=Active";

//1

   // public static String urlPrefix = "http://13.233.72.134:2001/api/";
   // public static String strDownloadCsv = "http://13.233.72.134:2001";


    /*public static String urlPrefix = "http://dev.gntstech.in/api/";
      public static String strDownloadCsv = "http://dev.gntstech.in";
*/

    //*P1


  /*  public static String urlPrefix = "http://13.126.0.169:2011/api/";
      public static String strDownloadCsv = "http://13.126.0.169:2011";*/
    //public static String urlPrefix = "http://192.168.1.77:2001/api/";
    //public static String strDownloadCsv = "http://192.168.1.77:2001";
    public static String strorderdetails = urlPrefix+"orderdetails/eodreport";
    public static String strchangepasswordUrl = urlPrefix+"users/changepassword";
    public static String strImageUrl = urlPrefix + "files/orders/download/";
    public static String strcomplainturl = urlPrefix + "files/complaints/download/";
    public static String strSignInUrl = urlPrefix + "users/login";
    public static String strdistanceUrl = urlPrefix + "distances";
    //public static String strSignInUrl = urlPrefix+"users?filter[where][mobileno]=";rrr
    public static String strSignInUrlAdd = "&filter[where][password]=";
    public static String strSignUpUrl = urlPrefix + "users";
    public static String Fileupload = urlPrefix + "orderdetails/upload?ordid=";
    public static String strProfile = urlPrefix + "users/upload";
    public static String strDealerOrderList = urlPrefix + "orderdetails?filter[where][duserid]=";
    public static String strDealerGetProduct = urlPrefix + "products?filter[include]=user&filter[where][userid]=";
    public static String strDealerProduct = urlPrefix + "products?filter[include]=user&filter[where][prodstatus]=Active&filter[where][userid]=";
    public static String strDealerAddProduct = urlPrefix + "products";
    public static String strDealerUpdateProduct = urlPrefix + "products/update?where[prodid]=";
    public static String strMerchantOrderList = urlPrefix + "orders/orderlist";
    //public static String strDealerList = urlPrefix+"dealer?filter[include]=D&filter[where][muserid]=";
    public static String strDealerList = urlPrefix + "dealer?filter[where][muserid]=";
    public static String strofflineDealerList = urlPrefix + "dealer?filter[where][muserid]=";
    public static String strAddDealerList = urlPrefix + "users/dealerlist";
    public static String strAddDealers = urlPrefix + "dealer";
    public static String strAddComplaintForMigration = urlPrefix + "complaints?muserid="; /// By Kumaravel For Moving Data from Offline to online
    public static String strAddComplaint = urlPrefix + "complaints?muserid=";
    public static String strGetComplaint = urlPrefix + "complaints?filter[where][duserid]=";
    public static String strDeleteComplaint = urlPrefix + "complaints/update?where[complid]=";
    public static String strAddComplaintGetDealer = urlPrefix + "dealer?filter[include]=D&filter[where][muserid]=";
    public static String strVerifyOtp = urlPrefix + "users/verifyotp";
    public static String strResendOtp = urlPrefix + "users/resendotp";
    public static String strAddProfileDetail = urlPrefix + "users/update?where[userid]=";
    public static String strGetProfileDetail = urlPrefix + "users/";
    // http://80.241.210.161:2001/api/users/update?where[userid]=27211
    public static String strSalesmanSaveURL = urlPrefix + "users/update?where[userid]=";
    public static String strCustomFieldURL = urlPrefix +"customfields?filter[where][usertype]=S";
    public static String strCustomFieldMerchantURL = urlPrefix +"customfields?&filter[where][usertype]=M&filter[where][duserid]=";
    public static String strSalesData = urlPrefix +"users?&filter[include]=userdetails&filter[where][userid]=";
    public static String strdealerOderdetail = urlPrefix + "orderdetails?filter[include]=product&filter[where][ordid]=";
    public static String strMerSingleOderdetail = urlPrefix + "orderdetails?filter[include]=product&filter[where][ordid]=";
    public static String strMerSingleOderdetailAdditon = "&filter[where][duserid]=";
    public static String strSalesmanSingleOderdetailAdditon = "&filter[where][suserid]=";
    public static String strsavedealerOderdetail = urlPrefix + "orderdetails/bulkorderupdate";
    public static String strImportFile = urlPrefix + "products/import?userid=" + Constants.USER_ID;
    public static String strmerchantOderdetail = urlPrefix + "dealer?filter[include]=products&filter[where][muserid]=";// dealer?filter[include]=products&filter[where][dstatus]=Active&filter[where][muserid]=1403&prodstatus=Active//&filter[where][dstatus]=Active
    public static String strdealerProduct = "&filter[where][duserid]=";
    public static String strsavemerchantOderdetail = urlPrefix + "orders";
    public static String strImportProfilePic = urlPrefix + "users/upload?userid=";
    public static String strProfilePicPrefix = urlPrefix + "files/profile/download/";
    public static String strGetPayment = urlPrefix + "paymentsetup?filter[where][pymttenure]=";
    public static String strGCMUrl = urlPrefix + "users/sendnotification";
    public static String strDownloadUrl = urlPrefix + "files/products/download/Import-Product.csv";
    public static String strExport = urlPrefix + "orders/exportcsv";
    public static String strofflineproduct = urlPrefix + "products/productlist";
    public static String strGetPaymentHsitory = urlPrefix + "paymentdetails?filter[where][userid]=";
    public static String strdeletedealer = urlPrefix + "dealer/update?[where][usrdlrid]=";
    public static String strisread = urlPrefix + "orderdetails/update?[where][ordid]=";
    public static String strdeleteorder = urlPrefix + "orderdetails/update?[where][ordid]=";
    public static String strgetdealerVisit = urlPrefix + "plans?filter[order]=planid%20DESC&filter[include]=muser&filter[include]=suser&filter[where][duserid]=";
    public static String strclientVisitFilter = urlPrefix + "plans/smplans";
    public static String strgetPaymentId = urlPrefix + "paymentdetails?filter[where][userid]=";
    public static String strSaveTracklog = urlPrefix + "geologs";
    public static String strstocks = urlPrefix + "stocks";
    public static String strsavebeat = urlPrefix + "beats";
    public static String strbeat = urlPrefix + "beats?filter[where][suserid]=";//
    public static String strMerchantCount = urlPrefix + "users/count";
    public static String strdistance ="https://maps.googleapis.com/maps/api/distancematrix/json?mode=transit&origins=";
    public static String apiKey="&key=AIzaSyCHG7Q2Eie0zO85-Y7WgUOBuZGq3A_zxJE";
    public static String destination="&destinations=";
    public static String stramountcalculation = urlPrefix + "settings?filter[where][refkey]=ALLOWANCE&filter[where][userid]=";
    public static String strsettingsalesmandata = urlPrefix + "settings?filter[where][refkey]=SCUSFIELD&filter[where][userid]=";
    public static String strsettingmerchantvali = urlPrefix + "settings?filter[where][refkey]=SURVEYUSER&filter[where][userid]=";
    public static String strGrnOrder = urlPrefix + "orders?origin=grn&filter[where][suserid]=" + Constants.USER_ID + "&filter[where][ordstatus]=Pending";
    public static String strOrderAcceptance = urlPrefix + "orders?filter[where][muserid]=" + Constants.USER_ID + "&filter[where][ordstatus]=Pending";
    public static String strGrnstocks = urlPrefix + "stocks/grn";
    public static String strBulkOrderAcceptance = urlPrefix + "orderdetails/bulkorderupdate";
    // Added by SK
    public static String strMerchantCurrentStockPosition = urlPrefix + "stocks/currentstock";

    public static String strMerchantStockPosition = urlPrefix + "stocks/outletstock";
    public static String strDistriProd= urlPrefix + "products/distributor";
    public static String strSaveDistriStock= urlPrefix + "stocks/distributorstock";
    public static String strSaveSurvey= urlPrefix + "users/survey";

    //public static String strgetScheme=urlPrefix+"schemes?filter[where][duserid]=";
    public static String stracknowledge = urlPrefix + "plans/smplans";
    public static String strsumdistancecal = urlPrefix + "geologs/logs";
    public static String strgetScheme = urlPrefix + "schemes/schemes";
    public static String strUpdateOrderHeader = urlPrefix + "orders/update?where[ordid]=";
    public static String strMySales = urlPrefix + "orderdetails/report";
    public static String strstockdetail = urlPrefix + "orderdetails/report";

//old - public static String strCreateAccessToken="https://api.instamojo.com/oauth2/token/?grant_type=client_credentials&client_id=QIvUsY7cixAa7nw3Wj6mvksGm4zAWkSSKYBVlC1e&client_secret=yynM2H46mmoxGrVokfvkT662ZBADcyORgyQIL9wV04IUGbd8wNxGfTYN8QL78UtZ5y2HPeVCg0SWI8dKWefre0Cj4LduGsFw79pWqhY0amMH6Y8Ymjx7qcwDCFo1JMF2";

    //client
    public static String strCreateAccessToken = "https://api.instamojo.com/oauth2/token/?grant_type=client_credentials&client_id=Wl1MLnLFUeTN1GtwgQHBDKuzisVbmRHxWB4K18zm&client_secret=hO24zBpLH8S2bpOIPre4wFSiyosSwwhV6m4Be9E6djXuDE3bnB0rMjvpy8tUZ5VXhzZfH3SHiOv4qU3HjaHSoKcsL4dGsoEh13u8XsKhQfRaM7JOzGxAkHJf0MAjR06b";

    //test
    //public static String strCreateAccessToken="https://test.instamojo.com/oauth2/token/?grant_type=client_credentials&client_id=2n6aCVOJtbeqfNHcSuoQ9Qc9OLqhXeN3u1BlQYZU&client_secret=YayOK9fRqnR1KUytsQUkUIP5Oj4wNnZ0KvdLO63VrgTcIXPbsGlgHspK9C1EeDfncjzif1f1kCHbyjdvkpS9ciSWa48FlogPDIV3QzgywV66Zzu0c9RfZC1SrCJcNGFR";

    public static String strInitiatePayment = urlPrefix + "paymentdetails";
    public static String strSalesmanInitiatePayment = urlPrefix + "paymentdetails/update?[where][pymntid]=";
    //production
    public static String strGetPaymentStatus = "https://www.instamojo.com/api/1.1/payment-requests/";
    // public static String strGetPaymentStatus="https://test.instamojo.com/api/1.1/payment-requests/";
    public static String strUpdatePayment = urlPrefix + "paymentdetails/update?where[pymntid]=";
    public static String strPaymentHistory = urlPrefix + "paymentdetails?filter[where][userid]=";
    public static String strForgotPassword = urlPrefix + "users/forgetpassword";
    public static String strGetMerchantList = urlPrefix + "users/merchantlist";//
    public static String strclientvisitsales = urlPrefix + "plans?";
    public static String strsalespaymntclctn = urlPrefix + "bills/list";
    public static String strupdatepaymntcollection = urlPrefix + "bills/updatepayment";
    public static String getProductSetting = urlPrefix + "productsettings";
    public static String getUserSetting = urlPrefix + "setti" +
            "" +
            "" +
            "" +
            "" +
            "" +
            "" +
            "" +
            "ngs?filter[where][refkey]=BEAT_TYPE&filter[where][userid]=";
    public static String getdistributor = urlPrefix + "dealerbeats/smdistributorlist";
    public static String getCompanyBeatMerchant = urlPrefix + "beats?filter[where][beattype]=C&filter[where][suserid]=null&filter[where][duserid]=";
    public static String strproductuom = urlPrefix + "uom?filter[where][status]=Active&filter[where][duserid]=";
    public static String strmydayplan = urlPrefix + "plans";
    public static String strworktype = urlPrefix + "plans/worktype";
    public static String strmydaydetail = urlPrefix + "plans?filter[include]=distributor&filter[include]=beat&filter[where][duserid]=";
    public static String urlUpdateMyDayPlan = urlPrefix + "plans/update?where[planid]=";


    public static String strManagerClient = urlPrefix + "plans/smplans";
    public static String strManagerSalesman = urlPrefix + "orders/orderlist";
    public static String strManagerMarketVisit = urlPrefix + "plans?filter=";
    public static String strupdatepaymntcollectionNew = urlPrefix + "bills/updatepayment";
    public static String strClosingStockAudit = urlPrefix + "stockaudit?filter=";
    public static String strClosingStockAuditSubmit = urlPrefix + "stockaudit/bulkupdate";
    public static String salesmanLive = urlPrefix + "geologs/traceroute";

    public static String strMenuSetting = urlPrefix + "settings?filter=";
    public static String strDistributor = urlPrefix + "users/distributorlist";

    public static String strDailySalesReport = urlPrefix + "orders?filter=";
    public static String strPlaceHierarchy = urlPrefix + "hierarchy?filter=";

    public static String strGetSalesManList = urlPrefix + "users/salesmanlist";//

    public static String strFCMSendURL = urlPrefix + "users/update?where[userid]=";





    public static void CopyStream(InputStream is, OutputStream os) {
  final int buffer_size = 1024;
  try {
   byte[] bytes = new byte[buffer_size];
   for (; ; ) {
    int count = is.read(bytes, 0, buffer_size);
    if (count == -1)
     break;
    os.write(bytes, 0, count);
   }
  } catch (Exception ex) {
  }
 }

  public static boolean isNetWorkAvailable(final Context mContext) {
    // for network connection
    try {
      ConnectivityManager connManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
      if(connManager != null){
        NetworkInfo mWifi = connManager
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo mNetwork = connManager
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

                return (mWifi.isConnected() || mNetwork.isConnected());
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public static ArrayList<GRN> getOrderList(final JSONArray array) {
        final ArrayList<GRN> grnList = new ArrayList<>();
        for (int i = 0; i <= array.length(); i++) {
            Log.d("grn Array", "Size"+ array.length());
            GRN grn = new GRN();
            try {
                final JSONObject obj = array.getJSONObject(i);
                grn.setOrdid(obj.optString("ordid"));
                grn.setMuserid(obj.optString("muserid"));
                grn.setSuserid(obj.optString("suserid"));
                grn.setDistributorid(obj.optString("distributorid"));
                grn.setOflnordid(obj.optString("oflnordid"));
                grn.setOrderdt(Utils.dateFormat(obj.optString("orderdt")));
                grn.setIscash(obj.optString("iscash"));
                grn.setDiscntval(obj.optString("discntval"));
                grn.setDiscntprcnt(obj.optString("discntprcnt"));
                grn.setAprxordval(obj.optString("aprxordval"));
                grn.setOrdertotal(obj.optString("ordertotal"));
                grn.setPymttotal(obj.optString("pymttotal"));
                grn.setPymtdate(obj.optString("pymtdate"));
                grn.setStart_time(obj.optString("start_time"));
                grn.setEnd_time(obj.optString("end_time"));
                grn.setGeolat(obj.optString("geolat"));
                grn.setGeolong(obj.optString("geolong"));
                grn.setOrdstatus(obj.optString("ordstatus"));
                final JSONObject distObj = obj.optJSONObject("distributor");
                if(distObj != null){
                    grn.setDistributorName(distObj.optString("fullname"));
                }
                final JSONArray orderDetailArr = obj.optJSONArray("orderdetail");
                if (orderDetailArr != null) {
                    for (int j = 0; j < orderDetailArr.length(); j++) {
                        final JSONObject detailObj = orderDetailArr.getJSONObject(j);
                        OrderDetail orderDetail = new OrderDetail();
                        orderDetail.setOrddtlid(detailObj.optString("orddtlid"));
                        orderDetail.setOrdid(detailObj.optString("ordid"));
                        orderDetail.setOrdslno(detailObj.optString("ordslno"));
                        orderDetail.setDuserid(detailObj.optString("duserid"));
                        orderDetail.setProdid(detailObj.optString("prodid"));
                        orderDetail.setBatchno(detailObj.optString("batchno"));
                        orderDetail.setManufdt(detailObj.optString("manufdt"));
                        orderDetail.setExpirydt(detailObj.optString("expirydt"));
                        orderDetail.setQty(detailObj.optString("qty"));
                        orderDetail.setCqty(detailObj.optString("cqty"));
                        orderDetail.setCurrstock(detailObj.optString("currstock"));
                        orderDetail.setPrate(detailObj.optString("prate"));
                        orderDetail.setPtax(detailObj.optString("ptax"));
                        orderDetail.setPvalue(detailObj.optString("pvalue"));
                        orderDetail.setIsfree(detailObj.optString("isfree"));
                        orderDetail.setIscash(detailObj.optString("iscash"));
                        orderDetail.setIsread(detailObj.optString("isread"));
                        orderDetail.setDeliverydt(detailObj.optString("deliverydt"));
                        orderDetail.setDeliverytype(detailObj.optString("deliverytype"));
                        orderDetail.setDeliverytime(detailObj.optString("deliverytime"));
                        orderDetail.setDiscntval(detailObj.optString("discntval"));
                        orderDetail.setDiscntprcnt(detailObj.optString("discntprcnt"));
                        orderDetail.setOrdimage(detailObj.optString("ordimage"));
                        orderDetail.setOrdstatus(detailObj.optString("ordstatus"));
                        orderDetail.setStock(detailObj.optString("stock"));
                        orderDetail.setOrd_uom(detailObj.optString("ord_uom"));
                        orderDetail.setDefault_uom(detailObj.optString("default_uom"));
                        orderDetail.setUpdateddt(detailObj.optString("updateddt"));
                        orderDetail.setUpdatedby(detailObj.optString("updatedby"));
                        orderDetail.setDeliveryqty(detailObj.optString("deliveryqty"));
                        orderDetail.setAcceptedyn(detailObj.optString("acceptedyn"));
                        orderDetail.setAccepted(false);
                        final JSONObject prodObj = detailObj.optJSONObject("product");
                        if(prodObj != null){
                            Product product = new Product();
                            product.setProdid(prodObj.optString("prodid"));
                            product.setUserid(prodObj.optString("userid"));
                            product.setProdcode(prodObj.optString("prodcode"));
                            product.setHsncode(prodObj.optString("hsncode"));
                            product.setProdname(prodObj.optString("prodname"));
                            product.setProdimage(prodObj.optString("prodimage"));
                            product.setProdprice(prodObj.optString("prodprice"));
                            product.setProdtax(prodObj.optString("prodtax"));
                            product.setMrp(prodObj.optString("mrp"));
                            product.setUom(prodObj.optString("uom"));
                            product.setUnitprice(prodObj.optString("unitprice"));
                            product.setStock(prodObj.optString("stock"));
                            product.setUnitsgm(prodObj.optString("unitsgm"));
                            product.setUnitperuom(prodObj.optString("unitperuom"));
                            product.setImportfile(prodObj.optString("importfile"));
                            product.setImportrmrk(prodObj.optString("importrmrk"));
                            product.setProdstatus(prodObj.optString("prodstatus"));
                            product.setDisplay_order(prodObj.optString("display_order"));
                            product.setMargin_percent(prodObj.optString("margin_percent"));
                            product.setUpdateddt(prodObj.optString("updateddt"));
                            orderDetail.setProduct(product);
                        }
                        grn.setOrderDetailList(orderDetail);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            grnList.add(grn);
        }
        return grnList;
    }

    public static String dateFormat(final String timeStamp) {
        if (TextUtils.isEmpty(timeStamp))
            return null;

        final String inputPattern1 = "yyyy-MM-dd";
        final String outputPattern = "dd-MMM-yyyy ";
        String date = null;
        final SimpleDateFormat inputFormat1 = new SimpleDateFormat(inputPattern1, Locale.ENGLISH);
        final SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern, Locale.ENGLISH);
        try {
            final Date dateFormat = inputFormat1.parse(timeStamp);
            date = outputFormat.format(dateFormat);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }


    public static int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        return (int)(Math.random() * ((max - min) + 1)) + min;
    }
}
