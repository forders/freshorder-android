package com.freshorders.freshorder.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import com.freshorders.freshorder.db.DatabaseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

public class SalesmanLocation extends BroadcastReceiver{
	 public static  SQLiteDatabase db;
	    LocationManager mLocManager;
	    String suserid,date,lat, lng,distance,amount;
	    GPSTracker gps;
	    final int INTERVAL = 1000 * 60 * 10; //2 minutes
		final Handler mHandler = new Handler();
		public static final String TAG = "PHONE STATE";
        private static String mLastState;
	    public static DatabaseHandler databaseHandler;
	    public static JsonServiceHandler JsonServiceHandler;
	    public static JSONObject JsonAccountObject = null;
	    public static JSONArray JsonAccountArray = null;
        public static String status="Active";
        public static  Context context1;

	    @Override
	    public void onReceive(final Context context,final Intent intent) {

        context1 = context;

	    final Handler handler = new Handler();
		handler.postDelayed(new Runnable() {
		    public void run() {
		    	
		    	  gps = new GPSTracker(context);

	               // check if GPS enabled
	               if(gps.canGetLocation()){

	                   double latitude = gps.getLatitude();
	                   double longitude = gps.getLongitude();
	                   lat = String.valueOf(latitude) ;
	                   lng = String.valueOf(longitude) ;
	                   
	                   Date d=new Date();
	   				   SimpleDateFormat ft=new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
	   	    	       date = ft.format(d).toString();
                       Log.e("time", date);

	                   //Toast.makeText(context.getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
	              

	   	    	   db = context.openOrCreateDatabase("freshorders", 0, null);
					String selectQuery = "SELECT  * FROM signintable ";

					Cursor cursor = db.rawQuery(selectQuery, null);
					int count =cursor.getCount();
					Log.e("Sign in count", String.valueOf(cursor.getCount()));
					
						if (cursor != null && cursor.getCount() > 0) {

		                    if (cursor.moveToFirst()) {
		                    	suserid = cursor.getString(4);

		                      
		                    }
                            if(netCheck()){
                              //  tracklog();
                            }else{
                                db.execSQL("INSERT INTO tracklog(suserid,logtime,geolat,geolong,status,updateddt,distance,amount) VALUES ('" + suserid + "','" + date + "','" + lat + "','" + lng + "','" + status + "','" + date + "','" + distance + "','" + amount + "' ) ");
                                Cursor cursr;
                                String selectQuery1 = "SELECT  * FROM tracklog ";
                                cursr = db.rawQuery(selectQuery1, null);
                                Log.e("Tracking Count", String.valueOf(cursr.getCount()));
                            }


						}

	               }else{
	                   // can't get location
	                   // GPS or Network is not enabled
	                   // Ask user to enable GPS/network in settings
	                   //gps.showSettingsAlert();
	               }
					
		        handler.postDelayed(this, 600000); //now is every 2 minutes //3600000
		    }
		    
		 }, 600000); //Every 120000 ms (2 minutes)
		
		Stop();
	

		
	}

	public void tracklog(){
        new AsyncTask<Void, Void, Void>() {

            String strStatus = "";
            String strMsg = "";

            @Override
            protected void onPreExecute() {

            }

            @Override
            protected void onPostExecute(Void result) {

                try {

                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("return message", strMsg);
                    if (strStatus.equals("false")) {
                        db.execSQL("INSERT INTO tracklog(suserid,logtime,geolat,geolong,status,updateddt,distance,amount) VALUES ('" + suserid + "','" + date + "','" + lat + "','" + lng + "','" + status + "','" + date + "','" + distance + "','" + amount + "' ) ");
                        Cursor cursr;
                        String selectQuery1 = "SELECT  * FROM tracklog ";
                        cursr = db.rawQuery(selectQuery1, null);
                        Log.e("Tracking Count", String.valueOf(cursr.getCount()));
                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            protected Void doInBackground(Void... params) {

                JSONArray trackSet = new JSONArray();
                Log.e("trackSet----------", String.valueOf(trackSet));
                JSONObject jsonObject = new JSONObject();

                try {
                    jsonObject.put("suserid",suserid);
                    jsonObject.put("logtime",date);
                    jsonObject.put("geolat",lat);
                    jsonObject.put("geolong",lng);
                    jsonObject.put("status",status);
                    jsonObject.put("updateddt",date);
                    jsonObject.put("distance",distance);
                    jsonObject.put("amount",amount);
                    trackSet.put(jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();}

                JsonServiceHandler = new JsonServiceHandler(Utils.strSaveTracklog, trackSet.toString(),context1);
                JsonAccountObject = JsonServiceHandler.ServiceData();
                return null;

            }
        }.execute(new Void[]{});


    }
	
	public void Stop() {
		   mHandler.removeMessages(0);
		}






















    public boolean netCheck() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) context1.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) context1.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            }

            else if (result == null) {
               /* showAlertDialog(SalesManOrderCheckoutActivity.this,
                        "No Internet Connection",
                        "Please Check Your internet connection.", false);*/
                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    public void Json(){

        //db.execSQL("INSERT INTO tracklog(suserid,logtime,geolat,geolong,status,updateddt) VALUES ('"+suserid+"','"+date+"','"+lat+"','"+lng+"','"+status+"','"+date+"' ) ");

						/*	Cursor cursr;
							String selectQuery1 = "SELECT  * FROM tracklog ";
							cursr = db.rawQuery(selectQuery1, null);
							Log.e("Tracking Count", String.valueOf(cursr.getCount()));

							final JSONArray trackSet = new JSONArray();
							if (cursr != null && cursr.getCount() > 0) {

								cursr.moveToFirst();
								while (cursr.isAfterLast() == false) {

									int totalColumn = cursr.getColumnCount();
									JSONObject trackObject = new JSONObject();

									for (int i = 0; i < totalColumn; i++) {

										if (cursr.getColumnName(i) != null) {

											try {

												if (cursr.getString(i) != null) {

													trackObject.put(cursr.getColumnName(i), cursr.getString(i));
												} else {
													trackObject.put(cursr.getColumnName(i), "");
												}
											} catch (Exception e) {
												Log.d("Track_NAME", e.getMessage());
											}
										}

									}

									trackSet.put(trackObject);
									cursr.moveToNext();
								}

								cursr.close();
								Log.e("trackSet", trackSet.toString());

								new AsyncTask<Void, Void, Void>() {

									String strStatus = "";
									String strMsg = "";

									@Override
									protected void onPreExecute() {

									}

									@Override
									protected void onPostExecute(Void result) {

									}

									@Override
									protected Void doInBackground(Void... params) {

										JsonServiceHandler = new JsonServiceHandler(Utils.strSaveTracklog, trackSet.toString(), context);
										JsonAccountObject = JsonServiceHandler.ServiceData();

										try {

											strStatus = JsonAccountObject.getString("status");
											Log.e("return status", strStatus);
											strMsg = JsonAccountObject.getString("message");
											Log.e("return message", strMsg);
											Cursor curs;
											if (strStatus.equals("true")) {


												db.execSQL("DELETE FROM tracklog");
												curs = db.rawQuery("SELECT * from tracklog", null);
												Log.e("deleteCount", String.valueOf(curs.getCount()));
											}
											else
											{
												curs = db.rawQuery("SELECT * from tracklog", null);
												Log.e("existCount", String.valueOf(curs.getCount()));
											}


										} catch (JSONException e) {
											e.printStackTrace();
										} catch (Exception e) {

										}


										return null;

									}
								}.execute(new Void[]{});


							}*/
    }
}
