package com.freshorders.freshorder.utils;

import android.content.Context;
import android.location.Location;
import android.preference.PreferenceManager;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.model.LatLong;

import java.text.DateFormat;
import java.util.Date;

public class LocationUpdateUtil {

    public static final String KEY_REQUESTING_LOCATION_UPDATES = "requesting_locaction_updates";

    public static final String KEY_PREV_LAT = "previous_latitude";
    public static final String KEY_PREV_LONG = "previous_longitude";
    public static final String KEY_DISTANCE = "total_distance";

    /**
     * Returns true if requesting location updates, otherwise returns false.
     *
     * @param context The {@link Context}.
     */
    public static boolean requestingLocationUpdates(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(KEY_REQUESTING_LOCATION_UPDATES, false);
    }

    /**
     * Stores the location updates state in SharedPreferences.
     * @param requestingLocationUpdates The location updates state.
     */
    public static void setRequestingLocationUpdates(Context context, boolean requestingLocationUpdates) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putBoolean(KEY_REQUESTING_LOCATION_UPDATES, requestingLocationUpdates)
                .apply();
    }

    public static void setLatLang(Context context, String prevLat, String preLong) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putString(KEY_PREV_LAT, prevLat)
                .apply();
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putString(KEY_PREV_LONG, preLong)
                .apply();
    }

    public static LatLong getPreviousLatLong(Context context) {
        LatLong latLong = new LatLong();
        latLong.setLatitude(PreferenceManager.getDefaultSharedPreferences(context)
                .getString(KEY_PREV_LAT, "0"));
        latLong.setLongitude(PreferenceManager.getDefaultSharedPreferences(context)
                .getString(KEY_PREV_LONG, "0"));
        return latLong;
    }

    public static void setDistance(Context context, float distance) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putFloat(KEY_DISTANCE, distance)
                .apply();
    }

    public static Float getDistance(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getFloat(KEY_DISTANCE, 0f);
    }

    /**
     * Returns the {@code location} object as a human readable string.
     * @param location  The {@link Location}.
     */
    public static String getLocationText(Location location) {
        return location == null ? "Unknown location" :
                "(" + location.getLatitude() + ", " + location.getLongitude() + ")";
    }

    public static String getLocationTitle(Context context) {
        return context.getString(R.string.location_updated,
                DateFormat.getDateTimeInstance().format(new Date()));
    }

}
