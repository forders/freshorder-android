package com.freshorders.freshorder.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;

import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.MerchantOrderDetailDomain;
import com.freshorders.freshorder.domain.MerchantOrderDomainSelected;
import com.freshorders.freshorder.model.Template2Model;
import com.freshorders.freshorder.model.Template3Model;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.freshorders.freshorder.MyApplication.previousSelected;
import static com.freshorders.freshorder.MyApplication.previousSelectedForSearch;
import static com.freshorders.freshorder.MyApplication.productIdPositionMap;


public class MerchantOrderSelectedConvert {

    private Context mContext;
    private Map<Integer, String> changedUOM;
    private boolean isTemplate2 = false;
    private DatabaseHandler databaseHandler;

    public MerchantOrderSelectedConvert(Context mContext) {
        this.mContext = mContext;
    }

    public MerchantOrderSelectedConvert(Context mContext, Map<Integer, String> changedUOM, boolean isTemplate2) {
        this.mContext = mContext;
        this.isTemplate2 = isTemplate2;
        this.changedUOM = changedUOM;
        databaseHandler = new DatabaseHandler(mContext);
    }


    public ArrayList<MerchantOrderDomainSelected> getMerchantSelectedFromTemplate2ForSearch(Map<Integer, Template2Model> selectedOrders,
                                                                                            ArrayList<MerchantOrderDetailDomain> availItems,
                                                                                            ArrayList<MerchantOrderDetailDomain> totalLoadedList){
        ArrayList<MerchantOrderDomainSelected> out = new ArrayList<>();
        for(Map.Entry entry: selectedOrders.entrySet()){
            MerchantOrderDomainSelected currentSelected = new MerchantOrderDomainSelected();
            int position = (int) entry.getKey();
            Log.e("Convert",".........................................."+ position);
            Template2Model template2Model = (Template2Model) entry.getValue();
            Log.e("template2Model","....................................closing."+ template2Model.getClosingStock() + "  qty " + template2Model.getOrderQuantity());
            String pId = template2Model.getProductId();
            int originalPosition = productIdPositionMap.get(pId);
            MerchantOrderDetailDomain currentAvailItem = totalLoadedList.get(originalPosition); ///////////////here see difference
            String quantity = template2Model.getOrderQuantity();
            String price =  currentAvailItem.getprodprice();
            int productValue = 0;
            if(!quantity.isEmpty() && !price.isEmpty()){
                productValue = (int) (Float.parseFloat(template2Model.getOrderQuantity()) * Float.parseFloat(currentAvailItem.getprodprice()));
            }

            String productId = currentAvailItem.getprodid();

            Log.e("closestock","..............close:" + template2Model.getClosingStock() + " qty: " +template2Model.getOrderQuantity());

            currentSelected.setqty(template2Model.getOrderQuantity());
            currentSelected.setOutstock(template2Model.getClosingStock());
            currentSelected.setCurrStock(template2Model.getClosingStock()); //////////////////  saravan and nambi said crrent and out stock is same
            currentSelected.setDate("");//////////////////////////////////////
            currentSelected.setusrdlrid(currentAvailItem.getusrdlrid());
            currentSelected.setprodid(currentAvailItem.getprodid());
            currentSelected.setprodcode(currentAvailItem.getprodcode());
            currentSelected.setprodname(currentAvailItem.getprodname());
            currentSelected.setUserid(currentAvailItem.getDuserid()); //// saravanan told to set this id (because in web order not showing)
            currentSelected.setCompanyname(currentAvailItem.getCompanyname());
            currentSelected.setFreeQty(currentAvailItem.getFreeQty());
            currentSelected.setserialno(currentAvailItem.getserialno());
            currentSelected.setindex(currentAvailItem.getindex());
            currentSelected.setprice(currentAvailItem.getprodprice());
            currentSelected.setptax(currentAvailItem.getprodtax());
            currentSelected.setpvalue(productValue);
            currentSelected.setpaymentType(currentAvailItem.getPaymentType());
            currentSelected.setpaymentType(currentAvailItem.getPaymentType());
            currentSelected.setBatchno(currentAvailItem.getBatchno());
            currentSelected.setExpdate(currentAvailItem.getExpdate());
            currentSelected.setMgfDate(currentAvailItem.getMgfDate());
            currentSelected.setUOM(currentAvailItem.getUOM());
            currentSelected.setorderfreeuom("");
            if(isTemplate2) {
                if(changedUOM.containsKey(position)) {
                    currentSelected.setorderuom(changedUOM.get(position));
                }else {
                    currentSelected.setorderuom(currentAvailItem.getSlectedUOM());
                }
            }else {
                currentSelected.setorderuom(currentAvailItem.getSlectedUOM());
            }
            currentSelected.setselectedfreeUOM(currentAvailItem.getselectedfreeUOM());
            currentSelected.setUnitGram(currentAvailItem.getUnitGram());
            currentSelected.setUnitPerUom(currentAvailItem.getUnitPerUom());
            currentSelected.setSelecetdQtyUomUnit(currentAvailItem.getSelectedQtyUomUnit());
            currentSelected.setSelecetdFreeQtyUomUnit(currentAvailItem.getSelectedFreeQtyUomUnit());
            currentSelected.setDeliverymode(currentAvailItem.getDeliverymode());
            /////currentSelected.setDeliverydate(currentAvailItem.getDeliverydate());
            currentSelected.setDeliverydate("");
            currentSelected.setDeliverytime(currentAvailItem.getDeliverytime());

            out.add(currentSelected);
            previousSelectedForSearch.put(productId,currentSelected); ////////// this is used for search Operation

            ///template1AsyncTask(currentAvailItem.getprodid());
        }
        return out;


    }


    public ArrayList<MerchantOrderDomainSelected> getMerchantSelectedFromTemplate1(Map<Integer, Template2Model> selectedOrders,
                                                                                   ArrayList<MerchantOrderDetailDomain> availItems){
        ArrayList<MerchantOrderDomainSelected> out = new ArrayList<>();
        Map<String, Integer> productIdPositionLocal = new LinkedHashMap<>();

        Log.e("PreviousCount","................................" + previousSelectedForSearch.size());
        int index = 0;
        for(Map.Entry entry: previousSelectedForSearch.entrySet()){
            String productId = (String) entry.getKey();
            MerchantOrderDomainSelected currentSelected = (MerchantOrderDomainSelected) entry.getValue();
            Log.e("productId","................................////////////////..........." + productId);
            int position = productIdPositionMap.get(productId);
            out.add(currentSelected);
            previousSelected.put(position,currentSelected); ////////// this is used for back arrow selection
            productIdPositionLocal.put(productId, index);
            index++;
        }



        for(Map.Entry entry: selectedOrders.entrySet()){
            MerchantOrderDomainSelected currentSelected = new MerchantOrderDomainSelected();
            int position = (int) entry.getKey();
            Log.e("Convert",".........................................."+ position);
            Template2Model template2Model = (Template2Model) entry.getValue();
            Log.e("template2Model","....................................closing."+ template2Model.getClosingStock() + "  qty " + template2Model.getOrderQuantity());
            MerchantOrderDetailDomain currentAvailItem = availItems.get(position);
            String quantity = template2Model.getOrderQuantity();
            String price =  currentAvailItem.getprodprice();
            int productValue = 0;
            if(!quantity.isEmpty() && !price.isEmpty()){
                productValue = (int) (Float.parseFloat(template2Model.getOrderQuantity()) * Float.parseFloat(currentAvailItem.getprodprice()));
            }

            String productId = currentAvailItem.getprodid();

            Log.e("closestock","..............close:" + template2Model.getClosingStock() + " qty: " +template2Model.getOrderQuantity());

            currentSelected.setqty(template2Model.getOrderQuantity());
            currentSelected.setOutstock(template2Model.getClosingStock());
            currentSelected.setCurrStock(template2Model.getClosingStock()); //////////////////  saravan and nambi said crrent and out stock is same
            currentSelected.setDate("");//////////////////////////////////////
            currentSelected.setusrdlrid(currentAvailItem.getusrdlrid());
            currentSelected.setprodid(currentAvailItem.getprodid());
            currentSelected.setprodcode(currentAvailItem.getprodcode());
            currentSelected.setprodname(currentAvailItem.getprodname());
            currentSelected.setUserid(currentAvailItem.getDuserid()); //// saravanan told to set this id (because in web order not showing)
            currentSelected.setCompanyname(currentAvailItem.getCompanyname());
            currentSelected.setFreeQty(currentAvailItem.getFreeQty());
            currentSelected.setserialno(currentAvailItem.getserialno());
            currentSelected.setindex(currentAvailItem.getindex());
            currentSelected.setprice(currentAvailItem.getprodprice());
            currentSelected.setptax(currentAvailItem.getprodtax());
            currentSelected.setpvalue(productValue);
            currentSelected.setpaymentType(currentAvailItem.getPaymentType());
            currentSelected.setpaymentType(currentAvailItem.getPaymentType());
            currentSelected.setBatchno(currentAvailItem.getBatchno());
            currentSelected.setExpdate(currentAvailItem.getExpdate());
            currentSelected.setMgfDate(currentAvailItem.getMgfDate());
            currentSelected.setUOM(currentAvailItem.getUOM());
            currentSelected.setorderfreeuom("");
            if(isTemplate2) {
                if(changedUOM.containsKey(position)) {
                    currentSelected.setorderuom(changedUOM.get(position));
                }else {
                    currentSelected.setorderuom(currentAvailItem.getSlectedUOM());
                }
            }else {
                currentSelected.setorderuom(currentAvailItem.getSlectedUOM());
            }
            currentSelected.setselectedfreeUOM(currentAvailItem.getselectedfreeUOM());
            currentSelected.setUnitGram(currentAvailItem.getUnitGram());
            currentSelected.setUnitPerUom(currentAvailItem.getUnitPerUom());
            currentSelected.setSelecetdQtyUomUnit(currentAvailItem.getSelectedQtyUomUnit());
            currentSelected.setSelecetdFreeQtyUomUnit(currentAvailItem.getSelectedFreeQtyUomUnit());
            currentSelected.setDeliverymode(currentAvailItem.getDeliverymode());
            /////currentSelected.setDeliverydate(currentAvailItem.getDeliverydate());
            currentSelected.setDeliverydate("");
            currentSelected.setDeliverytime(currentAvailItem.getDeliverytime());

            int newPosition = productIdPositionMap.get(productId);
            if(previousSelectedForSearch.get(productId) != null){
                out.set(productIdPositionLocal.get(productId),currentSelected);
                previousSelected.put(newPosition, currentSelected); ////////// this is used for back arrow selection
            }else {
                out.add(currentSelected);
                previousSelected.put(newPosition, currentSelected); ////////// this is used for back arrow selection
                previousSelectedForSearch.put(productId, currentSelected);////////////
            }
        }
        return out;
    }


    public ArrayList<MerchantOrderDomainSelected> getMerchantSelectedFromTemplate8(Map<Integer, Template2Model> selectedOrders, ArrayList<MerchantOrderDetailDomain> availItems){

        ArrayList<MerchantOrderDomainSelected> out = new ArrayList<>();
        Map<String, Integer> productIdPositionLocal = new LinkedHashMap<>();

        Log.e("PreviousCount","................................" + previousSelectedForSearch.size());
        int index = 0;
        for(Map.Entry entry: previousSelectedForSearch.entrySet()){
            String productId = (String) entry.getKey();
            MerchantOrderDomainSelected currentSelected = (MerchantOrderDomainSelected) entry.getValue();
            Log.e("productId","................................////////////////..........." + productId);
            int position = productIdPositionMap.get(productId);
            out.add(currentSelected);
            previousSelected.put(position,currentSelected); ////////// this is used for back arrow selection
            productIdPositionLocal.put(productId, index);
            index++;
        }


        for(Map.Entry entry: selectedOrders.entrySet()){
            MerchantOrderDomainSelected currentSelected = new MerchantOrderDomainSelected();
            int position = (int) entry.getKey();
            Log.e("Convert",".........................................."+ position);
            Template2Model template2Model = (Template2Model) entry.getValue();
            Log.e("template8Model","....................................closing."+ template2Model.getClosingStock());
            MerchantOrderDetailDomain currentAvailItem = availItems.get(position);
            String quantity = Constants.EMPTY;  //template2Model.getOrderQuantity();
            String price =  currentAvailItem.getprodprice();
            int productValue = 0;
            if(!quantity.isEmpty() && !price.isEmpty()){
                productValue = (int) (Float.parseFloat(template2Model.getOrderQuantity()) * Float.parseFloat(currentAvailItem.getprodprice()));
            }

            String productId = currentAvailItem.getprodid();

            Log.e("closestock","..............close:" + template2Model.getClosingStock() );

            currentSelected.setqty(template2Model.getOrderQuantity());
            currentSelected.setOutstock(template2Model.getClosingStock());
            currentSelected.setCurrStock(template2Model.getClosingStock()); //////////////////  saravan and nambi said crrent and out stock is same
            currentSelected.setDate("");//////////////////////////////////////
            currentSelected.setusrdlrid(currentAvailItem.getusrdlrid());
            currentSelected.setprodid(currentAvailItem.getprodid());
            currentSelected.setprodcode(currentAvailItem.getprodcode());
            currentSelected.setprodname(currentAvailItem.getprodname());
            currentSelected.setUserid(currentAvailItem.getDuserid()); //// saravanan told to set this id (because in web order not showing)
            currentSelected.setCompanyname(currentAvailItem.getCompanyname());
            currentSelected.setFreeQty(currentAvailItem.getFreeQty());
            currentSelected.setserialno(currentAvailItem.getserialno());
            currentSelected.setindex(currentAvailItem.getindex());
            currentSelected.setprice(currentAvailItem.getprodprice());
            currentSelected.setptax(currentAvailItem.getprodtax());
            currentSelected.setpvalue(productValue);
            currentSelected.setpaymentType(currentAvailItem.getPaymentType());
            currentSelected.setpaymentType(currentAvailItem.getPaymentType());
            currentSelected.setBatchno(currentAvailItem.getBatchno());
            currentSelected.setExpdate(currentAvailItem.getExpdate());
            currentSelected.setMgfDate(currentAvailItem.getMgfDate());
            currentSelected.setUOM(currentAvailItem.getUOM());
            currentSelected.setorderfreeuom("");
            if(isTemplate2) {
                if(changedUOM.containsKey(position)) {
                    currentSelected.setorderuom(changedUOM.get(position));
                }else {
                    currentSelected.setorderuom(currentAvailItem.getSlectedUOM());
                }
            }else {
                currentSelected.setorderuom(currentAvailItem.getSlectedUOM());
            }
            currentSelected.setselectedfreeUOM(currentAvailItem.getselectedfreeUOM());
            currentSelected.setUnitGram(currentAvailItem.getUnitGram());
            currentSelected.setUnitPerUom(currentAvailItem.getUnitPerUom());
            currentSelected.setSelecetdQtyUomUnit(currentAvailItem.getSelectedQtyUomUnit());
            currentSelected.setSelecetdFreeQtyUomUnit(currentAvailItem.getSelectedFreeQtyUomUnit());
            currentSelected.setDeliverymode(currentAvailItem.getDeliverymode());
            /////currentSelected.setDeliverydate(currentAvailItem.getDeliverydate());
            currentSelected.setDeliverydate("");
            currentSelected.setDeliverytime(currentAvailItem.getDeliverytime());

            int newPosition = productIdPositionMap.get(productId);
            if(previousSelectedForSearch.get(productId) != null){
                out.set(productIdPositionLocal.get(productId),currentSelected);
                previousSelected.put(newPosition, currentSelected); ////////// this is used for back arrow selection
            }else {
                out.add(currentSelected);
                previousSelected.put(newPosition, currentSelected); ////////// this is used for back arrow selection
                previousSelectedForSearch.put(productId, currentSelected);////////////
            }
        }
        return out;
    }


    /*

    public void template1AsyncTask(final String prodId){

        new AsyncTask<Void, Void, Void>() {
            String strStatus = "", strMsg;
            ProgressDialog dialog;
            String city;
            JSONObject JsonAccountObject = null;

            @Override
            protected void onPreExecute() {

            }
            @Override
            protected void onPostExecute(Void result) {

                try {
                    Log.e("onPostExecute","onlineStockValue");
                    strStatus = JsonAccountObject.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = JsonAccountObject.getString("message");
                    Log.e("returnmessage", strMsg);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            @Override
            protected Void doInBackground(Void... params) {

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("muserid", Constants.SalesMerchant_Id);
                    jsonObject.put("prodid", prodId);
                    jsonObject.put("distributorname", Constants.distributorname);

                    Log.e("OutletStkRqst",jsonObject.toString());

                    JsonServiceHandler JsonServiceHandler = new JsonServiceHandler(Utils.strMerchantStockPosition, jsonObject.toString(), mContext);
                    JsonAccountObject = JsonServiceHandler.ServiceData();
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute(new Void[]{});

    } */
}
