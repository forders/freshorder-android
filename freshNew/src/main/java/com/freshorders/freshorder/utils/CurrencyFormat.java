package com.freshorders.freshorder.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

public class CurrencyFormat {

    public static String getFormattedAmountsInt(String amounts){

        try{
            if(amounts == null || amounts.isEmpty()) {
                return "";
            }
            NumberFormat nf = NumberFormat.getCurrencyInstance(new Locale("en", "in"));
            DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat) nf).getDecimalFormatSymbols();
            decimalFormatSymbols.setCurrencySymbol("");
            nf.setMaximumFractionDigits(0);
            ((DecimalFormat) nf).setDecimalFormatSymbols(decimalFormatSymbols);
            String str = (nf.format(new BigDecimal(amounts)).trim());
            return  str.replaceAll("\\s+","");

        }catch (Exception e){
            e.printStackTrace();
            return "";
        }
    }

    public static String getFormattedAmounts(String amounts){  //formatter.setMaximumFractionDigits(2);

        try{
            if(amounts == null || amounts.isEmpty()) {
                return "";
            }
            NumberFormat nf = NumberFormat.getCurrencyInstance(new Locale("en", "in"));
            DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat) nf).getDecimalFormatSymbols();
            decimalFormatSymbols.setCurrencySymbol("");
            nf.setMaximumFractionDigits(2);
            ((DecimalFormat) nf).setDecimalFormatSymbols(decimalFormatSymbols);
            String str = (nf.format(new BigDecimal(amounts)).trim());
            return  str.replaceAll("\\s+","");

        }catch (Exception e){
            e.printStackTrace();
            return "";
        }
    }

    public static String getFormattedAmounts_Rs(String amounts){  //formatter.setMaximumFractionDigits(2);

        try{
            if(amounts == null || amounts.isEmpty()) {
                return "";
            }
            NumberFormat nf = NumberFormat.getCurrencyInstance(new Locale("en", "in"));
            DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat) nf).getDecimalFormatSymbols();
            decimalFormatSymbols.setCurrencySymbol("Rs. ");
            nf.setMaximumFractionDigits(2);
            ((DecimalFormat) nf).setDecimalFormatSymbols(decimalFormatSymbols);
            String str = (nf.format(new BigDecimal(amounts)).trim());
            //return  str.replaceAll("\\s+","");
            return str;

        }catch (Exception e){
            e.printStackTrace();
            return "";
        }
    }
}
