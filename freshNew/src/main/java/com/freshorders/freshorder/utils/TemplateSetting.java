package com.freshorders.freshorder.utils;

import android.content.ContentValues;
import android.content.Context;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.db.DatabaseHandler;

import static com.freshorders.freshorder.db.DatabaseHandler.KEY_TEMPLATE_KEY;
import static com.freshorders.freshorder.db.DatabaseHandler.KEY_TEMPLATE_USER_ID;
import static com.freshorders.freshorder.db.DatabaseHandler.KEY_TEMPLATE_VALUE;
import static com.freshorders.freshorder.utils.Constants.TEMPLATE1;
import static com.freshorders.freshorder.utils.Constants.TEMPLATE10;
import static com.freshorders.freshorder.utils.Constants.TEMPLATE2;
import static com.freshorders.freshorder.utils.Constants.TEMPLATE3;
import static com.freshorders.freshorder.utils.Constants.TEMPLATE4;
import static com.freshorders.freshorder.utils.Constants.TEMPLATE5;
import static com.freshorders.freshorder.utils.Constants.TEMPLATE6;
import static com.freshorders.freshorder.utils.Constants.TEMPLATE9;

public class TemplateSetting {

    Context mContext;

    public TemplateSetting(Context mContext) {
        this.mContext = mContext;
    }



    public static void templateSetting(String templateNO){

        try{
            Constants.TEMPLATE_NO = templateNO;
            MyApplication app = MyApplication.getInstance();
            switch (templateNO){
                case TEMPLATE1:
                    app.setTemplate1(true); app.setTemplate2(false); app.setTemplate3(false); app.setTemplate4(false);
                    app.setTemplate5(false); app.setTemplate6(false); app.setTemplate9(false);app.setTemplate10(false);
                    break;
                case TEMPLATE2:
                    app.setTemplate1(false); app.setTemplate2(true); app.setTemplate3(false); app.setTemplate4(false);
                    app.setTemplate5(false); app.setTemplate6(false); app.setTemplate9(false);app.setTemplate10(false);
                    break;
                case TEMPLATE3:
                    app.setTemplate1(false); app.setTemplate2(false); app.setTemplate3(true); app.setTemplate4(false);
                    app.setTemplate5(false); app.setTemplate6(false); app.setTemplate9(false);app.setTemplate10(false);
                    break;
                case TEMPLATE4:
                    app.setTemplate1(false); app.setTemplate2(false); app.setTemplate3(false); app.setTemplate4(true);
                    app.setTemplate5(false); app.setTemplate6(false); app.setTemplate9(false);app.setTemplate10(false);
                    break;
                case TEMPLATE5:
                    app.setTemplate1(false); app.setTemplate2(false); app.setTemplate3(false); app.setTemplate4(false);
                    app.setTemplate5(true); app.setTemplate6(false); app.setTemplate9(false);app.setTemplate10(false);
                    break;
                case TEMPLATE6:
                    app.setTemplate1(false); app.setTemplate2(false); app.setTemplate3(false); app.setTemplate4(false);
                    app.setTemplate5(false); app.setTemplate6(true); app.setTemplate9(false);app.setTemplate10(false);
                    break;
                case TEMPLATE9:
                    app.setTemplate1(false); app.setTemplate2(false); app.setTemplate3(false); app.setTemplate4(false);
                    app.setTemplate5(false); app.setTemplate6(false); app.setTemplate9(true);app.setTemplate10(false);
                    break;
                case TEMPLATE10:
                    app.setTemplate1(false); app.setTemplate2(false); app.setTemplate3(false); app.setTemplate4(false);
                    app.setTemplate5(false); app.setTemplate6(false); app.setTemplate9(false); app.setTemplate10(true);
                    break;
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
