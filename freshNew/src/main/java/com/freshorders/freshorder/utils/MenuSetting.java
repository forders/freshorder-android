package com.freshorders.freshorder.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.db.DatabaseHandler;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.util.ArrayList;
import java.util.List;

import static com.freshorders.freshorder.utils.Constants.MENU_ACKNOWLEDGE_CODE;
import static com.freshorders.freshorder.utils.Constants.MENU_ADD_MERCHANT_CODE;
import static com.freshorders.freshorder.utils.Constants.MENU_CLOSING_STOCK_AUDIT_CODE;
import static com.freshorders.freshorder.utils.Constants.MENU_CLOSING_STOCK_CODE;
import static com.freshorders.freshorder.utils.Constants.MENU_CREATE_ORDER_CODE;
import static com.freshorders.freshorder.utils.Constants.MENU_DISTANCE_CALCULATION_CODE;
import static com.freshorders.freshorder.utils.Constants.MENU_DISTRIBUTOR_STOCK_CODE;
import static com.freshorders.freshorder.utils.Constants.MENU_DOWNLOAD_SCHEME_CODE;
import static com.freshorders.freshorder.utils.Constants.MENU_MY_CLIENT_VISIT_CODE;
import static com.freshorders.freshorder.utils.Constants.MENU_MY_DAY_PLAN_CODE;
import static com.freshorders.freshorder.utils.Constants.MENU_MY_ORDERS_CODE;
import static com.freshorders.freshorder.utils.Constants.MENU_MY_SALES_CODE;
import static com.freshorders.freshorder.utils.Constants.MENU_PAYMENT_COLLECTION_CODE;
import static com.freshorders.freshorder.utils.Constants.MENU_PENDING_DATA_CODE;
import static com.freshorders.freshorder.utils.Constants.MENU_PKD_DATA_CAPTURE_CODE;
import static com.freshorders.freshorder.utils.Constants.MENU_POST_NOTES_CODE;
import static com.freshorders.freshorder.utils.Constants.MENU_PROFILE_CODE;
import static com.freshorders.freshorder.utils.Constants.MENU_REFRESH_CODE;
import static com.freshorders.freshorder.utils.Constants.MENU_SIGN_OUT_CODE;
import static com.freshorders.freshorder.utils.Constants.MENU_SURVEY_USER_CODE;

public class MenuSetting {


    private Context mContext;

    private void menuSetting(Cursor menuCur) throws Exception{
        menuCur.moveToFirst();
        for(int i = 0; i < menuCur.getCount(); i++){
            int currentVisibility = menuCur.getInt(menuCur.getColumnIndex(DatabaseHandler.KEY_MENU_IS_VISIBLE));
            String currentMenuCode = menuCur.getString(menuCur.getColumnIndex(DatabaseHandler.KEY_MENU_CODE));
            if(currentVisibility == 1){
                menuEnable(currentMenuCode);
            }
            menuCur.moveToNext();
        }
    }

    private void menuEnable(String code){

        MyApplication app = MyApplication.getInstance();

        switch (code){

            case MENU_PROFILE_CODE:
                app.setProfile(true);
                break;
            case MENU_MY_DAY_PLAN_CODE:
                app.setMyDayPlan(true);
                break;
            case MENU_MY_ORDERS_CODE:
                app.setMyOrders(true);
                break;
            case MENU_ADD_MERCHANT_CODE:
                app.setAddMerchant(true);
                break;
            case MENU_CREATE_ORDER_CODE:
                app.setCreateOrder(true);
                break;
            case MENU_MY_SALES_CODE:
                app.setMySales(true);
                break;
            case MENU_POST_NOTES_CODE:
                app.setPostNotes(true);
                break;
            case MENU_MY_CLIENT_VISIT_CODE:
                app.setMyClientVisit(true);
                break;
            case MENU_ACKNOWLEDGE_CODE:
                app.setAcknowledge(true);
                break;
            case MENU_PAYMENT_COLLECTION_CODE:
                app.setPaymentCollection(true);
                break;
            case MENU_PKD_DATA_CAPTURE_CODE:
                app.setPkdDataCapture(true);
                break;
            case MENU_DISTRIBUTOR_STOCK_CODE:
                app.setDistributorStock(true);
                break;
            case MENU_SURVEY_USER_CODE:
                app.setSurveyUser(true);
                break;
            case MENU_DOWNLOAD_SCHEME_CODE:
                app.setDownLoadScheme(true);
                break;
            case MENU_DISTANCE_CALCULATION_CODE:
                app.setDistanceCalculation(true);
                break;
            case MENU_REFRESH_CODE:
                app.setRefresh(true);
                break;
            case MENU_SIGN_OUT_CODE:
                app.setLogout(true);
                break;
            case MENU_CLOSING_STOCK_CODE:
                app.setClosingStock(true);
                break;
            case MENU_CLOSING_STOCK_AUDIT_CODE:
                app.setClosingStockAudit(true);
                break;
            case MENU_PENDING_DATA_CODE:
                app.setPendingData(true);
                break;

        }
    }

    public void loadMenu(DatabaseHandler databaseHandler) throws Exception {
        if(databaseHandler != null){
            Cursor cursor = databaseHandler.getMenuItems();
            if(cursor != null && cursor.getCount() > 0){
                menuSetting(cursor);
            }
        }
    }

    public interface MenuSettingFetchingListener{
        void complete();
    }

    public static class LoadMenuAsyncTask extends AsyncTask<Void, Void, JSONObject> {

        private DatabaseHandler databaseHandler;
        private JsonServiceHandler jsonServiceHandler;
        private MenuSetting.MenuSettingFetchingListener listener;

        public LoadMenuAsyncTask(DatabaseHandler databaseHandler, JsonServiceHandler jsonServiceHandler,
                                 MenuSettingFetchingListener listener) {
            this.databaseHandler = databaseHandler;
            this.jsonServiceHandler = jsonServiceHandler;
            this.listener = listener;
        }

        @Override
        protected JSONObject doInBackground(Void... voids) {
            try{
                return jsonServiceHandler.ServiceDataModifiedGet();
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(JSONObject result) {

            if(result != null && result.length() > 0){
                try {
                    String strStatus = result.getString("status");
                    android.util.Log.e("return status", strStatus);
                    String strMsg = result.getString("message");
                    android.util.Log.e("return message", strMsg);
                    if (strStatus.equals("true")) {
                        JSONArray resultArray = result.getJSONArray("data");
                        JSONObject obj = resultArray.getJSONObject(0);
                        String refKey = obj.getString("refkey");
                        if(refKey.equalsIgnoreCase(Constants.MOBILE_MENUS)){
                            String value = obj.getString("refvalue");
                            JSONArray array = new JSONArray(value);
                            ///JSONArray array = obj.getJSONArray("refvalue");
                            List<ContentValues> valuesList = new ArrayList<>();
                            for(int i = 0; i < array.length(); i++){
                                JSONObject menuObj = array.getJSONObject(i);
                                String strMenuName = menuObj.getString("screenname");
                                String visible = menuObj.getString("isactive");
                                String code = menuObj.getString("screencode");
                                boolean isShow = visible.equalsIgnoreCase("true");
                                ContentValues values = new ContentValues();
                                values.put(DatabaseHandler.KEY_MENU_NAME,strMenuName);
                                values.put(DatabaseHandler.KEY_MENU_IS_VISIBLE,isShow);
                                values.put(DatabaseHandler.KEY_MENU_CODE,code);
                                valuesList.add(values);
                            }
                            if(valuesList.size() > 0){
                                databaseHandler.loadFullMenu(valuesList);
                                new MenuSetting().loadMenu(databaseHandler);
                            }
                        }else {
                            android.util.Log.e("return refkey", ".....MOBILE_MENUS...not found");
                        }
                    }else {
                        android.util.Log.e("return status", ".....false");
                    }
                    listener.complete();
                }catch (Exception e){
                    e.printStackTrace();
                    listener.complete();
                }
            }else {
                listener.complete();
            }
        }
    }
}
