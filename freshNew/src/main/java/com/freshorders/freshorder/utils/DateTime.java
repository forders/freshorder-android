package com.freshorders.freshorder.utils;

import android.content.Context;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateTime {
    Context context;

    public static Locale getLocale() {
        return locale;
    }

    public static void setLocale(Locale locale) {
        DateTime.locale = locale;
        DATE_TIME_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss", locale);
    }

    public static Locale locale;


    public static SimpleDateFormat DATE_TIME_FORMAT ;
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMdd", getLocale());

    public static Date fromString(String source, String pattern) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern, getLocale());
        return sdf.parse(source);
    }

    public static Date parseDateTime(String dateStr) {
        try {
            return DATE_TIME_FORMAT.parse(dateStr);
        } catch (ParseException e) {
            return null;
        }
    }

    public static Date parseDate(String dateStr) {
        try {
            return DATE_FORMAT.parse(dateStr);
        } catch (ParseException e) {
            return null;
        }
    }

    public static String formatDateTime(Date date) {
        if (date == null) {
            return "";
        } else {
            return DATE_TIME_FORMAT.format(date);
        }
    }

    public static String formatDate(Date date) {
        if (date == null) {
            return "";
        } else {
            return DATE_FORMAT.format(date);
        }
    }

}
