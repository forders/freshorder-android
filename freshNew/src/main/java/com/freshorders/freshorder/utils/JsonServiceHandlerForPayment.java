package com.freshorders.freshorder.utils;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

public class JsonServiceHandlerForPayment {
    String URL = null;
    static InputStream is = null;
    static JSONArray jary = null;
    static JSONObject Jobj = null;
    String json = "";
    String parameter = "";

    Context context;

    public JsonServiceHandlerForPayment(String url, Context context) {
        this.URL = url;
        this.context = context;
    }

    public JsonServiceHandlerForPayment(String url, String parameter, Context context) {
        this.URL = url;
        this.parameter = parameter;
        this.context = context;

    }

    public JSONObject ServiceData() {
        is = null;
        jary = new JSONArray();
        Jobj = new JSONObject();
        json = "";
        try {
            Log.e("param service handler", parameter);
            Log.e("URL service handler", URL);
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(URL);
            StringEntity input = new StringEntity(parameter);

            //client api's
          httpPost.addHeader("Content-Type","application/json");
			httpPost.addHeader("X-Api-Key","c962afbb191ab6777711f339bd5a51e6");//old - 682cf8c6deb39a035cd541bd88b0ca73
			httpPost.addHeader("X-Auth-Token","00cc93d48906bc47f79b13e69c123c11");//old - 0a205c35178bb8b1509fc18ee81125b5

            ///test keys        b883990135d499a178c8f2d554be4ea2
           /*httpPost.addHeader("Content-Type", "application/json");
            httpPost.addHeader("X-Api-Key", "50a8a33a495b73a3e8160290d1837d30");
            httpPost.addHeader("X-Auth-Token", "84034e7ffa3ef84e05f103eda534e4d7");*/


            httpPost.setEntity(input);
            //  Log.e("httpPost",""+httpPost.getFirstHeader("X-Auth-TokenX-Api-Key"));

            HttpResponse httpResponse = httpClient.execute(httpPost);

            Log.e("httpResponse", "res" + httpResponse);
            int responseCode = httpResponse.getStatusLine().getStatusCode();

            Log.e("asdss", String.valueOf(httpResponse.getStatusLine().getStatusCode()));
            if (responseCode == 200) {
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();
                Log.e("inputStream", is.toString());
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                Log.e("reader", reader.toString());
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                json = sb.toString();
                Log.e("json", json);
                System.out.println("----" + URL + "JSON--->" + json);
                //jary=new JSONArray(json);
                Jobj = new JSONObject(json);

            } else {
                if (responseCode == 404) {
                    ((Activity) context).runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast toast = Toast.makeText(context,
                                    "Requested resource not found",
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();

                        }
                    });

                } else if (responseCode == 500) {
                    ((Activity) context).runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast toast = Toast.makeText(context,
                                    "Something went wrong at server end",
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }

                    });
                } else {
                    ((Activity) context).runOnUiThread(new Runnable() {

                        @Override
                        public void run() {

                            Toast toast = Toast.makeText(context,
                                    "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet]",
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }

                    });
                }
            }


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            Log.e("JSON Parser Service Handler", "Error parsing data " + e.toString());
            //Toast.makeText(SyncDetail.context, "Data Not Inserted", Toast.LENGTH_SHORT ).show();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        return Jobj;
        //return jary;
    }

    public JSONObject ServiceDataNoHeader() {
        is = null;
        jary = new JSONArray();
        Jobj = new JSONObject();
        json = "";
        try {
            Log.e("param service handler", parameter);
            Log.e("URL service handler", URL);
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(URL);
            StringEntity input = new StringEntity(parameter);
            //      httpPost.set
            //     httpPost.addHeader("Content-Type","application/json");
            //    httpPost.addHeader("X-Api-Key","37c8ed44dbb901e5e327e1117a143a47");
            //    httpPost.addHeader("X-Auth-Token","b4b19668b1f4c32bcb525e7f64ad47be");

            ///test keys
            //	httpPost.addHeader("Content-Type","application/json");
            //	httpPost.addHeader("X-Api-Key","682cf8c6deb39a035cd541bd88b0ca73");
            //	httpPost.addHeader("X-Auth-Token","0a205c35178bb8b1509fc18ee81125b5");


            //	httpPost.setEntity(input);
            //	Log.e("httpPost",""+httpPost.getFirstHeader("X-Auth-TokenX-Api-Key"));

            HttpResponse httpResponse = httpClient.execute(httpPost);

            Log.e("httpResponse", "res" + httpResponse);
            int responseCode = httpResponse.getStatusLine().getStatusCode();

            Log.e("asdss", String.valueOf(httpResponse.getStatusLine().getStatusCode()));
            if (responseCode == 200) {
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();
                Log.e("inputStream", is.toString());
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
                Log.e("reader", reader.toString());
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                json = sb.toString();
                Log.e("json", json);
                System.out.println("----" + URL + "JSON--->" + json);
                //jary=new JSONArray(json);
                Jobj = new JSONObject(json);

            } else {
                if (responseCode == 404) {
                    ((Activity) context).runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast toast = Toast.makeText(context,
                                    "Requested resource not found",
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();

                        }
                    });

                } else if (responseCode == 500) {
                    ((Activity) context).runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            Toast toast = Toast.makeText(context,
                                    "Something went wrong at server end",
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }

                    });
                } else {
                    ((Activity) context).runOnUiThread(new Runnable() {

                        @Override
                        public void run() {

                            Toast toast = Toast.makeText(context,
                                    "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet]",
                                    Toast.LENGTH_SHORT);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }

                    });
                }
            }


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            Log.e("JSON Parser Service Handler", "Error parsing data " + e.toString());
            //Toast.makeText(SyncDetail.context, "Data Not Inserted", Toast.LENGTH_SHORT ).show();
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }
        return Jobj;
        //return jary;
    }


}