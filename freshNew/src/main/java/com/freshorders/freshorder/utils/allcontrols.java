package com.freshorders.freshorder.utils;

import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.provider.Settings;
import androidx.fragment.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;
import com.codetroopers.betterpickers.calendardatepicker.MonthAdapter;
import com.codetroopers.betterpickers.radialtimepicker.RadialTimePickerDialogFragment;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


/**
 * Created by Soham on 5/10/2016.
 */
public class allcontrols {

    public static void setdatepicker (final EditText e1, final Context context)
    {
        e1.setFocusable(false);
        e1.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment()
                        .setOnDateSetListener(new CalendarDatePickerDialogFragment.OnDateSetListener() {
                            @Override
                            public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {


                                int month = (monthOfYear+1);

                                String month_str= month<10?"0"+month:""+month;
                                String day_str= dayOfMonth<10?"0"+dayOfMonth:""+dayOfMonth;

                                //e1.setText(year + "-" +month_str + "-" + day_str);

                                e1.setText(day_str + "-" +month_str + "-" + year);
                            }
                        })
                        .setFirstDayOfWeek(Calendar.SUNDAY)

                        .setDateRange(null, null)
                        .setDoneText("Yes")
                        .setCancelText("No")
                        .setThemeDark();

                if(e1.getText().toString().trim().equals(""))
                {
                    cdp.setPreselectedDate(Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH) , Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
                }
                else
                {
                    String datestring = e1.getText().toString().trim();
                    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
                    try {
                        Date date = format.parse(datestring);

                        Calendar c1 = Calendar.getInstance();
                        c1.setTime(date);
                        cdp.setPreselectedDate(c1.get(Calendar.YEAR), c1.get(Calendar.MONTH) , c1.get(Calendar.DAY_OF_MONTH));
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }
                FragmentActivity activity = (FragmentActivity) context;
                cdp.show(activity.getSupportFragmentManager(), "date");

            }
        });
    }
    public static void settimepicker (final EditText e1, final Context context)
    {
        int hour= Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        int min= Calendar.getInstance().get(Calendar.MINUTE);
        if(!e1.getText().toString().trim().equals(""))
        {
            String time = e1.getText().toString().trim();
            String t[]=time.split(":");
            hour= Integer.parseInt(t[0]);
            min= Integer.parseInt( t[1]);
        }
        e1.setFocusable(false);
        final int finalHour = hour;
        final int finalMin = min;
        e1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadialTimePickerDialogFragment cdp = new RadialTimePickerDialogFragment()
                        .setOnTimeSetListener(new RadialTimePickerDialogFragment.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(RadialTimePickerDialogFragment dialog, int hourOfDay, int minute) {
                                e1.setText(hourOfDay+":"+minute);
                            }
                        })
                        .setStartTime(finalHour, finalMin)
                        .setDoneText("Yes")
                        .setCancelText("No")
                        .setThemeDark();

                FragmentActivity activity = (FragmentActivity) context;
                cdp.show(activity.getSupportFragmentManager(), "date");

            }
        });
    }

    public static void setdatepicker_validation (final EditText e1, final Context context, final MonthAdapter.CalendarDay minday, final MonthAdapter.CalendarDay maxday)
    {

        e1.setFocusable(false);
//       final MonthAdapter.CalendarDay minday=new MonthAdapter.CalendarDay(Calendar.getInstance().get(Calendar.YEAR)-99,0,1);
//        final MonthAdapter.CalendarDay maxday=new MonthAdapter.CalendarDay(Calendar.getInstance().get(Calendar.YEAR)-11,11,31);

        e1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment()
                        .setOnDateSetListener(new CalendarDatePickerDialogFragment.OnDateSetListener() {
                            @Override
                            public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {

                                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                                e1.setText(dayOfMonth + "/" + (monthOfYear+1) + "/" + year);

                            }
                        })
                        .setFirstDayOfWeek(Calendar.SUNDAY)

                        .setDateRange(null, null)
                        .setDoneText("Yes")
                        .setCancelText("No")

                        .setDateRange(minday,maxday)
                        .setThemeDark();


                if(e1.getText().toString().equals(""))
                {
                    cdp.setPreselectedDate((Calendar.getInstance().get(Calendar.YEAR)), Calendar.getInstance().get(Calendar.MONTH) , Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
                }
                else
                {
                    String datestring = e1.getText().toString();
                    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                    try {
                        Date date = format.parse(datestring);

                        Calendar c1 = Calendar.getInstance();
                        c1.setTime(date);
                        cdp.setPreselectedDate(c1.get(Calendar.YEAR), c1.get(Calendar.MONTH) , c1.get(Calendar.DAY_OF_MONTH));
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }
                FragmentActivity activity = (FragmentActivity) context;
                cdp.show(activity.getSupportFragmentManager(), "date");

            }
        });
    }

    public static void setyearspinner(final EditText e1, final Context c1)
    {
        e1.setFocusable(false);
        e1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                AlertDialog alert;
                AlertDialog.Builder alertbuilder = new AlertDialog.Builder(c1);


                final ArrayList<String> list = new ArrayList<String>();

                list.add("2000-01");
                list.add("2001-02");
                list.add("2002-03");
                list.add("2003-04");
                list.add("2004-05");
                list.add("2005-06");
                list.add("2006-07");
                list.add("2007-08");
                list.add("2008-09");
                list.add("2009-10");
                list.add("2010-11");
                list.add("2011-12");
                list.add("2012-13");
                list.add("2013-14");
                list.add("2014-15");
                String[] listarr = new String[list.size()];
                listarr = list.toArray(listarr);
                alertbuilder.setSingleChoiceItems(listarr, list.indexOf(e1.getText().toString().trim()), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        e1.setText(list.get(arg1).toString());

                        arg0.dismiss();
                    }
                });


                alertbuilder.setTitle("Select Year");


                alert = alertbuilder.create();

                alert.getWindow().setLayout(600, 400);


                alert.show();

            }
        });


    }



    public static void setquarterspinner(final EditText e1, final Context c1)
    {
        e1.setFocusable(false);
        e1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                AlertDialog alert;
                AlertDialog.Builder alertbuilder = new AlertDialog.Builder(c1);


                final ArrayList<String> list = new ArrayList<String>();

                list.add("March");
                list.add("June");
                list.add("September");
                list.add("December");

                String[] listarr = new String[list.size()];
                listarr = list.toArray(listarr);
                alertbuilder.setSingleChoiceItems(listarr, list.indexOf(e1.getText().toString().trim()), new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        e1.setText(list.get(arg1).toString());

                        arg0.dismiss();
                    }
                });


                alertbuilder.setTitle("Select Quarter");


                alert = alertbuilder.create();

                alert.getWindow().setLayout(600, 400);


                alert.show();

            }
        });


    }


    public static final String md5(final String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest
                    .getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            Log.d("error",e.toString());
        }
        return "";
    }
    public static String getdeviceid(ContentResolver rs)
    {
        String android_id = Settings.Secure.getString(rs, Settings.Secure.ANDROID_ID);
        String deviceId = md5(android_id).toUpperCase();
        return deviceId;
    }




}