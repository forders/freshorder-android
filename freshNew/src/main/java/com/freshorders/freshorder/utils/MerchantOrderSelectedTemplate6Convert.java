package com.freshorders.freshorder.utils;

import android.content.Context;
import android.util.Log;

import com.freshorders.freshorder.domain.MerchantOrderDetailDomain;
import com.freshorders.freshorder.domain.MerchantOrderDomainSelected;
import com.freshorders.freshorder.model.Template3Model;
import com.freshorders.freshorder.model.Template6Model;

import java.util.ArrayList;
import java.util.Map;

import static com.freshorders.freshorder.utils.Constants.EMPTY;

public class MerchantOrderSelectedTemplate6Convert {

    private Context mContext;

    public MerchantOrderSelectedTemplate6Convert(Context mContext) {
        this.mContext = mContext;
    }

    public ArrayList<MerchantOrderDomainSelected>
        getMerchantSelectedFromTemplate6(Map<Integer, Template6Model> selectedOrders, ArrayList<MerchantOrderDetailDomain> availItems){

        ArrayList<MerchantOrderDomainSelected> out = new ArrayList<>();
        Log.e("selectedOrders","....................."+selectedOrders);
        for(Map.Entry entry: selectedOrders.entrySet()) {
            MerchantOrderDomainSelected currentSelected = new MerchantOrderDomainSelected();
            int position = (int) entry.getKey();
            MerchantOrderDetailDomain currentAvailItem = availItems.get(position);
            Template6Model template6Model = (Template6Model) entry.getValue();
            String quantity = template6Model.getQuantity();
            String proCode = template6Model.getProductCode();

            currentSelected.setqty(quantity);
            currentSelected.setprice(EMPTY);////////////
            currentSelected.setprodcode(proCode);
            currentSelected.setOutstock(currentAvailItem.getCurrStock());
            currentSelected.setCurrStock(currentAvailItem.getCurrStock()); //////////////////  saravan and nambi said crrent and out stock is same
            currentSelected.setDate("");//////////////////////////////////////
            currentSelected.setusrdlrid(currentAvailItem.getusrdlrid());
            currentSelected.setprodid(currentAvailItem.getprodid());
            currentSelected.setprodname(currentAvailItem.getprodname());
            currentSelected.setUserid(currentAvailItem.getDuserid()); //// saravanan told to set this id (because in web order not showing)
            currentSelected.setCompanyname(currentAvailItem.getCompanyname());
            currentSelected.setFreeQty(currentAvailItem.getFreeQty());
            currentSelected.setserialno(currentAvailItem.getserialno());
            currentSelected.setindex(currentAvailItem.getindex());
            currentSelected.setptax(currentAvailItem.getprodtax());
            currentSelected.setpvalue(0);
            currentSelected.setpaymentType(currentAvailItem.getPaymentType());
            currentSelected.setpaymentType(currentAvailItem.getPaymentType());
            currentSelected.setBatchno(currentAvailItem.getBatchno());
            currentSelected.setExpdate(currentAvailItem.getExpdate());
            currentSelected.setMgfDate(currentAvailItem.getMgfDate());
            currentSelected.setUOM(currentAvailItem.getUOM());
            currentSelected.setorderfreeuom(""); ////////
            currentSelected.setorderuom(currentAvailItem.getSlectedUOM());
            currentSelected.setselectedfreeUOM(currentAvailItem.getselectedfreeUOM());
            currentSelected.setUnitGram(currentAvailItem.getUnitGram());
            currentSelected.setUnitPerUom(currentAvailItem.getUnitPerUom());
            currentSelected.setSelecetdQtyUomUnit(currentAvailItem.getSelectedQtyUomUnit());
            currentSelected.setSelecetdFreeQtyUomUnit(currentAvailItem.getSelectedFreeQtyUomUnit());
            currentSelected.setDeliverymode(currentAvailItem.getDeliverymode());
            /////currentSelected.setDeliverydate(currentAvailItem.getDeliverydate());
            currentSelected.setDeliverydate("");
            currentSelected.setDeliverytime(currentAvailItem.getDeliverytime());
            out.add(currentSelected);

        }

        return out;
    }
}
