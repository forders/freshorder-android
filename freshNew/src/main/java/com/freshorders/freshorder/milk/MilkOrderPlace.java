package com.freshorders.freshorder.milk;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.MerchantOrderDetailDomain;
import com.freshorders.freshorder.model.Template6Model;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.ui.CreateOrderActivity;
import com.freshorders.freshorder.ui.SalesManOrderCheckoutActivity;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

public class MilkOrderPlace {

    private String[] orderdtid, userid, prodid, qty, date, status, freeQty, paymentType, companyname, prodCode, prodName,
            price, ptax, pvalue, fread, qread, batchno, expdate, mfgdate, outstock,
            uom, ordruom, orderfreeuom,unitgram,qtyUomUnit,freeqtyUomUnit,deliverydate,deliverytime,deliverymode, currstock;

    private double[] fval;

    private String Orderno = "0" , selected_dealer;
    private static String OfflineOrderNo = "0";

    public SQLiteDatabase db;
    private JSONArray resultSet;

    private MilkOrderPlace.MilkOrderPlaceListener listener;
    private Context mContext;
    private DatabaseHandler databaseHandler ;
    private Map<Integer, Template6Model> merchantOrderDetailList;
    private ArrayList<MerchantOrderDetailDomain> arraylistMerchantOrderDetailList;
    private int orderSize = 0;
    private double total = 0;
    private String aprxordval = "0";

    public MilkOrderPlace(Context mContext,
                          DatabaseHandler databaseHandler,
                          Map<Integer, Template6Model> merchantOrderDetailList,
                          ArrayList<MerchantOrderDetailDomain> arraylistMerchantOrderDetailList,
                          double total,
                          MilkOrderPlaceListener listener) {
        this.listener = listener;
        this.mContext = mContext;
        this.databaseHandler = databaseHandler;
        this.merchantOrderDetailList = merchantOrderDetailList;
        this.arraylistMerchantOrderDetailList = arraylistMerchantOrderDetailList;
        orderSize = merchantOrderDetailList.size();
        this.total = total;
    }

    public interface MilkOrderPlaceListener{
        void onOrderPlaced(String msg);
        void onOrderFailed(String errorMSG);
    }


    public void milkDataPack(){

        if(initializeAndSetValues()){
            if(saveOfflineHeader()){
                try {
                    JSONObject jsonObject ;
                    if(netCheck()) {
                        if(formJSON()){
                            jsonObject = resultSet.getJSONObject(0);
                            JsonServiceHandler JsonServiceHandler = new JsonServiceHandler(Utils.strsavemerchantOderdetail, jsonObject.toString(), mContext.getApplicationContext());
                            new MilkOrderPlace.MilkOrderAsyncTask(JsonServiceHandler, jsonObject, listener, databaseHandler).execute();
                        }else {
                            listener.onOrderPlaced("Online Order Store Failed.. In Mobile saved Successfully");
                        }
                    }else {
                        listener.onOrderPlaced("No Internet Connection Order Placed In Mobile Successfully");
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    listener.onOrderFailed("Went Wrong :" + e.getMessage());
                }
            }
        }
    }

    private static class MilkOrderAsyncTask extends AsyncTask<Void, Void, JSONObject>{

        private JsonServiceHandler jsonServiceHandler;
        private JSONObject jsonObject;
        private MilkOrderPlace.MilkOrderPlaceListener listener;
        private DatabaseHandler databaseHandler;

        public MilkOrderAsyncTask(JsonServiceHandler jsonServiceHandler,
                                  JSONObject jsonObject, MilkOrderPlaceListener listener,
                                  DatabaseHandler databaseHandler) {
            this.jsonServiceHandler = jsonServiceHandler;
            this.jsonObject = jsonObject;
            this.listener = listener;
            this.databaseHandler = databaseHandler;
        }

        @Override
        protected JSONObject doInBackground(Void... voids) {


            try {
                OfflineOrderNo = jsonObject.getString("oflnordid");
                System.out.println("Before Change: OfflineOrderNo::: "+OfflineOrderNo);
                System.out.println("Before Change: "+jsonObject);
                JSONObject jsonObjMerchant=jsonObject.getJSONObject("merchant");
                System.out.println("Before  Change jsonObjMerchant: "+jsonObjMerchant);
                jsonObjMerchant.put("createdby",Constants.USER_ID);
                jsonObjMerchant.put("customdata", "");
                System.out.println("After Change jsonObjMerchant: "+jsonObjMerchant);
                jsonObject.put("merchant",jsonObjMerchant);
                System.out.println("After Change: "+jsonObject);

                return jsonServiceHandler.ServiceData();

            } catch (JSONException e) {
                e.printStackTrace();
                listener.onOrderFailed("Went Wrong :" + e.getMessage());
                return null;
            }
        }

        @Override
        protected void onPostExecute(JSONObject resultJSON) {

            try{
                String strStatus;
                String strMsg;
                String strOrderId;
                if(resultJSON != null && resultJSON.length() > 0) {
                    strStatus = resultJSON.getString("status");
                    Log.e("return status", strStatus);
                    strMsg = resultJSON.getString("message");
                    Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {
                        strOrderId = resultJSON.getJSONObject("data").getString("ordid");
                        Log.e("strOrderId", strOrderId);
                        String muserid = resultJSON.getJSONObject("data").getString("muserid");
                        Log.e("muserid", muserid);
                        String updatedFlag = jsonObject.getJSONObject("merchant").getString("cflag"); ////
                        Log.e("updatedFlag", updatedFlag);
                        ContentValues contentValues1 = new ContentValues();
                        contentValues1.put(DatabaseHandler.KEY_Mcompanyname, jsonObject.getString("mname"));
                        contentValues1.put(DatabaseHandler.KEY_Muserid, muserid);
                        contentValues1.put(DatabaseHandler.KEY_MFlag, updatedFlag);
                        databaseHandler.updateMerchant(contentValues1);
                        Log.e("updateMerchant", String.valueOf(contentValues1));

                        /// this require part need to change Order status if its success
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(DatabaseHandler.KEY_orderno, OfflineOrderNo);
                        contentValues.put(DatabaseHandler.KEY_pushStatus, "Success");
                        contentValues.put(DatabaseHandler.KEY_onlineOrderNo, strOrderId);
                        databaseHandler.updateOrderHeader(contentValues);

                        Cursor curs;
                        curs = databaseHandler.getOrderHeaderStatus(OfflineOrderNo);
                        Log.e("HeaderStatusCount", String.valueOf(curs.getCount()));

                        if (curs != null && curs.getCount() > 0) {

                            if (curs.moveToFirst()) {
                                String Pushstatus = curs.getString(0);
                                Log.e("Pushstatus", curs.getString(0));
                                if(Pushstatus.equalsIgnoreCase("Success")){
                                    listener.onOrderPlaced("Order Placed Successfully");
                                }else {
                                    listener.onOrderPlaced("Order Placed And Status Update Failed");
                                }
                            }
                        }
                    }

                }else {
                    listener.onOrderFailed("Went Worng : Empty Result From Server");
                }
            }catch (Exception e){
                e.printStackTrace();
                listener.onOrderFailed("Went Worng " + e.getMessage());
            }

        }
    }

    private boolean initializeAndSetValues() {
        try{
            orderfreeuom = new String[orderSize];
            ordruom = new String[orderSize];
            uom = new String[orderSize];
            outstock = new String[orderSize];
            batchno = new String[orderSize];
            expdate = new String[orderSize];
            mfgdate = new String[orderSize];
            prodCode = new String[orderSize];
            prodName = new String[orderSize];
            companyname = new String[orderSize];
            userid = new String[orderSize];
            prodid = new String[orderSize];
            qty = new String[orderSize];
            freeQty = new String[orderSize];
            price = new String[orderSize];
            ptax = new String[orderSize];
            unitgram = new String[orderSize];
            qtyUomUnit = new String[orderSize];
            freeqtyUomUnit = new String[orderSize];
            deliverydate = new String[orderSize];
            deliverymode = new String[orderSize];
            deliverytime = new String[orderSize];
            currstock = new String[orderSize];
            fval = new double[orderSize];
            qread = new String[orderSize];
            // this operation is doing order the items how they comes in list
            int i = 0;
            for (int index = 0; index < arraylistMerchantOrderDetailList.size(); index++) {
                String currentProductCode = arraylistMerchantOrderDetailList.get(index).getprodcode();
                boolean isFound = false;
                String qty_value = "";
                for (Map.Entry entry : merchantOrderDetailList.entrySet()) {
                    Template6Model temp = (Template6Model) entry.getValue();
                    String pCode = temp.getProductCode();
                    if (pCode.equals(currentProductCode)) {
                        isFound = true;
                        qty_value = temp.getQuantity();
                        if(pCode.equals(Constants.MILK)){
                            ///////////////////////// round two decimal place
                            Double netPrice = (Math.round(total * 100.0) / 100.0);
                            String sNetPrice = String.format(Locale.getDefault(), "%.2f", netPrice);
                            total = Double.parseDouble(sNetPrice);
                            fval[i] = total ;
                            aprxordval = sNetPrice;
                        }else {
                            String sNetPrice = String.format(Locale.getDefault(), "%.2f", 0.00);
                            fval[i] = Double.parseDouble(sNetPrice);
                        }
                    }
                }
                if (!isFound) {
                    continue;
                }
                userid[i] = arraylistMerchantOrderDetailList.get(index).getDuserid();////////////////////////////
                prodid[i] = arraylistMerchantOrderDetailList.get(index).getprodid();
                qty[i] = qty_value;//////////////////////////////////////////////////
                freeQty[i] = arraylistMerchantOrderDetailList.get(index).getFreeQty();
                companyname[i] = arraylistMerchantOrderDetailList.get(index).getCompanyname();
                prodCode[i] = arraylistMerchantOrderDetailList.get(index).getprodcode();
                prodName[i] = arraylistMerchantOrderDetailList.get(index).getprodname();
                price[i] = arraylistMerchantOrderDetailList.get(index).getprodprice();////////////
                ptax[i] = arraylistMerchantOrderDetailList.get(index).getprodtax();
                batchno[i] = arraylistMerchantOrderDetailList.get(index).getBatchno();
                expdate[i] = arraylistMerchantOrderDetailList.get(index).getExpdate();
                mfgdate[i] = arraylistMerchantOrderDetailList.get(index).getMgfDate();
                outstock[i] = arraylistMerchantOrderDetailList.get(index).getprodtax();
                uom[i] = arraylistMerchantOrderDetailList.get(index).getUOM();
                ordruom[i] = arraylistMerchantOrderDetailList.get(index).getSlectedUOM(); ///////// default value is set because no selection from user
                orderfreeuom[i] = Constants.EMPTY;
                unitgram[i] = arraylistMerchantOrderDetailList.get(index).getUnitGram();
                qtyUomUnit[i] = Constants.EMPTY;
                freeqtyUomUnit[i] = Constants.EMPTY;
                deliverymode[i] = Constants.EMPTY;
                deliverytime[i] = Constants.EMPTY;
                deliverydate[i] = Constants.EMPTY;
                currstock[i] = Constants.EMPTY;

                Log.e("ordruom", ordruom[i]);
                Log.e("orderfreeuom", orderfreeuom[i]);
                /// need to increase
                i++;


            }
            return true;
        }catch (Exception e){
            e.printStackTrace();
            listener.onOrderFailed("Went Wrong : " + e.getMessage());
            return false;
        }
    }

    private boolean saveOfflineHeader() {

        try{
            String inputPattern = "ddMMyyhhmmss";
            String inputFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date());
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.getDefault());
            String dt = inputFormat.format(new Date());
            String iscash = "";
            Double geolat = 0d;
            Double geolog = 0d;
            String endtime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new java.util.Date());

            databaseHandler.addorderheader(Constants.USER_ID, Constants.SalesMerchant_Id, Constants.Merchantname,
                    iscash, "Pending", inputFormat1, "0",
                    Constants.USER_ID + dt, aprxordval, geolat, geolog, Constants.startTime, endtime, "");  /// add aprxordval value

            Cursor curs = databaseHandler.getorderheader();

            if (curs != null && curs.getCount() > 0) {
                if (curs.moveToLast()) {
                    String orderNo = curs.getString(0);
                    String pushstauts = curs.getString(4);
                    String date = curs.getString(5);
                    Log.e("onlineorderno", curs.getString(6));
                    Log.e("orderno", orderNo);
                    Log.e("Hdate", inputFormat1);
                    return saveOfflineDetail(orderNo); //////////// return you to check result if Exception Thrown or not
                }
            }
        return true;
        }catch (Exception e){
            e.printStackTrace();
            listener.onOrderFailed("Went Wrong :" + e.getMessage());
            return false;
        }
    }

    private boolean saveOfflineDetail(String orderno) {

        try{
            String flag = "NF";
            String imagepath = "0";
            String productStatus = "Pending";
            String deldate="";
            Double defaultuomTotal = 0.00;

            for(int i = 0; i < merchantOrderDetailList.size(); i++){

                qread[i] = Constants.USER_ID + "," + Constants.SalesMerchant_Id + "," + userid[i];

                databaseHandler.addorderdetail(orderno, userid[i], companyname[i], prodid[i], qty[i], flag, productStatus,
                        String.valueOf(i), prodCode[i], prodName[i],deldate, price[i], ptax[i],
                        fval[i], qread[i], "", batchno[i], mfgdate[i], expdate[i],
                        imagepath, uom[i], ordruom[i],defaultuomTotal,deliverytime[i],deliverymode[i], currstock[i]);

            }

            Cursor curs = databaseHandler.getorderheader();
            if (curs != null && curs.getCount() > 0) {
                if (curs.moveToLast()) {
                    Orderno = curs.getString(0);
                }
            }
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public boolean formJSON(){
        try {
            resultSet = new JSONArray();

            Cursor curs = databaseHandler.getorderheader();
            Log.e("HeaderCount", String.valueOf(curs.getCount()));
            curs.moveToFirst();

            while (!curs.isAfterLast()) {

                int totalColumn = curs.getColumnCount();
                JSONObject rowObject = new JSONObject();

                for (int i = 0; i < totalColumn; i++) {
                    if (curs.getColumnName(i) != null) {

                        try {

                            if (curs.getString(i) != null) {
                                if (!curs.getString(i).equals("New User")) {
                                    Log.d("TAG_NAME", curs.getString(i));
                                    rowObject.put(curs.getColumnName(i), curs.getString(i));
                                }

                            } else {
                                if (!curs.getColumnName(i).equals("muserid")) {
                                    rowObject.put(curs.getColumnName(i), "");
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d("TAG_NAME", e.getMessage());
                            listener.onOrderFailed("Went Worng :" + e.getMessage());
                            return false;
                        }
                    }

                }
                try {
                    //Kumaravel 13-07-2019
                    if(Constants.distributorname == null || Constants.distributorname.isEmpty()){
                        Constants.distributorname = new PrefManager(mContext).getStringDataByKey(PrefManager.KEY_DISTRIBUTOR_NAME);
                    }
                    rowObject.put("distributorname",Constants.distributorname);
                } catch (Exception e) {
                    Log.d("Exception in Dist. name", e.getMessage());
                }

                try {
                    db = mContext.getApplicationContext().openOrCreateDatabase("freshorders", 0, null);

                    Cursor curs1;
                    curs1 = db.rawQuery("SELECT * from dealertable", null);
                    Log.e("dEALER", String.valueOf(curs1.getCount()));
                    if (curs1.getCount() > 0) {
                        curs1.moveToFirst();
                        selected_dealer = curs1.getString(curs1
                                .getColumnIndex(DatabaseHandler.KEY_Duserid));
                    }

                    Cursor cursor2;
                    Constants.Merchantname = rowObject.getString("mname");

                    cursor2 = db.rawQuery("SELECT * from merchanttable where companyname =" + "'" + Constants.Merchantname + "'", null);
                    Log.e("HeaderCountCanled", String.valueOf(cursor2.getCount()));

                    cursor2.moveToFirst();
                    while (!cursor2.isAfterLast()) {

                        int Columncount = cursor2.getColumnCount();
                        Log.e("merchantColumn", String.valueOf(cursor2.getColumnCount()));
                        JSONObject merchantdetail = new JSONObject();
                        for (int j = 0; j < Columncount; j++) {
                            if (cursor2.getColumnName(j) != null) {
                                try {
                                    if (cursor2.getColumnName(j).equals("cflag")) {
                                        if (cursor2.getString(j).equals("R")) {
                                            merchantdetail.put(cursor2.getColumnName(j), "G");
                                        } else if (cursor2.getString(j).equals("G")) {
                                            merchantdetail.put(cursor2.getColumnName(j), "");
                                        } else {
                                            merchantdetail.put(cursor2.getColumnName(j), "");
                                        }

                                    } else {
                                        if (cursor2.getString(j) != null) {
                                            if (!cursor2.getString(j).equals("New User")) {

                                                merchantdetail.put(cursor2.getColumnName(j), cursor2.getString(j));

                                            }
                                        } else {
                                            if (!cursor2.getColumnName(j).equals("userid")) {
                                                merchantdetail.put(cursor2.getColumnName(j), "");
                                            }

                                        }
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.d("Merchant_Exception", e.getMessage());
                                    listener.onOrderFailed("Went Worng :" + e.getMessage());
                                    return false;
                                }
                            }
                        }
                        merchantdetail.put("usertype", "M");
                        merchantdetail.put("duserid", selected_dealer);
                        rowObject.put("merchant", merchantdetail);
                        cursor2.moveToNext();
                    }

                    cursor2.close();

                    Log.e("Detailarray", rowObject.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                    listener.onOrderFailed("Went Worng :" + e.getMessage());
                    return false;
                }


                Log.e("jsonarray", rowObject.toString());

                JSONArray resultDetail = new JSONArray();
                try {

                    Cursor cursor;
                    cursor = databaseHandler.getorderdetail(rowObject.getString("oflnordid"));
                    Log.e("DetailCount", String.valueOf(cursor.getCount()));
                    cursor.moveToFirst();
                    while (!cursor.isAfterLast()) {

                        int Columncount = cursor.getColumnCount();
                        Log.e("columnCount", String.valueOf(cursor.getColumnCount()));
                        JSONObject detailObject = new JSONObject();

                        for (int i = 0; i < Columncount; i++) //17
                        {
                            if (cursor.getColumnName(i) != null) {

                                try {

                                    if (cursor.getString(i) != null) {
                                        Log.d("TAG_NAME", cursor.getString(i));
                                        detailObject.put(cursor.getColumnName(i), cursor.getString(i));
                                    } else {
                                        detailObject.put(cursor.getColumnName(i), "");
                                    }
                                } catch (Exception e) {
                                    Log.d("TAG_NAME", e.getMessage());
                                }
                            }

                        }

                        resultDetail.put(detailObject);
                        cursor.moveToNext();
                    }

                    cursor.close();

                    Log.e("Detailarray", resultDetail.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                    listener.onOrderFailed("Went Worng :" + e.getMessage());
                    return false;
                }
                try {
                    rowObject.put("orderdtls", resultDetail);
                } catch (JSONException e) {
                    e.printStackTrace();
                    listener.onOrderFailed("Went Worng :" + e.getMessage());
                    return false;
                }
                resultSet.put(rowObject);
                curs.moveToNext();
            }
            curs.close();
            Log.e("SaveArray", resultSet.toString());

            return true;
        }catch (Exception e){
            e.printStackTrace();
            listener.onOrderFailed("Went Wrong :" +e.getMessage());
            return false;
        }
    }

    public boolean netCheck() {
        // for network connection
        try {
            ConnectivityManager connManager = (ConnectivityManager) mContext.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mWifi = connManager
                    .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

            ConnectivityManager connectionManager = (ConnectivityManager) mContext.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo mNetwork = connectionManager
                    .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            Object result = null;
            if (mWifi.isConnected() || mNetwork.isConnected()) {
                return true;
            }

            else if (result == null) {
               /* showAlertDialog(SalesManOrderCheckoutActivity.this,
                        "No Internet Connection",
                        "Please Check Your internet connection.", false);*/
                return false;
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }
        return false;
    }
}
