package com.freshorders.freshorder.syncadapter.adapter;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.content.SyncResult;
import android.database.Cursor;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;

import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.model.GPSPointsDetail;
import com.freshorders.freshorder.model.GPSSubResponse;
import com.freshorders.freshorder.model.PointsResponse;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.syncadapter.provider.LocationContract;
import com.freshorders.freshorder.ui.SplashActivity;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.NotificationUtil;
import com.freshorders.freshorder.utils.Utils;
import com.freshorders.freshorder.volleylib.APIClient;
import com.freshorders.freshorder.volleylib.APIInterface;

//import junit.framework.Assert;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LocationSyncAdapter extends AbstractThreadedSyncAdapter {

    private static final String TAG = "SYNC_ADAPTER";

    private final ContentResolver mContentResolver;
    JsonServiceHandler JsonServiceHandler;
    private long rowId = 0;
    String bugPath;
    com.freshorders.freshorder.utils.Log  L;

    APIInterface apiInterface;


    public LocationSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        mContentResolver = context.getContentResolver();
        JsonServiceHandler = new JsonServiceHandler(Utils.strSaveTracklog, context);
        bugPath = new PrefManager(context).getStringDataByKey(PrefManager.KEY_BUG_FILE_PATH);
         L = new com.freshorders.freshorder.utils.Log(bugPath);
    }

    public LocationSyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        mContentResolver = context.getContentResolver();
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {


        apiInterface = APIClient.getClient().create(APIInterface.class);
        Log.e(TAG, "Starting synchronization...................");

        try {
            // Synchronize our news feed
            uploadGPSPoints();

            // Add any other things you may want to sync

        } catch (IOException ex) {
            Log.e(TAG, "Error synchronizing!", ex);
            syncResult.stats.numIoExceptions++;
        } catch (JSONException ex) {
            Log.e(TAG, "Error synchronizing!", ex);
            syncResult.stats.numParseExceptions++;
        } catch (RemoteException | OperationApplicationException ex) {
            Log.e(TAG, "Error synchronizing!", ex);
            syncResult.stats.numAuthExceptions++;
        }catch (Exception e){
            e.printStackTrace();
        }

        Log.w(TAG, "Finished synchronization!");

    }


    private void uploadGPSPoints() throws IOException, JSONException, RemoteException, OperationApplicationException, Exception {



        List<GPSPointsDetail> bulkList = new ArrayList<>();

            // We need to collect all the network items in a hash table
            Log.i(TAG, "Fetching server entries...");

        /*Map<String, Article> networkEntries = new HashMap<>();

        // Parse the pretend json news feed
        String jsonFeed = download(rssFeedEndpoint);
        JSONArray jsonArticles = new JSONArray(jsonFeed);
        for (int i = 0; i < jsonArticles.length(); i++) {
            Article article = ArticleParser.parse(jsonArticles.optJSONObject(i));
            networkEntries.put(article.getId(), article);
        }  */

            // Create list for batching ContentProvider transactions
            ArrayList<ContentProviderOperation> batch = new ArrayList<>();

            // Compare the hash table of network entries to all the local entries
            Log.i(TAG, "Fetching local entries...");

            // Defines a string to contain the selection clause
            String selectionClause1 = null;
            // Initializes an array to contain selection arguments
            String[] selectionArgs1 = {""};
            // Constructs a selection clause that matches the word that the user entered.
            selectionClause1 = LocationContract.Locations.COL_IS_SYNC + " = ?";
            // Moves the user's input string to the selection arguments.
            selectionArgs1[0] = String.valueOf(0);

            Cursor c = mContentResolver.query(LocationContract.Locations.CONTENT_URI, null, selectionClause1, selectionArgs1, null);
            if(c != null && c.getCount() > 0) {
                //assert c != null;
                c.moveToFirst();

                String id;
                String title;
                String content;
                String link;
                //Article found;
                for (int i = 0; i < c.getCount() ; i++) {

                    Location start = new Location("locationA");
                    Location end = new Location("locationB");
                    double lat = 0, lon = 0, preLat = 0, preLon = 0;
                    JSONObject item = new JSONObject();

                    rowId = c.getLong(c.getColumnIndex(DatabaseHandler.KEY_LOCATION_SERIAL_NO));

                    item.put("rowid", rowId);
                    item.put("batchid", 0);
                    item.put("suserid", c.getString(c.getColumnIndex(DatabaseHandler.KEY_LOCATION_SUSER_ID)));
                    item.put("logtime", c.getString(c.getColumnIndex(DatabaseHandler.KEY_LOCATION_DATE_TIME)));
                    lat = c.getDouble(c.getColumnIndex(DatabaseHandler.KEY_LOCATION_LAT));
                    lon = c.getDouble(c.getColumnIndex(DatabaseHandler.KEY_LOCATION_LON));
                    start.setLatitude(lat);
                    start.setLongitude(lon);

                    preLat = c.getDouble(c.getColumnIndex(DatabaseHandler.KEY_LOCATION_P_LAT));
                    preLon = c.getDouble(c.getColumnIndex(DatabaseHandler.KEY_LOCATION_P_LON));
                    end.setLatitude(preLat);
                    end.setLongitude(preLon);

                    // Defines a string to contain the selection clause
                    String selectionClause = null;
                    // Initializes an array to contain selection arguments
                    String[] selectionArgs = {""};
                    long preId = c.getLong(c.getColumnIndex(DatabaseHandler.KEY_LOCATION_PREVIOUS_ID));
                    // Constructs a selection clause that matches the word that the user entered.
                    selectionClause = LocationContract.Locations.COL_ID + " = ?";
                    // Moves the user's input string to the selection arguments.
                    selectionArgs[0] = String.valueOf(preId);

                    //Uri singleUri = ContentUris.withAppendedId(LocationContract.Locations.CONTENT_URI,2);
                    /*
                    Cursor curs = mContentResolver.query(LocationContract.Locations.CONTENT_URI, null, selectionClause, selectionArgs, null);

                    //Cursor curs = databaseHandler.getLocationsForPreById(preId);
                    if (curs != null && curs.getCount() > 0) {
                        Log.e(TAG, "...........................previousFound");
                        curs.moveToFirst();
                        preLat = curs.getDouble(curs.getColumnIndex(DatabaseHandler.KEY_LOCATION_LAT));
                        preLon = curs.getDouble(curs.getColumnIndex(DatabaseHandler.KEY_LOCATION_LON));
                        end.setLatitude(preLat);
                        end.setLongitude(preLon);
                        curs.close();
                    } else {
                        c.moveToNext();
                        continue;
                    }  */

                    double temp_distance = start.distanceTo(end);
                    temp_distance = temp_distance / 1000;
                    Log.e(TAG, ".................temp_distance::::KM::" + temp_distance);

                    item.put("geolat", lat);
                    item.put("geolong", lon);
                    item.put("pgeolat", preLat);
                    item.put("pgeolong", preLon);
                    item.put("status", "Active");
                    item.put("updateddt", c.getString(c.getColumnIndex(DatabaseHandler.KEY_LOCATION_DATE_TIME)));
                    item.put("distance", temp_distance);
                    item.put("amount", 0);
                    item.put("issynced", 1);
                    item.put("pre_id", c.getLong(c.getColumnIndex(DatabaseHandler.KEY_LOCATION_PREVIOUS_ID)));
                    item.put("seqid", c.getString(c.getColumnIndex(DatabaseHandler.KEY_LOCATION_UNIQUE_ID)));

                    Log.e(TAG, "....item:: " + item.toString());

                    GPSPointsDetail obj = new GPSPointsDetail(rowId, 0, Constants.USER_ID, c.getString(c.getColumnIndex(DatabaseHandler.KEY_LOCATION_DATE_TIME)),
                            lat, lon, preLat, preLon, "Active", c.getString(c.getColumnIndex(DatabaseHandler.KEY_LOCATION_DATE_TIME)),
                            temp_distance, 0, 1, c.getLong(c.getColumnIndex(DatabaseHandler.KEY_LOCATION_PREVIOUS_ID)),
                            c.getString(c.getColumnIndex(DatabaseHandler.KEY_LOCATION_UNIQUE_ID)));

                    //addToQueue(item,databaseHandler, bugPath);/////////////////
                    bulkList.add(obj);

                    if(i == c.getCount() - 1){
                        bulkQueue(bulkList, bugPath);/////////////////
                        bulkList = new ArrayList<>();
                        Log.i(TAG, "Bulk sending finished...");
                    }else if( i % 50 == 0){
                        bulkQueue(bulkList, bugPath);/////////////////
                        bulkList = new ArrayList<>();
                        Log.i(TAG, "Bulk sending chunk finished...");
                    }

                    /*
                    JsonServiceHandler.setParams(item.toString());
                    JSONObject object = JsonServiceHandler.ServiceData();
                    if (object != null) {
                        if (object.has("status")) {
                            String status = object.getString("status");
                            if (status.equals("true")) {
                                String data = object.getString("data");
                                Log.e(TAG, "..........response" + data);
                                L.ee(TAG, "..........response" + data);

                                ContentValues contentValues = new ContentValues();
                                contentValues.put(DatabaseHandler.KEY_LOCATION_IS_SERVER, 1);
                                String selection = LocationContract.Locations.COL_ID + " = ?";
                                String[] selectionArg = {""};
                                selectionArg[0] = String.valueOf(rowId);

                                mContentResolver.update(LocationContract.Locations.CONTENT_URI, contentValues, selection, selectionArg);
                                //////////////////databaseHandler.setDistancePointSuccess(rowId);
                                //databaseHandler.deleteSuccessLocationById(preId);
                            }
                        } else {
                            Log.e(TAG, "..........status not found");
                        }
                    } else {
                        Log.e(TAG, "..........object null");
                    }  */

                    c.moveToNext();
                }
                c.close();
                new NotificationUtil(77777).showNotification(getContext(), SplashActivity.class, "GPS Points", "All Pending Location Successfully Moved");
            }else{
                    L.ee(TAG, "..........No Points Avail to Move");
            }
        }





    private synchronized void bulkQueue(List<GPSPointsDetail> bulkList, final String bugPath){
        Log.e(TAG,"................retro...........Trig");
        //final Call<PointsResponse> call = getRetrofit().sendPoints(jsonStringer);
        if(apiInterface != null) {
            final Call<PointsResponse> call = apiInterface.sendPoints(bulkList);

            call.enqueue(new Callback<PointsResponse>() {
                @Override
                public void onResponse(Call<PointsResponse> call, Response<PointsResponse> response) {
                    try {
                        if (response != null) {
                            if (response.isSuccessful()) {
                                Log.e(TAG,"......................Success");
                                new com.freshorders.freshorder.utils.
                                        Log(bugPath).
                                        ee(TAG, ".........currentId........Success:::" + response.body().getData());
                                PointsResponse res = response.body();
                                if(res != null){
                                    String status = res.getStatus();
                                    if(status.equalsIgnoreCase("true")){
                                        List<GPSSubResponse> list = res.getData();
                                        if(list != null && list.size() > 0){
                                            List<Long> idList = new ArrayList<>();
                                            String[] selectionArgs = new String[list.size()];

                                            for(int i = 0; i < list.size() - 1; i++){ ///last item need......
                                                String duplicate = list.get(i).getIsduplicate();
                                                String success = list.get(i).getSuccess();
                                                String id = list.get(i).getId();
                                                if(duplicate.equalsIgnoreCase("true") ||
                                                        success.equalsIgnoreCase("true")){
                                                    idList.add(Long.parseLong(id));
                                                    selectionArgs[i] = id;
                                                }
                                            }

                                            String lastSequenceId = list.get(list.size()-1).getId();
                                            if(lastSequenceId != null && !lastSequenceId.isEmpty()){  /// LAST ONE TO CHANGE SUCCESS

                                                ContentValues contentValues = new ContentValues();
                                                contentValues.put(DatabaseHandler.KEY_LOCATION_IS_SERVER, 1);
                                                String selection = LocationContract.Locations.COL_UNIQUE_ID + " = ?";
                                                String[] selectionArg = {""};
                                                selectionArg[0] = String.valueOf(lastSequenceId);
                                                mContentResolver.update(LocationContract.Locations.CONTENT_URI, contentValues, selection, selectionArg);

                                            }
                                            String selectionClause = DatabaseHandler.KEY_LOCATION_UNIQUE_ID + " = ?";
                                            int deletedSize = 0;
                                            /////////////deletedSize = databaseHandler.removeAllSuccessfulPoints(selectionClause, selectionArgs);
                                            deletedSize = mContentResolver.delete(LocationContract.Locations.CONTENT_URI, selectionClause, selectionArgs);

                                            if(deletedSize > 0){
                                                new com.freshorders.freshorder.utils.
                                                        Log(bugPath).
                                                        ee(TAG, ".........successfully removed points in local table:::deletedSize :: "+ deletedSize);
                                            }
                                        }
                                    }else {
                                        Log.e(TAG,".............unsuccessful response");
                                    }
                                }

                            }else {
                                Log.e(TAG,"......................Fail");
                                new com.freshorders.freshorder.utils.
                                        Log(bugPath).
                                        ee(TAG, ".........currentId........Fail:::" + response.toString());
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<PointsResponse> call, Throwable throwable) {
                    try {
                        Log.e(TAG,"......................Fail");
                        new com.freshorders.freshorder.utils.
                                Log(bugPath).
                                ee(TAG, ".........currentId.......Reason:::" + throwable.getMessage());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
        }else {
            new com.freshorders.freshorder.utils.Log(bugPath).ee(TAG, "............Retrofit not created.");
        }

    }

}
