package com.freshorders.freshorder.syncadapter.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.freshorders.freshorder.syncadapter.Authenticator;

public class AuthenticatorService extends Service {

    private Authenticator authenticator;

    @Override
    public void onCreate() {
        authenticator = new Authenticator(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return authenticator.getIBinder();

    }
}

