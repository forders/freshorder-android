package com.freshorders.freshorder.syncadapter.provider;

import android.net.Uri;

import com.freshorders.freshorder.db.DatabaseHandler;

public class LocationContract {

    // ContentProvider information
    static final String PATH_LOCATIONS = DatabaseHandler.TBL_LOCATION;
    public static final String CONTENT_AUTHORITY = "com.freshorders.freshorder"; //similar
    public static final Uri BASE_CONTENT_URI  = Uri.parse("content://" + CONTENT_AUTHORITY);


    /**
     * This represents our SQLite table for our articles.
     */
    public static abstract class Locations {

        public static final String NAME = DatabaseHandler.TBL_LOCATION;
        public static final String COL_ID = DatabaseHandler.KEY_LOCATION_SERIAL_NO;
        public static final String COL_IS_SYNC = DatabaseHandler.KEY_LOCATION_IS_SERVER;
        public static final String COL_PRE_ID = DatabaseHandler.KEY_LOCATION_IS_SERVER;
        public static final String COL_UNIQUE_ID = DatabaseHandler.KEY_LOCATION_UNIQUE_ID;

        public static final String COL_TITLE = "articleTitle";
        public static final String COL_CONTENT = "articleContent";
        public static final String COL_LINK = "articleLink";

        // ContentProvider information for gps Location
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_LOCATIONS).build();
        public static final String CONTENT_TYPE =
                "vnd.android.cursor.dir/" + CONTENT_URI + "/" + PATH_LOCATIONS;
        public static final String CONTENT_ITEM_TYPE =
                "vnd.android.cursor.item/" + CONTENT_URI + "/" + PATH_LOCATIONS;
    }



}
