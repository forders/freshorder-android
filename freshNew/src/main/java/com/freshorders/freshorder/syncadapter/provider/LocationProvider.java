package com.freshorders.freshorder.syncadapter.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.utils.Log;

public class LocationProvider extends ContentProvider {

    private static final String TAG = "PROVIDER     ";

    // Use ints to represent different queries
    private static final int LOCATION = 1;
    private static final int LOCATION_ID = 2;
    private static final int LOCATION_IS_SYNC = 3;

    private static final UriMatcher uriMatcher;
    static {
        // Add all our query types to our UriMatcher
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(LocationContract.CONTENT_AUTHORITY, LocationContract.PATH_LOCATIONS, LOCATION);
        uriMatcher.addURI(LocationContract.CONTENT_AUTHORITY, LocationContract.PATH_LOCATIONS + "/#", LOCATION_ID);
        uriMatcher.addURI(LocationContract.CONTENT_AUTHORITY, LocationContract.PATH_LOCATIONS + "/#", LOCATION_IS_SYNC);
    }

    private SQLiteDatabase db;

    @Override
    public boolean onCreate() {
        this.db = DatabaseHandler.getInstance(getContext()).getDb();
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        Cursor c;
        switch (uriMatcher.match(uri)) {
            // Query for multiple article results
            case LOCATION:
                c = db.query(LocationContract.Locations.NAME,
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);
                break;

            // Query for single LOCATION result
            case LOCATION_ID:
                long _id = ContentUris.parseId(uri);
                Log.e(TAG,".........LOCATION_ID." + _id);
                c = db.query(LocationContract.Locations.NAME,
                        projection,
                        LocationContract.Locations.COL_ID + "=?",
                        new String[] { String.valueOf(_id) },
                        null,
                        null,
                        sortOrder);
                break;
            // Query for single LOCATION result
            /*case LOCATION_IS_SYNC:
                long _isSync = ContentUris.parseId(uri);
                c = db.query(LocationContract.Locations.NAME,
                        projection,
                        LocationContract.Locations.COL_IS_SYNC + "=?",
                        new String[] { String.valueOf(_isSync) },
                        null,
                        null,
                        sortOrder);
                break; */
            default: throw new IllegalArgumentException("Invalid URI!");
        }

        // Tell the cursor to register a content observer to observe changes to the
        // URI or its descendants.
        assert getContext() != null;
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        // Find the MIME type of the results... multiple results or a single result
        switch (uriMatcher.match(uri)) {
            case LOCATION:
                return LocationContract.Locations.CONTENT_TYPE;
            case LOCATION_ID:
                return LocationContract.Locations.CONTENT_ITEM_TYPE;
            default: throw new IllegalArgumentException("Invalid URI!");
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {

        int rows;
        switch (uriMatcher.match(uri)) {
            case LOCATION:
                rows = db.delete(LocationContract.Locations.NAME, selection, selectionArgs);
                break;
            default: throw new IllegalArgumentException("Invalid URI!");
        }

        // Notify any observers to update the UI
        if (rows != 0) {
            assert getContext() != null;
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rows;

    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {

        int rows;
        switch (uriMatcher.match(uri)) {
            case LOCATION:
                rows = db.update(LocationContract.Locations.NAME, values, selection, selectionArgs);
                break;
            default: throw new IllegalArgumentException("Invalid URI!");
        }

        // Notify any observers to update the UI
        if (rows != 0) {
            assert getContext() != null;
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rows;

    }
}
