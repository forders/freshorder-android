package com.freshorders.freshorder.volleylib;

import com.freshorders.freshorder.model.CommonResponseModel;
import com.freshorders.freshorder.model.DSRDetailInputModel;
import com.freshorders.freshorder.model.DSRFullDetailModel;
import com.freshorders.freshorder.model.DSRFullResponseModel;
import com.freshorders.freshorder.model.GPSPointsDetail;
import com.freshorders.freshorder.model.PRFullDetailModel;
import com.freshorders.freshorder.model.PlaceHierarchyResModel;
import com.freshorders.freshorder.model.PointsResponse;
import com.freshorders.freshorder.model.SalesManFullResponseModel;
import com.freshorders.freshorder.model.SalesmanListFetchByIdModel;
import com.freshorders.freshorder.utils.ParameterConstants;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface APIInterface {

    @Headers({"Content-type: application/json",
            "Accept: */*"})
    @POST("geologs")
    Call<PointsResponse> sendPoints(@Body List<GPSPointsDetail> body);

    @Headers({"Content-type: application/json",
            "Accept: */*"})
    @POST("geologs")
    Call<PointsResponse> sendPoints(@Body GPSPointsDetail body);

    @GET
    Call<DSRFullResponseModel> getDSR(@Url String url);

    @Headers({"Content-type: application/json",
            "Accept: */*"})
    @POST("orderdetails/report")
    Call<DSRFullDetailModel> getDSRFullDetail(@Body DSRDetailInputModel body);  //

    @Headers({"Content-type: application/json",
            "Accept: */*"})
    @POST("orderdetails/report")
    Call<PRFullDetailModel> getPRFullDetail(@Body DSRDetailInputModel body);  //

    @Headers({"Content-type: application/json",
            "Accept: */*"})
    @POST("users/merchantlist")
    Call<SalesManFullResponseModel> getSalesManOrBranchList(@Body SalesmanListFetchByIdModel body);

    @GET
    Call<ResponseBody> getPlaceHierarchy(@Url String url);


    @FormUrlEncoded
    @POST()
    Call<CommonResponseModel> sendFCMToken(@Url String api_name,
                                           @Field(ParameterConstants.GCM_ID) String gcmid);
}
