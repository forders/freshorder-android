package com.freshorders.freshorder.attendance.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.freshorders.freshorder.R;
import com.google.android.material.textfield.TextInputEditText;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import com.freshorders.freshorder.view.DatePicker;

import static android.app.Activity.RESULT_CANCELED;

public class AddEmployee extends Fragment {


    ArrayAdapter<String> adapter;
    String[] arrIDProof = {
            "AADHAR CARD",
            "DRIVING LICENCE",
            "VOTER ID",
            "OTHER"
    };
    private Context mContext;

    private Spinner spinIdProof, spinSalaryType, spinEmpSex;
    private List<String> listIDProof;

    private String selectedIDProof = "";
    private int selectedIDIndex = -1;

    private String selectedSex = "";
    private int selectedSexIndex = -1;

    private static final String IMAGE_DIRECTORY = "/FRESHORDERS_EMP";
    private int GALLERY = 1, CAMERA = 2;

    private EditText etxDOB, etxJoinDate;
    private ImageView imvPhoto;
    private String strSelectedDOB = "";
    private String strSelectedJoinDate = "";

    private TextInputEditText tieEmpName,tieFatherName,tieDesignation,tieDOB,tieJoiningDate, tieEmpAddress,tieEmpContactNumber,
            tieEmpMobile1, tieIdProof, tieEmpDesignation, tieEmpWorkExperience, tieEmpOther;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //just change the fragment_dashboard
        //with the fragment you want to inflate
        //like if the class is HomeFragment it should have R.layout.home_fragment
        //if it is DashboardFragment it should have R.layout.fragment_dashboard
        return inflater.inflate(R.layout.fragment_attendance_add_employee, null);
    }


    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mContext = view.getContext();
        setEvents(view);


    }

    private void setEvents(View view){

        tieEmpName = view.findViewById(R.id.id_tie_attendance_name);
        tieFatherName = view.findViewById(R.id.id_tie_attendance_father_name);
        tieDesignation = view.findViewById(R.id.id_tie_attendance_designation);
        tieDOB = view.findViewById(R.id.id_tie_attendance_DOB);
        spinIdProof = view.findViewById(R.id.id_spin_attendance_id_proof);
        tieJoiningDate = view.findViewById(R.id.id_tie_attendance_date_joining);
        tieEmpAddress = view.findViewById(R.id.id_tie_attendance_address);
        tieEmpContactNumber = view.findViewById(R.id.id_tie_attendance_contact_number);

        spinEmpSex = view.findViewById(R.id.id_Spin_attendance_sex);
        imvPhoto = view.findViewById(R.id.id_IV_attendance_pro_image);

        listIDProof = new ArrayList<>(Arrays.asList(arrIDProof));
        adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, listIDProof);
        spinIdProof.setAdapter(adapter);
        tieIdProof  = view.findViewById(R.id.id_tie_attendance_idProof);

        ///Default date setting
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        tieDOB.setText(formattedDate);
        strSelectedDOB = formattedDate;
        Button btnSelectImg = view.findViewById(R.id.id_Btn_attendance_img);

        tieDOB.setOnClickListener(selectDate);
        spinIdProof.setOnItemSelectedListener(new AddEmployee.IDProofSelectListener());
        spinEmpSex.setOnItemSelectedListener(new AddEmployee.EmpSexSelectListener());

        btnSelectImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPictureDialog();
            }
        });

        tieJoiningDate.setText(formattedDate);
        tieJoiningDate.setOnClickListener(selectJoinDate);

    }

    private View.OnClickListener selectJoinDate = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DatePicker datePopup = new
                    DatePicker(
                    mContext, tieJoiningDate,
                    new DatePicker.PopupListener() {
                        @Override
                        public void onPopupClosed() {
                            if(tieJoiningDate.getText() != null) {
                                strSelectedJoinDate = tieJoiningDate.getText().toString().trim();
                            }
                        }
                    });
            datePopup.showPopup();
        }
    };

    private void showPictureDialog(){
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(mContext);
        pictureDialog.setTitle("Add Photo!");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera" };
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                AddEmployee.this.choosePhotoFromGallery();
                                break;
                            case 1:
                                AddEmployee.this.takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    private void choosePhotoFromGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    private View.OnClickListener selectDate = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            DatePicker datePopup = new
                    DatePicker(
                    mContext, tieDOB,
                    new DatePicker.PopupListener() {
                        @Override
                        public void onPopupClosed() {
                            String dob = String.valueOf(tieDOB.getText());
                            if(!dob.isEmpty()) {
                                strSelectedDOB = dob;
                            }
                        }
                    });
            datePopup.showPopup();
        }
    };


    class IDProofSelectListener implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            selectedIDIndex = pos;
            selectedIDProof = listIDProof.get(selectedIDIndex);
            Toast.makeText(mContext, selectedIDProof, Toast.LENGTH_LONG).show();

        }
        public void onNothingSelected(AdapterView<?> parent) {
            // Dummy
        }
    }

    class EmpSexSelectListener implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            selectedSexIndex = pos;
            selectedSex = spinEmpSex.getSelectedItem().toString();
        }
        public void onNothingSelected(AdapterView<?> parent) {
            // Dummy
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), contentURI);
                    String path = saveImage(bitmap);
                    Toast.makeText(mContext, "Image Saved!", Toast.LENGTH_SHORT).show();
                    imvPhoto.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(mContext, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {
            Bitmap thumbnail = (Bitmap) Objects.requireNonNull(data.getExtras()).get("data");
            imvPhoto.setImageBitmap(thumbnail);
            saveImage(Objects.requireNonNull(thumbnail));
            Toast.makeText(mContext, "Image Saved!", Toast.LENGTH_SHORT).show();
        }
    }

    private String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(mContext,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::---&gt;" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

}
