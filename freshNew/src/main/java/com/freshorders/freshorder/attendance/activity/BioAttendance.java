package com.freshorders.freshorder.attendance.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.attendance.fragment.AddEmployee;
import com.freshorders.freshorder.attendance.fragment.AttendanceFragment;
import com.freshorders.freshorder.attendance.fragment.EmployeeDetailFragment;
import com.freshorders.freshorder.attendance.view.CurvedBottomNavigationView;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.service.PrefManager;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

public class BioAttendance extends AppCompatActivity
        implements BottomNavigationView.OnNavigationItemSelectedListener,
        AttendanceFragment.OnFragmentInteractionListener, EmployeeDetailFragment.OnFragmentInteractionListener{

    private static final String TAG = BioAttendance.class.getSimpleName();

    private DrawerLayout mDrawerLayout;
    private Toolbar toolbar;
    private LinearLayout lineHeaderProgress ;


    private BottomNavigationView navigation;

    public static int navItemIndex = 0;
    public static int bottomNavItemId = 0;
    private NavigationView navigationView;

    DatabaseHandler databaseHandler;

    private String bugPath;


    private CurvedBottomNavigationView mView;
    private FloatingActionButton heartVector;
    //private FloatingActionButton heartVector1;
    //private FloatingActionButton heartVector2;
    private float mYVal;
    private RelativeLayout mlinId;
    //PathModel outline;

    final Fragment fragAddEmployee = new AddEmployee();
    final Fragment fragAttendance = new AttendanceFragment();
    final Fragment fragEmployeeDetail = new EmployeeDetailFragment();
    final FragmentManager fm = getSupportFragmentManager();
    Fragment active = fragAddEmployee;

    private Fragment fragment;

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container_attendance, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bio_attendance);

        bugPath = new PrefManager(this).getStringDataByKey(PrefManager.KEY_BUG_FILE_PATH);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setHomeAsUpIndicator(R.drawable.ic_menu_green_24dp);
            ab.setDisplayHomeAsUpEnabled(true);
        }

        databaseHandler = new DatabaseHandler(getApplicationContext());

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);


        mView = findViewById(R.id.navigation);
        heartVector = findViewById(R.id.fab);

        mlinId = findViewById(R.id.lin_id);
        mView.inflateMenu(R.menu.bottom_menu);
        mView.setSelectedItemId(R.id.action_schedules);

        mView.setOnNavigationItemSelectedListener(BioAttendance.this);
        /*
        fm.beginTransaction().add(R.id.frame_container_attendance, fragEmployeeDetail, "3").hide(fragEmployeeDetail).commit();
        fm.beginTransaction().add(R.id.frame_container_attendance, fragAttendance, "2").hide(fragAttendance).commit();
        fm.beginTransaction().add(R.id.frame_container_attendance,fragAddEmployee, "1").commit(); */
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.action_attendance:
                //fm.beginTransaction().hide(active).show(fragAddEmployee).commit();
                //active = fragAddEmployee;
                fragment = new AddEmployee();
                loadFragment(fragment);
                return true;
            case R.id.action_schedules:
                //fm.beginTransaction().hide(active).show(fragAttendance).commit();
                //active = fragAttendance;
                break;
            case R.id.action_music:
                //fm.beginTransaction().hide(active).show(fragEmployeeDetail).commit();
                //active = fragEmployeeDetail;
                fragment = new EmployeeDetailFragment();
                loadFragment(fragment);
                return true;
        }

        return true;
    }


    @Override
    public void onAttendanceInteraction(Uri uri) {

    }

    @Override
    public void onEmpDetailInteraction(Uri uri) {

    }
}
