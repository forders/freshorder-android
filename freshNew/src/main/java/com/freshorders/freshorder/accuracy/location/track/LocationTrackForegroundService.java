package com.freshorders.freshorder.accuracy.location.track;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.interfaces.GPSPoints;
import com.freshorders.freshorder.model.GPSPointsDetail;
import com.freshorders.freshorder.model.GPSSubResponse;
import com.freshorders.freshorder.model.PointsResponse;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.service.UserActivityIntentService;
import com.freshorders.freshorder.syncadapter.adapter.SyncAdapterManager;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.volleylib.APIClient;
import com.freshorders.freshorder.volleylib.APIInterface;
import com.google.android.gms.location.ActivityRecognitionClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;
import static com.freshorders.freshorder.service.PrefManager.KEY_IS_USER_MOVING;
import static com.freshorders.freshorder.utils.Constants.USER_ACTIVITY_RQ_CODE;


public class LocationTrackForegroundService extends Service implements LocationListener, GpsStatus.Listener, SensorEventListener {

    private static final String TAG = "DistanceCal";
    private ActivityRecognitionClient mActivityRecognitionClient;

    private Long intervalId = 0l;

    SyncAdapterManager syncAdapterManager;
    private static boolean isPreIdAvail = true;

    public String bugPath;

    public GPSPoints api;
    public static long previousId = 0l;
    private static boolean isFirst = true;
    DatabaseHandler databaseHandler;
    // flag for GPS status
    boolean isGPSEnabled = false;
    int minutes = 1;
    // flag for GPS status
    boolean canGetLocation = false;

    int notiID = 1001;
    int accuracy = 500;

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 5; // 10 meters
    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 30 * 1; // 1 minute
    // Declaring a Location Manager
    protected LocationManager locationManager;
    //  private FusedLocationProviderClient fusedLocationClient;
    String NOTIFICATION_CHANNEL_ID = "Channel_location1";
    Disposable disposable;

    APIInterface apiInterface;


    boolean isLocationManagerUpdatingLocation;


    ArrayList<Location> locationList;

    ArrayList<Location> oldLocationList;
    ArrayList<Location> noAccuracyLocationList;
    ArrayList<Location> inaccurateLocationList;
    ArrayList<Location> kalmanNGLocationList;


    boolean isLogging;

    float currentSpeed = 0.0f; // meters/second

    KalmanFilterLatLon kalmanFilter;
    long runStartTimeInMillis;

    ArrayList<Integer> batteryLevelArray;
    ArrayList<Float> batteryLevelScaledArray;
    int batteryScale;
    int gpsCount;

    PrefManager pre;
    private Intent mIntentService;
    private PendingIntent mPendingIntent;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {

        databaseHandler = new DatabaseHandler(getApplicationContext());
        pre = new PrefManager(getApplicationContext());
        bugPath = pre.getStringDataByKey(PrefManager.KEY_BUG_FILE_PATH);
        Toast.makeText(this, "Service Created", Toast.LENGTH_LONG).show();
        apiInterface = APIClient.getClient().create(APIInterface.class);
        insertError("SERVICE INITIATED");

        ////////////////////////////////////////////////////////////////////
        isLocationManagerUpdatingLocation = false;
        locationList = new ArrayList<>();
        noAccuracyLocationList = new ArrayList<>();
        oldLocationList = new ArrayList<>();
        inaccurateLocationList = new ArrayList<>();
        kalmanNGLocationList = new ArrayList<>();
        kalmanFilter = new KalmanFilterLatLon(3);
        isLogging = false;
        batteryLevelArray = new ArrayList<>();
        batteryLevelScaledArray = new ArrayList<>();

        //startUpdatingLocation();

    }

    public void startUpdatingLocation() {

        if (PrefManager.getStatus(LocationTrackForegroundService.this))
            if (android.os.Build.VERSION.SDK_INT >= 17) {
                runStartTimeInMillis = (long) (SystemClock.elapsedRealtimeNanos() / 1000000);
            } else {
                runStartTimeInMillis = System.currentTimeMillis();
            }
        locationList.clear();

        oldLocationList.clear();
        noAccuracyLocationList.clear();
        inaccurateLocationList.clear();
        kalmanNGLocationList.clear();

        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        //Exception thrown when GPS or Network provider were not available on the user's device.
        try {
            Criteria criteria = new Criteria();
            criteria.setAccuracy(Criteria.ACCURACY_FINE);
            criteria.setPowerRequirement(Criteria.POWER_HIGH);
            criteria.setAltitudeRequired(false);
            criteria.setSpeedRequired(true);
            criteria.setCostAllowed(true);
            criteria.setBearingRequired(false);

            //API level 9 and up
            criteria.setHorizontalAccuracy(Criteria.ACCURACY_HIGH);
            criteria.setVerticalAccuracy(Criteria.ACCURACY_HIGH);
            //criteria.setBearingAccuracy(Criteria.ACCURACY_HIGH);
            //criteria.setSpeedAccuracy(Criteria.ACCURACY_HIGH);

            Integer gpsFreqInMillis = 3500;  //5000
            Integer gpsFreqInDistance = 4;  // 5 in meters

            locationManager.addGpsStatusListener(this);

            locationManager.requestLocationUpdates(gpsFreqInMillis, gpsFreqInDistance, criteria, this, null);

            /* Battery Consumption Measurement */
            gpsCount = 0;
            batteryLevelArray.clear();
            batteryLevelScaledArray.clear();

        } catch (IllegalArgumentException e) {
            Log.e(TAG, e.getLocalizedMessage());
        } catch (SecurityException e) {
            Log.e(TAG, e.getLocalizedMessage());
        } catch (RuntimeException e) {
            Log.e(TAG, e.getLocalizedMessage());
        }
    }

    @SuppressLint("NewApi")
    private long getLocationAge(Location newLocation) {
        long locationAge;
        if (android.os.Build.VERSION.SDK_INT >= 17) {
            long currentTimeInMilli = (long) (SystemClock.elapsedRealtimeNanos() / 1000000);
            long locationTimeInMilli = (long) (newLocation.getElapsedRealtimeNanos() / 1000000);
            locationAge = currentTimeInMilli - locationTimeInMilli;
        } else {
            locationAge = System.currentTimeMillis() - newLocation.getTime();
        }
        return locationAge;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        insertError("start");
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        // getting network status
        if (!isGPSEnabled) {
            // no network provider is enabled
            showSettingsAlert();
            insertError("GPS is Disabled");
        }

        Toast.makeText(this, "Creating Notification", Toast.LENGTH_SHORT).show();

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant") NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_HIGH);

            // Configure the notification channel.
            notificationChannel.setDescription("Channel description");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        Notification notification = getMyNotification("Distance Calculation started");
        startForeground(notiID, notification);


        startUpdatingLocation();

        ////////////////////////////////////
        setActivityListener();
        ////////////////////////////////

        Observable.interval(minutes, TimeUnit.MINUTES)
                .subscribeOn(Schedulers.newThread())
                .repeat()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Long>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        disposable = d;
                    }

                    @Override
                    public void onNext(Long aLong) {
                        intervalId = aLong;
                        new com.freshorders.freshorder.utils.Log(bugPath).ee(TAG, "interval..........Long" + aLong);
                        sendPointsThroughRetrofit(0, databaseHandler, bugPath);
                    }

                    @Override
                    public void onError(Throwable e) {
                        insertError(e.getMessage() + "");
                    }

                    @Override
                    public void onComplete() {
                        insertError("complete");
                    }
                });

        return START_STICKY;
    }

    private void insertError(String message) {
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

        String date = simpleDateFormat.format(new Date(System.currentTimeMillis()));
        System.out.println(date);

        String pattern1 = "HH : mm : ss";
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(pattern1);

        String time = simpleDateFormat1.format(new Date(System.currentTimeMillis()));
        System.out.println(time);

        ContentValues cv = new ContentValues();
        cv.put(DatabaseHandler.KEY_ERROR_MESSAGE, message);
        cv.put(DatabaseHandler.KEY_ERROR_DATE, date);
        cv.put(DatabaseHandler.KEY_ERROR_TIME, time);
        databaseHandler.insertError(cv);
        //AppDatabase.getAppDatabase(DistanceCalForegroundService.this).errorDao().insert(new errorModel(message,date,time));
    }


    @Override
    public void onDestroy() {
        Toast.makeText(this, "Service Stopped", Toast.LENGTH_LONG).show();

        ///////////////////////////// 03-09-2019
        removeUserActivityListener();
        ////////////////////////////

        if (!disposable.isDisposed()) {
            disposable.dispose();
        }
        insertError("destroy");
        Intent restartService = new Intent("RestartService");
        sendBroadcast(restartService);
    }


    private boolean filterAndAddLocation(Location location) {

        long age = getLocationAge(location);

        if (age > 5 * 1000) { //more than 5 seconds
            Log.d(TAG, "Location is old");
            oldLocationList.add(location);
            return false;
        }

        if (location.getAccuracy() <= 0) {
            Log.d(TAG, "Latitidue and longitude values are invalid.");
            noAccuracyLocationList.add(location);
            return false;
        }

        //setAccuracy(newLocation.getAccuracy());
        float horizontalAccuracy = location.getAccuracy();
        if (horizontalAccuracy > 10) { //10meter filter ///////////////////////////////////////////////////////////////////////////
            Log.d(TAG, "Accuracy is too low.");
            inaccurateLocationList.add(location);
            return false;
        }


        /* Kalman Filter */
        float Qvalue;
        long locationTimeInMillis;
        long elapsedTimeInMillis;

        if (android.os.Build.VERSION.SDK_INT >= 17) {
            locationTimeInMillis = (long) (location.getElapsedRealtimeNanos() / 1000000);
            elapsedTimeInMillis = locationTimeInMillis - runStartTimeInMillis;
        } else {
            elapsedTimeInMillis = location.getTime() - runStartTimeInMillis;
        }

        if (currentSpeed == 0.0f) {
            Qvalue = 3.0f; //3 meters per second
        } else {
            Qvalue = currentSpeed; // meters per second
        }

        kalmanFilter.Process(location.getLatitude(), location.getLongitude(), location.getAccuracy(), elapsedTimeInMillis, Qvalue);
        double predictedLat = kalmanFilter.get_lat();
        double predictedLng = kalmanFilter.get_lng();

        Location predictedLocation = new Location("");//provider name is unecessary
        predictedLocation.setLatitude(predictedLat);//your coords of course
        predictedLocation.setLongitude(predictedLng);
        float predictedDeltaInMeters = predictedLocation.distanceTo(location);

        if (predictedDeltaInMeters > 60) {
            Log.d(TAG, "Kalman Filter detects mal GPS, we should probably remove this from track");
            kalmanFilter.consecutiveRejectCount += 1;

            if (kalmanFilter.consecutiveRejectCount > 3) {
                kalmanFilter = new KalmanFilterLatLon(3); //reset Kalman Filter if it rejects more than 3 times in raw.
            }

            kalmanNGLocationList.add(location);
            return false;
        } else {
            kalmanFilter.consecutiveRejectCount = 0;
        }

        /* Notifiy predicted location to UI
        Intent intent = new Intent("PredictLocation");
        intent.putExtra("location", predictedLocation);
        LocalBroadcastManager.getInstance(this.getApplication()).sendBroadcast(intent);  */

        Log.d(TAG, "Location quality is good enough.");
        currentSpeed = location.getSpeed();
        //////////locationList.add(location);
        insertlocation(location);


        return true;
    }


    private synchronized void insertlocation(Location location) {

        if (PrefManager.getStatus(LocationTrackForegroundService.this)) {
            try {
                double p_lat = 0;
                double p_lon = 0;

                if (Constants.USER_ID == null || Constants.USER_ID.isEmpty()) {
                    Cursor curs;
                    curs = databaseHandler.getDetails();
                    if (curs != null && curs.getCount() > 0) {
                        curs.moveToFirst();
                        Constants.USER_ID = curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_id));
                        Log.e("Constants.USER_ID", Constants.USER_ID);
                        curs.close();
                    }
                }

                String pattern = "dd-MM-yyyy";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, Locale.getDefault());

                String date = simpleDateFormat.format(new Date(System.currentTimeMillis()));
                System.out.println(date);

                String pattern1 = "HH:mm:ss";
                SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(pattern1, Locale.getDefault());

                String time = simpleDateFormat1.format(new Date(System.currentTimeMillis()));
                System.out.println(time);
                ///////////////updateNotification("Last Entry " + date + "," + time);

                SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                String dateTime = ft.format(new Date(System.currentTimeMillis()));
                long preId = 0;
                //Cursor cur_ = databaseHandler.getMaxRowIdFromLocation(date);
                Cursor cur_ = databaseHandler.getMaxRowFromLocation(date);
                if (cur_ != null && cur_.getCount() > 0) {
                    cur_.moveToFirst();
                    if (cur_.getString(0) != null) {
                        preId = cur_.getLong(0);
                        p_lat = cur_.getDouble(cur_.getColumnIndex(DatabaseHandler.KEY_LOCATION_LAT));
                        p_lon = cur_.getDouble(cur_.getColumnIndex(DatabaseHandler.KEY_LOCATION_LON));
                    } else {
                        p_lat = location.getLatitude();
                        p_lon = location.getLongitude();
                    }
                    ///////////newly added 03-09-2019
                    if(pre.getBooleanDataByKey(KEY_IS_USER_MOVING)){
                        com.freshorders.freshorder.utils.Log.p(bugPath, TAG, "Allow User Location...User Is Moving.. Time : " + time);
                    }else {
                        com.freshorders.freshorder.utils.Log.p(bugPath, TAG, "Location Rejected... User Still Stand.. Time :" + time);
                        return;
                    }
                    /////////////// change end 03-09-2019

                } else {
                    p_lat = location.getLatitude(); ////////may be zero//////////////////////
                    p_lon = location.getLongitude();
                }
                Gson gson = new Gson();
                ContentValues values = new ContentValues();
                values.put(DatabaseHandler.KEY_LOCATION_LOCATION, gson.toJson(location).getBytes());
                values.put(DatabaseHandler.KEY_LOCATION_DATE, date);
                values.put(DatabaseHandler.KEY_LOCATION_TIME, time);
                values.put(DatabaseHandler.KEY_LOCATION_DATE_TIME, dateTime);
                values.put(DatabaseHandler.KEY_LOCATION_ACCURACY, location.getAccuracy());
                values.put(DatabaseHandler.KEY_LOCATION_LAT, location.getLatitude());
                values.put(DatabaseHandler.KEY_LOCATION_LON, location.getLongitude());
                values.put(DatabaseHandler.KEY_LOCATION_P_LAT, p_lat);
                values.put(DatabaseHandler.KEY_LOCATION_P_LON, p_lon);
                values.put(DatabaseHandler.KEY_LOCATION_PREVIOUS_ID, preId);
                values.put(DatabaseHandler.KEY_LOCATION_SUSER_ID, Constants.USER_ID);

                long cur_time = 0;
                ////cur_time = cur_time + Integer.parseInt(Constants.USER_ID);

                /////////////final Locale locale = getApplicationContext().getResources().getConfiguration().locale;
                //////////////DateTime.setLocale(locale);

                String current = formatDateTime(new Date(System.currentTimeMillis()));
                Log.e("Date", "............................current" + current);
                //current = Constants.USER_ID + current;
                cur_time = Long.parseLong(current);

                values.put(DatabaseHandler.KEY_LOCATION_UNIQUE_ID, cur_time);

                long id = databaseHandler.insertLocation(values);
                /*
                //JsonServiceHandler JsonServiceHandler = new JsonServiceHandler(Utils.strSaveTracklog, getApplicationContext());
                try {
                    if (isNetAvail(getApplicationContext())) {
                        sendPointsThroughRetrofit(id, databaseHandler, bugPath);/////////
                    } else {
                        Log.e(TAG, "Mobile Data Not Avail......");
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    new com.freshorders.freshorder.utils.Log(bugPath).ee(TAG,"............."+ e.getMessage());
                }  */

                ///sendPointsThroughRetrofit(id,databaseHandler,JsonServiceHandler);

                //syncAdapterManager.syncImmediately();

                /*new LocationTrackAsyncTask.SendPointAsyncTask(id,databaseHandler,JsonServiceHandler,
                        new LocationTrackAsyncTask.LocationCompleteListener() {
                    @Override
                    public void onComplete(Result result) {
                        Log.e(TAG,".................complete");
                    }
                }).execute();  */

            } catch (Exception e) {
                e.printStackTrace();
                new com.freshorders.freshorder.utils.Log(bugPath).ee(TAG, "............." + e.getMessage());
            }
            //AppDatabase.getAppDatabase(DistanceCalForegroundService.this).locationDao().insert(new locationModel(location, date, time, location.getAccuracy()));
        }
    }

    @Override
    public void onLocationChanged(Location location) {

        if (location != null) {

            filterAndAddLocation(location);

            /*
            if(location.hasAccuracy())
            {
                if(location.getAccuracy()<=accuracy)
                {
                    insertlocation(location);
                }
                else
                {
                    insertError("location LOW accuracy : "+location.getAccuracy()+" by GPS API");
                }
            }
            else
            {
                insertError("location NO accuracy by GPS API");
            }  */

        } else {
            insertError("location null by GPS API");
        }

    }

    @Override
    public void onProviderDisabled(String provider) {

        insertError("Provider is Disabled");
    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    public void showSettingsAlert() {
        Toast.makeText(LocationTrackForegroundService.this, "GPS IS DISABLE", Toast.LENGTH_LONG).show();
    }

    private Notification getMyNotification(String text) {
        // The PendingIntent to launch our activity if the user selects

        // this notification
        Bitmap icon = BitmapFactory.decodeResource(getResources(),
                R.drawable.appiconnew);


        /**
         * This is the method that can be called to update the Notification
         */
        return new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
                .setContentTitle("Location Tracking")
                .setTicker("Location Tracking")
                .setContentText(text)
                .setSmallIcon(R.drawable.appiconnew)
                .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                .setOngoing(true).build();
    }

    private void updateNotification(String text) {

        Notification notification = getMyNotification(text);

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(notiID, notification);
    }

    private boolean isNetAvail(Context context) {
        try {
            boolean isConnected, isMobile = false;
            ConnectivityManager cm =
                    (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = null;
            if (cm != null) {
                activeNetwork = cm.getActiveNetworkInfo();
            } else {
                return false;
            }

            isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
            if (activeNetwork != null) {
                isMobile = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE;
            }
            if (isConnected && isMobile) {
                return true;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public String formatDateTime(java.util.Date date) {
        SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault());
        if (date == null) {
            return "";
        } else {
            return DATE_TIME_FORMAT.format(date);
        }
    }


    private void addToQueue(final GPSPointsDetail jsonStringer, final DatabaseHandler databaseHandler, final String bugPath) {
        Log.e(TAG, "................retro...........Trig");
        //final Call<PointsResponse> call = getRetrofit().sendPoints(jsonStringer);
        if (apiInterface != null) {
            final Call<PointsResponse> call = apiInterface.sendPoints(jsonStringer);

            call.enqueue(new Callback<PointsResponse>() {
                @Override
                public void onResponse(Call<PointsResponse> call, Response<PointsResponse> response) {
                    try {
                        if (response != null) {
                            if (response.isSuccessful()) {
                                //long currentId = jsonStringer.getLong("rowid");
                                long currentId = jsonStringer.getRowid();
                                new com.freshorders.freshorder.utils.
                                        Log(bugPath).
                                        ee(TAG, ".........currentId...." + currentId + ".....Success:::");
                                Log.e(TAG, "..........success....." + response.toString());
                                if (response.body().getStatus().equals("true")) {
                                    Log.e(TAG, "..........success.....Data::::" + response.body().getData());
                                    Log.e(TAG, "..........success.....Request::pre_id::" + jsonStringer.getPre_id());
                                    //long currentId = jsonStringer.getLong("rowid");
                                    databaseHandler.setDistancePointSuccess(currentId);
                                    //databaseHandler.deleteSuccessLocationById(jsonStringer.getLong("pre_id"));
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<PointsResponse> call, Throwable throwable) {
                    try {
                        //long currentId = jsonStringer.getLong("rowid");
                        long currentId = jsonStringer.getRowid();
                        new com.freshorders.freshorder.utils.
                                Log(bugPath).
                                ee(TAG, ".........currentId...." + currentId + ".....Reason:::" + throwable.getMessage());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
        } else {
            new com.freshorders.freshorder.utils.Log(bugPath).ee(TAG, "............Retrofit not created.");
        }

    }


    public synchronized void sendPointsThroughRetrofit(long rowId,
                                                       DatabaseHandler databaseHandler, String bugPath) {
        Log.e(TAG, "...........................Trig");

        try {

            if (Constants.USER_ID == null || Constants.USER_ID.isEmpty()) {
                Cursor curs;
                curs = databaseHandler.getDetails();
                if (curs != null && curs.getCount() > 0) {
                    curs.moveToFirst();
                    Constants.USER_ID = curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_id));
                    Log.e("Constants.USER_ID", Constants.USER_ID);
                    curs.close();
                } else {
                    return;
                }
            }

            //Cursor cursor = databaseHandler.getUnSyncLocationsById(rowId);
            Cursor cursor = databaseHandler.getAllUnSyncLocation();
            if (cursor != null && cursor.getCount() > 0) {
                List<GPSPointsDetail> bulkList = new ArrayList<>();

                cursor.moveToFirst();
                for (int i = 0; i < cursor.getCount() - 1; i++) { ///////////////can't send last item for maintain previous id
                    Log.e(TAG, "...........................dataFound::" + i);
                    rowId = cursor.getLong(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_SERIAL_NO));
                    Location start = new Location("locationA");
                    Location end = new Location("locationB");
                    double lat = 0, lon = 0, preLat = 0, preLon = 0;
                    JSONObject item = new JSONObject();
                    item.put("rowid", rowId);
                    item.put("batchid", 0);
                    item.put("suserid", Constants.USER_ID);
                    item.put("logtime", cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_DATE_TIME)));
                    lat = cursor.getDouble(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_LAT));
                    lon = cursor.getDouble(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_LON));
                    start.setLatitude(lat);
                    start.setLongitude(lon);

                    preLat = cursor.getDouble(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_P_LAT));
                    preLon = cursor.getDouble(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_P_LON));
                    end.setLatitude(preLat);
                    end.setLongitude(preLon);


                    long preId = cursor.getLong(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_PREVIOUS_ID));
                    /*Cursor curs = databaseHandler.getLocationsForPreById(preId);
                    if (curs != null && curs.getCount() > 0) {
                        Log.e(TAG, "...........................previousFound :; " + preId);
                        curs.moveToFirst();
                        preLat = curs.getDouble(curs.getColumnIndex(DatabaseHandler.KEY_LOCATION_LAT));
                        preLon = curs.getDouble(curs.getColumnIndex(DatabaseHandler.KEY_LOCATION_LON));
                        end.setLatitude(preLat);
                        end.setLongitude(preLon);
                        curs.close();
                        isPreIdAvail = true;
                    } else {
                        isPreIdAvail = false;
                    }  */
                    double temp_distance = 0;
                    if (isPreIdAvail) {
                        temp_distance = start.distanceTo(end);
                        temp_distance = temp_distance / 1000;
                    }
                    Log.e(TAG, ".................temp_distance::::KM::" + temp_distance);

                    item.put("geolat", lat);
                    item.put("geolong", lon);
                    item.put("pgeolat", preLat);
                    item.put("pgeolong", preLon);
                    item.put("status", "Active");
                    item.put("updateddt", cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_DATE_TIME)));
                    item.put("distance", temp_distance);
                    item.put("amount", 0);
                    item.put("issynced", 1);
                    item.put("pre_id", cursor.getLong(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_PREVIOUS_ID)));
                    item.put("seqid", cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_UNIQUE_ID)));

                    GPSPointsDetail obj = new GPSPointsDetail(rowId, 0, Constants.USER_ID, cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_DATE_TIME)),
                            lat, lon, preLat, preLon, "Active", cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_DATE_TIME)),
                            temp_distance, 0, 1, cursor.getLong(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_PREVIOUS_ID)),
                            cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_UNIQUE_ID)));

                    //addToQueue(item,databaseHandler, bugPath);/////////////////
                    bulkList.add(obj);
                    cursor.moveToNext();
                }
                cursor.close();
                try {
                    if (isNetAvail(getApplicationContext())) {
                        if (bulkList.size() > 0) {
                            bulkQueue(bulkList, databaseHandler, bugPath);/////////////////
                        } else {
                            new com.freshorders.freshorder.utils.Log(bugPath).ee(TAG, "............NO data..Avail..to Push..intervalId.." + intervalId);
                        }
                    } else {
                        Log.e(TAG, "Mobile Data Not Avail......");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    new com.freshorders.freshorder.utils.Log(bugPath).ee(TAG, "............." + e.getMessage());
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void bulkQueue(List<GPSPointsDetail> bulkList, final DatabaseHandler databaseHandler, final String bugPath) {
        new com.freshorders.freshorder.utils.Log(bugPath).ee(TAG, "interval..........intervalId:: " + intervalId);
        Log.e(TAG, "................retro...........Trig");
        //final Call<PointsResponse> call = getRetrofit().sendPoints(jsonStringer);
        if (apiInterface != null) {
            final Call<PointsResponse> call = apiInterface.sendPoints(bulkList);

            call.enqueue(new Callback<PointsResponse>() {
                @Override
                public void onResponse(Call<PointsResponse> call, Response<PointsResponse> response) {
                    try {
                        if (response != null) {
                            if (response.isSuccessful()) {
                                Log.e(TAG, "......................Success");
                                new com.freshorders.freshorder.utils.
                                        Log(bugPath).
                                        ee(TAG, ".........currentId........Success:::" + response.body().getData());
                                PointsResponse res = response.body();
                                if (res != null) {
                                    String status = res.getStatus();
                                    if (status.equalsIgnoreCase("true")) {
                                        List<GPSSubResponse> list = res.getData();
                                        if (list != null && list.size() > 0) {
                                            List<Long> idList = new ArrayList<>();
                                            String[] selectionArgs = new String[list.size()];
                                            for (int i = 0; i < list.size() - 1; i++) { ///last item need
                                                String duplicate = list.get(i).getIsduplicate();
                                                String success = list.get(i).getSuccess();
                                                String id = list.get(i).getId();
                                                if (duplicate.equalsIgnoreCase("true") ||
                                                        success.equalsIgnoreCase("true")) {
                                                    idList.add(Long.parseLong(id));
                                                    selectionArgs[i] = id;
                                                }
                                            }

                                            String lastSequenceId = list.get(list.size() - 1).getId();
                                            if (lastSequenceId != null && !lastSequenceId.isEmpty()) {
                                                databaseHandler.setDistancePointSuccessBySequence(Long.parseLong(lastSequenceId));
                                            }
                                            String selectionClause = DatabaseHandler.KEY_LOCATION_UNIQUE_ID + " = ?";
                                            int deletedSize = 0;
                                            deletedSize = databaseHandler.removeAllSuccessfulPoints(selectionClause, selectionArgs);
                                            if (deletedSize > 0) {
                                                new com.freshorders.freshorder.utils.
                                                        Log(bugPath).
                                                        ee(TAG, ".........successfully removed points in local table:::deletedSize :: " + deletedSize);
                                            }
                                        }
                                    } else {
                                        Log.e(TAG, ".............unsuccessful response");
                                    }
                                }

                            } else {
                                Log.e(TAG, "......................Fail");
                                new com.freshorders.freshorder.utils.
                                        Log(bugPath).
                                        ee(TAG, ".........currentId........Fail:::" + response.toString());
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<PointsResponse> call, Throwable throwable) {
                    try {
                        Log.e(TAG, "......................Fail");
                        new com.freshorders.freshorder.utils.
                                Log(bugPath).
                                ee(TAG, ".........currentId.......Reason:::" + throwable.getMessage());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
        } else {
            new com.freshorders.freshorder.utils.Log(bugPath).ee(TAG, "............Retrofit not created.");
        }

    }


    public void sendSinglePointsThroughRetrofit(long rowId,
                                                DatabaseHandler databaseHandler, String bugPath) {
        Log.e(TAG, "...........................Trig");

        try {

            if (Constants.USER_ID == null || Constants.USER_ID.isEmpty()) {
                Cursor curs;
                curs = databaseHandler.getDetails();
                if (curs != null && curs.getCount() > 0) {
                    curs.moveToFirst();
                    Constants.USER_ID = curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_id));
                    Log.e("Constants.USER_ID", Constants.USER_ID);
                    curs.close();
                } else {
                    return;
                }
            }

            Cursor cursor = databaseHandler.getUnSyncLocationsById(rowId);
            if (cursor != null && cursor.getCount() > 0) {
                Log.e(TAG, "...........................dataFound");
                cursor.moveToFirst();
                Location start = new Location("locationA");
                Location end = new Location("locationB");
                double lat = 0, lon = 0, preLat = 0, preLon = 0;
                JSONObject item = new JSONObject();
                item.put("rowid", rowId);
                item.put("batchid", 0);
                item.put("suserid", Constants.USER_ID);
                item.put("logtime", cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_DATE_TIME)));
                lat = cursor.getDouble(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_LAT));
                lon = cursor.getDouble(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_LON));
                start.setLatitude(lat);
                start.setLongitude(lon);
                long preId = cursor.getLong(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_PREVIOUS_ID));
                Cursor curs = databaseHandler.getLocationsForPreById(preId);
                if (curs != null && curs.getCount() > 0) {
                    Log.e(TAG, "...........................previousFound ::" + preId);
                    curs.moveToFirst();
                    preLat = curs.getDouble(curs.getColumnIndex(DatabaseHandler.KEY_LOCATION_LAT));
                    preLon = curs.getDouble(curs.getColumnIndex(DatabaseHandler.KEY_LOCATION_LON));
                    end.setLatitude(preLat);
                    end.setLongitude(preLon);
                    curs.close();
                } else {
                    return;
                }

                double temp_distance = start.distanceTo(end);
                temp_distance = temp_distance / 1000;
                Log.e(TAG, ".................temp_distance::::KM::" + temp_distance);

                item.put("geolat", lat);
                item.put("geolong", lon);
                item.put("pgeolat", preLat);
                item.put("pgeolong", preLon);
                item.put("status", "Active");
                item.put("updateddt", cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_DATE_TIME)));
                item.put("distance", temp_distance);
                item.put("amount", 0);
                item.put("issynced", 1);
                item.put("pre_id", cursor.getLong(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_PREVIOUS_ID)));
                item.put("seqid", cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_UNIQUE_ID)));

                GPSPointsDetail obj = new GPSPointsDetail(rowId, 0, Constants.USER_ID, cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_DATE_TIME)),
                        lat, lon, preLat, preLon, "Active", cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_DATE_TIME)),
                        temp_distance, 0, 1, cursor.getLong(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_PREVIOUS_ID)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_UNIQUE_ID)));

                //addToQueue(item,databaseHandler, bugPath);/////////////////
                addToQueue(obj, databaseHandler, bugPath);/////////////////
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setActivityListener(){

        mActivityRecognitionClient = new ActivityRecognitionClient(this);
        mIntentService = new Intent(this, UserActivityIntentService.class);
        mPendingIntent = PendingIntent.getService(this, USER_ACTIVITY_RQ_CODE, mIntentService, PendingIntent.FLAG_UPDATE_CURRENT);
        Task<Void> task = mActivityRecognitionClient.requestActivityUpdates(
                Constants.DETECTION_INTERVAL_IN_MILLISECONDS,
                mPendingIntent);

        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void result) {
                Toast.makeText(getApplicationContext(),
                        "Successfully requested activity updates",
                        Toast.LENGTH_SHORT)
                        .show();
            }
        });

        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getApplicationContext(),
                        "Requesting activity updates failed to start",
                        Toast.LENGTH_SHORT)
                        .show();
            }
        });
    }

    private void removeUserActivityListener(){
        if(mActivityRecognitionClient != null) {
            Task<Void> task = mActivityRecognitionClient.removeActivityUpdates(
                    mPendingIntent);
            task.addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void result) {
                    Toast.makeText(getApplicationContext(),
                            "Removed User Activity Updates Successfully!",
                            Toast.LENGTH_SHORT)
                            .show();
                }
            });

            task.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(getApplicationContext(), "Failed to remove activity updates!",
                            Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @Override
    public void onGpsStatusChanged(int i) {

    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    private SensorManager mSensorManager;
    private Sensor sensor;
    public static final int rate = SensorManager.SENSOR_DELAY_FASTEST;

    protected float lastX;
    protected float lastY;
    protected float lastZ;
    private static final int BUFFER_SIZE = 500;
    // calibration
    private float dX = 0;
    private float dY = 0;
    private float dZ = 0;
    // buffer variables
    private float X;
    private float Y;
    private float Z;
    private int cnt = 0;

    private void setAccelerometer() {
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        //sensor = mSensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER).get(0);
        List<Sensor> sensors = mSensorManager.getSensorList(Sensor.TYPE_ACCELEROMETER);
        sensor = sensors.size() == 0 ? null : sensors.get(0);
        if (sensor != null) {
            mSensorManager.registerListener(this, sensor, rate);
        }
    }

    private void processSensorUpdate(SensorEvent sensorEvent) {
        float[] values = sensorEvent.values;
        if (values.length < 3)
            return;

    }


}