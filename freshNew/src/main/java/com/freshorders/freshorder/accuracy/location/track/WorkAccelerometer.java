package com.freshorders.freshorder.accuracy.location.track;

import android.hardware.SensorEvent;
import android.hardware.SensorManager;

public class WorkAccelerometer extends Accelerometer {

    private static final int BUFFER_SIZE = 500;
    // calibration
    private  float dX = 0;
    private  float dY = 0;
    private  float dZ = 0;
    // buffer variables
    private float X;
    private float Y;
    private float Z;
    private int cnt = 0;

    // returns last SenorEvent parameters
    public Point getLastPoint(){
        return new Point(lastX, lastY, lastZ, 1);
    }

    // returrns parameters, using buffer: average acceleration
    // since last call of getPoint().
    public Point getPoint(){

        if (cnt == 0){
            return new Point(lastX, lastY, lastZ, 1);
        }

        Point p =  new Point(X, Y, Z, cnt);

        reset();
        return p;
    }

    // resets buffer
    public void reset(){
        cnt = 0;
        X = 0;
        Y = 0;
        Z = 0;
    }


    public void onSensorChanged(SensorEvent se) {

        final float alpha = 0.8f;
        //gravity is calculated here
        float x;
        float y;
        float z;

        float[] gravityV = new float[3];
        gravityV[0] = alpha * gravityV[0] + (1 - alpha) * se.values[0];
        gravityV[1] = alpha * gravityV[1] + (1 - alpha)* se.values[1];
        gravityV[2] = alpha * gravityV[2] + (1 - alpha) * se.values[2];
        //acceleration retrieved from the event and the gravity is removed
        x = se.values[0] - gravityV[0];
        y = se.values[1] - gravityV[1];
        z = se.values[2] - gravityV[2];

         x = x + dX;
         y = y + dY;
         z = z + dZ;

        lastX = x;
        lastY = y;
        lastZ = z;

        X+= x;
        Y+= y;
        Z+= z;

        if (cnt < BUFFER_SIZE-1) {
            cnt++;
        } else
        {
            reset();
        }
    }

    public int getCnt(){
        return cnt;
    }

    public  void setdX(float dX) {
        this.dX = dX;
    }

    public  void setdY(float dY) {
        this.dY = dY;
    }

    public  void setdZ(float dZ) {
        this.dZ = dZ;
    }
}
