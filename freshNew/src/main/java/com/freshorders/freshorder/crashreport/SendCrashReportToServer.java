package com.freshorders.freshorder.crashreport;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.freshorders.freshorder.ui.MerchantComplaintActivity;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.UploadImagePostNote;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;

public class SendCrashReportToServer {
    private static final String TAG = SendCrashReportToServer.class.getSimpleName();
    private Context mContext;
    private String path = "";
    private static String filePath = "";
    private Context curContext;

    public SendCrashReportToServer(Context context, String path) {
        this.mContext = context.getApplicationContext();
        this.path = path;
        filePath = path;
        this.curContext = context;
    }

    private static boolean clearOldDateBugFiles(){

        String directoryPath = FileUtils.getParent(filePath);
        if( directoryPath != null) {
            String inputPattern = "dd-MM-yyyy";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.getDefault());
            String dt = inputFormat.format(new Date());

            File directory = new File(directoryPath);
            if (!directory.exists() || !directory.isDirectory()) {
                Log.e(TAG, "Crash Root Directory Not Founding");
                return false;
            }
            ArrayList<File> listOfFiles = new ArrayList<>(Arrays.asList(directory.listFiles()));

            for (File crashFile : listOfFiles) {
                if (!(crashFile.getName().contains(dt))) {
                    Log.e(TAG, "Root Folder Contain Files.............."+ listOfFiles.size());
                    if (crashFile.getName().contains(AppCrashConstants.CRASH_SUFFIX)) {
                        if (!FileUtils.delete(crashFile)) {
                            Log.e(TAG, "Deleting Old Files Got Problem...........");
                            return false;
                        }
                    }
                }
            }
        }else {
            Log.e(TAG,"Crash Root Folder Can not Found");
            return false;
        }
        return true;
    }

    public void sendCrashFile(){
        if(clearOldDateBugFiles()){
            AppCrashUtil.writeStringToBugFile(path, MobileAppUtil.getDeviceDetails(mContext));
            UploadImagePostNote ufs = new UploadImagePostNote(curContext,
                    mContext, path, Constants.EMPTY, Constants.EMPTY, Constants.EMPTY, " Report Sending To Support Team....");
            ufs.execute();
        }else {
            Toast.makeText(mContext,"Report File Not Moving..",Toast.LENGTH_LONG).show();
        }
    }



/*

    public static class CrashReportAsyncTask extends AsyncTask<Void, Void, String> {
        private ProgressDialog Dialog = new ProgressDialog();

        @Override
        protected void onPreExecute() {

            Dialog.setMessage(" Report Sending To Support Team....");
            Dialog.show();
        }

        @Override
        protected String doInBackground(Void... voids) {
            if(clearOldDateBugFiles()){

            }else {

            }
            return Constants.EMPTY;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.e("BugReport","Done by .....................................");
            Dialog.dismiss();
        }

    }

    */
}
