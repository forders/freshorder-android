package com.freshorders.freshorder.crashreport;

public class AppCrashReporterException extends RuntimeException {

    public AppCrashReporterException() {
        super();
    }

    public AppCrashReporterException(String message) {
        super(message);
    }

    public AppCrashReporterException(Throwable throwable) {
        super(throwable);
    }

    public AppCrashReporterException(String message, Throwable throwable) {
        super(message, throwable);
    }

    @Override
    public String toString() {
        return getMessage();
    }
}
