package com.freshorders.freshorder.crashreport;


import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.service.PrefManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;

public class AppCrashUtil {

    private static final String TAG = AppCrashUtil.class.getSimpleName();

    private static String getCrashLogTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        return dateFormat.format(new Date());
    }

    public static void writeCrashToCrashFile(final Throwable throwable){
        String crashPath = AppCrashReporter.getCrashReportPath();
        if(crashPath.isEmpty()){
            crashPath = MyApplication.getInstance().getCrashReporterPath();
        }
        Log.d(TAG, "crash report path : " + crashPath);
        if(!crashPath.isEmpty()) {
            BufferedWriter bufferedWriter;
            String crashTime = getCrashLogTime() + "\n";
            try {
                bufferedWriter = new BufferedWriter(new FileWriter(crashPath, true));
                bufferedWriter.write(crashTime);///////
                bufferedWriter.write(getStackTrace(throwable));
                bufferedWriter.write("\n");///////
                bufferedWriter.flush();
                bufferedWriter.close();
                Log.d(TAG, "crash report saved in : " + crashPath);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static String getStackTrace(Throwable e) {
        final Writer result = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(result);
        e.printStackTrace(printWriter);
        String crashLog = result.toString();
        printWriter.close();
        return crashLog;
    }

    public static void initialCrashCustomerInfo(String cusInfo){
        BufferedWriter bufferedWriter;
        String crashTime = getCrashLogTime() + "\t";
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(AppCrashReporter.getCrashReportPath(), true));
            bufferedWriter.write(crashTime);///////
            bufferedWriter.write(cusInfo);
            bufferedWriter.write("\n");///////
            bufferedWriter.flush();
            bufferedWriter.close();
            Log.d(TAG, "crash report saved in : " + AppCrashReporter.getCrashReportPath());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void writeStringToBugFile(String path, String bugInfo){
        android.util.Log.e("BugFile", ".......Path::" + path);
        BufferedWriter bufferedWriter;
        String crashTime = getCrashLogTime() + "\t";
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(path, true));
            bufferedWriter.write("Reporting Time :");///////
            bufferedWriter.write(crashTime);///////
            bufferedWriter.write(bugInfo);
            bufferedWriter.write("\n");///////
            bufferedWriter.flush();
            bufferedWriter.close();
            Log.d(TAG, "crash report saved in : " + path);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }






    public static void saveAppCrashReport(final Throwable throwable) {

        String crashReportPath = AppCrashReporter.getCrashReportPath();
        String filename = AppCrashConstants.CRASH_PREFIX + AppCrashConstants.CRASH_SUFFIX + AppCrashConstants.FILE_EXTENSION;
        if(crashReportPath != null) {
            writeToCrashFile(crashReportPath, filename, getStackTrace(throwable));
        }
    }

    public static void initialCrashWrite(String cusDetail){
        String crashReportPath = AppCrashReporter.getCrashReportPath();
        if(crashReportPath != null) {
            String filename = AppCrashConstants.CRASH_PREFIX + AppCrashConstants.CRASH_SUFFIX + AppCrashConstants.FILE_EXTENSION;
            File isCrashFile = new File(crashReportPath + File.separator + filename);
            if(!FileUtils.exists(isCrashFile)){
                writeToCrashFile(crashReportPath, filename, cusDetail);
            }
        }

    }

    private static void writeToCrashFile(String crashReportPath, String filename, String crashLog) {

        if (TextUtils.isEmpty(crashReportPath)) {
            crashReportPath = getDefaultPath();
        }

        assert crashReportPath != null;
        File crashDir = new File((crashReportPath));
        if (!crashDir.exists() || !crashDir.isDirectory()) {
            crashReportPath = getDefaultPath();
            Log.e(TAG, "Given Path For AppCrash doesn't exists : " + crashDir + "\nSaving crash report at : " + getDefaultPath());
        }

        BufferedWriter bufferedWriter;
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(
                    crashReportPath + File.separator + filename));

            bufferedWriter.write(crashLog);
            bufferedWriter.flush();
            bufferedWriter.close();
            Log.d(TAG, "crash report saved in : " + crashReportPath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getDefaultPath() {

        Context context = AppCrashReporter.getContext();
        File appCrashDir;
        String relativePath = AppCrashConstants.APP_ROOT_FOLDER + File.separator +AppCrashConstants.CRASH_REPORT_DIR;

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            appCrashDir = new File(Environment.getExternalStorageDirectory(), relativePath);
        } else {
            appCrashDir = new File(context.getFilesDir(), relativePath);
        }
        if (!appCrashDir.exists()) {
            if (!appCrashDir.mkdirs()) {
                return null;
            }
        }
        return appCrashDir.getAbsolutePath();
    }

    public static String getInitialDefaultPath(Context context) {

        /////Context context = AppCrashReporter.getContext();
        File appCrashDir;
        String relativePath = AppCrashConstants.APP_ROOT_FOLDER + File.separator + AppCrashConstants.CRASH_REPORT_DIR;

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            appCrashDir = new File(Environment.getExternalStorageDirectory(), relativePath);
        } else {
            appCrashDir = new File(context.getFilesDir(), relativePath);
        }
        if (!appCrashDir.exists()) {
            if (!appCrashDir.mkdirs()) {
                return null;
            }
        }
        return appCrashDir.getAbsolutePath();
    }


    public static ArrayList<File> getAllAppCrashFile(Context context) {
        String directoryPath;
        directoryPath = AppCrashUtil.getDefaultPath();

        File directory = new File(directoryPath);
        if (!directory.exists() || !directory.isDirectory()) {
            throw new AppCrashReporterException("The path provided doesn't exists : " + directoryPath);
        }
        ArrayList<File> listOfFiles = new ArrayList<>(Arrays.asList(directory.listFiles()));

        for(File crashFile: listOfFiles){
            if (!(crashFile.getName().contains(AppCrashConstants.CRASH_SUFFIX))) {
                listOfFiles.remove(crashFile);
            }
        }
        if(listOfFiles.size() > 0) {
            Collections.sort(listOfFiles, Collections.reverseOrder());
        }
        return listOfFiles;
    }


}
