package com.freshorders.freshorder.crashreport;

import android.content.Context;
public class AppCrashReporter {

    private static Context appContext;
    private static String crashReportPath;

    public static void setCrashFilePath(String path){
        crashReportPath = path;
    }

    private AppCrashReporter() {
    }

    public static void initialize(Context context) {
        appContext = context;
        setCustomExceptionHandler();
    }

    public static void initialize(Context context, String crashReportSavePath) {
        appContext = context;
        crashReportPath = crashReportSavePath;
        setCustomExceptionHandler();
    }

    public static String getCrashReportPath() {
        return crashReportPath;
    }

    private static void setCustomExceptionHandler() {
        if (!(Thread.getDefaultUncaughtExceptionHandler() instanceof AppCrashReporterExceptionHandler)) {
            Thread.setDefaultUncaughtExceptionHandler(new AppCrashReporterExceptionHandler(appContext, CrashReportActivity.class));
        }
    }

    public static Context getContext() {
        if (appContext == null) {
            try {
                throw new AppCrashReporterException("App Crash Report Initialize Failed");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return appContext;
    }

}
