package com.freshorders.freshorder.crashreport;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.freshorders.freshorder.R;

public class CrashReportActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crash_report);
        String bug = "";
        Intent intent  = getIntent();
        if(intent != null){
            if(intent.hasExtra("STACKTRACE")){
                bug = intent.getStringExtra("MERCHANT_NAME");
            }
        }
        TextView tvBug = findViewById(R.id.id_menu_mig_TV_status);
        tvBug.setText(bug);
        Button btnClose = findViewById(R.id.id_crash_btn_close);
        btnClose.setOnClickListener(close);

    }

    View.OnClickListener close = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };
}
