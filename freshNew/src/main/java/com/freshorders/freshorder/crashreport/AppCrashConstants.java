package com.freshorders.freshorder.crashreport;

public class AppCrashConstants {

    public static final String APP_ROOT_FOLDER = "FreshOrders";
    public static final String CRASH_PREFIX = "today";
    public static final String CRASH_SUFFIX = "_crash";
    public static final String FILE_EXTENSION = ".txt";
    public static final String CRASH_REPORT_DIR = "FreshOrdersCrashReport";
    public static final String LOG_TAG_EXTERNAL_STORAGE = "EXTERNAL_STORAGE";
    public static final int REQUEST_CODE_WRITE_EXTERNAL_STORAGE_PERMISSION = 1;

    public static String USER_DETAILS = "";

    public static final String MIGRATION_SUFFIX = "_migration";
    public static final String MIG_FOLDER = "Migration";
    public static final String FOLDER_HIERARCHY = "hierarchy";

}
