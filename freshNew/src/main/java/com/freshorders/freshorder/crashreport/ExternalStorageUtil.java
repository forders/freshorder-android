package com.freshorders.freshorder.crashreport;


import android.content.Context;
import android.os.Environment;
import android.os.StatFs;

import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.utils.Constants;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import eu.janmuller.android.simplecropimage.Util;

import static com.freshorders.freshorder.service.PrefManager.KEY_PATH_CRASH_FILE;
import static com.freshorders.freshorder.service.PrefManager.KEY_PATH_HIERARCHY_PLACE_FILE;
import static com.freshorders.freshorder.service.PrefManager.KEY_PATH_MIGRATION_FILE;
import static com.freshorders.freshorder.utils.Constants.PLACE_HIERARCHY_FILE_JSON;

public class ExternalStorageUtil {

    // Check whether the external storage is mounted or not.
    public static boolean isExternalStorageMounted() {

        String dirState = Environment.getExternalStorageState();
        if(Environment.MEDIA_MOUNTED.equals(dirState))
        {
            return true;
        }else
        {
            return false;
        }
    }

    // Check whether the external storage is read only or not.
    public static boolean isExternalStorageReadOnly() {

        String dirState = Environment.getExternalStorageState();
        if(Environment.MEDIA_MOUNTED_READ_ONLY.equals(dirState))
        {
            return true;
        }else
        {
            return false;
        }
    }

    public static String createOrGetCrashFolderFile(Context context){
        try {
            String rootDir;
            String inputPattern = "dd-MM-yyyy";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.getDefault());
            String dt = inputFormat.format(new Date());
            String crashFile = dt + AppCrashConstants.CRASH_SUFFIX + AppCrashConstants.FILE_EXTENSION;

            String relativePath = AppCrashConstants.APP_ROOT_FOLDER + File.separator + AppCrashConstants.CRASH_REPORT_DIR;
            if (isExternalStorageMounted()) {
                File file = Environment.getExternalStorageDirectory();
                rootDir = file.getAbsolutePath();
            } else {
                File file = context.getFilesDir();
                rootDir = file.getAbsolutePath();
            }
            File crashDir = new File(rootDir, relativePath);
            if (!crashDir.exists()) {
                if (!crashDir.mkdirs()) {
                    return Constants.EMPTY;
                }
            }
            File fileCrash = new File(crashDir, crashFile);
            if(!fileCrash.exists()){
                if(fileCrash.createNewFile()){
                    new PrefManager(context).setStringDataByKey(KEY_PATH_CRASH_FILE, fileCrash.getAbsolutePath());
                    return fileCrash.getAbsolutePath();
                }
            }else {
                return fileCrash.getAbsolutePath();
            }
            return Constants.EMPTY;
        }catch (Exception e){
            e.printStackTrace();
            return Constants.EMPTY;
        }
    }

    public static String createOrGetMigrationFolderFile(Context context){
        try {
            String rootDir;
            String inputPattern = "dd-MM-yyyy";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.getDefault());
            String dt = inputFormat.format(new Date());
            String crashFile = dt + AppCrashConstants.MIGRATION_SUFFIX + AppCrashConstants.FILE_EXTENSION;

            String relativePath = AppCrashConstants.APP_ROOT_FOLDER + File.separator + AppCrashConstants.MIG_FOLDER;
            if (isExternalStorageMounted()) {
                File file = Environment.getExternalStorageDirectory();
                rootDir = file.getAbsolutePath();
            } else {
                File file = context.getFilesDir();
                rootDir = file.getAbsolutePath();
            }
            File crashDir = new File(rootDir, relativePath);
            if (!crashDir.exists()) {
                if (!crashDir.mkdirs()) {
                    return Constants.EMPTY;
                }
            }
            File fileCrash = new File(crashDir, crashFile);
            if(!fileCrash.exists()){
                if(fileCrash.createNewFile()){
                    new PrefManager(context).setStringDataByKey(KEY_PATH_MIGRATION_FILE, fileCrash.getAbsolutePath());
                    return fileCrash.getAbsolutePath();
                }
            }else {
                return fileCrash.getAbsolutePath();
            }
            return Constants.EMPTY;
        }catch (Exception e){
            e.printStackTrace();
            return Constants.EMPTY;
        }
    }

    public static String createOrGetHierarchyFolderFile(Context context){
        try {
            String rootDir;
            //String inputPattern = "dd-MM-yyyy";
            //SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.getDefault());
            //String dt = inputFormat.format(new Date());
            String crashFile = PLACE_HIERARCHY_FILE_JSON;

            String relativePath = AppCrashConstants.APP_ROOT_FOLDER + File.separator + AppCrashConstants.FOLDER_HIERARCHY;
            if (isExternalStorageMounted()) {
                File file = Environment.getExternalStorageDirectory();
                rootDir = file.getAbsolutePath();
            } else {
                File file = context.getFilesDir();
                rootDir = file.getAbsolutePath();
            }
            File crashDir = new File(rootDir, relativePath);
            if (!crashDir.exists()) {
                if (!crashDir.mkdirs()) {
                    return Constants.EMPTY;
                }
            }
            File fileCrash = new File(crashDir, crashFile);
            if(!fileCrash.exists()){
                if(fileCrash.createNewFile()){
                    new PrefManager(context).setStringDataByKey(KEY_PATH_HIERARCHY_PLACE_FILE, fileCrash.getAbsolutePath());
                    return fileCrash.getAbsolutePath();
                }
            }else {
                return fileCrash.getAbsolutePath();
            }
            return Constants.EMPTY;
        }catch (Exception e){
            e.printStackTrace();
            return Constants.EMPTY;
        }
    }

    public static void deleteHierarchyPlaceFile(String path, Context context){
        File fdelete = new File(path);
        if (fdelete.exists()) {
            if (fdelete.delete()) {
                System.out.println("file Deleted :" + path);
                new PrefManager(context).setStringDataByKey(KEY_PATH_HIERARCHY_PLACE_FILE, "");
            } else {
                System.out.println("file Deleted Failed..............:" + path);
            }
        }else {
            //String hierarchyFile = PLACE_HIERARCHY_FILE_JSON;
            String rootDir;
            String relativePath = AppCrashConstants.APP_ROOT_FOLDER + File.separator + AppCrashConstants.FOLDER_HIERARCHY;
            if (isExternalStorageMounted()) {
                File file = Environment.getExternalStorageDirectory();
                rootDir = file.getAbsolutePath();
            } else {
                File file = context.getFilesDir();
                rootDir = file.getAbsolutePath();
            }
            File del = new File(rootDir, relativePath);
            if(del.exists()){
                if(deleteNon_EmptyDir(del)){
                    System.out.println("Folder and its files Deleted :" + relativePath);
                }else {
                    System.out.println("Folder not Deleted :" + relativePath);
                }
            }else {
                System.out.println("Not Require to Delete File or Folder :");
            }
        }
    }

    /* IF THE DIRECTORY IS NOT EMPTY . USE THIS FUNCTION */
    public static boolean deleteNon_EmptyDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (String child : children) {
                boolean success = deleteNon_EmptyDir(new File(dir, child));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

    // Get private external storage base directory.
    public static String getPrivateExternalStorageBaseDir(Context context, String dirType)
    {
        String ret = "";
        if(isExternalStorageMounted()) {
            File file = context.getExternalFilesDir(dirType);
            ret = file.getAbsolutePath();
        }
        return ret;
    }

    // Get private cache external storage base directory.
    public static String getPrivateCacheExternalStorageBaseDir(Context context)
    {
        String ret = "";
        if(isExternalStorageMounted()) {
            File file = context.getExternalCacheDir();
            ret = file.getAbsolutePath();
        }
        return ret;
    }


    // Get public external storage base directory.
    public static String getPublicExternalStorageBaseDir()
    {
        String ret = "";
        if(isExternalStorageMounted()) {
            File file = Environment.getExternalStorageDirectory();
            ret = file.getAbsolutePath();
        }
        return ret;
    }

    // Get public external storage base directory.
    public static String getPublicExternalStorageBaseDir(String dirType)
    {
        String ret = "";
        if(isExternalStorageMounted()) {
            File file = Environment.getExternalStoragePublicDirectory(dirType);
            ret = file.getAbsolutePath();
        }
        return ret;
    }

    // Get external storage disk space, return MB
    public static long getExternalStorageSpace() {
        long ret = 0;
        if (isExternalStorageMounted()) {
            StatFs fileState = new StatFs(getPublicExternalStorageBaseDir());

            // Get total block count.
            long count = fileState.getBlockCountLong();

            // Get each block size.
            long size = fileState.getBlockSizeLong();

            // Calculate total space size
            ret = count * size / 1024 / 1024;
        }
        return ret;
    }

    // Get external storage left free disk space, return MB
    public static long getExternalStorageLeftSpace() {
        long ret = 0;
        if (isExternalStorageMounted()) {
            StatFs fileState = new StatFs(getPublicExternalStorageBaseDir());

            // Get free block count.
            long count = fileState.getFreeBlocksLong();

            // Get each block size.
            long size = fileState.getBlockSizeLong();

            // Calculate free space size
            ret = count * size / 1024 / 1024;
        }
        return ret;
    }

    // Get external storage available disk space, return MB
    public static long getExternalStorageAvailableSpace() {
        long ret = 0;
        if (isExternalStorageMounted()) {
            StatFs fileState = new StatFs(getPublicExternalStorageBaseDir());

            // Get available block count.
            long count = fileState.getAvailableBlocksLong();

            // Get each block size.
            long size = fileState.getBlockSizeLong();

            // Calculate available space size
            ret = count * size / 1024 / 1024;
        }
        return ret;
    }


}
