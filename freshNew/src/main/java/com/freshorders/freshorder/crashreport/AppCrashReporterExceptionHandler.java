package com.freshorders.freshorder.crashreport;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.freshorders.freshorder.service.PrefManager;

import java.io.PrintWriter;
import java.io.StringWriter;

import static com.freshorders.freshorder.service.PrefManager.KEY_IS_CRASH_HAPPENED;

public class AppCrashReporterExceptionHandler implements Thread.UncaughtExceptionHandler {

    private Thread.UncaughtExceptionHandler uncaughtExceptionHandler;
    private Context myContext = null;
    private Class<?> intentClass;

    public AppCrashReporterExceptionHandler(Context context, Class<?> intentClass) {
        this.myContext = context;
        this.intentClass = intentClass;
        this.uncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
    }


    public AppCrashReporterExceptionHandler() {
        this.uncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
    }

    @Override
    public void uncaughtException(Thread thread, Throwable throwable) {
        Log.e("AppCrash","Reporter..............Reach"+throwable.getMessage());
        AppCrashUtil.writeCrashToCrashFile(throwable);
        new PrefManager(myContext).setBooleanDataByKey(KEY_IS_CRASH_HAPPENED, true);
        ////////////////////////////////uncaughtExceptionHandler.uncaughtException(thread, throwable);
        StringWriter stackTrace = new StringWriter();
        throwable.printStackTrace(new PrintWriter(stackTrace));
        System.err.println(stackTrace);

        Intent intent = new Intent(myContext, intentClass);
        String s = stackTrace.toString();
        //you can use this String to know what caused the exception and in which Activity
        intent.putExtra("EXCEPTION", "Exception is: " + stackTrace.toString());
        intent.putExtra("STACKTRACE", s);
        myContext.startActivity(intent);
        //for restarting the Activity
        ////////////Process.killProcess(Process.myPid());
        System.exit(10);///////////////////NEED VERIFY
    }
}
