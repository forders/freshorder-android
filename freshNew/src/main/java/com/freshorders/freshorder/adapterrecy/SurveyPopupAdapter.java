package com.freshorders.freshorder.adapterrecy;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.freshorders.freshorder.model.SurveyCustomField;
import com.freshorders.freshorder.model.SurveyDetailModel;

import java.util.List;

public class SurveyPopupAdapter extends RecyclerView.Adapter<SurveyPopupAdapter.MyViewHolder>{

    private Context mContext;
    SurveyDetailModel currentSurvey;
    private List<SurveyCustomField> surveyDetail;

    public SurveyPopupAdapter(Context mContext, SurveyDetailModel currentSurvey, List<SurveyCustomField> surveyDetail) {
        this.mContext = mContext;
        this.currentSurvey = currentSurvey;
        this.surveyDetail = surveyDetail;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return surveyDetail.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView mTextView;

        public MyViewHolder(View itemView) {
            super(itemView);

            //mTextView = (TextView) itemView.findViewById(R.id.tv_text2);
        }
    }
}
