package com.freshorders.freshorder.adapterrecy;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.model.SurveyDetailModel;
import com.freshorders.freshorder.utils.Log;

import java.util.List;

public class SurveyPendingAdapter  extends
        RecyclerView.Adapter<SurveyPendingAdapter.ItemViewHolder> {


    public static final String TAG = SurveyPendingAdapter.class.getSimpleName();
    private Context mContext;
    private List<SurveyDetailModel> surveyList;
    private View pendingFragment;

    public SurveyPendingAdapter(Context mContext, List<SurveyDetailModel> surveyList, View pendingFragment) {
        this.mContext = mContext;
        this.surveyList = surveyList;
        this.pendingFragment = pendingFragment;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View myInflatedView = inflater.inflate(R.layout.adapter_pending_survey, parent,false);  ////////////parent --- Null(can be)
        return new ItemViewHolder(myInflatedView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ItemViewHolder holder, int position) {

        SurveyDetailModel currentSurvey = surveyList.get(position);
        holder.cvSurveyPending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG,"...............Clicked");
                SurveyDetailModel currentSurvey = surveyList.get(holder.getAdapterPosition());
                showPopupWithPendingData(currentSurvey);
            }
        });
        holder.tvCompanyName.setText(currentSurvey.getCompanyName());

    }

    @Override
    public int getItemCount() {
        return surveyList.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        TextView tvCompanyName;
        CardView cvSurveyPending;

        ItemViewHolder(View itemView) {
            super(itemView);
            tvCompanyName = itemView.findViewById(R.id.id_survey_pending_companyName);
            cvSurveyPending = itemView.findViewById(R.id.id_CV_survey_pending);
        }

    }

    private void showPopupWithPendingData(SurveyDetailModel currentSurvey){

        DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();

    }

}
