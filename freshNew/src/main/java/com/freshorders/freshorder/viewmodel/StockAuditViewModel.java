package com.freshorders.freshorder.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.content.Context;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.freshorders.freshorder.asyntask.ClosingStockAuditAsyncTask;
import com.freshorders.freshorder.interfaces.StockAuditFetchAPI;
import com.freshorders.freshorder.model.StockAuditDisplayModel;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Log;
import com.freshorders.freshorder.utils.Utils;

import org.json.JSONStringer;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class StockAuditViewModel  extends ViewModel {

    private void show(){
        if( swipeContainer != null)
        swipeContainer.setRefreshing(true);
    }

    private void hide(){
        if( swipeContainer != null)
        swipeContainer.setRefreshing(false);
    }

    private SwipeRefreshLayout swipeContainer;

    //this is the data that we will fetch asynchronously
    private MutableLiveData<List<StockAuditDisplayModel>> stockAuditList;

    //we will call this method to get the data
    public LiveData<List<StockAuditDisplayModel>> getStockList(JSONStringer jsonStringer) {
        //if the list is null
        if (stockAuditList == null) {
            stockAuditList = new MutableLiveData<List<StockAuditDisplayModel>>();
            //we will load it asynchronously from server in this method
            //loadStockAuditItems(jsonStringer);
        }

        //finally we will return the list
        return stockAuditList;
    }

    public LiveData<List<StockAuditDisplayModel>> getStockList(String jsonStringer,
                                                               Context mContext,
                                                               SwipeRefreshLayout swipeContainers) {
        swipeContainer = swipeContainers;
        //if the list is null
        if (stockAuditList == null) {
            stockAuditList = new MutableLiveData<List<StockAuditDisplayModel>>();
            //we will load it asynchronously from server in this method
            loadStockItems(jsonStringer, mContext);
        }
        //finally we will return the list
        return stockAuditList;
    }

    private void loadStockItems(final String jsonStringer, final Context mContext){

        JsonServiceHandler jsonServiceHandler = new JsonServiceHandler(Utils.strClosingStockAudit, jsonStringer, mContext);
        ClosingStockAuditAsyncTask obj = new ClosingStockAuditAsyncTask(jsonServiceHandler, new ClosingStockAuditAsyncTask.GetAuditCompleteListener() {
            @Override
            public void onSuccess(List<StockAuditDisplayModel> object) {
                new Log(mContext).ee("ViewModel","......SUCCESS");
                stockAuditList.setValue(object);
            }

            @Override
            public void onFailed(String msg) {
                new Log(mContext).ee("ViewModel","......FAILED : " + Utils.strClosingStockAudit + jsonStringer);
                stockAuditList.setValue(null);
            }

            @Override
            public void onException(String msg) {
                new Log(mContext).ee("ViewModel","......EXCEPTION : " + Utils.strClosingStockAudit + jsonStringer);
                stockAuditList.setValue(null);
            }
        });
        obj.execute();
    }




    //This method is using Retrofit to get the JSON data from URL
    private void loadStockAuditItems(String jsonStringer) {


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(StockAuditFetchAPI.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        StockAuditFetchAPI api = retrofit.create(StockAuditFetchAPI.class);
        Call<List<StockAuditDisplayModel>> call = api.getStockAuditList(jsonStringer);

       // Call<List<StockAuditDisplayModel>> call = api.getStockAuditList(null, "28142");


        call.enqueue(new Callback<List<StockAuditDisplayModel>>() {
            @Override
            public void onResponse(Call<List<StockAuditDisplayModel>> call, Response<List<StockAuditDisplayModel>> response) {

                Log.e("url",".SUCCESS...................."+ call.request().url());
                Log.e("url",".SUCCESS...................."+ response.toString());
                Log.e("url",".SUCCESS...................."+ response.isSuccessful());
                //finally we are setting the list to our MutableLiveData
                stockAuditList.setValue(response.body());
            }

            @Override
            public void onFailure(Call<List<StockAuditDisplayModel>> call, Throwable t) {
                Log.e("ViewModel","....................."+ t.getMessage());
                Log.e("headers","....................."+ call.request().headers());
                Log.e("request","....................."+ call.request().toString());
                Log.e("url","....................."+ call.request().url());

            }
        });
    }
}
