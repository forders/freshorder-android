package com.freshorders.freshorder.model;

public class PetrolBunkModel {

    private String qty;
    private String productCode;

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
}
