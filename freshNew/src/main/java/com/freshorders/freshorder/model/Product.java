package com.freshorders.freshorder.model;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;

@SuppressLint("ParcelCreator")
public class Product implements Parcelable {

    private String prodid;
    private String userid;
    private String prodcode;
    private String hsncode;
    private String prodname;
    private String prodimage;
    private String prodprice;
    private String prodtax;
    private String mrp;
    private String uom;
    private String unitprice;
    private String stock;
    private String unitsgm;
    private String unitperuom;
    private String importfile;
    private String importrmrk;
    private String prodstatus;
    private String display_order;
    private String margin_percent;
    private String updateddt;


    public Product(Parcel in) {
        prodid = in.readString();
        userid = in.readString();
        prodcode = in.readString();
        hsncode = in.readString();
        prodname = in.readString();
        prodimage = in.readString();
        prodprice = in.readString();
        prodtax = in.readString();
        mrp = in.readString();
        uom = in.readString();
        unitprice = in.readString();
        stock = in.readString();
        unitsgm = in.readString();
        unitperuom = in.readString();
        importfile = in.readString();
        importrmrk = in.readString();
        prodstatus = in.readString();
        display_order = in.readString();
        margin_percent = in.readString();
        updateddt = in.readString();
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    public Product() {

    }

    public String getProdid() {
        return prodid;
    }

    public void setProdid(String prodid) {
        this.prodid = prodid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getProdcode() {
        return prodcode;
    }

    public void setProdcode(String prodcode) {
        this.prodcode = prodcode;
    }

    public String getHsncode() {
        return hsncode;
    }

    public void setHsncode(String hsncode) {
        this.hsncode = hsncode;
    }

    public String getProdname() {
        return prodname;
    }

    public void setProdname(String prodname) {
        this.prodname = prodname;
    }

    public String getProdimage() {
        return prodimage;
    }

    public void setProdimage(String prodimage) {
        this.prodimage = prodimage;
    }

    public String getProdprice() {
        return prodprice;
    }

    public void setProdprice(String prodprice) {
        this.prodprice = prodprice;
    }

    public String getProdtax() {
        return prodtax;
    }

    public void setProdtax(String prodtax) {
        this.prodtax = prodtax;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getUnitprice() {
        return unitprice;
    }

    public void setUnitprice(String unitprice) {
        this.unitprice = unitprice;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getUnitsgm() {
        return unitsgm;
    }

    public void setUnitsgm(String unitsgm) {
        this.unitsgm = unitsgm;
    }

    public String getUnitperuom() {
        return unitperuom;
    }

    public void setUnitperuom(String unitperuom) {
        this.unitperuom = unitperuom;
    }

    public String getImportfile() {
        return importfile;
    }

    public void setImportfile(String importfile) {
        this.importfile = importfile;
    }

    public String getImportrmrk() {
        return importrmrk;
    }

    public void setImportrmrk(String importrmrk) {
        this.importrmrk = importrmrk;
    }

    public String getProdstatus() {
        return prodstatus;
    }

    public void setProdstatus(String prodstatus) {
        this.prodstatus = prodstatus;
    }

    public String getDisplay_order() {
        return display_order;
    }

    public void setDisplay_order(String display_order) {
        this.display_order = display_order;
    }

    public String getMargin_percent() {
        return margin_percent;
    }

    public void setMargin_percent(String margin_percent) {
        this.margin_percent = margin_percent;
    }

    public String getUpdateddt() {
        return updateddt;
    }

    public void setUpdateddt(String updateddt) {
        this.updateddt = updateddt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(prodid);
        parcel.writeString(userid);
        parcel.writeString(prodcode);
        parcel.writeString(hsncode);
        parcel.writeString(prodname);
        parcel.writeString(prodimage);
        parcel.writeString(prodprice);
        parcel.writeString(prodtax);
        parcel.writeString(mrp);
        parcel.writeString(uom);
        parcel.writeString(unitprice);
        parcel.writeString(stock);
        parcel.writeString(unitsgm);
        parcel.writeString(unitperuom);
        parcel.writeString(importfile);
        parcel.writeString(importrmrk);
        parcel.writeString(prodstatus);
        parcel.writeString(display_order);
        parcel.writeString(margin_percent);
        parcel.writeString(updateddt);
    }
}
