package com.freshorders.freshorder.model;

public class PostNoteModel {

    private int id;
    private String muserid;
    private String particular;
    private String duserid;
    private String failedReason;
    private boolean isSuccess;
    private boolean isImageContain;
    private byte[] image;
    private String responseCode;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMuserid() {
        return muserid;
    }

    public void setMuserid(String muserid) {
        this.muserid = muserid;
    }

    public String getParticular() {
        return particular;
    }

    public void setParticular(String particular) {
        this.particular = particular;
    }

    public String getDuserid() {
        return duserid;
    }

    public void setDuserid(String duserid) {
        this.duserid = duserid;
    }

    public String getFailedReason() {
        return failedReason;
    }

    public void setFailedReason(String failedReason) {
        this.failedReason = failedReason;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }

    public boolean isImageContain() {
        return isImageContain;
    }

    public void setImageContain(boolean imageContain) {
        isImageContain = imageContain;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }
}
