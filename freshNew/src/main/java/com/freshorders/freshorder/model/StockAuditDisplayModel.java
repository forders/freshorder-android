package com.freshorders.freshorder.model;

public class StockAuditDisplayModel {

    private int stkauditid;
    private int duserid;
    private int suserid;
    private int prodid;
    private int stockqty;
    private int auditqty;

    private String auditdt;
    private String reference;
    private String remarks;
    private String createddt;
    private String createdby;
    private String status;
    private String updateddt;
    private String Test;

    private StockAuditProduct product;

    public StockAuditProduct getProduct() {
        return product;
    }

    public void setProduct(StockAuditProduct product) {
        this.product = product;
    }

    public int getStkauditid() {
        return stkauditid;
    }

    public void setStkauditid(int stkauditid) {
        this.stkauditid = stkauditid;
    }

    public int getDuserid() {
        return duserid;
    }

    public void setDuserid(int duserid) {
        this.duserid = duserid;
    }

    public int getSuserid() {
        return suserid;
    }

    public void setSuserid(int suserid) {
        this.suserid = suserid;
    }

    public int getProdid() {
        return prodid;
    }

    public void setProdid(int prodid) {
        this.prodid = prodid;
    }

    public int getStockqty() {
        return stockqty;
    }

    public void setStockqty(int stockqty) {
        this.stockqty = stockqty;
    }

    public int getAuditqty() {
        return auditqty;
    }

    public void setAuditqty(int auditqty) {
        this.auditqty = auditqty;
    }

    public String getAuditdt() {
        return auditdt;
    }

    public void setAuditdt(String auditdt) {
        this.auditdt = auditdt;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCreateddt() {
        return createddt;
    }

    public void setCreateddt(String createddt) {
        this.createddt = createddt;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUpdateddt() {
        return updateddt;
    }

    public void setUpdateddt(String updateddt) {
        this.updateddt = updateddt;
    }

    public String getTest() {
        return Test;
    }

    public void setTest(String test) {
        Test = test;
    }
}
