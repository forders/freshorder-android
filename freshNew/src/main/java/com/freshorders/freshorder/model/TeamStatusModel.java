package com.freshorders.freshorder.model;

import java.io.Serializable;
import java.util.List;

public class TeamStatusModel implements Serializable {

    private static final long serialVersionUID = 1L;

    private String workerName;
    private String percentageOfWorkCompleted;

    private List<MyTeamStatusDetailModel> myTeamStatusDetailModels;


    public String getWorkerName() {
        return workerName;
    }

    public void setWorkerName(String workerName) {
        this.workerName = workerName;
    }

    public String getPercentageOfWorkCompleted() {
        return percentageOfWorkCompleted;
    }

    public void setPercentageOfWorkCompleted(String percentageOfWorkCompleted) {
        this.percentageOfWorkCompleted = percentageOfWorkCompleted;
    }

    public List<MyTeamStatusDetailModel> getMyTeamStatusDetailModels() {
        return myTeamStatusDetailModels;
    }

    public void setMyTeamStatusDetailModels(List<MyTeamStatusDetailModel> myTeamStatusDetailModels) {
        this.myTeamStatusDetailModels = myTeamStatusDetailModels;
    }
}
