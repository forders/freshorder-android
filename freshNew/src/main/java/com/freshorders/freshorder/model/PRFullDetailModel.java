package com.freshorders.freshorder.model;

import java.util.List;

public class PRFullDetailModel {

    private String status;
    private List<PRModel> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<PRModel> getData() {
        return data;
    }

    public void setData(List<PRModel> data) {
        this.data = data;
    }
}
