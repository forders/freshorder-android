package com.freshorders.freshorder.model;

import java.util.List;

public class DSRFullDetailModel {

    private String status;
    private List<DSRDetailModel> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<DSRDetailModel> getData() {
        return data;
    }

    public void setData(List<DSRDetailModel> data) {
        this.data = data;
    }
}
