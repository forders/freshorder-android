package com.freshorders.freshorder.model;

public class DSRResponseModel {

    private String ordid;
    private String muserid;
    private String suserid;
    private String oflnordid;
    private String aprxordval;

    public String getOrdid() {
        return ordid;
    }

    public void setOrdid(String ordid) {
        this.ordid = ordid;
    }

    public String getMuserid() {
        return muserid;
    }

    public void setMuserid(String muserid) {
        this.muserid = muserid;
    }

    public String getSuserid() {
        return suserid;
    }

    public void setSuserid(String suserid) {
        this.suserid = suserid;
    }

    public String getOflnordid() {
        return oflnordid;
    }

    public void setOflnordid(String oflnordid) {
        this.oflnordid = oflnordid;
    }

    public String getAprxordval() {
        return aprxordval;
    }

    public void setAprxordval(String aprxordval) {
        this.aprxordval = aprxordval;
    }
}
