package com.freshorders.freshorder.model;

public class GPSSubResponse {

    private String id;
    private String success;
    private String isduplicate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getIsduplicate() {
        return isduplicate;
    }

    public void setIsduplicate(String isduplicate) {
        this.isduplicate = isduplicate;
    }
}
