package com.freshorders.freshorder.model;

import java.util.List;

public class DSRFullResponseModel {

    private String status;
    private List<DSRResponseModel> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<DSRResponseModel> getData() {
        return data;
    }

    public void setData(List<DSRResponseModel> data) {
        this.data = data;
    }
}
