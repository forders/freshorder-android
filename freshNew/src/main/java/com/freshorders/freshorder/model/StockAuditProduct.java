package com.freshorders.freshorder.model;

public class StockAuditProduct {

    private String prodname;

    public String getProdname() {
        return prodname;
    }

    public void setProdname(String prodname) {
        this.prodname = prodname;
    }
}
