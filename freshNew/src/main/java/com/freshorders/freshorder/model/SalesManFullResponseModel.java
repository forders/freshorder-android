package com.freshorders.freshorder.model;

import java.util.List;

public class SalesManFullResponseModel {

    private String status;
    private List<SalesManResponseModel> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<SalesManResponseModel> getData() {
        return data;
    }

    public void setData(List<SalesManResponseModel> data) {
        this.data = data;
    }
}
