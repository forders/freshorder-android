package com.freshorders.freshorder.model;

public class MigrationError {

    private String errorMSG;
    private int isNeedMigration;

    public String getErrorMSG() {
        return errorMSG;
    }

    public void setErrorMSG(String errorMSG) {
        this.errorMSG = errorMSG;
    }

    public int getIsNeedMigration() {
        return isNeedMigration;
    }

    public void setIsNeedMigration(int isNeedMigration) {
        this.isNeedMigration = isNeedMigration;
    }
}
