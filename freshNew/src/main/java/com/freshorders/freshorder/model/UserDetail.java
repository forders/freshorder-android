package com.freshorders.freshorder.model;

public class UserDetail {

    private String userId;
    private String partyCd;
    private String mobileNo;
    private String userType;
    private String userTier;
    private String password;
    private String fullName;
    private String dUserId;
    private String profileImg;
    private String companyName;
    private String emailId;
    private String custCode;
    private String address;
    private String city;
    private String pinCode;
    private String taxNo;
    private String pymtTenure;
    private String otp;
    private String otpStatus;
    private String pymtStatus;
    private String gcmid;
    private String geoLat;
    private String geoLong;
    private String creditLimit;
    private String cFlag;
    private String createdBy;
    private String updatedDt;
    private String lastPymtDt;
    private String nextPymtDt;
    private String lastLogin;
    private String state;
    private String tin;
    private String panNo;
    private String contactNo;
    private String stockiest;
    private String route;
    private String userStatus;

    public String getUserId() {
        return userId;
    }

    public String getPartyCd() {
        return partyCd;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public String getUserType() {
        return userType;
    }

    public String getUserTier() {
        return userTier;
    }

    public String getPassword() {
        return password;
    }

    public String getFullName() {
        return fullName;
    }

    public String getdUserId() {
        return dUserId;
    }

    public String getProfileImg() {
        return profileImg;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getEmailId() {
        return emailId;
    }

    public String getCustCode() {
        return custCode;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getPinCode() {
        return pinCode;
    }

    public String getTaxNo() {
        return taxNo;
    }

    public String getPymtTenure() {
        return pymtTenure;
    }

    public String getOtp() {
        return otp;
    }

    public String getOtpStatus() {
        return otpStatus;
    }

    public String getPymtStatus() {
        return pymtStatus;
    }

    public String getGcmid() {
        return gcmid;
    }

    public String getGeoLat() {
        return geoLat;
    }

    public String getGeoLong() {
        return geoLong;
    }

    public String getCreditLimit() {
        return creditLimit;
    }

    public String getcFlag() {
        return cFlag;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public String getUpdatedDt() {
        return updatedDt;
    }

    public String getLastPymtDt() {
        return lastPymtDt;
    }

    public String getNextPymtDt() {
        return nextPymtDt;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public String getState() {
        return state;
    }

    public String getTin() {
        return tin;
    }

    public String getPanNo() {
        return panNo;
    }

    public String getContactNo() {
        return contactNo;
    }

    public String getStockiest() {
        return stockiest;
    }

    public String getRoute() {
        return route;
    }

    public String getUserStatus() {
        return userStatus;
    }
}
