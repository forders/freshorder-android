package com.freshorders.freshorder.model;

import com.freshorders.freshorder.utils.Constants;

public class MigrationResultModel {

    private String migName = Constants.EMPTY;
    private int migTotal = 0;
    private int migSuccessCount = 0;
    private int failedCount = 0;
    private String status = Constants.EMPTY;
    private String failedReason = Constants.EMPTY;
}
