package com.freshorders.freshorder.model;

public class Template7TestNameModel {

    private String testName;

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

}
