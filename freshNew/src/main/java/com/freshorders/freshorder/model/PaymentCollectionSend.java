package com.freshorders.freshorder.model;

import org.json.JSONArray;

public class PaymentCollectionSend {

    private String suserid;
    private String updateddt;
    private String notes;
    private String pymtmode;
    private String payamt;
    private JSONArray bill_list;

    public String getSuserid() {
        return suserid;
    }

    public void setSuserid(String suserid) {
        this.suserid = suserid;
    }

    public String getUpdateddt() {
        return updateddt;
    }

    public void setUpdateddt(String updateddt) {
        this.updateddt = updateddt;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getPymtmode() {
        return pymtmode;
    }

    public void setPymtmode(String pymtmode) {
        this.pymtmode = pymtmode;
    }

    public String getPayamt() {
        return payamt;
    }

    public void setPayamt(String payamt) {
        this.payamt = payamt;
    }

    public JSONArray getBill_list() {
        return bill_list;
    }

    public void setBill_list(JSONArray bill_list) {
        this.bill_list = bill_list;
    }
}
