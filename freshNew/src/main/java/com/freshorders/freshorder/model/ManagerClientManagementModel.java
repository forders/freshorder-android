package com.freshorders.freshorder.model;

public class ManagerClientManagementModel {

    private String duserid;
    private String fromplndt;
    private String mode = "MOB";
    private String saleslist;
    private String toplndt;
    private String usertype = "D";

    public String getDuserid() {
        return duserid;
    }

    public void setDuserid(String duserid) {
        this.duserid = duserid;
    }

    public String getFromplndt() {
        return fromplndt;
    }

    public void setFromplndt(String fromplndt) {
        this.fromplndt = fromplndt;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getSaleslist() {
        return saleslist;
    }

    public void setSaleslist(String saleslist) {
        this.saleslist = saleslist;
    }

    public String getToplndt() {
        return toplndt;
    }

    public void setToplndt(String toplndt) {
        this.toplndt = toplndt;
    }

    public String getUsertype() {
        return usertype;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }
}
