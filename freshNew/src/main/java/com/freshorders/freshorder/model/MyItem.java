package com.freshorders.freshorder.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class MyItem implements ClusterItem {
    private final LatLng mPosition;
    private final String mTitle;
    private final String mSnippet;

    private boolean isColorRed;


    private String sUserId;

    public double getDistance() {
        return distance;
    }

    private double distance;

    private int color;

    public MyItem(double lat, double lng, double distance) {
        mPosition = new LatLng(lat, lng);
        this.distance = distance;
        mTitle = "";
        mSnippet = "";
    }

    public MyItem(double lat, double lng, String t, String s) {
        mPosition = new LatLng(lat, lng);
        mTitle = t;
        mSnippet = s;
    }

    public MyItem(double lat, double lng, String t, String s, boolean isColorRed) {
        mPosition = new LatLng(lat, lng);
        mTitle = t;
        mSnippet = s;
        this.isColorRed = isColorRed;
    }

    public MyItem(double lat, double lng, String t, String s, int col, String sUserId) {
        mPosition = new LatLng(lat, lng);
        mTitle = t;
        mSnippet = s;
        color = col;
        this.sUserId = sUserId;
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    public String getTitle(){
        return mTitle;
    }

    public String getSnippet(){
        return mSnippet;
    }

    public int getColor() {
        return color;
    }


    public String getsUserId() {
        return sUserId;
    }

    public boolean isColorRed() {
        return isColorRed;
    }

    public void setColorRed(boolean colorRed) {
        isColorRed = colorRed;
    }
}
