package com.freshorders.freshorder.model;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

@SuppressLint("ParcelCreator")
public class GRN implements Parcelable{
    private String ordid;
    private String muserid;
    private String suserid;
    private String distributorid;
    private String oflnordid;
    private String orderdt;
    private String iscash;
    private String discntval;
    private String discntprcnt;
    private String aprxordval;
    private String ordertotal;
    private String pymttotal;
    private String pymtdate;
    private String start_time;
    private String end_time;
    private String geolat;
    private String geolong;
    private String ordstatus;

    public String getDistributorName() {
        return distributorName;
    }

    public void setDistributorName(String distributorName) {
        this.distributorName = distributorName;
    }

    private String distributorName;
    private ArrayList<OrderDetail> orderDetailList = new ArrayList<>();

    public GRN(Parcel in) {
        ordid = in.readString();
        muserid = in.readString();
        suserid = in.readString();
        distributorid = in.readString();
        oflnordid = in.readString();
        orderdt = in.readString();
        iscash = in.readString();
        discntval = in.readString();
        discntprcnt = in.readString();
        aprxordval = in.readString();
        ordertotal = in.readString();
        pymttotal = in.readString();
        pymtdate = in.readString();
        start_time = in.readString();
        end_time = in.readString();
        geolat = in.readString();
        geolong = in.readString();
        ordstatus = in.readString();
        distributorName = in.readString();
        in.readTypedList(orderDetailList, OrderDetail.CREATOR);
    }

    public static final Creator<GRN> CREATOR = new Creator<GRN>() {
        @Override
        public GRN createFromParcel(Parcel in) {
            return new GRN(in);
        }

        @Override
        public GRN[] newArray(int size) {
            return new GRN[size];
        }
    };

    public GRN() {

    }

    public String getOrdid() {
        return ordid;
    }

    public void setOrdid(String ordid) {
        this.ordid = ordid;
    }

    public String getMuserid() {
        return muserid;
    }

    public void setMuserid(String muserid) {
        this.muserid = muserid;
    }

    public String getSuserid() {
        return suserid;
    }

    public void setSuserid(String suserid) {
        this.suserid = suserid;
    }

    public String getDistributorid() {
        return distributorid;
    }

    public void setDistributorid(String distributorid) {
        this.distributorid = distributorid;
    }

    public String getOflnordid() {
        return oflnordid;
    }

    public void setOflnordid(String oflnordid) {
        this.oflnordid = oflnordid;
    }

    public String getOrderdt() {
        return orderdt;
    }

    public void setOrderdt(String orderdt) {
        this.orderdt = orderdt;
    }

    public String getIscash() {
        return iscash;
    }

    public void setIscash(String iscash) {
        this.iscash = iscash;
    }

    public String getDiscntval() {
        return discntval;
    }

    public void setDiscntval(String discntval) {
        this.discntval = discntval;
    }

    public String getDiscntprcnt() {
        return discntprcnt;
    }

    public void setDiscntprcnt(String discntprcnt) {
        this.discntprcnt = discntprcnt;
    }

    public String getAprxordval() {
        return aprxordval;
    }

    public void setAprxordval(String aprxordval) {
        this.aprxordval = aprxordval;
    }

    public String getOrdertotal() {
        return ordertotal;
    }

    public void setOrdertotal(String ordertotal) {
        this.ordertotal = ordertotal;
    }

    public String getPymttotal() {
        return pymttotal;
    }

    public void setPymttotal(String pymttotal) {
        this.pymttotal = pymttotal;
    }

    public String getPymtdate() {
        return pymtdate;
    }

    public void setPymtdate(String pymtdate) {
        this.pymtdate = pymtdate;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getGeolat() {
        return geolat;
    }

    public void setGeolat(String geolat) {
        this.geolat = geolat;
    }

    public String getGeolong() {
        return geolong;
    }

    public void setGeolong(String geolong) {
        this.geolong = geolong;
    }

    public String getOrdstatus() {
        return ordstatus;
    }

    public void setOrdstatus(String ordstatus) {
        this.ordstatus = ordstatus;
    }

    public ArrayList<OrderDetail> getOrderDetailList() {
        return orderDetailList;
    }

    public void setOrderDetailList(OrderDetail orderDetail) {
        this.orderDetailList.add(orderDetail);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(ordid);
        parcel.writeString(muserid);
        parcel.writeString(suserid);
        parcel.writeString(distributorid);
        parcel.writeString(oflnordid);
        parcel.writeString(orderdt);
        parcel.writeString(iscash);
        parcel.writeString(discntval);
        parcel.writeString(discntprcnt);
        parcel.writeString(aprxordval);
        parcel.writeString(ordertotal);
        parcel.writeString(pymttotal);
        parcel.writeString(pymtdate);
        parcel.writeString(start_time);
        parcel.writeString(end_time);
        parcel.writeString(geolat);
        parcel.writeString(geolong);
        parcel.writeString(ordstatus);
        parcel.writeString(distributorName);
        parcel.writeTypedList(orderDetailList);
    }
}
