package com.freshorders.freshorder.model;

public class DSRDetailModel {

    private String prodname;
    private String qtotal;
    private String value;

    public String getProdname() {
        return prodname;
    }

    public void setProdname(String prodname) {
        this.prodname = prodname;
    }

    public String getQtotal() {
        return qtotal;
    }

    public void setQtotal(String qtotal) {
        this.qtotal = qtotal;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
