package com.freshorders.freshorder.model;

public class ManagerClientVisitAdapterModel {

    private String name;
    private String remarks;
    private String geolong;
    private String geolat;
    private String updateddt;
    private String time;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUpdateddt() {
        return updateddt;
    }

    public void setUpdateddt(String updateddt) {
        this.updateddt = updateddt;
    }

    public String getGeolong() {
        return geolong;
    }

    public void setGeolong(String geolong) {
        this.geolong = geolong;
    }

    public String getGeolat() {
        return geolat;
    }

    public void setGeolat(String geolat) {
        this.geolat = geolat;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
