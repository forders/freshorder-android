package com.freshorders.freshorder.model;

public class PaymentCollectionModel {

    private String updateddt;
    private String notes;
    private String pymtmode;
    private String payamt;

    public String getPayamt() {
        return payamt;
    }

    public void setPayamt(String payamt) {
        this.payamt = payamt;
    }

    public String getUpdateddt() {
        return updateddt;
    }

    public void setUpdateddt(String updateddt) {
        this.updateddt = updateddt;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getPymtmode() {
        return pymtmode;
    }

    public void setPymtmode(String pymtmode) {
        this.pymtmode = pymtmode;
    }
}
