package com.freshorders.freshorder.model;

import java.util.List;

public class StockAuditWholeModel {

    private String updateddt;
    private String updatedby;
    private List<StockAuditSendStock> auditarray;

    public String getUpdateddt() {
        return updateddt;
    }

    public void setUpdateddt(String updateddt) {
        this.updateddt = updateddt;
    }

    public String getUpdatedby() {
        return updatedby;
    }

    public void setUpdatedby(String updatedby) {
        this.updatedby = updatedby;
    }

    public List<StockAuditSendStock> getAuditarray() {
        return auditarray;
    }

    public void setAuditarray(List<StockAuditSendStock> auditarray) {
        this.auditarray = auditarray;
    }
}
