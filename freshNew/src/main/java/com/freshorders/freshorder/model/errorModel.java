package com.freshorders.freshorder.model;

public class errorModel {


    private int srno;
    private String message;
    private String date;
    private String time;

    public int getSrno() {
        return srno;
    }

    public void setSrno(int srno) {
        this.srno = srno;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public errorModel(String message, String date, String time) {
        this.message = message;
        this.date = date;
        this.time = time;
    }


    @Override
    public String toString() {
        return "errorModel{" +
                "srno=" + srno +
                ", message='" + message + '\'' +
                ", date='" + date + '\'' +
                ", time='" + time + '\'' +
                '}';
    }
}