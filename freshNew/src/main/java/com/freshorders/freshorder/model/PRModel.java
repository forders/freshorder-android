package com.freshorders.freshorder.model;

public class PRModel {

    private String prodname;
    private String qtotal;
    private String value;
    private String uom;

    private String totalSales;

    public PRModel(String prodname, String qtotal, String value) {
        this.prodname = prodname;
        this.qtotal = qtotal;
        this.value = value;
    }

    public PRModel() {
    }

    public String getProdname() {
        return prodname;
    }

    public void setProdname(String prodname) {
        this.prodname = prodname;
    }

    public String getQtotal() {
        return qtotal;
    }

    public void setQtotal(String qtotal) {
        this.qtotal = qtotal;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(String totalSales) {
        this.totalSales = totalSales;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }
}
