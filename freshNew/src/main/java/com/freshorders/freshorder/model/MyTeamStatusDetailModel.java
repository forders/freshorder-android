package com.freshorders.freshorder.model;

import java.io.Serializable;

public class MyTeamStatusDetailModel implements Serializable {

    private String workName;
    private String workCompletion;
    private int workCompletionPercentage;

    public String getWorkName() {
        return workName;
    }

    public void setWorkName(String workName) {
        this.workName = workName;
    }

    public String getWorkCompletion() {
        return workCompletion;
    }

    public void setWorkCompletion(String workCompletion) {
        this.workCompletion = workCompletion;
    }

    public int getWorkCompletionPercentage() {
        return workCompletionPercentage;
    }

    public void setWorkCompletionPercentage(int workCompletionPercentage) {
        this.workCompletionPercentage = workCompletionPercentage;
    }
}
