package com.freshorders.freshorder.model;


import android.location.Location;


public class locationModel {


    private int srno;
    private Location location;
    private String date;
    private String time;
    private float accuracy;

    public int getSrno() {
        return srno;
    }

    public float getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(float accuracy) {
        this.accuracy = accuracy;
    }

    public void setSrno(int srno) {
        this.srno = srno;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }


    public locationModel(Location location, String date, String time, float accuracy) {
        this.location = location;
        this.date = date;
        this.time = time;
        this.accuracy = accuracy;
    }

    @Override
    public String toString() {
        return "locationModel{" +
                "srno=" + srno +
                ", location=" + location +
                ", date='" + date + '\'' +
                ", time='" + time + '\'' +
                ", accuracy=" + accuracy +
                '}';
    }
}