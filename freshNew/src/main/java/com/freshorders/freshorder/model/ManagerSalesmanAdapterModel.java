package com.freshorders.freshorder.model;

public class ManagerSalesmanAdapterModel {

    private String ordid;
    private String orderdt;
    private String storename;
    private String start_time;
    private String end_time;
    private String geolat;
    private String geolong;

    public String getGeolat() {
        return geolat;
    }

    public void setGeolat(String geolat) {
        this.geolat = geolat;
    }

    public String getGeolong() {
        return geolong;
    }

    public void setGeolong(String geolong) {
        this.geolong = geolong;
    }

    public String getOrdid() {
        return ordid;
    }

    public void setOrdid(String ordid) {
        this.ordid = ordid;
    }

    public String getOrderdt() {
        return orderdt;
    }

    public void setOrderdt(String orderdt) {
        this.orderdt = orderdt;
    }

    public String getStorename() {
        return storename;
    }

    public void setStorename(String storename) {
        this.storename = storename;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }
}
