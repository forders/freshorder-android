package com.freshorders.freshorder.model;

public class HierarchyObjModel {

    private int levelid;
    private int parentlevelid;
    private String name;
    private boolean isExpanded = false;
    private boolean isActive = true;
    private boolean selected = true;

    public int getLevelid() {
        return levelid;
    }

    public void setLevelid(int levelid) {
        this.levelid = levelid;
    }

    public int getParentlevelid() {
        return parentlevelid;
    }

    public void setParentlevelid(int parentlevelid) {
        this.parentlevelid = parentlevelid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
