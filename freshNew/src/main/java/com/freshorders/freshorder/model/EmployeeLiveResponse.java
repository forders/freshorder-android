package com.freshorders.freshorder.model;

public class EmployeeLiveResponse {

    private String userid;
    private String mobileno;
    private String fullname;
    private double geolat;
    private double geolong;
    private String logtime;

    public EmployeeLiveResponse(String userid, String mobileno, String fullname, double geolat, double geolong,String logtime) {
        this.userid = userid;
        this.mobileno = mobileno;
        this.fullname = fullname;
        this.geolat = geolat;
        this.geolong = geolong;
        this.logtime = logtime;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public double getGeolat() {
        return geolat;
    }

    public void setGeolat(double geolat) {
        this.geolat = geolat;
    }

    public double getGeolong() {
        return geolong;
    }

    public void setGeolong(double geolong) {
        this.geolong = geolong;
    }

    public String getLogtime() {
        return logtime;
    }

    public void setLogtime(String logtime) {
        this.logtime = logtime;
    }




}
