package com.freshorders.freshorder.model;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;

public class SurveyDetailModel {

    private int id;
    private int merchantRowId;
    private String mUserId;
    private String companyName;
    private String surveyDate;
    private String duserid;
    private int isMigrated;
    private String failedReason;
    private List<SurveyCustomField> surveyDetail;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMerchantRowId() {
        return merchantRowId;
    }

    public void setMerchantRowId(int merchantRowId) {
        this.merchantRowId = merchantRowId;
    }

    public String getmUserId() {
        return mUserId;
    }

    public void setmUserId(String mUserId) {
        this.mUserId = mUserId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getSurveyDate() {
        return surveyDate;
    }

    public void setSurveyDate(String surveyDate) {
        this.surveyDate = surveyDate;
    }

    public String getDuserid() {
        return duserid;
    }

    public void setDuserid(String duserid) {
        this.duserid = duserid;
    }

    public int getIsMigrated() {
        return isMigrated;
    }

    public void setIsMigrated(int isMigrated) {
        this.isMigrated = isMigrated;
    }

    public String getFailedReason() {
        return failedReason;
    }

    public void setFailedReason(String failedReason) {
        this.failedReason = failedReason;
    }


    public List<SurveyCustomField> getSurveyDetail() {
        return surveyDetail;
    }

    public void setSurveyDetail(List<SurveyCustomField> surveyDetail) {
        this.surveyDetail = surveyDetail;
    }
}
