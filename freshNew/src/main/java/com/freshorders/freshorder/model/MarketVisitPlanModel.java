package com.freshorders.freshorder.model;

public class MarketVisitPlanModel {

    private String name;
    private String workType;
    private String remarks;

    private String geolat;
    private String geolong;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWorkType() {
        return workType;
    }

    public void setWorkType(String workType) {
        this.workType = workType;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getGeolat() {
        return geolat;
    }

    public void setGeolat(String geolat) {
        this.geolat = geolat;
    }

    public String getGeolong() {
        return geolong;
    }

    public void setGeolong(String geolong) {
        this.geolong = geolong;
    }

}
