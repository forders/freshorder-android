package com.freshorders.freshorder.model;


/*
Created By Kumaravel 14-12-2018
 */
public class Template2Model {

    private String closingStock = "";
    private String orderQuantity = "";
    private String productId = "";

    public String getClosingStock() {
        return closingStock;
    }

    public void setClosingStock(String closingStock) {
        this.closingStock = closingStock;
    }

    public String getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(String orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
