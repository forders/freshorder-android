package com.freshorders.freshorder.model;

public class DSRDetailInputModel {

    private String suserid;
    private String reporttype;
    private String usertype;
    private String fromdt;

    public DSRDetailInputModel(String suserid, String reporttype, String usertype, String fromdt, String todt) {
        this.suserid = suserid;
        this.reporttype = reporttype;
        this.usertype = usertype;
        this.fromdt = fromdt;
        this.todt = todt;
    }

    public String getSuserid() {
        return suserid;
    }

    public void setSuserid(String suserid) {
        this.suserid = suserid;
    }

    public String getReporttype() {
        return reporttype;
    }

    public void setReporttype(String reporttype) {
        this.reporttype = reporttype;
    }

    public String getUsertype() {
        return usertype;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

    public String getFromdt() {
        return fromdt;
    }

    public void setFromdt(String fromdt) {
        this.fromdt = fromdt;
    }

    public String getTodt() {
        return todt;
    }

    public void setTodt(String todt) {
        this.todt = todt;
    }

    private String todt;


}
