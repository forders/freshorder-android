package com.freshorders.freshorder.model;

public class SalesmanListFetchByIdModel {

    private String usertype = "D";
    private String userid;
    private String saleslist = "Y";

    public String getUsertype() {
        return usertype;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getSaleslist() {
        return saleslist;
    }

    public void setSaleslist(String saleslist) {
        this.saleslist = saleslist;
    }
}
