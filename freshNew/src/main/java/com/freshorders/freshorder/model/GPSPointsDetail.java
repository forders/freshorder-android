package com.freshorders.freshorder.model;

public class GPSPointsDetail {

    private Long rowid;
    private int batchid;

    private String suserid;
    private String logtime;
    private double geolat;
    private double geolong;
    private double pgeolat;
    private double pgeolong;

    private String status;
    private String updateddt;
    private double distance;
    private int amount;

    private int issynced;
    private Long pre_id;
    private String seqid;

    public GPSPointsDetail(Long rowid, int batchid, String suserid, String logtime, double geolat, double geolong, double preLat, double preLon, String status, String updateddt, double distance, int amount, int issynced, Long pre_id, String seqid) {
        this.rowid = rowid;
        this.batchid = batchid;
        this.suserid = suserid;
        this.logtime = logtime;
        this.geolat = geolat;
        this.geolong = geolong;
        this.pgeolong  = preLat;
        this.pgeolong  = preLon;
        this.status = status;
        this.updateddt = updateddt;
        this.distance = distance;
        this.amount = amount;
        this.issynced = issynced;
        this.pre_id = pre_id;
        this.seqid = seqid;
    }

    public Long getRowid() {
        return rowid;
    }

    public void setRowid(Long rowid) {
        this.rowid = rowid;
    }

    public int getBatchid() {
        return batchid;
    }

    public void setBatchid(int batchid) {
        this.batchid = batchid;
    }

    public String getSuserid() {
        return suserid;
    }

    public void setSuserid(String suserid) {
        this.suserid = suserid;
    }

    public String getLogtime() {
        return logtime;
    }

    public void setLogtime(String logtime) {
        this.logtime = logtime;
    }

    public double getGeolat() {
        return geolat;
    }

    public void setGeolat(double geolat) {
        this.geolat = geolat;
    }

    public double getGeolong() {
        return geolong;
    }

    public void setGeolong(double geolong) {
        this.geolong = geolong;
    }


    public double getPgeolat() {
        return pgeolat;
    }

    public void setPgeolat(double pgeolat) {
        this.pgeolat = pgeolat;
    }

    public double getPgeolong() {
        return pgeolong;
    }

    public void setPgeolong(double pgeolong) {
        this.pgeolong = pgeolong;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUpdateddt() {
        return updateddt;
    }

    public void setUpdateddt(String updateddt) {
        this.updateddt = updateddt;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getIssynced() {
        return issynced;
    }

    public void setIssynced(int issynced) {
        this.issynced = issynced;
    }

    public Long getPre_id() {
        return pre_id;
    }

    public void setPre_id(Long pre_id) {
        this.pre_id = pre_id;
    }

    public String getSeqid() {
        return seqid;
    }

    public void setSeqid(String seqid) {
        this.seqid = seqid;
    }
}
