package com.freshorders.freshorder.model;

public class StockAuditSendStock {

    private int stkauditid;
    private int auditqty;
    ////////////////////  need to remove
    private int stockqty;

    public int getStkauditid() {
        return stkauditid;
    }

    public void setStkauditid(int stkauditid) {
        this.stkauditid = stkauditid;
    }

    public int getAuditqty() {
        return auditqty;
    }

    public void setAuditqty(int auditqty) {
        this.auditqty = auditqty;
    }

    public int getStockqty() {
        return stockqty;
    }

    public void setStockqty(int stockqty) {
        this.stockqty = stockqty;
    }
}
