package com.freshorders.freshorder.model;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;

@SuppressLint("ParcelCreator")
public class OrderDetail implements Parcelable {

    private String orddtlid;
    private String ordid;
    private String ordslno;
    private String duserid;
    private String prodid;
    private String batchno;
    private String manufdt;
    private String expirydt;
    private String qty;
    private String cqty;
    private String currstock;
    private String prate;
    private String ptax;
    private String pvalue;
    private String isfree;
    private String iscash;
    private String isread;
    private String deliverydt;
    private String deliverytype;
    private String deliverytime;
    private String discntval;
    private String discntprcnt;
    private String ordimage;
    private String ordstatus;
    private String stock;
    private String ord_uom;
    private String default_uom;
    private String updateddt;
    private String updatedby;
    private String deliveryqty;
    private String shippedqty;
    private String acceptedyn;
    private String receivedQty;
    private boolean isAccepted;

    public String getReceivedQty() {
        return receivedQty;
    }

    public void setReceivedQty(String receivedQty) {
        this.receivedQty = receivedQty;
    }

    public boolean isAccepted() {
        return isAccepted;
    }

    public void setAccepted(boolean accepted) {
        isAccepted = accepted;
    }

    public OrderDetail(Parcel in) {
        orddtlid = in.readString();
        ordid = in.readString();
        ordslno = in.readString();
        duserid = in.readString();
        prodid = in.readString();
        batchno = in.readString();
        manufdt = in.readString();
        expirydt = in.readString();
        qty = in.readString();
        cqty = in.readString();
        currstock = in.readString();
        prate = in.readString();
        ptax = in.readString();
        pvalue = in.readString();
        isfree = in.readString();
        iscash = in.readString();
        isread = in.readString();
        deliverydt = in.readString();
        deliverytype = in.readString();
        deliverytime = in.readString();
        discntval = in.readString();
        discntprcnt = in.readString();
        ordimage = in.readString();
        ordstatus = in.readString();
        stock = in.readString();
        ord_uom = in.readString();
        default_uom = in.readString();
        updateddt = in.readString();
        updatedby = in.readString();
        deliveryqty = in.readString();
        shippedqty = in.readString();
        acceptedyn = in.readString();
        receivedQty = in.readString();
        isAccepted  = (in.readInt() == 0) ? false : true;
        product = in.readParcelable(Product.class.getClassLoader());
    }

    public static final Creator<OrderDetail> CREATOR = new Creator<OrderDetail>() {
        @Override
        public OrderDetail createFromParcel(Parcel in) {
            return new OrderDetail(in);
        }

        @Override
        public OrderDetail[] newArray(int size) {
            return new OrderDetail[size];
        }
    };

    public OrderDetail() {

    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    private Product product;

    public String getOrddtlid() {
        return orddtlid;
    }

    public void setOrddtlid(String orddtlid) {
        this.orddtlid = orddtlid;
    }

    public String getOrdid() {
        return ordid;
    }

    public void setOrdid(String ordid) {
        this.ordid = ordid;
    }

    public String getOrdslno() {
        return ordslno;
    }

    public void setOrdslno(String ordslno) {
        this.ordslno = ordslno;
    }

    public String getDuserid() {
        return duserid;
    }

    public void setDuserid(String duserid) {
        this.duserid = duserid;
    }

    public String getProdid() {
        return prodid;
    }

    public void setProdid(String prodid) {
        this.prodid = prodid;
    }

    public String getBatchno() {
        return batchno;
    }

    public void setBatchno(String batchno) {
        this.batchno = batchno;
    }

    public String getManufdt() {
        return manufdt;
    }

    public void setManufdt(String manufdt) {
        this.manufdt = manufdt;
    }

    public String getExpirydt() {
        return expirydt;
    }

    public void setExpirydt(String expirydt) {
        this.expirydt = expirydt;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getCqty() {
        return cqty;
    }

    public void setCqty(String cqty) {
        this.cqty = cqty;
    }

    public String getCurrstock() {
        return currstock;
    }

    public void setCurrstock(String currstock) {
        this.currstock = currstock;
    }

    public String getPrate() {
        return prate;
    }

    public void setPrate(String prate) {
        this.prate = prate;
    }

    public String getPtax() {
        return ptax;
    }

    public void setPtax(String ptax) {
        this.ptax = ptax;
    }

    public String getPvalue() {
        return pvalue;
    }

    public void setPvalue(String pvalue) {
        this.pvalue = pvalue;
    }

    public String getIsfree() {
        return isfree;
    }

    public void setIsfree(String isfree) {
        this.isfree = isfree;
    }

    public String getIscash() {
        return iscash;
    }

    public void setIscash(String iscash) {
        this.iscash = iscash;
    }

    public String getIsread() {
        return isread;
    }

    public void setIsread(String isread) {
        this.isread = isread;
    }

    public String getDeliverydt() {
        return deliverydt;
    }

    public void setDeliverydt(String deliverydt) {
        this.deliverydt = deliverydt;
    }

    public String getDeliverytype() {
        return deliverytype;
    }

    public void setDeliverytype(String deliverytype) {
        this.deliverytype = deliverytype;
    }

    public String getDeliverytime() {
        return deliverytime;
    }

    public void setDeliverytime(String deliverytime) {
        this.deliverytime = deliverytime;
    }

    public String getDiscntval() {
        return discntval;
    }

    public void setDiscntval(String discntval) {
        this.discntval = discntval;
    }

    public String getDiscntprcnt() {
        return discntprcnt;
    }

    public void setDiscntprcnt(String discntprcnt) {
        this.discntprcnt = discntprcnt;
    }

    public String getOrdimage() {
        return ordimage;
    }

    public void setOrdimage(String ordimage) {
        this.ordimage = ordimage;
    }

    public String getOrdstatus() {
        return ordstatus;
    }

    public void setOrdstatus(String ordstatus) {
        this.ordstatus = ordstatus;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getOrd_uom() {
        return ord_uom;
    }

    public void setOrd_uom(String ord_uom) {
        this.ord_uom = ord_uom;
    }

    public String getDefault_uom() {
        return default_uom;
    }

    public void setDefault_uom(String default_uom) {
        this.default_uom = default_uom;
    }

    public String getUpdateddt() {
        return updateddt;
    }

    public void setUpdateddt(String updateddt) {
        this.updateddt = updateddt;
    }

    public String getUpdatedby() {
        return updatedby;
    }

    public void setUpdatedby(String updatedby) {
        this.updatedby = updatedby;
    }

    public String getDeliveryqty() {
        return deliveryqty;
    }

    public void setDeliveryqty(String deliveryqty) {
        this.deliveryqty = deliveryqty;
    }

    public String getShippedqty() {
        return shippedqty;
    }

    public void setShippedqty(String shippedqty) {
        this.shippedqty = shippedqty;
    }

    public String getAcceptedyn() {
        return acceptedyn;
    }

    public void setAcceptedyn(String acceptedyn) {
        this.acceptedyn = acceptedyn;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(orddtlid);
        parcel.writeString(ordid);
        parcel.writeString(ordslno);
        parcel.writeString(duserid);
        parcel.writeString(prodid);
        parcel.writeString(batchno);
        parcel.writeString(manufdt);
        parcel.writeString(expirydt);
        parcel.writeString(qty);
        parcel.writeString(cqty);
        parcel.writeString(currstock);
        parcel.writeString(prate);
        parcel.writeString(ptax);
        parcel.writeString(pvalue);
        parcel.writeString(isfree);
        parcel.writeString(iscash);
        parcel.writeString(isread);
        parcel.writeString(deliverydt);
        parcel.writeString(deliverytype);
        parcel.writeString(deliverytime);
        parcel.writeString(discntval);
        parcel.writeString(discntprcnt);
        parcel.writeString(ordimage);
        parcel.writeString(ordstatus);
        parcel.writeString(stock);
        parcel.writeString(ord_uom);
        parcel.writeString(default_uom);
        parcel.writeString(updateddt);
        parcel.writeString(updatedby);
        parcel.writeString(deliveryqty);
        parcel.writeString(shippedqty);
        parcel.writeString(acceptedyn);
        parcel.writeString(receivedQty);
        parcel.writeInt(isAccepted ? 1 : 0);
        parcel.writeParcelable(product, i);
    }
}
