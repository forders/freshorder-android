package com.freshorders.freshorder.model;

public class SurveyCustomField {

    private int id;
    private String custmfldid;
    private String custmfldvalue;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCustmfldid() {
        return custmfldid;
    }

    public void setCustmfldid(String custmfldid) {
        this.custmfldid = custmfldid;
    }

    public String getCustmfldvalue() {
        return custmfldvalue;
    }

    public void setCustmfldvalue(String custmfldvalue) {
        this.custmfldvalue = custmfldvalue;
    }
}
