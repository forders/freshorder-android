package com.freshorders.freshorder.model;

import org.json.JSONArray;

public class PlaceHierarchyResModel {

    private String status;
    private JSONArray data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public JSONArray getData() {
        return data;
    }

    public void setData(JSONArray data) {
        this.data = data;
    }
}
