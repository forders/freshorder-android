package com.freshorders.freshorder.fragment;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.db.DatabaseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

public class ManagerTargetManagementFragment extends Fragment {

    public static final String TAG = ManagerTargetManagementFragment.class.getSimpleName();

    private View viewFragment;
    private Context mContext;
    private LinearLayout lineHeaderProgress ;

    JSONObject JsonSurvyObject = null;
    JSONArray JsonAccountArray = null;
    public static DatabaseHandler databaseHandler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        viewFragment = inflater.inflate(R.layout.fragment_manager_target_management, container, false);
        mContext = inflater.getContext();
        databaseHandler = new DatabaseHandler(mContext.getApplicationContext());
        JsonAccountArray = new JSONArray();

        return viewFragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lineHeaderProgress = viewFragment.findViewById(R.id.lineHeaderProgress_manager_target_management);

        fetchingData();
    }

    private void fetchingData(){

    }

}
