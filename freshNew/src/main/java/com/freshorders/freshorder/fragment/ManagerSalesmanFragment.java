package com.freshorders.freshorder.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.ManagerSalesmanAdapter;
import com.freshorders.freshorder.asyntask.ManagerClientManagement;
import com.freshorders.freshorder.asyntask.ManagerSalesManagement;
import com.freshorders.freshorder.asyntask.SalesmanListAsync;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.model.HierarchyObjModel;
import com.freshorders.freshorder.model.ManagerSalesmanAdapterModel;
import com.freshorders.freshorder.model.MyItem;
import com.freshorders.freshorder.model.SalesmanListFetchByIdModel;
import com.freshorders.freshorder.model.SalesmanPlanModel;
import com.freshorders.freshorder.popup.PopupOutletStockEntryDatePicker;
import com.freshorders.freshorder.popup.PopupTreeView;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.Dialogs;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.MobileNet;
import com.freshorders.freshorder.utils.Utils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import static android.widget.LinearLayout.VERTICAL;
import static com.freshorders.freshorder.service.PrefManager.KEY_IS_HIERARCHY_PLACE_FILE_LOADED;
import static com.freshorders.freshorder.service.PrefManager.KEY_MANAGER_TYPE;
import static com.freshorders.freshorder.service.PrefManager.KEY_PATH_HIERARCHY_PLACE_FILE;

public class ManagerSalesmanFragment extends Fragment implements OnMapReadyCallback,
                                                                    ClusterManager.OnClusterClickListener<MyItem>,
                                                                    ClusterManager.OnClusterInfoWindowClickListener<MyItem>,
                                                                    ClusterManager.OnClusterItemClickListener<MyItem>,
                                                                    ClusterManager.OnClusterItemInfoWindowClickListener<MyItem>{

    public static final String TAG = ManagerClientVisitFragment.class.getSimpleName();

    private String selectedSalesmanId;
    ////////////private String selectedDate;
    private String distributorId;
    private final int INITIAL_DEFAULT_ZOOM = 8;

    SupportMapFragment mMapFragment;
    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationClient;
    private double longitude;
    private double latitude;
    private final int DEFAULT_ZOOM = 18;

    private ClusterManager<MyItem> mClusterManager;
    private Cluster<MyItem> clickedCluster;
    private MyItem clickedClusterItem;

    private View viewFragment;
    private Context mContext;
    private LinearLayout lineHeaderProgress ;

    JSONObject JsonSurvyObject = null;
    JSONArray JsonAccountArray = null;
    public static DatabaseHandler databaseHandler;

    EditText txtDate;
    String strSelectedDate = "";
    androidx.appcompat.widget.AppCompatSpinner spinSalesman;
    RelativeLayout llFilter;
    ImageView ivFilter;

    ArrayAdapter<String> adapter;
    private String[] arrSpinSalesman = {
    };

    LinkedHashMap<String,String> linkedHashMap = new LinkedHashMap<>();
    List<String> listOfSalesman = new ArrayList<>();
    List<String> listSalesmanId = new ArrayList<>();

    List<String> listSalesman;
    private String selectedSalesman = "";
    private int selectedSalesmanIndex = -1;

    private LinearLayout llMap;
    private LinearLayout llSalesman;

    private RecyclerView recyclerview;
    ManagerSalesmanAdapter rvAdapter;
    private List<ManagerSalesmanAdapterModel> listItems;


    private String ManagerType = "";
    private EditText eTxtPlace;
    private String hierarchyPlace = "";
    private HierarchyObjModel selectedItem ;

    private void setRecyclerView(){
        recyclerview = (RecyclerView) viewFragment.findViewById(R.id.id_manager_salesman_RV);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerview.setLayoutManager(layoutManager);
        //Add Divider
        DividerItemDecoration itemDecor = new DividerItemDecoration(recyclerview.getContext(), VERTICAL);
        recyclerview.addItemDecoration(itemDecor);
        //creating recyclerView adapter
        rvAdapter = new ManagerSalesmanAdapter(getActivity(), listItems,viewFragment);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        //setting adapter to recyclerView
        recyclerview.setAdapter(rvAdapter);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        viewFragment = inflater.inflate(R.layout.fragment_manager_salesman_management, container, false);
        mContext = inflater.getContext();
        listItems = new ArrayList<>();
        databaseHandler = new DatabaseHandler(mContext.getApplicationContext());
        JsonAccountArray = new JSONArray();
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(mContext);
        setUpMap();///////////////
        setRecyclerView();
        return viewFragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        lineHeaderProgress = viewFragment.findViewById(R.id.lineHeaderProgress_manager_salesman);
        txtDate = viewFragment.findViewById(R.id.id_manager_salesman_select_date);
        spinSalesman = viewFragment.findViewById(R.id.id_manager_salesman_spin_salesman);
        llFilter = viewFragment.findViewById(R.id.LL_manager_salesman_show_hide);
        ivFilter = viewFragment.findViewById(R.id.id_manager_salesman_IV_filter);

        llMap = viewFragment.findViewById(R.id.ll_manager_salesman_bottom);
        llSalesman = viewFragment.findViewById(R.id.ll_manager_salesman_bottom1);

        ManagerType = new PrefManager(mContext).getStringDataByKey(KEY_MANAGER_TYPE);
        updateUserId();/////////////////

        ///Default date setting
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        txtDate.setText(formattedDate);
        strSelectedDate = formattedDate;

        ////////////////fetchingData();

        Button btnSalesman = viewFragment.findViewById(R.id.id_manager_salesman_btn_view_sales);
        Button btnMap = viewFragment.findViewById(R.id.id_manager_salesman_btn_view_map);

        btnSalesman.setOnClickListener(btnSalesmanClickListener);
        btnMap.setOnClickListener(btnMapClickListener);

        listSalesman = new ArrayList<>(Arrays.asList(arrSpinSalesman));
        adapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, listSalesman);
        spinSalesman.setAdapter(adapter);

        txtDate.setOnClickListener(selectDate);

        ivFilter.setOnClickListener(filterListener);
        spinSalesman.setOnItemSelectedListener(new ManagerSalesmanFragment.SalesmanSelectedListener());

        eTxtPlace = viewFragment.findViewById(R.id.id_M_salesman_eTxt_select_place);
        if(ManagerType.equalsIgnoreCase("U")){
            eTxtPlace.setVisibility(View.VISIBLE);
            eTxtPlace.setOnClickListener(placeClick);
        }
    }

    private View.OnClickListener placeClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //DisplayMetrics displayMetrics = new DisplayMetrics();
            //mContext.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            ////hideKeyBoard();
            DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();

            boolean isFileLoaded = new PrefManager(mContext).getBooleanDataByKey(KEY_IS_HIERARCHY_PLACE_FILE_LOADED);
            if(isFileLoaded){
                String filePath = new PrefManager(mContext).getStringDataByKey(KEY_PATH_HIERARCHY_PLACE_FILE);
                PopupTreeView popup = new PopupTreeView(viewFragment,
                        mContext, filePath, eTxtPlace,
                        displayMetrics, true,  new PopupTreeView.OnDismissListener() {
                    @Override
                    public void onDismiss(HierarchyObjModel selectedItems) {
                        if(selectedItems == null){
                            showAlertDialogToast(getResources().getString(R.string.not_retrieve_data));
                        }else {
                            selectedItem = selectedItems;
                        }
                        hierarchyPlace = eTxtPlace.getText().toString().trim();
                        if(!hierarchyPlace.isEmpty()){
                            clear();
                            fetchingData();
                        }
                    }
                });
                ////popup.showPopup();
                popup.showPopupWindow();
            }else{
                showAlertDialogToast(getResources().getString(R.string.data_not_load_when_login));
            }
        }
    };

    private void fetchingData(){
        if(MobileNet.isNetAvail(mContext)) {
            if(ManagerType.equalsIgnoreCase("U")){
                Log.e(TAG, "..Type...." + ManagerType);
                hierarchyPlace = eTxtPlace.getText().toString().trim();
                if(!hierarchyPlace.isEmpty()){
                    show();
                    getSalesmanList();
                }
            }else {
                show();
                getSalesmanList();
            }
        }else {
            showAlertDialogToast(mContext.getResources().getString(R.string.no_active_network_avail));
        }
    }

    private void clearCluster(){
        mMap.clear();
        mClusterManager.clearItems();
    }

    View.OnClickListener btnMapClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if(!MobileNet.isNetAvail(mContext)){
                showAlertDialogToast(mContext.getResources().getString(R.string.no_active_network_avail));
                return;
            }

            if(ManagerType.equalsIgnoreCase("U")){
                if(hierarchyPlace.isEmpty()){
                    showAlertDialogToast(mContext.getResources().getString(R.string.select_place_first));
                    return;
                }
            }

            updateUserId();/////////////

            if(llSalesman.getVisibility() == View.VISIBLE){
                llSalesman.setVisibility(View.GONE);
            }
            llMap.setVisibility(View.VISIBLE);

            SalesmanPlanModel model = new SalesmanPlanModel();

            listItems.clear();
            clearCluster();

            strSelectedDate = txtDate.getText().toString().trim();
            if(strSelectedDate.isEmpty() || selectedSalesmanId.isEmpty()){
                showAlertDialogToast("Should select salesman and date");
                return;
            }

            try {
                Log.e("Selected",".........................................Date:" + strSelectedDate);
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
                SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                Date newDate = format.parse(strSelectedDate);
                Log.e("Date",".........................................." + newDate);
                strSelectedDate = format1.format(newDate);
            }catch (Exception e){
                e.printStackTrace();
                showAlertDialogToast(mContext.getResources().getString(R.string.technical_fault) + " " +e.getMessage());
                return;
            }

            if(selectedSalesmanId != null && selectedSalesmanId.length() > 0){
                model.setSuserid(selectedSalesmanId);
            }else {
                if(ManagerType.equalsIgnoreCase("D") ||
                        ManagerType.equalsIgnoreCase("U")){
                    new Dialogs(mContext).showDialogOKCancel(
                            "Info",
                            mContext.getResources().getString(R.string.salesman_list_not_loaded_click_ok),
                            "OK",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    show();
                                    getSalesmanList();

                                }
                            },
                            "Cancel",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();

                                }
                            },
                            false
                    );
                }
                return;
            }
            model.setDate(strSelectedDate);
            Gson gson = new Gson();
            String json = gson.toJson(model);
            Log.e(TAG, "............................."+json);
            show();

            JsonServiceHandler jsonServiceHandler = new JsonServiceHandler(Utils.strManagerSalesman, json, mContext);
            ManagerSalesManagement obj = new ManagerSalesManagement(jsonServiceHandler,
                    new ManagerSalesManagement.ClientManagementCompleteListener() {
                        @Override
                        public void onSuccess(JSONObject result) {
                            hide();
                            try {
                                JSONArray resultArray;
                                if(!result.has("data")){
                                    showAlertDialogToast("no data found");
                                    return;
                                }else {
                                    resultArray = result.getJSONArray("data");
                                    if(resultArray == null){
                                        showAlertDialogToast("no data found");
                                        return;
                                    }
                                }
                                for(int i = 0; i < resultArray.length(); i++){
                                    JSONObject object = resultArray.getJSONObject(i);
                                    ManagerSalesmanAdapterModel currentItem = new ManagerSalesmanAdapterModel();
                                    try {
                                        currentItem.setStorename(object.getString("storename"));
                                        currentItem.setOrdid(object.getString("ordid"));
                                        currentItem.setStart_time(object.getString("start_time"));
                                        currentItem.setEnd_time(object.getString("end_time"));
                                        String dateTime = object.getString("orderdt");
                                        currentItem.setOrderdt(dateTime);
                                        currentItem.setGeolat(object.getString("geolat"));
                                        currentItem.setGeolong(object.getString("geolong"));
                                        listItems.add(currentItem);
                                    }catch (Exception e){
                                        e.printStackTrace();
                                        showAlertDialogToast(e.getMessage());
                                        return;
                                    }
                                }
                                ////////////////////////
                                addCursorItems();
                            }catch (Exception e){
                                e.printStackTrace();
                                showAlertDialogToast(e.getMessage());
                            }
                        }

                        @Override
                        public void onFailed(String msg) {
                            hide();
                            if(msg.isEmpty()){
                                showAlertDialogToast("No response from cloud server");
                            }else {
                                showAlertDialogToast(msg);
                            }
                        }

                        @Override
                        public void onException(String msg) {
                            hide();
                            if(msg.isEmpty()){
                                showAlertDialogToast("No response from cloud server");
                            }else {
                                showAlertDialogToast(msg);
                            }
                        }
                    });
            obj.execute();
        }
    };

    View.OnClickListener btnSalesmanClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if(!MobileNet.isNetAvail(mContext)){
                showAlertDialogToast(mContext.getResources().getString(R.string.no_active_network_avail));
                return;
            }

            if(ManagerType.equalsIgnoreCase("U")){
                if(hierarchyPlace.isEmpty()){
                    showAlertDialogToast(mContext.getResources().getString(R.string.select_place_first));
                    return;
                }
            }
            updateUserId();/////////////

            if(llMap.getVisibility() == View.VISIBLE){
                llMap.setVisibility(View.GONE);
            }
            llSalesman.setVisibility(View.VISIBLE);
            SalesmanPlanModel model = new SalesmanPlanModel();

            listItems.clear();
            strSelectedDate = txtDate.getText().toString().trim();

            if(strSelectedDate.isEmpty() || selectedSalesmanId.isEmpty()){
                showAlertDialogToast("Should select salesman and date");
                return;
            }

            if(strSelectedDate.length() > 0){
                try {
                    Log.e("Selected",".........................................Date:" + strSelectedDate);
                    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
                    SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                    Date newDate = format.parse(strSelectedDate);
                    Log.e("Date",".........................................." + newDate);
                    strSelectedDate = format1.format(newDate);
                }catch (Exception e){
                    e.printStackTrace();
                    showAlertDialogToast(mContext.getResources().getString(R.string.technical_fault) + " " +e.getMessage());
                    return;
                }
            }

            if(selectedSalesmanId != null && selectedSalesmanId.length() > 0){
                model.setSuserid(selectedSalesmanId);
            }else {
                if(ManagerType.equalsIgnoreCase("D") ||
                        ManagerType.equalsIgnoreCase("U")){
                    new Dialogs(mContext).showDialogOKCancel(
                            "Info",
                            mContext.getResources().getString(R.string.salesman_list_not_loaded_click_ok),
                            "OK",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    show();
                                    getSalesmanList();

                                }
                            },
                            "Cancel",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();

                                }
                            },
                            false
                    );
                }
                return;
            }

            model.setDate(strSelectedDate);

            Gson gson = new Gson();
            String json = gson.toJson(model);
            Log.e(TAG, "............................."+json);
            show();
            String param = "{where={status=Active,orderplndt={between=["+strSelectedDate+"%2B00:00,"+strSelectedDate+"%2B23:59]}," +
                    "duserid="+Constants.DUSER_ID+",suserid="+selectedSalesmanId+",include=[distributor,beat,suser]}";


            JsonServiceHandler jsonServiceHandler = new JsonServiceHandler(Utils.strManagerSalesman, json, mContext);
            ManagerClientManagement obj = new ManagerClientManagement(jsonServiceHandler,
                    new ManagerClientManagement.ClientManagementCompleteListener() {
                        @Override
                        public void onSuccess(JSONObject result) {
                            hide();
                            try {
                                JSONArray resultArray;
                                if(!result.has("data")){
                                    showAlertDialogToast("no data found");
                                    return;
                                }else {
                                    resultArray = result.getJSONArray("data");
                                    if(resultArray == null){
                                        showAlertDialogToast("no data found");
                                        return;
                                    }
                                }

                                //JSONArray resultArray = result.getJSONArray("data");
                                for(int i = 0; i < resultArray.length(); i++){
                                    JSONObject object = resultArray.getJSONObject(i);
                                    ManagerSalesmanAdapterModel currentItem = new ManagerSalesmanAdapterModel();
                                    try {
                                        currentItem.setStorename(object.getString("storename"));
                                        currentItem.setOrdid(object.getString("ordid"));
                                        currentItem.setStart_time(object.getString("start_time"));
                                        currentItem.setEnd_time(object.getString("end_time"));
                                        String dateTime = object.getString("orderdt");
                                        currentItem.setOrderdt(dateTime);


                                        listItems.add(currentItem);
                                    }catch (Exception e){
                                        e.printStackTrace();
                                        showAlertDialogToast(mContext.getResources().getString(R.string.technical_fault) + " " +e.getMessage());
                                        return;
                                    }
                                }
                                rvAdapter.notifyDataSetChanged();

                            }catch (Exception e){
                                e.printStackTrace();
                                showAlertDialogToast(mContext.getResources().getString(R.string.technical_fault) + " " +e.getMessage());
                            }
                        }

                        @Override
                        public void onFailed(String msg) {
                            hide();
                            if(msg.isEmpty()){
                                showAlertDialogToast("No response from cloud server");
                            }else {
                                showAlertDialogToast(msg);
                            }
                        }

                        @Override
                        public void onException(String msg) {
                            hide();
                            if(msg.isEmpty()){
                                showAlertDialogToast("No response from cloud server");
                            }else {
                                showAlertDialogToast(msg);
                            }
                        }
                    });
            obj.execute();
        }
    };

    View.OnClickListener selectDate = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            PopupOutletStockEntryDatePicker datePopup = new
                    PopupOutletStockEntryDatePicker(
                    mContext, txtDate,
                    new PopupOutletStockEntryDatePicker.PopupListener() {
                        @Override
                        public void onPopupClosed() {
                            strSelectedDate = txtDate.getText().toString().trim();
                        }
                    });
            datePopup.showPopup();
        }
    };

    View.OnClickListener filterListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(llFilter.getVisibility() == View.VISIBLE){
                llFilter.setVisibility(View.INVISIBLE);
                ////////////// reset
                txtDate.setText(Constants.EMPTY);
                strSelectedDate = Constants.EMPTY;
            }else {
                llFilter.setVisibility(View.VISIBLE);
                spinSalesman.setSelection(0);
            }
        }
    };


    class SalesmanSelectedListener implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            selectedSalesmanIndex = pos;
            if(listOfSalesman.size() > 0) {
                selectedSalesman = listOfSalesman.get(selectedSalesmanIndex);
                selectedSalesmanId = listSalesmanId.get(selectedSalesmanIndex);
                Toast.makeText(mContext, selectedSalesman, Toast.LENGTH_LONG).show();
            }
        }
        public void onNothingSelected(AdapterView<?> parent) {
            // Dummy
        }
    }

    private void setUpMap() {
        mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mMapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (mMap != null) {
            return;
        }
        mMap = googleMap;
        initializeMap();
        Log.e(TAG,"Map ready...........................................................");
    }

    @SuppressLint("MissingPermission")
    private void initializeMap(){
        try {
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.getUiSettings().setCompassEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.getUiSettings().setRotateGesturesEnabled(true);
            mMap.getUiSettings().setScrollGesturesEnabled(true);
            mMap.getUiSettings().setTiltGesturesEnabled(true);
            mMap.getUiSettings().setZoomGesturesEnabled(true);
            mMap.setMyLocationEnabled(true);

            initializeCluster();

        } catch (Exception e) {
            //mMapFrame.setVisibility(View.GONE);
            //Utils.displayToast("Your device doesn't support Google Map", Map.this);
        }
    }

    private void initializeCluster(){

        mClusterManager = new ClusterManager<>(mContext, mMap);

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(13.067439,80.237617), INITIAL_DEFAULT_ZOOM));

        mMap.setOnCameraIdleListener(mClusterManager);

        mClusterManager.setRenderer(new ManagerSalesmanFragment.MyClusterRenderer(mContext, mMap,
                mClusterManager));


        mMap.setOnMarkerClickListener(mClusterManager);

        mMap.setInfoWindowAdapter(mClusterManager.getMarkerManager());

        mMap.setOnInfoWindowClickListener(mClusterManager); //added
        mClusterManager.setOnClusterItemInfoWindowClickListener(this); //added

        mClusterManager
                .setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<MyItem>() {
                    @Override
                    public boolean onClusterItemClick(MyItem item) {
                        clickedClusterItem = item;
                        return false;
                    }
                });

        mClusterManager
                .setOnClusterClickListener(new ClusterManager.OnClusterClickListener<MyItem>() {
                    @Override
                    public boolean onClusterClick(Cluster<MyItem> cluster) {
                        clickedCluster = cluster;
                        return false;
                    }
                });




        /////addItems();

        mClusterManager.getMarkerCollection().setOnInfoWindowAdapter(
                new ManagerSalesmanFragment.MyCustomAdapterForItems());
    }

    private void addCursorItems(){


        double firstLat = 13.067439, firstLan = 80.237617;
        if(listItems != null && listItems.size() > 0){
            Log.e(TAG,"..........................................................`````````````size"+listItems.size());
            for(int i = 0; i < listItems.size(); i++){
                ManagerSalesmanAdapterModel currentItems = listItems.get(i);
                String strLat = currentItems.getGeolat();
                String strLan = currentItems.getGeolong();
                String name = currentItems.getStorename();

                double lat = Double.parseDouble(strLat);
                double lng = Double.parseDouble(strLan);
                if(i == 0){
                    firstLat = lat;
                    firstLan = lng;
                }
                MyItem offsetItem = new MyItem(lat, lng, "Merchant " + name, Constants.EMPTY);
                mClusterManager.addItem(offsetItem);

            }
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(firstLat,firstLan), DEFAULT_ZOOM));
            mClusterManager.cluster();
        }else {
            showAlertDialogToast("No Data From Server");
        }
    }

    private void addItems() {

        double latitude = 37.779977;
        double longitude = -122.413742;
        for (int i = 0; i < 10; i++) {
            double offset = i / 60d;

            double lat = latitude + offset;
            double lng = longitude + offset;
            MyItem offsetItem = new MyItem(lat, lng, "title " + i+1, "snippet " + i+1);
            mClusterManager.addItem(offsetItem);

        }

    }

    class MyClusterRenderer extends DefaultClusterRenderer<MyItem> {

        public MyClusterRenderer(Context context, GoogleMap map,
                                 ClusterManager<MyItem> clusterManager) {
            super(context, map, clusterManager);
        }

        @Override
        protected void onBeforeClusterItemRendered(MyItem item,
                                                   MarkerOptions markerOptions) {
            super.onBeforeClusterItemRendered(item, markerOptions);
        }

        @Override
        protected void onClusterItemRendered(MyItem clusterItem, Marker marker) {
            super.onClusterItemRendered(clusterItem, marker);
        }

    }

    public class MyCustomAdapterForItems implements GoogleMap.InfoWindowAdapter {

        private final View myContentsView;

        MyCustomAdapterForItems() {
            myContentsView = getActivity().getLayoutInflater().inflate(
                    R.layout.info_window, null);
        }

        @Override
        public View getInfoWindow(Marker marker) {
            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {

            TextView tvTitle = ((TextView) myContentsView
                    .findViewById(R.id.txtTitle));
            TextView tvSnippet = ((TextView) myContentsView
                    .findViewById(R.id.txtSnippet));

            tvTitle.setText(clickedClusterItem.getTitle());
            tvSnippet.setText(clickedClusterItem.getSnippet());

            return myContentsView;
        }
    }



    @Override
    public boolean onClusterClick(Cluster<MyItem> cluster) {
        return false;
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<MyItem> cluster) {

    }

    @Override
    public boolean onClusterItemClick(MyItem myItem) {
        return false;
    }

    @Override
    public void onClusterItemInfoWindowClick(MyItem myItem) {

    }

    private void spinnerUpdate(List<String> listSalesman){
        adapter.clear();
        if(listSalesman.size() > 0) {
            adapter.addAll(listSalesman);
        }
        adapter.notifyDataSetChanged();
        /// Initial Setting  //////////////////////
        selectedSalesman = listSalesman.get(0);
        selectedSalesmanId = listSalesmanId.get(0);
        ///////////////////////////////////////////
    }

    private void getSalesmanList(){

        updateUserId();//////////////
        String json = "";
        if(ManagerType.equalsIgnoreCase("U")){
            try {
                /*
                JSONObject obj = new JSONObject();
                obj.put("duserid", Constants.DUSER_ID);
                JSONArray array = new JSONArray(hierarchyPlace);
                obj.put("hierarchytext" , array);
                Log.e(TAG, "..........ManagerType U............" + obj.toString()); */

                JSONStringer jsonStringer = new JSONStringer();
                jsonStringer.object()
                        .key("duserid").value(Constants.DUSER_ID)
                        .key("hierarchytext")
                        .array()
                        .value(hierarchyPlace)
                        .endArray()
                        .endObject();
                json = jsonStringer.toString();
            }catch (Exception e){
                hide();
                e.printStackTrace();
                return;
            }
        }else {
            SalesmanListFetchByIdModel model = new SalesmanListFetchByIdModel();
            model.setUserid(Constants.DUSER_ID);
            Gson gson = new Gson();
            json = gson.toJson(model);
        }
        Log.e(TAG, "............................."+json);
        JsonServiceHandler jsonServiceHandler;
        if(ManagerType.equalsIgnoreCase("U")){
            jsonServiceHandler = new JsonServiceHandler(Utils.strGetSalesManList, json, mContext);
        }else {
            jsonServiceHandler = new JsonServiceHandler(Utils.strGetMerchantList, json, mContext);
        }
        SalesmanListAsync obj = new SalesmanListAsync(jsonServiceHandler,
                new SalesmanListAsync.SalesmanListCompleteListener() {
                    @Override
                    public void onSuccess(JSONObject result) {
                        hide();
                        if(result.has("data")) {
                            try {
                                JSONArray jsonArray = result.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    String strUserId = object.getString("userid");
                                    String strName = object.getString("fullname");
                                    listOfSalesman.add(strName);
                                    listSalesmanId.add(strUserId);
                                    linkedHashMap.put(strUserId, strName);
                                }
                                spinnerUpdate(listOfSalesman);

                            }catch (Exception e){
                                e.printStackTrace();
                                showAlertDialogToast(mContext.getResources().getString(R.string.technical_fault) + " " +e.getMessage());
                            }
                        }
                    }

                    @Override
                    public void onFailed(String msg) {
                        hide();
                        showAlertDialogToast(msg);
                    }

                    @Override
                    public void onException(String msg) {
                        hide();
                        showAlertDialogToast(msg);
                    }
                });
        obj.execute();
    }


    private void show(){
        lineHeaderProgress.setVisibility(View.VISIBLE);
    }

    private void hide(){
        lineHeaderProgress.setVisibility(View.GONE);
    }

    /*
    private boolean checkPlayServices(){
        int resultCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(mContext);
        if(resultCode != ConnectionResult.SUCCESS){
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)){
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }else {
                Toast.makeText(mContext,
                        "This device is not supported", Toast.LENGTH_LONG)
                        .show();
            }
            return false;
        }
        return true;
    }  */


    public void showAlertDialogToast(String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    private void updateUserId(){
        if (Constants.DUSER_ID == null || Constants.DUSER_ID.isEmpty()) {
            if(databaseHandler == null) {
                databaseHandler =  new DatabaseHandler(getContext());//DatabaseHandler.getInstance(mContext.getApplicationContext());//new DatabaseHandler(getContext());
            }
            Cursor curs;
            curs = databaseHandler.getDetails();
            if(curs != null && curs.getCount() > 0) {
                curs.moveToFirst();
                if(ManagerType.equalsIgnoreCase("U")){
                    Constants.DUSER_ID = curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_clientdealid));
                }else {
                    Constants.DUSER_ID = curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_id));
                }
                curs.close();
                if(Constants.DUSER_ID != null && !Constants.DUSER_ID.isEmpty()){
                    Log.e("Constants.DUSER_ID", Constants.DUSER_ID);
                }else {
                    showAlertDialogToast(getResources().getString(R.string.should_re_login));
                }
            }else {
                showAlertDialogToast(getResources().getString(R.string.need_to_re_open));
            }
        }
    }

    private boolean isNetAvail(Context context){
        try {
            boolean isConnected,isMobile = false;
            ConnectivityManager cm =
                    (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = null;
            if (cm != null) {
                activeNetwork = cm.getActiveNetworkInfo();
            }else {
                return false;
            }

            isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
            if (activeNetwork != null) {
                isMobile = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE;
            }
            if(isConnected && isMobile){
                return true;
            }
            return false;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }



    @Override
    public void onStart() {
        super.onStart();
        if (listOfSalesman != null && listOfSalesman.size() > 0) {
            //returning from backstack, data is fine, do nothing
            Log.e(TAG,".............onStart..data is fine");
        } else {
            //newly created, compute data
            Log.e(TAG,".............onStart..newly created, compute data");
            clear();
            fetchingData();
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("list", (Serializable) listOfSalesman);
        Log.e(TAG,".............onSaveInstanceState..data is loaded");
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.e(TAG,".............onActivityCreated............");
        if (savedInstanceState != null) {
            //probably orientation change
            try {
                //listOfSalesman = (List<String>) savedInstanceState.getSerializable("list");
                Log.e(TAG,".............onActivityCreated..probably from background");
                clear();
                fetchingData();
            }catch (Exception e){
                e.printStackTrace();
            }

        } else {
            if (listOfSalesman != null && listOfSalesman.size() > 0) {
                //returning from backstack, data is fine, do nothing
                Log.e(TAG,".............onActivityCreated..data is fine");
            } else {
                //newly created, compute data
                Log.e(TAG,".............onActivityCreated..newly created, compute data");
                //clear();
                //fetchingData();
            }
        }
    }

    private void clear(){
        if(listOfSalesman != null){
            listOfSalesman.clear();
        }else {
            listOfSalesman = new ArrayList<>();
        }
        if(listSalesmanId != null){
            listSalesmanId.clear();
        }else {
            listSalesmanId = new ArrayList<>();
        }
        if(linkedHashMap != null){
            linkedHashMap.clear();
        }else {
            linkedHashMap = new LinkedHashMap<>();
        }
    }


}
