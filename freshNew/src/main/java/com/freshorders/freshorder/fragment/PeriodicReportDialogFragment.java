package com.freshorders.freshorder.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.utils.Log;
import com.freshorders.freshorder.weekcalendar.WeekCalendar;
import com.freshorders.freshorder.weekcalendar.listener.OnDateClickListener;
import com.freshorders.freshorder.weekcalendar.listener.OnWeekChangeListener;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class PeriodicReportDialogFragment extends DialogFragment {

    private View popupView;
    private View rootView;
    private Context mContext;
    private static final String TAG = PeriodicReportDialogFragment.class.getSimpleName();

    private WeekCalendar weekCalendar;
    private DateTimeFormatter dateTimeFormatter;
    private TextView tvSelectedDate;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.popup_week_view, container, false);
        mContext = v.getContext();
        popupView = v;
        // Do all the stuff to initialize your custom view
        init();
        return v;
    }

    private void init() {
        dateTimeFormatter = DateTimeFormat.forPattern("dd/MM/yyyy");
        tvSelectedDate = popupView.findViewById(R.id.id_week_selected_date);

        Button todaysDate = (Button) popupView.findViewById(R.id.today);
        Button selectedDate = (Button) popupView.findViewById(R.id.selectedDateButton);
        Button startDate = (Button) popupView.findViewById(R.id.startDate);
        todaysDate.setText(new DateTime().toLocalDate().toString() + " (Reset Button)");
        selectedDate.setText(new DateTime().plusDays(50).toLocalDate().toString()
                + " (Set Selected Date Button)");
        startDate.setText(new DateTime().plusDays(7).toLocalDate().toString()
                + " (Set Start Date Button)");

        weekCalendar = (WeekCalendar) popupView.findViewById(R.id.weekCalendar);
        weekCalendar.setOnDateClickListener(new OnDateClickListener() {
            @Override
            public void onDateClick(DateTime dateTime) {
                Toast.makeText(mContext, "You Selected " + dateTime.toString(), Toast
                        .LENGTH_SHORT).show();
                //DateTime parsedDateTimeUsingFormatter = DateTime.parse(dateTime.toString(), dateTimeFormatter);
                String date = dateTimeFormatter.print(dateTime);
                Log.e(TAG, "..........DATE:::" + date);
                tvSelectedDate.setText(date);
            }

        });



        weekCalendar.setOnWeekChangeListener(new OnWeekChangeListener() {
            @Override
            public void onWeekChange(DateTime firstDayOfTheWeek, boolean forward) {
                Toast.makeText(mContext, "Week changed: " + firstDayOfTheWeek +
                        " Forward: " + forward, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onNextClick(View veiw) {
        weekCalendar.moveToNext();
    }


    public void onPreviousClick(View view) {
        weekCalendar.moveToPrevious();
    }

    public void onResetClick(View view) {
        weekCalendar.reset();

    }
    public void onSelectedDateClick(View view){
        weekCalendar.setSelectedDate(new DateTime().plusDays(50));
    }
    public void onStartDateClick(View view){
        weekCalendar.setStartDate(new DateTime().plusDays(7));
    }
}