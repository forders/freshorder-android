package com.freshorders.freshorder.fragment;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapterrecy.SurveyPendingAdapter;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.model.SurveyCustomField;
import com.freshorders.freshorder.model.SurveyDetailModel;
import com.freshorders.freshorder.utils.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SurveyPendingFragment extends Fragment {

    public static final String TAG = SurveyPendingFragment.class.getSimpleName();

    private View viewFragment;
    private Context mContext;
    private LinearLayout lineHeaderProgress ;
    private LinearLayout llNoDataFound;

    private RecyclerView recyclerview;
    private SurveyPendingAdapter adapter;
    private List<SurveyDetailModel> surveyList;

    JSONObject JsonSurvyObject = null;
    JSONArray JsonAccountArray = null;
    public static DatabaseHandler databaseHandler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        viewFragment = inflater.inflate(R.layout.fragment_pending_survey, container, false);
        mContext = inflater.getContext();
        surveyList = new ArrayList<>();

        recyclerview = (RecyclerView) viewFragment.findViewById(R.id.id_RV_pending_survey);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerview.setLayoutManager(layoutManager);

        adapter = new SurveyPendingAdapter(getActivity(), surveyList,viewFragment);
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        //setting adapter to recyclerView
        recyclerview.setAdapter(adapter);

        databaseHandler = new DatabaseHandler(mContext.getApplicationContext());
        JsonAccountArray = new JSONArray();
        return viewFragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lineHeaderProgress = viewFragment.findViewById(R.id.lineHeaderProgress_home_start);
        llNoDataFound = viewFragment.findViewById(R.id.id_LL_no_data);
        fetchingData();
    }

    private void fetchingData(){
        Cursor cur_count = databaseHandler.getPendingSurvey();
        if(cur_count != null && cur_count.getCount() > 0){
            cur_count.moveToFirst();
            showD();
            while (!cur_count.isAfterLast()){
                SurveyDetailModel currentSurvey = new SurveyDetailModel();
                int id = cur_count.getInt(cur_count.getColumnIndex(DatabaseHandler.KEY_SURVEY_ID));
                currentSurvey.setId(id);
                currentSurvey.setIsMigrated(cur_count.getInt(cur_count.getColumnIndex(DatabaseHandler.KEY_SURVEY_MIGRATION_STATUS)));
                int merchantRowId = cur_count.getInt(cur_count.getColumnIndex(DatabaseHandler.KEY_SURVEY_MERCHANT_PRIMARY_KEY));
                currentSurvey.setMerchantRowId(merchantRowId);
                currentSurvey.setFailedReason(cur_count.getString(cur_count.getColumnIndex(DatabaseHandler.KEY_SURVEY_FAILED_REASON)));

                String strSurveyDetail = cur_count.getString(cur_count.getColumnIndex(DatabaseHandler.KEY_SURVEY_SYNC_DATA));
                try {
                    JsonSurvyObject = new JSONObject(strSurveyDetail);
                    currentSurvey.setSurveyDate(JsonSurvyObject.getString("surveydate"));
                    currentSurvey.setDuserid(JsonSurvyObject.getString("duserid"));
                    currentSurvey.setmUserId(JsonSurvyObject.getString("muserid"));
                    JSONArray data = JsonSurvyObject.getJSONArray("customdata");
                    List<SurveyCustomField> surveyDetail = new ArrayList<>(data.length());
                    for(int i = 0; i < data.length(); i++){
                        SurveyCustomField currentCustomData = new SurveyCustomField();
                        JSONObject obj = data.getJSONObject(i);
                        currentCustomData.setCustmfldid(obj.getString("custmfldid"));
                        currentCustomData.setCustmfldvalue(obj.getString("custmfldvalue"));
                        surveyDetail.add(currentCustomData);
                    }
                    currentSurvey.setSurveyDetail(surveyDetail);
                    Cursor curMerchant = databaseHandler.getMerchantById(merchantRowId);
                    if(curMerchant != null && curMerchant.getCount() > 0){
                        curMerchant.moveToFirst();
                        currentSurvey.setCompanyName(curMerchant.getString(curMerchant.getColumnIndex("companyname")));
                        curMerchant.close();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                surveyList.add(currentSurvey);
                cur_count.moveToNext();
            }
            hideD();
            cur_count.close();
            adapter.notifyDataSetChanged();
        }else {
            llNoDataFound.setVisibility(View.VISIBLE);
            Log.e(TAG,"............................NO Data Found");
        }
    }

    private void showD(){
        if(lineHeaderProgress != null){
            lineHeaderProgress.setVisibility(View.VISIBLE);
        }
    }

    private void hideD(){
        if(lineHeaderProgress != null){
            lineHeaderProgress.setVisibility(View.GONE);
        }
    }

}
