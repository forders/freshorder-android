package com.freshorders.freshorder.fragment;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.ManagerSalesmanAdapter;
import com.freshorders.freshorder.asyntask.GeneralAsyncTask;
import com.freshorders.freshorder.asyntask.SalesmanListAsync;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.model.DateDiffModel;
import com.freshorders.freshorder.model.EmployeeLiveResponse;
import com.freshorders.freshorder.model.HierarchyObjModel;
import com.freshorders.freshorder.model.ManagerSalesmanAdapterModel;
import com.freshorders.freshorder.model.MyItem;
import com.freshorders.freshorder.model.SalesmanListFetchByIdModel;
import com.freshorders.freshorder.popup.PopupOutletStockEntryDatePicker;
import com.freshorders.freshorder.popup.PopupTreeView;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.MobileNet;
import com.freshorders.freshorder.utils.Utils;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import static com.freshorders.freshorder.service.PrefManager.KEY_IS_HIERARCHY_PLACE_FILE_LOADED;
import static com.freshorders.freshorder.service.PrefManager.KEY_MANAGER_TYPE;
import static com.freshorders.freshorder.service.PrefManager.KEY_PATH_HIERARCHY_PLACE_FILE;

public class ManagerLiveVisitFragment extends Fragment implements OnMapReadyCallback,
        ClusterManager.OnClusterClickListener<MyItem>,
        ClusterManager.OnClusterInfoWindowClickListener<MyItem>,
        ClusterManager.OnClusterItemClickListener<MyItem>,
        ClusterManager.OnClusterItemInfoWindowClickListener<MyItem>{

    public static final String TAG = ManagerLiveVisitFragment.class.getSimpleName();

    double preDistance;

    private String selectedSalesmanId;
    ////////////private String selectedDate;
    private String distributorId;

    SupportMapFragment mMapFragment;
    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationClient;
    private double longitude;
    private double latitude;
    private final int DEFAULT_ZOOM = 8;

    private ClusterManager<MyItem> mClusterManager;
    private Cluster<MyItem> clickedCluster;
    private MyItem clickedClusterItem;

    private View viewFragment;
    private Context mContext;
    private LinearLayout lineHeaderProgress ;
    private LinearLayout linearProgress;

    JSONObject JsonSurvyObject = null;
    JSONArray JsonAccountArray = null;
    public DatabaseHandler databaseHandler;

    EditText txtDate;
    String strSelectedDate = "";
    androidx.appcompat.widget.AppCompatSpinner spinSalesman;
    RelativeLayout llFilter;
    ImageView ivFilter;



    LinkedHashMap<Integer,String> linkedHashMap = new LinkedHashMap<>();
    List<String> listOfSalesman = new ArrayList<>();
    List<Integer> listSalesmanId = new ArrayList<>();



    private LinearLayout llMap;
    private LinearLayout llSalesman;

    private RecyclerView recyclerview;
    ManagerSalesmanAdapter rvAdapter;
    private List<ManagerSalesmanAdapterModel> listItems;

    private TextView tvTraveled;



    private String ManagerType = "";
    private EditText eTxtPlace;
    private String hierarchyPlace = "";
    private HierarchyObjModel selectedItem ;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        viewFragment = inflater.inflate(R.layout.fragment_manager_live_visit, container, false);
        mContext = inflater.getContext();
        listItems = new ArrayList<>();
        databaseHandler = new DatabaseHandler(mContext.getApplicationContext());//DatabaseHandler.getInstance(mContext.getApplicationContext());// new DatabaseHandler(mContext.getApplicationContext());
        JsonAccountArray = new JSONArray();
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(mContext);
        setUpMap();///////////////
        return viewFragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lineHeaderProgress = viewFragment.findViewById(R.id.lineHeaderProgress_manager_live_visit);
        linearProgress = viewFragment.findViewById(R.id.lineHeaderProgress_manager_live_inner);
        txtDate = viewFragment.findViewById(R.id.id_manager_live_visit_select_date);
        tvTraveled = viewFragment.findViewById(R.id.manager_live_TV_travel_distance);

        ManagerType = new PrefManager(mContext).getStringDataByKey(KEY_MANAGER_TYPE);
        updateUserId();////////////

        llMap = viewFragment.findViewById(R.id.ll_manager_live_visit_bottom);


        ///Default date setting
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        txtDate.setText(formattedDate);
        strSelectedDate = formattedDate;

        //////////fetchingData();

        Button btnMap = viewFragment.findViewById(R.id.id_manager_live_visit_btn_view_map);

        btnMap.setOnClickListener(btnMapClickListener);

        txtDate.setOnClickListener(selectDate);

        eTxtPlace = viewFragment.findViewById(R.id.id_M_live_track_eTxt_select_place);
        if(ManagerType.equalsIgnoreCase("U")){
            eTxtPlace.setVisibility(View.VISIBLE);
            eTxtPlace.setOnClickListener(placeClick);
        }

    }

    @Override
    public void onStart() {
        super.onStart();

        ManagerType = new PrefManager(mContext).getStringDataByKey(KEY_MANAGER_TYPE);
        if(ManagerType.equalsIgnoreCase("U")) {
            if (listOfSalesman != null && listOfSalesman.size() > 0) {
                //returning from backstack, data is fine, do nothing
                Log.e(TAG, ".............onStart..data is fine");
            } else {
                //newly created, compute data
                Log.e(TAG, ".............onStart..newly created, compute data");
                clear();
                fetchingData();
            }
        }else {
            fetchingData();
        }
    }

    private View.OnClickListener placeClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            //DisplayMetrics displayMetrics = new DisplayMetrics();
            //mContext.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            ////hideKeyBoard();
            DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();

            boolean isFileLoaded = new PrefManager(mContext).getBooleanDataByKey(KEY_IS_HIERARCHY_PLACE_FILE_LOADED);
            if(isFileLoaded){
                String filePath = new PrefManager(mContext).getStringDataByKey(KEY_PATH_HIERARCHY_PLACE_FILE);
                PopupTreeView popup = new PopupTreeView(viewFragment,
                        mContext, filePath, eTxtPlace,
                        displayMetrics, true,  new PopupTreeView.OnDismissListener() {
                    @Override
                    public void onDismiss(HierarchyObjModel selectedItems) {
                        if(selectedItems == null){
                            showAlertDialogToast(getResources().getString(R.string.not_retrieve_data));
                        }else {
                            selectedItem = selectedItems;
                        }
                        hierarchyPlace = eTxtPlace.getText().toString().trim();
                        if(!hierarchyPlace.isEmpty()){
                            clear();
                            fetchingData();
                        }
                    }
                });
                ////popup.showPopup();
                popup.showPopupWindow();
            }else{
                showAlertDialogToast(getResources().getString(R.string.data_not_load_when_login));
            }
        }
    };

    private void fetchingData(){
        if(MobileNet.isNetAvail(mContext)) {
            if(ManagerType.equalsIgnoreCase("U")){
                Log.e(TAG, "..Type...." + ManagerType);
                hierarchyPlace = eTxtPlace.getText().toString().trim();
                if(!hierarchyPlace.isEmpty()){
                    //////////
                    clearCluster();
                    show();
                    clear();
                    getSalesmanList();
                }
            }else {
                clearCluster();
                show();
                getSalesmanPositionList();
            }
        }else {
            showAlertDialogToast(mContext.getResources().getString(R.string.no_active_network_avail));
        }
    }


    private void clearCluster(){
        if(mMap != null) {
            mMap.clear();
            mClusterManager.clearItems();
        }
    }

    View.OnClickListener btnMapClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if(MobileNet.isNetAvail(mContext)) {
                tvTraveled.setVisibility(View.GONE);
                tvTraveled.setText("");
                mMap.setMaxZoomPreference(mMap.getMaxZoomLevel());
                mMap.setMinZoomPreference(mMap.getMinZoomLevel());
                show();
                clearCluster();
                getSalesmanPositionList();
            }else {
                showAlertDialogToast(mContext.getResources().getString(R.string.no_active_network_avail));
            }

        }
    };


    View.OnClickListener selectDate = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            PopupOutletStockEntryDatePicker datePopup = new
                    PopupOutletStockEntryDatePicker(
                    mContext, txtDate,
                    new PopupOutletStockEntryDatePicker.PopupListener() {
                        @Override
                        public void onPopupClosed() {
                            strSelectedDate = txtDate.getText().toString().trim();
                        }
                    });
            datePopup.showPopup();
        }
    };




    private void setUpMap() {
        mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mMapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (mMap != null) {
            return;
        }
        mMap = googleMap;
        initializeMap();
        Log.e(TAG,"Map ready...........................................................");
    }

    @SuppressLint("MissingPermission")
    private void initializeMap(){
        try {
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.getUiSettings().setCompassEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.getUiSettings().setRotateGesturesEnabled(true);
            mMap.getUiSettings().setScrollGesturesEnabled(true);
            mMap.getUiSettings().setTiltGesturesEnabled(true);
            mMap.getUiSettings().setZoomGesturesEnabled(true);
            mMap.setMyLocationEnabled(true);

            initializeCluster();

        } catch (Exception e) {
            //mMapFrame.setVisibility(View.GONE);
            //Utils.displayToast("Your device doesn't support Google Map", Map.this);
        }
    }

    private void initializeCluster(){

        mClusterManager = new ClusterManager<>(mContext, mMap);

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(37.779977,-122.413742), 10));

        mMap.setOnCameraIdleListener(mClusterManager);

        mClusterManager.setRenderer(new ManagerLiveVisitFragment.MyClusterRenderer(mContext, mMap,
                mClusterManager));


        mMap.setOnMarkerClickListener(mClusterManager);

        mMap.setInfoWindowAdapter(mClusterManager.getMarkerManager());

        mMap.setOnInfoWindowClickListener(mClusterManager); //added
        mClusterManager.setOnClusterItemInfoWindowClickListener(this); //added

        mClusterManager
                .setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<MyItem>() {
                    @Override
                    public boolean onClusterItemClick(MyItem item) {
                        clickedClusterItem = item;
                        return false;
                    }
                });

        mClusterManager
                .setOnClusterClickListener(new ClusterManager.OnClusterClickListener<MyItem>() {
                    @Override
                    public boolean onClusterClick(Cluster<MyItem> cluster) {
                        clickedCluster = cluster;
                        return false;
                    }
                });




        /////addItems();

        mClusterManager.getMarkerCollection().setOnInfoWindowAdapter(
                new ManagerLiveVisitFragment.MyCustomAdapterForItems());
    }

    private void addCursorItems(){


        double firstLat = 13.067439, firstLan = 80.237617;
        if(listItems != null && listItems.size() > 0){
            Log.e(TAG,"..........................................................`````````````size"+listItems.size());
            for(int i = 0; i < listItems.size(); i++){
                ManagerSalesmanAdapterModel currentItems = listItems.get(i);
                String strLat = currentItems.getGeolat();
                String strLan = currentItems.getGeolong();
                String name = currentItems.getStorename();

                double lat = Double.parseDouble(strLat);
                double lng = Double.parseDouble(strLan);
                if(i == 0){
                    firstLat = lat;
                    firstLan = lng;
                }
                MyItem offsetItem = new MyItem(lat, lng, "Merchant " + name, Constants.EMPTY);
                mClusterManager.addItem(offsetItem);

            }
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(firstLat,firstLan), DEFAULT_ZOOM));
            mClusterManager.cluster();
        }else {
            showAlertDialogToast("No Data From Server");
        }
    }

    private void addItems() {

        double latitude = 37.779977;
        double longitude = -122.413742;
        for (int i = 0; i < 10; i++) {
            double offset = i / 60d;

            double lat = latitude + offset;
            double lng = longitude + offset;
            MyItem offsetItem = new MyItem(lat, lng, "title " + i+1, "snippet " + i+1);
            mClusterManager.addItem(offsetItem);

        }

    }

    class MyClusterRenderer extends DefaultClusterRenderer<MyItem> {

        public MyClusterRenderer(Context context, GoogleMap map,
                                 ClusterManager<MyItem> clusterManager) {
            super(context, map, clusterManager);
        }

        @Override
        protected void onBeforeClusterItemRendered(MyItem item,
                                                   MarkerOptions markerOptions) {

            BitmapDescriptor markerDescriptor;
            switch (item.getColor()){

                case 1:
                    markerDescriptor = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN);
                    break;
                case 2:
                    markerDescriptor = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED);
                    break;
                default:
                    markerDescriptor = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW);
                    break;
            }
            //BitmapDescriptor markerDescriptor = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA);
            markerOptions.icon(markerDescriptor);
            super.onBeforeClusterItemRendered(item, markerOptions);
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            //float currentZoom = mMapCopy.getCameraPosition().zoom;
            //float currentMaxZoom = mMapCopy.getMaxZoomLevel();
            return cluster.getSize() > 1;
        }

        @Override
        protected void onClusterItemRendered(MyItem clusterItem, Marker marker) {
            super.onClusterItemRendered(clusterItem, marker);
        }

        /*@Override
        protected void onBeforeClusterRendered(Cluster<MyItem> cluster, MarkerOptions markerOptions){

        } */

    }

    public class MyCustomAdapterForItems implements GoogleMap.InfoWindowAdapter {

        private final View myContentsView;

        MyCustomAdapterForItems() {
            myContentsView = getActivity().getLayoutInflater().inflate(
                    R.layout.info_window, null);
        }

        @Override
        public View getInfoWindow(Marker marker) {
            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {

            TextView tvTitle = ((TextView) myContentsView
                    .findViewById(R.id.txtTitle));
            TextView tvSnippet = ((TextView) myContentsView
                    .findViewById(R.id.txtSnippet));

            tvTitle.setText(clickedClusterItem.getTitle());
            tvSnippet.setText(clickedClusterItem.getSnippet());

            return myContentsView;
        }
    }



    @Override
    public boolean onClusterClick(Cluster<MyItem> cluster) {
        return false;
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<MyItem> cluster) {

    }

    @Override
    public boolean onClusterItemClick(MyItem myItem) {
        return false;
    }

    @Override
    public void onClusterItemInfoWindowClick(MyItem myItem) {
        Log.e(TAG,"...................onClusterItemInfoWindowClick....." + myItem.getsUserId());
        try {
            clearCluster();
            getSingleSalesmanPosition(myItem.getsUserId());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void addMarker(double lat, double lon, float markerColor, String logtime, boolean isStart){

        String dateTime = dateStringToString(logtime);
        // Creating a marker
        MarkerOptions markerOptions = new MarkerOptions();

        // Setting the position for the marker
        LatLng latLng = new LatLng(lat ,lon);
        markerOptions.position(latLng);
        //markerOptions.title(latLng.latitude + " : " + latLng.longitude);
        if(isStart){
            markerOptions.title(" Start Time : " + dateTime);
        }else {
            markerOptions.title(" End Time : " + dateTime);
        }
        BitmapDescriptor markerDescriptor = BitmapDescriptorFactory.defaultMarker(markerColor);
        markerOptions.icon(markerDescriptor);
        mMap.addMarker(markerOptions);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f));
    }

    private void getSingleSalesmanPosition(String userId){

        if(mMap != null){
            mMap.setMaxZoomPreference(17.0f);
        }

        if(!isNetAvail(mContext)) {
            showAlertDialogToast(mContext.getResources().getString(R.string.no_active_network_avail));
            return;
        }

        updateUserId();

        JSONObject model;
        String formattedDate = "";
        try {
            if(strSelectedDate != null && !strSelectedDate.isEmpty()) {
                Log.e("Selected",".........................................Date:" + strSelectedDate);
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
                SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                Date newDate = format.parse(strSelectedDate);
                Log.e("Date",".........................................." + newDate);
                formattedDate = format1.format(newDate);
            }else {
                showAlertDialogToast("Some think wrong can't get date in App");
                return;
            }

            model = new JSONObject();
            model.put("duserid", Constants.DUSER_ID);
            model.put("date", formattedDate);
            model.put("suserid", userId);

        }catch (Exception e){
            e.printStackTrace();
            showAlertDialogToast("Some Think Wrong In App");
            return;
        }

        innerShow();

        JsonServiceHandler jsonServiceHandler = new JsonServiceHandler(Utils.salesmanLive,model.toString() , mContext);
        GeneralAsyncTask obj = new GeneralAsyncTask(jsonServiceHandler,
                new GeneralAsyncTask.CompleteListener() {
                    @Override
                    public void onSuccess(JSONObject result) {
                        /////hide();
                        try {
                            if (result != null) {
                                if (result.has("data")) {
                                    try {
                                        PolylineOptions options = new PolylineOptions().width(5).color(Color.BLUE).geodesic(true);
                                        JSONArray jsonArray = result.getJSONArray("data");
                                        if(jsonArray.length() > 0){
                                            double totalDistance = 0d;
                                            JSONObject jsonResult = jsonArray.getJSONObject(0);
                                            if(jsonResult != null && jsonResult.length() > 0){
                                                String strUserId = jsonResult.getString("userid");
                                                String strName = jsonResult.getString("fullname");
                                                String strMobile = jsonResult.getString("mobileno");

                                                if(jsonResult.has("traceroute")){
                                                    JSONArray jsonArray1 = jsonResult.getJSONArray("traceroute");
                                                    if(jsonArray1 != null && jsonArray1.length() > 0){
                                                        for(int i = 0; i < jsonArray1.length(); i++){
                                                            JSONObject obj = jsonArray1.getJSONObject(i);
                                                            double lat = obj.getDouble("geolat");
                                                            double lon = obj.getDouble("geolong");
                                                            double distance = obj.getDouble("distance");
                                                            Log.e(TAG,"......................CurrentDistance:: " + i +" :: " + distance);
                                                            totalDistance = totalDistance + distance;

                                                            Log.e(TAG,"............lat::lan" + lat + lon + ".........:::" + distance);
                                                            if( i== 0 || i == jsonArray1.length() -1){
                                                                LatLng mPosition = new LatLng(lat, lon);
                                                                options.add(mPosition);
                                                                preDistance = distance;
                                                                String logtime = obj.getString("logtime");

                                                                SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                                                                String dateTime = ft.format(new java.sql.Date(System.currentTimeMillis()));
                                                                Date current = ft.parse(dateTime);
                                                                Date liveTime = getDateFromApp(logtime);
                                                                float color = BitmapDescriptorFactory.HUE_RED;
                                                                if(liveTime != null) {
                                                                    DateDiffModel model1 = printDifference(current, liveTime);
                                                                    if (model1.getDay() > 0 || model1.getHours() > 0) {
                                                                        color = BitmapDescriptorFactory.HUE_RED;
                                                                    } else {
                                                                        long min = model1.getMinutes();
                                                                        if (min <= 30) {
                                                                            color = BitmapDescriptorFactory.HUE_GREEN;
                                                                        } else {
                                                                            color = BitmapDescriptorFactory.HUE_RED;
                                                                        }
                                                                    }
                                                                }else{

                                                                }

                                                                if( i == 0){
                                                                    addMarker(lat,lon, color, logtime, true);
                                                                }else {
                                                                    addMarker(lat,lon, color, logtime, false);
                                                                }
                                                            }else {
                                                                if(preDistance + distance >= 0.035){
                                                                    double rLat = BigDecimal.valueOf(lat)
                                                                            .setScale(1, RoundingMode.HALF_UP)
                                                                            .doubleValue();
                                                                    double rLon = BigDecimal.valueOf(lon)
                                                                            .setScale(1, RoundingMode.HALF_UP)
                                                                            .doubleValue();
                                                                    ////////LatLng mPosition = new LatLng(rLat, rLon);
                                                                    //LatLng mPosition = new LatLng(lat, lon);
                                                                    //options.add(mPosition);
                                                                    preDistance = 0;

                                                                }else{
                                                                    preDistance = preDistance + distance;
                                                                }
                                                                LatLng mPosition = new LatLng(lat, lon);
                                                                options.add(mPosition);
                                                            }
                                                        }
                                                        innerHide();
                                                        mMap.addPolyline(options);//////////////////////////////
                                                        setTraveledDistance(totalDistance);

                                                    }else {
                                                        showAlertDialogToast("No positions recorded yet");
                                                    }
                                                }else {
                                                    showAlertDialogToast("No positions recorded yet");
                                                }

                                            }else {
                                                showAlertDialogToast("No positions recorded yet");
                                            }
                                        }else {
                                            showAlertDialogToast("No positions recorded yet");
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        innerHide();
                                        showAlertDialogToast("Internal problem occur:: " + e.getMessage());
                                    }
                                }else {
                                    showAlertDialogToast("No data from cloud server");
                                }
                            }else {
                                showAlertDialogToast("No data from cloud server");
                            }
                            innerHide();
                        } catch (Exception e) {
                            e.printStackTrace();
                            showAlertDialogToast("Internal problem occur:: " + e.getMessage());
                        }finally {
                            innerHide();
                        }
                    }
                    @Override
                    public void onFailed(String msg) {
                        innerHide();
                        showAlertDialogToast(msg);
                    }

                    @Override
                    public void onException(String msg) {
                        innerHide();
                        showAlertDialogToast(msg);
                    }

                });
        obj.execute();
    }

    private void setTraveledDistance(double totalDistance) {
        if(tvTraveled != null){
            double distance = BigDecimal.valueOf(totalDistance)
                    .setScale(2, RoundingMode.HALF_UP)
                    .doubleValue();
            String strDistance = String.valueOf(distance);
            //String temp = tvTraveled.getText().toString();
            String temp = Objects.requireNonNull(getContext()).getString(R.string.travelled);
            temp = temp + " " + strDistance + " KM(s)";
            tvTraveled.setText(temp);
            tvTraveled.setVisibility(View.VISIBLE);
        }
    }


    private void getSalesmanPositionList(){

        String urlParams = "";

        updateUserId();///////////////////


        final JSONObject model;
        String formattedDate = "";
        try {
            if(strSelectedDate != null && !strSelectedDate.isEmpty()) {
                Log.e("Selected",".........................................Date:" + strSelectedDate);
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
                SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                Date newDate = format.parse(strSelectedDate);
                Log.e("Date",".........................................." + newDate);
                formattedDate = format1.format(newDate);
            }else {
                hide();
                showAlertDialogToast("Some think wrong can't get date in App");
                return;
            }

            if(ManagerType.equalsIgnoreCase("U")) {
                if(hierarchyPlace == null || hierarchyPlace.isEmpty()){
                    hide();
                    showAlertDialogToast(mContext.getResources().getString(R.string.should_select_place));
                    return;
                }
                if (listOfSalesman != null && listOfSalesman.size() > 0) {
                    model = new JSONObject();
                    model.put("duserid", Integer.parseInt(Constants.DUSER_ID));
                    model.put("date", formattedDate);
                    //////////
                    JSONObject inObj = new JSONObject();
                    inObj.put("inq",new JSONArray(listSalesmanId.toArray()));
                    model.put("suserid", inObj);
                    urlParams = model.toString();
                    Log.e(TAG,".........Type U params..." + model.toString());
                }else {
                    hide();
                    showAlertDialogToast(mContext.getResources().getString(R.string.re_select_place));
                    return;
                }
            }else {

                model = new JSONObject();
                model.put("duserid", Constants.DUSER_ID);
                model.put("date", formattedDate);
                urlParams = model.toString();
            }

        }catch (Exception e){
            hide();
            e.printStackTrace();
            showAlertDialogToast("Some Think Wrong In App " + e.getMessage());
            return;
        }

        final List<EmployeeLiveResponse> liveData = new ArrayList<>();

        JsonServiceHandler jsonServiceHandler = new JsonServiceHandler(Utils.salesmanLive,urlParams , mContext);
        GeneralAsyncTask obj = new GeneralAsyncTask(jsonServiceHandler,
                new GeneralAsyncTask.CompleteListener() {
                    @Override
                    public void onSuccess(JSONObject result) {
                        /////hide();
                        try {
                            if (result != null) {
                                if (result.has("data")) {
                                    try {

                                        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                                        String dateTime = ft.format(new java.sql.Date(System.currentTimeMillis()));

                                        JSONArray jsonArray = result.getJSONArray("data");
                                        for (int i = 0; i < jsonArray.length(); i++) {

                                            int color = 0;
                                            Date liveTime ;//= ft.parse(time);

                                            JSONObject object = jsonArray.getJSONObject(i);
                                            Log.e("current","............:: "+ object);
                                            String strUserId = object.getString("userid");
                                            String strName = object.getString("fullname");
                                            String strMobile = object.getString("mobileno");

                                            /*JSONArray inArr = object.getJSONArray("traceroute");
                                            if(inArr.length() <= 0 ){
                                                continue;
                                            } */
                                            JSONObject objLocation;
                                            JSONObject objDayPlane;
                                            double lat = 0;
                                            double lon = 0;
                                            String time = "";

                                            if(object.has("livelocation") || object.has("dayplan")) {
                                                if(object.has("livelocation") && object.has("dayplan")){
                                                    objLocation = object.getJSONObject("livelocation");
                                                    objDayPlane = object.getJSONObject("dayplan");
                                                    if(objLocation.length() > 0 && objDayPlane.length()>0){
                                                        if(objDayPlane.getString("worktype").equals("Leave")){
                                                            lat = objDayPlane.getDouble("geolat");
                                                            lon = objDayPlane.getDouble("geolong");
                                                            time = objDayPlane.getString("updateddt");
                                                            color = 3;
                                                            liveTime = getDateFromCloud(time);
                                                        }else {
                                                            lat = objLocation.getDouble("geolat");
                                                            lon = objLocation.getDouble("geolong");
                                                            time = objLocation.getString("logtime");
                                                            liveTime = getDateFromApp(time);
                                                        }
                                                    }else {
                                                        if(objDayPlane.length()>0 && objDayPlane.getString("worktype").equals("Leave")){
                                                            lat = objDayPlane.getDouble("geolat");
                                                            lon = objDayPlane.getDouble("geolong");
                                                            time = objDayPlane.getString("updateddt");
                                                            color = 3;
                                                            liveTime = getDateFromCloud(time);
                                                        }else if(objLocation.length() > 0){
                                                            lat = objLocation.getDouble("geolat");
                                                            lon = objLocation.getDouble("geolong");
                                                            time = objLocation.getString("logtime");
                                                            liveTime = getDateFromApp(time);
                                                        }else {
                                                            continue;
                                                        }
                                                    }
                                                }else if(object.has("livelocation")){
                                                    objLocation = object.getJSONObject("livelocation");
                                                    if(objLocation.length()>0 ){
                                                        lat = objLocation.getDouble("geolat");
                                                        lon = objLocation.getDouble("geolong");
                                                        time = objLocation.getString("logtime");
                                                        liveTime = getDateFromApp(time);
                                                    }else {
                                                        continue;
                                                    }
                                                } else if(object.has("dayplan")){
                                                    objDayPlane = object.getJSONObject("dayplan");
                                                    if(objDayPlane.length()>0 && objDayPlane.getString("worktype").equals("Leave")){
                                                        lat = objDayPlane.getDouble("geolat");
                                                        lon = objDayPlane.getDouble("geolong");
                                                        time = objDayPlane.getString("updateddt");
                                                        color = 3;
                                                        liveTime = getDateFromCloud(time);
                                                    }else {
                                                        continue;
                                                    }
                                                }else {
                                                    continue;
                                                }
                                            }else {
                                                continue;
                                            }

                                            if(liveTime == null || dateTime == null){
                                                continue;
                                            }

                                            /*JSONObject inObj = object.getJSONObject("livelocation");
                                            double lat = inObj.getDouble("geolat");
                                            double lon = inObj.getDouble("geolong");
                                            String time = inObj.getString("logtime");  */
                                            EmployeeLiveResponse live = new EmployeeLiveResponse(strUserId,
                                                    strName, strMobile, lat, lon, time);
                                            liveData.add(live);

                                            Date current = ft.parse(dateTime);


                                            /*

                                            DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
                                            DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyy", Locale.ENGLISH);
                                            LocalDate date = LocalDate.parse("2018-04-10T04:00:00.000Z", inputFormatter);
                                            String formattedDate = outputFormatter.format(date);  */



                                            Log.e("current",".............Time::"+current.toString());
                                            Log.e("live",".............Time::"+liveTime.toString());
                                            //in milliseconds
                                            long diff = current.getTime() - liveTime.getTime();
                                            if( color != 3) {
                                                DateDiffModel model1 = printDifference(current, liveTime);
                                                if(model1.getDay() > 0 || model1.getHours() > 0){
                                                    color = 2;
                                                }else {
                                                    long min = model1.getMinutes();
                                                    if(min <= 30){
                                                        color = 1;
                                                    }else {
                                                        color = 2;
                                                    }
                                                }
                                            }

                                            /////////////////////////////////////////
                                            DateFormat dateFormat = new SimpleDateFormat("hh.mm aa",Locale.getDefault());
                                            String dateString = dateFormat.format(liveTime).toString();

                                            //////////////////////////////////////////////

                                            MyItem offsetItem = new MyItem(lat, lon,
                                                    " " + strName + " @ " + dateString, Constants.EMPTY, color, strUserId);
                                            mClusterManager.addItem(offsetItem);
                                        }
                                        /////////////////////// newly added --- to check items is loaded or not
                                        Collection<MyItem> items = mClusterManager.getAlgorithm().getItems();
                                        if(items != null && items.size() > 0){
                                            Log.e(TAG,"Location items loaded successfully.........");
                                        }else {
                                            hide();
                                            showAlertDialogToast(mContext.getResources().getString(R.string.no_location_found));
                                            return;
                                        }
                                        double firstLat = 13.067439, firstLan = 80.237617;
                                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(firstLat,firstLan), DEFAULT_ZOOM));
                                        mClusterManager.cluster();

                                    } catch (Exception e) {
                                        hide();
                                        e.printStackTrace();
                                        showAlertDialogToast("Unknown interrupt occur");
                                    }
                                } else {
                                    hide();
                                    showAlertDialogToast("No one person start yet or no data found from selected date");
                                }
                            } else {
                                hide();
                                showAlertDialogToast("No response from server");
                            }
                        }catch (Exception e){
                            hide();
                            e.printStackTrace();
                            showAlertDialogToast(mContext.getResources().getString(R.string.technical_fault) + e.getMessage());
                        }finally {
                            hide();
                        }
                    }

                    @Override
                    public void onFailed(String msg) {
                        hide();
                        showAlertDialogToast(msg);
                    }

                    @Override
                    public void onException(String msg) {
                        hide();
                        showAlertDialogToast(msg);
                    }
                });
        obj.execute();
    }

    private String dateStringToString(String date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        try{
            Date newDate = getDateFromApp(date);
            if(newDate != null){
                sdf.applyPattern("dd MMM yyyy hh:mm");
                return sdf.format(newDate);
            }else {
                return "";
            }
        }catch (Exception e){
            return "";
        }
    }

    private Date getDateFromCloud(String date){
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
        try {
            if (date != null && !date.isEmpty()) {
                return input.parse(date);
            }
        }catch (ParseException e){
            e.printStackTrace();
            return null;
        }
        return null;
    }

    private Date getDateFromApp(String date){
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        try {
            if (date != null && !date.isEmpty()) {
                return input.parse(date);
            }
        }catch (ParseException e){
            e.printStackTrace();
            return null;
        }
        return null;
    }


    private void show(){
        if(lineHeaderProgress != null) {
            lineHeaderProgress.setVisibility(View.VISIBLE);
        }
    }

    private void hide(){
        if(lineHeaderProgress != null) {
            if(lineHeaderProgress.getVisibility() == View.VISIBLE) {
                lineHeaderProgress.setVisibility(View.GONE);
            }
        }
    }

    /*
    private boolean checkPlayServices(){
        int resultCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(mContext);
        if(resultCode != ConnectionResult.SUCCESS){
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)){
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }else {
                Toast.makeText(mContext,
                        "This device is not supported", Toast.LENGTH_LONG)
                        .show();
            }
            return false;
        }
        return true;
    }  */


    public void showAlertDialogToast(String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    private void innerShow(){
        if(linearProgress != null){
            int visible = linearProgress.getVisibility();
            if( visible == View.INVISIBLE || visible == View.GONE){
                linearProgress.setVisibility(View.VISIBLE);
            }
        }
    }

    private void innerHide(){
        if(linearProgress != null){
            int visible = linearProgress.getVisibility();
            if( visible == View.VISIBLE){
                linearProgress.setVisibility(View.GONE);
            }
        }
    }

    public DateDiffModel printDifference(Date todayDate, Date oldDate) {

        DateDiffModel model = new DateDiffModel();

        //milliseconds
        long different = todayDate.getTime() - oldDate.getTime();

        System.out.println("todayDate : " + todayDate);
        System.out.println("oldDate : "+ oldDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        System.out.printf(
                "%d days, %d hours, %d minutes, %d seconds%n",
                elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);

        model.setDay(elapsedDays);
        model.setHours(elapsedHours);
        model.setMinutes(elapsedMinutes);
        model.setSeconds(elapsedSeconds);
        return model;
    }

    @Override
    public void onResume() {
        super.onResume();
        /////////updateUserId();
    }

    private void updateUserId(){
        if (Constants.DUSER_ID == null || Constants.DUSER_ID.isEmpty()) {
            if(databaseHandler == null) {
                databaseHandler =  new DatabaseHandler(getContext());//DatabaseHandler.getInstance(mContext.getApplicationContext());//new DatabaseHandler(getContext());
            }
            Cursor curs;
            curs = databaseHandler.getDetails();
            if(curs != null && curs.getCount() > 0) {
                curs.moveToFirst();
                if(ManagerType.equalsIgnoreCase("U")){
                    Constants.DUSER_ID = curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_clientdealid));
                }else {
                    Constants.DUSER_ID = curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_id));
                }
                curs.close();
                if(Constants.DUSER_ID != null && !Constants.DUSER_ID.isEmpty()){
                    Log.e("Constants.DUSER_ID", Constants.DUSER_ID);
                }else {
                    showAlertDialogToast(getResources().getString(R.string.should_re_login));
                }
            }else {
                showAlertDialogToast(getResources().getString(R.string.need_to_re_open));
            }
        }
    }

    private boolean isNetAvail(Context context){
        try {
            boolean isConnected,isMobile = false;
            ConnectivityManager cm =
                    (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = null;
            if (cm != null) {
                activeNetwork = cm.getActiveNetworkInfo();
            }else {
                return false;
            }

            isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
            if (activeNetwork != null) {
                isMobile = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE;
            }
            if(isConnected && isMobile){
                return true;
            }
            return false;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }


    private void getSalesmanList(){

        updateUserId();//////////////
        String json = "";
        if(ManagerType.equalsIgnoreCase("U")){
            try {
                /*
                JSONObject obj = new JSONObject();
                obj.put("duserid", Constants.DUSER_ID);
                JSONArray array = new JSONArray(hierarchyPlace);
                obj.put("hierarchytext" , array);
                Log.e(TAG, "..........ManagerType U............" + obj.toString()); */

                JSONStringer jsonStringer = new JSONStringer();
                jsonStringer.object()
                        .key("duserid").value(Constants.DUSER_ID)
                        .key("hierarchytext")
                        .array()
                        .value(hierarchyPlace)
                        .endArray()
                        .endObject();
                json = jsonStringer.toString();
            }catch (Exception e){
                hide();
                e.printStackTrace();
                showAlertDialogToast(mContext.getResources().getString(R.string.technical_fault) + e.getMessage());
                return;
            }
        }else {
            SalesmanListFetchByIdModel model = new SalesmanListFetchByIdModel();
            model.setUserid(Constants.DUSER_ID);
            Gson gson = new Gson();
            json = gson.toJson(model);
        }
        Log.e(TAG, "............................."+json);
        JsonServiceHandler jsonServiceHandler;
        if(ManagerType.equalsIgnoreCase("U")){
            jsonServiceHandler = new JsonServiceHandler(Utils.strGetSalesManList, json, mContext);
        }else {
            jsonServiceHandler = new JsonServiceHandler(Utils.strGetMerchantList, json, mContext);
        }
        SalesmanListAsync obj = new SalesmanListAsync(jsonServiceHandler,
                new SalesmanListAsync.SalesmanListCompleteListener() {
                    @Override
                    public void onSuccess(JSONObject result) {
                        hide();
                        if(result.has("data")) {
                            try {
                                JSONArray jsonArray = result.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    //String strUserId = object.getString("userid");
                                    int strUserId = object.getInt("userid");
                                    String strName = object.getString("fullname");
                                    listOfSalesman.add(strName);
                                    listSalesmanId.add(strUserId);
                                    linkedHashMap.put(strUserId, strName);
                                }
                                /////spinnerUpdate(listOfSalesman);
                                /////////////////
                                show();
                                getSalesmanPositionList();

                            }catch (Exception e){
                                e.printStackTrace();
                                showAlertDialogToast(mContext.getResources().getString(R.string.technical_fault) + " " +e.getMessage());
                            }
                        }
                    }

                    @Override
                    public void onFailed(String msg) {
                        hide();
                        showAlertDialogToast(msg);
                    }

                    @Override
                    public void onException(String msg) {
                        hide();
                        showAlertDialogToast(msg);
                    }
                });
        obj.execute();
    }


    private void clear(){
        if(listOfSalesman != null){
            listOfSalesman.clear();
        }else {
            listOfSalesman = new ArrayList<>();
        }
        if(listSalesmanId != null){
            listSalesmanId.clear();
        }else {
            listSalesmanId = new ArrayList<>();
        }
        if(linkedHashMap != null){
            linkedHashMap.clear();
        }else {
            linkedHashMap = new LinkedHashMap<>();
        }
    }

}
