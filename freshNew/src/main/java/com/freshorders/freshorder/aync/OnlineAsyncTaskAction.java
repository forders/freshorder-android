package com.freshorders.freshorder.aync;


import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.domain.MerchantOrderDetailDomain;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.freshorders.freshorder.utils.Constants.COMPANY_STOCK;
import static com.freshorders.freshorder.utils.Constants.CURRENT_STOCK_NOT_AVAIL;
import static com.freshorders.freshorder.utils.Constants.DISTRIBUTOR_STOCK;
import static com.freshorders.freshorder.utils.Constants.EMPTY;
import static com.freshorders.freshorder.utils.Constants.SERVER_SEND_NO_STOCK_RESULT;
import static com.freshorders.freshorder.utils.Constants.STR_CURRENT_STOCK;

// By Kumaravel
public class OnlineAsyncTaskAction {

    public interface OnlineStockCompletedListener{
        void onStockFetchFinished(boolean result, String errorMSG);
    }

    public interface DistributorNameCompletedListener{
        void onDistributorNameFetchFinished(String distributorname,boolean result, String errorMSG);
    }

    private Context mContext;
    //private ArrayList<MerchantOrderDetailDomain> merchantOrderList;

    private static final String TAG = OnlineAsyncTaskAction.class.getSimpleName();

    private static String beatType = "";

    public OnlineAsyncTaskAction(Context mContext) {
        this.mContext = mContext;
        //Kumaravel 13-07-2019
        if(Constants.distributorname == null || Constants.distributorname.isEmpty()){
            Constants.distributorname = new PrefManager(mContext).getStringDataByKey(PrefManager.KEY_DISTRIBUTOR_NAME);
        }
    }

    private class GetDistributorNameAsyncTask extends AsyncTask<Void, Void, String>{


        Context mContext;
        DatabaseHandler databaseHandler;
        String duserid;
        OnlineAsyncTaskAction.DistributorNameCompletedListener listener;

        public GetDistributorNameAsyncTask(Context mContext,
                                           String duserid,
                                           OnlineAsyncTaskAction.DistributorNameCompletedListener listener) throws Exception {
            this.mContext = mContext;
            databaseHandler = new DatabaseHandler(mContext);
            this.duserid = duserid;
            this.listener = listener;
        }

        @Override
        protected String doInBackground(Void... voids) {
            Cursor curs = null;
                try {
                    curs = databaseHandler.getDistributorNameById(duserid);
                    curs.moveToFirst();
                    if (curs != null && curs.getCount() > 0) {
                        String strDistributorName;
                        strDistributorName = curs.getString(0);
                        Log.e("strDistributorName", curs.getString(0));
                        return strDistributorName;
                    } else {
                        Log.e("strDistributorName", "Not Avail");
                    }
                }catch (Exception e){
                    listener.onDistributorNameFetchFinished("",false,e.getMessage()+ "-" + TAG);
                }finally {
                    if (curs != null) {
                        curs.close();
                    }
                }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                if(result != null) {
                    listener.onDistributorNameFetchFinished(result, true, EMPTY);
                }else {
                    listener.onDistributorNameFetchFinished(result, true, EMPTY);   /////Should Change result to false
                }
            } catch (Exception e) {
                listener.onDistributorNameFetchFinished(result,false,e.getMessage() + "-" + TAG) ;
            }
        }
    }

    private void getDisNameAndFormOnlineRequest(final String userBeatModel,
                                                final ArrayList<MerchantOrderDetailDomain> merchantOrderList,
                                                final OnlineStockCompletedListener listener){


        String duserid = merchantOrderList.get(0).getDuserid();  ///////////// 0
        try {
            new GetDistributorNameAsyncTask(mContext, duserid, new DistributorNameCompletedListener() {
                @Override
                public void onDistributorNameFetchFinished(String distributorname,boolean result, String errorMSG) {
                    try {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("muserid", Constants.SalesMerchant_Id);
                        jsonObject.put("duserid", merchantOrderList.get(0).getDuserid());
                        if (userBeatModel.equals("C")) {
                            jsonObject.put("beattype", "C");
                            jsonObject.put("distributorname", Constants.distributorname);
                        } else if (userBeatModel.equals("D")) {
                            jsonObject.put("beattype", "D");
                        }
                        Log.e("beattype", "..................!!!!!!!!!1`````````````````````" + userBeatModel);

                        Log.e("OutletStkRqst", jsonObject.toString());
                        JsonServiceHandler jsonServiceHandler = new JsonServiceHandler(Utils.strMerchantCurrentStockPosition,
                                jsonObject.toString(), mContext);

                        new OnlineStockAsyncTask(merchantOrderList, jsonServiceHandler, listener).execute();
                    }catch (Exception e){
                        listener.onStockFetchFinished(false, e.getMessage()+ "-" + TAG);
                    }
                }
            }).execute();
        } catch (Exception e) {
            e.printStackTrace();
            listener.onStockFetchFinished(false, e.getMessage()+ "-" + TAG);
        }


    }

    public void getStock(final String userBeatModel, final ArrayList<MerchantOrderDetailDomain> merchantOrderList,
                         final OnlineStockCompletedListener listener) throws Exception{
        beatType = userBeatModel;
        for(int i = 0; i < merchantOrderList.size(); i++){
            Log.e("welcome",""+ "\n" + merchantOrderList.get(i).getprodid());
            Log.e("welcome",""+ "\n" + merchantOrderList.get(i).getprodname());
            Log.e("welcome",""+ "\n" + merchantOrderList.get(i).getFreeQty());
        }
        String duserid = merchantOrderList.get(0).getDuserid();  ///////////// 0
        if(beatType.equals("C")){
            if(Constants.distributorname.isEmpty()){
                Log.e("Distributor","is................empty");
                getDisNameAndFormOnlineRequest(userBeatModel, merchantOrderList, listener);
            }else {
                formRequestForOnlineStock(userBeatModel, merchantOrderList, listener);
            }
        }else {
            formRequestForOnlineStock(userBeatModel, merchantOrderList, listener);
        }

    }

    private void formRequestForOnlineStock(final String userBeatModel, final ArrayList<MerchantOrderDetailDomain> merchantOrderList,
                                           final OnlineStockCompletedListener listener){
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("muserid", Constants.SalesMerchant_Id);
            jsonObject.put("duserid", merchantOrderList.get(0).getDuserid());
            if (userBeatModel.equals("C")) {
                jsonObject.put("beattype", "C");
                jsonObject.put("distributorname", Constants.distributorname);
                Log.e("CompanyMode",".................beattype..C.." + "```````````````````````distributorname````" +Constants.distributorname);
            } else if (userBeatModel.equals("D")) {
                jsonObject.put("beattype", "D");
            }
            Log.e("beattype", "..................!!!!!!!!!1`````````````````````" + userBeatModel);

            Log.e("OutletStkRqst", jsonObject.toString());
            JsonServiceHandler jsonServiceHandler = new JsonServiceHandler(Utils.strMerchantCurrentStockPosition,
                    jsonObject.toString(), mContext);

            new OnlineAsyncTaskAction.OnlineStockAsyncTask(merchantOrderList, jsonServiceHandler, listener).execute();
        }catch (Exception e){
            listener.onStockFetchFinished(false, e.getMessage()+ "-" + TAG);
        }
    }

    private static class OnlineStockAsyncTask extends AsyncTask<Void, Void, JSONObject> {

        private OnlineAsyncTaskAction.OnlineStockCompletedListener listener;
        JsonServiceHandler jsonServiceHandler;
        private ArrayList<MerchantOrderDetailDomain> merchantOrderList;

        private OnlineStockAsyncTask(ArrayList<MerchantOrderDetailDomain> merchantOrderList,
                                     JsonServiceHandler jsonServiceHandler, OnlineStockCompletedListener listener) {
            this.listener = listener;
            this.jsonServiceHandler = jsonServiceHandler;
            this.merchantOrderList = merchantOrderList;
        }

        @Override
        protected JSONObject doInBackground(Void... voids) {
            return jsonServiceHandler.ServiceData();
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            super.onPostExecute(result);
            try {
                parsingStockJsonObject(merchantOrderList, result, listener);
            } catch (Exception e) {
                e.printStackTrace();
                listener.onStockFetchFinished(false, e.getMessage()+ "-" + TAG);
            }
        }
    }

    private static void parsingStockJsonObject(ArrayList<MerchantOrderDetailDomain> merchantOrderList,
                                               JSONObject resultStockObj,
                                               OnlineAsyncTaskAction.OnlineStockCompletedListener listener) throws Exception{
        try {
            if (resultStockObj != null) {
                Log.e("stock", "Result........." + resultStockObj.toString());
                Log.e("onPostExecute", "onlineStockValue");
                String strStatus = resultStockObj.getString("status");
                Log.e("return status", strStatus);
                String strMsg = resultStockObj.getString("message");
                Log.e("returnmessage", strMsg);
                if (strStatus.equalsIgnoreCase("false")) {
                    Log.e("Error", "Result Failed");
                    if(!beatType.equals("C")) {
                        for (int j = 0; j < merchantOrderList.size(); j++) {
                            merchantOrderList.get(j).setStockPosition(DISTRIBUTOR_STOCK + CURRENT_STOCK_NOT_AVAIL);
                        }
                    }else {
                        for (int j = 0; j < merchantOrderList.size(); j++) {
                            merchantOrderList.get(j).setStockPosition(DISTRIBUTOR_STOCK +
                                    CURRENT_STOCK_NOT_AVAIL + "\n" + COMPANY_STOCK + CURRENT_STOCK_NOT_AVAIL);
                        }
                    }

                    listener.onStockFetchFinished(true, SERVER_SEND_NO_STOCK_RESULT);
                } else {
                    JSONObject data = resultStockObj.getJSONObject("data");
                    if(!beatType.equals("C")) { //// Only Distributor
                        JSONArray dataArray = null;
                        if(data.has("mclosing")) {
                            dataArray = data.getJSONArray("mclosing");
                        }else {
                            Log.e("mclosing","not,,,,,,,,,,,,,,,,,,,,avail");
                            return;
                        }
                        Log.e("arrayLength", ".................................." + dataArray.length());
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject jsonobject = dataArray.getJSONObject(i);
                            String prodid = jsonobject.getString("prodid");
                            String closing = jsonobject.getString("closing");
                            for (int j = 0; j < merchantOrderList.size(); j++) {
                                MerchantOrderDetailDomain item = merchantOrderList.get(j);
                                if (item.getprodid().equals(prodid)) {
                                    item.setStockPosition(DISTRIBUTOR_STOCK + closing); /// newly Added Variable setter method
                                    merchantOrderList.set(j, item);
                                    break;
                                }
                                if (j == merchantOrderList.size() - 1) {
                                    item.setStockPosition(DISTRIBUTOR_STOCK + CURRENT_STOCK_NOT_AVAIL);
                                    merchantOrderList.set(j, item);
                                }
                            }
                        }
                    }else {
                        JSONArray mClosingArray = data.getJSONArray("mclosing");
                        JSONArray dClosingArray = data.getJSONArray("dclosing");
                        Log.e("arrayLength", "...........................dclosing......." + dClosingArray.length());
                        Log.e("arrayLength", ".............................mclosing....." + mClosingArray.length());

                        for (int i = 0; i < dClosingArray.length(); i++) {
                            JSONObject jsonobject = dClosingArray.getJSONObject(i);
                            String prodid = jsonobject.getString("prodid");
                            String closing = jsonobject.getString("closing");

                            JSONObject mJSONObj = mClosingArray.getJSONObject(i);
                            String mClosing = mJSONObj.getString("closing");

                            for (int j = 0; j < merchantOrderList.size(); j++) {
                                MerchantOrderDetailDomain item = merchantOrderList.get(j);
                                if (item.getprodid().equals(prodid)) {
                                    item.setStockPosition(DISTRIBUTOR_STOCK + closing + "\n" + COMPANY_STOCK + mClosing); /// newly Added Variable setter method
                                    merchantOrderList.set(j, item);
                                    break;
                                }
                                if (j == merchantOrderList.size() - 1) {
                                    item.setStockPosition(DISTRIBUTOR_STOCK +
                                            CURRENT_STOCK_NOT_AVAIL + "\n" + COMPANY_STOCK + CURRENT_STOCK_NOT_AVAIL);
                                    merchantOrderList.set(j, item);
                                }
                            }
                        }
                    }
                }
                listener.onStockFetchFinished(true, "");
            } else {
                listener.onStockFetchFinished(false, "No Data From Server For Stock"+ "-" + TAG);
            }
        }catch (Exception e){
            e.printStackTrace();
            listener.onStockFetchFinished(false, "Something Went Wrong Fetch Stock"+ "-" + TAG);
        }
    }



}
