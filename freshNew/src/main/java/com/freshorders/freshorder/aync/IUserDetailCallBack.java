package com.freshorders.freshorder.aync;

import org.json.JSONObject;

public interface IUserDetailCallBack {
    void userResponse(final JSONObject jsonObject);
}
