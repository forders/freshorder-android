package com.freshorders.freshorder.aync;

import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.freshorders.freshorder.utils.JsonServiceHandler;

import org.json.JSONObject;

public class UserDetailTask extends AsyncTask<Void, Void, JSONObject> {

    private String mUrl, mParams;
    private Context mContext;
    private IUserDetailCallBack iUserDetailCallBack;

    public UserDetailTask(final String url, final String params,
                          final Context context, final IUserDetailCallBack callBack) {
        this.mUrl = url;
        this.mParams = params;
        this.mContext = context;
        this.iUserDetailCallBack = callBack;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);
        iUserDetailCallBack.userResponse(jsonObject);
    }

    @Override
    protected JSONObject doInBackground(Void... voids) {
        if (TextUtils.isEmpty(mParams)) {

            final JsonServiceHandler serviceHandler =
                    new JsonServiceHandler(mUrl, mContext);
            return serviceHandler.ServiceDataGet();
        } else {
            final JsonServiceHandler serviceHandler =
                    new JsonServiceHandler(mUrl, mParams, mContext);
            return serviceHandler.ServiceData();
        }
    }
}
