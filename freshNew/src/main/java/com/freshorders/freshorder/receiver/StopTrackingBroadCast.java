package com.freshorders.freshorder.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;

import com.freshorders.freshorder.accuracy.location.track.LocationTrackForegroundService;
import com.freshorders.freshorder.activity.DistanceCalActivity;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.utils.Log;

import static com.freshorders.freshorder.utils.Constants.MENU_DISTANCE_CALCULATION_CODE;

public class StopTrackingBroadCast extends BroadcastReceiver {

    private static final String TAG = StopTrackingBroadCast.class.getSimpleName();
    private Context mContext;

    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;

            if(!PrefManager.getStatus(mContext)){
                new Log(mContext).ee(TAG,"...Tracking Not Avail To Stop........");
            }else {
                PrefManager.setStatus(mContext,false);
                mContext.stopService(new Intent(mContext,LocationTrackForegroundService.class));
                new Log(mContext).ee(TAG,"...Tracking Auto Stopped........");
            }
    }


    private boolean isTrackMenuFound(){
        DatabaseHandler db = new DatabaseHandler(mContext);
        Cursor menuCur = db.getMenuItems();
        if(menuCur != null && menuCur.getCount() > 0){
            menuCur.moveToFirst();
            int currentVisibility = menuCur.getInt(menuCur.getColumnIndex(DatabaseHandler.KEY_MENU_IS_VISIBLE));
            String currentMenuCode = menuCur.getString(menuCur.getColumnIndex(DatabaseHandler.KEY_MENU_CODE));
            if(MENU_DISTANCE_CALCULATION_CODE.equalsIgnoreCase(currentMenuCode)){
                if(currentVisibility == 1){
                    return true;
                }
            }
            menuCur.moveToNext();
        }
        return false;
    }
}
