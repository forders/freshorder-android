package com.freshorders.freshorder.receiver;

import android.accounts.Account;
import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SyncInfo;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.activity.DistanceCalActivity;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.service.BackgroundMigrationService;
import com.freshorders.freshorder.service.ConnectivityReset;
import com.freshorders.freshorder.service.FindAppIsBackground;
import com.freshorders.freshorder.service.MigrationResetBroadCast;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.syncadapter.adapter.SyncAdapterManager;
import com.freshorders.freshorder.toonline.BendingOrderMigrateToOnline;
import com.freshorders.freshorder.toonline.BendingValuesMigrateToServer;
import com.freshorders.freshorder.toonline.Migration;
import com.freshorders.freshorder.toonline.PendingClientVisitToServer;
import com.freshorders.freshorder.ui.BaseActivity;
import com.freshorders.freshorder.ui.SplashActivity;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.NetworkUtil;
import com.freshorders.freshorder.utils.NotificationUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static android.content.Context.ALARM_SERVICE;
import static com.freshorders.freshorder.service.PrefManager.KEY_IS_CONNECTIVITY_FIRED;
import static com.freshorders.freshorder.utils.Constants.ALERT_MIGRATION_MESSAGE;
import static com.freshorders.freshorder.utils.Constants.ALERT_MIGRATION_TITLE;

public class NetworkConnectReceiver extends BroadcastReceiver {

    public static ConnectivityReceiverListener connectivityReceiverListener;
    public Context mContext;
    private static String TAG = NetworkConnectReceiver.class.getSimpleName();

    private static final int CHANNEL_ID = 1000;
    private static final int CHANNEL_ID_FAILED = 1001;
    private static final String MIGRATION_TITLE = "Bending Data Moved Status";
    BendingOrderMigrateToOnline obj;

    public static Date startTime ;
    public static Long timeElapsed = 0L;
    public static Date endTime ;
    private Cursor curs;
    private Cursor cursClientVisit;

    ////////////////////////// for gps points
    private String authority;
    private String type;
    private Account account;


    @Override
    public void onReceive(Context context, Intent intent) {
        this.mContext = context.getApplicationContext();
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        type = context.getString(R.string.account_type);
        authority = context.getString(R.string.authority);
        account = new Account(context.getString(R.string.app_name), type);

        if(cm != null){
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            final boolean isConnected = activeNetwork != null
                    && activeNetwork.isConnectedOrConnecting();
            if (connectivityReceiverListener != null) {
                connectivityReceiverListener.onNetworkConnectionChanged(isConnected);
            }
            try {
                synchronized (this) {  /// onReceive called multiple times so some work done
                    // start background process .

                    String status = NetworkUtil.getConnectivityStatusString(context);
                    if (status.equals("Wifi enabled") || status.equals("Mobile data enabled")) {
                        startTime = new Date();

                        boolean isConnectedAlready = new PrefManager(mContext).getBooleanDataByKey(PrefManager.KEY_IS_CONNECTIVITY_FIRED);
                        if(!isConnectedAlready){
                            new PrefManager(mContext).setBooleanDataByKey(PrefManager.KEY_IS_CONNECTIVITY_FIRED, true);
                            FindAppIsBackground backgroundFinderObj = new FindAppIsBackground();
                            boolean isBackground = backgroundFinderObj.isAppIsInBackground(context);
                            if(!isBackground) {
                                ///////////////fullMigration();
                                /////////////////startReset(mContext);
                            }else {
                                Log.e(TAG, "App Not Visible.......................");
                            }
                            Log.e(TAG, "Mobile data enabled..............Fired........");
                            /// this for gps data sending
                            SyncAdapterManager syncAdapterManager;
                            syncAdapterManager = new SyncAdapterManager(mContext);
                            syncAdapterManager.initializePeriodicSync();
                            if(!isSyncActive(account, authority)){
                                syncAdapterManager.syncImmediately();
                            }else {
                                String bugPath = new PrefManager(mContext).getStringDataByKey(PrefManager.KEY_BUG_FILE_PATH);
                                new com.freshorders.freshorder.utils.Log(bugPath).ee("NetChange","............" + "Sync Already Running");
                            }
                            startResetConnectivity(mContext);
                        }
                        /*
                        Log.e("NetworkConnectReceiver", "...........isConnected::" + isConnected);
                        DatabaseHandler databaseHandler = new DatabaseHandler(mContext);
                        curs = databaseHandler.getorderheader();
                        cursClientVisit = databaseHandler.getClientVisit(Constants.STATUS_PENDING);
                        if ((curs != null && curs.getCount() > 0) || (cursClientVisit != null && cursClientVisit.getCount() > 0)) {
                            boolean isMigrationSet = new PrefManager(mContext).getBooleanDataByKey(PrefManager.KEY_IS_MIGRATION_SET);
                            if (!isMigrationSet) {
                                new PrefManager(mContext).setBooleanDataByKey(PrefManager.KEY_IS_MIGRATION_SET, true);
                                startReset(mContext);////////////
                                Log.e("NetworkConnectReceiver", "...........curs.getCount()::" + curs.getCount());
                                FindAppIsBackground backgroundFinderObj = new FindAppIsBackground();
                                boolean isBackground = backgroundFinderObj.isAppIsInBackground(context);
                                if (!isBackground) {
                                    Log.e("NetworkConnectReceiver", "...........getInstance::" + curs.getCount());
                                    Runnable runnable = new Runnable() {
                                        public void run() {
                                            System.out.println("Hello !!");
                                            //Intent migrationIntent = new Intent(mContext.getApplicationContext(), BackgroundMigrationService.class);
                                            //mContext.startService(migrationIntent);
                                            if(curs.getCount() > 0){
                                                doMigration();
                                            }
                                            Log.e("cursClientVisit", "...........cursClientVisit::Count::" + cursClientVisit.getCount());
                                            if(cursClientVisit.getCount() > 0){
                                                //clientVisitMigration();
                                            }

                                        }
                                    };
                                    ScheduledExecutorService service = Executors
                                            .newSingleThreadScheduledExecutor();
                                    service.schedule(runnable, 60, TimeUnit.SECONDS);  /// wait for user move to myorder page // changed kumaravel
                                } else {
                                    Log.e("NetworkConnectReceiver", "...........getInstance::NULL");
                                    NotificationUtil notificationUtil = new NotificationUtil(Constants.ALERT_CHANNEL_ID);
                                    notificationUtil.showAlertNotification(mContext, SplashActivity.class, ALERT_MIGRATION_TITLE, ALERT_MIGRATION_MESSAGE);
                                }
                            }
                        } */
                    }

                }
            }catch (Exception e){
                e.printStackTrace();
                Log.e("NetworkConnectReceiver",".................error:::"+e.getMessage());
            }

        }
    }

    private void fullMigration(){
        ///// Migration Work Start here///////////////////
        Migration migration = new Migration(mContext);
        boolean isNeedMigration = migration.isNeedForegroundMigration();
        if (isNeedMigration) {
            if(NetworkUtil.getConnectivityStatus(mContext) == 1 ||
                    NetworkUtil.getConnectivityStatus(mContext) == 2) {
                if(!migration.isMigrationServiceWorking()) {
                    migration.startMigrationByAlarmManager(mContext);
                }else {
                    Log.e(TAG, "Migration Alarm Service Already Working");
                }
            }else {
                Log.e(TAG, "Migration Alarm Service No Mobile Data Found");
            }
        }else {
            Log.e(TAG, "Migration Alarm Service Not Required");
        }
    }

    public void startResetConnectivity(Context context){

        Intent intent = new Intent(context, ConnectivityReset.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 234324242, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if (alarmManager != null) {
            alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + (30 * 1000), pendingIntent);
            Toast.makeText(context, "Reset Started",Toast.LENGTH_LONG).show();
        }else {
            Toast.makeText(context, "Reset Started Failed",Toast.LENGTH_LONG).show();
        }

    }

    public void startReset(Context context){

        Intent intent = new Intent(context, MigrationResetBroadCast.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 234324243, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if (alarmManager != null) {
            alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + (4 * 60 * 1000), pendingIntent);
            Toast.makeText(context, "Migration Reset Started",Toast.LENGTH_LONG).show();
        }else {
            Toast.makeText(context, "Migration Reset Started Failed",Toast.LENGTH_LONG).show();
        }

    }

    private void clientVisitMigration(){
        try {
            // New Client Migration
            new PendingClientVisitToServer(mContext, new PendingClientVisitToServer.ServerCompleteListener() {
                @Override
                public void onSuccess() {
                    Log.e("ClientVisitToServer", "Pending Client Visit Successfully Moved");
                    if(mContext != null)
                    Toast.makeText(mContext, "Pending Client Visit Successfully Moved", Toast.LENGTH_LONG).show();
                }
                @Override
                public void onFailed(String errorMSG) {
                    Log.e("ClientVisitToServer", "Pending Client Visit Failed");
                    if(mContext != null)
                    Toast.makeText(mContext, "Pending Client Visit Failed", Toast.LENGTH_LONG).show();
                }
            }).startMyClientVisitMigration();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void doMigration(){
        try {
            Log.e("onHandleIntent", ".................Inside");
            obj = new BendingOrderMigrateToOnline(mContext, new BendingOrderMigrateToOnline.ServerMigrationListener() {
                @Override
                public void onMigrationFinished(boolean isSuccess, String statusMSG, boolean isException, String excMSG) {
                    Log.e("BackgroundMigration", "......................isSuccess" + isSuccess + ":::excMSG::" + excMSG);
                    String result = "";
                    if (isSuccess) {
                        result = "Pending Order Migrated to Server Successfully. To See Result Click me";
                        NotificationUtil notificationUtil = new NotificationUtil(CHANNEL_ID);
                        notificationUtil.showNotification(mContext, SplashActivity.class, MIGRATION_TITLE, result);
                    } else if (isException) {
                        result = "Pending Order Migration Failed ..." + excMSG;
                        NotificationUtil notificationUtil = new NotificationUtil(CHANNEL_ID_FAILED);
                        notificationUtil.showNotification(mContext, SplashActivity.class, MIGRATION_TITLE, result);
                    }
                    if (obj != null && obj.getListener() != null) {
                        obj.setListenerNULL();
                    }

                    try {
                        /// here add other pending table migration started.........
                        //new BendingValuesMigrateToServer(mContext).startPendingMigration();
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }
            });
            obj.migration();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public interface ConnectivityReceiverListener {
        void onNetworkConnectionChanged(boolean isConnected);
    }

    public interface MigrationConnectivityListener{
        void onNetworkConnectionChanged(boolean isConnected);
    }

    private static boolean isSyncActive(Account account, String authority)
    {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
        {
            return isSyncActiveHoneycomb(account, authority);
        } else
        {
            SyncInfo currentSync = ContentResolver.getCurrentSync();
            return currentSync != null && currentSync.account.equals(account) &&
                    currentSync.authority.equals(authority);
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private static boolean isSyncActiveHoneycomb(Account account, String authority)
    {
        for(SyncInfo syncInfo : ContentResolver.getCurrentSyncs())
        {
            if(syncInfo.account.equals(account) &&
                    syncInfo.authority.equals(authority))
            {
                return true;
            }
        }
        return false;
    }

}