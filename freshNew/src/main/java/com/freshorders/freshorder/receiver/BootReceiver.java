package com.freshorders.freshorder.receiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.widget.Toast;

import com.freshorders.freshorder.service.BootCompletedService;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.service.RunAfterBootService;
import com.freshorders.freshorder.service.ServiceForegroundMigration;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.Log;

import java.util.Calendar;

import static android.content.Context.ALARM_SERVICE;
import static com.freshorders.freshorder.utils.Constants.BR_REQUEST_CODE_START_LIVE_TRACK;
import static com.freshorders.freshorder.utils.Constants.BR_REQUEST_CODE_START_LIVE_TRACK_BY_ALARM;
import static com.freshorders.freshorder.utils.Constants.BR_REQUEST_CODE_STOP_LIVE_TRACK;
import static com.freshorders.freshorder.utils.Constants.SELF_START_TIME_LIVE_TRACK;
import static com.freshorders.freshorder.utils.Constants.SELF_STOP_TIME_LIVE_TRACK;

public class BootReceiver extends BroadcastReceiver {

    private static String TAG = BootReceiver.class.getSimpleName();
    private Context mContext;

    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            new Log(context).ee(TAG, "ACTION_BOOT_COMPLETED.....Triggered");
            /////////////BootCompletedService.enqueueWork(context, new Intent());
            if(PrefManager.getStatus(context.getApplicationContext()))
            {
                startMigrationByAlarmManager(context);
            }


            startAndStopTrackingService();


                /*
            Intent startActivityIntent = new Intent(context, DummyActivity.class);
            context.startActivity(startActivityIntent);  */
        }
    }


    private void startAndStopTrackingService(){


        final long delay;
        final long delayStop;

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, SELF_START_TIME_LIVE_TRACK);

        AlarmManager alarmManager = (AlarmManager) mContext.getSystemService(ALARM_SERVICE);

        Intent intent = new Intent(mContext, StartTrackingBroadCast.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, BR_REQUEST_CODE_START_LIVE_TRACK, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        delay = AlarmManager.INTERVAL_DAY;
		/*
		if (DateTime.now().getHourOfDay() < SELF_START_TIME_LIVE_TRACK) {
			delay = new Duration(DateTime.now() , DateTime.now().withTimeAtStartOfDay().plusHours(SELF_START_TIME_LIVE_TRACK)).getStandardMinutes();
		} else {
			delay = new Duration(DateTime.now() , DateTime.now().withTimeAtStartOfDay().plusDays(1).plusHours(SELF_START_TIME_LIVE_TRACK)).getStandardMinutes();
		} */

        alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), delay, pendingIntent);  //AlarmManager.INTERVAL_FIFTEEN_MINUTES

        /////////////////////////////////////////////////////
        Intent intentStop = new Intent(mContext, StopTrackingBroadCast.class);
        PendingIntent piStop = PendingIntent.getBroadcast(mContext, BR_REQUEST_CODE_STOP_LIVE_TRACK, intentStop,
                PendingIntent.FLAG_UPDATE_CURRENT);
        delayStop = AlarmManager.INTERVAL_DAY;
		/*
		if (DateTime.now().getHourOfDay() < SELF_START_TIME_LIVE_TRACK) {
			delayStop = new Duration(DateTime.now() , DateTime.now().withTimeAtStartOfDay().plusHours(SELF_START_TIME_LIVE_TRACK)).getStandardMinutes();
		} else {
			delayStop = new Duration(DateTime.now() , DateTime.now().withTimeAtStartOfDay().plusDays(1).plusHours(SELF_START_TIME_LIVE_TRACK)).getStandardMinutes();
		} */

        AlarmManager alarmManager1 = (AlarmManager) mContext.getSystemService(ALARM_SERVICE);
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTimeInMillis(System.currentTimeMillis());
        calendar1.set(Calendar.HOUR_OF_DAY, SELF_STOP_TIME_LIVE_TRACK);


        alarmManager1.setInexactRepeating(AlarmManager.RTC_WAKEUP, calendar1.getTimeInMillis(), delayStop, piStop);  //AlarmManager.INTERVAL_FIFTEEN_MINUTES

        ///startTrackingOnAppStart();

    }


    public void startMigrationByAlarmManager(Context mContext){
        /** this gives us the time for the first trigger.  */
        try {
            Calendar cal = Calendar.getInstance();
            AlarmManager am = (AlarmManager) mContext.getSystemService(ALARM_SERVICE);
            long interval = 1000 * 5; // 5 seconds
            Intent serviceIntent = new Intent(mContext, RunAfterBootService.class);
            PendingIntent servicePendingIntent =
                    PendingIntent.getService(mContext,
                            Constants.BOOT_COMPLETED_AM_ID, // integer constant used to identify the service
                            serviceIntent,
                            PendingIntent.FLAG_CANCEL_CURRENT);  // FLAG to avoid creating a second service if there's already one running
            if (am != null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    am.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis() + interval,
                            servicePendingIntent);
                }else {
                    am.setExact(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis() + interval,
                            servicePendingIntent);
                }
            }else {
                android.util.Log.e(TAG, "Alarm Manager get null ");
                Toast.makeText(mContext,"Please start track by manual",Toast.LENGTH_LONG).show();
            }
        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(mContext,"Start track interrupted",Toast.LENGTH_LONG).show();
        }
    }
}
