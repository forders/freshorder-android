package com.freshorders.freshorder.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;

import com.freshorders.freshorder.accuracy.location.track.LocationTrackForegroundService;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.utils.Log;

import org.joda.time.DateTime;

import static com.freshorders.freshorder.utils.Constants.DATE_TIME_SUNDAY;
import static com.freshorders.freshorder.utils.Constants.MENU_DISTANCE_CALCULATION_CODE;
import static com.freshorders.freshorder.utils.Constants.SELF_START_TIME_LIVE_TRACK;
import static com.freshorders.freshorder.utils.Constants.SELF_STOP_TIME_LIVE_TRACK;

public class StartTrackingBroadCastImmediate extends BroadcastReceiver {

    private static final String TAG = StartTrackingBroadCast.class.getSimpleName();

    private Context mContext;

    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;
        DateTime dateTime = new DateTime();
        if (!(dateTime.getDayOfWeek() == DATE_TIME_SUNDAY/* Sunday*/)) {
            if (isTrackMenuFound()) {
                if (!PrefManager.getStatus(mContext)) {
                    try {
                        PrefManager.setStatus(mContext, true);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            mContext.startForegroundService(new Intent(mContext, LocationTrackForegroundService.class));
                        } else {
                            mContext.startService(new Intent(mContext, LocationTrackForegroundService.class));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        new Log(mContext).ee(TAG, "...Error Produced in Auto Start Tracking........" + e.getMessage());
                    }
                } else {
                    new Log(mContext).ee(TAG, "...Already Tracking Started........");
                }
            } else {
                new Log(mContext).ee(TAG, "...Tracking Menu Not Found........");
            }
        } else {
            new Log(mContext).ee(TAG, "...Today Sunday Not Start Live Track........");
        }

    }

    private boolean isTrackMenuFound(){
        DatabaseHandler db = new DatabaseHandler(mContext);
        Cursor menuCur = db.getMenuItems();
        if(menuCur != null && menuCur.getCount() > 0){
            menuCur.moveToFirst();
            for(int i = 0; i < menuCur.getCount(); i++) {
                int currentVisibility = menuCur.getInt(menuCur.getColumnIndex(DatabaseHandler.KEY_MENU_IS_VISIBLE));
                String currentMenuCode = menuCur.getString(menuCur.getColumnIndex(DatabaseHandler.KEY_MENU_CODE));
                if (MENU_DISTANCE_CALCULATION_CODE.equalsIgnoreCase(currentMenuCode)) {
                    if (currentVisibility == 1) {
                        return true;
                    }
                }
                menuCur.moveToNext();
            }
        }
        return false;
    }


}