package com.freshorders.freshorder;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.freshorders.freshorder.accuracy.location.track.LocationTrackForegroundService;
import com.freshorders.freshorder.activity.DistanceCalActivity;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.model.GPSPointsDetail;
import com.freshorders.freshorder.model.PointsResponse;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.NotificationUtil;
import com.freshorders.freshorder.utils.ServiceClass;
import com.freshorders.freshorder.volleylib.APIInterface;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.freshorders.freshorder.service.PrefManager.FCM_TOKEN;
import static com.freshorders.freshorder.service.PrefManager.IS_DELIVERED_FCM_TOKEN;
import static com.freshorders.freshorder.service.PrefManager.KEY_IS_MY_DAY_PLAN_LEAVE;
import static com.freshorders.freshorder.service.PrefManager.KEY_MY_DAY_PLAN_LEAVE_DATE;
import static com.freshorders.freshorder.utils.Constants.DATE_PATTERN_FOR_APP;
import static com.freshorders.freshorder.utils.Constants.MENU_DISTANCE_CALCULATION_CODE;
import static com.freshorders.freshorder.utils.Constants.SELF_START_TIME_LIVE_TRACK;
import static com.freshorders.freshorder.utils.Constants.SELF_STOP_TIME_LIVE_TRACK;


public class MyFirebaseMessagingService extends FirebaseMessagingService {


    ////////
    APIInterface apiInterface;
    DatabaseHandler databaseHandler;
    String bugPath = "";

    private Context mContext;

    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onNewToken(@NonNull String token) {
        super.onNewToken(token);
            Log.d(TAG, "Refreshed token: " + token);
            //NotificationUtil.showMyNotification(getApplicationContext(),R.string.fcm_message, token, 9000);
            //FirebaseMessaging.getInstance().subscribeToTopic("all");
            tokenToSharedPreference(getApplicationContext(), token);


    }

    private void tokenToSharedPreference(Context ctx, String token){
        PrefManager pre = new PrefManager(ctx);
        pre.setBooleanDataByKey(IS_DELIVERED_FCM_TOKEN, false);
        pre.setStringDataByKey(FCM_TOKEN, token);
    }

    /**
     * Schedule async work using WorkManager.
     */
    /*
    private void scheduleJob() {
        // [START dispatch_job]
        OneTimeWorkRequest work = new OneTimeWorkRequest.Builder(MyWorker.class)
                .build();
        WorkManager.getInstance().beginWith(work).enqueue();
        // [END dispatch_job]
    }  */

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {


        mContext = getApplicationContext();
        Log.d(TAG, "Message Received........................." + remoteMessage);

        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            NotificationUtil.showMyNotification(getApplicationContext(),R.string.fcm_message, "DataMessage : "+ remoteMessage.getData(), 9003);

            String strIsStart = remoteMessage.getData().get("isstart");
            String date = remoteMessage.getData().get("timestamp");
            boolean isStart ;
            if(strIsStart != null && date != null){
                isStart = Boolean.parseBoolean(strIsStart);
                if(isStart) {
                    if (!isTrackingServiceRun(mContext)) {
                        if(isCurrentDate(date) && isTimeEligible()) {
                            checkAndStartTracking();
                        }
                    }
                }else{
                    if(isTrackingServiceRun(mContext)){
                        if(isCurrentDate(date)) {
                            stopTrackingService(mContext);
                        }
                    }
                }
            }



            //String type_of_message = remoteMessage.getData().get("type_of_message")+"";
            /*
            if(type_of_message.equalsIgnoreCase("location"))
            {
                // Kumaravel
                apiInterface = APIClient.getClient().create(APIInterface.class);
                databaseHandler = DatabaseHandler.getInstance(getApplicationContext());
                bugPath = new PrefManager(getApplicationContext()).getStringDataByKey(PrefManager.KEY_BUG_FILE_PATH);
                ////////
                callApis();
            }  */
        }

        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            NotificationUtil.showMyNotification(getApplicationContext(),R.string.fcm_message, "Notification : ", 9004);
            checkAndStartTracking();
        }else {
            Log.d(TAG, "Message Notification Failed.............");
        }
        //remoteMessage.
    }

    private boolean isTimeEligible(){
        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);
        return timeOfDay >= SELF_START_TIME_LIVE_TRACK && timeOfDay < SELF_STOP_TIME_LIVE_TRACK;
    }

    private boolean isCurrentDate(String receivedDate){
        try{
            Log.e(TAG,"................receivedDate " + receivedDate);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            String todayDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new java.util.Date());

            Date givenDate = sdf.parse(receivedDate);
            Date now = sdf.parse(todayDate);
            return givenDate.compareTo(now) == 0;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    private void stopTrackingService(Context mContext){
        stopService(new Intent(mContext,LocationTrackForegroundService.class));
        PrefManager.setStatus(mContext,false);
    }

    private void checkAndStartTracking() {
        if (isTrackMenuFound()) {
            if (!isTrackingServiceRun(mContext)) {
                if(!isMyDayPlanLeave()) {
                    try {
                        PrefManager.setStatus(mContext, true);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            mContext.startForegroundService(new Intent(mContext, LocationTrackForegroundService.class));
                        } else {
                            mContext.startService(new Intent(mContext, LocationTrackForegroundService.class));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        new com.freshorders.freshorder.utils.Log(mContext).ee(TAG, "...Error Produced in Auto Start Tracking........" + e.getMessage());
                    }
                }else {
                    new com.freshorders.freshorder.utils.Log(mContext).ee(TAG, "...MyDay Plan is Leave Wont Start Tracking........");
                }
            }else {
                new com.freshorders.freshorder.utils.Log(mContext).ee(TAG, "...Already Tracking Started........");
            }

        }else {
            new com.freshorders.freshorder.utils.Log(mContext).ee(TAG, "...Tracking Menu Not Found........");
        }
    }

    private boolean isMyDayPlanLeave(){

        PrefManager pre = new PrefManager(mContext);
        DateTime curDate = new DateTime();
        DateTimeFormatter formatter = DateTimeFormat.forPattern(DATE_PATTERN_FOR_APP);


        if(pre.getBooleanDataByKey(KEY_IS_MY_DAY_PLAN_LEAVE)){
            String myDayPlaneDate = pre.getStringDataByKey(KEY_MY_DAY_PLAN_LEAVE_DATE);
            if(!myDayPlaneDate.isEmpty()) {
                DateTime planDate = formatter.parseDateTime(myDayPlaneDate);
                return DateTimeComparator.getDateOnlyInstance().compare(curDate, planDate) == 0;
            }
        }

        return false;
    }

    private boolean isTrackingServiceRun(Context context){

        try {
            if (ServiceClass.isServiceRunningInForeground(context, LocationTrackForegroundService.class, TAG)) {
                return true;
            } else{
                PrefManager.setStatus(context, false);  /////////////////
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return PrefManager.getStatus(context);
        }
    }



    private boolean isTrackMenuFound(){
        DatabaseHandler db = new DatabaseHandler(mContext);
        Cursor menuCur = db.getMenuItems();
        if(menuCur != null && menuCur.getCount() > 0){
            menuCur.moveToFirst();
            for(int i = 0; i < menuCur.getCount(); i++) {
                int currentVisibility = menuCur.getInt(menuCur.getColumnIndex(DatabaseHandler.KEY_MENU_IS_VISIBLE));
                String currentMenuCode = menuCur.getString(menuCur.getColumnIndex(DatabaseHandler.KEY_MENU_CODE));
                if (MENU_DISTANCE_CALCULATION_CODE.equalsIgnoreCase(currentMenuCode)) {
                    if (currentVisibility == 1) {
                        return true;
                    }
                }
                menuCur.moveToNext();
            }
        }
        return false;
    }


    public void callApis()
    {
        sendPointsThroughRetrofit(0, databaseHandler, bugPath);/////////
    }






    public void sendPointsThroughRetrofit(long rowId,
                                          DatabaseHandler databaseHandler, String bugPath){
        Log.e(TAG,"...........................Trig");

        try{

            if (Constants.USER_ID == null || Constants.USER_ID.isEmpty()) {
                Cursor curs;
                curs = databaseHandler.getDetails();
                if (curs != null && curs.getCount() > 0) {
                    curs.moveToFirst();
                    Constants.USER_ID = curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_id));
                    Log.e("Constants.USER_ID", Constants.USER_ID);
                    curs.close();
                }else {
                    return ;
                }
            }

            //Cursor cursor = databaseHandler.getUnSyncLocationsById(rowId);
            Cursor cursor = databaseHandler.getAllUnSyncLocation();
            if(cursor != null && cursor.getCount() > 0) {
                Log.e(TAG,"...........................dataFound");
                cursor.moveToFirst();
                rowId = cursor.getLong(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_DATE_TIME));
                Location start = new Location("locationA");
                Location end = new Location("locationB");
                double lat = 0, lon = 0, preLat = 0, preLon = 0;
                JSONObject item = new JSONObject();
                item.put("rowid", rowId);
                item.put("batchid", 0);
                item.put("suserid", Constants.USER_ID);
                item.put("logtime", cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_DATE_TIME)));
                lat = cursor.getDouble(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_LAT));
                lon = cursor.getDouble(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_LON));
                start.setLatitude(lat);
                start.setLongitude(lon);
                long preId = cursor.getLong(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_PREVIOUS_ID));
                Cursor curs = databaseHandler.getLocationsForPreById(preId);
                if (curs != null && curs.getCount() > 0) {
                    Log.e(TAG,"...........................previousFound");
                    curs.moveToFirst();
                    preLat = curs.getDouble(curs.getColumnIndex(DatabaseHandler.KEY_LOCATION_LAT));
                    preLon = curs.getDouble(curs.getColumnIndex(DatabaseHandler.KEY_LOCATION_LON));
                    end.setLatitude(preLat);
                    end.setLongitude(preLon);
                    curs.close();
                } else {
                    return;
                }

                double temp_distance = start.distanceTo(end);
                temp_distance = temp_distance / 1000;
                Log.e(TAG, ".................temp_distance::::KM::" + temp_distance);

                item.put("geolat", lat);
                item.put("geolong", lon);
                item.put("pgeolat", preLat);
                item.put("pgeolong", preLon);
                item.put("status", "Active");
                item.put("updateddt", cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_DATE_TIME)));
                item.put("distance", temp_distance);
                item.put("amount", 0);
                item.put("issynced", 1);
                item.put("pre_id", cursor.getLong(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_PREVIOUS_ID)));
                item.put("seqid", cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_UNIQUE_ID)));

                GPSPointsDetail obj = new GPSPointsDetail(rowId,0,Constants.USER_ID,cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_DATE_TIME)),
                        lat,lon,preLat,preLon,"Active",cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_DATE_TIME)),
                        temp_distance,0,1,cursor.getLong(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_PREVIOUS_ID)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_UNIQUE_ID)));

                //addToQueue(item,databaseHandler, bugPath);/////////////////
                try {
                    if (isNetAvail(getApplicationContext())) {
                        addToQueue(obj,databaseHandler, bugPath);/////////////////
                    } else {
                        Log.e(TAG, "Mobile Data Not Avail......");
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    new com.freshorders.freshorder.utils.Log(bugPath).ee(TAG,"............."+ e.getMessage());
                }

            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void addToQueue(final GPSPointsDetail jsonStringer,final DatabaseHandler databaseHandler, final String bugPath){
        Log.e(TAG,"................retro...........Trig");
        //final Call<PointsResponse> call = getRetrofit().sendPoints(jsonStringer);
        if(apiInterface != null) {
            final Call<PointsResponse> call = apiInterface.sendPoints(jsonStringer);

            call.enqueue(new Callback<PointsResponse>() {
                @Override
                public void onResponse(Call<PointsResponse> call, Response<PointsResponse> response) {
                    try {
                        if (response != null) {
                            if (response.isSuccessful()) {
                                //long currentId = jsonStringer.getLong("rowid");
                                long currentId = jsonStringer.getRowid();
                                new com.freshorders.freshorder.utils.
                                        Log(bugPath).
                                        ee(TAG, ".........currentId...." + currentId + ".....Success:::");
                                Log.e(TAG, "..........success....." + response.toString());
                                if (response.body().getStatus().equals("true")) {
                                    Log.e(TAG, "..........success.....Data::::" + response.body().getData());
                                    Log.e(TAG, "..........success.....Request::pre_id::" + jsonStringer.getPre_id());
                                    //long currentId = jsonStringer.getLong("rowid");
                                    databaseHandler.setDistancePointSuccess(currentId);
                                    //databaseHandler.deleteSuccessLocationById(jsonStringer.getLong("pre_id"));
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<PointsResponse> call, Throwable throwable) {
                    try {
                        //long currentId = jsonStringer.getLong("rowid");
                        long currentId = jsonStringer.getRowid();
                        new com.freshorders.freshorder.utils.
                                Log(bugPath).
                                ee(TAG, ".........currentId...." + currentId + ".....Reason:::" + throwable.getMessage());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
        }else {
            new com.freshorders.freshorder.utils.Log(bugPath).ee(TAG, "............Retrofit not created.");
        }
    }

    private boolean isNetAvail(Context context){
        try {
            boolean isConnected,isMobile = false;
            ConnectivityManager cm =
                    (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = null;
            if (cm != null) {
                activeNetwork = cm.getActiveNetworkInfo();
            }else {
                return false;
            }

            isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
            if (activeNetwork != null) {
                isMobile = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE;
            }
            if(isConnected && isMobile){
                return true;
            }
            return false;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
}