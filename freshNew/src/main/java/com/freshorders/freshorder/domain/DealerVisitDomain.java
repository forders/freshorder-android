package com.freshorders.freshorder.domain;

/**
 * Created by dhanapriya on 21/10/16.
 */
public class DealerVisitDomain {

    String planid;
    String mname;
    String sname;
    String date;
    String serialno;
    String geolat;
    String geolng;
    String notes;


    public String getPlanId() {
        return planid;
    }

    public void setPlanId(String planid) {
        this.planid = planid;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSerialNo() {
        return serialno;
    }

    public void setSerialNo(String serialno) {
        this.serialno = serialno;
    }

    public String getGeoLat() {
        return geolat;
    }

    public void setGeoLat(String geolat) {
        this.geolat = geolat;
    }

    public String getGeoLng() {
        return geolng;
    }

    public void setGeoLng(String geolng) {
        this.geolng = geolng;
    }
    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }


}
