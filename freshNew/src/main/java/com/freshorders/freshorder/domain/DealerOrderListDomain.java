package com.freshorders.freshorder.domain;

public class DealerOrderListDomain {
	String comName;
	String Address;
	String Date;
	String Orderid;
	String fullname;
	String muserid;
	String mname;
	String serialno;
	String serialnonew;
	String isread;
	String rowid;
	String orderimage;
	double approximate;


	public double getapproximate() {
		return approximate;
	}

	public void setapproximate(double approximate) {
		this.approximate = approximate;
	}

	public String getorderimage() {
		return orderimage;
	}

	public void setorderimage(String orderimage) {
		this.orderimage = orderimage;
	}

	public String getrowid() {
		return rowid;
	}

	public void setrowid(String rowid) {
		this.rowid = rowid;
	}

	public String getisread() {
		return isread;
	}

	public void setisread(String isread) {
		this.isread = isread;
	}

	public String getSerialno() {
		return serialno;
	}

	public void setSerialno(String serialno1) {
		this.serialno = serialno1;
	}

	public String getserialnonew() {
		return serialnonew;
	}

	public void setserialnonew(String serialnonew) {
		this.serialnonew = serialnonew;
	}

	public String getMname() {
		return mname;
	}

	public void setMname(String mname1) {
		this.mname = mname1;
	}

	public String getMuserid() {
		return muserid;
	}

	public void setMuserid(String muserid) {
		this.muserid = muserid;
	}

	public String getOrderid() {
		return Orderid;
	}

	public void setOrderid(String orderid) {
		Orderid = orderid;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getComName() {
		return comName;
	}

	public void setComName(String comName) {
		this.comName = comName;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}

}
