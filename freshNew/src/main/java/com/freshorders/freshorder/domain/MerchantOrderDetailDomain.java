package com.freshorders.freshorder.domain;

public class MerchantOrderDetailDomain {
	String usrdlrid;
	String prodid;
	String Date;
	String prodcode;
	String qty;
	String prodname;
	String prodimage;
	String duserid;
	String companyname;
	String freeQty;
	String serialno;
	String paymentType;
	private int index;
	String prodprice;
	String prodtax;
	String batchno;
	String expdate;
	String mgfdate;
	String uom,selectedUOM,selectedfreeUOM, unitgram, unitperUom,selectedQtyUomUnit,selectedFreeQtyUomUnit;
	String deliverymode;
	String deliverydate;
	String deliverytime;
	String currstock;

	// By Kumaravel
	String stockPosition = "";

	public String getStockPosition() {
		return stockPosition;
	}

	public void setStockPosition(String stockPosition) {
		this.stockPosition = stockPosition;
	}

	public String getProdimage() {
		return prodimage;
	}

	public void setProdimage(String prodimage) {
		this.prodimage = prodimage;
	}

	public String getCurrStock() {
		return currstock;
	}

	public void setCurrStock(String currstock) {
		this.currstock = currstock;
	}

	public String getDeliverytime() {
		return deliverytime;
	}

	public void setDeliverytime(String deliverytime) {
		this.deliverytime = deliverytime;
	}

	public String getDeliverydate() {
		return deliverydate;
	}

	public void setDeliverydate(String deliverydate) {
		this.deliverydate = deliverydate;
	}

	public String getDeliverymode() {
		return deliverymode;
	}

	public void setDeliverymode(String deliverymode) {
		this.deliverymode = deliverymode;
	}
    public String getSelectedQtyUomUnit() {return selectedQtyUomUnit;}

    public void setSelecetdQtyUomUnit(String selectedQtyUomUnit) {
        this.selectedQtyUomUnit = selectedQtyUomUnit;
    }
	public String getSelectedFreeQtyUomUnit() {return selectedFreeQtyUomUnit;}

	public void setSelecetdFreeQtyUomUnit(String selectedFreeQtyUomUnit) {
		this.selectedFreeQtyUomUnit = selectedFreeQtyUomUnit;
	}

    public String getUnitPerUom() {return unitperUom;}

    public void setUnitPerUom(String unitperUom) {
        this.unitperUom = unitperUom;
    }

	public String getUnitGram() {return unitgram;}

	public void setUnitGram(String unitgram) {
		this.unitgram = unitgram;
	}


	public String getselectedfreeUOM() {return selectedfreeUOM;}

	public void setselectedfreeUOM(String selectedfreeUOM) {
		this.selectedfreeUOM = selectedfreeUOM;
	}

	public String getSlectedUOM() {return selectedUOM;}

	public void setSelectedUOM(String selectedUOM) {
		this.selectedUOM = selectedUOM;
	}

	public String getUOM()
	{return uom;}

	public void setUOM(String uom) {
		this.uom = uom;
	}

	public String getMgfDate() {return mgfdate;}

	public void setMgfDate(String mgfdate) {
		this.mgfdate = mgfdate;
	}

	public String getExpdate() {return expdate;}

	public void setExpdate(String expdate) {
		this.expdate = expdate;
	}

	public String getBatchno() {return batchno;}

	public void setBatchno(String batchno) {
		this.batchno = batchno;
	}

	public String getprodprice() {return prodprice;}

	public void setprodprice(String prodprice) {
		this.prodprice = prodprice;
	}

	public String getprodtax() {return prodtax;}

	public void setprodtax(String prodtax) {
		this.prodtax = prodtax;
	}

	public String getPaymentType() {return paymentType;}

	public void setpaymentType(String paymentType1) {
		this.paymentType = paymentType1;
	}

	public String getserialno() {return serialno;}

	public void setserialno(String serialno) {
		this.serialno = serialno;
	}

	public String getFreeQty() {
		return freeQty;
	}

	public void setFreeQty(String freeQty1) {
		freeQty = freeQty1;
	}

	public void setDuserid(String duserid) {
		this.duserid = duserid;
	}

	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}


	public String getCompanyname() {
		return companyname;
	}

	public String getDuserid() {
		return duserid;
	}



	public String getprodcode() {
		return prodcode;
	}

	public void setprodcode(String prodtcode) {
		prodcode = prodtcode;
	}

	public String getusrdlrid() {
		return usrdlrid;
	}

	public void setusrdlrid(String usrdlrid) {
		this.usrdlrid = usrdlrid;
	}

	public String getqty() {
		return qty;
	}

	public void setqty(String qty) {
		this.qty = qty;
	}

	public String getprodid() {
		return prodid;
	}

	public void setprodid(String prodtid) {
		prodid = prodtid;
	}

	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}
	
	public String getprodname() {
		return prodname;
	}

	public void setprodname(String prodtname) {
		prodname = prodtname;
	}


	public int getindex() {return index;}
	public void setindex(int index) {
		index = index;
	}
}
