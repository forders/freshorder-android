package com.freshorders.freshorder.domain;

public class DealerOrderDetailDomain {
	public String getProdcode() {
		return prodcode;
	}

	public void setProdcode(String prodcode) {
		this.prodcode = prodcode;
	}

	String orddtlid;
	String prodid;
	String Date;
	String Orderid;
	String qty;
	String ordstatus;
	String prodcode;
	String prodname;
	String freeQty;
	String flag;
	String duserid;
	String isread;
	String ordimage;
	String prodprice;
	String paymentType;
	String uom,selectedUom,selectedFreeUom, unitgram, unitperUom,selectedQtyUomUnit,selectedFreeQtyUomUnit;
	String orderfreeuom;
	String deliverymode;
	String deliverydate;
	String deliverytime,prodPrice, prodTax, pvalue, defaultUomTotal;
	String currstock;

	public String getPvalue() {return pvalue;}

	public void setPvalue(String pvalue) {
		this.pvalue = pvalue;
	}

	public String getCurrStock() {
		return currstock;
	}

	public void setCurrStock(String currstock) {
		this.currstock = currstock;
	}

	public String getProdprice() {
		return prodprice;
	}

	public void setProdprice(String prodprice) {
		this.prodprice = prodprice;
	}

	public String getDefaultUomTotal() {return defaultUomTotal;}

	public void setDefaultUomTotal(String defaultUomTotal) {
		this.defaultUomTotal = defaultUomTotal;
	}

	public String getProductTax() {return prodTax;}

	public void setProductTax(String prodTax) {
		this.prodTax = prodTax;
	}

	public String getProductPrice() {return prodPrice;}

	public void setProductPrice(String prodPrice) {
		this.prodPrice = prodPrice;
	}

	public String getSelectedQtyUomUnit() {
		return selectedQtyUomUnit;
	}

	public void setSelecetdQtyUomUnit(String selectedQtyUomUnit) {
		this.selectedQtyUomUnit = selectedQtyUomUnit;
	}
	public String getSelectedFreeQtyUomUnit() {return selectedFreeQtyUomUnit;}

	public void setSelecetdFreeQtyUomUnit(String selectedFreeQtyUomUnit) {
		this.selectedFreeQtyUomUnit = selectedFreeQtyUomUnit;
	}

	public String getUnitPerUom() {return unitperUom;}

	public void setUnitPerUom(String unitperUom) {
		this.unitperUom = unitperUom;
	}

	public String getUnitGram() {return unitgram;}

	public void setUnitGram(String unitgram) {
		this.unitgram = unitgram;
	}



	public String getDeliverytime() {
		return deliverytime;
	}

	public void setDeliverytime(String deliverytime) {
		this.deliverytime = deliverytime;
	}

	public String getDeliverydate() {
		return deliverydate;
	}

	public void setDeliverydate(String deliverydate) {
		this.deliverydate = deliverydate;
	}

	public String getDeliverymode() {
		return deliverymode;
	}

	public void setDeliverymode(String deliverymode) {
		this.deliverymode = deliverymode;
	}


	public String getorderfreeuom() {return orderfreeuom;}

	public void setorderfreeuom(String orderfreeuom) {
		this.orderfreeuom = orderfreeuom;
	}

	public String getSelcetedUOM() {return selectedUom;}

	public void setSelcetedUOM(String selectedUom) {
		this.selectedUom = selectedUom;
	}
	public String getSelectedFreeUom() {
		return selectedFreeUom;
	}
	public void setSelectedFreeUom(String SelectedFreeUom) {
		this.selectedFreeUom = SelectedFreeUom;
	}


	public String getUOM()
	{return uom;}

	public void setUOM(String uom) {
		this.uom = uom;
	}

	public String getordimage() {
		return ordimage;
	}

	public void setordimage(String ordimage) {
		this.ordimage = ordimage;
	}

	public String getisread() {
		return isread;
	}

	public void setisread(String isread) {
		this.isread = isread;
	}

	public String getDuserid() {
		return duserid;
	}

	public void setDuserid(String duserid1) {
		this.duserid = duserid1;
	}

	public String getPaymentType() {return paymentType;}

	public void setpaymentType(String paymentType1) {
		this.paymentType = paymentType1;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag1) {
		flag = flag1;
	}

	public String getFreeQty() {
		return freeQty;
	}

	public void setFreeQty(String freeQty1) {
		freeQty = freeQty1;
	}

	public String getOrderid() {
		return Orderid;
	}

	public void setOrderid(String orderid) {
		Orderid = orderid;
	}

	public String getproductname() {
		return prodname;
	}

	public void setproductname(String prodname1) {
		this.prodname = prodname1;
	}

	public String getorddtlid() {
		return orddtlid;
	}

	public void setorddtlid(String orddtlid) {
		this.orddtlid = orddtlid;
	}

	public String getqty() {
		return qty;
	}

	public void setqty(String qty) {
		this.qty = qty;
	}

	public String getprodid() {
		return prodid;
	}

	public void setprodid(String prodtid) {
		prodid = prodtid;
	}

	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}
	
	public String getordstatus() {
		return ordstatus;
	}

	public void setordstatus(String ordtatus) {
		ordstatus = ordtatus;
	}


}
