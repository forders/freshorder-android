package com.freshorders.freshorder.domain;

/**
 * Created by Karthika on 01/03/2018.
 */

public class DistanceCalculationDomain {
    String suserid;
    String duserid;
    String  distance;
    String  amount;
    String createddt;
    String logtime;
    String  geolong;
    String geolat;
    String updateddt;
    String status;
    String smname;
    String traveldistance;
    String usertype;
    String startdt;
    String enddt;
    String mode;

    public String getSmname() {
        return smname;
    }

    public void setSmname(String smname) {
        this.smname = smname;
    }

    public String getTraveldistance() {
        return traveldistance;
    }

    public void setTraveldistance(String traveldistance) {
        this.traveldistance = traveldistance;
    }

    public String getUsertype() {
        return usertype;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

    public String getStartdt() {
        return startdt;
    }

    public void setStartdt(String startdt) {
        this.startdt = startdt;
    }

    public String getEnddt() {
        return enddt;
    }

    public void setEnddt(String enddt) {
        this.enddt = enddt;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getLogtime() {
        return logtime;
    }

    public void setLogtime(String logtime) {
        this.logtime = logtime;
    }

    public String getGeolong() {
        return geolong;
    }

    public void setGeolong(String geolong) {
        this.geolong = geolong;
    }

    public String getGeolat() {
        return geolat;
    }

    public void setGeolat(String geolat) {
        this.geolat = geolat;
    }

    public String getUpdateddt() {
        return updateddt;
    }

    public void setUpdateddt(String updateddt) {
        this.updateddt = updateddt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }



    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }


    public String getSuserid() {
        return suserid;
    }

    public void setSuserid(String suserid) {
        this.suserid = suserid;
    }

    public String getDuserid() {
        return duserid;
    }

    public void setDuserid(String duserid) {
        this.duserid = duserid;
    }


    public String getCreateddt() {
        return createddt;
    }

    public void setCreateddt(String createddt) {
        this.createddt = createddt;
    }


}
