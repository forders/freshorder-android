package com.freshorders.freshorder.domain;

public class DealerAddProductDomain {

    String productName;
    String productID;
    String status;
    String productCode;
    String Dqty;
    String Dfqty;
    String serialno;
    String deliverymode;
    String deliverydate;
    String deliverytime,unitgram, unitperuom,uom,price,tax;

    public String getPrice() {
        return price;
    }
    public void setPrice(String price) {
        this.price = price;
    }

    public String getTax() {
        return tax;
    }
    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getUom() {
        return uom;
    }
    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getUnitPerUom() {
        return unitperuom;
    }
    public void setUnitPerUom(String unitperuom) {
        this.unitperuom = unitperuom;
    }

    public String getUnitGram() {
        return unitgram;
    }
    public void setUnitGram(String unitgram) {
        this.unitgram = unitgram;
    }

    public String getProductCode() {
        return productCode;
    }
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
    public String getProductName() {
        return productName;
    }
    public void setProductName(String productName) {
        this.productName = productName;
    }
    public String getProductID() {
        return productID;
    }
    public void setProductID(String productID) {
        this.productID = productID;
    }
    public String getDqty() {
        return Dqty;
    }
    public void setDqty(String Dqty) {
        this.Dqty = Dqty;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public String getDfqty() {
        return Dfqty;
    }
    public void setDfqty(String Dfqty) {
        this.Dfqty = Dfqty;
    }
    public String getserialno() {
        return serialno;
    }
    public void setserialno(String serialno) {
        this.serialno = serialno;
    }
    public String getDeliverytime() {
        return deliverytime;
    }

    public void setDeliverytime(String deliverytime) {
        this.deliverytime = deliverytime;
    }

    public String getDeliverydate() {
        return deliverydate;
    }

    public void setDeliverydate(String deliverydate) {
        this.deliverydate = deliverydate;
    }

    public String getDeliverymode() {
        return deliverymode;
    }
    public void setDeliverymode(String deliverymode) {
        this.deliverymode = deliverymode;
    }

}
