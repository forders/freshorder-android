package com.freshorders.freshorder.domain;

public class MDealerCompDropdownDomain {
	
	String Name;
	String  comName;
	String ID;
	String mobileno;
	String mflag;
	String email;
	String address;
	private String offlineMerchantId = "";
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getComName() {
		return comName;
	}
	public void setComName(String comName) {
		this.comName = comName;
	}
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}


	public String getmobileno() {
		return mobileno;
	}
	public void setmobileno(String mobileno) {
this.		mobileno = mobileno;
	}



	public String getmflag() {
		return mflag;
	}
	public void setmflag(String mflag) {
		this.mflag = mflag;
	}
	public String getemail() {
		return email;
	}
	public void setemail(String email) {
		this.		email = email;
	}
	public String getaddress() {
		return address;
	}
	public void setaddress(String address) {
		this.address = address;
	}

	public String getOfflineMerchantId() {
		return offlineMerchantId;
	}

	public void setOfflineMerchantId(String offlineMerchantId) {
		this.offlineMerchantId = offlineMerchantId;
	}

	//Kumaravel
	private int merchantRowId = 0;

	public int getMerchantRowId() {
		return merchantRowId;
	}

	public void setMerchantRowId(int merchantRowId) {
		this.merchantRowId = merchantRowId;
	}
}
