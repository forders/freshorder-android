package com.freshorders.freshorder.domain;

public class DealerProductDomain {
	
	String productName;
	String productID;
	String status;
	String productCode;
	String serialno;
	
	
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductID() {
		return productID;
	}
	public void setProductID(String productID) {
		this.productID = productID;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getserialno() {
		return serialno;
	}
	public void setserialno(String serialno) {
		this.serialno = serialno;
	}
	

}
