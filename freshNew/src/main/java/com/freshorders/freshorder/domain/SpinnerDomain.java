package com.freshorders.freshorder.domain;

/**
 * Created by Karthika on 16/04/2018.
 */

public class SpinnerDomain {

        private String title;
        private boolean selected;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public boolean isSelected() {
            return selected;
        }

        public void setSelected(boolean selected) {
            this.selected = selected;
        }

}
