package com.freshorders.freshorder.domain;

/**
 * Created by karthika on 21/8/17.
 */

public class AddEodDomain {
    String prodcode;
    String prodname;
    String qtotal;
    private String salesValue;

    public String getSalesValue() {
        return salesValue;
    }

    public void setSalesValue(String salesValue) {
        this.salesValue = salesValue;
    }

    public String getProdcode() {
        return prodcode;
    }

    public void setProdcode(String prodcode) {
        this.prodcode = prodcode;
    }

    public String getProdname() {
        return prodname;
    }

    public void setProdname(String prodname) {
        this.prodname = prodname;
    }

    public String getQtotal() {
        return qtotal;
    }

    public void setQtotal(String qtotal) {
        this.qtotal = qtotal;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    String uom;
}
