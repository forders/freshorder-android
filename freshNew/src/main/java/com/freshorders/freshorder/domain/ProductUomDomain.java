package com.freshorders.freshorder.domain;

/**
 * Created by Jayesh on 26/04/2017.
 */

public class ProductUomDomain {
    public String getUomdesc() {
        return Uomdesc;
    }

    public void setUomdesc(String uomdesc) {
        Uomdesc = uomdesc;
    }

    public String getUomvalue() {
        return Uomvalue;
    }

    public void setUomvalue(String uomvalue) {
        Uomvalue = uomvalue;
    }

    public String getProdid() {
        return Prodid;
    }

    public void setProdid(String prodid) {
        Prodid = prodid;
    }

    public String getUomid() {
        return Uomid;
    }

    public void setUomid(String uomid) {
        Uomid = uomid;
    }

    String Uomdesc;
    String Uomvalue;
    String Prodid;
    String Uomid;

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    String flag;
}
