package com.freshorders.freshorder.domain;

public class MerchantOrderListDomain {
	String comName;
	String Address;
	String Date;
	String Orderid;
	String fullname;
	String duserid;
	String serialno;
	String isread;

	public String getisread() {
		return isread;
	}
	public void setisread(String isread) {
		this.isread = isread;
	}
	public String getDuserid() {
		return duserid;
	}
	public void setDuserid(String duserid) {
		this.duserid = duserid;
	}
	public String getComName() {
		return comName;
	}
	public void setComName(String comName) {
		this.comName = comName;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getOrderid() {
		return Orderid;
	}
	public void setOrderid(String orderid) {
		Orderid = orderid;
	}
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public String getserialno() {
		return serialno;
	}
	public void setserialno(String serialno) {
		this.serialno = serialno;
	}
}
