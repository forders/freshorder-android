package com.freshorders.freshorder.domain;

/**
 * Created by ragul on 29/06/16.
 */
public class PaymentHistoryDomain {

    String PaymentId;
    String Status;
    String Date;

    public void setDate(String date) {
        Date = date;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public void setPaymentId(String paymentId) {
        PaymentId = paymentId;
    }



    public String getDate() {
        return Date;
    }

    public String getStatus() {
        return Status;
    }

    public String getPaymentId() {
        return PaymentId;
    }



}
