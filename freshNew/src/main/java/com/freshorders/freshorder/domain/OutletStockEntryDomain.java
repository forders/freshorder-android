package com.freshorders.freshorder.domain;

public class OutletStockEntryDomain {
	String usrdlrid;
	String prodid;
	String Date;
	String prodcode;
	String qty;
	String prodname;
	String duserid;
	String companyname;
	String freeQty;
	String serialno;
	String paymentType;
	private int index;
	String prodprice;
	String prodtax;
	String batchno;
	String expdate;
	String mgfdate;
	String uom;
	String pvalue;
	String starttime;
	String endtime;

	public String getUOM()
	{return uom;}

	public void setUOM(String uom) {
		this.uom = uom;
	}

	public String getMgfDate() {return mgfdate;}

	public void setMgfDate(String mgfdate) {
		this.mgfdate = mgfdate;
	}

	public String getExpdate() {return expdate;}

	public void setExpdate(String expdate) {
		this.expdate = expdate;
	}

	public String getStarttime() {return starttime;}

	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}

	public String getEndtime() {return endtime;}

	public void setEndtime(String expdate) {
		this.endtime = endtime;
	}

	public String getPvalue() {return pvalue;}

	public void setPvalue(String pvalue) {
		this.pvalue = pvalue;
	}

	public String getBatchno() {return batchno;}

	public void setBatchno(String batchno) {
		this.batchno = batchno;
	}

	public String getprodprice() {return prodprice;}

	public void setprodprice(String prodprice) {
		this.prodprice = prodprice;
	}

	public String getprodtax() {return prodtax;}

	public void setprodtax(String prodtax) {
		this.prodtax = prodtax;
	}

	public String getPaymentType() {return paymentType;}

	public void setpaymentType(String paymentType1) {
		this.paymentType = paymentType1;
	}

	public String getserialno() {return serialno;}

	public void setserialno(String serialno) {
		this.serialno = serialno;
	}

	public String getFreeQty() {
		return freeQty;
	}

	public void setFreeQty(String freeQty1) {
		freeQty = freeQty1;
	}

	public void setDuserid(String duserid) {
		this.duserid = duserid;
	}

	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}


	public String getCompanyname() {
		return companyname;
	}

	public String getDuserid() {
		return duserid;
	}



	public String getprodcode() {
		return prodcode;
	}

	public void setprodcode(String prodtcode) {
		prodcode = prodtcode;
	}

	public String getusrdlrid() {
		return usrdlrid;
	}

	public void setusrdlrid(String usrdlrid) {
		this.usrdlrid = usrdlrid;
	}

	public String getqty() {
		return qty;
	}

	public void setqty(String qty) {
		this.qty = qty;
	}

	public String getprodid() {
		return prodid;
	}

	public void setprodid(String prodtid) {
		prodid = prodtid;
	}

	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}

	public String getprodname() {
		return prodname;
	}

	public void setprodname(String prodtname) {
		prodname = prodtname;
	}


	public int getindex() {return index;}
	public void setindex(int index) {
		index = index;
	}
}
