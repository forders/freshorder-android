package com.freshorders.freshorder.domain;

public class MerchantOrderDomainSelected {
	String usrdlrid;
	String prodid;
	String Date;
	String prodcode;
	String qty;
	String prodname;
	String userid;
	String companyname;
	String freeQty;
	String serialno;
int index;
	String price;
	String ptax;
	int pvalue;
	String paymentType;
	String batchno;
	String expdate;
	String mgfdate;
	String outstock;
	String uom;
	String orderuom;
	String orderfreeuom,selectedfreeUOM,unitgram, unitperUom,selectedQtyUomUnit,selectedFreeQtyUomUnit;
	String deliverymode;
	String deliverydate;
	String deliverytime;
	String currstock;

	public String getDeliverymode() {
		return deliverymode;
	}

	public void setDeliverymode(String deliverymode) {
		this.deliverymode = deliverymode;
	}

	public String getDeliverydate() {
		return deliverydate;
	}

	public void setDeliverydate(String deliverydate) {
		this.deliverydate = deliverydate;
	}

	public String getCurrStock() {
		return currstock;
	}

	public void setCurrStock(String currstock) {
		this.currstock = currstock;
	}

	public String getDeliverytime() {
		return deliverytime;
	}

	public void setDeliverytime(String deliverytime) {
		this.deliverytime = deliverytime;
	}

	public String getSelectedQtyUomUnit() {return selectedQtyUomUnit;}

	public void setSelecetdQtyUomUnit(String selectedQtyUomUnit) {
		this.selectedQtyUomUnit = selectedQtyUomUnit;
	}
	public String getSelectedFreeQtyUomUnit() {return selectedFreeQtyUomUnit;}

	public void setSelecetdFreeQtyUomUnit(String selectedFreeQtyUomUnit) {
		this.selectedFreeQtyUomUnit = selectedFreeQtyUomUnit;
	}

	public String getUnitPerUom() {return unitperUom;}

	public void setUnitPerUom(String unitperUom) {
		this.unitperUom = unitperUom;
	}

	public String getUnitGram() {return unitgram;}

	public void setUnitGram(String unitgram) {
		this.unitgram = unitgram;
	}


	public String getselectedfreeUOM() {
		return selectedfreeUOM;}

	public void setselectedfreeUOM(String selectedfreeUOM) {
		this.selectedfreeUOM = selectedfreeUOM;
	}

	public String getorderfreeuom() {return orderfreeuom;}

	public void setorderfreeuom(String orderfreeuom) {
		this.orderfreeuom = orderfreeuom;
	}

	public String getorderuom() {return orderuom;}

	public void setorderuom(String orderuom) {
		this.orderuom = orderuom;
	}

	public String getUOM() {return uom;}

	public void setUOM(String uom) {
		this.uom = uom;
	}

    public String getOutstock() {return outstock;}

    public void setOutstock(String outstock) {
        this.outstock = outstock;
    }


	public String getMgfDate() {return mgfdate;}

	public void setMgfDate(String mgfdate) {
		this.mgfdate = mgfdate;
	}

	public String getExpdate() {return expdate;}

	public void setExpdate(String expdate) {
		this.expdate = expdate;
	}

	public String getBatchno() {return batchno;}

	public void setBatchno(String batchno) {
		this.batchno = batchno;
	}


	public String getprice() {return price;}

	public void setprice(String price) {
		this.price = price;
	}
	public String getptax() {return ptax;}

	public void setptax(String ptax) {
		this.ptax = ptax;
	}
	public int getpvalue() {return pvalue;}

	public void setpvalue(int pvalue) {
		this.pvalue = pvalue;
	}

	public String getserialno() {return serialno;}

	public void setserialno(String serialno) {
		this.serialno = serialno;
	}

	public int getindex() {return index;}

	public void setindex(int index1) {
		this.index = index1;
	}

	public String getPaymentType() {return paymentType;}

	public void setpaymentType(String paymentType1) {
		this.paymentType = paymentType1;
	}

	public String getFreeQty() {
		return freeQty;
	}

	public void setFreeQty(String freeQty1) {
		freeQty = freeQty1;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public void setCompanyname(String companyname) {
		this.companyname = companyname;
	}



	public String getUserid() {
		return userid;
	}

	public String getCompanyname() {
		return companyname;
	}


	public String getprodcode() {
		return prodcode;
	}

	public void setprodcode(String prodtcode) {
		prodcode = prodtcode;
	}

	public String getusrdlrid() {
		return usrdlrid;
	}

	public void setusrdlrid(String usrdlrid) {
		this.usrdlrid = usrdlrid;
	}

	public String getqty() {
		return qty;
	}

	public void setqty(String qty) {
		this.qty = qty;
	}

	public String getprodid() {
		return prodid;
	}

	public void setprodid(String prodtid) {
		prodid = prodtid;
	}

	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}
	
	public String getprodname() {
		return prodname;
	}

	public void setprodname(String prodtname) {
		prodname = prodtname;
	}


}
