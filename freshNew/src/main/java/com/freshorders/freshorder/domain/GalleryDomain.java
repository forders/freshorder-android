package com.freshorders.freshorder.domain;


public class GalleryDomain {
	String newsId,company_images,gallery_content,gallery_title,status,lastupdatedDate,updatedBy;
	
	
	public String getImages() {
		return company_images;
	}

	public void setImages(String company_images) {
		this.company_images = company_images;
	}
	

	public String getNewsId() {
		return newsId;
	}

	public void setNewsId(String newsId) {
		this.newsId = newsId;
	}

	public String getGalleryContent() {
		return gallery_content;
	}

	public void setGalleryContent(String gallery_content) {
		this.gallery_content = gallery_content;
	}
	
	public String getGalleryTitle() {
		return gallery_title;
	}

	public void setGalleryTitle(String gallery_title) {
		this.gallery_title = gallery_title;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getUpdatedDate() {
		return lastupdatedDate;
	}

	public void setUpdatedDate(String lastupdatedDate) {
		this.lastupdatedDate = lastupdatedDate;
	}
	
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdateBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}


}
