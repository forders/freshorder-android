package com.freshorders.freshorder.domain;

/**
 * Created by ragul on 19/08/16.
 */
public class ViewPagerDomain {
    String image;
    String date;
    String status;

    public void setImage(String image1) {
        this.image = image1;
    }

    public String getImage() {
        return image;
    }

    public void setDate(String date1) {
        this.date = date1;
    }

    public String getDate() {
        return date;
    }

    public void setStatus(String status1) {
        this.status = status1;
    }

    public String getStatus() {
        return status;
    }



}
