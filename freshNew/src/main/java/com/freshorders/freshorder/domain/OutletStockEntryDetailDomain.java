package com.freshorders.freshorder.domain;

public class OutletStockEntryDetailDomain {


	String orddtlid;
	String prodid;
	String Date;
	String Orderid;
	String qty;
	String ordstatus;
	String prodcode;
	String prodname;
	String freeQty;
	String flag;
	String duserid;
	String paymentType;
	String stock;
	String batchno;
	String manufdt;

	public String getOrderdate() {
		return orderdate;
	}

	public void setOrderdate(String orderdate) {
		this.orderdate = orderdate;
	}

	String orderdate;
	String expdate;

	public String getmanufdt() {
		return manufdt;
	}
	public void setmanufdt(String manufdt) {
		this.manufdt = manufdt;
	}

	public String getexpdate() {
		return expdate;
	}
	public void setexpdate(String expdate) {
		this.expdate = expdate;
	}

	public String getbatchno() {
		return batchno;
	}
	public void setbatchno(String batchno) {
		this.batchno = batchno;
	}

	public String getstock() {
		return stock;
	}
	public void setstock(String stock) {
		this.stock = stock;
	}

	public String getprodcode() {
		return prodcode;
	}
	public void setprodcode(String prodcode) {
		this.prodcode = prodcode;
	}

	public String getDuserid() {
		return duserid;
	}

	public void setDuserid(String duserid1) {
		this.duserid = duserid1;
	}

	public String getPaymentType() {return paymentType;}

	public void setpaymentType(String paymentType1) {
		this.paymentType = paymentType1;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag1) {
		flag = flag1;
	}

	public String getFreeQty() {
		return freeQty;
	}

	public void setFreeQty(String freeQty1) {
		freeQty = freeQty1;
	}

	public String getOrderid() {
		return Orderid;
	}

	public void setOrderid(String orderid) {
		Orderid = orderid;
	}

	public String getproductname() {
		return prodname;
	}

	public void setproductname(String prodname1) {
		this.prodname = prodname1;
	}

	public String getorddtlid() {
		return orddtlid;
	}

	public void setorddtlid(String orddtlid) {
		this.orddtlid = orddtlid;
	}

	public String getqty() {
		return qty;
	}

	public void setqty(String qty) {
		this.qty = qty;
	}

	public String getprodid() {
		return prodid;
	}

	public void setprodid(String prodtid) {
		prodid = prodtid;
	}

	public String getDate() {
		return Date;
	}

	public void setDate(String date) {
		Date = date;
	}
	
	public String getordstatus() {
		return ordstatus;
	}

	public void setordstatus(String ordtatus) {
		ordstatus = ordtatus;
	}


}
