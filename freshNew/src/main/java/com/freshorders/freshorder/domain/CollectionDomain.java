package com.freshorders.freshorder.domain;

/**
 * Created by Pavithra on 27-10-2016.
 */
public class CollectionDomain {

    String billno;
    String billDate;
    String billAmount;
    Double balncAmount;
    String billid;

    public void setBillno(String billno) {
        this.billno = billno;
    }
    public String getBillno() {
        return billno;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }
    public String getBillDate() {
        return billDate;
    }

    public void setBillAmount(String billAmount) {
        this.billAmount = billAmount;
    }
    public String getBillAmount() {
        return billAmount;
    }

    public void setBalncAmount(Double balncAmount) {
        this.balncAmount = balncAmount;
    }
    public Double getBalncAmount() {
        return balncAmount;
    }

    public void setbillid(String billid) {
        this.billid = billid;
    }
    public String getbillid() {
        return billid;
    }
}
