package com.freshorders.freshorder;


import com.freshorders.freshorder.ui.PaymentActivity;
import com.freshorders.freshorder.ui.SignupActivity;
import com.google.android.gcm.GCMBaseIntentService;

import static com.freshorders.freshorder.CommonUtilities.SENDER_ID;
import static com.freshorders.freshorder.CommonUtilities.displayMessage;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import androidx.core.app.NotificationCompat;
import android.util.Log;

public class GCMIntentService extends GCMBaseIntentService {

	private static final String TAG = "GCMIntentService";

    public GCMIntentService() {
        super(SENDER_ID);
    }

    /**
     * Method called on device registered
     **/
    @Override
    protected void onRegistered(Context context, String registrationId) {
        Log.i(TAG, "Device registered: regId = " + registrationId);
        displayMessage(context, "Your device registred with GCM");
        //Log.d("NAME", MainNotification.name);
        ServerUtilities.register(context, SignupActivity.name, SignupActivity.email, registrationId);
    }

    /**
     * Method called on device un registred
     * */
    @Override
    protected void onUnregistered(Context context, String registrationId) {
        Log.i(TAG, "Device unregistered");
        displayMessage(context, getString(R.string.gcm_unregistered));
        ServerUtilities.unregister(context, registrationId);
    }

    /**
     * Method called on Receiving a new message
     * */
    @Override
    protected void onMessage(Context context, Intent intent) {
        Log.i(TAG, "Received message");
        String message = intent.getExtras().getString("price");
        
        displayMessage(context, message);
        // notifies user
        generateNotification(context, message);
    }

    /**
     * Method called on receiving a deleted message
     * */
    @Override
    protected void onDeletedMessages(Context context, int total) {
        Log.i(TAG, "Received deleted messages notification");
        String message = getString(R.string.gcm_deleted, total);
        displayMessage(context, message);
        // notifies user
        generateNotification(context, message);
    }

    /**
     * Method called on Error
     * */
    @Override
    public void onError(Context context, String errorId) {
        Log.i(TAG, "Received error: " + errorId);
        displayMessage(context, getString(R.string.gcm_error, errorId));
    }

    @Override
    protected boolean onRecoverableError(Context context, String errorId) {
        // log message
        Log.i(TAG, "Received recoverable error: " + errorId);
        displayMessage(context, getString(R.string.gcm_recoverable_error,
                errorId));
        return super.onRecoverableError(context, errorId);
    }

    /**
     * Issues a notification to inform the user that server has sent a message.
     */
    private static void generateNotification(Context context, String message) {
//        int icon = R.drawable.launchicon_white;
//        long when = System.currentTimeMillis();
//        NotificationManager notificationManager = (NotificationManager)
//                context.getSystemService(Context.NOTIFICATION_SERVICE);
//        Notification notification = new Notification(icon, message, when);
//        
//        String title = context.getString(R.string.app_name);
//        
//        Intent notificationIntent = new Intent(context, MainActivity.class);
//        // set intent so it does not start a new activity
//        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
//                Intent.FLAG_ACTIVITY_SINGLE_TOP);
//        PendingIntent intent =
//                PendingIntent.getActivity(context, 0, notificationIntent, 0);
//        notification.setLatestEventInfo(context, title, message, intent);
//        notification.flags |= Notification.FLAG_AUTO_CANCEL;
//        
//        // Play default notification sound
//        notification.defaults |= Notification.DEFAULT_SOUND;
//        
//        //notification.sound = Uri.parse("android.resource://" + context.getPackageName() + "your_sound_file_name.mp3");
//        
//        // Vibrate if vibrate is enabled
//        notification.defaults |= Notification.DEFAULT_VIBRATE;
//        try {
//			notificationManager.notify(0, notification);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}    
    	
    	int icon = R.drawable.btn_crop_pressed;
    	 
    	int mNotificationId = 001;
    	String title = context.getString(R.string.app_name);
    	Intent intent = new Intent(context, PaymentActivity.class);
    	PendingIntent resultPendingIntent =
    	        PendingIntent.getActivity(
    	        		context,
    	                0,
    	                intent,
    	                PendingIntent.FLAG_CANCEL_CURRENT
    	        );
    	 
    	NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
    			context);
    	Notification notification = mBuilder.setSmallIcon(icon).setTicker(title).setWhen(0)
    	        .setAutoCancel(true)
    	        .setContentTitle(title)
    	        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
    	        .setContentIntent(resultPendingIntent)
    	        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
    	        .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.appicon_blue))
    	        .setContentText(message).build();
    	 
    	NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    	notificationManager.notify(mNotificationId, notification);

    }

}
