package com.freshorders.freshorder.http;

public class Response {
    public int statusCode;

    public String message;

    public String response;

    @Override
    public String toString() {
        return  "statusCode :  " + statusCode+
                "message    :  " + message+
                "response   :  " + response;
    }
}
