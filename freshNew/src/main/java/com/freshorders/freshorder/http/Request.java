package com.freshorders.freshorder.http;

import org.json.JSONObject;

import java.util.HashMap;

import okhttp3.RequestBody;

public class Request {
    private int mMethod;

    private final String mUrl;

    private HashMap<String, String> mParams;

    private HashMap<String, String> mHeaders = new HashMap<>();

    private JSONObject mJsonData = null;

    private int mRequestType;

    public static final int REQUEST_DEFAULT = 0;

    private String mRequestBody;

    private  String mRequestMethod;

    public int getRequestType() {
        return mRequestType;
    }

    public interface Method {
        int GET = 0;
        int POST = 1;
    }

    public Request(final String url, final int method, final int requestType) {
        if (url == null || url.trim().length() == 0) {
            throw new IllegalArgumentException("url should not be null");
        }
        this.mUrl = url;
        this.mMethod = method;
        this.mRequestType = requestType;
    }

    public void setParams(HashMap<String, String> params) {
        mParams = params;
    }

    public void setRequestBody(final String requestBody) {
        if (requestBody != null) {
            mRequestBody = requestBody;
        }
    }

    public String getRequestBody() {
        return mRequestBody;
    }

    public String getUrl() {
        return mUrl;
    }

    public int getMethod() {
        return mMethod;
    }

    public String getRequestMethod() {
        return mRequestMethod;
    }

    public void setRequestMethod(String requestMethod) {
        mRequestMethod = requestMethod;
    }

    public HashMap<String, String> addHeader(final String key,final String value) {
        mHeaders.put(key,value);
        return mHeaders;
    }
    public HashMap<String, String> addUserAgentHeader() {
        return mHeaders;
    }

    public HashMap<String, String> getHeaders() {
        mHeaders.put("Content-Type","application/json");
        return mHeaders;
    }

    public HashMap<String, String> getParams() {
        return mParams;
    }

    public void setRequestData(final JSONObject data){
        this.mJsonData = data;
    }

    public JSONObject getRequestData(){
        return mJsonData;
    }
}
