package com.freshorders.freshorder.http;

import android.text.TextUtils;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Map;

public class VolleySingleton {

    public static final String TAG = MyApplication.class.getSimpleName();

    private static final int TIME_OUT = 60000;

    private String req_tag = ""; // default value

    public interface ResponseListener {
        void onSuccess(final Response res);

        void onFailure(final Response res);
    }

    private static VolleySingleton INSTANCE;

    private RequestQueue mRequestQueue;

    private VolleySingleton() {
        mRequestQueue = Volley.newRequestQueue(MyApplication.getInstance().getApplicationContext());
    }

    private VolleySingleton(String req_tag) {
        mRequestQueue = Volley.newRequestQueue(MyApplication.getInstance().getApplicationContext());

    }

    public synchronized static VolleySingleton getInstance() {

        if (INSTANCE == null) {
            INSTANCE = new VolleySingleton();
        }
        return INSTANCE;
    }

    // Kumaravel 20/01/2019

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(MyApplication.getInstance().getApplicationContext());
        }
        mRequestQueue.addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
            @Override
            public void onRequestFinished(com.android.volley.Request<Object> request) {
                try {
                    Log.e(TAG,"getOriginUrl : " + request.getOriginUrl());
                    //Log.e(TAG,"getUrl : " + request.getUrl());
                    //Log.e(TAG,"getUrl : " + request.getBody().toString());

                    if (request.getCacheEntry() != null) {
                        String cacheValue = new String(request.getCacheEntry().data, "UTF-8");
                        Log.d(TAG, request.getCacheKey() + " " + cacheValue);
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(com.android.volley.Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(com.android.volley.Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }



    public void connect(final Request request,
                        final ResponseListener responseListener) throws Exception{
        if (request.getRequestType() == Request.REQUEST_DEFAULT) {


            StringRequest stringRequest = new StringRequest(request.getMethod(), request.getUrl(),
                    new com.android.volley.Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            final Response res = new Response(); //Change by Kumaravel
                            try {
                                final JSONObject resObj = new JSONObject(response);
                                res.response = resObj.optString("status");
                                res.message = resObj.optString("message");
                                responseListener.onSuccess(res);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                res.response = "Unknown";
                                res.message = "Unknown";
                                responseListener.onSuccess(res);
                            }

                        }
                    }, new com.android.volley.Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    String errorMsg = "UnKnown Error";
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                        errorMsg = Constants.TIME_OUT_ERROR;
                        //showSignOutAlertDialog(context, "TimeoutError");

                    } else if (error instanceof AuthFailureError) {
                        errorMsg = Constants.AUTH_FAILURE_ERROR;

                    } else if (error instanceof ServerError) {

                        errorMsg = Constants.SERVER_ERROR;

                    } else if (error instanceof NetworkError) {

                        errorMsg = Constants.NETWORK_ERROR;
                    } else if (error instanceof ParseError) {

                        errorMsg = Constants.PARSE_ERROR;
                    }else {
                        if(error.getMessage() != null && !error.getMessage().isEmpty()){
                            errorMsg = error.getMessage();
                        }
                    }

                    final Response response = new Response();
                    response.message = errorMsg;
                    responseListener.onFailure(response);

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return request.getHeaders() != null ? request.getHeaders() : super.getHeaders();
                }

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    return request.getParams() != null ? request.getParams() : super.getParams();
                }

                @Override
                public byte[] getBody() throws AuthFailureError {
                    try {
                        return request.getRequestBody() == null ? null : request.getRequestBody().getBytes("utf-8");
                    } catch (UnsupportedEncodingException uee) {
                        VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", request.getRequestBody(), "utf-8");
                        return null;
                    }
                }

                @Override
                protected com.android.volley.Response<String> parseNetworkResponse(NetworkResponse response) {
                    int mStatusCode = response.statusCode;
                    System.out.println("Status code is===>"+mStatusCode);
                    return super.parseNetworkResponse(response);
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 3,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            if(req_tag.isEmpty()) {
                mRequestQueue.add(stringRequest);
            }else {
                addToRequestQueue(stringRequest, req_tag);
            }
        }
    }

}
