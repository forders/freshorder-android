package com.freshorders.freshorder.worker;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.utils.SendFCMTokenUtil;

import static com.freshorders.freshorder.service.PrefManager.FCM_TOKEN;
import static com.freshorders.freshorder.service.PrefManager.IS_DELIVERED_FCM_TOKEN;

public class FCMTokenUploadWorker extends Worker {

    public FCMTokenUploadWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {

        Context mContext = getApplicationContext();

        PrefManager pre = new PrefManager(mContext);
        boolean isDeliveredFCMToken = pre.getBooleanDataByKey(IS_DELIVERED_FCM_TOKEN);
        String token = pre.getStringDataByKey(FCM_TOKEN);

        SendFCMTokenUtil call = new SendFCMTokenUtil(mContext, new SendFCMTokenUtil.ITokenUploadListener() {
            @Override
            public Result response(boolean isSuccess) {
                if(isSuccess){
                    return Worker.Result.success();
                }
                return Worker.Result.failure();
            }
        });

        if(!isDeliveredFCMToken){
            if(token.length() > 0){
                call.sendFCMToken();
            }else {
                //// Refresh token, get and send
                call.getToken();
            }
        }

        return Worker.Result.success();
    }
}
