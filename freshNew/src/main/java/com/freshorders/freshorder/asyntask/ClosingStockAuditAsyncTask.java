package com.freshorders.freshorder.asyntask;

import android.os.AsyncTask;
import android.util.Log;

import com.freshorders.freshorder.model.StockAuditDisplayModel;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.util.List;

public class ClosingStockAuditAsyncTask {

    public interface GetAuditCompleteListener{
        void onSuccess(List<StockAuditDisplayModel> object);
        void onFailed(String msg);
        void onException(String msg);
    }

    private JsonServiceHandler jsonServiceHandler;
    private JSONObject jsonObject;
    private ClosingStockAuditAsyncTask.GetAuditCompleteListener listener;

    public ClosingStockAuditAsyncTask(JsonServiceHandler jsonServiceHandler,
                                      ClosingStockAuditAsyncTask.GetAuditCompleteListener listener) {
        this.jsonServiceHandler = jsonServiceHandler;
        this.listener = listener;
    }

    public void execute(){
        new ClosingStockAuditAsyncTask.AuditAsyncTask(jsonServiceHandler, listener).execute();
    }

    public static class AuditAsyncTask extends AsyncTask<Void, Void, JSONObject> {

        private JsonServiceHandler jsonServiceHandler;
        private ClosingStockAuditAsyncTask.GetAuditCompleteListener listener;

        public AuditAsyncTask(JsonServiceHandler jsonServiceHandler,
                              ClosingStockAuditAsyncTask.GetAuditCompleteListener listener) {
            this.jsonServiceHandler = jsonServiceHandler;
            this.listener = listener;
        }

        @Override
        protected JSONObject doInBackground(Void... voids) {
            Log.e("AAAAAAAAAAAA","''''''''''''''''''''''''''''''''''......................");
            //return jsonServiceHandler.ServiceData();
            return jsonServiceHandler.ServiceDataModifiedGet();
        }

        @Override
        protected void onPostExecute(JSONObject result) {

            try {
                if (result != null) {
                    String strMsg;
                    String strStatus;
                    if(result.has("status")) {
                        strStatus = result.getString("status");
                    }else {
                        listener.onException("No response from Server");
                        return;
                    }
                    if(result.has("status")) {
                        strMsg = result.getString("message");
                    }else {
                        listener.onException("No response from server");
                        return;
                    }
                    Log.e("return status", strStatus);
                    Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {
                        Log.e("Answer",".........................." + result.toString());
                        List<StockAuditDisplayModel> resultObj =
                                new Gson().fromJson(result.getJSONArray("data").toString(),
                                        new TypeToken<List<StockAuditDisplayModel>>(){}.getType());

                        listener.onSuccess(resultObj);
                        return;
                    }else {
                        listener.onFailed(strMsg);
                        return;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                listener.onException(e.getMessage());
            }
        }
    }
}
