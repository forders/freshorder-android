package com.freshorders.freshorder.asyntask;

import android.os.AsyncTask;
import android.util.Log;

import com.freshorders.freshorder.utils.JsonServiceHandler;

import org.json.JSONObject;

public class ManagerClientManagement {

    public interface ClientManagementCompleteListener{
        void onSuccess(JSONObject object);
        void onFailed(String msg);
        void onException(String msg);
    }

    private JsonServiceHandler jsonServiceHandler;
    private JSONObject jsonObject;
    private ManagerClientManagement.ClientManagementCompleteListener listener;

    public ManagerClientManagement(JsonServiceHandler jsonServiceHandler,
                                   ClientManagementCompleteListener listener) {
        this.jsonServiceHandler = jsonServiceHandler;
        this.listener = listener;
    }

    public void execute(){
        new ManagerClientManagementAsyncTask(jsonServiceHandler, listener).execute();
    }

    public static class ManagerClientManagementAsyncTask extends AsyncTask<Void, Void, JSONObject> {

        private JsonServiceHandler jsonServiceHandler;
        private ManagerClientManagement.ClientManagementCompleteListener listener;

        public ManagerClientManagementAsyncTask(JsonServiceHandler jsonServiceHandler,
                                                ClientManagementCompleteListener listener) {
            this.jsonServiceHandler = jsonServiceHandler;
            this.listener = listener;
        }

        @Override
        protected JSONObject doInBackground(Void... voids) {
            Log.e("AAAAAAAAAAAA","''''''''''''''''''''''''''''''''''......................");
            return jsonServiceHandler.ServiceData();
            //return jsonServiceHandler.ServiceDataModifiedGet();
        }

        @Override
        protected void onPostExecute(JSONObject result) {

            try {
                if (result != null) {
                    String strMsg;
                    String strStatus;
                    if(result.has("status")) {
                        strStatus = result.getString("status");
                    }else {
                        listener.onException("No response from Server");
                        return;
                    }
                    if(result.has("status")) {
                        strMsg = result.getString("message");
                    }else {
                        listener.onException("No response from server");
                        return;
                    }
                    Log.e("return status", strStatus);
                    Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {
                        Log.e("Answer",".........................." + result.toString());
                        listener.onSuccess(result);
                        return;
                    }else {
                        listener.onFailed(strMsg);
                        return;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                listener.onException(e.getMessage());
            }
        }
    }
}
