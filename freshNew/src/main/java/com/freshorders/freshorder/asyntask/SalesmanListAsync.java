package com.freshorders.freshorder.asyntask;

import android.os.AsyncTask;
import android.util.Log;

import com.freshorders.freshorder.utils.JsonServiceHandler;

import org.json.JSONObject;

public class SalesmanListAsync {

    public interface SalesmanListCompleteListener{
        void onSuccess(JSONObject result);
        void onFailed(String msg);
        void onException(String msg);
    }

    private JsonServiceHandler jsonServiceHandler;
    private JSONObject jsonObject;
    private SalesmanListAsync.SalesmanListCompleteListener listener;

    public SalesmanListAsync(JsonServiceHandler jsonServiceHandler,
                             SalesmanListAsync.SalesmanListCompleteListener listener) {
        this.jsonServiceHandler = jsonServiceHandler;
        this.listener = listener;
    }

    public void execute(){
        new SalesmanListAsync.SalesmanListAsyncTask(jsonServiceHandler, listener).execute();
    }

    public static class SalesmanListAsyncTask extends AsyncTask<Void, Void, JSONObject> {

        private JsonServiceHandler jsonServiceHandler;
        private SalesmanListAsync.SalesmanListCompleteListener listener;

        public SalesmanListAsyncTask(JsonServiceHandler jsonServiceHandler,
                                     SalesmanListAsync.SalesmanListCompleteListener listener) {
            this.jsonServiceHandler = jsonServiceHandler;
            this.listener = listener;
        }

        @Override
        protected JSONObject doInBackground(Void... voids) {
            Log.e("AAAAAAAAAAAA","''''''''''''''''''''''''''''''''''......................");
            return jsonServiceHandler.ServiceData();
        }

        @Override
        protected void onPostExecute(JSONObject result) {

            try {
                if (result != null) {
                    String strMsg;
                    String strStatus;
                    if(result.has("status")) {
                        strStatus = result.getString("status");
                    }else {
                        listener.onException("No response from Server");
                        return;
                    }
                    if(result.has("message")) {
                        strMsg = result.getString("message");
                    }else {
                        listener.onException("No response from server");
                        return;
                    }
                    Log.e("return status", strStatus);
                    Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {
                        Log.e("Answer",".........................." + result.toString());
                        listener.onSuccess(result);
                        return;
                    }else {
                        listener.onFailed("No value from server");
                        return;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                listener.onException(e.getMessage());
            }
        }

    }

}
