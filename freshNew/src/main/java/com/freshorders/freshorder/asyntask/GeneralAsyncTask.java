package com.freshorders.freshorder.asyntask;

import android.os.AsyncTask;
import android.util.Log;

import com.freshorders.freshorder.utils.JsonServiceHandler;

import org.json.JSONObject;

public class GeneralAsyncTask {

    public interface CompleteListener{
        void onSuccess(JSONObject result);
        void onFailed(String msg);
        void onException(String msg);
    }

    private JsonServiceHandler jsonServiceHandler;
    private JSONObject jsonObject;
    private GeneralAsyncTask.CompleteListener listener;

    public GeneralAsyncTask(JsonServiceHandler jsonServiceHandler,
                            GeneralAsyncTask.CompleteListener listener) {
        this.jsonServiceHandler = jsonServiceHandler;
        this.listener = listener;
    }

    public void execute(){
        new GeneralAsyncTask.MyAsyncTask(jsonServiceHandler, listener).execute();
    }

    public static class MyAsyncTask extends AsyncTask<Void, Void, JSONObject> {

        private JsonServiceHandler jsonServiceHandler;
        private GeneralAsyncTask.CompleteListener listener;

        public MyAsyncTask(JsonServiceHandler jsonServiceHandler,
                           GeneralAsyncTask.CompleteListener listener) {
            this.jsonServiceHandler = jsonServiceHandler;
            this.listener = listener;
        }

        @Override
        protected JSONObject doInBackground(Void... voids) {
            Log.e("MyAsyncTask","''''''''''''''''''''''''''''''''''..............Trig........");
            try {
                return jsonServiceHandler.ServiceData();
            }catch (Exception e){
                e.printStackTrace();
                listener.onException("Some think wrong " + e.getMessage());
                return null;
            }
        }

        @Override
        protected void onPostExecute(JSONObject result) {

            try {
                if (result != null) {
                    String strMsg;
                    String strStatus;
                    if(result.has("status")) {
                        strStatus = result.getString("status");
                    }else {
                        listener.onException("No response from Server");
                        return;
                    }
                    Log.e("return status", strStatus);
                    if (strStatus.equals("true")) {
                        listener.onSuccess(result);
                    }else {
                        listener.onFailed("No value from server");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                listener.onException(e.getMessage());
            }
        }

    }

}
