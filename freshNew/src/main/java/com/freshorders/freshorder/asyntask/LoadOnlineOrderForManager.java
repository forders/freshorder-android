package com.freshorders.freshorder.asyntask;

import android.content.ContentValues;
import android.os.AsyncTask;

import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import android.util.Log;
import com.freshorders.freshorder.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class LoadOnlineOrderForManager {

    private static final String TAG = LoadOnlineOrderForManager.class.getSimpleName();

    public static String datePattern = "yyyy-MM-dd";
    private static int currentRow = 0;


    public interface OrderCompleteListener {
        void onLoadFinished(JSONArray result);

        void onLoadFailed(String msg);

        void onError(String error);
    }

    public static class LoadOrderByDateAsyncTask extends AsyncTask<Void, Void, String> {

        private com.freshorders.freshorder.utils.JsonServiceHandler JsonServiceHandler;
        private JSONObject JsonAccountObject;
        private LoadOnlineOrderForManager.OrderCompleteListener listener;
        private DatabaseHandler databaseHandler;
        private com.freshorders.freshorder.utils.JsonServiceHandler jsonServiceHandlerGet;
        String dUserId;
        private JSONObject finalResult;


        public LoadOrderByDateAsyncTask(com.freshorders.freshorder.utils.JsonServiceHandler jsonServiceHandler,
                                        JsonServiceHandler jsonServiceHandlerGet,
                                        String dUserId,
                                        LoadOnlineOrderForManager.OrderCompleteListener listener) {
            JsonServiceHandler = jsonServiceHandler;
            JsonAccountObject = new JSONObject();
            this.jsonServiceHandlerGet = jsonServiceHandlerGet;
            this.dUserId = dUserId;
            this.listener = listener;
            //this.databaseHandler = databaseHandler;
        }

        @Override
        protected String doInBackground(Void... voids) {
            /*if(android.os.Debug.isDebuggerConnected())
                android.os.Debug.waitForDebugger();  */
            String strMsg;
            String strStatus;
            String currentOrderId = "";
            try {
                JsonAccountObject = JsonServiceHandler.ServiceData();
                if(JsonAccountObject.has("status")) {
                    strStatus = JsonAccountObject.getString("status");
                    if (strStatus.equals("true")) {
                        if(JsonAccountObject.has("data")) {
                            JSONArray resultArray = JsonAccountObject.getJSONArray("data");
                            if(resultArray.length() > 0){
                                int resultLength = resultArray.length();
                                Log.e(TAG, "...........OrderList." + resultArray.toString());
                                JSONObject currentObj = resultArray.getJSONObject(resultLength-1);
                                currentOrderId = currentObj.getString("ordid");
                                //Forming url
                                String url = Utils.urlPrefix +
                                        "orderdetails?filter[where][ordid]="+currentOrderId+"&filter[where][duserid]="+dUserId;
                                jsonServiceHandlerGet.setURL(url);
                                finalResult = jsonServiceHandlerGet.ServiceDataGet();
                                if(finalResult.has("status")){
                                    strStatus = JsonAccountObject.getString("status");
                                    if (strStatus.equals("true")) {
                                        if(finalResult.has("data")) {
                                            JSONArray finalArray = finalResult.getJSONArray("data");
                                            if(finalArray.length() > 0){
                                                if (listener != null)
                                                    listener.onLoadFinished(finalArray);
                                                return "Success";
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }
                }

            }catch (Exception e){
                e.printStackTrace();
                if (listener != null)
                    listener.onError(e.getMessage());
                return null;
            }
            return "";
        }

        @Override
        protected void onPostExecute(String result) {

            try {
                if(result.isEmpty()){
                    listener.onLoadFailed("No data found for this date");
                }

            } catch (Exception e) {
                e.printStackTrace();
                if (listener != null)
                    listener.onError(e.getMessage());
            }
        }

    }
}