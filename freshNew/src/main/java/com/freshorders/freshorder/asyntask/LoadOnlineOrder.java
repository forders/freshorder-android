package com.freshorders.freshorder.asyntask;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.ui.SigninActivity;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import eu.janmuller.android.simplecropimage.Util;

import static com.freshorders.freshorder.db.DatabaseHandler.KEY_IS_ORDER_PROCESSED;
import static com.freshorders.freshorder.db.DatabaseHandler.KEY_ORDER_INFO_ORDER_ID;
import static com.freshorders.freshorder.db.DatabaseHandler.KEY_SNF_CALC__ID;

public class LoadOnlineOrder {

    public static String datePattern = "yyyy-MM-dd";
    private static int currentRow = 0;


    public interface TodayOrderListener{
        void onLoadFinished();
        void onLoadFailed(String msg);
    }


    public static class LoadTodayOrderAsyncTask extends AsyncTask<Void, Void, String> {

        private com.freshorders.freshorder.utils.JsonServiceHandler JsonServiceHandler;
        private JSONObject JsonAccountObject;
        private LoadOnlineOrder.TodayOrderListener listener;
        private DatabaseHandler databaseHandler;
        private List<ContentValues> orderHeaderList = new ArrayList<>();
        private List<ContentValues> orderDetailList = new ArrayList<>();
        private List<ContentValues> orderInfoList = new ArrayList<>();


        public LoadTodayOrderAsyncTask(DatabaseHandler databaseHandler, JsonServiceHandler jsonServiceHandler,
                                       LoadOnlineOrder.TodayOrderListener listener) {
            JsonServiceHandler = jsonServiceHandler;
            JsonAccountObject = new JSONObject();
            this.listener = listener;
            this.databaseHandler = databaseHandler;
            setURL();
        }

        private void setURL(){
            try {
                Calendar c = Calendar.getInstance();
                Date date = new Date();
                String url = Utils.urlPrefix +
                        "orders?saleslist=Y&filter[where][suserid]=" +
                        Constants.USER_ID + "&filter[where][orderdt][between][0]=" +
                        getStartDate(date)+ "%20" + URLEncoder.encode("00:00") +
                        "&filter[where][orderdt][between][2]=" + currentDateAndTime(date) + "%20" + URLEncoder.encode("23:59");
                /*
                String url = Utils.urlPrefix +
                        "orders?saleslist=Y&filter[where][suserid]=" +
                        Constants.USER_ID + "&filter[where][orderdt][between][0]=" +
                        URLEncoder.encode(getStartDate(date)) + "&filter[where][orderdt][between][2]=" + URLEncoder.encode(currentDateAndTime(date));
                        */
                /*
                String url = Utils.urlPrefix +
                        "orders?saleslist=Y&filter[where][suserid]=" +
                        Constants.USER_ID + "&filter[where][orderdt][between][0]=" +
                        getStartDate(date) + "&filter[where][orderdt][between][2]=" + currentDateAndTime(date); */
                Log.e("FormedURL", "................." + url);
                ///String URL = URLEncoder.encode(url, "UTF-8");
                ///Log.e("FormedURL", "...............URL.." + URL);
                ///JsonServiceHandler.setURL(URL);
                JsonServiceHandler.setURL(url);
            }catch (Exception e){
                e.printStackTrace();
                if(listener != null)
                    listener.onLoadFailed("Went Wrong.." + e.getMessage());
            }
        }

        @Override
        protected String doInBackground(Void... voids) {
            /*if(android.os.Debug.isDebuggerConnected())
                android.os.Debug.waitForDebugger();  */
            JsonAccountObject = JsonServiceHandler.ServiceDataGet();
            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            try {

                String strStatus = JsonAccountObject.getString("status");
                Log.e("return status", strStatus);
                String strMsg = JsonAccountObject.getString("message");
                Log.e("return message", strMsg);

                if (strStatus.equals("true")) {
                    JSONArray resultArray = JsonAccountObject.getJSONArray("data");
                    Log.e("TodayOrder","........................" + resultArray.toString());
                    int arraySize = resultArray.length();
                    String currentOflnordid = "";
                    for(int row = 0; row < resultArray.length(); row++){
                        JSONObject currentOrder = resultArray.getJSONObject(row);
                        boolean isOrderProcessed = false;
                        String strOrderStatus = currentOrder.getString("ordstatus");
                        JSONArray orderDetailArray = currentOrder.getJSONArray("orderdetail");

                        //////////////////////////////////////////
                        if(orderDetailArray.length() <= 0) {
                            continue;
                        }

                        if(orderDetailArray.getJSONObject(0).getString("qty").equals(Constants.CLOSING_STOCK_QUANTITY)) {
                            continue;
                        }   //////////////block closing stock

                        ///////////////////////////////////////
                        // here Need to change (filter outlets) by Kumaravel 10/03/2019 /// No Order implement in 21-3-2019
                        if(!strOrderStatus.equals(Constants.OUTLET) && !strOrderStatus.equals("No Order")) {

                            if (strOrderStatus.equals("Partially Delivered") || strOrderStatus.equals("Delivered")) {
                                isOrderProcessed = true;
                            }
                            ContentValues values = new ContentValues();
                            values.put(KEY_ORDER_INFO_ORDER_ID, currentOrder.getInt("ordid"));
                            values.put(KEY_IS_ORDER_PROCESSED, isOrderProcessed);
                            orderInfoList.add(values);
                            //Header values loading
                            LoadOnlineOrder.loadOrderHeader(currentOrder, orderHeaderList, databaseHandler);
                            ////////////////databaseHandler.loadHeader(orderHeaderList.get(currentRow));////changed row to currentRow
                            // header id fetching
                            Cursor cursor;
                            cursor = databaseHandler.getOrderHeaderId(currentOrder.getString("ordid"));//
                            if (cursor != null && cursor.moveToFirst()) {
                                currentOflnordid = cursor.getString(cursor.getColumnIndex("oflnordid"));
                                cursor.close();
                            } else {
                                listener.onLoadFailed("No Matching Data Received From Server");
                                break;
                            }
                            Log.e("currentOflnordid", "......." + currentOflnordid);

                            // order detail process
                            for (int orderRow = 0; orderRow < orderDetailArray.length(); orderRow++) {
                                loadOrderDetail(currentOflnordid, orderDetailArray.getJSONObject(orderRow), orderDetailList);
                            }
                            currentRow++;
                        }
                    }
                    databaseHandler.addOrderInfo(orderInfoList); //////////
                    Log.e("orderInfoList","____________________________________size"+orderInfoList.size());
                    ////databaseHandler.addBulkOrderHeader(orderHeaderList);// no need here
                    databaseHandler.addBulkOrderDetail(orderDetailList);///
                    Log.e("orderDetailList","___________________________________size"+orderDetailList.size());


                    if(listener != null)
                        listener.onLoadFinished();
                }else {
                    Log.e("TodayOrder",",,,,,,...............No Result From Server");
                    if(listener != null)
                        listener.onLoadFailed("No Data From Server");
                }
            }catch (Exception e){
                e.printStackTrace();
                if(listener != null)
                    listener.onLoadFailed(e.getMessage());
            }
        }
    }
    // orderheader(oflnordid INTEGER PRIMARY KEY AUTOINCREMENT ,
    // suserid INTEGER,muserid INTEGER,mname VARCHAR,
    // iscash VARCHAR,pushstatus VARCHAR,orderdt VARCHAR,
    // onlineorderno VARCHAR,oflnordno VARCHAR,aprxordval VARCHAR,
    // pymnttotal VARCHAR,geolat VARCHAR,geolong VARCHAR,start_time VARCHAR,end_time VARCHAR,followup VARCHAR);";
    private static void loadOrderHeader(JSONObject currentOrder, List<ContentValues> orderHeaderList, DatabaseHandler databaseHandler){
        try {
            Log.e("loadOrderHeader-@@",".@@@@@@@@......"+currentOrder.toString());
            String companyName = currentOrder.getJSONObject("muser").getString("companyname");
            ContentValues values = new ContentValues();
            values.put("suserid",currentOrder.getInt("suserid"));
            values.put("muserid",currentOrder.getInt("muserid"));
            values.put("mname",companyName);  /////
            values.put("iscash",currentOrder.getString("iscash"));
            values.put("pushstatus","Success");
            values.put("orderdt",currentOrder.getString("orderdt"));
            values.put("onlineorderno",currentOrder.getString("ordid"));//////////
            values.put("oflnordno",currentOrder.getInt("oflnordid"));////////////
            values.put("aprxordval",currentOrder.getString("aprxordval"));
            if(currentOrder.getString("pymttotal") == null){
                values.put("pymnttotal","");
            }else {
                values.put("pymnttotal",currentOrder.getString("pymttotal"));
            }
            values.put("geolat",currentOrder.getString("geolat"));
            values.put("geolong",currentOrder.getString("geolong"));
            values.put("start_time",currentOrder.getString("start_time"));
            values.put("end_time",currentOrder.getString("end_time"));
            values.put("followup","");////

            orderHeaderList.add(values);

            databaseHandler.loadHeader(values);////changed row to currentRow

        }catch (Exception e){
            e.printStackTrace();
        }
    }
    private static void loadOrderDetail(String oflnordid, JSONObject currentOrder, List<ContentValues> orderDetailList){
        try {
            Log.e("loadOrderDetail-$$","..$$$$$$$$$$$$$$$$$$....."+currentOrder.toString());
            ContentValues values = new ContentValues();
            values.put("oflnordid",oflnordid);
            values.put("duserid",currentOrder.getInt("duserid"));
            String Dcompany = currentOrder.getJSONObject("duser").getString("companyname");
            values.put("Dcompany",Dcompany);  /////////////
            values.put("prodid",currentOrder.getString("prodid"));
            ///important Kumaravel 06-06-2019
            if(MyApplication.getInstance().isTemplate1() || MyApplication.getInstance().isTemplate2()) {
                if(currentOrder.getString("qty").equals(Constants.PRODUCT_QUANTITY)) {
                    values.put("qty", "0");
                }else {
                    values.put("qty", currentOrder.getString("qty"));
                }
            }else {
                values.put("qty", currentOrder.getString("qty"));
            }

            //Change End
            values.put("isfree",currentOrder.getString("isfree"));
            values.put("ordstatus",currentOrder.getString("ordstatus"));
            values.put("ordslno",currentOrder.getString("ordslno"));

            JSONObject productObj = currentOrder.getJSONObject("product");
            values.put("productcode",productObj.getString("prodcode"));//
            values.put("productname",productObj.getString("prodname"));//

            values.put("updateddt",currentOrder.getString("updateddt"));
            values.put("deliverydt",currentOrder.getString("deliverydt"));
            values.put("prate",currentOrder.getString("prate"));
            values.put("ptax",currentOrder.getString("ptax"));
            values.put("pvalue",currentOrder.getString("pvalue"));
            values.put("isread",currentOrder.getString("isread"));
            values.put("stock",currentOrder.getString("stock"));
            values.put("batchno",currentOrder.getString("batchno"));
            values.put("manufdt",currentOrder.getString("manufdt"));
            values.put("expirydt",currentOrder.getString("expirydt"));
            /////values.put("ordimage",currentOrder.getString("ordimage"));
            values.put("ordimage","0"); /////////////need to set "0" because no data to populate image 10/07/2019
            values.put("default_uom",currentOrder.getString("default_uom"));
            values.put("ord_uom",currentOrder.getString("ord_uom"));
            values.put("cqty",currentOrder.getString("cqty"));
            values.put("deliverytime",currentOrder.getString("deliverytime"));
            values.put("deliverytype",currentOrder.getString("deliverytype"));
            values.put("currstock",currentOrder.getString("currstock"));

            orderDetailList.add(values);

        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public static String getStartDate(Date date){
        Calendar c = Calendar.getInstance();
        String startDate = "";
        SimpleDateFormat df = new SimpleDateFormat(datePattern, Locale.getDefault());
        startDate = df.format(date);
        return startDate ;//+ Constants.SINGLE_SPACE + "00:00";
    }

    public static String currentDateAndTime(Date date){
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        String startDate = "", finalDate = "";
        SimpleDateFormat df = new SimpleDateFormat(datePattern, Locale.getDefault());
        startDate = df.format(date);
        String time = "";
        String mHour = String.valueOf(c.get(Calendar.HOUR_OF_DAY));
        String mMinute = String.valueOf(c.get(Calendar.MINUTE));
        //String mSeconds = String.valueOf(c.get(Calendar.SECOND));
        //////////time = mHour + ":" + mMinute;////////////
        time = "23:59";
        finalDate = startDate ;//+ Constants.SINGLE_SPACE + time;///Constants.SINGLE_SPACE +
        return finalDate;
    }


    public static JSONArray remove(final int idx, final JSONArray from) {
        final List<JSONObject> objs = asList(from);
        objs.remove(idx);

        final JSONArray ja = new JSONArray();
        for (final JSONObject obj : objs) {
            ja.put(obj);
        }

        return ja;
    }

    private static List<JSONObject> asList(final JSONArray ja) {
        final int len = ja.length();
        final ArrayList<JSONObject> result = new ArrayList<JSONObject>(len);
        for (int i = 0; i < len; i++) {
            final JSONObject obj = ja.optJSONObject(i);
            if (obj != null) {
                result.add(obj);
            }
        }
        return result;
    }
}
