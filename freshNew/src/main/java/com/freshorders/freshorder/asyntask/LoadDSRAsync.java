package com.freshorders.freshorder.asyntask;

import android.os.AsyncTask;
import android.util.Log;

import com.freshorders.freshorder.utils.JsonServiceHandler;

import org.json.JSONObject;

import static com.freshorders.freshorder.utils.Constants.DSR_NO_DATA_FOUND;

public class LoadDSRAsync {

    private static final String TAG = LoadDSRAsync.class.getSimpleName();

    public LoadDSRAsync(JsonServiceHandler jsonServiceHandler,
                        _DSR_CompleteListener listener) {
        this.jsonServiceHandler = jsonServiceHandler;
        this.listener = listener;
    }

    public interface _DSR_CompleteListener{
        void onSuccess(JSONObject object);
        void onFailed(String msg);
        void onException(String msg);
    }

    private JsonServiceHandler jsonServiceHandler;
    private JSONObject jsonObject;
    private LoadDSRAsync._DSR_CompleteListener listener;

    public void execute(){
        new LoadDSRAsync.DSRAsyncTask(jsonServiceHandler, listener).execute();
    }

    public static class DSRAsyncTask extends AsyncTask<Void, Void, JSONObject> {

        private JsonServiceHandler jsonServiceHandler;
        private LoadDSRAsync._DSR_CompleteListener listener;

        public DSRAsyncTask(JsonServiceHandler jsonServiceHandler,
                            LoadDSRAsync._DSR_CompleteListener listener) {
            this.jsonServiceHandler = jsonServiceHandler;
            this.listener = listener;
        }

        @Override
        protected JSONObject doInBackground(Void... voids) {
            Log.e(TAG,"''''''''''''''doInBackground''''''''''''''''''''......................");
            //return jsonServiceHandler.ServiceData();
            return jsonServiceHandler.ServiceDataModifiedGet();
        }

        @Override
        protected void onPostExecute(JSONObject result) {

            try {
                if (result != null) {
                    String strMsg;
                    String strStatus;
                    if(result.has("status")) {
                        strStatus = result.getString("status");
                    }else {
                        listener.onException(DSR_NO_DATA_FOUND);
                        return;
                    }
                    if(result.has("status")) {
                        strMsg = result.getString("message");
                    }else {
                        listener.onException(DSR_NO_DATA_FOUND);
                        return;
                    }
                    Log.e("return status", strStatus);
                    Log.e("return message", strMsg);

                    if (strStatus.equals("true")) {
                        Log.e("Answer",".........................." + result.toString());
                        listener.onSuccess(result);
                        return;
                    }else {
                        listener.onFailed(strMsg);
                        return;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                listener.onException(e.getMessage());
            }
        }
    }
}
