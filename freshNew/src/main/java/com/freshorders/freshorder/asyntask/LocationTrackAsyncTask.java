package com.freshorders.freshorder.asyntask;

import android.content.Context;
import android.database.Cursor;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.interfaces.GPSPoints;
import com.freshorders.freshorder.model.GPSPointsDetail;
import com.freshorders.freshorder.model.PointsResponse;
import com.freshorders.freshorder.model.Result;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LocationTrackAsyncTask {

    private static String TAG = "LocationTrack";
    public GPSPoints api;
    private Context mContext;

    private LinkedHashMap<Integer,Integer> idPosition = new LinkedHashMap<>();
    private List<Integer> idList = new ArrayList<>();

    public LocationTrackAsyncTask(Context mContext) {
        this.mContext = mContext;
    }

    public LocationTrackAsyncTask() {
    }

    public interface LocationCompleteListener{
        void onComplete(Result result);
    }


    public void sendAllPoints(DatabaseHandler databaseHandler, Context mContext, LocationCompleteListener listener){
        Log.e(TAG, "........................inner Stop+databaseHandler::"+databaseHandler);
        if(Constants.USER_ID == null){
            Cursor curs;
            curs = databaseHandler.getDetails();
            if(curs != null && curs.getCount() > 0) {
                curs.moveToFirst();
                Constants.USER_ID = curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_id));
                Log.e("Constants.USER_ID", Constants.USER_ID);
                curs.close();
            }
        }
        Cursor cur = databaseHandler.getAllUnSyncLocation();

        JSONArray array = new JSONArray();
        if(cur != null && cur.getCount() > 0){
            Log.e(TAG,"................count:::"+cur.getCount());
            cur.moveToFirst();
            try {
                do {
                    int id = cur.getInt(cur.getColumnIndex(DatabaseHandler.KEY_LOCATION_SERIAL_NO));
                    JSONObject item = new JSONObject();
                    item.put("rowid", id);
                    item.put("batchid", 0);
                    item.put("suserid", Constants.USER_ID);
                    item.put("logtime", cur.getInt(cur.getColumnIndex(DatabaseHandler.KEY_LOCATION_DATE_TIME)));
                    byte[] blob = cur.getBlob(cur.getColumnIndex(DatabaseHandler.KEY_LOCATION_LOCATION));
                    String json = new String(blob);

                    double lat = 0,lon = 0, preLat = 0, preLon = 0;

                    Location start;
                    Location end = null;

                    {//////////current lat lon value
                        Gson gson1 = new Gson();
                        start = gson1.fromJson(json,
                                new TypeToken<Location>() {}.getType());
                         lat = start.getLatitude();
                         lon = start.getLongitude();
                    }

                    {
                        int previousId = id - 1;
                        if(previousId > 0) {
                            Cursor curs = databaseHandler.getLocationsById((int) previousId);
                            if (curs != null && curs.getCount() > 0) {
                                curs.moveToFirst();
                                byte[] blob1 = curs.getBlob(cur.getColumnIndex(DatabaseHandler.KEY_LOCATION_LOCATION));
                                String json1 = new String(blob1);
                                Gson gson1 = new Gson();
                                end = gson1.fromJson(json1,
                                        new TypeToken<Location>() {}.getType());
                                preLat = end.getLatitude();
                                preLon = end.getLongitude();
                                curs.close();
                            }
                        }else {
                            if(id != 1){
                                continue;
                            }else {
                                end = start;////////////////
                            }
                        }
                    }
                    double temp_distance;
                    if(start != null && end != null) {
                        temp_distance = start.distanceTo(end);
                    }else {
                        continue;
                    }

                    item.put("geolat", lat);
                    item.put("geolong", lon);
                    item.put("pgeolat", preLat);
                    item.put("pgeolong", preLon);
                    item.put("status", 0);
                    item.put("updateddt", cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_LOCATION_DATE_TIME)));
                    item.put("distance", temp_distance);
                    item.put("amount", 0);
                    item.put("issynced", 1);
                    array.put(item);
                    Log.e(TAG,"...points"+item.toString());
                }while (cur.moveToNext());

                JsonServiceHandler JsonServiceHandler = new JsonServiceHandler(Utils.strSaveTracklog, mContext);
                new LocationTrackAsyncTask.LocationSendAsyncTask(array,JsonServiceHandler,listener).execute();

            }catch (Exception e){
                Result result = new Result();
                result.setStatusCode(-1);
                result.setMsg(e.getMessage());
                e.printStackTrace();
                listener.onComplete(result);
            }
        }
    }

    public void sendEveryPoints(DatabaseHandler databaseHandler, Context mContext, LocationCompleteListener listener){
        JsonServiceHandler JsonServiceHandler = new JsonServiceHandler(Utils.strSaveTracklog, mContext);
        new LocationSendAllAsyncTask(databaseHandler,JsonServiceHandler,listener).execute();
    }

    public static class LocationSendAllAsyncTask extends AsyncTask<Void, Void, Result> {


        private LocationTrackAsyncTask.LocationCompleteListener listener;
        JsonServiceHandler JsonServiceHandler;
        DatabaseHandler databaseHandler;
        private LinkedHashMap<Integer,Integer> idPosition;
        private List<Integer> idList;

        public LocationSendAllAsyncTask(DatabaseHandler databaseHandler, JsonServiceHandler JsonServiceHandler, LocationCompleteListener listener) {
            this.databaseHandler = databaseHandler;
            this.listener = listener;
            this.JsonServiceHandler = JsonServiceHandler;
            idPosition = new LinkedHashMap<>();
            idList = new ArrayList<>();

        }

        @Override
        protected Result doInBackground(Void... voids) {
            if(android.os.Debug.isDebuggerConnected())
                android.os.Debug.waitForDebugger();

            Cursor cur_ = databaseHandler.getAllLocations();

            if(cur_ != null && cur_.getCount() > 0){
                cur_.moveToFirst();
                for(int i = 0; i < cur_.getCount(); i++){
                    int id = cur_.getInt(cur_.getColumnIndex(DatabaseHandler.KEY_LOCATION_SERIAL_NO));
                    idPosition.put(id,i);
                    idList.add(id);
                    cur_.moveToNext();
                }
                cur_.close();
            }else {
                Result result = new Result();
                result.setStatusCode(0);
                result.setMsg("No pending points available");
                return result;
            }

            Result result = new Result();
            if (Constants.USER_ID == null) {
                Cursor curs;
                curs = databaseHandler.getDetails();
                if (curs != null && curs.getCount() > 0) {
                    curs.moveToFirst();
                    Constants.USER_ID = curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_id));
                    Log.e("Constants.USER_ID", Constants.USER_ID);
                    curs.close();
                }
            }
            Cursor cur = databaseHandler.getAllUnSyncLocation();

            JSONArray array = new JSONArray();
            if (cur != null && cur.getCount() > 0) {
                Log.e(TAG, "................count:::" + cur.getCount());
                cur.moveToFirst();
                try {
                    do {
                        int id = cur.getInt(cur.getColumnIndex(DatabaseHandler.KEY_LOCATION_SERIAL_NO));
                        JSONObject item = new JSONObject();
                        item.put("rowid", id);
                        item.put("batchid", 0);
                        item.put("suserid", Constants.USER_ID);
                        item.put("logtime", cur.getInt(cur.getColumnIndex(DatabaseHandler.KEY_LOCATION_DATE_TIME)));
                        /*byte[] blob = cur.getBlob(cur.getColumnIndex(DatabaseHandler.KEY_LOCATION_LOCATION));
                        String json = new String(blob);*/

                        double lat = 0, lon = 0, preLat = 0, preLon = 0;

                        Location start = new Location("locationA");
                        Location end = new Location("locationB");

                        //////////current lat lon value
                            /*Gson gson = new Gson();
                            start = gson.fromJson(json,
                                    new TypeToken<Location>() {
                                    }.getType());  */
                        lat = cur.getDouble(cur.getColumnIndex(DatabaseHandler.KEY_LOCATION_LAT));
                        lon = cur.getDouble(cur.getColumnIndex(DatabaseHandler.KEY_LOCATION_LON));
                        start.setLatitude(lat);
                        start.setLongitude(lon);
                        /////////////////////////////////
                        //Log.e(TAG,"............." + json);
                        ////////////////////////////////
                        int position = idPosition.get(id);
                        if(position > 0){
                            int prePos = position -1;
                            int previousId = 0;
                            if(prePos >= 0 && prePos < idList.size()) {
                                previousId = idList.get(prePos);

                                Cursor curs = databaseHandler.getLocationsById(previousId);
                                if (curs != null && curs.getCount() > 0) {
                                    curs.moveToFirst();
                                    /*byte[] blob1 = curs.getBlob(cur.getColumnIndex(DatabaseHandler.KEY_LOCATION_LOCATION));
                                    String json1 = new String(blob1);
                                    Gson gson1 = new Gson();
                                    end = gson1.fromJson(json1,
                                            new TypeToken<Location>() {
                                            }.getType()); */
                                    preLat = curs.getDouble(cur.getColumnIndex(DatabaseHandler.KEY_LOCATION_LAT));
                                    preLon = curs.getDouble(cur.getColumnIndex(DatabaseHandler.KEY_LOCATION_LON));
                                    end.setLatitude(preLat);
                                    end.setLongitude(preLon);
                                    curs.close();
                                }else {
                                    continue;
                                }
                            }else {
                                continue;
                            }
                        }else if(position == 0){
                            end = start;////////////////
                        }else {
                            continue;
                        }
                        /////////////////////////////////////////////////
                        //Log.e(TAG,"............." + json);
                        double temp_distance;
                        if (start != null && end != null) {
                            temp_distance = start.distanceTo(end);
                        } else {
                            continue;
                        }

                        item.put("geolat", lat);
                        item.put("geolong", lon);
                        item.put("pgeolat", preLat);
                        item.put("pgeolong", preLon);
                        item.put("status", 0);
                        item.put("updateddt", cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_LOCATION_DATE_TIME)));
                        item.put("distance", temp_distance);
                        item.put("amount", 0);
                        item.put("issynced", 1);
                        array.put(item);
                        Log.e(TAG, "...points" + item.toString());
                    } while (cur.moveToNext());

                cur.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    result.setStatusCode(-1);
                    result.setMsg(e.getMessage());
                    return result;
                }


                Log.e("Location", "''''''''''''''''''''''''''''''''''....................triggered..");

                try {
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject jsonObject = array.getJSONObject(i);
                        JsonServiceHandler.setParams(jsonObject.toString());
                        JSONObject object = JsonServiceHandler.ServiceData();
                        if (object != null) {
                            if (object.has("status")) {
                                String status = object.getString("status");
                                if (status.equals("true")) {
                                    String data = object.getString("data");
                                    Log.e(TAG, "..........response" + data);
                                    int id = jsonObject.getInt("rowid");
                                    if(idPosition.get(id) != null){
                                        int pos = idPosition.get(id);
                                        int prePos = pos -1;
                                        if(prePos >= 0 && prePos < idList.size()) {
                                            int previousId = idList.get(prePos);
                                        }
                                    }

                                }
                            } else {
                                Log.e(TAG, "..........status not found");
                            }
                        } else {
                            Log.e(TAG, "..........object null");
                        }
                    }
                    result.setStatusCode(1);
                    return result;
                } catch (Exception e) {
                    e.printStackTrace();
                    result.setStatusCode(-1);
                    result.setMsg(e.getMessage());
                    return result;
                }
                //return jsonServiceHandler.ServiceDataModifiedGet();
            } else {
                result.setStatusCode(0);
                result.setMsg("Failed");
                return result;
            }
        }

        @Override
        protected void onPostExecute(Result result) {
            try {
                //Result result = new Result();
                int integer = result.getStatusCode();
                if (integer == 1) {
                    result.setStatusCode(1);
                    result.setMsg("Location points successfully moved");
                    listener.onComplete(result);
                }else if(integer == -1){
                    listener.onComplete(result);
                }
            }catch (Exception e){
                e.printStackTrace();
                result.setMsg(e.getMessage());
                listener.onComplete(result);
            }
        }
    }

    public void sendPointsThroughRetrofit(long rowId,
                                          DatabaseHandler databaseHandler, String bugPath){
        Log.e(TAG,"...........................Trig");

        try{

            if (Constants.USER_ID == null || Constants.USER_ID.isEmpty()) {
                Cursor curs;
                curs = databaseHandler.getDetails();
                if (curs != null && curs.getCount() > 0) {
                    curs.moveToFirst();
                    Constants.USER_ID = curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_id));
                    Log.e("Constants.USER_ID", Constants.USER_ID);
                    curs.close();
                }else {
                    return ;
                }
            }

            Cursor cursor = databaseHandler.getUnSyncLocationsById(rowId);
            if(cursor != null && cursor.getCount() > 0) {
                Log.e(TAG,"...........................dataFound");
                cursor.moveToFirst();
                Location start = new Location("locationA");
                Location end = new Location("locationB");
                double lat = 0, lon = 0, preLat = 0, preLon = 0;
                JSONObject item = new JSONObject();
                item.put("rowid", rowId);
                item.put("batchid", 0);
                item.put("suserid", Constants.USER_ID);
                item.put("logtime", cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_DATE_TIME)));
                lat = cursor.getDouble(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_LAT));
                lon = cursor.getDouble(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_LON));
                start.setLatitude(lat);
                start.setLongitude(lon);
                long preId = cursor.getLong(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_PREVIOUS_ID));
                Cursor curs = databaseHandler.getLocationsForPreById(preId);
                if (curs != null && curs.getCount() > 0) {
                    Log.e(TAG,"...........................previousFound");
                    curs.moveToFirst();
                    preLat = curs.getDouble(curs.getColumnIndex(DatabaseHandler.KEY_LOCATION_LAT));
                    preLon = curs.getDouble(curs.getColumnIndex(DatabaseHandler.KEY_LOCATION_LON));
                    end.setLatitude(preLat);
                    end.setLongitude(preLon);
                    curs.close();
                } else {
                    return;
                }

                double temp_distance = start.distanceTo(end);
                temp_distance = temp_distance / 1000;
                Log.e(TAG, ".................temp_distance::::KM::" + temp_distance);

                item.put("geolat", lat);
                item.put("geolong", lon);
                item.put("pgeolat", preLat);
                item.put("pgeolong", preLon);
                item.put("status", "Active");
                item.put("updateddt", cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_DATE_TIME)));
                item.put("distance", temp_distance);
                item.put("amount", 0);
                item.put("issynced", 1);
                item.put("pre_id", cursor.getLong(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_PREVIOUS_ID)));
                item.put("seqid", cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_UNIQUE_ID)));

                GPSPointsDetail obj = new GPSPointsDetail(rowId,0,Constants.USER_ID,cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_DATE_TIME)),
                        lat,lon,preLat,preLon,"Active",cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_DATE_TIME)),
                        temp_distance,0,1,cursor.getLong(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_PREVIOUS_ID)),
                        cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_UNIQUE_ID)));

                //addToQueue(item,databaseHandler, bugPath);/////////////////
                addToQueue(obj,databaseHandler, bugPath);/////////////////
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void addToQueue(final GPSPointsDetail jsonStringer,final DatabaseHandler databaseHandler, final String bugPath){
        Log.e(TAG,"................retro...........Trig");
        //final Call<PointsResponse> call = getRetrofit().sendPoints(jsonStringer);
        final Call<PointsResponse> call = getRetrofit().sendPoints(jsonStringer);

        call.enqueue(new Callback<PointsResponse>() {
            @Override
            public void onResponse(Call<PointsResponse> call, Response<PointsResponse> response) {
                try {
                    if (response != null) {
                        if (response.isSuccessful()) {
                            //long currentId = jsonStringer.getLong("rowid");
                            long currentId = jsonStringer.getRowid();
                            new com.freshorders.freshorder.utils.
                                    Log(bugPath).
                                    ee(TAG, ".........currentId...." + currentId + ".....Success:::");
                            Log.e(TAG, "..........success....." + response.toString());
                            if (response.body().getStatus().equals("true")) {
                                Log.e(TAG, "..........success.....Data::::" + response.body().getData());
                                Log.e(TAG, "..........success.....Request::pre_id::" + jsonStringer.getPre_id());
                                //long currentId = jsonStringer.getLong("rowid");
                                databaseHandler.setDistancePointSuccess(currentId);
                                //databaseHandler.deleteSuccessLocationById(jsonStringer.getLong("pre_id"));
                            }
                        }
                    }
                    new com.freshorders.freshorder.utils.Log(bugPath).ee(TAG, "............." + response.toString());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<PointsResponse> call, Throwable throwable) {
                try {
                    //long currentId = jsonStringer.getLong("rowid");
                    long currentId = jsonStringer.getRowid();
                    new com.freshorders.freshorder.utils.
                            Log(bugPath).
                            ee(TAG, ".........currentId...." + currentId + ".....Reason:::"+throwable.getMessage());
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        });

    }

    private GPSPoints getRetrofit(){
        if(api == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(GPSPoints.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            api = retrofit.create(GPSPoints.class);
        }
        return api;
    }

    public static class SendPointAsyncTask extends AsyncTask<Void, Void, JSONObject>{

        private long rowId;
        private DatabaseHandler databaseHandler;
        private JsonServiceHandler JsonServiceHandler;
        private LocationTrackAsyncTask.LocationCompleteListener listener;

        public SendPointAsyncTask(long rowId,
                                  DatabaseHandler databaseHandler,
                                  com.freshorders.freshorder.utils.JsonServiceHandler jsonServiceHandler,
                                  LocationCompleteListener listener) {
            this.rowId = rowId;
            this.databaseHandler = databaseHandler;
            JsonServiceHandler = jsonServiceHandler;
            this.listener = listener;
        }

        @Override
        protected JSONObject doInBackground(Void... voids) {
            try{

                if (Constants.USER_ID == null) {
                    Cursor curs;
                    curs = databaseHandler.getDetails();
                    if (curs != null && curs.getCount() > 0) {
                        curs.moveToFirst();
                        Constants.USER_ID = curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_id));
                        Log.e("Constants.USER_ID", Constants.USER_ID);
                        curs.close();
                    }else {
                        return null;
                    }
                }

                Cursor cursor = databaseHandler.getUnSyncLocationsById(rowId);
                if(cursor != null && cursor.getCount() > 0){
                    cursor.moveToFirst();
                    Location start = new Location("locationA");
                    Location end = new Location("locationB");
                    double lat = 0, lon = 0, preLat = 0, preLon = 0;
                    JSONObject item = new JSONObject();
                    item.put("rowid", rowId);
                    item.put("batchid", 0);
                    item.put("suserid", Constants.USER_ID);
                    item.put("logtime", cursor.getInt(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_DATE_TIME)));
                    lat = cursor.getDouble(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_LAT));
                    lon = cursor.getDouble(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_LON));
                    start.setLatitude(lat);
                    start.setLongitude(lon);
                    long preId = cursor.getLong(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_PREVIOUS_ID));
                    Cursor curs = databaseHandler.getLocationsForPreById(preId);
                    if(curs != null && curs.getCount() > 0) {
                        curs.moveToFirst();
                        preLat = curs.getDouble(curs.getColumnIndex(DatabaseHandler.KEY_LOCATION_LAT));
                        preLon = curs.getDouble(curs.getColumnIndex(DatabaseHandler.KEY_LOCATION_LON));
                        end.setLatitude(preLat);
                        end.setLongitude(preLon);
                        curs.close();
                    }else {
                        return null;
                    }

                    double temp_distance = start.distanceTo(end);
                    temp_distance = temp_distance / 1000;
                    Log.e(TAG,".................temp_distance::::KM::" + temp_distance);

                    item.put("geolat", lat);
                    item.put("geolong", lon);
                    item.put("pgeolat", preLat);
                    item.put("pgeolong", preLon);
                    item.put("status", 0);
                    item.put("updateddt", cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_DATE_TIME)));
                    item.put("distance", temp_distance);
                    item.put("amount", 0);
                    item.put("issynced", 1);
                    item.put("seqid", cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_LOCATION_UNIQUE_ID)));

                    JsonServiceHandler.setParams(item.toString());
                    JSONObject object = JsonServiceHandler.ServiceData();
                    if(object != null){
                        if(object.has("status")) {
                            String status = object.getString("status");
                            if(status.equals("true")){
                                String data = object.getString("data");
                                Log.e(TAG,"..........response" + data);
                                databaseHandler.setDistancePointSuccess(rowId);
                                databaseHandler.deleteSuccessLocationById(preId);
                                return object;
                            }
                        }else {
                            Log.e(TAG,"..........status not found" );
                            return null;
                        }
                    }else {
                        Log.e(TAG,"..........object null" );
                        return null;
                    }

                }else {
                    return null;
                }
                return null;
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            if(result != null){
                Log.e(TAG,".............................Success");
            }else{
                Log.e(TAG,".............................Failed");
            }
        }
    }

    public static String getFormattedDistance(Location loc1, Location loc2) {
        float distance = loc1.distanceTo(loc2);
        return String.format(Locale.getDefault(), "%.0f km", distance / 1000);
    }

    public static class LocationSendAsyncTask extends AsyncTask<Void, Void, Result> {

        private JSONArray array;
        private LocationTrackAsyncTask.LocationCompleteListener listener;
        JsonServiceHandler JsonServiceHandler;

        public LocationSendAsyncTask(JSONArray array,JsonServiceHandler JsonServiceHandler, LocationCompleteListener listener) {
            this.array = array;
            this.listener = listener;
            this.JsonServiceHandler = JsonServiceHandler;
        }

        @Override
        protected Result doInBackground(Void... voids) {
            Log.e("Location", "''''''''''''''''''''''''''''''''''....................triggered..");
            Result result = new Result();
            try {
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsonObject = array.getJSONObject(i);
                    JsonServiceHandler.setParams(jsonObject.toString());
                    JSONObject object = JsonServiceHandler.ServiceData();
                    if(object != null){
                        if(object.has("status")) {
                            String status = object.getString("status");
                            if(status.equals("true")){
                                String data = object.getString("data");
                                Log.e(TAG,"..........response" + data);                                                            }
                        }else {
                            Log.e(TAG,"..........status not found" );
                        }
                    }else {
                        Log.e(TAG,"..........object null" );
                    }
                }
                result.setStatusCode(1);
                return result;
            }catch (Exception e){
                e.printStackTrace();
                result.setStatusCode(-1);
                result.setMsg(e.getMessage());
                return result;
            }
            //return jsonServiceHandler.ServiceDataModifiedGet();
        }

        @Override
        protected void onPostExecute(Result result) {
            try {
                //Result result = new Result();
                int integer = result.getStatusCode();
                if (integer == 1) {
                    result.setStatusCode(1);
                    result.setMsg("Location points successfully moved");
                    listener.onComplete(result);
                }else if(integer == -1){
                    listener.onComplete(result);
                }
            }catch (Exception e){
                e.printStackTrace();
                result.setMsg(e.getMessage());
                listener.onComplete(result);
            }
        }
    }
}
