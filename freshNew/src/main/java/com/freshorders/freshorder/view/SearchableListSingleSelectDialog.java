package com.freshorders.freshorder.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
//import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.SearchView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.SearchableDialogAdapter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SearchableListSingleSelectDialog extends DialogFragment implements
        SearchView.OnQueryTextListener, SearchView.OnCloseListener {

    public interface SearchableItem<T> extends Serializable {
        void onSearchableItemClicked(T item, int position);
    }

    public interface OnSearchTextChanged {
        void onSearchTextChanged(String strText);
    }

    private SearchView searchView;
    private SearchableItem searchableItem;
    private OnSearchTextChanged onSearchTextChanged;
    private String strDialogTitle;

    private static final String ITEMS = "items";

    private ArrayAdapter itemsAdapter;
    private RecyclerView recyclerView;
    private List<String> items = new ArrayList<>();

    private String strPositiveButtonText;

    private DialogInterface.OnClickListener onClickListener;

    private SearchableDialogAdapter adapter;

    public SearchableListSingleSelectDialog() {

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable("item", searchableItem);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if( getDialog().getWindow() != null) {
            getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public static SearchableListSingleSelectDialog newInstance(List items) {
        SearchableListSingleSelectDialog multiSelectExpandableFragment = new
                SearchableListSingleSelectDialog();

        Bundle args = new Bundle();
        args.putSerializable(ITEMS, (Serializable) items);

        multiSelectExpandableFragment.setArguments(args);

        return multiSelectExpandableFragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        // Getting the layout inflater to inflate the view in an alert dialog.
        LayoutInflater inflater = LayoutInflater.from(getActivity());

        if (null != savedInstanceState) {
            searchableItem = (SearchableItem) savedInstanceState.getSerializable("item");
        }

        View rootView = inflater.inflate(R.layout.searchable_list_dialog, null);
        setData(rootView);

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setView(rootView);

        String strPositiveButton = strPositiveButtonText == null ? "CLOSE" : strPositiveButtonText;
        alertDialog.setPositiveButton(strPositiveButton, onClickListener);

        String strTitle = strDialogTitle == null ? "Select Item" : strDialogTitle;
        alertDialog.setTitle(strTitle);

        final AlertDialog dialog = alertDialog.create();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams
                .SOFT_INPUT_STATE_HIDDEN);
        return dialog;
    }

    private void setData(View rootView) {
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context
                .SEARCH_SERVICE);

        searchView = (SearchView) rootView.findViewById(R.id.search);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName
                ()));
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(this);
        searchView.setOnCloseListener(this);
        searchView.clearFocus();
        InputMethodManager mgr = (InputMethodManager) getActivity().getSystemService(Context
                .INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(searchView.getWindowToken(), 0);


        items = (List) getArguments().getSerializable(ITEMS);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.listItems);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity().getBaseContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));

        //create the adapter by passing your ArrayList data
        adapter = new SearchableDialogAdapter(getActivity(), items, new SearchableDialogAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String item) {
                searchableItem.onSearchableItemClicked(item, 0); // Here We don't use position so simply put Zero(0)
                getDialog().dismiss();
            }
        });
        //attach the adapter to the list
        recyclerView.setAdapter(adapter);

    }


    @Override
    public boolean onClose() {
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        //adapter.getFilter().filter(query);
        searchView.clearFocus();
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adapter.getFilter().filter(newText);
        if(onSearchTextChanged != null){
            onSearchTextChanged.onSearchTextChanged(newText);
        }
        return true;
    }

    public void setTitle(String strTitle) {
        strDialogTitle = strTitle;
    }

    public void setPositiveButton(String strPositiveButtonTxt) {
        strPositiveButtonText = strPositiveButtonTxt;
    }

    public void setPositiveButton(String strPositiveButtonText, DialogInterface.OnClickListener onClickListener) {
        this.strPositiveButtonText = strPositiveButtonText;
        this.onClickListener = onClickListener;
    }

    public void setOnSearchableItemClickListener(SearchableItem searchableItem) {
        this.searchableItem = searchableItem;
    }

    public void setOnSearchTextChangedListener(OnSearchTextChanged onSearchTextChanged) {
        this.onSearchTextChanged = onSearchTextChanged;
    }
}
