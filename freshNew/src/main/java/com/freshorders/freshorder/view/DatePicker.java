package com.freshorders.freshorder.view;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.freshorders.freshorder.R;

import java.util.Date;

public class DatePicker {

    public interface PopupListener{
        void onPopupClosed();
    }

    private Context mContext;
    private View popupView;
    private PopupWindow popup;
    private View rootView;
    private DatePicker.PopupListener listener;
    private android.widget.DatePicker datePicker;

    private static final String DATE_FORMAT = "dd-MM-yyyy";

    public DatePicker(Context mContext, View rootView, DatePicker.PopupListener listener) {
        this.mContext = mContext;
        this.rootView = rootView;
        this.listener = listener;
        createPopup();
    }

    private void createPopup() {
        try {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (layoutInflater != null) {
                popupView = layoutInflater.inflate(R.layout.popup_date_picker, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showPopup() {
        popup = new PopupWindow(mContext);
        popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setContentView(popupView);
        addWidgetsToListener(popupView);
        // Set popup window animation style.
        //////////popup.setAnimationStyle(R.anim.up_from_bottom);
        popup.setTouchable(true);
        popup.setFocusable(true);
        popup.setOutsideTouchable(false);
        //popup.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        popup.update();
        popup.showAtLocation(rootView, Gravity.CENTER, 0, 0);
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                //parentListener.onPopupDismiss();
                listener.onPopupClosed();
            }
        });

    }
    private void addWidgetsToListener(View popupView){

        datePicker = (android.widget.DatePicker) popupView.findViewById(R.id.datePicker);
        // Define min & max date for sample
        Date minDate = LazyDatePicker.stringToDate("01-01-2000", DATE_FORMAT);
        Date maxDate = LazyDatePicker.stringToDate("12-31-3018", DATE_FORMAT);
        // Init LazyDatePicker
        LazyDatePicker lazyDatePicker = (LazyDatePicker) popupView.findViewById(R.id.lazyDatePicker);
        lazyDatePicker.setDateFormat(LazyDatePicker.DateFormat.DD_MM_YYYY);
        lazyDatePicker.setMinDate(minDate);
        lazyDatePicker.setMaxDate(maxDate);

        lazyDatePicker.setOnDatePickListener(new LazyDatePicker.OnDatePickListener() {
            @Override
            public void onDatePick(Date dateSelected) {
                String selectedDate = LazyDatePicker.dateToString(dateSelected, DATE_FORMAT);
                Toast.makeText(mContext,
                        "Selected date: " + selectedDate,
                        Toast.LENGTH_SHORT).show();
                ((TextView)rootView).setText(selectedDate);
                popup.dismiss();
            }
        });

        Button btnOk = (Button) popupView.findViewById(R.id.id_outlet_stock_date_ok);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int day = datePicker.getDayOfMonth();
                int month = datePicker.getMonth() + 1;
                int year = datePicker.getYear();

                String monthLength =String.valueOf(month);//
                if(monthLength.length()==1){
                    monthLength = "0"+monthLength;
                }
                String dayLength =String.valueOf(day);
                if(dayLength.length()==1){
                    dayLength = "0"+dayLength;
                }

                String selectedDate = dayLength + "-" + monthLength + "-" + String.valueOf(year);
                ((TextView)rootView).setText(selectedDate);
                popup.dismiss();
            }
        });
    }




}
