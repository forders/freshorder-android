package com.freshorders.freshorder.treeview;

import org.json.JSONArray;

public class Pair {

    private String childId ;
    private String parentId;
    private String name;

    public Pair(String childId, String parentId) {
        this.childId = childId;
        this.parentId = parentId;
    }

    public Pair() {
    }

    public String getChildId() {
        return childId;
    }
    public void setChildId(String childId) {
        this.childId = childId;
    }
    public String getParentId() {
        return parentId;
    }
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
