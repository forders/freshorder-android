package com.freshorders.freshorder.interfaces;

import com.freshorders.freshorder.model.GPSPointsDetail;
import com.freshorders.freshorder.model.PointsResponse;
import com.freshorders.freshorder.utils.Utils;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.List;

import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface GPSPoints {

    String BASE_URL = Utils.urlPrefix;
/*
    @POST("geologs")
    Call<PointsResponse> sendPoints(@Body JSONObject jsonObject);  */

    @Headers({"Content-type: application/json",
            "Accept: */*"})
    @POST("geologs")
    Call<PointsResponse> sendPoints(@Body GPSPointsDetail body);


}
