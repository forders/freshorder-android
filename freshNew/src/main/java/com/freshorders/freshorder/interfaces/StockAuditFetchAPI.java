package com.freshorders.freshorder.interfaces;

import com.freshorders.freshorder.model.StockAuditDisplayModel;
import com.freshorders.freshorder.utils.Utils;

import org.json.JSONStringer;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface StockAuditFetchAPI {

    String BASE_URL = Utils.urlPrefix;

    //@FormUrlEncoded
    @GET("stockaudit")
    Call<List<StockAuditDisplayModel>> getStockAuditList(@Query(value="filter", encoded=true) String filter);
    //Call<List<StockAuditDisplayModel>> getStockAuditList(@Query("auditqty") String auditqty, @Query("suserid") String suserid);
    //Call<List<StockAuditDisplayModel>> getStockAuditList(/*@Body*/JSONStringer data);
}
