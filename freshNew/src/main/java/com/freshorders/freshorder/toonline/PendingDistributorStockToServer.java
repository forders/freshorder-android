package com.freshorders.freshorder.toonline;

import android.content.Context;
import android.database.Cursor;

import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.http.Request;
import com.freshorders.freshorder.http.Response;
import com.freshorders.freshorder.http.VolleySingleton;
import com.freshorders.freshorder.model.CommonMigrationModel;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.Log;

import java.util.LinkedHashMap;
import java.util.Map;

import static com.freshorders.freshorder.toonline.Migration.KEY_IS_DISTRIBUTOR_STOCK_MIGRATION_START;

public class PendingDistributorStockToServer {

    public static final String TAG = PendingSurveyToServer.class.getSimpleName();
    public int migrationCount = 0;

    public interface DistributorStockMigrationCompleteListener{
        void onComplete(LinkedHashMap<Integer, CommonMigrationModel> migrationList);
        void noMigrationRequired();
        void migrationStartFailed();
        void migrationInterrupted(LinkedHashMap<Integer, CommonMigrationModel> migrationList);
    }

    private Context mContext;
    private DatabaseHandler databaseHandler;
    private LinkedHashMap<Integer, CommonMigrationModel> migrationList;
    private PendingDistributorStockToServer.DistributorStockMigrationCompleteListener listener;

    public PendingDistributorStockToServer(Context mContext,
                                           PendingDistributorStockToServer.DistributorStockMigrationCompleteListener listener) {
        this.mContext = mContext;
        this.listener = listener;
        this.databaseHandler = new DatabaseHandler(mContext.getApplicationContext());
        this.migrationList = new LinkedHashMap<>();

    }

    public void startDistributorStockMigration(){
        try{
            Cursor cursor = databaseHandler.getDistributorStockPending();
            if(cursor != null){
                if(cursor.getCount() > 0){
                    cursor.moveToFirst();
                    while (!cursor.isAfterLast()) {
                        Log.e(TAG, "startDistributorMigration........count" + cursor.getCount());
                        CommonMigrationModel model = new CommonMigrationModel();
                        int id = cursor.getInt(cursor.getColumnIndex(DatabaseHandler.KEY_DISTRIBUTOR_STOCK_ID));
                        int migration = cursor.getInt(cursor.getColumnIndex(DatabaseHandler.KEY_DISTRIBUTOR_STOCK_MIGRATION_STATUS));
                        String url = cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_DISTRIBUTOR_STOCK_URL));
                        String urlData = cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_DISTRIBUTOR_STOCK_SYNC_DATA));
                        String error = cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_DISTRIBUTOR_STOCK_FAILED_REASON));
                        model.setId(id); model.setUrl(url);  model.setUrlData(urlData);  model.setStatus(migration);  model.setErrorReason(error);
                        migrationList.put(id, model);
                        cursor.moveToNext();
                    }
                    migrationCount = migrationList.size();
                    loadDataToServer();
                }else {
                    if(listener != null) {
                        listener.noMigrationRequired();
                    }else {
                        new PrefManager(mContext).setBooleanDataByKey(KEY_IS_DISTRIBUTOR_STOCK_MIGRATION_START, false);
                    }
                }
            }else {
                if(listener != null) {
                    listener.migrationStartFailed();
                }else {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_DISTRIBUTOR_STOCK_MIGRATION_START, false);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
            if(listener != null) {
                listener.migrationInterrupted(migrationList);
            }else {
                new PrefManager(mContext).setBooleanDataByKey(KEY_IS_DISTRIBUTOR_STOCK_MIGRATION_START, false);
            }
        }

    }

    private void loadDataToServer() throws Exception{

        new PrefManager(mContext).setBooleanDataByKey(KEY_IS_DISTRIBUTOR_STOCK_MIGRATION_START, true);
        for (Map.Entry entry : migrationList.entrySet()) {
            try {
                final int id = (int) entry.getKey();
                final CommonMigrationModel currentData = (CommonMigrationModel) entry.getValue();
                String url = currentData.getUrl();
                String data = currentData.getUrlData();
                final Request request = new Request(url, Request.Method.POST, Request.REQUEST_DEFAULT);
                request.setRequestBody(data);
                VolleySingleton.getInstance().connect(request, new VolleySingleton.ResponseListener() {

                    @Override
                    public void onSuccess(Response res) {
                        currentData.setStatus(0);
                        migrationList.put(id, currentData);
                        databaseHandler.updateDistributorStockSync(id, Migration.MIGRATION_COMPLETE, Constants.EMPTY);
                        migrationCount--;
                        if (migrationCount == 0) { // Migration Complete
                            if (listener != null) {
                                listener.onComplete(migrationList);
                            } else {
                                new PrefManager(mContext).setBooleanDataByKey(KEY_IS_DISTRIBUTOR_STOCK_MIGRATION_START, false);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Response res) {
                        setFailureStatus(res.message, currentData, id);
                    /*currentData.setStatus(-1);
                    migrationList.put(id, currentData);
                    databaseHandler.updateSurveySync(id, Migration.MIGRATION_FAILED, Constants.EMPTY);
                    migrationCount--;
                    if(migrationCount == 0){ // Migration Complete
                        if(listener != null) {
                            listener.onComplete(migrationList);
                        }else {
                            new PrefManager(mContext).setBooleanDataByKey(KEY_IS_DISTRIBUTOR_STOCK_MIGRATION_START, false);
                        }
                    } */
                    }
                });
            }catch (Exception e){
                e.printStackTrace();
                Log.e("Distributor.Mig","...........Exception." + e.getMessage());
            }
        }
    }

    private void setFailureStatus(String error, CommonMigrationModel currentData, int id){

        int status = -1;

        if(error.equals(Constants.TIME_OUT_ERROR)){
            status = 1; // Again retry set;
        }else if(error.equals(Constants.AUTH_FAILURE_ERROR)){
            status = -1;
        }else if(error.equals(Constants.SERVER_ERROR)){
            status = 1;
        }else if(error.equals(Constants.NETWORK_ERROR)){
            status = 1;
        }else if(error.equals(Constants.PARSE_ERROR)){
            status = -1;
        }else {
            status = -1;
        }

        currentData.setStatus(status);
        migrationList.put(id, currentData);
        databaseHandler.updateDistributorStockSync(id, Migration.MIGRATION_FAILED, error);
        migrationCount--;
        if(migrationCount == 0){ // Migration Complete
            if(listener != null) {
                listener.onComplete(migrationList);
            }else {
                new PrefManager(mContext).setBooleanDataByKey(KEY_IS_DISTRIBUTOR_STOCK_MIGRATION_START, false);
            }
        }
    }
}
