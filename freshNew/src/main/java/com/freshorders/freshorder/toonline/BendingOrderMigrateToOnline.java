package com.freshorders.freshorder.toonline;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.ui.AddMerchantNew;
import com.freshorders.freshorder.ui.CreateOrderActivity;
import com.freshorders.freshorder.ui.MerchantOrderConfirmation;
import com.freshorders.freshorder.ui.SalesManOrderActivity;
import com.freshorders.freshorder.ui.SalesManOrderCheckoutActivity;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.SM_UploadToServerCheckout;
import com.freshorders.freshorder.utils.SM_uploadToServerCheckout_NCR;
import com.freshorders.freshorder.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class BendingOrderMigrateToOnline {

    public interface ServerMigrationListener{
        void onMigrationFinished(boolean isSuccess, String statusMSG, boolean isException, String excMSG);
    }

    private Context mContext;
    DatabaseHandler databaseHandler;
    public SQLiteDatabase db;
    private BendingOrderMigrateToOnline.ServerMigrationListener listener;
    private String Merchantname = "";
    private boolean isNewClientOrder = false;

    public static String payment_type,orderdate,Orderno,flag,productStatus,mode,paymenttype,pushstauts,OfflineOrderNo,rowid,beatrowid, selected_dealer;
    public static int year,month,day,hr,mins,sec,deliverydt;

    public  static ArrayList<String> arraylistimagepath;

    public  static  String [] image;

    JSONObject JsonAccountObject = null;
    JSONArray JsonAccountArray = null;
    JsonServiceHandler JsonServiceHandler;

    private static void reSetDefault(){
        imagepath1 = ""; imagepath2 = ""; imagepath3 = ""; imagepath4 = ""; imagepath5 = ""; imagepath6 = "";
                imagepath7 = ""; imagepath8 = ""; imagepath9 = ""; imagepath10 = ""; imagepath11 = ""; imagepath12 = "";
                merchantRowid = "";
    }

    public static String imagepath1 = "", imagepath2 = "", imagepath3 = "", imagepath4 = "", imagepath5 = "", imagepath6 = "",
            imagepath7 = "", imagepath8 = "", imagepath9 = "", imagepath10 = "", imagepath11 = "", imagepath12 = "",
            merchantRowid = "";

    public BendingOrderMigrateToOnline(Context mContext, BendingOrderMigrateToOnline.ServerMigrationListener listener) {
        this.mContext = mContext;
        databaseHandler = new DatabaseHandler(mContext);
        this.listener = listener;
        Log.e("DataMigration", "started.............Constructor");
        arraylistimagepath=new ArrayList<String>();
        db = mContext.openOrCreateDatabase("freshorders", 0, null);
    }

    public void setListenerNULL(){
        if(listener != null) {
            listener = null;
        }
    }

    public BendingOrderMigrateToOnline.ServerMigrationListener getListener(){
        return listener;
    }




    public synchronized void migration() {

        try {
            Cursor curs;
            //curs = db.rawQuery("SELECT * from orderheader where pushstatus ='Pending'", null);

            //curs = db.rawQuery("SELECT * from orderheader where pushstatus ='Pending' AND muserid != 'New User'", null);

            curs = db.rawQuery("SELECT * from orderheader where pushstatus ='Pending'", null);


            Log.e("HeaderCount", String.valueOf(curs.getCount()));
            final JSONArray resultSet = new JSONArray();

            if (curs != null && curs.getCount() > 0) {

                new PrefManager(mContext.getApplicationContext()).setBooleanDataByKey(PrefManager.KEY_IS_MIGRATION_SET, true);

                curs.moveToFirst();
                while (curs.isAfterLast() == false) {

                    int totalColumn = curs.getColumnCount();
                    JSONObject rowObject = new JSONObject();

                    for (int i = 0; i < totalColumn; i++) {

                        if (curs.getColumnName(i) != null) {

                            try {

                                if (curs.getString(i) != null) {
                                    if (!curs.getString(i).equals("New User")) {
                                        Log.d("TAG_NAME", curs.getString(i));
                                        rowObject.put(curs.getColumnName(i), curs.getString(i));
                                    }
                                  /*  Log.d("TAG_NAME", curs.getString(i));
                                    rowObject.put(curs.getColumnName(i), curs.getString(i));*/
                                } else {

                                    if (!curs.getColumnName(i).equals("muserid")) {
                                        rowObject.put(curs.getColumnName(i), "");
                                    }

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                if (listener != null) {
                                    listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                                    return;
                                }
                            }
                        }
                    }

                    try {
                        //Kumaravel 13-07-2019
                        if(Constants.distributorname == null || Constants.distributorname.isEmpty()){
                            Constants.distributorname = new PrefManager(mContext).getStringDataByKey(PrefManager.KEY_DISTRIBUTOR_NAME);
                        }
                        rowObject.put("distributorname",Constants.distributorname);
                    } catch (Exception e) {
                        Log.d("Exception in Dist. name", e.getMessage());
                    }


                    try {

                        Cursor curs1;
                        curs1 = db.rawQuery("SELECT userid from dealertable", null);
                        Log.e("dEALER", String.valueOf(curs1.getCount()));
                        if (curs1.getCount() > 0) {
                            curs1.moveToFirst();
                            selected_dealer = curs1.getString(0);
                        }


                        Cursor cursor2;
                        Merchantname = rowObject.getString("mname");

                        cursor2 = db.rawQuery("SELECT * from merchanttable where companyname =" + "'" + Merchantname + "'", null);
                        Log.e("HeaderCountCanled", String.valueOf(cursor2.getCount()));

                        cursor2.moveToFirst();
                        while (cursor2.isAfterLast() == false) {

                            int Columncount = cursor2.getColumnCount();
                            Log.e("merchantColumn", String.valueOf(cursor2.getColumnCount()));
                            JSONObject merchantdetail = new JSONObject();
                            for (int j = 0; j < Columncount; j++) {
                                if (cursor2.getColumnName(j) != null) {
                                    try {
                                        if (cursor2.getColumnName(j).equals("cflag")) {
                                            if (cursor2.getString(j).equals("R")) {
                                                merchantdetail.put(cursor2.getColumnName(j), "G");
                                            } else if (cursor2.getString(j).equals("G")) {
                                                merchantdetail.put(cursor2.getColumnName(j), "");
                                            } else {
                                                merchantdetail.put(cursor2.getColumnName(j), "");
                                            }

                                        } else {
                                            if (cursor2.getString(j) != null) {
                                                if (!cursor2.getString(j).equals("New User")) {

                                                    merchantdetail.put(cursor2.getColumnName(j), cursor2.getString(j));

                                                }
                                                //merchantdetail.put(cursor2.getColumnName(j) ,  cursor2.getString(j) );
                                            } else {
                                                if (!cursor2.getColumnName(j).equals("userid")) {
                                                    merchantdetail.put(cursor2.getColumnName(j), "");
                                                }

                                            }
                                        }

                                    } catch (Exception e) {
                                        Log.d("Merchant_Exception", e.getMessage());
                                        e.printStackTrace();
                                        if (listener != null) {
                                            listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                                            return;
                                        }
                                    }
                                }

                            }
                            merchantdetail.put("usertype", "M");
                            merchantdetail.put("duserid", selected_dealer);
                            rowObject.put("merchant", merchantdetail);
                            cursor2.moveToNext();
                        }
                        cursor2.close();
                        Log.e("Detailarray", rowObject.toString());

                    } catch (Exception e) {
                        e.printStackTrace();
                        if (listener != null) {
                            listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                            return;
                        }
                    }

                    Log.e("jsonarray", rowObject.toString());

                    JSONArray resultDetail = new JSONArray();
                    try {

                        Cursor cursor;
                        cursor = db.rawQuery("SELECT * from oredrdetail where oredrdetail.oflnordid=" + rowObject.getString("oflnordid"), null);
                        Log.e("DetailCount", String.valueOf(cursor.getCount()));
                        cursor.moveToFirst();
                        while (cursor.isAfterLast() == false) {

                            int Columncount = cursor.getColumnCount();
                            JSONObject detailObject = new JSONObject();

                            for (int i = 0; i < Columncount; i++) {
                                if (cursor.getColumnName(i) != null) {

                                    try {

                                        if (cursor.getString(i) != null) {
                                            Log.d("TAG_NAME", cursor.getString(i));
                                            // important Kumaravel 06-06-2019  //////////////////////
                                            if(cursor.getColumnName(i).equals("qty")){
                                                Log.e("Qty","......................................");
                                                if(MyApplication.getInstance().isTemplate1() || MyApplication.getInstance().isTemplate2()){
                                                    Log.e("Qty","................cursor.getString(i).........::::"+cursor.getString(i));
                                                    if(cursor.getString(i).equals("0")){
                                                        detailObject.put(cursor.getColumnName(i), Constants.PRODUCT_QUANTITY); //////////
                                                    }else {
                                                        detailObject.put(cursor.getColumnName(i), cursor.getString(i));
                                                    }
                                                }else {
                                                    detailObject.put(cursor.getColumnName(i), cursor.getString(i));
                                                }
                                            }else{
                                                detailObject.put(cursor.getColumnName(i), cursor.getString(i));
                                            }
                                            /////// Change End//////////////////////////////////////
                                        } else {
                                            detailObject.put(cursor.getColumnName(i), "");

                                        }
                                    } catch (Exception e) {
                                        Log.d("TAG_NAME", e.getMessage());
                                        e.printStackTrace();
                                        if (listener != null) {
                                            listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                                            return;
                                        }
                                    }
                                }

                            }

                            resultDetail.put(detailObject);
                            cursor.moveToNext();
                        }
                        Log.e("arraylist", String.valueOf(arraylistimagepath.size()));
                        cursor.close();

                        Log.e("Detailarray", resultDetail.toString());

                    } catch (Exception e) {
                        e.printStackTrace();
                        if (listener != null) {
                            listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                            return;
                        }
                    }
                    try {
                        rowObject.put("orderdtls", resultDetail);
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (listener != null) {
                            listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                            return;
                        }
                    }
                    resultSet.put(rowObject);
                    curs.moveToNext();
                }
                curs.close();
                Log.e("SaveArray111", resultSet.toString());
                JsonServiceHandler = new JsonServiceHandler(Utils.strsavemerchantOderdetail, mContext);
                new MigrationAsyncTask(db, listener, databaseHandler, JsonServiceHandler, resultSet).execute();
            } else {
                Log.e("else_ordercancel", "ordercancel");
                ///////////ordercancel();
            }
        }catch (Exception e){
            e.printStackTrace();
            if(listener != null) {
                listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
            }
        }
    }

    public static class MigrationAsyncTask extends AsyncTask<Void, Void, String>{
        private  SQLiteDatabase db;
        private BendingOrderMigrateToOnline.ServerMigrationListener listener;
        private DatabaseHandler databaseHandler;
        private JSONObject JsonAccountObject;
        private JsonServiceHandler JsonServiceHandler;
        private JSONArray resultSet;

        String strStatus = "";
        String strMsg = "", strOrderId = "";

        public MigrationAsyncTask(SQLiteDatabase db,
                                  ServerMigrationListener listener,
                                  DatabaseHandler databaseHandler,
                                  JsonServiceHandler jsonServiceHandler,
                                  JSONArray resultSet) {
            this.db = db;
            this.listener = listener;
            this.databaseHandler = databaseHandler;
            JsonServiceHandler = jsonServiceHandler;
            this.resultSet = resultSet;
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onPostExecute(String result) {
            Log.e("AllWork","Done by .....................................");
            if(result != null) {
                listener.onMigrationFinished(true, Constants.MIGRATION_SUCCESS, false, Constants.EMPTY);
            }
        }

        @SuppressLint("WrongThread")
        @Override
        protected String doInBackground(Void... voids) {
            JSONObject jsonObject = new JSONObject();
            try {

                for (int k = 0; k < resultSet.length(); k++) {

                    try {
                        jsonObject = resultSet.getJSONObject(k);
                        Log.e("jsonObjectOffline", String.valueOf(jsonObject));
                        OfflineOrderNo = jsonObject.getString("oflnordid");
                        System.out.println("BeforeChange1: " + jsonObject);
                        JSONObject jsonObjMerchant = jsonObject.getJSONObject("merchant");
                        System.out.println("BeforeChangejsonObjMerchant1: " + jsonObjMerchant);
                        jsonObjMerchant.put("createdby", Constants.USER_ID);
                        jsonObjMerchant.put("customdata", "");
                        System.out.println("AfterChangejsonObjMerchant1: " + jsonObjMerchant);
                        jsonObject.put("merchant", jsonObjMerchant);
                        System.out.println("AfterChange1: " + jsonObject);
                        System.out.println("SVTestinside101");
                        JsonServiceHandler.setParams(jsonObject.toString()); /////////////
                        JsonAccountObject = JsonServiceHandler.ServiceData();

                        reSetDefault();/////
                        strOrderId = ""; strStatus = ""; strMsg = ""; //////////////////////recent change Kumaravel
                        strStatus = JsonAccountObject.getString("status");
                        if(strStatus != null && !strStatus.isEmpty()) {
                            Log.e("return status", strStatus);
                            strMsg = JsonAccountObject.getString("message");
                            Log.e("return message", strMsg);

                            if (strStatus.equals("true")) {
                                strOrderId = JsonAccountObject.getJSONObject("data").getString("ordid");
                                Log.e("strOrderId", strOrderId);
                                String muserid = JsonAccountObject.getJSONObject("data").getString("muserid");

                                String updatedFlag = jsonObject.getJSONObject("merchant").getString("cflag");
                                Log.e("updatedFlag", updatedFlag);

                                String companyName = jsonObject.getString("mname");

                                db.execSQL("UPDATE merchanttable SET userid = " + muserid + ",cflag =" + "'" + updatedFlag + "'" + " WHERE companyname =" + "'" + companyName + "'");


                                Cursor curs3;
                                curs3 = db.rawQuery("SELECT rowid from merchanttable where userid=" + muserid, null);
                                Log.e("merchantrowid", String.valueOf(curs3.getCount()));
                                String merchantRowid = "";
                                if (curs3 != null && curs3.getCount() > 0) {

                                    if (curs3.moveToFirst()) {
                                        Log.e("rowid ", curs3.getString(0));
                                        merchantRowid = curs3.getString(0);

                                    }
                                }
                                Log.e("rowidmuserid ", muserid);
                                db.execSQL("UPDATE beat SET muserid = " + muserid + " ,flag =" + "'InActive'" + " WHERE mid =" + "'" + merchantRowid + "'");


                                Cursor curs4;
                                curs4 = db.rawQuery("SELECT muserid from beat where muserid=" + muserid, null);
                                Log.e("mid", String.valueOf(curs4.getCount()));
                                if (curs3 != null && curs3.getCount() > 0) {

                                    if (curs3.moveToFirst()) {
                                        Log.e("mid ", curs3.getString(0));

                                    }
                                }

                                Log.e("arraysize", String.valueOf(arraylistimagepath.size()));

                                String rowid = jsonObject.getJSONArray("orderdtls").getJSONObject(0).getString("rowid");
                                Log.e("rowid", rowid);

                                Cursor curs;
                                curs = db.rawQuery("SELECT ordimage from oredrdetail where oredrdetail.rowid=" + rowid, null);

                                Log.e("countorderimage", String.valueOf(curs.getCount()));
                                Log.e("columncount", String.valueOf(curs.getColumnCount()));


                                curs.moveToFirst();
                                while (curs.isAfterLast() == false) {

                                    int totalColumn = curs.getColumnCount();

                                    for (int i = 0; i < totalColumn; i++) {
                                        if (curs.getColumnName(i) != null) {
                                            Log.e("value", String.valueOf(curs.getColumnName(i)));
                                            try {

                                                if (curs.getString(i) != null) {
                                                    image = curs.getString(i).split(",");

                                                    Log.e("value", String.valueOf(curs.getString(i)));
                                                } else {
                                                    arraylistimagepath.add("0");
                                                }
                                            } catch (Exception e) {
                                                Log.d("TAG_NAME", e.getMessage());
                                                e.printStackTrace();
                                                if (listener != null) {
                                                    listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                                                    return null;
                                                }
                                            }
                                        }

                                    }

                                    curs.moveToNext();
                                }
                                curs.close();


                                for (int i = 0; i < image.length; i++) {
                                    arraylistimagepath.add(image[i]);
                                }

                                Log.e("arraysize", String.valueOf(arraylistimagepath.size()));
                                for (int m = 0; m < arraylistimagepath.size(); m++) {
                                    Log.e("inside", "inside");
                                    Log.e("inside", arraylistimagepath.get(0).toString());

                                    if (m == 0) {
                                        imagepath1 = arraylistimagepath.get(0).toString();
                                    } else if (m == 1) {
                                        imagepath2 = arraylistimagepath.get(1).toString();
                                    } else if (m == 2) {
                                        imagepath3 = arraylistimagepath.get(2).toString();
                                    } else if (m == 3) {
                                        imagepath4 = arraylistimagepath.get(3).toString();
                                    } else if (m == 4) {
                                        imagepath5 = arraylistimagepath.get(4).toString();
                                    } else if (m == 5) {
                                        imagepath6 = arraylistimagepath.get(5).toString();
                                    } else if (m == 6) {
                                        imagepath7 = arraylistimagepath.get(6).toString();
                                    } else if (m == 7) {
                                        imagepath8 = arraylistimagepath.get(7).toString();
                                    } else if (m == 8) {
                                        imagepath9 = arraylistimagepath.get(8).toString();
                                    } else if (m == 9) {
                                        imagepath10 = arraylistimagepath.get(9).toString();
                                    } else if (m == 10) {
                                        imagepath11 = arraylistimagepath.get(10).toString();
                                    } else if (m == 11) {
                                        imagepath12 = arraylistimagepath.get(11).toString();
                                    }

                                    Log.e("path", String.valueOf(arraylistimagepath.get(m)));

                                }


                                db.execSQL("UPDATE orderheader SET pushstatus = 'Success',onlineorderno =" + strOrderId + " WHERE oflnordid =" + OfflineOrderNo);
                                Cursor cur;
                                cur = db.rawQuery("SELECT pushstatus from orderheader where pushstatus ='Success'", null);
                                Log.e("HeaderStatusCount", String.valueOf(cur.getCount()));

                                SM_uploadToServerCheckout_NCR ufs = new SM_uploadToServerCheckout_NCR(
                                        imagepath1, imagepath2, imagepath3, imagepath4,
                                        imagepath5, imagepath6, imagepath7, imagepath8,
                                        imagepath9, imagepath10, imagepath11, imagepath12, strOrderId);
                                ufs.execute();///////////////////////////////////////////////remove lint


                                Log.e("length", String.valueOf(resultSet.length() - 1));

                                if (k == (resultSet.length() - 1)) {

                                    //////////////ordercancel();
                                    Log.e("ordercancel_IF", "ordercancel");
                                }

                            } else {
                                if (strMsg != null && !strMsg.isEmpty() && strMsg.equals(Constants.ORDER_ALREADY_EXIST)) {
                                    db.execSQL("UPDATE orderheader SET pushstatus = 'Success' WHERE oflnordid =" + OfflineOrderNo); // Kumaravel 30-06-2019
                                } else if (strMsg != null && !strMsg.isEmpty() && strMsg.equals("Network unavailable, Please try again later")) {
                                    db.execSQL("UPDATE orderheader SET pushstatus = 'Pending' WHERE oflnordid =" + OfflineOrderNo); // Kumaravel
                                    if (listener != null) {
                                        listener.onMigrationFinished(false, Constants.MIGRATION_FAILED, true, strMsg);
                                        return null;
                                    }
                                    break;
                                } else if (strMsg != null && !strMsg.isEmpty() && strMsg.equals(Constants.ORDER_SAVE_FAILED)) {

                                    db.execSQL("UPDATE orderheader SET pushstatus = 'Pending' WHERE oflnordid =" + OfflineOrderNo); // Kumaravel

                                } else {
                                    //This may be need to delete order
                                    db.execSQL("UPDATE orderheader SET pushstatus = 'Failed' WHERE oflnordid =" + OfflineOrderNo); // Kumaravel
                                }
                            }
                        }else {
                            db.execSQL("UPDATE orderheader SET pushstatus = 'Pending' WHERE oflnordid =" + OfflineOrderNo); // Kumaravel
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e("Order.Mig",".................Exception..." + e.getMessage());
                    }
                }

            } catch (Exception e) {
                Log.e("Order.Mig",".................Exception..." + e.getMessage());
                e.printStackTrace();
                if (listener != null) {
                    listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                    return null;
                }
            }
            return "Success";
        }
    }




























    public void ordercancel(){

        Log.e("test_ordercancel","test");

        Cursor curs;
        curs=db.rawQuery("SELECT onlineorderno,oflnordid from orderheader where pushstatus ='Cancelled'", null);
        Log.e("HeaderCountCanled", String.valueOf(curs.getCount()));

        if(curs!=null && curs.getCount() > 0) {

            curs.moveToFirst();
            final JSONArray resultDetail = new JSONArray();

            while (curs.isAfterLast() == false) {

                int totalColumn = curs.getColumnCount();
                JSONObject rowObject = new JSONObject();

                for (int i = 0; i < totalColumn; i++) {

                    if (curs.getColumnName(i) != null) {

                        try {

                            if (curs.getString(i) != null) {
                                Log.d("TAG_NAME", curs.getString(i));
                                rowObject.put(curs.getColumnName(i), curs.getString(i));
                            } else {
                                rowObject.put(curs.getColumnName(i), "");
                            }

                        } catch (Exception e) {
                            Log.d("TAG_NAME", e.getMessage());
                            e.printStackTrace();
                            if(listener != null) {
                                listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                                return;
                            }
                        }
                    }
                }
                resultDetail.put(rowObject);
                curs.moveToNext();
            }
            Log.e("sendordarray", resultDetail.toString());
            curs.close();

            //pushing values to mstr DB
            new AsyncTask<Void, Void, Void>() {
                ProgressDialog dialog;
                String strStatus = "";
                String strMsg = "",strOrderId ="";

                @Override
                protected void onPreExecute() {

                }

                @Override
                protected void onPostExecute(Void result) {


                }
                @Override
                protected Void doInBackground(Void... params) {

                    JSONObject jsonObject = new JSONObject();
                    JSONObject jsonObject1 = new JSONObject();

                    try{

                        Log.e("arraysize", String.valueOf(resultDetail.length()));

                        for(int k=0;k<resultDetail.length();k++)
                        {
                            jsonObject1 = resultDetail.getJSONObject(k);
                            Log.e("values cancelled", resultDetail.getJSONObject(k).getString("onlineorderno"));

                            try {
                                jsonObject.put("ordstatus","Cancelled");
                            } catch (JSONException e) {
                                e.printStackTrace();
                                if(listener != null) {
                                    listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                                    return null;
                                }
                            } catch (Exception e){
                                e.printStackTrace();
                                if(listener != null) {
                                    listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                                    return null;
                                }
                            }

                            JsonServiceHandler = new JsonServiceHandler(Utils.strdeleteorder + resultDetail.getJSONObject(k).getString("onlineorderno") ,jsonObject.toString(),
                                    mContext);
                            JsonAccountObject = JsonServiceHandler.ServiceData();

                            try {

                                strStatus = JsonAccountObject.getString("status");
                                Log.e("return status", strStatus);
                                strMsg = JsonAccountObject.getString("message");
                                Log.e("return message", strMsg);

                                if(strStatus.equals("true"))
                                {

                                    Log.e("return inside", "inside1");
                                    db.execSQL("DELETE FROM oredrdetail where ( ordstatus = 'Cancelled' ) AND ( oflnordid = " + resultDetail.getJSONObject(k).getString("oflnordid") + " ) ");
                                    db.execSQL("DELETE FROM orderheader where ( pushstatus = 'Cancelled' ) AND ( oflnordid = " + resultDetail.getJSONObject(k).getString("oflnordid")  + " ) ");

                                    Log.e("count", String.valueOf(resultDetail.getJSONObject(k).getString("oflnordid")));



                                    if (k == (resultDetail.length() - 1)){

                                        clientvisit();
                                    }

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                if(listener != null) {
                                    listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                                    return null;
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                if(listener != null) {
                                    listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                                    return null;
                                }
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        if(listener != null) {
                            listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                            return  null;
                        }
                    }
                    return null;

                }
            }.execute(new Void[]{});


        }
        else {
            Log.e("inside_ELSEclientvisit", "clientclientvisit");
            clientvisit();
        }
    }

    public void clientvisit(){

        Log.e("insideclientvisit", "clientclientvisit");
        Cursor curs;

        curs = db.rawQuery("SELECT * FROM clientvisit where clientstatus ='Pending'", null);

        Log.e("clientpendingcount", String.valueOf(curs.getCount()));

        if (curs != null && curs.getCount() > 0) {

            curs.moveToFirst();
            final JSONArray resultDetail = new JSONArray();

            while (curs.isAfterLast() == false) {

                int totalColumn = curs.getColumnCount();
                JSONObject rowObject = new JSONObject();

                for (int i = 0; i < totalColumn; i++) {

                    if (curs.getColumnName(i) != null) {

                        try {

                            if (curs.getString(i) != null) {
                                Log.d("TAG_NAME", curs.getString(i));
                                rowObject.put(curs.getColumnName(i), curs.getString(i));

                            }
                            else {
                                rowObject.put(curs.getColumnName(i), "");
                            }


                        } catch (Exception e) {
                            Log.d("TAG_NAME", e.getMessage());
                            e.printStackTrace();
                            if(listener != null) {
                                listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                                return;
                            }
                        }
                    }
                }
                resultDetail.put(rowObject);
                curs.moveToNext();
            }
            Log.e("sendordarray", resultDetail.toString());
            curs.close();

            new AsyncTask<Void, Void, Void>() {
                ProgressDialog dialog;
                String strStatus = "";
                String strMsg = "";



                @Override
                protected void onPreExecute() {

                }

                @Override
                protected void onPostExecute(Void result) {

                }


                @Override
                protected Void doInBackground(Void... params) {
                    JSONObject jsonObject = new JSONObject();

                    try {

                        Log.e("arraysize", String.valueOf(resultDetail.length()));

                        for (int k = 0; k < resultDetail.length(); k++) {
                            jsonObject = resultDetail.getJSONObject(k);

                            try {
                                rowid = jsonObject.getString("id");
                                Log.e("id", rowid);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            JsonServiceHandler = new JsonServiceHandler(Utils.strclientvisitsales, jsonObject.toString(), mContext);
                            JsonAccountObject = JsonServiceHandler.ServiceData();


                            try {

                                strStatus = JsonAccountObject.getString("status");
                                Log.e("return status", strStatus);
                                strMsg = JsonAccountObject.getString("message");
                                Log.e("return message", strMsg);

                                if (strStatus.equals("true")) {

                                    db.execSQL("UPDATE clientvisit SET clientstatus = 'Success' WHERE id =" + rowid);
                                    Cursor cur;

                                    cur = db.rawQuery("SELECT * FROM clientvisit  WHERE  clientstatus = 'Success'", null);
                                    Log.e("HeaderCountcompleted", String.valueOf(cur.getCount()));

                                    if (k == (resultDetail.length() - 1)){
                                        getDetails();
                                    }
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                if(listener != null) {
                                    listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                                    return null;
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                if(listener != null) {
                                    listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                                    return null;
                                }
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        if(listener != null) {
                            listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                            return null;
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                        if(listener != null) {
                            listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                            return null;
                        }
                    }
                    return null;
                }
            }.execute(new Void[]{});



        }else{
            getDetails();
            Log.e("getDetails_ELSE","getdetails");
        }
    }

    private void getDetails() {
        Log.e("getDetails","getDetails");
        // TODO Auto-generated method stub
        new AsyncTask<Void, Void, Void>() {
            ProgressDialog dialog;
            String strStatus = "";
            String strMsg = "";

            @Override
            protected void onPreExecute() {

            }

            @Override
            protected void onPostExecute(Void result) {

                try {

                    String PaymentStatus=(JsonAccountObject.getString("pymtstatus"));
                    Log.e("PaymentStatus", PaymentStatus);


                    if(PaymentStatus.equals("Credit")){
                        db.execSQL("UPDATE signintable SET paymentStatus = 'Credit'");
                        Cursor cur;

                        cur = db.rawQuery("SELECT * FROM signintable  WHERE  paymentStatus = 'Credit'", null);
                        Log.e("HeaderCountcompleted", String.valueOf(cur.getCount()));

                    }
                    NoOrders();
                    Log.e("CreditNoOrders","NoOrders");
                } catch (JSONException e) {
                    e.printStackTrace();
                    if(listener != null) {
                        listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                        return;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if(listener != null) {
                        listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                        return;
                    }
                }
            }

            @Override
            protected Void doInBackground(Void... params) {
                try{
                    JSONObject jsonObject = new JSONObject();


                    JsonServiceHandler = new JsonServiceHandler(Utils.strGetProfileDetail+Constants.USER_ID, mContext);
                    JsonAccountObject = JsonServiceHandler.ServiceDataGet();
                } catch (Exception e) {
                    e.printStackTrace();
                    if(listener != null) {
                        listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                        return null;
                    }
                }

                return null;
            }
        }.execute(new Void[]{});
    }

    public void NoOrders() {
        Log.e("INSIDE_NoOrders","NoOrders");
        final JSONArray resultSet = new JSONArray();
        Cursor curs;
        curs = db.rawQuery("SELECT * from orderheader where pushstatus IN ('No Order', 'Outlet')", null);
        Log.e("HeaderCountoutlet", String.valueOf(curs.getCount()));
        if (curs != null && curs.getCount() > 0) {
            curs.moveToFirst();
            while (curs.isAfterLast() == false) {

                int totalColumn = curs.getColumnCount();
                JSONObject rowObject = new JSONObject();

                for (int i = 0; i < totalColumn; i++) {
                    if (curs.getColumnName(i) != null) {

                        try {

                            if (curs.getString(i) != null) {
                                if (!curs.getString(i).equals("New User")) {
                                    Log.d("TAG_NAME", curs.getString(i));
                                    rowObject.put(curs.getColumnName(i), curs.getString(i));
                                }

                            } else {
                                if (!curs.getColumnName(i).equals("muserid")) {
                                    rowObject.put(curs.getColumnName(i), "");
                                }

                            }
                        } catch (Exception e) {
                            Log.d("TAG_NAME", e.getMessage());
                            e.printStackTrace();
                            if(listener != null) {
                                listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                                return;
                            }
                        }
                    }

                }


                try {

                    Cursor curs1;
                    curs1 = db.rawQuery("SELECT * from dealertable", null);
                    Log.e("dEALER", String.valueOf(curs1.getCount()));
                    if (curs1.getCount() > 0) {
                        curs1.moveToFirst();
                        selected_dealer = curs1.getString(curs1
                                .getColumnIndex(DatabaseHandler.KEY_Duserid));
                    }

                    Cursor cursor2;
                    Merchantname = rowObject.getString("mname");

                    cursor2 = db.rawQuery("SELECT * from merchanttable where companyname =" + "'" + Merchantname + "'", null);
                    Log.e("HeaderCountCanled", String.valueOf(cursor2.getCount()));

                    cursor2.moveToFirst();
                    while (cursor2.isAfterLast() == false) {

                        int Columncount = cursor2.getColumnCount();
                        Log.e("merchantColumn", String.valueOf(cursor2.getColumnCount()));
                        JSONObject merchantdetail = new JSONObject();
                        for (int j = 0; j < Columncount; j++) {
                            if (cursor2.getColumnName(j) != null) {
                                try {

                                    if (cursor2.getString(j) != null) {
                                        if (!cursor2.getString(j).equals("New User")) {

                                            merchantdetail.put(cursor2.getColumnName(j), cursor2.getString(j));

                                        }
                                    } else {
                                        if (!cursor2.getColumnName(j).equals("userid")) {
                                            merchantdetail.put(cursor2.getColumnName(j), "");
                                        }

                                    }


                                } catch (Exception e) {
                                    Log.d("Merchant_Exception", e.getMessage());
                                    e.printStackTrace();
                                    if(listener != null) {
                                        listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                                        return;
                                    }
                                }
                            }

                        }
                        merchantdetail.put("usertype", "M");
                        merchantdetail.put("duserid", selected_dealer);
                        rowObject.put("merchant", merchantdetail);
                        cursor2.moveToNext();
                    }

                    cursor2.close();

                    Log.e("Detailarray", rowObject.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                    if(listener != null) {
                        listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                        return;
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    if(listener != null) {
                        listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                        return;
                    }
                }


                Log.e("jsonarray", rowObject.toString());

                JSONArray resultDetail = new JSONArray();
                try {

                    Cursor cursor;
                    cursor = db.rawQuery("SELECT * from oredrdetail where oredrdetail.oflnordid=" + rowObject.getString("oflnordid"), null);
                    Log.e("DetailCount", String.valueOf(cursor.getCount()));
                    cursor.moveToFirst();
                    while (cursor.isAfterLast() == false) {

                        int Columncount = cursor.getColumnCount();
                        Log.e("columnCount", String.valueOf(cursor.getColumnCount()));
                        JSONObject detailObject = new JSONObject();

                        for (int i = 0; i < 21; i++) //17
                        {
                            if (cursor.getColumnName(i) != null) {

                                try {

                                    if (cursor.getString(i) != null) {
                                        Log.d("TAG_NAME", cursor.getString(i));
                                        detailObject.put(cursor.getColumnName(i), cursor.getString(i));
                                    } else {
                                        detailObject.put(cursor.getColumnName(i), "");
                                    }
                                } catch (Exception e) {
                                    Log.d("TAG_NAME", e.getMessage());
                                    e.printStackTrace();
                                    if(listener != null) {
                                        listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                                        return;
                                    }
                                }
                            }

                        }

                        resultDetail.put(detailObject);
                        cursor.moveToNext();
                    }

                    cursor.close();

                    Log.e("Detailarray", resultDetail.toString());

                } catch (JSONException e) {
                    e.printStackTrace();
                    if(listener != null) {
                        listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                        return;
                    }
                }
                try {
                    rowObject.put("orderdtls", resultDetail);
                } catch (JSONException e) {
                    e.printStackTrace();
                    if(listener != null) {
                        listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                        return;
                    }
                }
                resultSet.put(rowObject);
                curs.moveToNext();
            }
            curs.close();
            Log.e("SaveArray000", resultSet.toString());
            Log.e("CountLength", String.valueOf(resultSet.length()));
            for (int i = 0; i < resultSet.length(); i++) {

                try {
                    JSONObject arrayMerchants = (JSONObject) resultSet.get(i);
                    Log.e("arrayMerchants--->" + i + " ", "" + arrayMerchants);
                } catch (Exception e) {
                    e.printStackTrace();
                    if(listener != null) {
                        listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                        return;
                    }
                }
            }


            new AsyncTask<Void, Void, Void>() {
                String strStatus = "";
                String strMsg = "", strOrderId = "";

                @Override
                protected void onPreExecute() {

                }

                @Override
                protected void onPostExecute(Void result) {

                }

                @Override
                protected Void doInBackground(Void... params) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        for (int k = 0; k < resultSet.length(); k++) {
                            jsonObject = resultSet.getJSONObject(k);
                            Log.e("jsonObject1", String.valueOf(jsonObject));
                            OfflineOrderNo = jsonObject.getString("oflnordid");
                            JSONObject jsonObjMerchant = jsonObject.getJSONObject("merchant");
                            Log.e("jsonObjMerchantOffline", String.valueOf(jsonObjMerchant));
                            String strJsonObjCustomdata = "" + jsonObjMerchant.get("customdata");
                            Log.e("strJsonObjCustomdata", strJsonObjCustomdata);
                            JSONArray jsonArrayCustomData = new JSONArray(strJsonObjCustomdata);
                            jsonObjMerchant.put("createdby", Constants.USER_ID);
                            Log.e("createdbyOFFLINE",Constants.USER_ID);
                            jsonObjMerchant.put("customdata", jsonArrayCustomData);
                            jsonObject.put("merchant", jsonObjMerchant);
                            Log.e("SHIURL :",Utils.strsavemerchantOderdetail);
                            Log.e("SHIOBJ :",jsonObject.toString());
                            JsonServiceHandler = new JsonServiceHandler(Utils.strsavemerchantOderdetail, jsonObject.toString(), mContext);
                            JsonAccountObject = JsonServiceHandler.ServiceData();
                            Log.e("SHIRESPONSE :",JsonAccountObject.toString());
                            try {

                                strStatus = JsonAccountObject.getString("status");
                                Log.e("return status", strStatus);
                                strMsg = JsonAccountObject.getString("message");
                                Log.e("return message", strMsg);
                                if (strStatus.equals("true")) {
                                    strOrderId = JsonAccountObject.getJSONObject("data").getString("ordid");
                                    Log.e("strOrderId", strOrderId);
                                    String muserid = JsonAccountObject.getJSONObject("data").getString("muserid");
                                    Log.e("muserid", muserid);

                                    String updatedFlag = jsonObject.getJSONObject("merchant").getString("cflag");

                                    db.execSQL("UPDATE merchanttable SET userid = " + muserid + ",cflag =" + "'" + updatedFlag + "'" + " WHERE mobileno =" + "'" + jsonObject.getJSONObject("merchant").getString("mobileno") + "'");

                                    Cursor curs1;
                                    curs1 = db.rawQuery("SELECT * from merchanttable where userid=" + muserid, null);
                                    Log.e("Outlet/Merchant Count", String.valueOf(curs1.getCount()));

                                    if (curs1 != null && curs1.getCount() > 0) {

                                        if (curs1.moveToFirst()) {
                                            Log.e("merchant name", curs1.getString(0));
                                            Log.e("merchant id", curs1.getString(1));
                                            Log.e("merchant flag", curs1.getString(2));
                                        }
                                    }


                                    db.execSQL("DELETE FROM oredrdetail where ( ordstatus IN ('No Order', 'Outlet')) AND ( oflnordid = " + OfflineOrderNo + " ) ");
                                    db.execSQL("DELETE FROM orderheader where ( pushstatus IN ('No Order', 'Outlet')) AND ( oflnordid = " + OfflineOrderNo + " ) ");
                                    Cursor curs;
                                    Log.e("OfflineOrderNonew", String.valueOf(OfflineOrderNo));
                                    curs = db.rawQuery("SELECT * from orderheader where pushstatus IN ('No Order', 'Outlet')", null);
                                    Log.e("exit count", String.valueOf(curs.getCount()));
                                    try {
                                        String strQuery=" UPDATE beat SET muserid=" + muserid + ",flag='Inactive' WHERE rowid = (select max(rowid) from beat where flag='OffInactive') and flag='OffInactive'";
                                        Log.e("SHIstrQuery:",strQuery);
                                        db.execSQL(strQuery);

                                        Cursor cursorBeatDetails= db.rawQuery("select * from beat",null);

                                        Log.e("BEATS:",""+cursorBeatDetails);
                                        if(cursorBeatDetails!=null) {
                                            Log.e("BeatCount:", ""+cursorBeatDetails.getCount());
                                            cursorBeatDetails.moveToFirst();
                                            for(int i=0;i<cursorBeatDetails.getCount();i++)
                                            {   String strValues="";
                                                for(int j=0;j<cursorBeatDetails.getColumnCount();j++)
                                                {
                                                    strValues+="~"+cursorBeatDetails.getString(j);
                                                }
                                                System.out.println("Row "+i+":"+strValues);
                                                cursorBeatDetails.moveToNext();
                                            }

                                        }
                                        Log.e("AFTER_UPDATE_VALUES->",cursorBeatDetails.toString());

                                        System.out.println(cursorBeatDetails);



                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        if(listener != null) {
                                            listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                                            return null;
                                        }
                                        Log.e("exit Exception", String.valueOf(e));

                                    }
                                    Log.e("exit UPDATE", String.valueOf(curs.getCount()));
                                    if (k == (resultSet.length() - 1)) {
                                        Log.e("IF_BEAT_UPDATE",String.valueOf(resultSet.length()));
                                        beatupdate();
                                    }
                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                                if(listener != null) {
                                    listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                                    return null;
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                if(listener != null) {
                                    listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                                    return null;
                                }
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        if(listener != null) {
                            listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                            return null;
                        }
                    }catch (Exception e){
                        if(listener != null) {
                            listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                            return null;
                        }
                        e.printStackTrace();
                    }
                    return null;

                }
            }.execute(new Void[]{});
        }else{
            beatupdate();
            Log.e("ELSE_BEAT_UPDATE","beatupdate");
        }
    }

    public  void beatupdate() {
        Log.e("INSIDE_beatupdate","beatupdate");
        Cursor cursor1;
        Cursor cursor2;
        cursor1 = db.rawQuery("SELECT * from beat where  flag ='Inactive'", null);
        if(cursor1 !=null)
        {
            System.out.println("Count_1_(IA) :"+ cursor1.getCount());
        }

        cursor2 = db.rawQuery("SELECT * from beat where  flag ='OffInactive' ", null);
        if(cursor2 !=null)
        {
            System.out.println("Count_2_(OIA) :"+ cursor2.getCount());
        }

        Cursor cursor;
        cursor = db.rawQuery("SELECT * from beat where  flag ='Inactive' or flag ='OffInactive' ", null);
        Log.e("SHICursor:",cursor.toString());
        Log.e("SHICursor Count:",""+cursor.getCount());

        // Log.e("SHiCUrItems:",databaseHandler.getCursorItems(cursor).toString());
//        for(int i=0;i<cursor.getCount();i++)
//        {
//
//        }
        Log.e("beatCountNetwork", String.valueOf(cursor.getCount()));
        if (cursor != null && cursor.getCount() > 0) {
            System.out.println("Count_2_(IA+OIA) :"+ cursor.getCount());
            cursor.moveToFirst();
            final JSONArray beatarray = new JSONArray();
            while (cursor.isAfterLast() == false) {

                int Columncount = cursor.getColumnCount();
                JSONObject beatobject = new JSONObject();


                for (int i = 0; i < Columncount; i++) {
                    if (cursor.getColumnName(i) != null) {

                        try {

                            if (cursor.getString(i) != null) {
                               /* if(cursor.getColumnName(i).equals("beattype")){
                                    if(cursor.getString(i).equalsIgnoreCase("C")){
                                        }
                                }*/
                                if(cursor.getColumnName(i).equals("suserid") ){
                                    if(!cursor.getString(18).equalsIgnoreCase("C") ){
                                        beatobject.put(cursor.getColumnName(i), cursor.getString(i));
                                    }

                                }else{
                                    beatobject.put(cursor.getColumnName(i), cursor.getString(i));

                                }
                                Log.d("TAG_NAME", cursor.getString(i));
                                Log.e("EXPECTED", String.valueOf(beatobject));
                            } else {
                                beatobject.put(cursor.getColumnName(i), "");

                            }
                        } catch (Exception e) {
                            Log.d("TAG_NAME", e.getMessage());
                            e.printStackTrace();
                            if(listener != null) {
                                listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                                return;
                            }
                        }
                    }

                }


                beatarray.put(beatobject);

                cursor.moveToNext();

                System.out.println("KAR_WHILE_INISIDE_-->"+beatarray.toString());
                System.out.println("KAR_WHILE_INISIDE_LEN:-->"+beatarray.length());
            }

            cursor.close();

            Log.e("Detailarray", beatarray.toString());

            new AsyncTask<Void, Void, Void>() {
                String strStatus = "";
                String strMsg = "";
                int intOnlineMerchantCount=0;
                @Override
                protected void onPreExecute() {

                }

                @Override
                protected void onPostExecute(Void result) {
                    //int intCount=databaseHandler.getMerchantCount();
                    //db.execSQL("select count(*)");
                    AddMerchantNew.textViewLocalMC1.setText("Online Count : "+intOnlineMerchantCount);
                    //int intCurrentValue=Integer.parseInt(""+AddMerchantNew.textViewLocalMC2.getText());
                    AddMerchantNew.textViewLocalMC2.setText("Offline Count : " + (intOnlineMerchantCount-intOnlineMerchantCount));
                    // AddMerchantNew.textViewLocalMC2.setText("Offline Count : " + (intOnlineMerchantCount-(Constants.onLineCount+intCurrentValue)));

                }

                @Override
                protected Void doInBackground(Void... params) {
                    JSONObject jsonObject = new JSONObject();
                    try {

                        System.out.println("KARBA:"+beatarray);
                        System.out.println("KARBALEN:"+beatarray.length());


                        for (int k = 0; k < beatarray.length(); k++) {
                            System.out.println("KARBAIN--->:"+k+":"+beatarray.length());

                            jsonObject = beatarray.getJSONObject(k);
                            beatrowid = jsonObject.getString("rowid");
                            Log.e("SHI_B_URL :",Utils.strsavebeat);
                            Log.e("SHIOBJBEAT :",jsonObject.toString());


                            JsonServiceHandler = new JsonServiceHandler(Utils.strsavebeat, jsonObject.toString(), mContext);
                            JsonAccountObject = JsonServiceHandler.ServiceData();

                            JSONObject jsonObjReqCount = new JSONObject();
                            try {
                                jsonObjReqCount.put("userid", Constants.USER_ID);
                                jsonObjReqCount.put("usertype", Constants.USER_TYPE);


                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                                if(listener != null) {
                                    listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                                    return null;
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                                if(listener != null) {
                                    listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                                    return null;
                                }
                            }
                            JsonServiceHandler = new JsonServiceHandler(Utils.strGetMerchantList, jsonObjReqCount.toString(), mContext);
                            JSONObject jsonOnlineMerchantCount = JsonServiceHandler.ServiceData();
                            intOnlineMerchantCount=jsonOnlineMerchantCount.getInt("count");
                            Constants.onLineCount=intOnlineMerchantCount;
                            Log.e("SHILOG","Before calling db exec...");

                            System.out.println("intOnlineMerchantCount777"+intOnlineMerchantCount);

                            String strQuery = "update  constants set `"+"value"+"`='"+intOnlineMerchantCount+"' where `"+"key"+"` = '"+"OUTLETCOUNT"+"'";
                            db.execSQL(strQuery);
                            Log.e("SHILOG","Executed ..... OUTLETCOUNT set");
//                            Log.e("SHILOG","Before calling database handler exec...");
//                            databaseHandler.setConstantValue("OUTLETCOUNT",""+intOnlineMerchantCount);


                            try {
                                strStatus = JsonAccountObject.getString("status");
                                Log.e("return status", strStatus);
                                strMsg = JsonAccountObject.getString("message");
                                Log.e("return message", strMsg);

                                if (strStatus.equals("true")) {

                                    db.execSQL("UPDATE beat SET flag = 'Active' WHERE rowid =" + "'" + beatrowid + "'");
                                    if (k == (beatarray.length() - 1)) {

                                    }
                                }else{

                                }


                            } catch (JSONException e) {
                                e.printStackTrace();
                                if(listener != null) {
                                    listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                                    return null;
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                if(listener != null) {
                                    listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                                    return null;
                                }
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        if(listener != null) {
                            listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                            return null;
                        }
                    }
                    return null;

                }
            }.execute(new Void[]{});

        }else{

        }
    }
}
