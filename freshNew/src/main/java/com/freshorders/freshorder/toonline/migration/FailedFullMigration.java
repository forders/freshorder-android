package com.freshorders.freshorder.toonline.migration;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.crashreport.AppCrashUtil;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.toonline.FailedOrderMigrateToOnline;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.SM_uploadToServerCheckout_NCR;
import com.freshorders.freshorder.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FailedFullMigration {

    private SQLiteDatabase db;

    public static String selected_dealer = "";
    private String Merchantname = "";
    private  ArrayList<String> arraylistimagepath = new ArrayList<String>();

    private static String migPath = MyApplication.getInstance().getMigPath();

    public synchronized void migration() {

        try {
            Cursor curs;
            curs = db.rawQuery("SELECT * from orderheader where pushstatus ='Failed'", null);

            Log.e("HeaderCount", String.valueOf(curs.getCount()));
            final JSONArray resultSet = new JSONArray();

            if (curs.getCount() > 0) {

                //new PrefManager(mContext.getApplicationContext()).setBooleanDataByKey(PrefManager.KEY_IS_FAILED_ORDER_MIGRATION_START, true);

                curs.moveToFirst();
                while (!curs.isAfterLast()) {

                    int totalColumn = curs.getColumnCount();
                    JSONObject rowObject = new JSONObject();

                    for (int i = 0; i < totalColumn; i++) {

                        if (curs.getColumnName(i) != null) {

                            try {

                                if (curs.getString(i) != null) {
                                    if (!curs.getString(i).equals("New User")) {
                                        Log.d("TAG_NAME", curs.getString(i));
                                        rowObject.put(curs.getColumnName(i), curs.getString(i));
                                    }
                                  /*  Log.d("TAG_NAME", curs.getString(i));
                                    rowObject.put(curs.getColumnName(i), curs.getString(i));*/
                                } else {

                                    if (!curs.getColumnName(i).equals("muserid")) {
                                        rowObject.put(curs.getColumnName(i), "");
                                    }

                                }
                            } catch (Exception e) {
                                //e.printStackTrace();
                                continue;
                            }
                        }
                    }


                    try {

                        Cursor curs1;
                        curs1 = db.rawQuery("SELECT userid from dealertable", null);
                        Log.e("dEALER", String.valueOf(curs1.getCount()));
                        if (curs1.getCount() > 0) {
                            curs1.moveToFirst();
                            selected_dealer = curs1.getString(0);
                        }


                        Cursor cursor2;
                        Merchantname = rowObject.getString("mname");

                        cursor2 = db.rawQuery("SELECT * from merchanttable where companyname =" + "'" + Merchantname + "'", null);
                        Log.e("HeaderCountCanled", String.valueOf(cursor2.getCount()));

                        cursor2.moveToFirst();
                        while (!cursor2.isAfterLast()) {

                            int Columncount = cursor2.getColumnCount();
                            Log.e("merchantColumn", String.valueOf(cursor2.getColumnCount()));
                            JSONObject merchantdetail = new JSONObject();
                            for (int j = 0; j < Columncount; j++) {

                                try {
                                        if (cursor2.getColumnName(j) != null) {

                                            if (cursor2.getColumnName(j).equals("cflag")) {
                                                if (cursor2.getString(j).equals("R")) {
                                                    merchantdetail.put(cursor2.getColumnName(j), "G");
                                                } else if (cursor2.getString(j).equals("G")) {
                                                    merchantdetail.put(cursor2.getColumnName(j), "");
                                                } else {
                                                    merchantdetail.put(cursor2.getColumnName(j), "");
                                                }

                                            } else {
                                                if (cursor2.getString(j) != null) {
                                                    if (!cursor2.getString(j).equals("New User")) {

                                                        merchantdetail.put(cursor2.getColumnName(j), cursor2.getString(j));

                                                    }
                                                    //merchantdetail.put(cursor2.getColumnName(j) ,  cursor2.getString(j) );
                                                } else {
                                                    if (!cursor2.getColumnName(j).equals("userid")) {
                                                        merchantdetail.put(cursor2.getColumnName(j), "");
                                                    }

                                                }
                                            }
                                        }
                                    } catch(Exception e){
                                        Log.d("Merchant_Exception", e.getMessage());
                                        //e.printStackTrace();
                                        continue;
                                }
                            }

                            merchantdetail.put("usertype", "M");
                            merchantdetail.put("duserid", selected_dealer);
                            rowObject.put("merchant", merchantdetail);
                            cursor2.moveToNext();
                        }
                        cursor2.close();
                        Log.e("Detailarray", rowObject.toString());

                    } catch (Exception e) {
                        AppCrashUtil.writeStringToBugFile(migPath,AppCrashUtil.getStackTrace(e));
                    }

                    Log.e("jsonarray", rowObject.toString());

                    JSONArray resultDetail = new JSONArray();
                    try {

                        Cursor cursor;
                        cursor = db.rawQuery("SELECT * from oredrdetail where oredrdetail.oflnordid=" + rowObject.getString("oflnordid"), null);
                        Log.e("DetailCount", String.valueOf(cursor.getCount()));
                        cursor.moveToFirst();
                        while (!cursor.isAfterLast()) {

                            int Columncount = cursor.getColumnCount();
                            JSONObject detailObject = new JSONObject();

                            for (int i = 0; i < Columncount; i++) {
                                if (cursor.getColumnName(i) != null) {

                                    try {

                                        if (cursor.getString(i) != null) {
                                            Log.d("TAG_NAME", cursor.getString(i));
                                            detailObject.put(cursor.getColumnName(i), cursor.getString(i));
                                        } else {
                                            detailObject.put(cursor.getColumnName(i), "");

                                        }
                                    } catch (Exception e) {
                                        Log.d("TAG_NAME", e.getMessage());
                                        AppCrashUtil.writeStringToBugFile(migPath,AppCrashUtil.getStackTrace(e));
                                    }
                                }

                            }
                            resultDetail.put(detailObject);
                            cursor.moveToNext();
                        }
                        Log.e("arraylist", String.valueOf(arraylistimagepath.size()));
                        cursor.close();
                        Log.e("Detailarray", resultDetail.toString());

                    } catch (Exception e) {
                        AppCrashUtil.writeStringToBugFile(migPath,AppCrashUtil.getStackTrace(e));
                    }
                    try {
                        rowObject.put("orderdtls", resultDetail);
                    } catch (Exception e) {
                        AppCrashUtil.writeStringToBugFile(migPath,AppCrashUtil.getStackTrace(e));
                    }
                    resultSet.put(rowObject);
                    curs.moveToNext();
                }
                curs.close();
                Log.e("SaveArray111", resultSet.toString());
                //JsonServiceHandler = new JsonServiceHandler(Utils.strsavemerchantOderdetail, mContext);
                //new FailedOrderMigrateToOnline.MigrationAsyncTask(db, listener, databaseHandler, JsonServiceHandler, resultSet).execute();
            } else {
                Log.e("else_ordercancel", "ordercancel");
                ///////////ordercancel();
            }
        }catch (Exception e){
            AppCrashUtil.writeStringToBugFile(migPath,AppCrashUtil.getStackTrace(e));
        }
    }

}
