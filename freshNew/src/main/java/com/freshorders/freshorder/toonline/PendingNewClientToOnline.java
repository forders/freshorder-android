package com.freshorders.freshorder.toonline;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.ui.AddMerchantNew;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.NetworkUtil;
import com.freshorders.freshorder.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PendingNewClientToOnline {

    private PendingNewClientToOnline.ServerNewClientMigrationListener listener;
    private PendingNewClientToOnline.NewClientCompleteListener workListener;
    private static int migrationCount = 0;
    private static String M_USER_ID = "";
    private JsonServiceHandler JsonServiceHandlerBeatUpdate;
    private JsonServiceHandler JsonServiceHandlerGetMerchantCount;
    private static String OFFLINE_MERCHANT_NO = "";

    public PendingNewClientToOnline(Context context, PendingNewClientToOnline.NewClientCompleteListener workListener) {
        this.context = context;
        this.workListener = workListener;
        db = context.openOrCreateDatabase("freshorders", 0, null);
        JsonServiceHandlerBeatUpdate = new JsonServiceHandler(Utils.strsavebeat, context);
        JsonServiceHandlerGetMerchantCount = new JsonServiceHandler(Utils.strGetMerchantList, context);
        databaseHandler = new DatabaseHandler(context);
    }

    public interface NewClientCompleteListener{
        void onWorkSuccess();
        void onWorkFailed(String msg);

    }



    public interface ServerNewClientMigrationListener{
        void onMigrationFinished();
        void onMigrationFailed(String msg);
    }

    public interface LocalMigrationListener{
        void OnMigrationSuccess();
        void OnLocalMigrationFailed(String errorMSG);
    }



    public static DatabaseHandler databaseHandler;
    public static com.freshorders.freshorder.utils.JsonServiceHandler JsonServiceHandler;
    public static JSONObject JsonAccountObject = null;
    public static JSONArray JsonAccountArray = null;
    public static String payment_type,orderdate,Orderno,flag,productStatus,mode,paymenttype,pushstauts,OfflineOrderNo,rowid,beatrowid, orderBeatId = "";
    public static int year,month,day,hr,mins,sec,deliverydt;
    Context context;
    private String selected_dealer;

    private SQLiteDatabase db,sp;
    public  static ArrayList<String> arraylistimagepath;
    private static String DBNAME = "freshorders.db";
    private static String Table = "orderheader";
    public int orderimage=0;

    public void startPendingNewClientMigration() throws Exception{

        try {
            String status = NetworkUtil.getConnectivityStatusString(context);

            //  Toast.makeText(context, status, Toast.LENGTH_LONG).show();
            arraylistimagepath = new ArrayList<String>();

            if (status.equals("Wifi enabled") || status.equals("Mobile data enabled")) {

                Log.e("insideNetwork", "inside!!!!!");

                Cursor cursr;
                db = context.openOrCreateDatabase("freshorders", 0, null);

                Log.e("INSIDE_NoOrders", "NoOrders");
                final JSONArray resultSet = new JSONArray();
                Cursor curs;
                curs = db.rawQuery("SELECT * from orderheader where pushstatus IN ('No Order', 'Outlet')", null);
                Log.e("HeaderCountoutlet", String.valueOf(curs.getCount()));
                if (curs != null && curs.getCount() > 0) {
                    curs.moveToFirst();
                    while (curs.isAfterLast() == false) {

                        int totalColumn = curs.getColumnCount();
                        JSONObject rowObject = new JSONObject();

                        for (int i = 0; i < totalColumn; i++) {
                            if (curs.getColumnName(i) != null) {

                                try {

                                    if (curs.getString(i) != null) {
                                        if (!curs.getString(i).equals("New User")) {
                                            Log.d("TAG_NAME", curs.getString(i));
                                            rowObject.put(curs.getColumnName(i), curs.getString(i));
                                        }

                                    } else {
                                        if (!curs.getColumnName(i).equals("muserid")) {
                                            rowObject.put(curs.getColumnName(i), "");
                                        }

                                    }
                                } catch (Exception e) {
                                    Log.d("TAG_NAME", e.getMessage());
                                    e.printStackTrace();
                                    if(workListener != null)
                                        workListener.onWorkFailed(e.getMessage());
                                    return;
                                }
                            }

                        }

                        try {
                            //Kumaravel 13-07-2019
                            if(Constants.distributorname == null || Constants.distributorname.isEmpty()){
                                Constants.distributorname = new PrefManager(context).getStringDataByKey(PrefManager.KEY_DISTRIBUTOR_NAME);
                            }
                            rowObject.put("distributorname",Constants.distributorname);
                        } catch (Exception e) {
                            Log.d("Exception in Dist. name", e.getMessage());
                        }


                        try {

                            Cursor curs1;
                            curs1 = db.rawQuery("SELECT * from dealertable", null);
                            Log.e("dEALER", String.valueOf(curs1.getCount()));
                            if (curs1.getCount() > 0) {
                                curs1.moveToFirst();
                                selected_dealer = curs1.getString(curs1
                                        .getColumnIndex(DatabaseHandler.KEY_Duserid));
                            }

                            Cursor cursor2;
                            Constants.Merchantname = rowObject.getString("mname");

                            cursor2 = db.rawQuery("SELECT * from merchanttable where companyname =" + "'" + Constants.Merchantname + "'", null);
                            Log.e("HeaderCountCanled", String.valueOf(cursor2.getCount()));

                            cursor2.moveToFirst();
                            while (cursor2.isAfterLast() == false) {

                                int Columncount = cursor2.getColumnCount();
                                Log.e("merchantColumn", String.valueOf(cursor2.getColumnCount()));
                                JSONObject merchantdetail = new JSONObject();
                                for (int j = 0; j < Columncount; j++) {
                                    if (cursor2.getColumnName(j) != null) {
                                        try {

                                            ////////////////////////////////////added for hierarchy text 12-09-2019 by vel
                                            String columnName = cursor2.getColumnName(j);
                                            if(columnName.equalsIgnoreCase("hierarchytext")){
                                                merchantdetail.put(cursor2.getColumnName(j), cursor2.getString(j));

                                            }else if(columnName.equalsIgnoreCase("hierarchyobj")){
                                                String jsonStr = cursor2.getString(j);
                                                Log.e("hierarchyobj","....UnmodifiedStr.." + jsonStr);
                                                JSONArray array = new JSONArray(jsonStr);
                                                merchantdetail.put(cursor2.getColumnName(j), array);
                                                Log.e("hierarchyobj","....modifiedStr.." + array.toString());

                                            } else {

                                                if (cursor2.getString(j) != null) {
                                                    if (!cursor2.getString(j).equals("New User")) {

                                                        merchantdetail.put(cursor2.getColumnName(j), cursor2.getString(j));

                                                    }
                                                } else {
                                                    if (!cursor2.getColumnName(j).equals("userid")) {
                                                        merchantdetail.put(cursor2.getColumnName(j), "");
                                                    }
                                                }
                                            }


                                        } catch (Exception e) {
                                            Log.d("Merchant_Exception", e.getMessage());
                                            e.printStackTrace();
                                            if(workListener != null)
                                                workListener.onWorkFailed(e.getMessage());
                                            return;
                                        }
                                    }

                                }
                                merchantdetail.put("usertype", "M");
                                merchantdetail.put("duserid", selected_dealer);
                                rowObject.put("merchant", merchantdetail);
                                cursor2.moveToNext();
                            }

                            cursor2.close();

                            Log.e("Detailarray", rowObject.toString());

                        } catch (JSONException e) {
                            e.printStackTrace();
                            if(workListener != null)
                                workListener.onWorkFailed(e.getMessage());
                            return;
                        }

                        Log.e("jsonarray", rowObject.toString());

                        JSONArray resultDetail = new JSONArray();
                        try {

                            Cursor cursor;
                            cursor = db.rawQuery("SELECT * from oredrdetail where oredrdetail.oflnordid=" + rowObject.getString("oflnordid"), null);
                            Log.e("DetailCount", String.valueOf(cursor.getCount()));
                            cursor.moveToFirst();
                            while (cursor.isAfterLast() == false) {

                                int Columncount = cursor.getColumnCount();
                                Log.e("columnCount", String.valueOf(cursor.getColumnCount()));
                                JSONObject detailObject = new JSONObject();

                                for (int i = 0; i < 21; i++) //17
                                {
                                    if (cursor.getColumnName(i) != null) {

                                        try {

                                            if (cursor.getString(i) != null) {
                                                Log.d("TAG_NAME", cursor.getString(i));
                                                detailObject.put(cursor.getColumnName(i), cursor.getString(i));
                                            } else {
                                                detailObject.put(cursor.getColumnName(i), "");
                                            }
                                        } catch (Exception e) {
                                            Log.d("TAG_NAME", e.getMessage());
                                            e.printStackTrace();
                                            if(workListener != null)
                                                workListener.onWorkFailed(e.getMessage());
                                        }
                                    }

                                }

                                resultDetail.put(detailObject);
                                cursor.moveToNext();
                            }

                            cursor.close();

                            Log.e("Detailarray", resultDetail.toString());

                        } catch (JSONException e) {
                            e.printStackTrace();
                            if(workListener != null)
                                workListener.onWorkFailed(e.getMessage());
                            return;
                        }
                        try {
                            rowObject.put("orderdtls", resultDetail);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            if(workListener != null)
                                workListener.onWorkFailed(e.getMessage());
                            return;
                        }
                        resultSet.put(rowObject);
                        curs.moveToNext();
                    }
                    curs.close();
                    Log.e("SaveArray000", resultSet.toString());
                    Log.e("CountLength", String.valueOf(resultSet.length()));
                    for (int i = 0; i < resultSet.length(); i++) {
                        try {
                            JSONObject arrayMerchants = (JSONObject) resultSet.get(i);
                            Log.e("arrayMerchants--->" + i + " ", "" + arrayMerchants);
                        } catch (Exception e) {
                            e.printStackTrace();
                            if(workListener != null)
                                workListener.onWorkFailed(e.getMessage());
                            return;
                        }
                    }
                    JsonServiceHandler = new JsonServiceHandler(Utils.strsavemerchantOderdetail, context);
                    new NewClientAsyncTask(JsonServiceHandler, JsonServiceHandlerBeatUpdate, JsonServiceHandlerGetMerchantCount, db, resultSet, new PendingNewClientToOnline.LocalMigrationListener() {
                        @Override
                        public void OnMigrationSuccess() {
                            try {
                                if(workListener != null)
                                    workListener.onWorkSuccess();
                            }catch (Exception e){
                                e.printStackTrace();
                                if(workListener != null)
                                    workListener.onWorkFailed(e.getMessage());
                            }
                        }

                        @Override
                        public void OnLocalMigrationFailed(String errorMSG) {
                            if(workListener != null)
                                workListener.onWorkFailed(errorMSG);
                        }
                    }).execute();/////////
                }else {
                    //if(workListener != null)
                        //workListener.onWorkSuccess(); /// No Need To Send Info because No Item Found To Migrate
                    return;
                }
            }else {
                if(workListener != null)
                    workListener.onWorkFailed("No Mobile Network Found");
            }
        }catch (Exception e){
            e.printStackTrace();
            if(workListener != null)
                workListener.onWorkFailed(e.getMessage());
        }
    }

    public static class NewClientAsyncTask extends AsyncTask<Void, Void, String>{

        String strStatus = "";
        String strMsg = "", strOrderId = "";
        private JSONArray resultSet;
        private PendingNewClientToOnline.LocalMigrationListener listener;
        private  SQLiteDatabase db;
        private JsonServiceHandler JsonServiceHandler;
        private JsonServiceHandler JsonServiceHandlerBeatUpdate;
        private JsonServiceHandler JsonServiceHandlerGetMerchantCount;

        public NewClientAsyncTask(JsonServiceHandler jsonServiceHandler,
                                  JsonServiceHandler JsonServiceHandlerBeatUpdate,
                                  JsonServiceHandler JsonServiceHandlerGetMerchantCount,
                                  SQLiteDatabase db,
                                  JSONArray resultSet,
                                  PendingNewClientToOnline.LocalMigrationListener listener) {
            this.JsonServiceHandler = jsonServiceHandler;
            this.db = db;
            this.resultSet = resultSet;
            this.listener = listener;
            this.JsonServiceHandlerBeatUpdate = JsonServiceHandlerBeatUpdate;
            this.JsonServiceHandlerGetMerchantCount = JsonServiceHandlerGetMerchantCount;
        }

        @Override
        protected String doInBackground(Void... voids) {

                JSONObject jsonObject = new JSONObject();
                try {
                    for (int k = 0; k < resultSet.length(); k++) {
                        try {
                            jsonObject = resultSet.getJSONObject(k);
                            Log.e("jsonObject1", String.valueOf(jsonObject));
                            OfflineOrderNo = jsonObject.getString("oflnordid");
                            orderBeatId = jsonObject.getString("oflnordno");
                            OFFLINE_MERCHANT_NO = jsonObject.getJSONObject("merchant").getString("oflnordid");
                            JSONObject jsonObjMerchant = jsonObject.getJSONObject("merchant");
                            Log.e("jsonObjMerchantOffline", String.valueOf(jsonObjMerchant));
                            String strJsonObjCustomdata = "" + jsonObjMerchant.get("customdata");
                            Log.e("strJsonObjCustomdata", strJsonObjCustomdata);
                            jsonObjMerchant.put("createdby", Constants.USER_ID);
                            Log.e("createdbyOFFLINE", Constants.USER_ID);

                            ////// This process handle for new merchant "no order"
                            JSONArray jsonArrayCustomData;
                            if(!strJsonObjCustomdata.isEmpty()) {
                                jsonArrayCustomData = new JSONArray(strJsonObjCustomdata);
                                jsonObjMerchant.put("customdata", jsonArrayCustomData);
                            }else {
                                jsonObjMerchant.put("customdata", Constants.EMPTY);
                            }

                            jsonObject.put("merchant", jsonObjMerchant);
                            Log.e("SHIURL :", Utils.strsavemerchantOderdetail);
                            Log.e("SHIOBJ :", jsonObject.toString());
                            JsonServiceHandler.setParams(jsonObject.toString()); /////////////
                            JsonAccountObject = JsonServiceHandler.ServiceData();
                            Log.e("SHIRESPONSE :", JsonAccountObject.toString());

                            strOrderId = ""; strStatus = ""; strMsg = ""; //////////////////////recent change Kumaravel
                            strStatus = JsonAccountObject.getString("status");
                            Log.e("return status", strStatus);
                            strMsg = JsonAccountObject.getString("message");

                            if(strMsg == null || strStatus == null){ ///////////////////Kumaravel 30-06-2019
                                continue;
                            }

                            Log.e("return message", strMsg);
                            if (strStatus.equals("true") || (strStatus.equals("false") && strMsg.equals(Constants.ORDER_ALREADY_EXIST))) {  ///////////////////Kumaravel 30-06-2019 accept success outlet (failed by timeout)
                                strOrderId = JsonAccountObject.getJSONObject("data").getString("ordid");
                                Log.e("strOrderId", strOrderId);
                                String muserid = JsonAccountObject.getJSONObject("data").getString("muserid");
                                M_USER_ID = muserid;
                                Log.e("muserid", muserid);

                                String updatedFlag = jsonObject.getJSONObject("merchant").getString("cflag");

                                db.execSQL("UPDATE merchanttable SET userid = " + muserid + ",cflag =" + "'" + updatedFlag + "'" + " WHERE mobileno =" + "'" + jsonObject.getJSONObject("merchant").getString("mobileno") + "'");

                                Cursor curs1;
                                curs1 = db.rawQuery("SELECT * from merchanttable where userid=" + muserid, null);
                                Log.e("Outlet/Merchant Count", String.valueOf(curs1.getCount()));
                                if (curs1 != null && curs1.getCount() > 0) {

                                    if (curs1.moveToFirst()) {
                                        Log.e("merchant name", curs1.getString(0));
                                        Log.e("merchant id", curs1.getString(1));
                                        Log.e("merchant flag", curs1.getString(2));
                                    }
                                }
                                int currentBeatRowId = curs1.getInt(0);
                                String strQuery = " UPDATE beat SET muserid=" + muserid + ",flag='Inactive' WHERE oflnordid =" + "'" + orderBeatId + "'";
                                Log.e("SHIstrQuery:", strQuery);
                                db.execSQL(strQuery);
                                if(currentBeatUpdate(JsonServiceHandlerBeatUpdate, db, orderBeatId)){
                                    Log.e("Kumaravel","BeatUpdate..On Server............currentBeatRowId:" + currentBeatRowId);
                                }
                                Log.e("OFFLINE_MERCHANT_NO","..............:::"+OFFLINE_MERCHANT_NO + "...muserId:::"+muserid);
                                Cursor merchantCursor = databaseHandler.getMORByOfflineMerchantId(OFFLINE_MERCHANT_NO);
                                if(merchantCursor != null && merchantCursor.getCount() > 0){
                                    merchantCursor.moveToFirst();
                                    List<String> id = new ArrayList<>();
                                    for(int i = 0; i < merchantCursor.getCount(); i++) {
                                        id.add(merchantCursor.getString(0));
                                        merchantCursor.moveToNext();
                                    }
                                    String[] ids = new String[id.size()];
                                    ids = id.toArray(ids);
                                    databaseHandler.setMerchantIdByIdsInMOR(ids, muserid);
                                    Log.e("OFFLINE_MERCHANT_NO","..............:::"+OFFLINE_MERCHANT_NO + "...id list:::"+id.toString());
                                    merchantCursor.close();
                                }


                                db.execSQL("DELETE FROM oredrdetail where ( ordstatus IN ('No Order', 'Outlet')) AND ( oflnordid = " + OfflineOrderNo + " ) ");
                                db.execSQL("DELETE FROM orderheader where ( pushstatus IN ('No Order', 'Outlet')) AND ( oflnordid = " + OfflineOrderNo + " ) ");
                                Cursor curs;
                                Log.e("OfflineOrderNonew", String.valueOf(OfflineOrderNo));
                                if(k == resultSet.length() -1 ){
                                    updateOnlineMerchantCount(JsonServiceHandlerGetMerchantCount, db);
                                }
                            }else {
                                if(k == resultSet.length() -1 ){
                                    updateOnlineMerchantCount(JsonServiceHandlerGetMerchantCount, db);
                                }
                            }

                        }catch (Exception e) {
                            Log.e("NewClient.Mig",",,,,,,,,......Exception ..." + e.getMessage());
                            e.printStackTrace();
                        }
                    }
                return "";
                }catch (Exception e){
                    e.printStackTrace();
                    return e.getMessage();
                }
                finally {
                    db.close();
                }
        }

        @Override
        protected void onPostExecute(String result) {

            if(result.isEmpty()){
                if(listener != null)
                listener.OnMigrationSuccess();
            }else {
                if(listener != null)
                listener.OnLocalMigrationFailed(result);
            }
        }
    }

    private static boolean currentBeatUpdate(JsonServiceHandler JsonServiceHandlerBeatUpdate ,SQLiteDatabase db, String offlineId){
        Cursor cursor;
        String query = "SELECT * from beat where  oflnordid =" + "'" + offlineId + "'" + " AND flag ='Inactive' ";
        Log.e("Query","Beat.................................."+query);
        cursor = db.rawQuery(query, null);
        Log.e("SHICursor:", cursor.toString());
        Log.e("SHICursor Count:", "" + cursor.getCount());

        if (cursor.getCount() > 0) {
            System.out.println("Count_2_(IA+OIA) :" + cursor.getCount());
            cursor.moveToFirst();
            final JSONArray beatarray = new JSONArray();

                int Columncount = cursor.getColumnCount();
                JSONObject beatobject = new JSONObject();

                for (int i = 0; i < Columncount; i++) {
                    if (cursor.getColumnName(i) != null) {
                        try {
                            if (cursor.getString(i) != null) {
                               /* if(cursor.getColumnName(i).equals("beattype")){
                                    if(cursor.getString(i).equalsIgnoreCase("C")){
                                        }
                                }*/
                                if (cursor.getColumnName(i).equals("suserid")) {
                                    if (!cursor.getString(18).equalsIgnoreCase("C")) {
                                        beatobject.put(cursor.getColumnName(i), cursor.getString(i));
                                    }

                                } else {
                                    beatobject.put(cursor.getColumnName(i), cursor.getString(i));

                                }
                                Log.d("TAG_NAME", cursor.getString(i));
                                Log.e("EXPECTED", String.valueOf(beatobject));
                            } else {
                                beatobject.put(cursor.getColumnName(i), "");
                            }
                        } catch (Exception e) {
                            Log.d("TAG_NAME", e.getMessage());
                            e.printStackTrace();
                            return false;
                        }
                    }
                }
                beatarray.put(beatobject);
                System.out.println("KAR_WHILE_INISIDE_-->" + beatarray.toString());
                System.out.println("KAR_WHILE_INISIDE_LEN:-->" + beatarray.length());
            try {
                for (int k = 0; k < beatarray.length(); k++) {
                    System.out.println("KARBAIN--->:" + k + ":" + beatarray.length());

                    JSONObject jsonObject = beatarray.getJSONObject(k);
                    beatrowid = jsonObject.getString("rowid");
                    Log.e("SHI_B_URL :", Utils.strsavebeat);
                    Log.e("SHIOBJBEAT :", jsonObject.toString());

                    JsonServiceHandlerBeatUpdate.setParams(jsonObject.toString()); /////////////
                    JsonAccountObject = JsonServiceHandlerBeatUpdate.ServiceData();
                    try {
                        String strStatus = JsonAccountObject.getString("status");
                        Log.e("return status", strStatus);
                        String strMsg = JsonAccountObject.getString("message");
                        Log.e("return message", strMsg);
                        if (strStatus.equals("true")) {

                            db.execSQL("UPDATE beat SET flag = 'Active' WHERE rowid =" + "'" + beatrowid + "'");
                        } else {
                            return false;
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        return false;
                    }
                }
                cursor.close();
                return true;
            }catch (Exception e){
                e.printStackTrace();
                return false;
            }
        }else {
            return false;
        }
    }


    private static void updateOnlineMerchantCount(JsonServiceHandler JsonServiceHandlerGetMerchantCount, SQLiteDatabase db){
        try{
            JSONObject jsonObjReqCount = new JSONObject();
            try {
                jsonObjReqCount.put("userid", Constants.USER_ID);
                jsonObjReqCount.put("usertype", Constants.USER_TYPE);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            JsonServiceHandlerGetMerchantCount.setParams(jsonObjReqCount.toString()); /////////////
            JSONObject jsonOnlineMerchantCount = JsonServiceHandlerGetMerchantCount.ServiceData();
            int intOnlineMerchantCount = jsonOnlineMerchantCount.getInt("count");
            Constants.onLineCount = intOnlineMerchantCount;
            Log.e("SHILOG", "Before calling db exec...");

            System.out.println("intOnlineMerchantCount777" + intOnlineMerchantCount);

            String strQuery = "update  constants set `" + "value" + "`='" + intOnlineMerchantCount + "' where `" + "key" + "` = '" + "OUTLETCOUNT" + "'";
            db.execSQL(strQuery);
            Log.e("Kumaravel", "Merchant Count Updated ..... OUTLETCOUNT :" + intOnlineMerchantCount);
        }catch (Exception e){
            e.printStackTrace();
        }
    }







    public  void beatupdate() throws Exception{

        try {
            Log.e("INSIDE_beatupdate", "beatupdate");
            Cursor cursor1;
            Cursor cursor2;
            cursor1 = db.rawQuery("SELECT * from beat where  flag ='Inactive'", null);
            if (cursor1 != null) {
                System.out.println("Count_1_(IA) :" + cursor1.getCount());
            }

            cursor2 = db.rawQuery("SELECT * from beat where  flag ='OffInactive' ", null);
            if (cursor2 != null) {
                System.out.println("Count_2_(OIA) :" + cursor2.getCount());
            }

            Cursor cursor;
            cursor = db.rawQuery("SELECT * from beat where  flag ='Inactive' or flag ='OffInactive' ", null);
            Log.e("SHICursor:", cursor.toString());
            Log.e("SHICursor Count:", "" + cursor.getCount());

            // Log.e("SHiCUrItems:",databaseHandler.getCursorItems(cursor).toString());
//        for(int i=0;i<cursor.getCount();i++)
//        {
//
//        }
            Log.e("beatCountNetwork", String.valueOf(cursor.getCount()));
            if (cursor != null && cursor.getCount() > 0) {
                System.out.println("Count_2_(IA+OIA) :" + cursor.getCount());
                cursor.moveToFirst();
                final JSONArray beatarray = new JSONArray();
                while (cursor.isAfterLast() == false) {

                    int Columncount = cursor.getColumnCount();
                    JSONObject beatobject = new JSONObject();


                    for (int i = 0; i < Columncount; i++) {
                        if (cursor.getColumnName(i) != null) {

                            try {

                                if (cursor.getString(i) != null) {
                               /* if(cursor.getColumnName(i).equals("beattype")){
                                    if(cursor.getString(i).equalsIgnoreCase("C")){
                                        }
                                }*/
                                    if (cursor.getColumnName(i).equals("suserid")) {
                                        if (!cursor.getString(18).equalsIgnoreCase("C")) {
                                            beatobject.put(cursor.getColumnName(i), cursor.getString(i));
                                        }

                                    } else {
                                        beatobject.put(cursor.getColumnName(i), cursor.getString(i));

                                    }
                                    Log.d("TAG_NAME", cursor.getString(i));
                                    Log.e("EXPECTED", String.valueOf(beatobject));
                                } else {
                                    beatobject.put(cursor.getColumnName(i), "");

                                }
                            } catch (Exception e) {
                                Log.d("TAG_NAME", e.getMessage());
                                e.printStackTrace();
                                if(workListener != null)
                                workListener.onWorkFailed(e.getMessage());
                                return;
                            }
                        }

                    }

                    beatarray.put(beatobject);

                    cursor.moveToNext();

                    System.out.println("KAR_WHILE_INISIDE_-->" + beatarray.toString());
                    System.out.println("KAR_WHILE_INISIDE_LEN:-->" + beatarray.length());
                }

                cursor.close();

                Log.e("Detailarray", beatarray.toString());

                JsonServiceHandler = new JsonServiceHandler(Utils.strsavebeat, context);
                JsonServiceHandler JsonServiceHandler1 = new JsonServiceHandler(Utils.strGetMerchantList, context);

                new PendingNewClientToOnline.BeatUpdateMigrationAsyncTask(db, JsonServiceHandler,
                        JsonServiceHandler1,
                        beatarray, new PendingNewClientToOnline.ServerNewClientMigrationListener() {
                    @Override
                    public void onMigrationFinished() {
                        if(workListener != null)
                        workListener.onWorkSuccess();
                    }

                    @Override
                    public void onMigrationFailed(String msg) {
                        if(workListener != null)
                        workListener.onWorkFailed(msg);
                    }
                }).execute();

            } else {
                if(workListener != null)
                workListener.onWorkSuccess();
            }
        }catch (Exception e){
            e.printStackTrace();
            if(workListener != null)
            workListener.onWorkFailed(e.getMessage());
        }
    }

    private static class BeatUpdateMigrationAsyncTask extends AsyncTask<Void, Void, String>{

        String strStatus = "";
        String strMsg = "";
        int intOnlineMerchantCount=0;

        private SQLiteDatabase db;
        private JsonServiceHandler JsonServiceHandler;
        private JsonServiceHandler JsonServiceHandler1;
        private JSONArray beatarray;
        private PendingNewClientToOnline.ServerNewClientMigrationListener finalListener;

        public BeatUpdateMigrationAsyncTask(SQLiteDatabase db,
                                            JsonServiceHandler jsonServiceHandler,
                                            JsonServiceHandler jsonServiceHandler1,
                                            JSONArray beatarray,
                                            ServerNewClientMigrationListener finalListener) {
            this.db = db;
            JsonServiceHandler = jsonServiceHandler;
            JsonServiceHandler1 = jsonServiceHandler1;
            this.beatarray = beatarray;
            this.finalListener = finalListener;
        }

        @Override
        protected String doInBackground(Void... voids) {
            JSONObject jsonObject ;
            try {

                System.out.println("KARBA:"+beatarray);
                System.out.println("KARBALEN:"+beatarray.length());


                for (int k = 0; k < beatarray.length(); k++) {
                    System.out.println("KARBAIN--->:"+k+":"+beatarray.length());

                    jsonObject = beatarray.getJSONObject(k);
                    beatrowid = jsonObject.getString("rowid");
                    Log.e("SHI_B_URL :",Utils.strsavebeat);
                    Log.e("SHIOBJBEAT :",jsonObject.toString());


                    JsonServiceHandler.setParams(jsonObject.toString()); /////////////
                    JsonAccountObject = JsonServiceHandler.ServiceData();

                    JSONObject jsonObjReqCount = new JSONObject();
                    try {
                        jsonObjReqCount.put("userid", Constants.USER_ID);
                        jsonObjReqCount.put("usertype", Constants.USER_TYPE);


                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        return e.getMessage();
                    }
                    JsonServiceHandler1.setParams(jsonObjReqCount.toString()); /////////////
                    JSONObject jsonOnlineMerchantCount = JsonServiceHandler1.ServiceData();
                    intOnlineMerchantCount=jsonOnlineMerchantCount.getInt("count");
                    Constants.onLineCount=intOnlineMerchantCount;
                    Log.e("SHILOG","Before calling db exec...");

                    System.out.println("intOnlineMerchantCount777"+intOnlineMerchantCount);

                    String strQuery = "update  constants set `"+"value"+"`='"+intOnlineMerchantCount+"' where `"+"key"+"` = '"+"OUTLETCOUNT"+"'";
                    db.execSQL(strQuery);
                    Log.e("SHILOG","Executed ..... OUTLETCOUNT set");
//                            Log.e("SHILOG","Before calling database handler exec...");
//                            databaseHandler.setConstantValue("OUTLETCOUNT",""+intOnlineMerchantCount);


                    try {

                        strStatus = JsonAccountObject.getString("status");
                        Log.e("return status", strStatus);
                        strMsg = JsonAccountObject.getString("message");
                        Log.e("return message", strMsg);

                        if (strStatus.equals("true")) {

                            db.execSQL("UPDATE beat SET flag = 'Active' WHERE rowid =" + "'" + beatrowid + "'");
                        }else{

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        return e.getMessage();
                    }

                }
                return Constants.EMPTY;
            } catch (JSONException e) {
                e.printStackTrace();
                return e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(String result) {

            if(result.isEmpty()){
                if(finalListener != null)
                    finalListener.onMigrationFinished();
            }else {
                if(finalListener != null)
                    finalListener.onMigrationFailed(result);
            }
        }
    }

}
