package com.freshorders.freshorder.toonline;

public class PendingBeatToServer {

    public interface BeatCompleteListener{
        void onBeatComplete();
        void onBeatFailed(String msg);

    }
}
