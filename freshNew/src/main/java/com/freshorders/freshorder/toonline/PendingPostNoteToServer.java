package com.freshorders.freshorder.toonline;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.http.VolleySingleton;
import com.freshorders.freshorder.model.MigrationError;
import com.freshorders.freshorder.model.PostNoteModel;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.DbBitmapUtility;
import com.freshorders.freshorder.utils.Log;
import com.freshorders.freshorder.utils.Utils;
import com.freshorders.freshorder.volleylib.VolleyMultipartRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import eu.janmuller.android.simplecropimage.Util;

import static com.freshorders.freshorder.toonline.Migration.KEY_IS_POST_NOTE_MIGRATION_START;

public class PendingPostNoteToServer {

    public static final String TAG = "PostNoteTo";
    public int migrationCount = 0;
    private int mStatusCode = 0;


    public interface PostNoteMigrationCompleteListener{
        void onComplete(LinkedHashMap<Integer, PostNoteModel> migrationList);
        void noMigrationRequired();
        void migrationStartFailed();
        void migrationInterrupted(LinkedHashMap<Integer, PostNoteModel> migrationList);
    }

    private Context mContext;
    private DatabaseHandler databaseHandler;
    private LinkedHashMap<Integer, PostNoteModel> migrationList;
    private PendingPostNoteToServer.PostNoteMigrationCompleteListener listener;

    public PendingPostNoteToServer(Context mContext, PendingPostNoteToServer.PostNoteMigrationCompleteListener listener) {
        this.mContext = mContext;
        this.listener = listener;
        databaseHandler = new DatabaseHandler(mContext.getApplicationContext());
        migrationList = new LinkedHashMap<>();

    }

    public void startPostNoteMigration(){
        try{
            Cursor cursor = databaseHandler.getPostNotePending();
            if(cursor != null){
                if(cursor.getCount() > 0){
                    cursor.moveToFirst();
                    while (!cursor.isAfterLast()){
                        Log.e(TAG, "startPostNoteMigration........count" + cursor.getCount());
                        PostNoteModel model = new PostNoteModel();
                        int id = cursor.getInt(cursor.getColumnIndex(DatabaseHandler.KEY_POST_NOTE_ID));
                        model.setId(id);
                        model.setMuserid(cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_POST_NOTE_USER_ID)));
                        model.setDuserid(cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_POST_NOTE_D_USER_ID)));
                        model.setParticular(cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_POST_NOTE_DESCRIPTION)));
                        byte[] image = cursor.getBlob(cursor.getColumnIndex(DatabaseHandler.KEY_POST_NOTE_IMAGE));
                        model.setImage(image);
                        if(image.length > 0){
                            model.setImageContain(true);
                        }else {
                            model.setImageContain(false);
                        }
                        migrationList.put(id, model);
                        cursor.moveToNext();
                    }
                    ///////////////
                    migrationCount = migrationList.size();
                    uploadBitmap(migrationList, listener);
                }else {
                    if(listener != null) {
                        listener.noMigrationRequired();
                    }else {
                        new PrefManager(mContext).setBooleanDataByKey(KEY_IS_POST_NOTE_MIGRATION_START, false);
                    }
                }
            }else {
                if(listener != null) {
                    listener.migrationStartFailed();
                }else {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_POST_NOTE_MIGRATION_START, false);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            if(listener != null) {
                listener.migrationInterrupted(migrationList);
            }else {
                new PrefManager(mContext).setBooleanDataByKey(KEY_IS_POST_NOTE_MIGRATION_START, false);
            }
        }

    }

    private void uploadWithOutImage(String url,
                                    final LinkedHashMap<Integer, PostNoteModel> migrationList,
                                    final PostNoteModel postNoteData,
                                    final int id) {

        StringRequest stringRequest = new StringRequest(
                Request.Method.POST,
                url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        updateStatus(migrationList, postNoteData, id);
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        updateFailedStatus(migrationList, postNoteData, id, error); // Kumaravel
                    }
        })
        {
            @Override
            protected Response<String> parseNetworkResponse (NetworkResponse response){
                mStatusCode = response.statusCode;
                return super.parseNetworkResponse(response);
        }
    };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2,
                2,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        //adding the request to volley
        VolleySingleton.getInstance().addToRequestQueue(stringRequest, Migration.TAG_POST_NOTE_MIGRATION);
    }
    private void updateStatus(final LinkedHashMap<Integer, PostNoteModel> migrationList,
                              final PostNoteModel postNoteData, int id) {

        postNoteData.setSuccess(true);
        postNoteData.setResponseCode(String.valueOf(mStatusCode));
        migrationList.put(id, postNoteData);
        databaseHandler.updatePostNote(id, Migration.MIGRATION_COMPLETE, Constants.EMPTY);
        migrationCount--;
        if(migrationCount == 0){ // Migration Complete
            if(listener != null) {
                listener.onComplete(migrationList);
            }else {
                new PrefManager(mContext).setBooleanDataByKey(KEY_IS_POST_NOTE_MIGRATION_START, false);
            }
        }
    }

    private void updateFailedStatus(final LinkedHashMap<Integer, PostNoteModel> migrationList,
                              final PostNoteModel postNoteData, int id, VolleyError error) {
        String errorMSG = error.getMessage();
        postNoteData.setSuccess(true);
        postNoteData.setFailedReason(errorMSG);
        postNoteData.setResponseCode(String.valueOf(mStatusCode));
        migrationList.put(id, postNoteData);
        databaseHandler.updatePostNote(id, Migration.MIGRATION_COMPLETE, Constants.EMPTY);
        migrationCount--;
        if(migrationCount == 0){ // Migration Complete
            if(listener != null) {
                listener.onComplete(migrationList);
            }else {
                new PrefManager(mContext).setBooleanDataByKey(KEY_IS_POST_NOTE_MIGRATION_START, false);
            }
        }
    }

    private MigrationError isPostNoteReTry(VolleyError error){
        MigrationError migrationError = new MigrationError();
        int migration = 0;
        String errorMsg = "";
        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
            errorMsg = Constants.TIME_OUT_ERROR;
            migration = 1;
        } else if (error instanceof AuthFailureError) {
            errorMsg = Constants.AUTH_FAILURE_ERROR;
            migration = 0;

        } else if (error instanceof ServerError) {
            errorMsg = Constants.SERVER_ERROR;
            migration = 1;

        } else if (error instanceof NetworkError) {
            errorMsg = Constants.NETWORK_ERROR;
            migration = 1;
        } else if (error instanceof ParseError) {
            errorMsg = Constants.PARSE_ERROR;
            migration = 0;
        }else if(error.getMessage() != null && !error.getMessage().isEmpty()){
                errorMsg = error.getMessage();
        }else {
            errorMsg = "UnKnown Error";
        }
        migrationError.setErrorMSG(errorMsg);
        migrationError.setIsNeedMigration(migration);
        return migrationError;
    }


    private void uploadBitmap(
            final LinkedHashMap<Integer, PostNoteModel> migrationList,
            final PendingPostNoteToServer.PostNoteMigrationCompleteListener listener) throws Exception {

        final DatabaseHandler databaseHandler = new DatabaseHandler(mContext.getApplicationContext());
        ////////
        new PrefManager(mContext).setBooleanDataByKey(KEY_IS_POST_NOTE_MIGRATION_START, true);
        ////////
        for(Map.Entry entry: migrationList.entrySet()){
            final int id = (int) entry.getKey();
            final PostNoteModel postNoteData = (PostNoteModel) entry.getValue();

            String url=Utils.strAddComplaint+postNoteData.getMuserid()+"&duserid="+postNoteData.getDuserid()+"&particular="+postNoteData.getParticular();
            url=url.replaceAll(" ", "%20");  //Utils.strAddComplaintForMigration
            if(!postNoteData.isImageContain()){
                uploadWithOutImage(url, migrationList, postNoteData, id);
                continue;
            }
            //our custom volley request
            VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, url,
                    new Response.Listener<NetworkResponse>() {
                        @Override
                        public void onResponse(NetworkResponse response) {
                            try {
                                JSONObject obj = new JSONObject(new String(response.data));
                                updateStatus(migrationList, postNoteData, id);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("LOG_VOLLEY", error.toString());
                            //Toast.makeText(mContext.getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                            updateFailedStatus(migrationList, postNoteData, id, error);
                        }
                    }) {

                /*
                 * If you want to add more parameters with the image
                 * you can do it here
                 * */
                /*
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    //params.put("muserid", postNoteData.getMuserid());
                    //params.put("duserid", postNoteData.getDuserid());
                   //params.put("particular", postNoteData.getParticular());
                    return params;
                }  */

                /*
                 * Here we are passing image by renaming it with a unique name
                 * */
                @Override
                protected Map<String, DataPart> getByteData() {
                    Map<String, DataPart> params = new HashMap<>();
                    long imagename = System.currentTimeMillis();
                    //params.put("pic", new DataPart(imagename + ".png", postNoteData.getImage()));
                    params.put("file", new DataPart(imagename + ".jpg", postNoteData.getImage(), "image/jpeg")); /// pic to file
                    return params;
                }
            };

            volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                    Constants.MY_SOCKET_TIMEOUT_MS,
                    2,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            //adding the request to volley
            VolleySingleton.getInstance().addToRequestQueue(volleyMultipartRequest, Migration.TAG_POST_NOTE_MIGRATION);
        }
    }
}
