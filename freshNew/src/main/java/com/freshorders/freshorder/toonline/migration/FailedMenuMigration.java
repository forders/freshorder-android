package com.freshorders.freshorder.toonline.migration;

import android.content.Context;
import android.database.Cursor;

import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.toonline.BendingOrderMigrateToOnline;
import com.freshorders.freshorder.toonline.FailedClosingStockToServer;
import com.freshorders.freshorder.toonline.FailedOrderMigrateToOnline;
import com.freshorders.freshorder.toonline.PendingClosingStockToServer;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.Log;

import java.util.ArrayList;
import java.util.List;

public class FailedMenuMigration {

    private static String TAG = FailedMenuMigration.class.getSimpleName();
    private static String failedPendingOrder = "Failed Order Migration";
    private static String failedClosingStock = "Failed Closing Stock Migration";

    private static String migAlreadyRunning = "MigrationAlreadyRunning";

    private static String MIG_NOT_REQUIRED = "Migration Not Required";
    private static String MIG_FINISHED_SUCCESS = "Migration Successfully Finished";
    private static String MIG_FAILED = "Migration Failed";


    private Context mContext;
    private DatabaseHandler databaseHandler;
    private List<MenuMigrationModel> migStatusList;

    public FailedMenuMigration(Context mContext) {
        this.mContext = mContext;
        databaseHandler = new DatabaseHandler(mContext);
        migStatusList = new ArrayList<>();
    }

    public boolean isNeedFailedMigration(){
        Cursor cursOrder = databaseHandler.getFailedOrderHeader();
        Cursor cursClosingStockOrder = databaseHandler.getFailedOrderHeader();

        boolean result =  ((cursOrder != null && cursOrder.getCount() > 0) ||
                                (cursClosingStockOrder != null && cursClosingStockOrder.getCount() > 0));

        if(cursOrder != null) {
            cursOrder.close();
        }
        if(cursClosingStockOrder != null){
            cursClosingStockOrder.close();
        }
        return result;
    }

    public void startReset(){
        new PrefManager(mContext).setBooleanDataByKey(PrefManager.KEY_IS_FAILED_ORDER_MIGRATION_START, false);
        new PrefManager(mContext).setBooleanDataByKey(PrefManager.KEY_IS_FAILED_CLOSING_MIGRATION_START, false);
    }

    public String doFailedSequenceMigration() {
        if(failedPendingOrderMigration()){
            if(failedPendingOrderMigration()){
                StringBuilder stringBuilder = new StringBuilder();
                for (int i = 0; i < migStatusList.size(); i++) {
                    MenuMigrationModel migrationModel = migStatusList.get(i);
                    stringBuilder.append("Data Moving Operation Name : ");
                    stringBuilder.append(migrationModel.getMigrationName());
                    stringBuilder.append("\n");
                    if (migrationModel.isStart()) {
                        stringBuilder.append("Moving Data Count : ");
                        stringBuilder.append(migrationModel.getMigrationCount());
                        stringBuilder.append("\n");

                        stringBuilder.append("Status : ");
                        stringBuilder.append(migrationModel.getStatus());
                        stringBuilder.append("\n");
                    } else {
                        stringBuilder.append("Moving Failed Reason : ");
                        stringBuilder.append(migrationModel.getExceptionReason());
                        stringBuilder.append("\n");

                        stringBuilder.append("Status : ");
                        stringBuilder.append(migrationModel.getStatus());
                        stringBuilder.append("\n");
                    }
                    stringBuilder.append("\n");
                    stringBuilder.append("\n");
                }
                return stringBuilder.toString();
            }
        }
        return Constants.EMPTY;
    }

    private boolean failedPendingOrderMigration(){

        MenuMigrationModel current = new MenuMigrationModel();
        current.setMigrationName(failedPendingOrder);
        try{
            boolean isMigrationSet = new PrefManager(mContext).getBooleanDataByKey(PrefManager.KEY_IS_FAILED_ORDER_MIGRATION_START);
            if (!isMigrationSet) {
                DatabaseHandler databaseHandler = new DatabaseHandler(mContext);
                Cursor curs = databaseHandler.getorderheader();
                if (curs.getCount() > 0) {
                    android.util.Log.e("DataMigration", "started.............");
                    // Do Migration
                    new FailedOrderMigrateToOnline(mContext, new BendingOrderMigrateToOnline.ServerMigrationListener() {
                        @Override
                        public void onMigrationFinished(boolean isSuccess, String statusMSG, boolean isException, final String excMSG) {
                            new PrefManager(mContext).setBooleanDataByKey(PrefManager.KEY_IS_FAILED_ORDER_MIGRATION_START, false);
                        }
                    }).migration();
                    current.setMigrationCount(curs.getCount());
                    current.setStatus(MIG_FINISHED_SUCCESS);
                    current.setStart(true);
                    migStatusList.add(current);
                    return true;
                } else {
                    android.util.Log.e("Migration", "NO Order Migrate");
                    current.setMigrationCount(0);
                    current.setStatus(MIG_NOT_REQUIRED);
                    migStatusList.add(current);
                    return true;
                }
            }else {
                Log.e(TAG, "Pending Order Migration Already Working.....");
                current.setStatus(migAlreadyRunning);
                migStatusList.add(current);
                return true;
            }
        }catch (Exception e){
            e.printStackTrace();
            new PrefManager(mContext).setBooleanDataByKey(PrefManager.KEY_IS_FAILED_ORDER_MIGRATION_START, false);
            Log.e(TAG, "Pending Order Migration Exception....."+ e.getMessage());
            current.setInterrupted(true);
            current.setExceptionReason(e.getMessage());
            migStatusList.add(current);
            return true;
        }
    }

    private synchronized boolean failedClosingStockMigration(){
        MenuMigrationModel current = new MenuMigrationModel();
        current.setMigrationName(failedClosingStock);
        try{
            boolean isMigrationSet = new PrefManager(mContext).getBooleanDataByKey(PrefManager.KEY_IS_FAILED_CLOSING_MIGRATION_START);
            if (!isMigrationSet) {
                DatabaseHandler databaseHandler = new DatabaseHandler(mContext);
                Cursor curs = databaseHandler.getStockOrderHeader();
                if (curs.getCount() > 0) {
                    android.util.Log.e("DataMigration", "started.............");
                    // Do Migration
                    new FailedClosingStockToServer(mContext, new PendingClosingStockToServer.ServerMigrationListener() {
                        @Override
                        public void onMigrationFinished(boolean isSuccess, String statusMSG, boolean isException, final String excMSG) {
                            new PrefManager(mContext).setBooleanDataByKey(PrefManager.KEY_IS_FAILED_CLOSING_MIGRATION_START, false);
                        }
                    }).migration();
                    current.setMigrationCount(curs.getCount());
                    current.setStatus(MIG_FINISHED_SUCCESS);
                    current.setStart(true);
                    migStatusList.add(current);
                    return true;
                } else {
                    android.util.Log.e("Migration", "NO Closing Stock Migrate Need");
                    current.setMigrationCount(0);
                    current.setStatus(MIG_NOT_REQUIRED);
                    migStatusList.add(current);
                    return true;
                }
            }else {
                Log.e(TAG, "Closing Stock Migration Already Working.....");
                current.setStatus(migAlreadyRunning);
                migStatusList.add(current);
                return true;
            }
        }catch (Exception e){
            new PrefManager(mContext).setBooleanDataByKey(PrefManager.KEY_IS_FAILED_CLOSING_MIGRATION_START, false);
            e.printStackTrace();
            Log.e(TAG, "Closing Stock Migrate Exception....."+ e.getMessage());
            current.setInterrupted(true);
            current.setExceptionReason(e.getMessage());
            migStatusList.add(current);
            return true;
        }
    }

}
