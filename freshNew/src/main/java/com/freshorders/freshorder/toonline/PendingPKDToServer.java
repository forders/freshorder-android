package com.freshorders.freshorder.toonline;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.http.Request;
import com.freshorders.freshorder.http.Response;
import com.freshorders.freshorder.http.VolleySingleton;
import com.freshorders.freshorder.model.PKDModel;
import com.freshorders.freshorder.model.PostNoteModel;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Log;
import com.freshorders.freshorder.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.LinkedHashMap;
import java.util.Map;

import static com.freshorders.freshorder.toonline.Migration.KEY_IS_PKD_MIGRATION_START;
import static com.freshorders.freshorder.toonline.Migration.KEY_IS_POST_NOTE_MIGRATION_START;

public class PendingPKDToServer {

    public static final String TAG = PendingPKDToServer.class.getSimpleName();
    public int migrationCount = 0;

    public interface PKDMigrationCompleteListener{
        void onComplete(LinkedHashMap<Integer, PKDModel> migrationList);
        void noMigrationRequired();
        void migrationStartFailed();
        void migrationInterrupted(LinkedHashMap<Integer, PKDModel> migrationList);
    }

    private Context mContext;
    private DatabaseHandler databaseHandler;
    private LinkedHashMap<Integer, PKDModel> migrationList;
    private PendingPKDToServer.PKDMigrationCompleteListener listener;
    private JsonServiceHandler JsonServiceHandler;

    public PendingPKDToServer(Context mContext,
                              PKDMigrationCompleteListener listener) {
        this.mContext = mContext;
        this.listener = listener;
        this.databaseHandler = new DatabaseHandler(mContext.getApplicationContext());
        this.migrationList = new LinkedHashMap<>();
        JsonServiceHandler = new JsonServiceHandler(Utils.strstocks, mContext);

    }

    public void startPKDMigration(){
        try{
            Cursor cursor = databaseHandler.getPKDPending();
            if(cursor != null){
                if(cursor.getCount() > 0){
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_PKD_MIGRATION_START, true);
                    cursor.moveToFirst();
                    while (!cursor.isAfterLast()) {
                        Log.e(TAG, "startPKDMigration........count" + cursor.getCount());
                        PKDModel model = new PKDModel();
                        int id = cursor.getInt(cursor.getColumnIndex(DatabaseHandler.KEY_PKD_ID));
                        int migration = cursor.getInt(cursor.getColumnIndex(DatabaseHandler.KEY_PKD_MIGRATION_STATUS));
                        String url = cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_PKD_URL));
                        String urlData = cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_PKD_SYNC_DATA));
                        String error = cursor.getString(cursor.getColumnIndex(DatabaseHandler.KEY_PKD_FAILED_REASON));
                        int merchant_primary_id = cursor.getInt(cursor.getColumnIndex(DatabaseHandler.KEY_PKD_MERCHANT_PRIMARY_KEY));
                        model.setId(id); model.setUrl(url); model.setStatus(migration);  model.setErrorReason(error);
                        // newly added for requirement new Merchant's PKD
                        try{
                            if(urlData.contains("New User")){
                                JSONObject obj = new JSONObject(urlData);
                                JSONArray array = obj.getJSONArray("stockarr");
                                for(int i = 0; i < array.length(); i++) {
                                    JSONObject currentOutlet = array.getJSONObject(i);
                                    String merchantId = currentOutlet.getString("muserid");
                                    if (merchantId.equals("New User")) {
                                        Cursor cur = databaseHandler.getMerchantById(merchant_primary_id);
                                        if (cur != null && cur.getCount() > 0) {
                                            cur.moveToFirst();
                                            String mUserId = cur.getString(cur.getColumnIndex("userid"));
                                            cur.close();
                                            currentOutlet.put("muserid", mUserId);
                                            obj.getJSONArray("stockarr").getJSONObject(i).put("muserid", mUserId);
                                        }
                                    }
                                }
                                model.setUrlData(obj.toString());
                                Log.e("PKD","IF.....................................urlData." + obj.toString());
                            }else {
                                model.setUrlData(urlData);
                                Log.e("PKD","ELSE.....................................urlData." + urlData);
                            }

                        }catch (Exception e){
                            e.printStackTrace();
                            if(listener != null) {
                                listener.migrationInterrupted(migrationList);
                            }
                            return;
                        }

                        migrationList.put(id, model);
                        cursor.moveToNext();
                    }
                    migrationCount = migrationList.size();
                    ///////////////loadDataToServer();/////////////////see
                    new PendingPKDToServer.PKDAsyncTask(migrationList,JsonServiceHandler,databaseHandler,listener).execute();
                }else {
                    if(listener != null) {
                        listener.noMigrationRequired();
                    }else {
                        new PrefManager(mContext).setBooleanDataByKey(KEY_IS_PKD_MIGRATION_START, false);
                    }
                }
            }else {
                if(listener != null) {
                    listener.migrationStartFailed();
                }else {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_PKD_MIGRATION_START, false);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            if(listener != null) {
                listener.migrationInterrupted(migrationList);
            }else {
                new PrefManager(mContext).setBooleanDataByKey(KEY_IS_PKD_MIGRATION_START, false);
            }
        }
    }



    public static class PKDAsyncTask extends AsyncTask<Void, Void, String> {

        private LinkedHashMap<Integer, PKDModel> migrationList;
        private com.freshorders.freshorder.utils.JsonServiceHandler JsonServiceHandler;
        private DatabaseHandler databaseHandler;
        private PendingPKDToServer.PKDMigrationCompleteListener listener;

        public PKDAsyncTask(LinkedHashMap<Integer, PKDModel> migrationList,
                            com.freshorders.freshorder.utils.JsonServiceHandler jsonServiceHandler,
                            DatabaseHandler databaseHandler,
                            PKDMigrationCompleteListener listener) {
            this.migrationList = migrationList;
            JsonServiceHandler = jsonServiceHandler;
            this.databaseHandler = databaseHandler;
            this.listener = listener;
        }

        //private Context mContext;


        @Override
        protected String doInBackground(Void... voids) {

            for (Map.Entry entry : migrationList.entrySet()) {
                try {
                    final int id = (int) entry.getKey();
                    final PKDModel currentData = (PKDModel) entry.getValue();
                    String data = currentData.getUrlData();
                    String url = currentData.getUrl();
                    JsonServiceHandler.setParams(data); /////////////
                    JSONObject JsonAccountObject = JsonServiceHandler.ServiceData();
                    android.util.Log.e("PKDAsyncTask :", ".....Response::"+JsonAccountObject.toString());

                    String strStatus = JsonAccountObject.getString("status");
                    android.util.Log.e("return status", strStatus);
                    String strMsg = JsonAccountObject.getString("message");
                    android.util.Log.e("return message", strMsg);
                    if (strStatus != null && strStatus.equals("true")) {
                        currentData.setStatus(0);
                        migrationList.put(id, currentData);
                        databaseHandler.updatePKDSync(id, Migration.MIGRATION_COMPLETE, strMsg);
                    }else {
                        String errorMSG = Constants.EMPTY;
                        if(strMsg != null) {
                            errorMSG = strMsg;
                            currentData.setStatus(1);
                        }else {
                            currentData.setStatus(-1);
                        }
                        migrationList.put(id, currentData);
                        databaseHandler.updatePKDSync(id, Migration.MIGRATION_COMPLETE, errorMSG);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    Log.e("PKD.Mig",".............Exception"+ e.getMessage());
                }
            }
            return Constants.SUCCESS;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.e("PKDAsyncTask","onPostExecute ...........................result...." + result);
            if(result.isEmpty()){
                if(listener != null) {
                    listener.onComplete(migrationList);
                }
            }else {
                if(listener != null) {
                    listener.onComplete(migrationList);
                }else {
                    //new PrefManager(mContext).setBooleanDataByKey(KEY_IS_PKD_MIGRATION_START, false);
                }
            }
        }
    }

    private void loadDataToServer() throws Exception{

            new PrefManager(mContext).setBooleanDataByKey(KEY_IS_PKD_MIGRATION_START, true);
            for (Map.Entry entry : migrationList.entrySet()) {
                final int id = (int) entry.getKey();
                final PKDModel currentData = (PKDModel) entry.getValue();
                String data = currentData.getUrlData();
                String url = currentData.getUrl();
                final Request request = new Request(url, Request.Method.POST, Request.REQUEST_DEFAULT);
                request.setRequestBody(data);
                VolleySingleton.getInstance().connect(request, new VolleySingleton.ResponseListener() {

                    @Override
                    public void onSuccess(Response res) {
                        currentData.setStatus(0);
                        migrationList.put(id, currentData);
                        databaseHandler.updatePKDSync(id, Migration.MIGRATION_COMPLETE, Constants.EMPTY);
                        migrationCount--;
                        if(migrationCount == 0){ // Migration Complete
                            if(listener != null) {
                                listener.onComplete(migrationList);
                            }else {
                                new PrefManager(mContext).setBooleanDataByKey(KEY_IS_PKD_MIGRATION_START, false);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Response res) {
                        setFailureStatus(res.message, currentData, id);
                        /*currentData.setStatus(-1);
                        migrationList.put(id, currentData);
                        databaseHandler.updatePKDSync(id, Migration.MIGRATION_FAILED, Constants.EMPTY);
                        migrationCount--;
                        if(migrationCount == 0){ // Migration Complete
                            if(listener != null) {
                                listener.onComplete(migrationList);
                            }else {
                                new PrefManager(mContext).setBooleanDataByKey(KEY_IS_PKD_MIGRATION_START, false);
                            }
                        } */
                    }
                });
            }
    }





    private void setFailureStatus(String error, PKDModel currentData, int id){

        int status = -1;

        if(error.equals(Constants.TIME_OUT_ERROR)){
            status = 1; // Again retry set;
        }else if(error.equals(Constants.AUTH_FAILURE_ERROR)){
            status = -1;
        }else if(error.equals(Constants.SERVER_ERROR)){
            status = 1;
        }else if(error.equals(Constants.NETWORK_ERROR)){
            status = 1;
        }else if(error.equals(Constants.PARSE_ERROR)){
            status = -1;
        }else {
            status = -1;
        }

        currentData.setStatus(status);
        migrationList.put(id, currentData);
        databaseHandler.updatePKDSync(id, Migration.MIGRATION_FAILED, error);
        migrationCount--;
        if(migrationCount == 0){ // Migration Complete
            if(listener != null) {
                listener.onComplete(migrationList);
            }else {
                new PrefManager(mContext).setBooleanDataByKey(KEY_IS_PKD_MIGRATION_START, false);
            }
        }
    }
}
