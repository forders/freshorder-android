package com.freshorders.freshorder.toonline;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FailedClosingStockToServer {

    public interface ServerMigrationListener{
        void onMigrationFinished(boolean isSuccess, String statusMSG, boolean isException, String excMSG);
    }

    private Context mContext;
    DatabaseHandler databaseHandler;
    public SQLiteDatabase db;
    private PendingClosingStockToServer.ServerMigrationListener listener;
    private String Merchantname = "";

    public static String payment_type,orderdate,Orderno,flag,productStatus,mode,paymenttype,pushstauts,OfflineOrderNo,rowid,beatrowid, selected_dealer;
    public static int year,month,day,hr,mins,sec,deliverydt;

    public  static ArrayList<String> arraylistimagepath;

    public  static  String [] image;

    JSONObject JsonAccountObject = null;
    JSONArray JsonAccountArray = null;
    com.freshorders.freshorder.utils.JsonServiceHandler JsonServiceHandler;

    public static String imagepath1 = "", imagepath2 = "", imagepath3 = "", imagepath4 = "", imagepath5 = "", imagepath6 = "",
            imagepath7 = "", imagepath8 = "", imagepath9 = "", imagepath10 = "", imagepath11 = "", imagepath12 = "",
            merchantRowid = "";

    public FailedClosingStockToServer(Context mContext, PendingClosingStockToServer.ServerMigrationListener listener) {
        this.mContext = mContext;
        databaseHandler = new DatabaseHandler(mContext);
        this.listener = listener;
        Log.e("DataMigration", "started.............Constructor");
        arraylistimagepath=new ArrayList<String>();
        db = mContext.openOrCreateDatabase("freshorders", 0, null);
    }

    public void setListenerNULL(){
        if(listener != null) {
            listener = null;
        }
    }

    public PendingClosingStockToServer.ServerMigrationListener getListener(){
        return listener;
    }




    public synchronized void migration() {

        try {
            Cursor curs;
            curs = db.rawQuery("SELECT * from stockOrderHeader where pushstatus ='Failed'", null);

            Log.e("HeaderCount", String.valueOf(curs.getCount()));
            final JSONArray resultSet = new JSONArray();

            if (curs != null && curs.getCount() > 0) {

                new PrefManager(mContext.getApplicationContext()).setBooleanDataByKey(PrefManager.KEY_IS_FAILED_CLOSING_MIGRATION_START, true);

                curs.moveToFirst();
                while (curs.isAfterLast() == false) {

                    int totalColumn = curs.getColumnCount();
                    JSONObject rowObject = new JSONObject();

                    for (int i = 0; i < totalColumn; i++) {

                        if (curs.getColumnName(i) != null) {

                            try {

                                if (curs.getString(i) != null) {
                                    if (!curs.getString(i).equals("New User")) {
                                        Log.d("TAG_NAME", curs.getString(i));
                                        rowObject.put(curs.getColumnName(i), curs.getString(i));
                                    }
                                  /*  Log.d("TAG_NAME", curs.getString(i));
                                    rowObject.put(curs.getColumnName(i), curs.getString(i));*/
                                } else {

                                    if (!curs.getColumnName(i).equals("muserid")) {
                                        rowObject.put(curs.getColumnName(i), "");
                                    }

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                if (listener != null) {
                                    listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                                    return;
                                }
                            }
                        }
                    }


                    try {

                        Cursor curs1;
                        curs1 = db.rawQuery("SELECT userid from dealertable", null);
                        Log.e("dEALER", String.valueOf(curs1.getCount()));
                        if (curs1.getCount() > 0) {
                            curs1.moveToFirst();
                            selected_dealer = curs1.getString(0);
                        }


                        Cursor cursor2;
                        Merchantname = rowObject.getString("mname");

                        cursor2 = db.rawQuery("SELECT * from merchanttable where companyname =" + "'" + Merchantname + "'", null);
                        Log.e("HeaderCountCanled", String.valueOf(cursor2.getCount()));

                        cursor2.moveToFirst();
                        while (cursor2.isAfterLast() == false) {

                            int Columncount = cursor2.getColumnCount();
                            Log.e("merchantColumn", String.valueOf(cursor2.getColumnCount()));
                            JSONObject merchantdetail = new JSONObject();
                            for (int j = 0; j < Columncount; j++) {
                                if (cursor2.getColumnName(j) != null) {
                                    try {
                                        if (cursor2.getColumnName(j).equals("cflag")) {
                                            if (cursor2.getString(j).equals("R")) {
                                                merchantdetail.put(cursor2.getColumnName(j), "G");
                                            } else if (cursor2.getString(j).equals("G")) {
                                                merchantdetail.put(cursor2.getColumnName(j), "");
                                            } else {
                                                merchantdetail.put(cursor2.getColumnName(j), "");
                                            }

                                        } else {
                                            if (cursor2.getString(j) != null) {
                                                if (!cursor2.getString(j).equals("New User")) {

                                                    merchantdetail.put(cursor2.getColumnName(j), cursor2.getString(j));

                                                }
                                                //merchantdetail.put(cursor2.getColumnName(j) ,  cursor2.getString(j) );
                                            } else {
                                                if (!cursor2.getColumnName(j).equals("userid")) {
                                                    merchantdetail.put(cursor2.getColumnName(j), "");
                                                }

                                            }
                                        }

                                    } catch (Exception e) {
                                        Log.d("Merchant_Exception", e.getMessage());
                                        e.printStackTrace();
                                        if (listener != null) {
                                            listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                                            return;
                                        }
                                    }
                                }

                            }
                            merchantdetail.put("usertype", "M");
                            merchantdetail.put("duserid", selected_dealer);
                            rowObject.put("merchant", merchantdetail);
                            cursor2.moveToNext();
                        }
                        cursor2.close();
                        Log.e("Detailarray", rowObject.toString());

                    } catch (Exception e) {
                        e.printStackTrace();
                        if (listener != null) {
                            listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                            return;
                        }
                    }

                    Log.e("jsonarray", rowObject.toString());

                    JSONArray resultDetail = new JSONArray();
                    try {

                        Cursor cursor;
                        cursor = db.rawQuery("SELECT * from stockOrderDetail where stockOrderDetail.oflnordid=" + rowObject.getString("oflnordid"), null);
                        Log.e("DetailCount", String.valueOf(cursor.getCount()));
                        cursor.moveToFirst();
                        while (cursor.isAfterLast() == false) {

                            int Columncount = cursor.getColumnCount();
                            JSONObject detailObject = new JSONObject();

                            for (int i = 0; i < Columncount; i++) {
                                if (cursor.getColumnName(i) != null) {

                                    try {

                                        if (cursor.getString(i) != null) {
                                            Log.d("TAG_NAME", cursor.getString(i));
                                            detailObject.put(cursor.getColumnName(i), cursor.getString(i));
                                        } else {
                                            detailObject.put(cursor.getColumnName(i), "");

                                        }
                                    } catch (Exception e) {
                                        Log.d("TAG_NAME", e.getMessage());
                                        e.printStackTrace();
                                        if (listener != null) {
                                            listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                                            return;
                                        }
                                    }
                                }

                            }

                            resultDetail.put(detailObject);
                            cursor.moveToNext();
                        }
                        Log.e("arraylist", String.valueOf(arraylistimagepath.size()));
                        cursor.close();

                        Log.e("Detailarray", resultDetail.toString());

                    } catch (Exception e) {
                        e.printStackTrace();
                        if (listener != null) {
                            listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                            return;
                        }
                    }
                    try {
                        rowObject.put("orderdtls", resultDetail);
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (listener != null) {
                            listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                            return;
                        }
                    }
                    resultSet.put(rowObject);
                    curs.moveToNext();
                }
                curs.close();
                Log.e("SaveArray111", resultSet.toString());
                JsonServiceHandler = new JsonServiceHandler(Utils.strsavemerchantOderdetail, mContext);
                new FailedClosingStockToServer.MigrationAsyncTask(db, listener, databaseHandler, JsonServiceHandler, resultSet).execute();
            } else {
                Log.e("else_ordercancel", "ordercancel");
                ///////////ordercancel();
            }
        }catch (Exception e){
            e.printStackTrace();
            if(listener != null) {
                listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
            }
        }
    }

    public static class MigrationAsyncTask extends AsyncTask<Void, Void, String> {
        private  SQLiteDatabase db;
        private PendingClosingStockToServer.ServerMigrationListener listener;
        private DatabaseHandler databaseHandler;
        private JSONObject JsonAccountObject;
        private JsonServiceHandler JsonServiceHandler;
        private JSONArray resultSet;

        String strStatus = "";
        String strMsg = "", strOrderId = "";

        public MigrationAsyncTask(SQLiteDatabase db,
                                  PendingClosingStockToServer.ServerMigrationListener listener,
                                  DatabaseHandler databaseHandler,
                                  JsonServiceHandler jsonServiceHandler,
                                  JSONArray resultSet) {
            this.db = db;
            this.listener = listener;
            this.databaseHandler = databaseHandler;
            JsonServiceHandler = jsonServiceHandler;
            this.resultSet = resultSet;
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onPostExecute(String result) {
            Log.e("AllWork","Done by .....................................");
            if(result != null) {
                listener.onMigrationFinished(true, Constants.MIGRATION_SUCCESS, false, Constants.EMPTY);
            }
        }

        @SuppressLint("WrongThread")
        @Override
        protected String doInBackground(Void... voids) {
            JSONObject jsonObject = new JSONObject();
            try {

                for (int k = 0; k < resultSet.length(); k++) {
                    jsonObject = resultSet.getJSONObject(k);
                    Log.e("jsonObjectOffline", String.valueOf(jsonObject));
                    OfflineOrderNo = jsonObject.getString("oflnordid");
                    System.out.println("BeforeChange1: " + jsonObject);
                    JSONObject jsonObjMerchant = jsonObject.getJSONObject("merchant");
                    System.out.println("BeforeChangejsonObjMerchant1: " + jsonObjMerchant);
                    jsonObjMerchant.put("createdby", Constants.USER_ID);
                    jsonObjMerchant.put("customdata", "");
                    System.out.println("AfterChangejsonObjMerchant1: " + jsonObjMerchant);
                    jsonObject.put("merchant", jsonObjMerchant);
                    System.out.println("AfterChange1: " + jsonObject);
                    System.out.println("SVTestinside101");
                    JsonServiceHandler.setParams(jsonObject.toString()); /////////////
                    JsonAccountObject = JsonServiceHandler.ServiceData();

                    try {
                        strOrderId = ""; strStatus = ""; strMsg = ""; //////////////////////recent change Kumaravel
                        strStatus = JsonAccountObject.getString("status");
                        if(strStatus != null && !strStatus.isEmpty()) {
                            Log.e("return status", strStatus);
                            strMsg = JsonAccountObject.getString("message");
                            Log.e("return message", strMsg);

                            if (strStatus.equals("true")) {
                                strOrderId = JsonAccountObject.getJSONObject("data").getString("ordid");
                                Log.e("strOrderId", strOrderId);
                                String muserid = JsonAccountObject.getJSONObject("data").getString("muserid");

                                String updatedFlag = jsonObject.getJSONObject("merchant").getString("cflag");
                                Log.e("updatedFlag", updatedFlag);

                                String companyName = jsonObject.getString("mname");

                            /*
                            db.execSQL("UPDATE merchanttable SET userid = " + muserid + ",cflag =" + "'" + updatedFlag + "'" + " WHERE companyname =" + "'" + companyName + "'");


                            Cursor curs3;
                            curs3 = db.rawQuery("SELECT rowid from merchanttable where userid=" + muserid, null);
                            Log.e("merchantrowid", String.valueOf(curs3.getCount()));
                            String merchantRowid = "";
                            if (curs3 != null && curs3.getCount() > 0) {

                                if (curs3.moveToFirst()) {
                                    Log.e("rowid ", curs3.getString(0));
                                    merchantRowid = curs3.getString(0);

                                }
                            }
                            Log.e("rowidmuserid ", muserid);
                            db.execSQL("UPDATE beat SET muserid = " + muserid + " ,flag =" + "'InActive'" + " WHERE mid =" + "'" + merchantRowid + "'");


                            Cursor curs4;
                            curs4 = db.rawQuery("SELECT muserid from beat where muserid=" + muserid, null);
                            Log.e("mid", String.valueOf(curs4.getCount()));
                            if (curs3 != null && curs3.getCount() > 0) {

                                if (curs3.moveToFirst()) {
                                    Log.e("mid ", curs3.getString(0));

                                }
                            }  */

                                Log.e("arraysize", String.valueOf(arraylistimagepath.size()));

                                String rowid = jsonObject.getJSONArray("orderdtls").getJSONObject(0).getString("rowid");
                                Log.e("rowid", rowid);

                                Cursor curs;
                                curs = db.rawQuery("SELECT ordimage from stockOrderDetail where stockOrderDetail.rowid=" + rowid, null);

                                Log.e("countorderimage", String.valueOf(curs.getCount()));
                                Log.e("columncount", String.valueOf(curs.getColumnCount()));


                                curs.moveToFirst();
                                while (curs.isAfterLast() == false) {

                                    int totalColumn = curs.getColumnCount();

                                    for (int i = 0; i < totalColumn; i++) {
                                        if (curs.getColumnName(i) != null) {
                                            Log.e("value", String.valueOf(curs.getColumnName(i)));
                                            try {

                                                if (curs.getString(i) != null) {
                                                    image = curs.getString(i).split(",");

                                                    Log.e("value", String.valueOf(curs.getString(i)));
                                                } else {
                                                    arraylistimagepath.add("0");
                                                }
                                            } catch (Exception e) {
                                                Log.d("TAG_NAME", e.getMessage());
                                                e.printStackTrace();
                                                if (listener != null) {
                                                    listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                                                    return null;
                                                }
                                            }
                                        }

                                    }

                                    curs.moveToNext();
                                }
                                curs.close();


                                for (int i = 0; i < image.length; i++) {
                                    arraylistimagepath.add(image[i]);
                                }

                                Log.e("arraysize", String.valueOf(arraylistimagepath.size()));
                                for (int m = 0; m < arraylistimagepath.size(); m++) {
                                    Log.e("inside", "inside");
                                    Log.e("inside", arraylistimagepath.get(0).toString());

                                    if (m == 0) {
                                        imagepath1 = arraylistimagepath.get(0).toString();
                                    } else if (m == 1) {
                                        imagepath2 = arraylistimagepath.get(1).toString();
                                    } else if (m == 2) {
                                        imagepath3 = arraylistimagepath.get(2).toString();
                                    } else if (m == 3) {
                                        imagepath4 = arraylistimagepath.get(3).toString();
                                    } else if (m == 4) {
                                        imagepath5 = arraylistimagepath.get(4).toString();
                                    } else if (m == 5) {
                                        imagepath6 = arraylistimagepath.get(5).toString();
                                    } else if (m == 6) {
                                        imagepath7 = arraylistimagepath.get(6).toString();
                                    } else if (m == 7) {
                                        imagepath8 = arraylistimagepath.get(7).toString();
                                    } else if (m == 8) {
                                        imagepath9 = arraylistimagepath.get(8).toString();
                                    } else if (m == 9) {
                                        imagepath10 = arraylistimagepath.get(9).toString();
                                    } else if (m == 10) {
                                        imagepath11 = arraylistimagepath.get(10).toString();
                                    } else if (m == 11) {
                                        imagepath12 = arraylistimagepath.get(11).toString();
                                    }

                                    Log.e("path", String.valueOf(arraylistimagepath.get(m)));

                                }


                                db.execSQL("UPDATE stockOrderHeader SET pushstatus = 'Success',onlineorderno =" + strOrderId + " WHERE oflnordid =" + OfflineOrderNo);
                                Cursor cur;
                                cur = db.rawQuery("SELECT pushstatus from stockOrderHeader where pushstatus ='Success'", null);
                                Log.e("HeaderStatusCount", String.valueOf(cur.getCount()));
                            /*
                            SM_uploadToServerCheckout_NCR ufs = new SM_uploadToServerCheckout_NCR(
                                    imagepath1, imagepath2, imagepath3, imagepath4,
                                    imagepath5, imagepath6, imagepath7, imagepath8,
                                    imagepath9, imagepath10, imagepath11, imagepath12, strOrderId);
                            ufs.execute();///////////////////////////////////////////////remove lint  */ // No need for closing stock


                                Log.e("length", String.valueOf(resultSet.length() - 1));

                                if (k == (resultSet.length() - 1)) {

                                    //////////////ordercancel();
                                    Log.e("ordercancel_IF", "ordercancel");
                                }

                            } else {
                                Cursor curs;
                                if (strMsg != null && !strMsg.isEmpty() && strMsg.equals(Constants.ORDER_ALREADY_EXIST)) {
                                    db.execSQL("UPDATE stockOrderHeader SET pushstatus = 'Failed' WHERE oflnordid =" + OfflineOrderNo); // Kumaravel
                                } else if (strMsg != null && !strMsg.isEmpty() && strMsg.equals("Network unavailable, Please try again later")) {
                                    db.execSQL("UPDATE stockOrderHeader SET pushstatus = 'Pending' WHERE oflnordid =" + OfflineOrderNo); // Kumaravel
                                    if (listener != null) {
                                        listener.onMigrationFinished(false, Constants.MIGRATION_FAILED, true, strMsg);
                                        return null;
                                    }
                                    break;
                                } else if (strMsg != null && !strMsg.isEmpty() && strMsg.equals(Constants.ORDER_SAVE_FAILED)) {

                                    db.execSQL("UPDATE stockOrderHeader SET pushstatus = 'Failed' WHERE oflnordid =" + OfflineOrderNo); // Kumaravel
                                } else {
                                    //This may be need to delete order
                                    db.execSQL("UPDATE stockOrderHeader SET pushstatus = 'Failed' WHERE oflnordid =" + OfflineOrderNo); // Kumaravel
                                }
                            }
                        }else {
                            db.execSQL("UPDATE stockOrderHeader SET pushstatus = 'Failed' WHERE oflnordid =" + OfflineOrderNo); // Kumaravel
                        }



                    } catch (JSONException e) {
                        e.printStackTrace();
                        if (listener != null) {
                            listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                            return null;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (listener != null) {
                            listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                            return null;
                        }
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
                if (listener != null) {
                    listener.onMigrationFinished(false, Constants.EMPTY, true, e.getMessage());
                    return null;
                }
            }
            return "Success";
        }
    }

}
