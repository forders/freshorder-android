package com.freshorders.freshorder.toonline;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.model.CommonMigrationModel;
import com.freshorders.freshorder.model.PKDModel;
import com.freshorders.freshorder.model.PostNoteModel;
import com.freshorders.freshorder.model.SurveyModel;
import com.freshorders.freshorder.service.MigrationResetBroadCast;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.service.ServiceForegroundMigration;
import com.freshorders.freshorder.ui.BaseActivity;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.freshorders.freshorder.ui.SalesManOrderActivity.showAlertDialogToast;

public class Migration {

    //newly Added For Migration complete status

    private static String pendingOrder = "Pending Order Migration";
    private static String closingStock = "Closing Stock Migration";
    private static String clientVisit = "Client Visit Migration";
    private static String newClient = "New Client Migration";
    private static String postNote = "Complaint Migration";
    private static String pkd = "PKD Migration";
    private static String survey = "Survey Migration";
    private static String distributorStock = "Distributor Stock Migration";

    private Map<String, Integer> migMonitor = new HashMap<>();

    private static String TAG = Migration.class.getSimpleName();


    // Preference keys
    public static final String KEY_IS_DISTRIBUTOR_STOCK_MIGRATION_START = "is_distributor_stock_migration_start";
    public static final String KEY_IS_SURVEY_MIGRATION_START = "is_survey_migration_start";
    public static final String KEY_IS_PKD_MIGRATION_START = "is_pkd_migration_start";
    public static final String KEY_IS_POST_NOTE_MIGRATION_START = "is_post_note_migration_start";
    public static final String KEY_IS_CLIENT_VISIT_MIGRATION_START = "is_client_visit_migration_start";//
    public static final String KEY_IS_NEW_CLIENT_MIGRATION_START = "is_new_client_migration_start";

    // volley TAG Name
    public static final String TAG_POST_NOTE_MIGRATION = "tag_post_note_migration";

    //
    public static final int MIGRATION_COMPLETE = 0;
    public static final int MIGRATION_PENDING = 1;
    public static final int MIGRATION_FAILED = -1;

    private Context mContext;
    private DatabaseHandler databaseHandler;
    private Notification migNotification;
    private ServiceForegroundMigration.ForegroundMigrationCommunication fullListener;

    public Migration(Context mContext) {
        this.mContext = mContext;
        databaseHandler = new DatabaseHandler(mContext);
    }

    public Migration(Context mContext,
                     Notification migNotification,
                     ServiceForegroundMigration.ForegroundMigrationCommunication fullListener) {
        this.mContext = mContext;
        databaseHandler = new DatabaseHandler(mContext);
        this.migNotification = migNotification;
        this.fullListener = fullListener;
    }

    public boolean isMigrationServiceWorking()
    {
        Intent serviceIntent = new Intent(mContext, ServiceForegroundMigration.class);
        return PendingIntent.getBroadcast(mContext, Constants.MIGRATION_SERVICE_ID, serviceIntent, PendingIntent.FLAG_NO_CREATE)!=null;
    }

    public void getBroadcast_withFlagNoCreate_shouldReturnNullIfNoPendingIntentExists() {
        Intent intent = new Intent(mContext, ServiceForegroundMigration.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, Constants.MIGRATION_SERVICE_ID, intent, PendingIntent.FLAG_NO_CREATE);
        //(pendingIntent)
    }

    public void startMigrationByAlarmManager(Context mContext){
        /** this gives us the time for the first trigger.  */
        try {
            Calendar cal = Calendar.getInstance();
            AlarmManager am = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
            long interval = 1000 * 10; // 20 in milliseconds
            Intent serviceIntent = new Intent(mContext, ServiceForegroundMigration.class);
            PendingIntent servicePendingIntent =
                    PendingIntent.getService(mContext,
                            Constants.MIGRATION_SERVICE_ID, // integer constant used to identify the service
                            serviceIntent,
                            PendingIntent.FLAG_CANCEL_CURRENT);  // FLAG to avoid creating a second service if there's already one running
            if (am != null) {
                am.set(AlarmManager.RTC_WAKEUP,
                        cal.getTimeInMillis() + interval,
                        servicePendingIntent);
            }else {
                android.util.Log.e(TAG, "Alarm Manager get Null");
                Toast.makeText(mContext,"Not able to move data",Toast.LENGTH_LONG).show();
            }
        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(mContext,"Data move interrupted",Toast.LENGTH_LONG).show();
        }

    }

    public boolean isNeedForegroundMigration(){
        //SQLiteDatabase db = mContext.openOrCreateDatabase("freshorders", 0, null);
        //Cursor cursNewClient = db.rawQuery("SELECT * from orderheader where pushstatus IN ('No Order', 'Outlet')", null);
        Cursor cursNewClient = databaseHandler.getAllNewClient();
        Cursor cursOrder = databaseHandler.getorderheader();
        Cursor cursClosingStockOrder = databaseHandler.getStockOrderHeader();
        Cursor cursClientVisit = databaseHandler.getClientVisit(Constants.STATUS_PENDING);
        Cursor cursPostNote = databaseHandler.getPostNotePending();
        Cursor cursPKD = databaseHandler.getPKDPending();
        Cursor cursSurvey = databaseHandler.getPendingSurvey();
        Cursor cursDistributorStock = databaseHandler.getDistributorStockPending();

        return ((cursOrder != null && cursOrder.getCount() > 0) ||
                (cursClientVisit != null && cursClientVisit.getCount() > 0) ||
                (cursNewClient != null && cursNewClient.getCount() > 0) ||
                (cursPostNote != null && cursPostNote.getCount() > 0) ||
                (cursSurvey != null && cursSurvey.getCount() > 0) ||
                (cursDistributorStock != null && cursDistributorStock.getCount() > 0) ||
                (cursPKD != null && cursPKD.getCount() > 0) ||
                (cursClosingStockOrder != null && cursClosingStockOrder.getCount() > 0)) ;
    }

    public void doAllMigration(){
        new PrefManager(mContext).setBooleanDataByKey(PrefManager.KEY_IS_FULL_MIGRATION_SET, true);////
        startNewClientMigration();
    }

    private void doAfterNewClient(){
        pendingOrderMigration();
        clientVisitMigration();

    }
    private void doAfterMyClientVisit(){

        doPKDMigration();

    }

    private void doAfterPKD(){
        pendingClosingStockMigration();
    }

    private void doAfterClosingStock(){
        doPostNoteMigration();
        doSurveyMigration();
        doDistributorStockMigration();

        start();////////////
    }


    private synchronized void pendingOrderMigration(){
        try{
            boolean isMigrationSet = new PrefManager(mContext).getBooleanDataByKey(PrefManager.KEY_IS_MIGRATION_SET);
            if (!isMigrationSet) {
                DatabaseHandler databaseHandler = new DatabaseHandler(mContext);
                Cursor curs = databaseHandler.getorderheader();
                if (curs.getCount() > 0) {
                    migMonitor.put(pendingOrder, 2);
                    android.util.Log.e("DataMigration", "started.............");
                    // Do Migration
                    new BendingOrderMigrateToOnline(mContext, new BendingOrderMigrateToOnline.ServerMigrationListener() {
                        @Override
                        public void onMigrationFinished(boolean isSuccess, String statusMSG, boolean isException, final String excMSG) {
                            new PrefManager(mContext).setBooleanDataByKey(PrefManager.KEY_IS_MIGRATION_SET, false);
                            migMonitor.remove(pendingOrder);
                        }
                    }).migration();
                } else {
                    android.util.Log.e("Migration", "NO Order Migrate");
                }
            }else {
                Log.e(TAG, "Pending Order Migration Already Working.....");
            }
        }catch (Exception e){
            migMonitor.remove(pendingOrder);
            e.printStackTrace();
            Log.e(TAG, "Pending Order Migration Exception....."+ e.getMessage());
        }
    }

    private synchronized void pendingOrderMigrationWithNewUserOrder(){
        try{
            boolean isMigrationSet = new PrefManager(mContext).getBooleanDataByKey(PrefManager.KEY_IS_NEW_CLIENT_ORDER_MIGRATION_SET);
            if (!isMigrationSet) {
                DatabaseHandler databaseHandler = new DatabaseHandler(mContext);
                Cursor curs = databaseHandler.getorderheader();
                if (curs.getCount() > 0) {
                    android.util.Log.e("DataMigration", "started.............");
                    // Do Migration
                    new NewClientPendingOrderToServer(mContext, true, new NewClientPendingOrderToServer.ServerMigrationListener() {
                        @Override
                        public void onMigrationFinished(boolean isSuccess, String statusMSG, boolean isException, final String excMSG) {
                            new PrefManager(mContext).setBooleanDataByKey(PrefManager.KEY_IS_NEW_CLIENT_ORDER_MIGRATION_SET, false);
                        }
                    }).migration();
                } else {
                    android.util.Log.e("Migration New Client", "NO Order Migrate");
                }
            }else {
                Log.e(TAG, "Pending Order Migration Already Working For New Client.....");
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG, "Pending Order Migration Exception..For New Client..."+ e.getMessage());
        }
    }

    private synchronized void pendingClosingStockMigration(){
        try{
            boolean isMigrationSet = new PrefManager(mContext).getBooleanDataByKey(PrefManager.KEY_IS_CLOSING_MIGRATION_START);
            if (!isMigrationSet) {
                DatabaseHandler databaseHandler = new DatabaseHandler(mContext);
                Cursor curs = databaseHandler.getStockOrderHeader();
                if (curs.getCount() > 0) {
                    migMonitor.put(closingStock, 5);
                    android.util.Log.e("DataMigration", "started.............");
                    // Do Migration
                    new PendingClosingStockToServer(mContext, new PendingClosingStockToServer.ServerMigrationListener() {
                        @Override
                        public void onMigrationFinished(boolean isSuccess, String statusMSG, boolean isException, final String excMSG) {
                            new PrefManager(mContext).setBooleanDataByKey(PrefManager.KEY_IS_CLOSING_MIGRATION_START, false);
                            doAfterClosingStock();
                            migMonitor.remove(closingStock);
                        }
                    }).migration();
                } else {
                    doAfterClosingStock();
                    android.util.Log.e("Migration", "NO Closing Stock Migrate Need");
                }
            }else {
                doAfterClosingStock();
                Log.e(TAG, "Closing Stock Migration Already Working.....");
            }
        }catch (Exception e){
            migMonitor.remove(closingStock);
            doAfterClosingStock();
            e.printStackTrace();
            Log.e(TAG, "Closing Stock Migrate Exception....."+ e.getMessage());
            new PrefManager(mContext).setBooleanDataByKey(PrefManager.KEY_IS_CLOSING_MIGRATION_START, false);
        }
    }

    private void clientVisitMigration(){
        try {
            migMonitor.put(clientVisit, 3);
            // New Client Migration
            new PendingClientVisitToServer(mContext, new PendingClientVisitToServer.ServerCompleteListener() {
                @Override
                public void onSuccess() {
                    migMonitor.remove(clientVisit);
                    android.util.Log.e("ClientVisitToServer", "Pending Client Visit Successfully Moved");
                    doAfterMyClientVisit();
                }
                @Override
                public void onFailed(String errorMSG) {
                    migMonitor.remove(clientVisit);
                    android.util.Log.e("ClientVisitToServer", "Pending Client Visit Failed");
                    doAfterMyClientVisit();
                }
            }).startMyClientVisitMigration();
        }catch (Exception e){
            migMonitor.remove(clientVisit);
            e.printStackTrace();
            doAfterMyClientVisit();
        }
    }

    private void startNewClientMigration(){

        try {
            Cursor cursNewClient = databaseHandler.getAllNewClient();
            if(cursNewClient != null && cursNewClient.getCount() > 0) {
                cursNewClient.close();
                migMonitor.put(newClient, 1);
                new PendingNewClientToOnline(mContext, new PendingNewClientToOnline.NewClientCompleteListener() {
                    @Override
                    public void onWorkSuccess() {
                        //Toast.makeText(mContext, "New Client Migrated Successfully" , Toast.LENGTH_LONG).show();
                        android.util.Log.e(TAG, "New Client Migrated Successfully........");
                        ///////////////////pendingOrderMigrationWithNewUserOrder();
                        doAfterNewClient();
                        migMonitor.remove(newClient);
                    }

                    @Override
                    public void onWorkFailed(final String msg) {
                        //Toast.makeText(mContext, "New Client Migration Failed" , Toast.LENGTH_LONG).show();
                        Log.e(TAG, "New Client Migration Failed........");
                        doAfterNewClient();
                        migMonitor.remove(newClient);
                    }
                }).startPendingNewClientMigration();
            }else {
                doAfterNewClient();
            }
        } catch (Exception e) {
            migMonitor.remove(newClient);
            e.printStackTrace();
            //Toast.makeText(mContext, "New Client Migration Failed ::::: " + e.getMessage() , Toast.LENGTH_LONG).show();
            Log.e(TAG, "New Client Migration Exception........" + e.getMessage());
            doAfterNewClient();
        }
    }

    public void doPostNoteMigration(){
        final String TAG = "Mig.PostNote";
        boolean isMigrationStarted = false;
        isMigrationStarted = new PrefManager(mContext).getBooleanDataByKey(KEY_IS_POST_NOTE_MIGRATION_START);
        if(!isMigrationStarted) {
            migMonitor.put(postNote, 6);
            new PendingPostNoteToServer(mContext, new PendingPostNoteToServer.PostNoteMigrationCompleteListener() {
                @Override
                public void onComplete(LinkedHashMap<Integer, PostNoteModel> migrationList) {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_POST_NOTE_MIGRATION_START, false);
                    Log.e(TAG, "Migration Complete.........." + migrationList.size());
                    migMonitor.remove(postNote);
                }

                @Override
                public void noMigrationRequired() {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_POST_NOTE_MIGRATION_START, false);
                    android.util.Log.e(TAG, "No Migration To Start..........");
                    migMonitor.remove(postNote);
                }

                @Override
                public void migrationStartFailed() {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_POST_NOTE_MIGRATION_START, false);
                    Log.e(TAG, "migrationStartFailed...............");
                    migMonitor.remove(postNote);
                }

                @Override
                public void migrationInterrupted(LinkedHashMap<Integer, PostNoteModel> migrationList) {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_POST_NOTE_MIGRATION_START, false);
                    Log.e(TAG, "Migration Interrupted.........." + migrationList.size());
                    migMonitor.remove(postNote);
                }
            }).startPostNoteMigration();
        }else {
            Log.e(TAG,"Post Note Migration Already Started...............");
        }
    }

    public void doPKDMigration(){
        final String TAG = "PKD.Mig";
        boolean isMigrationStarted = new PrefManager(mContext).getBooleanDataByKey(KEY_IS_PKD_MIGRATION_START);
        if(!isMigrationStarted) {
            migMonitor.put(pkd, 4);
            new PendingPKDToServer(mContext, new PendingPKDToServer.PKDMigrationCompleteListener() {
                @Override
                public void onComplete(LinkedHashMap<Integer, PKDModel> migrationList) {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_PKD_MIGRATION_START, false);
                    Log.e(TAG, "Migration Complete.........." + migrationList.size());
                    doAfterPKD();
                    migMonitor.remove(pkd);
                }

                @Override
                public void noMigrationRequired() {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_PKD_MIGRATION_START, false);
                    android.util.Log.e(TAG, "No Migration To Start..........");
                    doAfterPKD();
                    migMonitor.remove(pkd);
                }

                @Override
                public void migrationStartFailed() {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_PKD_MIGRATION_START, false);
                    Log.e(TAG, "migrationStartFailed...............");
                    doAfterPKD();
                    migMonitor.remove(pkd);
                }

                @Override
                public void migrationInterrupted(LinkedHashMap<Integer, PKDModel> migrationList) {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_PKD_MIGRATION_START, false);
                    Log.e(TAG, "Migration Interrupted.........." + migrationList.size());
                    doAfterPKD();
                    migMonitor.remove(pkd);

                }
            }).startPKDMigration();
        }else {
            doAfterPKD();
            Log.e(TAG,"PKD Migration Already Started...............");
        }
    }


    public void doSurveyMigration(){
        final String TAG = "Mig.Survey";
        boolean isMigrationStarted = new PrefManager(mContext).getBooleanDataByKey(KEY_IS_SURVEY_MIGRATION_START);
        if(!isMigrationStarted) {
            migMonitor.put(survey, 7);
            new PendingSurveyToServer(mContext, new PendingSurveyToServer.SurveyMigrationCompleteListener() {
                @Override
                public void onComplete(LinkedHashMap<Integer, SurveyModel> migrationList) {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_SURVEY_MIGRATION_START, false);
                    Log.e(TAG, "Migration Complete.........." + migrationList.size());
                    migMonitor.remove(survey);
                }

                @Override
                public void noMigrationRequired() {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_SURVEY_MIGRATION_START, false);
                    android.util.Log.e(TAG, "No Migration To Start..........");
                    migMonitor.remove(survey);
                }

                @Override
                public void migrationStartFailed() {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_SURVEY_MIGRATION_START, false);
                    Log.e(TAG, "migrationStartFailed...............");
                    migMonitor.remove(survey);
                }

                @Override
                public void migrationInterrupted(LinkedHashMap<Integer, SurveyModel> migrationList) {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_SURVEY_MIGRATION_START, false);
                    Log.e(TAG, "Migration Interrupted.........." + migrationList.size());
                    migMonitor.remove(survey);

                }
            }).startSurveyMigration();
        }else {
            Log.e(TAG,"Survey Migration Already Started...............");
        }
    }

    public void doDistributorStockMigration(){
        final String TAG = "Mig.Survey";
        boolean isMigrationStarted = new PrefManager(mContext).getBooleanDataByKey(KEY_IS_DISTRIBUTOR_STOCK_MIGRATION_START);
        if(!isMigrationStarted) {
            migMonitor.put(distributorStock, 8);
            new PendingDistributorStockToServer(mContext, new PendingDistributorStockToServer.DistributorStockMigrationCompleteListener() {
                @Override
                public void onComplete(LinkedHashMap<Integer, CommonMigrationModel> migrationList) {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_DISTRIBUTOR_STOCK_MIGRATION_START, false);
                    Log.e(TAG, "Distributor Stock Migration Complete.........." + migrationList.size());
                    migMonitor.remove(distributorStock);
                }

                @Override
                public void noMigrationRequired() {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_DISTRIBUTOR_STOCK_MIGRATION_START, false);
                    android.util.Log.e(TAG, "NO Distributor Stock Migration  To Start..........");
                    migMonitor.remove(distributorStock);
                }

                @Override
                public void migrationStartFailed() {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_DISTRIBUTOR_STOCK_MIGRATION_START, false);
                    Log.e(TAG, "Distributor Stock Migration StartFailed...............");
                    migMonitor.remove(distributorStock);
                }

                @Override
                public void migrationInterrupted(LinkedHashMap<Integer, CommonMigrationModel> migrationList) {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_DISTRIBUTOR_STOCK_MIGRATION_START, false);
                    Log.e(TAG, "istributor Stock Migration Interrupted.........." + migrationList.size());
                    migMonitor.remove(distributorStock);

                }
            }).startDistributorStockMigration();
        }else {
            Log.e(TAG,"Distributor Stock Migration Already Started...............");
        }
    }







    private boolean started = false;
    private Handler handler = new Handler();
    private static int count = 0;

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            android.util.Log.e("Runnable","..............................................................count...." + count++);
            //final Random random = new Random();
            //int i = random.nextInt(2 - 0 + 1) + 0;
            //random_note.setImageResource(image[i]);
            if(migMonitor.size() <= 0){
                //migNotification.
                //ServiceForegroundMigration.stopForeground(true);
                stop();
            }else {
                start();
            }
            //if(started) {
              //  start();
            //}
        }
    };

    public void stop() {
        started = false;
        handler.removeCallbacks(runnable);
        android.util.Log.e("Runnable","..............................................................stop...." + count++);
        startReset(mContext.getApplicationContext());
    }

    public void start() {
        started = true;
        handler.postDelayed(runnable, 1000 * 30);

    }


    public void startReset(Context context){

        if(fullListener != null) {
            fullListener.fullMigrationComplete();
        }

        Intent intent = new Intent(context, MigrationResetBroadCast.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 234324243, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if (alarmManager != null) {
            alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + (1000 * 5), pendingIntent);
            Toast.makeText(context, "Migration Re-started",Toast.LENGTH_LONG).show();
        }else {
            Toast.makeText(context, "Migration Re-start failed",Toast.LENGTH_LONG).show();
        }

    }


}
