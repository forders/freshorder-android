package com.freshorders.freshorder.toonline.migration;

import com.freshorders.freshorder.utils.Constants;

public class MenuMigrationModel {

    private String migrationName = Constants.EMPTY;
    private boolean isStart = false;
    private String status = Constants.EMPTY;
    private int migrationCount = 0;
    private int migrationSuccessCount = 0;
    private boolean isInterrupted = false;
    private boolean isSuccess = false;
    private String exceptionReason = Constants.EMPTY;


    public String getMigrationName() {
        return migrationName;
    }

    public void setMigrationName(String migrationName) {
        this.migrationName = migrationName;
    }

    public boolean isStart() {
        return isStart;
    }

    public void setStart(boolean start) {
        isStart = start;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getMigrationCount() {
        return migrationCount;
    }

    public void setMigrationCount(int migrationCount) {
        this.migrationCount = migrationCount;
    }

    public boolean isInterrupted() {
        return isInterrupted;
    }

    public void setInterrupted(boolean interrupted) {
        isInterrupted = interrupted;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }

    public String getExceptionReason() {
        return exceptionReason;
    }

    public void setExceptionReason(String exceptionReason) {
        this.exceptionReason = exceptionReason;
    }

    public int getMigrationSuccessCount() {
        return migrationSuccessCount;
    }

    public void setMigrationSuccessCount(int migrationSuccessCount) {
        this.migrationSuccessCount = migrationSuccessCount;
    }

}
