package com.freshorders.freshorder.toonline.migration;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.model.CommonMigrationModel;
import com.freshorders.freshorder.model.PKDModel;
import com.freshorders.freshorder.model.PostNoteModel;
import com.freshorders.freshorder.model.SurveyModel;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.toonline.BendingOrderMigrateToOnline;
import com.freshorders.freshorder.toonline.Migration;
import com.freshorders.freshorder.toonline.NewClientPendingOrderToServer;
import com.freshorders.freshorder.toonline.PendingClientVisitToServer;
import com.freshorders.freshorder.toonline.PendingClosingStockToServer;
import com.freshorders.freshorder.toonline.PendingDistributorStockToServer;
import com.freshorders.freshorder.toonline.PendingNewClientToOnline;
import com.freshorders.freshorder.toonline.PendingPKDToServer;
import com.freshorders.freshorder.toonline.PendingPostNoteToServer;
import com.freshorders.freshorder.toonline.PendingSurveyToServer;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.Log;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import static com.freshorders.freshorder.toonline.Migration.KEY_IS_DISTRIBUTOR_STOCK_MIGRATION_START;
import static com.freshorders.freshorder.toonline.Migration.KEY_IS_PKD_MIGRATION_START;
import static com.freshorders.freshorder.toonline.Migration.KEY_IS_POST_NOTE_MIGRATION_START;
import static com.freshorders.freshorder.toonline.Migration.KEY_IS_SURVEY_MIGRATION_START;


public class MenuMigration {

    private static String pendingOrder = "Pending Order Migration";
    private static String closingStock = "Closing Stock Migration";
    private static String clientVisit = "Client Visit Migration";
    private static String newClient = "New Client Migration";
    private static String postNote = "Complaint Migration";
    private static String pkd = "PKD Migration";
    private static String survey = "Survey Migration";
    private static String distributorStock = "Distributor Stock Migration";


    private static String migAlreadyRunning = "Migration in progress";
    private static String TAG = MenuMigration.class.getSimpleName();

    private static String MIG_NOT_REQUIRED = "Migration Not Required";
    private static String MIG_FINISHED_SUCCESS = "Migration is successful";
    private static String MIG_FAILED = "Migration Failed";

    private Context mContext;
    private DatabaseHandler databaseHandler;
    public List<MenuMigrationModel> migStatusList;
    public Object migrationBlock;

    public MenuMigration(Context mContext, Object migrationBlock) {
        this.mContext = mContext;
        databaseHandler = new DatabaseHandler(mContext);
        migStatusList = new ArrayList<>();
        this.migrationBlock = migrationBlock;
    }

    public boolean isNeedForegroundMigration() {
        //SQLiteDatabase db = mContext.openOrCreateDatabase("freshorders", 0, null);
        //Cursor cursNewClient = db.rawQuery("SELECT * from orderheader where pushstatus IN ('No Order', 'Outlet')", null);
        Cursor cursNewClient = databaseHandler.getAllNewClient();
        Cursor cursOrder = databaseHandler.getorderheader();
        Cursor cursClosingStockOrder = databaseHandler.getStockOrderHeader();
        Cursor cursClientVisit = databaseHandler.getClientVisit(Constants.STATUS_PENDING);
        Cursor cursPostNote = databaseHandler.getPostNotePending();
        Cursor cursPKD = databaseHandler.getPKDPending();
        Cursor cursSurvey = databaseHandler.getPendingSurvey();
        Cursor cursDistributorStock = databaseHandler.getDistributorStockPending();

        boolean result =  ((cursOrder != null && cursOrder.getCount() > 0) ||
                (cursClientVisit != null && cursClientVisit.getCount() > 0) ||
                (cursNewClient != null && cursNewClient.getCount() > 0) ||
                (cursPostNote != null && cursPostNote.getCount() > 0) ||
                (cursSurvey != null && cursSurvey.getCount() > 0) ||
                (cursDistributorStock != null && cursDistributorStock.getCount() > 0) ||
                (cursPKD != null && cursPKD.getCount() > 0) ||
                (cursClosingStockOrder != null && cursClosingStockOrder.getCount() > 0));
        cursOrder.close(); cursClientVisit.close(); cursNewClient.close(); cursPostNote.close(); cursSurvey.close();
        cursDistributorStock.close(); cursPKD.close(); cursClosingStockOrder.close();
        return result;
    }

    public void startReset(){
        new PrefManager(mContext).setBooleanDataByKey(PrefManager.KEY_IS_MIGRATION_SET, false);
        new PrefManager(mContext).setBooleanDataByKey(PrefManager.KEY_IS_CLOSING_MIGRATION_START, false);
        new PrefManager(mContext).setBooleanDataByKey(Migration.KEY_IS_CLIENT_VISIT_MIGRATION_START, false);
        new PrefManager(mContext).setBooleanDataByKey(Migration.KEY_IS_NEW_CLIENT_MIGRATION_START, false);
        new PrefManager(mContext).setBooleanDataByKey(Migration.KEY_IS_POST_NOTE_MIGRATION_START, false);
        new PrefManager(mContext).setBooleanDataByKey(Migration.KEY_IS_PKD_MIGRATION_START, false);
        new PrefManager(mContext).setBooleanDataByKey(Migration.KEY_IS_SURVEY_MIGRATION_START, false);
        new PrefManager(mContext).setBooleanDataByKey(Migration.KEY_IS_DISTRIBUTOR_STOCK_MIGRATION_START, false);
    }
    /*

        1. pendingOrderMigration();
        2. pendingClosingStockMigration();
        3. clientVisitMigration();
        4. startNewClientMigration();
        5. doPostNoteMigration();
        6. doPKDMigration();
        7. doSurveyMigration();
        8. doDistributorStockMigration();
     */
    public String doSequenceMigration() {
        if (pendingOrderMigration()) {
            if (pendingClosingStockMigration()) {
                if (clientVisitMigration()) {
                    if (startNewClientMigration()) {
                        if (doPostNoteMigration()) {
                            if (doPKDMigration()) {
                                if (doSurveyMigration()) {
                                    if (doDistributorStockMigration()) {
                                        StringBuilder stringBuilder = new StringBuilder();
                                        for (int i = 0; i < migStatusList.size(); i++) {
                                            MenuMigrationModel migrationModel = migStatusList.get(i);
                                            stringBuilder.append("Data Moving Operation Name : ");
                                            stringBuilder.append(migrationModel.getMigrationName());
                                            stringBuilder.append("\n");
                                            if (migrationModel.isStart()) {
                                                stringBuilder.append("Moving Data Count : ");
                                                stringBuilder.append(migrationModel.getMigrationCount());
                                                stringBuilder.append("\n");

                                                stringBuilder.append("Status : ");
                                                stringBuilder.append(migrationModel.getStatus());
                                                stringBuilder.append("\n");
                                            } else {
                                                stringBuilder.append("Moving Failed Reason : ");
                                                stringBuilder.append(migrationModel.getExceptionReason());
                                                stringBuilder.append("\n");

                                                stringBuilder.append("Status : ");
                                                stringBuilder.append(migrationModel.getStatus());
                                                stringBuilder.append("\n");
                                            }
                                            stringBuilder.append("\n");
                                            stringBuilder.append("\n");
                                        }
                                        //migrationBlock.notifyAll();////////////
                                        return stringBuilder.toString();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        //migrationBlock.notifyAll();////////////
        return Constants.EMPTY;
    }



    private synchronized boolean pendingOrderMigration(){
        MenuMigrationModel current = new MenuMigrationModel();
        current.setMigrationName(pendingOrder);
        try{
            boolean isMigrationSet = new PrefManager(mContext).getBooleanDataByKey(PrefManager.KEY_IS_MIGRATION_SET);
            if (!isMigrationSet) {
                DatabaseHandler databaseHandler = new DatabaseHandler(mContext);
                Cursor curs = databaseHandler.getorderheader();
                if (curs.getCount() > 0) {
                    android.util.Log.e("DataMigration", "started.............");
                    // Do Migration
                    new BendingOrderMigrateToOnline(mContext, new BendingOrderMigrateToOnline.ServerMigrationListener() {
                        @Override
                        public void onMigrationFinished(boolean isSuccess, String statusMSG, boolean isException, final String excMSG) {
                            new PrefManager(mContext).setBooleanDataByKey(PrefManager.KEY_IS_MIGRATION_SET, false);
                        }
                    }).migration();
                    current.setMigrationCount(curs.getCount());
                    current.setStatus(MIG_FINISHED_SUCCESS);
                    current.setStart(true);
                    migStatusList.add(current);
                    return true;
                } else {
                    android.util.Log.e("Migration", "NO Order Migrate");
                    current.setMigrationCount(0);
                    current.setStatus(MIG_NOT_REQUIRED);
                    migStatusList.add(current);
                    return true;
                }
            }else {
                Log.e(TAG, "Pending Order Migration Already Working.....");
                current.setStatus(migAlreadyRunning);
                migStatusList.add(current);
                return true;
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG, "Pending Order Migration Exception....."+ e.getMessage());
            current.setInterrupted(true);
            current.setExceptionReason(e.getMessage());
            migStatusList.add(current);
            return true;
        }
    }

    private synchronized boolean pendingClosingStockMigration(){
        MenuMigrationModel current = new MenuMigrationModel();
        current.setMigrationName(closingStock);
        try{
            boolean isMigrationSet = new PrefManager(mContext).getBooleanDataByKey(PrefManager.KEY_IS_CLOSING_MIGRATION_START);
            if (!isMigrationSet) {
                DatabaseHandler databaseHandler = new DatabaseHandler(mContext);
                Cursor curs = databaseHandler.getStockOrderHeader();
                if (curs.getCount() > 0) {
                    android.util.Log.e("DataMigration", "started.............");
                    // Do Migration
                    new PendingClosingStockToServer(mContext, new PendingClosingStockToServer.ServerMigrationListener() {
                        @Override
                        public void onMigrationFinished(boolean isSuccess, String statusMSG, boolean isException, final String excMSG) {
                            new PrefManager(mContext).setBooleanDataByKey(PrefManager.KEY_IS_CLOSING_MIGRATION_START, false);
                        }
                    }).migration();
                    current.setMigrationCount(curs.getCount());
                    current.setStatus(MIG_FINISHED_SUCCESS);
                    current.setStart(true);
                    migStatusList.add(current);
                    return true;
                } else {
                    android.util.Log.e("Migration", "NO Closing Stock Migrate Need");
                    current.setMigrationCount(0);
                    current.setStatus(MIG_NOT_REQUIRED);
                    migStatusList.add(current);
                    return true;
                }
            }else {
                Log.e(TAG, "Closing Stock Migration Already Working.....");
                current.setStatus(migAlreadyRunning);
                migStatusList.add(current);
                return true;
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG, "Closing Stock Migrate Exception....."+ e.getMessage());
            current.setInterrupted(true);
            current.setExceptionReason(e.getMessage());
            migStatusList.add(current);
            new PrefManager(mContext).setBooleanDataByKey(PrefManager.KEY_IS_CLOSING_MIGRATION_START, false);
            return true;
        }
    }

    private boolean clientVisitMigration(){
        final MenuMigrationModel current = new MenuMigrationModel();
        current.setMigrationName(clientVisit);
        try {
            boolean isMigrationSet = new PrefManager(mContext).getBooleanDataByKey(Migration.KEY_IS_CLIENT_VISIT_MIGRATION_START);
            if (!isMigrationSet) {
                final Cursor curs = databaseHandler.getClientVisit(Constants.STATUS_PENDING);
                if (curs != null && curs.getCount() > 0) {
                    // New Client Migration
                    new PendingClientVisitToServer(mContext, new PendingClientVisitToServer.ServerCompleteListener() {
                        @Override
                        public void onSuccess() {
                            android.util.Log.e("ClientVisitToServer", "Pending Client Visit Successfully Moved");
                            current.setMigrationCount(curs.getCount());
                            current.setStatus(MIG_FINISHED_SUCCESS);
                            current.setStart(true);
                            migStatusList.add(current);
                            new PrefManager(mContext).setBooleanDataByKey(Migration.KEY_IS_CLIENT_VISIT_MIGRATION_START, false);
                        }

                        @Override
                        public void onFailed(String errorMSG) {
                            android.util.Log.e("ClientVisitToServer", "Pending Client Visit Failed");
                            new PrefManager(mContext).setBooleanDataByKey(Migration.KEY_IS_CLIENT_VISIT_MIGRATION_START, false);
                            current.setMigrationCount(curs.getCount());
                            current.setStatus(MIG_FAILED);
                            current.setStart(true);
                            migStatusList.add(current);
                        }
                    }).startMyClientVisitMigration();
                    curs.close();
                    return true;
                } else {
                    current.setMigrationCount(0);
                    current.setStatus(MIG_NOT_REQUIRED);
                    migStatusList.add(current);
                    return true;
                }
            }else {
                current.setStatus(migAlreadyRunning);
                migStatusList.add(current);
                return true;
            }
        }catch (Exception e){
            e.printStackTrace();
            new PrefManager(mContext).setBooleanDataByKey(Migration.KEY_IS_CLIENT_VISIT_MIGRATION_START, false);
            current.setInterrupted(true);
            current.setExceptionReason(e.getMessage());
            migStatusList.add(current);
            return true;
        }
    }

    private synchronized void pendingOrderMigrationWithNewUserOrder(){
        try{
            boolean isMigrationSet = new PrefManager(mContext).getBooleanDataByKey(PrefManager.KEY_IS_NEW_CLIENT_ORDER_MIGRATION_SET);
            if (!isMigrationSet) {
                DatabaseHandler databaseHandler = new DatabaseHandler(mContext);
                Cursor curs = databaseHandler.getorderheader();
                if (curs.getCount() > 0) {
                    android.util.Log.e("DataMigration", "started.............");
                    // Do Migration
                    new NewClientPendingOrderToServer(mContext, true, new NewClientPendingOrderToServer.ServerMigrationListener() {
                        @Override
                        public void onMigrationFinished(boolean isSuccess, String statusMSG, boolean isException, final String excMSG) {
                            new PrefManager(mContext).setBooleanDataByKey(PrefManager.KEY_IS_NEW_CLIENT_ORDER_MIGRATION_SET, false);
                        }
                    }).migration();
                } else {
                    android.util.Log.e("Migration New Client", "NO Order Migrate");
                }
            }else {
                Log.e(TAG, "Pending Order Migration Already Working For New Client.....");
            }
        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG, "Pending Order Migration Exception..For New Client..."+ e.getMessage());
        }
    }

    private boolean startNewClientMigration(){
        final MenuMigrationModel current = new MenuMigrationModel();
        current.setMigrationName(newClient);
        final Cursor curs;
        try {
            boolean isMigrationSet = new PrefManager(mContext).getBooleanDataByKey(Migration.KEY_IS_NEW_CLIENT_MIGRATION_START);
            if (!isMigrationSet) {
                //SQLiteDatabase db = mContext.openOrCreateDatabase("freshorders", 0, null);
                //curs = db.rawQuery("SELECT * from orderheader where pushstatus IN ('No Order', 'Outlet')", null);
                curs = databaseHandler.getAllNewClient();
                if (curs != null && curs.getCount() > 0) {
                    new PendingNewClientToOnline(mContext, new PendingNewClientToOnline.NewClientCompleteListener() {
                        @Override
                        public void onWorkSuccess() {
                            //Toast.makeText(mContext, "New Client Migrated Successfully" , Toast.LENGTH_LONG).show();
                            Log.e(TAG, "New Client Migrated Successfully........");
                            current.setMigrationCount(curs.getCount());
                            current.setStatus(MIG_FINISHED_SUCCESS);
                            current.setStart(true);
                            migStatusList.add(current);
                            new PrefManager(mContext).setBooleanDataByKey(Migration.KEY_IS_NEW_CLIENT_MIGRATION_START, false);
                            pendingOrderMigrationWithNewUserOrder();
                        }

                        @Override
                        public void onWorkFailed(final String msg) {
                            //Toast.makeText(mContext, "New Client Migration Failed" , Toast.LENGTH_LONG).show();
                            Log.e(TAG, "New Client Migration Failed........");
                            new PrefManager(mContext).setBooleanDataByKey(Migration.KEY_IS_NEW_CLIENT_MIGRATION_START, false);
                            current.setMigrationCount(curs.getCount());
                            current.setStatus(MIG_FAILED);
                            current.setStart(true);
                            migStatusList.add(current);
                        }
                    }).startPendingNewClientMigration();
                    curs.close();
                }else {
                    if(curs != null){
                        curs.close();
                    }
                    current.setMigrationCount(0);
                    current.setStatus(MIG_NOT_REQUIRED);
                    migStatusList.add(current);
                }
                return true;
            }else {
                current.setStatus(migAlreadyRunning);
                migStatusList.add(current);
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            //Toast.makeText(mContext, "New Client Migration Failed ::::: " + e.getMessage() , Toast.LENGTH_LONG).show();
            Log.e(TAG, "New Client Migration Exception........" + e.getMessage());
            new PrefManager(mContext).setBooleanDataByKey(Migration.KEY_IS_NEW_CLIENT_MIGRATION_START, false);
            current.setInterrupted(true);
            current.setExceptionReason(e.getMessage());
            migStatusList.add(current);
            return true;
        }
    }

    public boolean doPostNoteMigration(){
        final MenuMigrationModel current = new MenuMigrationModel();
        current.setMigrationName(postNote);
        final String TAG = "Mig.PostNote";
        boolean isMigrationStarted = false;
        isMigrationStarted = new PrefManager(mContext).getBooleanDataByKey(KEY_IS_POST_NOTE_MIGRATION_START);
        if(!isMigrationStarted) {
            new PendingPostNoteToServer(mContext, new PendingPostNoteToServer.PostNoteMigrationCompleteListener() {
                @Override
                public void onComplete(LinkedHashMap<Integer, PostNoteModel> migrationList) {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_POST_NOTE_MIGRATION_START, false);
                    Log.e(TAG, "Migration Complete.........." + migrationList.size());
                    current.setMigrationCount(migrationList.size());
                    current.setStatus(MIG_FINISHED_SUCCESS);
                    current.setStart(true);
                    migStatusList.add(current);
                }

                @Override
                public void noMigrationRequired() {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_POST_NOTE_MIGRATION_START, false);
                    Log.e(TAG, "No Migration To Start..........");
                    current.setMigrationCount(0);
                    current.setStatus(MIG_NOT_REQUIRED);
                    migStatusList.add(current);
                }

                @Override
                public void migrationStartFailed() {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_POST_NOTE_MIGRATION_START, false);
                    Log.e(TAG, "migrationStartFailed...............");
                    current.setInterrupted(true);
                    current.setExceptionReason("Reason Not Avail");
                    migStatusList.add(current);
                }

                @Override
                public void migrationInterrupted(LinkedHashMap<Integer, PostNoteModel> migrationList) {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_POST_NOTE_MIGRATION_START, false);
                    Log.e(TAG, "Migration Interrupted.........." + migrationList.size());
                    current.setInterrupted(true);
                    current.setExceptionReason("Reason Not Avail");
                    migStatusList.add(current);
                }
            }).startPostNoteMigration();
            return true;
        }else {
            Log.e(TAG,"Post Note Migration Already Started...............");
            current.setStatus(migAlreadyRunning);
            migStatusList.add(current);
            return true;
        }
    }

    public boolean doPKDMigration(){
        final MenuMigrationModel current = new MenuMigrationModel();
        current.setMigrationName(pkd);
        final String TAG = "Mig.PostNote";
        boolean isMigrationStarted = new PrefManager(mContext).getBooleanDataByKey(KEY_IS_PKD_MIGRATION_START);
        if(!isMigrationStarted) {
            new PendingPKDToServer(mContext, new PendingPKDToServer.PKDMigrationCompleteListener() {
                @Override
                public void onComplete(LinkedHashMap<Integer, PKDModel> migrationList) {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_PKD_MIGRATION_START, false);
                    Log.e(TAG, "Migration Complete.........." + migrationList.size());
                    current.setMigrationCount(migrationList.size());
                    current.setStatus(MIG_FINISHED_SUCCESS);
                    current.setStart(true);
                    migStatusList.add(current);
                }

                @Override
                public void noMigrationRequired() {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_PKD_MIGRATION_START, false);
                    Log.e(TAG, "No Migration To Start..........");
                    current.setMigrationCount(0);
                    current.setStatus(MIG_NOT_REQUIRED);
                    migStatusList.add(current);
                }

                @Override
                public void migrationStartFailed() {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_PKD_MIGRATION_START, false);
                    Log.e(TAG, "migrationStartFailed...............");
                    current.setInterrupted(true);
                    current.setExceptionReason("Reason Not Avail");
                    migStatusList.add(current);
                }

                @Override
                public void migrationInterrupted(LinkedHashMap<Integer, PKDModel> migrationList) {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_PKD_MIGRATION_START, false);
                    Log.e(TAG, "Migration Interrupted.........." + migrationList.size());
                    current.setInterrupted(true);
                    current.setExceptionReason("Reason Not Avail");
                    migStatusList.add(current);

                }
            }).startPKDMigration();
            return true;
        }else {
            Log.e(TAG,"PKD Migration Already Started...............");
            current.setStatus(migAlreadyRunning);
            migStatusList.add(current);
            return true;
        }
    }


    public boolean doSurveyMigration(){
        final MenuMigrationModel current = new MenuMigrationModel();
        current.setMigrationName(survey);
        final String TAG = "Mig.Survey";
        boolean isMigrationStarted = new PrefManager(mContext).getBooleanDataByKey(KEY_IS_SURVEY_MIGRATION_START);
        if(!isMigrationStarted) {
            new PendingSurveyToServer(mContext, new PendingSurveyToServer.SurveyMigrationCompleteListener() {
                @Override
                public void onComplete(LinkedHashMap<Integer, SurveyModel> migrationList) {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_SURVEY_MIGRATION_START, false);
                    Log.e(TAG, "Migration Complete.........." + migrationList.size());
                    current.setMigrationCount(migrationList.size());
                    current.setStatus(MIG_FINISHED_SUCCESS);
                    current.setStart(true);
                    migStatusList.add(current);
                }

                @Override
                public void noMigrationRequired() {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_SURVEY_MIGRATION_START, false);
                    Log.e(TAG, "No Migration To Start..........");
                    current.setMigrationCount(0);
                    current.setStatus(MIG_NOT_REQUIRED);
                    migStatusList.add(current);
                }

                @Override
                public void migrationStartFailed() {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_SURVEY_MIGRATION_START, false);
                    Log.e(TAG, "migrationStartFailed...............");
                    current.setInterrupted(true);
                    current.setExceptionReason("Reason Not Avail");
                    migStatusList.add(current);
                }

                @Override
                public void migrationInterrupted(LinkedHashMap<Integer, SurveyModel> migrationList) {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_SURVEY_MIGRATION_START, false);
                    Log.e(TAG, "Migration Interrupted.........." + migrationList.size());
                    current.setInterrupted(true);
                    current.setExceptionReason("Reason Not Avail");
                    migStatusList.add(current);

                }
            }).startSurveyMigration();
            return true;
        }else {
            Log.e(TAG,"Survey Migration Already Started...............");
            current.setStatus(migAlreadyRunning);
            migStatusList.add(current);
            return true;
        }
    }

    public boolean doDistributorStockMigration(){
        final MenuMigrationModel current = new MenuMigrationModel();
        current.setMigrationName(distributorStock);
        final String TAG = "Mig.Survey";
        boolean isMigrationStarted = new PrefManager(mContext).getBooleanDataByKey(KEY_IS_DISTRIBUTOR_STOCK_MIGRATION_START);
        if(!isMigrationStarted) {
            new PendingDistributorStockToServer(mContext, new PendingDistributorStockToServer.DistributorStockMigrationCompleteListener() {
                @Override
                public void onComplete(LinkedHashMap<Integer, CommonMigrationModel> migrationList) {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_DISTRIBUTOR_STOCK_MIGRATION_START, false);
                    Log.e(TAG, "Distributor Stock Migration Complete.........." + migrationList.size());
                    current.setMigrationCount(migrationList.size());
                    current.setStatus(MIG_FINISHED_SUCCESS);
                    current.setStart(true);
                    migStatusList.add(current);
                }

                @Override
                public void noMigrationRequired() {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_DISTRIBUTOR_STOCK_MIGRATION_START, false);
                    Log.e(TAG, "NO Distributor Stock Migration  To Start..........");
                    current.setMigrationCount(0);
                    current.setStatus(MIG_NOT_REQUIRED);
                    migStatusList.add(current);
                }

                @Override
                public void migrationStartFailed() {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_DISTRIBUTOR_STOCK_MIGRATION_START, false);
                    Log.e(TAG, "Distributor Stock Migration StartFailed...............");
                    current.setInterrupted(true);
                    current.setExceptionReason("Reason Not Avail");
                    migStatusList.add(current);
                }

                @Override
                public void migrationInterrupted(LinkedHashMap<Integer, CommonMigrationModel> migrationList) {
                    new PrefManager(mContext).setBooleanDataByKey(KEY_IS_DISTRIBUTOR_STOCK_MIGRATION_START, false);
                    Log.e(TAG, "istributor Stock Migration Interrupted.........." + migrationList.size());
                    current.setInterrupted(true);
                    current.setExceptionReason("Reason Not Avail");
                    migStatusList.add(current);

                }
            }).startDistributorStockMigration();
            return true;
        }else {
            Log.e(TAG,"istributor Stock Migration Already Started...............");
            current.setStatus(migAlreadyRunning);
            migStatusList.add(current);
            return true;
        }
    }


}
