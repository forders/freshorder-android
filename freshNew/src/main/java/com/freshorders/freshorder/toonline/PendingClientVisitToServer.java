package com.freshorders.freshorder.toonline;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.ui.ClientVisitActivity;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.NetworkUtil;
import com.freshorders.freshorder.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

public class PendingClientVisitToServer {

    public interface ServerCompleteListener{
        void onSuccess();
        void onFailed(String errorMSG);
    }
    private Context context;
    private static SQLiteDatabase db;
    private PendingClientVisitToServer.ServerCompleteListener finalListener;
    public static DatabaseHandler databaseHandler;
    private JSONObject rowObject = null;
    private JSONArray rowArray = null;
    private JsonServiceHandler JsonServiceHandler;

    public PendingClientVisitToServer(Context context,ServerCompleteListener finalListener) throws Exception{
        this.context = context;
        this.finalListener = finalListener;
        db = context.openOrCreateDatabase("freshorders", 0, null);
        databaseHandler = new DatabaseHandler(context);
        rowArray = new JSONArray();
        JsonServiceHandler = new JsonServiceHandler(Utils.strclientvisitsales, context);
    }

    public void startMyClientVisitMigration() throws Exception {
        try {
            String status = NetworkUtil.getConnectivityStatusString(context);
            if (status.equals("Wifi enabled") || status.equals("Mobile data enabled")) {
                Log.e("MyClientVisit", "............Mobile data enabled");
                Cursor curs;
                curs = databaseHandler.getClientVisit(Constants.STATUS_PENDING);
                if(curs != null && curs.getCount() > 0){
                    curs.moveToFirst();
                    while (!curs.isAfterLast()){
                        rowArray.put(formDataToSend(curs)); ///
                        curs.moveToNext();
                    }
                    Log.e("MyClientVisit", "............rowArray" + rowArray.toString());
                    new PendingClientVisitToServer.ClientVisitAsyncTask(
                            JsonServiceHandler, rowArray, finalListener).execute();

                }else {
                    Log.e("MyClientVisit","Migration...........No Data To Migrate.....");
                    if(finalListener != null)
                    finalListener.onSuccess();
                }
            }else {
                if(finalListener != null)
                finalListener.onFailed("--No Mobile Data Found To Migrate ClientVisit--");
            }
        }catch (Exception e){
            e.printStackTrace();
            if(finalListener != null)
            finalListener.onFailed("Client Visit Moving Failed::" + e.getMessage());
        }
    }

    private JSONObject formDataToSend(Cursor curs){

        int totalColumn = curs.getColumnCount();
        rowObject = new JSONObject();

        for (int i = 0; i < totalColumn; i++) {
            if (curs.getColumnName(i) != null) {

                try {

                    if (curs.getString(i) != null) {
                        Log.d("TAG_NAME", curs.getString(i));
                        if(curs.getString(i).equalsIgnoreCase("New User") && curs.getColumnName(i).equals("muserid")){
                            int merchantPrimaryKey = curs.getInt(curs.getColumnIndex("rowid"));
                            Cursor merCur = databaseHandler.getMerchantById(merchantPrimaryKey);
                            if(merCur != null && merCur.getCount() > 0){
                                merCur.moveToFirst();
                                String mUserId = merCur.getString(merCur.getColumnIndex("userid"));
                                Log.e("mUserId","New User MUserId............."+mUserId);
                                rowObject.put(curs.getColumnName(i), mUserId);
                                merCur.close();
                            }
                        }else {
                            rowObject.put(curs.getColumnName(i), curs.getString(i));
                        }
                    } else {
                        rowObject.put(curs.getColumnName(i), "");
                    }


                } catch (Exception e) {
                    Log.d("TAG_NAME", e.getMessage());
                    e.printStackTrace();
                    if(finalListener != null)
                    finalListener.onFailed("Client Visit Moving Failed::" + e.getMessage());
                    return null;
                }
            }
        }
        return rowObject;
    }

    public static class ClientVisitAsyncTask extends AsyncTask<Void, Void, JSONArray> {
        private JsonServiceHandler JsonServiceHandler;
        private JSONArray rowArray;
        private PendingClientVisitToServer.ServerCompleteListener finalListener;
        private JSONArray resultArray;

        public ClientVisitAsyncTask(JsonServiceHandler jsonServiceHandler,
                                    JSONArray rowArray,
                                    PendingClientVisitToServer.ServerCompleteListener finalListener) {
            this.JsonServiceHandler = jsonServiceHandler;
            this.rowArray = rowArray;
            this.finalListener = finalListener;
            resultArray = new JSONArray();
        }

        @Override
        protected JSONArray doInBackground(Void... voids) {
            try {
                for (int i = 0; i < rowArray.length(); i++) {
                    try {
                        JsonServiceHandler.setParams(rowArray.get(i).toString());
                        Log.e("Client Visit", ".........rowArray.." + rowArray.get(i).toString());
                        resultArray.put(JsonServiceHandler.ServiceData());
                        Log.e("Client Visit", ".........resultArray.." + resultArray.get(i).toString());
                        ////////Thread.sleep(500);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
                Log.e("Client Visit", ".........resultArray.." + resultArray.toString());
                return resultArray;
            } catch (Exception e) {
                e.printStackTrace();
                if(finalListener != null)
                finalListener.onFailed("Client Visit Moving Failed::" + e.getMessage());
                return null;
            }
        }

        @Override
        protected void onPostExecute(JSONArray resultArray) {
            try {
                Log.e("Client Visit", "..............resultArray" + resultArray.toString());
                String strStatus = "";
                JSONObject result;
                JSONObject inputObj;
                if (resultArray.length() > 0) {
                    for (int i = 0; i < resultArray.length(); i++) {
                        result = (JSONObject) resultArray.get(i);
                        if(result.has("status")) {
                            strStatus = result.getString("status");
                            inputObj = rowArray.getJSONObject(i);
                            int rowid = Integer.parseInt(inputObj.getString("id"));
                            if (strStatus.equals("true")) {
                                db.execSQL("UPDATE clientvisit SET clientstatus = 'Success' WHERE id =" + rowid);
                            } else {
                                Log.e("Client Visit", ".................Moving Failed Record id::" + rowid);
                            }
                        }else {
                            inputObj = rowArray.getJSONObject(i);
                            int rowid = Integer.parseInt(inputObj.getString("id"));
                            Log.e("Client Visit","..........No status Found For Row Id" + rowid);
                        }
                    }
                    if(finalListener != null)
                    finalListener.onSuccess();///
                } else {
                    if(finalListener != null)
                    finalListener.onFailed("--No Result Received From Server--");
                    Log.e("Client Visit",".............NO result From Server");
                }
            } catch (Exception e) {
                e.printStackTrace();
                if(finalListener != null)
                finalListener.onFailed("Client Visit Moving Failed::" + e.getMessage());
            }
        }
    }
}
