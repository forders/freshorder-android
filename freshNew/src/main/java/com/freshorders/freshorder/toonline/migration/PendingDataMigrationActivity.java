package com.freshorders.freshorder.toonline.migration;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Environment;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.freshorders.freshorder.MyApplication;
import com.freshorders.freshorder.R;
import com.freshorders.freshorder.activity.ManagerActivity;
import com.freshorders.freshorder.crashreport.AppCrashConstants;
import com.freshorders.freshorder.crashreport.AppCrashUtil;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.service.ServiceClass;
import com.freshorders.freshorder.service.ServiceForegroundMigration;
import com.freshorders.freshorder.toonline.Migration;
import com.freshorders.freshorder.toonline.backupdb.SQLiteImporterExporter;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Log;
import com.freshorders.freshorder.utils.NetworkUtil;
import com.freshorders.freshorder.utils.SM_uploadToServerCheckout_NCR;
import com.freshorders.freshorder.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class PendingDataMigrationActivity extends AppCompatActivity {

    // Migration variable
    private SQLiteDatabase db;
    public static String imagepath1 = "", imagepath2 = "", imagepath3 = "", imagepath4 = "", imagepath5 = "", imagepath6 = "",
            imagepath7 = "", imagepath8 = "", imagepath9 = "", imagepath10 = "", imagepath11 = "", imagepath12 = "",
            merchantRowid = "";

    private static String Merchantname = "";
    private  static ArrayList<String> arraylistimagepath = new ArrayList<String>();
    public static String payment_type,orderdate,Orderno,flag,productStatus,mode,paymenttype,pushstauts,OfflineOrderNo,rowid,beatrowid, selected_dealer;
    public static int year,month,day,hr,mins,sec,deliverydt;
    public  static  String [] image;

    private static void reSetDefault(){
        imagepath1 = ""; imagepath2 = ""; imagepath3 = ""; imagepath4 = ""; imagepath5 = ""; imagepath6 = "";
        imagepath7 = ""; imagepath8 = ""; imagepath9 = ""; imagepath10 = ""; imagepath11 = ""; imagepath12 = "";
        merchantRowid = "";
    }
    private static String migPath = MyApplication.getInstance().getMigPath();
    private static int orderSuccessCount = 0;

    private static String orderMigration = "Order Migration";
    private static String closingStock = "Closing Stock Migration";
    private static String clientVisit = "Client Visit Migration";
    private static String newClient = "New Client Migration";
    private static String postNote = "Complaint Migration";
    private static String pkd = "PKD Migration";
    private static String survey = "Survey Migration";
    private static String distributorStock = "Distributor Stock Migration";


    private static String migAlreadyRunning = "MigrationAlreadyRunning";
    private static String TAG = MenuMigration.class.getSimpleName();

    private static String MIG_NOT_REQUIRED = "Migration already completed";
    private static String MIG_FINISHED_SUCCESS = "Migration is successful";
    private static String MIG_FAILED = "Migration Failed";


    //////////////////////////////////

    ///////////////////
    public List<MenuMigrationModel> migStatusList;
    ////////////

    private static int i = 0;
    Cursor testCur;
    Cursor curOrder;

    private androidx.appcompat.widget.Toolbar toolbar;
    private MenuMigration migration;
    private Migration migrationForeground;
    private FailedMenuMigration failedMigration;
    private Migration mig;
    private Context mContext;
    public Object migrationBlock;
    private ProgressBar progressBar;
    private TextView tvStatus;

    SQLiteImporterExporter sqLiteImporterExporter;
    SQLiteImporterExporter sqLiteImporterExporter1;
    SQLiteImporterExporter sqLiteImporterExporter2;
    public static String rootdb = "freshorders";
    public static String db1 = "freshorders-journal";

    String relativePath = AppCrashConstants.APP_ROOT_FOLDER + File.separator + Constants.BACKUP_FOLDER;
    String path = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + relativePath+ "/";

    private String importPath = "";
    public static final int FOLDER_CREATION_ERROR = 0;
    public static DatabaseHandler databaseHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_data_migration);
        mContext  = PendingDataMigrationActivity.this;
        toolbar =  findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        db = mContext.openOrCreateDatabase("freshorders", 0, null);
        databaseHandler = new DatabaseHandler(getApplicationContext());
        ActionBar actionBar = getSupportActionBar();
        // Set below attributes to add logo in ActionBar.
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setLogo(R.drawable.appicon_blue);
        actionBar.setTitle("Pending Data To Server");

        migPath = new PrefManager(PendingDataMigrationActivity.this).getStringDataByKey(PrefManager.KEY_BUG_FILE_PATH);///

        Log.setMigPath(MyApplication.getInstance().getMigPath());
        migStatusList = new ArrayList<>();

        String rootDir = Environment.getExternalStorageDirectory().getAbsolutePath();
        File backupDir = new File(rootDir, relativePath);
        if (!backupDir.exists()) {
            if (!backupDir.mkdirs()) {
                showAlertDialog("Error", "FolderCreation for Backup DB failed");
            }else {
                createImportFolder(backupDir);
            }
        }else {
            createImportFolder(backupDir);
        }


////////////////////
        Button btnManager = findViewById(R.id.btn_migration_manager);
        btnManager.setOnClickListener(managerListener);
        /////////

        Button btnPending = findViewById(R.id.btn_migration_pending_data);
        Button btnFailed = findViewById(R.id.btn_migration_Failed_data);
        btnPending.setOnClickListener(btnPendingMigration);
        btnFailed.setOnClickListener(btnFailedMigration);

        progressBar = (ProgressBar) findViewById(R.id.id_PB_menu_mig_progressbar);
        progressBar.setVisibility(View.INVISIBLE);
        tvStatus = findViewById(R.id.id_menu_mig_TV_status);
        tvStatus.setMovementMethod(new ScrollingMovementMethod()); //scrollable text view

        migrationBlock = new Object();
        migration = new MenuMigration(this.getApplicationContext(), migrationBlock);
        migrationForeground = new Migration(mContext.getApplicationContext());
        failedMigration = new FailedMenuMigration(this.getApplicationContext());
        mig = new Migration(getApplicationContext());

        Button btnBug = findViewById(R.id.btn_migration_bug_generate);
        btnBug.setOnClickListener(bug);

        Button btnBackupDB = findViewById(R.id.btn_migration_back_upDB);
        btnBackupDB.setOnClickListener(DBBackup);
        Button btnImportDB = findViewById(R.id.btn_migration_importDB);
        btnImportDB.setOnClickListener(importDB);

        /////////////

        //btnBackupDB.setVisibility(View.INVISIBLE);
        //btnImportDB.setVisibility(View.INVISIBLE);
        //btnPending.setVisibility(View.INVISIBLE);

        ////////////



        sqLiteImporterExporter = new SQLiteImporterExporter(getApplicationContext(), rootdb);
        sqLiteImporterExporter.setOnImportListener(new SQLiteImporterExporter.ImportListener() {
            @Override
            public void onSuccess(String message) {
                //Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                sqLiteImporterExporter1.importDataBase(importPath);

            }

            @Override
            public void onFailure(Exception exception) {
                android.util.Log.e("Error:","~~~~`"+exception);
                Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        sqLiteImporterExporter1 = new SQLiteImporterExporter(getApplicationContext(), db1);
        sqLiteImporterExporter1.setOnImportListener(new SQLiteImporterExporter.ImportListener() {
            @Override
            public void onSuccess(String message) {
                //Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                //sqLiteImporterExporter2.importDataBase(importPath);
                ////////sqLiteImporterExporter1.importDataBase(importPath);
            }

            @Override
            public void onFailure(Exception exception) {
                android.util.Log.e("Error:","~~~~`"+exception);
                Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        //////////////////////////

        sqLiteImporterExporter.setOnExportListener(new SQLiteImporterExporter.ExportListener() {
            @Override
            public void onSuccess(String message) {
                //Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                sqLiteImporterExporter1.exportDataBase(path);
            }

            @Override
            public void onFailure(Exception exception) {
                android.util.Log.e("Error:","~~~~`"+exception);
                Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        sqLiteImporterExporter1.setOnExportListener(new SQLiteImporterExporter.ExportListener() {
            @Override
            public void onSuccess(String message) {
                //Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                sqLiteImporterExporter2.exportDataBase(path);
            }

            @Override
            public void onFailure(Exception exception) {
                android.util.Log.e("Error:","~~~~`"+exception);
                Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void createImportFolder(File backupDir){
        String backupPath = backupDir.getAbsolutePath();
        File importDir = new File(backupPath, Constants.IMPORT_FOLDER);
        if (!importDir.exists()) {
            if (!importDir.mkdirs()) {
                showAlertDialog("Error", "FolderCreation for Import DB failed");
            }else {
                importPath = importDir.getAbsolutePath();
            }
        }else {
            importPath = importDir.getAbsolutePath();
        }
        importPath = importPath + "/";
    }

    private View.OnClickListener DBBackup = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            android.util.Log.i("DatabasePath","~~~~~~~~~~~~~~~~~~~"+db+":::::::"+sqLiteImporterExporter.getDatabaseName());
            if (sqLiteImporterExporter.isDataBaseExists()) {
                Toast.makeText(getApplicationContext(), "DB already exists", Toast.LENGTH_SHORT).show();
                initialExport();
            }else {
                android.util.Log.i("DatabasePath","~~~~~~~~~~~~~~~~~~~"+db+":::::::"+sqLiteImporterExporter.getDatabaseName());
                Toast.makeText(getApplicationContext(), "DB doesn't exist", Toast.LENGTH_SHORT).show();
            }
        }
    };

    private View.OnClickListener importDB = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            initialImport();
        }
    };

    private void initialImport() {

        if (checkForExternalDataBase()) {

            if(this.deleteDatabase(Constants.DATABASE)){
                Log.e("DB_Deleted",".........................");
            }else {
                Log.e("DB_Deleted",".......................failed." );
            }
            /*
            File directory = new File(this.getDatabasePath(Constants.DATABASE + ".db"), "");
            if (directory.exists()) {
                this.deleteDatabase(Constants.DATABASE);
                android.util.Log.i("Current Path:", "~~~~~~~~~~~~~~~~~~~~" + directory.getAbsolutePath());

                File dir = directory.getParentFile();
                for (File f : dir.listFiles()) {
                    if (f.getName().startsWith(Constants.DATABASE)) {
                        android.util.Log.i("Current Path:", "~~~~~~~~~~~~~~~~~~~~" + f.getAbsolutePath());
                        if (!f.delete()) {
                            dialog("Error", "Something Went Wrong In Import DB (delete folder):::" + f.getAbsolutePath(), 1);
                            return;
                        }
                    }
                }
            }  */

            sqLiteImporterExporter.importDataBase(importPath);
        } else {
            dialog("Alert", "Cant import from Memory Card , We need two files to import process", 2);
        }
    }

    private void initialExport(){
        try{
            File myDirectory = new File(path);
            if(!myDirectory.exists()) {
                if(myDirectory.mkdirs()){
                    sqLiteImporterExporter.exportDataBase(path);
                }else {
                    dialog("Error","Folder Creation Error Redo",FOLDER_CREATION_ERROR);
                }
            }else {

                File dir = myDirectory.getParentFile();
                for (File f : dir.listFiles()) {
                    if (f.getName().startsWith(Constants.DATABASE)) {
                        android.util.Log.i("Current Path:", "~~~~~~~~~~~~~~~~~~~~" + f.getAbsolutePath());
                        if (!f.delete()) {
                            dialog("Error", "Something Went Wrong In Import DB (delete folder):::" + f.getAbsolutePath(), 1);
                            return;
                        }
                    }
                }
                sqLiteImporterExporter.exportDataBase(path);//////
                /*if(deleteDir(myDirectory)){

                    if(myDirectory.mkdirs()){
                        sqLiteImporterExporter.exportDataBase(path);
                    }else {
                        dialog("Error","Folder Creation Error Redo",FOLDER_CREATION_ERROR);
                    }

                }else {
                    dialog("Error","Deleting Old Folder",FOLDER_CREATION_ERROR);
                }  */
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i=0; i<children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }


    private boolean checkForExternalDataBase(){

        File externalDB = new File(path + File.separator + Constants.IMPORT_FOLDER + File.separator + rootdb);
        //File externalDB1 = new File(path + File.separator + Constants.IMPORT_FOLDER + File.separator + db1);
        if(!externalDB.exists()){
            return false;
        }
        /*
        if(!externalDB1.exists()){
            return false;
        }  */
        return true;
    }

    private View.OnClickListener bug = new View.OnClickListener() {
        @Override
        public void onClick(View v) {


            //////////////////////////////////////////////////////////////////////////////////////// This for only test Purpose
            HashMap<String, String> oldNewMerchantPair = new HashMap<>();
            //Cursor testCur = db.rawQuery("SELECT * from orderheader where pushstatus IN ('No Order', 'Outlet', 'Pending')", null);/////databaseHandler.isOrderHeaderForNewUser();
            try {
                testCur = databaseHandler.getAllPendingOutlet();
                if (testCur != null && testCur.getCount() > 0) {
                    testCur.moveToFirst();
                    Log.e("MerchantChange", "...........i..." + testCur.getCount());
                    while (!testCur.isAfterLast()) {
                        try {

                            Log.e("MerchantChange", "...........i..." + i);
                            Date date = new Date();
                            //This method returns the time in millis
                            long timeMilli = date.getTime();

                            int oflnordid = testCur.getInt(0);
                            String mName = testCur.getString(3);
                            String offlineOrderNo = testCur.getString(8);

                            Thread.sleep(300);

                            String muserId = testCur.getString(2);

                            long startTime = System.currentTimeMillis();
                            mName = "Fresh-" + timeMilli + "-" + i++;

                            String newOfflineOrderNo = Constants.USER_ID + startTime + timeMilli;

                            String inputFormat1 = new SimpleDateFormat(
                                    "yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new java.util.Date());
                            if (!muserId.equals("New User")) {
                                String updateOrderHeaderQuery = "UPDATE orderheader SET orderdt = '" + inputFormat1 + "',oflnordno = '" + newOfflineOrderNo + "'  WHERE oflnordid = '" + oflnordid + "'";
                                db.execSQL(updateOrderHeaderQuery);
                            } else {
                                Log.e("NewMerchantName", "..........mName.." + mName);
                                String updateOrderHeaderQuery = "UPDATE orderheader SET mname = '" + mName + "',orderdt = '" + inputFormat1 + "',oflnordno = '" + newOfflineOrderNo + "'  WHERE oflnordid = '" + oflnordid + "'";
                                db.execSQL(updateOrderHeaderQuery);
                            }
                            ///thhis section is for merchant table
                            String getMerchant = "SELECT * from merchanttable where oflnordid = '" + offlineOrderNo + "'";
                            Cursor c = db.rawQuery(getMerchant, null);
                            if (c != null && c.getCount() > 0) {
                                c.moveToFirst();
                                beatrowid = "";
                                int rowid = c.getInt(0);
                                String oldMerchant = c.getString(1);
                                beatrowid = c.getString(19);
                                oldNewMerchantPair.put(oldMerchant, mName);

                                c.close();
                                String updateMerchantTableQuery = "UPDATE merchanttable SET companyname = '" + mName + "',fullname = '" + mName + "',mobileno = '" + startTime + "',oflnordid = '" + newOfflineOrderNo + "' WHERE rowid = '" + rowid + "'";
                                db.execSQL(updateMerchantTableQuery);

                                Cursor beatCursor = db.rawQuery("SELECT * from beat where oflnordid='" + offlineOrderNo +"'", null);
                                if(beatCursor != null && beatCursor.getCount() > 0){
                                    Log.e("BeatMatch","................beatrowid"+beatrowid);
                                    beatCursor.moveToFirst();
                                    int beatrowid = beatCursor.getInt(0);
                                    String strQuery = " UPDATE beat SET oflnordid='" + newOfflineOrderNo + "', createddt = '" + inputFormat1 + "' WHERE rowid = '" + beatrowid + "'";
                                    db.execSQL(strQuery);
                                    beatCursor.close();
                                }
                            }
                            testCur.moveToNext();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }

                    curOrder = databaseHandler.getAllPendingToChange();

                    if (curOrder != null && curOrder.getCount() > 0) {
                        curOrder.moveToFirst();
                        Log.e("OrderChange", "...........i..." + curOrder.getCount());
                        while (!curOrder.isAfterLast()) {
                            try {

                                Log.e("OrderChange", "...........i..." + i++);
                                Date date = new Date();
                                //This method returns the time in millis
                                long timeMilli = date.getTime();

                                int oflnordid = curOrder.getInt(0);
                                String mName = curOrder.getString(3);
                                String offlineOrderNo = curOrder.getString(8);

                                Thread.sleep(300);

                                String muserId = curOrder.getString(2);

                                long startTime = System.currentTimeMillis();

                                String newOfflineOrderNo = Constants.USER_ID + startTime + timeMilli;

                                String inputFormat1 = new SimpleDateFormat(
                                        "yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new java.util.Date());
                                if (!muserId.equals("New User")) {
                                    String updateOrderHeaderQuery = "UPDATE orderheader SET orderdt = '" + inputFormat1 + "',oflnordno = '" + newOfflineOrderNo + "'  WHERE oflnordid = '" + oflnordid + "'";
                                    db.execSQL(updateOrderHeaderQuery);
                                } else {
                                    String newMerchant = oldNewMerchantPair.get(mName);
                                    Log.e("OrderChange", "...........i..oldName." + mName + "....."  + newMerchant);
                                    String updateOrderHeaderQuery = "UPDATE orderheader SET mname = '" + newMerchant + "',orderdt = '" + inputFormat1 + "',oflnordno = '" + newOfflineOrderNo + "'  WHERE oflnordid = '" + oflnordid + "'";
                                    db.execSQL(updateOrderHeaderQuery);
                                }

                                curOrder.moveToNext();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    Toast.makeText(getApplicationContext(), "Data modification completed", Toast.LENGTH_LONG).show();
            }catch (Exception e){
                e.printStackTrace();
            }finally {
                if(testCur != null)
                testCur.close();
                if(curOrder != null){
                    curOrder.close();
                }
            }



            ////////////////////////////////////////////////////////////////////////////////////////



            ///throw new NullPointerException();//////////
            /*
            ArrayList<String> list = new ArrayList();
            list.add("hello");
            String crashMe = list.get(2);  */
        }
    };



    private void doMenuMigration(){
        showProgress();
        tvStatus.setText("");// need to clear for every migration
        //String status = migration.doSequenceMigration();
        new Migration(mContext).doAllMigration();
        //tvStatus.setText(status);
        migration.startReset();///
        hideProgress();
    }

    private void doFailedMenuMigration(){

        tvStatus.setText("");// need to clear for every migration
        //String status = failedMigration.doFailedSequenceMigration();
        //tvStatus.setText(status);
        failedMigration.startReset();///
        //hideProgress();

        JsonServiceHandler FailedOrderServiceHandler = new JsonServiceHandler(Utils.strsavemerchantOderdetail, mContext);
        new FailedMigrationAsyncTask(new PendingDataMigrationActivity(), db, FailedOrderServiceHandler).execute();
    }

    public static class FailedMigrationAsyncTask extends AsyncTask<Void, MenuMigrationModel, String> {

        private JsonServiceHandler FailedOrderServiceHandler;
        private PendingDataMigrationActivity classObj;
        private SQLiteDatabase db;

        public FailedMigrationAsyncTask(PendingDataMigrationActivity classObj,
                                        SQLiteDatabase db,
                                        JsonServiceHandler FailedOrderServiceHandler) {
            this.FailedOrderServiceHandler = FailedOrderServiceHandler;
            this.classObj = classObj;
            this.db = db;
        }

        @Override
        protected void onPreExecute() {
            //classObj.showProgress();
        }

        @Override
        protected String doInBackground(Void... voids) {
            migration(db,FailedOrderServiceHandler);
            return Constants.EMPTY;
        }

        @Override
        protected void onProgressUpdate(MenuMigrationModel... migProgressModel) {
            /////////MenuMigrationModel
            super.onProgressUpdate(migProgressModel);
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Data Moving Operation Name : ");
            ///////stringBuilder.append(migProgressModel.());
        }

        @Override
        protected void onPostExecute(String result) {
            //classObj.hideProgress();
            Log.e("BugReport","Done by .....................................");
        }

    }


    // Create an anonymous implementation of OnClickListener
    private View.OnClickListener btnPendingMigration = new View.OnClickListener() {
        public void onClick(View v) {
            if(migration.isNeedForegroundMigration()){
                if(!mig.isMigrationServiceWorking() && (!ServiceClass.isServiceRunning(ServiceForegroundMigration.class, mContext.getApplicationContext()))) {
                    if(NetworkUtil.getConnectivityStatus(mContext) == 1 ||
                            NetworkUtil.getConnectivityStatus(mContext) == 2) {
                        boolean isFullMig = new PrefManager(mContext).getBooleanDataByKey(PrefManager.KEY_IS_FULL_MIGRATION_SET);
                        if(!isFullMig) {
                            showAlertForMigration(getResources().getString(R.string.mig_alert));
                        }else {
                            showAlertDialogToast(getResources().getString(R.string.mig_currently_working));
                            Log.e(TAG, "Migration Already Working");
                        }
                    }else {
                        showAlertDialogToast("Please enable Mobile Data or Wi-Fi");
                    }
                }else {
                    showAlertDialogToast("Data move cannot start until the previous operation is completed.");
                }
            }else {
                showAlertDialogToast("No pending data available for migration");
            }
        }
    };

    private View.OnClickListener btnFailedMigration = new View.OnClickListener() {
        public void onClick(View v) {
            if(failedMigration.isNeedFailedMigration()){
                if(!mig.isMigrationServiceWorking()) {
                    if(NetworkUtil.getConnectivityStatus(mContext) == 1 ||
                            NetworkUtil.getConnectivityStatus(mContext) == 2) {
                        if(!ServiceClass.isServiceRunning(ServiceForegroundMigration.class, mContext)) {
                            ////////////
                            boolean isFullMig = new PrefManager(mContext).getBooleanDataByKey(PrefManager.KEY_IS_FULL_MIGRATION_SET);
                            if(!isFullMig) {
                                showAlertForFailedMigration(getResources().getString(R.string.mig_alert));
                            }else {
                                Log.e(TAG, "Migration Already Working");
                                showAlertDialogToast(getResources().getString(R.string.mig_currently_working));
                            }
                        }else {
                            showAlertDialogToast("Already Data Moving Work Can't Start Migration Until Previous Operation Complete");
                        }
                    }else {
                        showAlertDialogToast("Before This Enable Mobile Data");
                    }
                }else {
                    showAlertDialogToast("Already Data Moving Work Can't Start Migration Until Previous Operation Complete");
                }
            }else {
                showAlertDialogToast("No failed data available for migration");
            }
        }
    };

    public void showAlertForFailedMigration(String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        doFailedMenuMigration();//////
                    }
                });
        builder1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }


    public void showAlertForMigration(String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        migrationForeground.startMigrationByAlarmManager(mContext.getApplicationContext());
                    }
                });
        builder1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    public void showAlertDialogToast(String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    @Override
    public void onBackPressed() {
        Log.e("onBackPressed","...................................");
        super.onBackPressed();
        finish();
    }
    /*
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent objEvent) {
        Log.e("onKeyUp","..................................."+keyCode);
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Log.e("onKeyUp","...................................");
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, objEvent);
    }  */
    //I use toolbar to actionbar so this function only trigger
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void hideProgress(){
        progressBar.setVisibility(View.INVISIBLE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    private void showProgress(){
        progressBar.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }


    private void dialog(String title,String message, int id){
        final AlertDialog.Builder builder1 = new AlertDialog.Builder(mContext);
        builder1.setTitle(title);
        builder1.setMessage(message);
        builder1.setCancelable(false);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private void showAlertDialog(String title, String message){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle(title);
        alertDialog.setCancelable(false);
        // Setting Dialog Message
        alertDialog.setMessage(message);
        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.delete);

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,int which) {
                //finishAffinity();/////////////////////////////
                // Write your code here to invoke YES event
                //Toast.makeText(mContext, "You clicked on YES", Toast.LENGTH_SHORT).show();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    public synchronized static MenuMigrationModel migration(SQLiteDatabase db,JsonServiceHandler FailedOrderServiceHandler) {
        MenuMigrationModel orderMig = new MenuMigrationModel();
        try {
            orderMig.setMigrationName(orderMigration);
            Cursor curs;
            curs = db.rawQuery("SELECT * from orderheader where pushstatus ='Failed'", null);

            android.util.Log.e("HeaderCount", String.valueOf(curs.getCount()));
            final JSONArray resultSet = new JSONArray();

            if (curs.getCount() > 0) {
                orderMig.setMigrationCount(curs.getCount());
                //new PrefManager(mContext.getApplicationContext()).setBooleanDataByKey(PrefManager.KEY_IS_FAILED_ORDER_MIGRATION_START, true);

                curs.moveToFirst();
                while (!curs.isAfterLast()) {

                    int totalColumn = curs.getColumnCount();
                    JSONObject rowObject = new JSONObject();

                    for (int i = 0; i < totalColumn; i++) {

                        if (curs.getColumnName(i) != null) {

                            try {

                                if (curs.getString(i) != null) {
                                    if (!curs.getString(i).equals("New User")) {
                                        android.util.Log.d("TAG_NAME", curs.getString(i));
                                        rowObject.put(curs.getColumnName(i), curs.getString(i));
                                    }
                                  /*  Log.d("TAG_NAME", curs.getString(i));
                                    rowObject.put(curs.getColumnName(i), curs.getString(i));*/
                                } else {

                                    if (!curs.getColumnName(i).equals("muserid")) {
                                        rowObject.put(curs.getColumnName(i), "");
                                    }

                                }
                            } catch (Exception e) {
                                //e.printStackTrace();
                                orderMig.setExceptionReason(e.getMessage());
                                continue;
                            }
                        }
                    }

                    try {
                        rowObject.put("distributorname",Constants.distributorname);
                    } catch (Exception e) {
                        android.util.Log.d("Exception in Dist. name", e.getMessage());
                    }

                    try {

                        Cursor curs1;
                        curs1 = db.rawQuery("SELECT userid from dealertable", null);
                        android.util.Log.e("dEALER", String.valueOf(curs1.getCount()));
                        if (curs1.getCount() > 0) {
                            curs1.moveToFirst();
                            selected_dealer = curs1.getString(0);
                        }


                        Cursor cursor2;
                        Merchantname = rowObject.getString("mname");

                        cursor2 = db.rawQuery("SELECT * from merchanttable where companyname =" + "'" + Merchantname + "'", null);
                        android.util.Log.e("HeaderCountCanled", String.valueOf(cursor2.getCount()));

                        cursor2.moveToFirst();
                        while (!cursor2.isAfterLast()) {

                            int Columncount = cursor2.getColumnCount();
                            android.util.Log.e("merchantColumn", String.valueOf(cursor2.getColumnCount()));
                            JSONObject merchantdetail = new JSONObject();
                            for (int j = 0; j < Columncount; j++) {

                                try {
                                    if (cursor2.getColumnName(j) != null) {

                                        if (cursor2.getColumnName(j).equals("cflag")) {
                                            if (cursor2.getString(j).equals("R")) {
                                                merchantdetail.put(cursor2.getColumnName(j), "G");
                                            } else if (cursor2.getString(j).equals("G")) {
                                                merchantdetail.put(cursor2.getColumnName(j), "");
                                            } else {
                                                merchantdetail.put(cursor2.getColumnName(j), "");
                                            }

                                        } else {
                                            if (cursor2.getString(j) != null) {
                                                if (!cursor2.getString(j).equals("New User")) {

                                                    merchantdetail.put(cursor2.getColumnName(j), cursor2.getString(j));

                                                }
                                                //merchantdetail.put(cursor2.getColumnName(j) ,  cursor2.getString(j) );
                                            } else {
                                                if (!cursor2.getColumnName(j).equals("userid")) {
                                                    merchantdetail.put(cursor2.getColumnName(j), "");
                                                }

                                            }
                                        }
                                    }
                                } catch(Exception e){
                                    android.util.Log.d("Merchant_Exception", e.getMessage());
                                    orderMig.setExceptionReason(e.getMessage());
                                    //e.printStackTrace();
                                    continue;
                                }
                            }

                            merchantdetail.put("usertype", "M");
                            merchantdetail.put("duserid", selected_dealer);
                            rowObject.put("merchant", merchantdetail);
                            cursor2.moveToNext();
                        }
                        cursor2.close();
                        android.util.Log.e("Detailarray", rowObject.toString());

                    } catch (Exception e) {
                        orderMig.setExceptionReason(e.getMessage());
                        AppCrashUtil.writeStringToBugFile(migPath,AppCrashUtil.getStackTrace(e));
                    }

                    android.util.Log.e("jsonarray", rowObject.toString());

                    JSONArray resultDetail = new JSONArray();
                    try {

                        Cursor cursor;
                        cursor = db.rawQuery("SELECT * from oredrdetail where oredrdetail.oflnordid=" + rowObject.getString("oflnordid"), null);
                        android.util.Log.e("DetailCount", String.valueOf(cursor.getCount()));
                        cursor.moveToFirst();
                        while (!cursor.isAfterLast()) {

                            int Columncount = cursor.getColumnCount();
                            JSONObject detailObject = new JSONObject();

                            for (int i = 0; i < Columncount; i++) {
                                if (cursor.getColumnName(i) != null) {

                                    try {

                                        if (cursor.getString(i) != null) {
                                            android.util.Log.d("TAG_NAME", cursor.getString(i));
                                            detailObject.put(cursor.getColumnName(i), cursor.getString(i));
                                        } else {
                                            detailObject.put(cursor.getColumnName(i), "");

                                        }
                                    } catch (Exception e) {
                                        android.util.Log.d("TAG_NAME", e.getMessage());
                                        AppCrashUtil.writeStringToBugFile(migPath,AppCrashUtil.getStackTrace(e));
                                    }
                                }

                            }
                            resultDetail.put(detailObject);
                            cursor.moveToNext();
                        }
                        android.util.Log.e("arraylist", String.valueOf(arraylistimagepath.size()));
                        cursor.close();
                        android.util.Log.e("Detailarray", resultDetail.toString());

                    } catch (Exception e) {
                        AppCrashUtil.writeStringToBugFile(migPath,AppCrashUtil.getStackTrace(e));
                        orderMig.setExceptionReason(e.getMessage());
                    }
                    try {
                        rowObject.put("orderdtls", resultDetail);
                    } catch (Exception e) {
                        AppCrashUtil.writeStringToBugFile(migPath,AppCrashUtil.getStackTrace(e));
                        orderMig.setExceptionReason(e.getMessage());
                    }
                    resultSet.put(rowObject);
                    curs.moveToNext();
                }
                curs.close();
                Log.e("SaveArray111", resultSet.toString());
                //JsonServiceHandler = new JsonServiceHandler(Utils.strsavemerchantOderdetail, mContext);
                //new FailedOrderMigrateToOnline.MigrationAsyncTask(db, listener, databaseHandler, JsonServiceHandler, resultSet).execute();
                return MigrationProcess(resultSet, db, FailedOrderServiceHandler, orderMig);
            } else {
                Log.e("else_ordercancel", "ordercancel");
                orderMig.setStatus(MIG_NOT_REQUIRED);
                ///////////ordercancel();
            }
        }catch (Exception e){
            AppCrashUtil.writeStringToBugFile(migPath,AppCrashUtil.getStackTrace(e));
            orderMig.setExceptionReason(e.getMessage());
        }
        return orderMig;
    }

    private static MenuMigrationModel MigrationProcess(JSONArray resultSet, SQLiteDatabase db,
                                           JsonServiceHandler FailedOrderServiceHandler, MenuMigrationModel orderMig){
        JSONObject jsonObject = new JSONObject();
        String strStatus = "";
        String strMsg = "", strOrderId = "";
        try {

            for (int k = 0; k < resultSet.length(); k++) {
                jsonObject = resultSet.getJSONObject(k);
                android.util.Log.e("jsonObjectOffline", String.valueOf(jsonObject));
                OfflineOrderNo = jsonObject.getString("oflnordid");
                System.out.println("BeforeChange1: " + jsonObject);
                JSONObject jsonObjMerchant = jsonObject.getJSONObject("merchant");
                System.out.println("BeforeChangejsonObjMerchant1: " + jsonObjMerchant);
                jsonObjMerchant.put("createdby", Constants.USER_ID);
                jsonObjMerchant.put("customdata", "");
                System.out.println("AfterChangejsonObjMerchant1: " + jsonObjMerchant);
                jsonObject.put("merchant", jsonObjMerchant);
                System.out.println("AfterChange1: " + jsonObject);
                System.out.println("SVTestinside101");
                FailedOrderServiceHandler.setParams(jsonObject.toString()); /////////////
                JSONObject JsonAccountObject = FailedOrderServiceHandler.ServiceData();

                try {
                    reSetDefault();/////
                    strOrderId = ""; strStatus = ""; strMsg = ""; //////////////////////recent change Kumaravel
                    strStatus = JsonAccountObject.getString("status");
                    if(strStatus != null && !strStatus.isEmpty()) {
                        Log.e("return status", strStatus);
                        strMsg = JsonAccountObject.getString("message");
                        Log.e("return message", strMsg);

                        if (strStatus.equals("true")) {
                            orderSuccessCount++;

                            strOrderId = JsonAccountObject.getJSONObject("data").getString("ordid");
                            Log.e("strOrderId", strOrderId);
                            String muserid = JsonAccountObject.getJSONObject("data").getString("muserid");

                            String updatedFlag = jsonObject.getJSONObject("merchant").getString("cflag");
                            android.util.Log.e("updatedFlag", updatedFlag);

                            String companyName = jsonObject.getString("mname");

                            db.execSQL("UPDATE merchanttable SET userid = " + muserid + ",cflag =" + "'" + updatedFlag + "'" + " WHERE companyname =" + "'" + companyName + "'");


                            Cursor curs3;
                            curs3 = db.rawQuery("SELECT rowid from merchanttable where userid=" + muserid, null);
                            android.util.Log.e("merchantrowid", String.valueOf(curs3.getCount()));
                            String merchantRowid = "";
                            if (curs3 != null && curs3.getCount() > 0) {

                                if (curs3.moveToFirst()) {
                                    android.util.Log.e("rowid ", curs3.getString(0));
                                    merchantRowid = curs3.getString(0);

                                }
                            }
                            android.util.Log.e("rowidmuserid ", muserid);
                            db.execSQL("UPDATE beat SET muserid = " + muserid + " ,flag =" + "'InActive'" + " WHERE mid =" + "'" + merchantRowid + "'");


                            Cursor curs4;
                            curs4 = db.rawQuery("SELECT muserid from beat where muserid=" + muserid, null);
                            android.util.Log.e("mid", String.valueOf(curs4.getCount()));
                            if (curs3 != null && curs3.getCount() > 0) {

                                if (curs3.moveToFirst()) {
                                    android.util.Log.e("mid ", curs3.getString(0));

                                }
                            }

                            android.util.Log.e("arraysize", String.valueOf(arraylistimagepath.size()));

                            String rowid = jsonObject.getJSONArray("orderdtls").getJSONObject(0).getString("rowid");
                            android.util.Log.e("rowid", rowid);

                            Cursor curs;
                            curs = db.rawQuery("SELECT ordimage from oredrdetail where oredrdetail.rowid=" + rowid, null);

                            android.util.Log.e("countorderimage", String.valueOf(curs.getCount()));
                            android.util.Log.e("columncount", String.valueOf(curs.getColumnCount()));


                            curs.moveToFirst();
                            while (curs.isAfterLast() == false) {

                                int totalColumn = curs.getColumnCount();

                                for (int i = 0; i < totalColumn; i++) {
                                    if (curs.getColumnName(i) != null) {
                                        android.util.Log.e("value", String.valueOf(curs.getColumnName(i)));
                                        try {

                                            if (curs.getString(i) != null) {
                                                image = curs.getString(i).split(",");

                                                android.util.Log.e("value", String.valueOf(curs.getString(i)));
                                            } else {
                                                arraylistimagepath.add("0");
                                            }
                                        } catch (Exception e) {
                                            Log.e("TAG_NAME", e.getMessage());
                                            AppCrashUtil.writeStringToBugFile(migPath,AppCrashUtil.getStackTrace(e));
                                        }
                                    }

                                }

                                curs.moveToNext();
                            }
                            curs.close();


                            for (int i = 0; i < image.length; i++) {
                                arraylistimagepath.add(image[i]);
                            }

                            android.util.Log.e("arraysize", String.valueOf(arraylistimagepath.size()));
                            for (int m = 0; m < arraylistimagepath.size(); m++) {
                                Log.e("inside", "inside");
                                Log.e("inside", arraylistimagepath.get(0).toString());

                                if (m == 0) {
                                    imagepath1 = arraylistimagepath.get(0).toString();
                                } else if (m == 1) {
                                    imagepath2 = arraylistimagepath.get(1).toString();
                                } else if (m == 2) {
                                    imagepath3 = arraylistimagepath.get(2).toString();
                                } else if (m == 3) {
                                    imagepath4 = arraylistimagepath.get(3).toString();
                                } else if (m == 4) {
                                    imagepath5 = arraylistimagepath.get(4).toString();
                                } else if (m == 5) {
                                    imagepath6 = arraylistimagepath.get(5).toString();
                                } else if (m == 6) {
                                    imagepath7 = arraylistimagepath.get(6).toString();
                                } else if (m == 7) {
                                    imagepath8 = arraylistimagepath.get(7).toString();
                                } else if (m == 8) {
                                    imagepath9 = arraylistimagepath.get(8).toString();
                                } else if (m == 9) {
                                    imagepath10 = arraylistimagepath.get(9).toString();
                                } else if (m == 10) {
                                    imagepath11 = arraylistimagepath.get(10).toString();
                                } else if (m == 11) {
                                    imagepath12 = arraylistimagepath.get(11).toString();
                                }
                                Log.e("path", String.valueOf(arraylistimagepath.get(m)));

                            }


                            db.execSQL("UPDATE orderheader SET pushstatus = 'Success',onlineorderno =" + strOrderId + " WHERE oflnordid =" + OfflineOrderNo);
                            Cursor cur;
                            cur = db.rawQuery("SELECT pushstatus from orderheader where pushstatus ='Success'", null);
                            android.util.Log.e("HeaderStatusCount", String.valueOf(cur.getCount()));

                            SM_uploadToServerCheckout_NCR ufs = new SM_uploadToServerCheckout_NCR(
                                    imagepath1, imagepath2, imagepath3, imagepath4,
                                    imagepath5, imagepath6, imagepath7, imagepath8,
                                    imagepath9, imagepath10, imagepath11, imagepath12, strOrderId);
                            ufs.execute();///////////////////////////////////////////////remove lint

                            Log.e("length", String.valueOf(resultSet.length() - 1));

                            if (k == (resultSet.length() - 1)) {

                                //////////////ordercancel();
                                android.util.Log.e("ordercancel_IF", "ordercancel");
                            }

                        } else {
                            if (strMsg != null && !strMsg.isEmpty() && strMsg.equals(Constants.ORDER_ALREADY_EXIST)) {
                                db.execSQL("UPDATE orderheader SET pushstatus = 'Failed' WHERE oflnordid =" + OfflineOrderNo); // Kumaravel
                            } else if (strMsg != null && !strMsg.isEmpty() && strMsg.equals("Network unavailable, Please try again later")) {
                                db.execSQL("UPDATE orderheader SET pushstatus = 'Pending' WHERE oflnordid =" + OfflineOrderNo); // Kumaravel
                            } else if (strMsg != null && !strMsg.isEmpty() && strMsg.equals(Constants.ORDER_SAVE_FAILED)) {

                                db.execSQL("UPDATE orderheader SET pushstatus = 'Failed' WHERE oflnordid =" + OfflineOrderNo); // Kumaravel

                            } else {
                                //This may be need to delete order
                                db.execSQL("UPDATE orderheader SET pushstatus = 'Failed' WHERE oflnordid =" + OfflineOrderNo); // Kumaravel
                            }
                        }
                    }else {
                        db.execSQL("UPDATE orderheader SET pushstatus = 'Failed' WHERE oflnordid =" + OfflineOrderNo); // Kumaravel
                        Log.e("return message", "No Return Data From Server");
                    }


                } catch (JSONException e) {
                    AppCrashUtil.writeStringToBugFile(migPath,AppCrashUtil.getStackTrace(e));
                } catch (Exception e) {
                    AppCrashUtil.writeStringToBugFile(migPath,AppCrashUtil.getStackTrace(e));
                }
            }

        } catch (JSONException e) {
            AppCrashUtil.writeStringToBugFile(migPath,AppCrashUtil.getStackTrace(e));
        }
        orderMig.setStatus(MIG_FINISHED_SUCCESS);
        return orderMig;
    }

    View.OnClickListener managerListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent to = new Intent(PendingDataMigrationActivity.this,
                    ManagerActivity.class);
            startActivity(to);
            finish();
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        try {
            //Kumaravel 13-07-2019
            if(Constants.distributorname == null || Constants.distributorname.isEmpty()){
                Constants.distributorname = new PrefManager(PendingDataMigrationActivity.this).getStringDataByKey(PrefManager.KEY_DISTRIBUTOR_NAME);
            }
        } catch (Exception e) {
            android.util.Log.d("Exception in Dist. name", e.getMessage());
        }
    }
}
