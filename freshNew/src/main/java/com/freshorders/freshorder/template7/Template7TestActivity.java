package com.freshorders.freshorder.template7;

import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.template7.adapter.AdapterTestCompany;
import com.freshorders.freshorder.template7.fragment.FragmentPendingTest;

public class Template7TestActivity extends AppCompatActivity {

    private AdapterTestCompany adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Toolbar toolbar;

    private int[] tabIcons = {
            R.drawable.arrow_up,
            R.drawable.ic_nav_back,
            R.drawable.button_arrow,
            R.drawable.ic_nav_back
    };

    private String[] tabName = {
            "Pending Test",
            "Completed Test",
            "Test Result",
            "Edit Test Result"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_template7_test);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        // Set below attributes to add logo in ActionBar.
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setLogo(R.drawable.appicon_blue);
        actionBar.setTitle("My Test");

        viewPager = (ViewPager) findViewById(R.id.id_testCompany_viewPager);
        tabLayout = (TabLayout) findViewById(R.id.id_testCompany_tab);

        adapter = new AdapterTestCompany(getSupportFragmentManager(), this);
        adapter.addFragment(new FragmentPendingTest(), tabName[0], tabIcons[0]);
        adapter.addFragment(new FragmentPendingTest(), tabName[1], tabIcons[1]);
        adapter.addFragment(new FragmentPendingTest(), tabName[2], tabIcons[2]);
        adapter.addFragment(new FragmentPendingTest(), tabName[3], tabIcons[3]);

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        highLightCurrentTab(0);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position) {
                highLightCurrentTab(position);
            }
            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    private void highLightCurrentTab(int position) {
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            assert tab != null;
            tab.setCustomView(null);
            tab.setCustomView(adapter.getTabView(i));
        }
        TabLayout.Tab tab = tabLayout.getTabAt(position);
        assert tab != null;
        tab.setCustomView(null);
        tab.setCustomView(adapter.getSelectedTabView(position));
    }
}

