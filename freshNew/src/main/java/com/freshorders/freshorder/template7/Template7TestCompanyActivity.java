package com.freshorders.freshorder.template7;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;


import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.Template7TestAdapter;
import com.freshorders.freshorder.model.Template7Model;

import java.util.ArrayList;
import java.util.List;

public class Template7TestCompanyActivity extends AppCompatActivity {


    private List<Template7Model> test0 = new ArrayList<>();
    private List<Template7Model> test1 = new ArrayList<>();
    private List<Template7Model> test2 = new ArrayList<>();
    private List<Template7Model> test3 = new ArrayList<>();
    private List<Template7Model> test4 = new ArrayList<>();

    private void loadData(){

        denimArray = getResources().getStringArray(R.array.denim_test_data);

        for(int i = 0; i < denimArray.length; i++) {
            Template7Model item = new Template7Model();
            String strData = denimArray[i];
            item.setTestName(strData);
            test0.add(item);
        }

        for(int i = 0; i < 15; i++) {
            Template7Model item = new Template7Model();
            String strData = "Cotton Sample" + i;
            item.setTestName(strData);
            test1.add(item);
        }

        for(int i = 0; i < 15; i++) {
            Template7Model item = new Template7Model();
            String strData = "Jercy Sample" + i;
            item.setTestName(strData);
            test2.add(item);
        }

        for(int i = 0; i < 15; i++) {
            Template7Model item = new Template7Model();
            String strData = "Polyester Sample" + i;
            item.setTestName(strData);
            test3.add(item);
        }

        for(int i = 0; i < 15; i++) {
            Template7Model item = new Template7Model();
            String strData = "Outerwear Sample" + i;
            item.setTestName(strData);
            test4.add(item);
        }


    }

    private Context mContext;
    public RecyclerView recyclerView;
    private List<Template7Model> testDataList = new ArrayList<>();
    private Template7TestAdapter adapter;
    private Spinner spinTestName;

    private String[] denimArray;

    private String selectedTest = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_template7_test_company);
        mContext = this;

        for(int i = 0; i < 50; i++){
            Template7Model item = new Template7Model();
            String strData = "Data" + i;
            item.setTestName(strData);
            testDataList.add(item);
        }

        loadData();

        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        // toolbar fancy stuff
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Get ActionBar
        ActionBar actionBar = getSupportActionBar();
        // Set below attributes to add logo in ActionBar.
        assert actionBar != null;
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setLogo(R.drawable.appicon_blue);
        actionBar.setDisplayShowTitleEnabled(false);

        recyclerView = (RecyclerView) findViewById(R.id.id_RV_Template7);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));

        adapter = new Template7TestAdapter(testDataList, mContext);
        recyclerView.setAdapter(adapter);

        handleIntent(getIntent());

        spinTestName = (Spinner) findViewById(R.id.spinner);
        spinTestName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedTest = (String) adapterView.getSelectedItem();
                int pos = adapterView.getSelectedItemPosition();
                changeTest(pos);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        Button save = (Button) findViewById(R.id.id_ok);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAlertDialogToast1("Test Result Successfully Saved");
            }
        });

    }

    private void changeTest(int pos){
        switch (pos){

            case 0:
                testDataList.clear();
                adapter.notifyDataSetChanged();
                testDataList.addAll(test0);
                adapter.notifyDataSetChanged();
                break;

            case 1:
                testDataList.clear();
                adapter.notifyDataSetChanged();
                testDataList.addAll(test1);
                adapter.notifyDataSetChanged();
                break;

            case 2:
                testDataList.clear();
                adapter.notifyDataSetChanged();
                testDataList.addAll(test2);
                adapter.notifyDataSetChanged();
                break;

            case 3:
                testDataList.clear();
                adapter.notifyDataSetChanged();
                testDataList.addAll(test3);
                adapter.notifyDataSetChanged();
                break;

            case 4:
                testDataList.clear();
                adapter.notifyDataSetChanged();
                testDataList.addAll(test4);
                adapter.notifyDataSetChanged();
                break;
        }

        adapter.valueListReset();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_template7, menu);
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            //use the query to search
        }
    }


    public void showAlertDialogToast1( String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setCancelable(false);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();
        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));


    }
}
