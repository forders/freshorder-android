package com.freshorders.freshorder.template7.fragment;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.Template7TestAdapter;
import com.freshorders.freshorder.model.Template7Model;

import java.util.ArrayList;
import java.util.List;

public class FragmentPendingTest extends Fragment {

    private Context mContext;
    public RecyclerView recyclerView;
    private List<Template7Model> testDataList = new ArrayList<>();
    private Template7TestAdapter adapter;
    private Spinner spinTestName;
    private String selectedTest = "";

    private LayoutInflater mInflater;
    View view;

    TableLayout tbl;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_pending_test, container, false);

        mInflater = inflater;
        mContext = inflater.getContext();

        spinTestName = (Spinner) view.findViewById(R.id.spinner);
        spinTestName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedTest = (String) adapterView.getSelectedItem();
                int pos = adapterView.getSelectedItemPosition();
                changeTest(selectedTest);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        tbl = (TableLayout) view.findViewById(R.id.id_test_TBL_current_test);

        addHeaders();

        return view;
    }

    private void changeTest(String testName) {
        switch (testName) {

        }
    }


    private void addHeaders() {
        TableRow.LayoutParams tableRowParams =
                new TableRow.LayoutParams (TableRow.LayoutParams.MATCH_PARENT,
                        TableRow.LayoutParams.WRAP_CONTENT,10f);

        TableRow tr = new TableRow(mContext);

        tr.setLayoutParams(tableRowParams);

        /* Creating a TextView to add to the row **/
        TextView tvItemSNOTH = new TextView(mContext);
        tvItemSNOTH.setGravity(Gravity.CENTER);///
        tvItemSNOTH.setText(mContext.getResources().getString(R.string.str_s_no));
        tvItemSNOTH.setTextColor(Color.BLACK);
        tvItemSNOTH.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        tvItemSNOTH.setPadding(5, 5, 5, 5);
        tvItemSNOTH.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                mContext.getResources().getDimension(R.dimen.test_tbl_header));
        tr.addView(tvItemSNOTH, new TableRow.LayoutParams
                (0,TableRow.LayoutParams.WRAP_CONTENT,1.0f));


        /* Creating a TextView to add to the row **/
        TextView tvItemNameTH = new TextView(mContext);
        tvItemNameTH.setGravity(Gravity.START);///
        tvItemNameTH.setText(mContext.getResources().getString(R.string.client));
        tvItemNameTH.setTextColor(Color.BLACK);
        tvItemNameTH.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        tvItemNameTH.setPadding(5, 5, 5, 5);
        tvItemNameTH.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                mContext.getResources().getDimension(R.dimen.test_tbl_header));
        tr.addView(tvItemNameTH, new TableRow.LayoutParams
                (0,TableRow.LayoutParams.WRAP_CONTENT,2.0f));

        /* Creating a TextView to add to the row **/
        TextView tvQuantity = new TextView(mContext);
        tvQuantity.setGravity(Gravity.CENTER);
        tvQuantity.setText(mContext.getResources().getString(R.string.sample));
        tvQuantity.setTextColor(Color.BLACK);
        tvQuantity.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        tvQuantity.setPadding(5, 5, 5, 5);
        tvQuantity.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                mContext.getResources().getDimension(R.dimen.test_tbl_header));
        tr.addView(tvQuantity, new TableRow.LayoutParams
                (0,TableRow.LayoutParams.WRAP_CONTENT,4f));

        /* Creating a TextView to add to the row **/
        TextView tvUnitPrice = new TextView(mContext);
        tvUnitPrice.setText(mContext.getResources().getString(R.string.type));
        tvUnitPrice.setGravity(Gravity.CENTER);
        tvUnitPrice.setTextColor(Color.RED);
        tvUnitPrice.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        tvUnitPrice.setPadding(5, 5, 5, 5);
        tvUnitPrice.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                mContext.getResources().getDimension(R.dimen.test_tbl_header));
        tr.addView(tvUnitPrice, new TableRow.LayoutParams
                (0,TableRow.LayoutParams.WRAP_CONTENT,2.0f));

        /* Creating a TextView to add to the row **/
        TextView tvPrice = new TextView(mContext);
        tvPrice.setText(mContext.getResources().getString(R.string.value));
        tvPrice.setGravity(Gravity.CENTER);

        tvPrice.setTextColor(Color.CYAN);
        tvPrice.setTypeface(Typeface.DEFAULT, Typeface.BOLD);
        tvPrice.setPadding(5, 5, 5, 5);
        tvPrice.setTextSize(TypedValue.COMPLEX_UNIT_PX,
                mContext.getResources().getDimension(R.dimen.test_tbl_header));
        tr.addView(tvPrice, new TableRow.LayoutParams
                (0,TableRow.LayoutParams.WRAP_CONTENT,1.0f));

        tbl.addView(tr);
        Log.d("tableRow","FirstRowCreated<<<<<<<<<<<<<<<<<<<<");

    }


}
