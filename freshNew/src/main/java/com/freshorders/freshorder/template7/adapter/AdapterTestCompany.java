package com.freshorders.freshorder.template7.adapter;

import android.content.Context;
import android.graphics.PorterDuff;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.freshorders.freshorder.R;

import java.util.ArrayList;
import java.util.List;

public class AdapterTestCompany extends FragmentStatePagerAdapter {

    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();
    private final List<Integer> mFragmentIconList = new ArrayList<>();
    private Context context;

    public AdapterTestCompany(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }
    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }
    public void addFragment(Fragment fragment, String title, int tabIcon) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
        mFragmentIconList.add(tabIcon);
    }
    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
//return mFragmentTitleList.get(position);
        return null;
    }
    @Override
    public int getCount() {
        return mFragmentList.size();
    }
    public View getTabView(int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.tab_custom_test_company, null);
        TextView tabTextView = (TextView) view.findViewById(R.id.id_test_TV_txtTabName);
        tabTextView.setText(mFragmentTitleList.get(position));
        ImageView tabImageView = (ImageView) view.findViewById(R.id.id_test_IV_tabImage);
        tabImageView.setImageResource(mFragmentIconList.get(position));
        return view;
    }
    public View getSelectedTabView(int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.tab_custom_test_company, null);
        TextView tabTextView = (TextView) view.findViewById(R.id.id_test_TV_txtTabName);
        tabTextView.setText(mFragmentTitleList.get(position));
        tabTextView.setTextColor(ContextCompat.getColor(context, R.color.app_color_green));
        ImageView tabImageView = (ImageView) view.findViewById(R.id.id_test_IV_tabImage);
        tabImageView.setImageResource(mFragmentIconList.get(position));
        tabImageView.setColorFilter(ContextCompat.getColor(context, R.color.app_color_green), PorterDuff.Mode.SRC_ATOP);
        return view;
    }
}