package com.freshorders.freshorder.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.freshorders.freshorder.R;

import com.freshorders.freshorder.view.LazyDatePicker;
import com.freshorders.freshorder.weekcalendar.WeekCalendar;
import com.freshorders.freshorder.weekcalendar.listener.OnDateClickListener;
import com.freshorders.freshorder.weekcalendar.listener.OnWeekChangeListener;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Date;

import static com.freshorders.freshorder.utils.Constants.INTENT_KEY_DATE_RESULT_CODE;

public class WeekDateSelectActivity extends AppCompatActivity {

    private static final String TAG = WeekDateSelectActivity.class.getSimpleName();

    private WeekCalendar weekCalendar;
    private DateTimeFormatter dateTimeFormatter;
    private TextView tvSelectedDate, tvFirstDay, tvLastDay;
    private Button btnPrevious, btnForward;

    String strStartDate = "";
    String strEndDate = "";


    private static final String DATE_FORMAT = "dd-MM-yyyy";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_week_date_select);

        init();
    }

    private void init() {
        weekCalendar = (WeekCalendar) findViewById(R.id.weekCalendar);
        ///////////////////////////// Kumaravel
        Button btnPick = findViewById(R.id.id_week_pop_btn_pick_date);
        btnPick.setOnClickListener(pickDateListener);

        dateTimeFormatter = DateTimeFormat.forPattern("dd-MM-yyyy");
        tvSelectedDate = findViewById(R.id.id_week_popup_tv_selected_date);
        tvFirstDay = findViewById(R.id.id_week_popup_tv_start_date);
        tvLastDay = findViewById(R.id.id_week_popup_tv_end_date);

        DateTime curDate = new DateTime().plusDays(0);
        String date = dateTimeFormatter.print(curDate);
        tvSelectedDate.setText(date);

        DateTime start = curDate.withDayOfWeek(1);
        DateTime end = curDate.withDayOfWeek(7);
        strStartDate = dateTimeFormatter.print(start);
        tvFirstDay.setText(strStartDate);
        strEndDate = dateTimeFormatter.print(end);
        tvLastDay.setText(strEndDate);

        btnPrevious = findViewById(R.id.id_week_pop_btn_previous_week);
        btnForward = findViewById(R.id.id_week_pop_btn_next_week);

        btnPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                weekCalendar.previousWeek();
            }
        });

        btnForward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                weekCalendar.nextWeek();
            }
        });


        Date minDate = LazyDatePicker.stringToDate("01-01-2000", DATE_FORMAT);
        Date maxDate = LazyDatePicker.stringToDate("12-31-3018", DATE_FORMAT);
        // Init LazyDatePicker
        LazyDatePicker lazyDatePicker = (LazyDatePicker) findViewById(R.id.lazyDatePicker);
        lazyDatePicker.setDateFormat(LazyDatePicker.DateFormat.DD_MM_YYYY);
        lazyDatePicker.setMinDate(minDate);
        lazyDatePicker.setMaxDate(maxDate);

        lazyDatePicker.setOnDatePickListener(new LazyDatePicker.OnDatePickListener() {
            @Override
            public void onDatePick(Date dateSelected) {
                try {
                    String selectedDate = LazyDatePicker.dateToString(dateSelected, DATE_FORMAT);
                    DateTime dt = dateTimeFormatter.parseDateTime(selectedDate);
                    weekCalendar.setSelectedDate(dt);
                    /////////////////////
                    DateTime start = dt.withDayOfWeek(1);
                    DateTime end = dt.withDayOfWeek(7);
                    strStartDate = dateTimeFormatter.print(start);
                    tvFirstDay.setText(strStartDate);
                    strEndDate = dateTimeFormatter.print(end);
                    tvLastDay.setText(strEndDate);

                    Log.e(TAG,"..........DateTime......." + dt);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        /////////////////////////////////end
        /*
        Button todaysDate = (Button) findViewById(R.id.today);
        Button selectedDate = (Button) findViewById(R.id.selectedDateButton);
        final Button startDate = (Button) findViewById(R.id.startDate);
        todaysDate.setText(new DateTime().toLocalDate().toString() + " (Reset Button)");
        selectedDate.setText(new DateTime().plusDays(50).toLocalDate().toString()
                + " (Set Selected Date Button)");
        startDate.setText(new DateTime().plusDays(7).toLocalDate().toString()
                + " (Set Start Date Button)"); */



        weekCalendar.setOnDateClickListener(new OnDateClickListener() {
            @Override
            public void onDateClick(DateTime dateTime) {
                Toast.makeText(WeekDateSelectActivity.this, "You Selected " + dateTime.toString(), Toast
                        .LENGTH_SHORT).show();

                DateTime start = dateTime.withDayOfWeek(1);
                DateTime end = dateTime.withDayOfWeek(7);
                strStartDate = dateTimeFormatter.print(start);
                tvFirstDay.setText(strStartDate);
                strEndDate = dateTimeFormatter.print(end);
                tvLastDay.setText(strEndDate);

                String date = dateTimeFormatter.print(dateTime);
                Log.e(TAG, "..........DATE:::" + date);
                tvSelectedDate.setText(date);

            }
        });
        weekCalendar.setOnWeekChangeListener(new OnWeekChangeListener() {
            @Override
            public void onWeekChange(DateTime firstDayOfTheWeek, boolean forward) {
                //Toast.makeText(WeekDateSelectActivity.this, "Week changed: " + firstDayOfTheWeek + " Forward: " + forward, Toast.LENGTH_SHORT).show();

                strStartDate = dateTimeFormatter.print(firstDayOfTheWeek);
                //startDate = firstDate;
                tvFirstDay.setText(strStartDate);
                DateTime endDate = firstDayOfTheWeek.plusDays(6);
                strEndDate = dateTimeFormatter.print(endDate);
                tvLastDay.setText(strEndDate);
            }
        });
    }

    View.OnClickListener pickDateListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent();
            intent.putExtra("FROM_DATE",strStartDate);
            intent.putExtra("TO_DATE",strEndDate);
            setResult(123,intent);
            finish();//finishing activity
        }
    };


    public void onNextClick(View veiw) {
        weekCalendar.moveToNext();
    }


    public void onPreviousClick(View view) {
        weekCalendar.moveToPrevious();
    }

    public void onResetClick(View view) {
        weekCalendar.reset();

    }
    public void onSelectedDateClick(View view){
        weekCalendar.setSelectedDate(new DateTime().plusDays(50));
    }
    public void onStartDateClick(View view){
        weekCalendar.setStartDate(new DateTime().plusDays(7));
    }
}
