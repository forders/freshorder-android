package com.freshorders.freshorder.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.PRAdapter;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.fragment.PeriodicReportDialogFragment;
import com.freshorders.freshorder.model.DSRDetailInputModel;
import com.freshorders.freshorder.model.DSRDetailModel;
import com.freshorders.freshorder.model.DSRFullDetailModel;
import com.freshorders.freshorder.model.DSRModel;
import com.freshorders.freshorder.model.PRFullDetailModel;
import com.freshorders.freshorder.model.PRModel;
import com.freshorders.freshorder.model.SalesManFullResponseModel;
import com.freshorders.freshorder.model.SalesManResponseModel;
import com.freshorders.freshorder.model.SalesmanListFetchByIdModel;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.CurrencyFormat;
import com.freshorders.freshorder.utils.MobileNet;
import com.freshorders.freshorder.volleylib.APIClient;
import com.freshorders.freshorder.volleylib.APIInterface;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.LinearLayout.VERTICAL;
import static com.freshorders.freshorder.utils.Constants.INTENT_KEY_DATE_RESULT_CODE;

public class PeriodicReportActivity extends AppCompatActivity {

    LinkedHashMap<String,String> salesMan_name_id_pair = new LinkedHashMap<>();
    List<String> listOfSalesman = new ArrayList<>();
    List<String> listSalesmanId = new ArrayList<>();

    private DatabaseHandler databaseHandler;

    public interface MyBSheet{
        public void show(String msg);
        public void hide();
    }

    public static final String TAG = PeriodicReportActivity.class.getSimpleName();

    private LinearLayout lineHeaderProgress ;
    private Toolbar toolbar;

    private RecyclerView recyclerview;
    private PRAdapter rvAdapter;
    private List<PRModel> listItems;
    private View rootView;

    LinearLayout layoutBottomSheet;
    private BottomSheetBehavior sheetBehavior;
    private Button btnBottomSheet;
    private TextView tvBottomSheet;

    private LinearLayout reportView;
    private LinearLayout chartView;
    private EditText selectDate;
    private boolean datePopupVisible = false;

    private Button btnViewWeekReport;
    private Button btnViewWeekReportAsChart;
    private HorizontalBarChart chart;


    private String fromDate = "", toDate = "";
    private String reportFromDate = "" , reportToDate = "";

    private DateTimeFormatter dateTimeFormatterForServer;
    private DateTimeFormatter dateTimeFormatterAPP;

    private List<PRModel>  detailModelList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_periodic_report);

        toolbar = (Toolbar) findViewById(R.id.id_toolbar_pr);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
            ab.setDisplayHomeAsUpEnabled(true);

            ab.setDisplayShowHomeEnabled(true);
            ab.setTitle(getResources().getString(R.string.periodic_report));
        }
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // perform whatever you want on back arrow click
                Log.e(TAG,"...back arrow clicked");
                moveToHomeActivity();
            }
        });

        dateTimeFormatterForServer = DateTimeFormat.forPattern("yyyy-MM-dd");
        dateTimeFormatterAPP = DateTimeFormat.forPattern("dd-MM-yyyy");

        rootView = findViewById(R.id.LL_pr_root);
        lineHeaderProgress = findViewById(R.id.lineHeaderProgress_PR);

        setWeekCalender();
        btnViewWeekReport = findViewById(R.id.id_pr_Table);
        btnViewWeekReportAsChart = findViewById(R.id.id_pr_Chart);
        btnViewWeekReport.setOnClickListener(viewReportListener);
        btnViewWeekReportAsChart.setOnClickListener(chartListener);

        reportView = findViewById(R.id.id_pr_LL_tableView_root);
        chartView = findViewById(R.id.id_pr_LL_chartView_root);

        chart = (HorizontalBarChart) findViewById(R.id.id_pr_chart);
        initializeChart();
        //////////
        ///bottom sheet
        setBottomSheetListener();
        updateUserId();
        dateSetting();
        setRecyclerView();
        ///fetchingData(true);
        //////////setChartListener();
    }

    @Override
    public void onStart() {
        super.onStart();
        //resultDate = "";
        updateUserId();
        if (listOfSalesman != null && listOfSalesman.size() > 0) {
            //returning from backstack, data is fine, do nothing
            Log.e(TAG,".............onStart..data is fine");
        } else {
            //newly created, compute data
            Log.e(TAG,".............onStart..newly created, compute data");
            fetchingData(false);
        }
    }

    View.OnClickListener viewReportListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(isDateSelected()) {
                if (isNameListLoaded()) {
                    switchView(btnViewWeekReport.getId());
                } else {
                    reportView.setVisibility(View.VISIBLE);
                    chartView.setVisibility(View.GONE);
                    fetchingData(true);
                }
            }else {
                showAlertDialogToast("Please Select/Re-Select Date");
            }
        }
    };

    View.OnClickListener chartListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(isDateSelected()) {
                if (isNameListLoaded()) {
                    switchView(btnViewWeekReportAsChart.getId());
                } else {
                    reportView.setVisibility(View.GONE);
                    chartView.setVisibility(View.VISIBLE);
                    fetchingData(false);
                }
            }else {
                showAlertDialogToast("Please Select/Re-Select Date");
            }
        }
    };

    private boolean isNameListLoaded(){
        return listOfSalesman != null && listOfSalesman.size() > 0;
    }

    private void fetchingData(boolean isDisplayDSR){
        if(MobileNet.isNetAvail(PeriodicReportActivity.this)) {
            if (isDateSelected()){
                getSalesmanList(isDisplayDSR);
            }else {
                showBottomSheet("Please select date");
            }
        }else {
            //showAlertDialogToast(getResources().getString(R.string.no_active_network_avail));
            showBottomSheet(getResources().getString(R.string.no_active_network_avail));
        }
    }

    private void dateSetting() {
        ///Default date setting
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        DateTime today = new DateTime();
        //fromDate = dateTimeFormatterForServer.print(today);
        //toDate = dateTimeFormatterForServer.print(today);
    }

    private void clear(){
        if(listOfSalesman != null){
            listOfSalesman.clear();
        }else {
            listOfSalesman = new ArrayList<>();
        }
        if(listSalesmanId != null){
            listSalesmanId.clear();
        }else {
            listSalesmanId = new ArrayList<>();
        }
        if(salesMan_name_id_pair != null){
            salesMan_name_id_pair.clear();
        }else {
            salesMan_name_id_pair = new LinkedHashMap<>();
        }
    }

    private void displayDSRChart(){
        setData();
        chart.setFitBars(true);
        chart.animateY(2000);
        chart.invalidate();
    }

    private void setData() {

        ////////////
        String strTotal = listItems.get(listItems.size()-1).getTotalSales();
        String total = CurrencyFormat.getFormattedAmounts_Rs(strTotal);
        String strFinal = "Total : " + total;
        chart.getDescription().setText(strFinal);
        ////////////

        /////////mapID_DSRModel.clear();
        /*
        ///listItems.clear();
        for(int i = 0; i < listItems.size(); i++){
            DSRModel cur = listItems.get(i);
            cur.setBranch_salesManName(cur.getBranch_salesManName());
            cur.setSalesValue(cur.getSalesValue());
            cur.setSalesAmount(cur.getSalesAmount());
        } */

        XAxis xAxis = chart.getXAxis();
        //xAxis.setCenterAxisLabels(true);
        xAxis.setLabelCount(listItems.size());

        //Now add the labels to be added on the vertical axis
        final ArrayList<String> axiss = new ArrayList<>();


        float barWidth = 0.23f;
        float spaceForBar = 10f;
        ArrayList<BarEntry> values = new ArrayList<>();

        float item = 0f;
        for (int i = 0; i < listItems.size(); i++) {
            PRModel cur = listItems.get(i);
            String str = cur.getValue().replaceAll("[^\\d.]", "");
            float val = Float.parseFloat(str);
            String brName = cur.getProdname();
            axiss.add(brName);
            values.add(new BarEntry(item, val));
            ////////mapID_DSRModel.put((int) item,cur);
            item ++;

        }
        xAxis.setValueFormatter(new IndexAxisValueFormatter(axiss));

        //BarDataSet set1;
        PeriodicReportActivity.MyBarDataSet set1;


        if (chart.getData() != null &&
                chart.getData().getDataSetCount() > 0) {
            //set1 = (BarDataSet) chart.getData().getDataSetByIndex(0);
            set1 = (PeriodicReportActivity.MyBarDataSet) chart.getData().getDataSetByIndex(0);

            //set1.setColors(Color.parseColor("#2DC9D7"));
            //set1.setColor(ResourcesCompat.getColor(getResources(), R.color.maroon, null));
            //setColorToBarChart(set1);

            set1.setValues(values);
            chart.getData().notifyDataChanged();
            chart.notifyDataSetChanged();
        } else {
            //set1 = new BarDataSet(values, "Today Sales");
            set1 = new PeriodicReportActivity.MyBarDataSet(values, "Today Sales");

            //set1.setColors(Color.parseColor("#2DC9D7"));
            //set1.setColor(ResourcesCompat.getColor(getResources(), R.color.maroon, null));
            //setColorToBarChart(set1);
            set1.setColors(new int[]{R.color.Green,R.color.Red,R.color.Brown,R.color.Fuchsia,
                            R.color.DarkSeaGreen,R.color.Yellow,R.color.Crimson,R.color.MediumSlateBlue,
                            R.color.LimeGreen,R.color.Gold,R.color.DarkOrange,R.color.MidnightBlue,
                            R.color.Navy,R.color.Chocolate,R.color.DarkViolet,R.color.DeepSkyBlue,
                            R.color.Indigo,R.color.SeaGreen,R.color.DarkRed,R.color.DarkOliveGreen} ,
                    PeriodicReportActivity.this);

            set1.setDrawIcons(false);

            ArrayList<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            data.setValueTextSize(9f);
            ////data.setValueTypeface(tfLight);
            data.setBarWidth(barWidth);
            chart.setData(data);
        }
    }

    public class MyBarDataSet extends BarDataSet {


        public MyBarDataSet(List<BarEntry> vals, String label) {
            super(vals, label);
        }

        @Override
        public int getColor(int index) {

            switch(index % 20){
                case 0:
                    return mColors.get(0);
                case 1:
                    return mColors.get(1);
                case 2:
                    return mColors.get(2);
                case 3:
                    return mColors.get(3);
                case 4:
                    return mColors.get(4);
                case 5:
                    return mColors.get(5);
                case 6:
                    return mColors.get(6);
                case 7:
                    return mColors.get(7);
                case 8:
                    return mColors.get(8);
                case 9:
                    return mColors.get(9);
                case 10:
                    return mColors.get(10);
                case 11:
                    return mColors.get(11);
                case 12:
                    return mColors.get(12);
                case 13:
                    return mColors.get(13);
                case 14:
                    return mColors.get(14);
                case 15:
                    return mColors.get(15);
                case 16:
                    return mColors.get(16);
                case 17:
                    return mColors.get(17);
                case 18:
                    return mColors.get(18);
                case 19:
                    return mColors.get(19);
                default:
                    return mColors.get(5);
            }
            /*
            if(getEntryForXIndex(index).getVal() < 95) // less than 95 green
                return mColors.get(0);
            else if(getEntryForXIndex(index).getVal() < 100) // less than 100 orange
                return mColors.get(1);
            else // greater or equal than 100 red
                return mColors.get(2); */
        }

    }

    private void initializeChart(){
        chart.setDrawBarShadow(false);
        String strDescription = "";

        chart.setPinchZoom(false);
        chart.setDrawValueAboveBar(true);

        //Display the axis on the left (contains the labels 1*, 2* and so on)
        XAxis xAxis = chart.getXAxis();
        xAxis.setDrawGridLines(false);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setEnabled(true);
        xAxis.setDrawAxisLine(true);
        xAxis.setGranularity(1f);

        YAxis yLeft = chart.getAxisLeft();

        //Set the minimum and maximum bar lengths as per the values that they represent
        //yLeft.setAxisMaximum(100f);
        yLeft.setDrawAxisLine(true);
        yLeft.setDrawGridLines(true);
        yLeft.setAxisMinimum(0f);
        yLeft.setEnabled(true);


        YAxis yRight = chart.getAxisRight();
        yRight.setDrawAxisLine(true);
        yRight.setDrawGridLines(false);
        yRight.setEnabled(false);
        yRight.setAxisMinimum(0f);
    }



    private void initialize(){
        databaseHandler =  new DatabaseHandler(this);
    }

    private void updateUserId(){
        if (Constants.DUSER_ID == null || Constants.DUSER_ID.isEmpty()) {
            if(databaseHandler == null) {
                databaseHandler =  new DatabaseHandler(this);
            }
            Cursor curs;
            curs = databaseHandler.getDetails();
            if(curs != null && curs.getCount() > 0) {
                curs.moveToFirst();
                Constants.DUSER_ID = curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_id));
                Log.e("Constants.DUSER_ID", Constants.DUSER_ID);
                curs.close();
            }else {
                showAlertDialogToast(getResources().getString(R.string.need_to_re_open));
            }
        }
    }

    public void showAlertDialogToast(String message) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(PeriodicReportActivity.this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    private void show(){
        if(lineHeaderProgress != null) {
            lineHeaderProgress.setVisibility(View.VISIBLE);
        }
    }

    private void hide(){
        if(lineHeaderProgress != null) {
            lineHeaderProgress.setVisibility(View.GONE);
        }
    }

    private void getSalesmanList(final boolean isDisplayDSR){
        clear();
        updateUserId();//////////////

        SalesmanListFetchByIdModel model = new SalesmanListFetchByIdModel();
        model.setUserid(Constants.DUSER_ID);

        show();//////////////

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<SalesManFullResponseModel> call = apiInterface.getSalesManOrBranchList(model);

        call.enqueue(new Callback<SalesManFullResponseModel>() {

            @Override
            public void onResponse(Call<SalesManFullResponseModel> call, Response<SalesManFullResponseModel> response) {
                Log.e(TAG,"......................response");
                hide();
                try {
                    if (response != null) {
                        if (response.isSuccessful()) {
                            Log.e(TAG, "......................Success");
                            SalesManFullResponseModel res = response.body();
                            if (res != null) {
                                String status = res.getStatus();
                                if (status.equalsIgnoreCase("true")) {
                                    List<SalesManResponseModel> list = res.getData();
                                    if (list != null && list.size() > 0) {
                                        for (int i = 0; i < list.size(); i++) {
                                            SalesManResponseModel model = new SalesManResponseModel();
                                            SalesManResponseModel cur = list.get(i);
                                            String strUserId = cur.getUserid();
                                            String strName = cur.getFullname();
                                            listOfSalesman.add(strName);
                                            listSalesmanId.add(strUserId);
                                            salesMan_name_id_pair.put(strUserId, strName);
                                        }
                                        if(listOfSalesman.size() > 0 && isDisplayDSR){
                                            reportView.setVisibility(View.VISIBLE);
                                            chartView.setVisibility(View.GONE);
                                            displayDSR(false);
                                        }else if(listOfSalesman.size() > 0 && !isDisplayDSR){
                                            reportView.setVisibility(View.GONE);
                                            chartView.setVisibility(View.VISIBLE);
                                            displayDSR(true);
                                        }else {
                                            showBottomSheet(getResources().getString(R.string.salesman_branch_not_found));
                                        }
                                    } else {
                                        showBottomSheet(getResources().getString(R.string.no_data_avail));
                                    }
                                } else {
                                    showBottomSheet(getResources().getString(R.string.no_data_avail));
                                }
                            } else {
                                showBottomSheet(getResources().getString(R.string.no_res_server));
                            }
                        } else {

                            // error case
                            switch (response.code()) {
                                case 404:
                                    //Toast.makeText(ErrorHandlingActivity.this, "not found", Toast.LENGTH_SHORT).show();
                                    showBottomSheet(getResources().getString(R.string.response_code_404));
                                    break;
                                case 500:
                                    //Toast.makeText(ErrorHandlingActivity.this, "server broken", Toast.LENGTH_SHORT).show();
                                    showBottomSheet(getResources().getString(R.string.response_code_500));
                                    break;
                                default:
                                    //Toast.makeText(ErrorHandlingActivity.this, "unknown error", Toast.LENGTH_SHORT).show();
                                    showBottomSheet(getResources().getString(R.string.unknown_error) + "_" + response.code());
                                    break;
                            }

                        }
                    } else {
                        showBottomSheet(getResources().getString(R.string.no_res_server));
                    }
                }catch (Exception e){
                    hide();////
                    e.printStackTrace();
                    showBottomSheet(getResources().getString(R.string.technical_fault)+e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SalesManFullResponseModel> call, Throwable t) {
                hide();///
                Log.e(TAG,"......................Fail");
                Log.e(TAG,"......................Fail" + t.getMessage());
                showBottomSheet("-------" +  t.getMessage());
            }
        });
    }

    private void displayDSR(boolean isShowChart){
        try {
            String suserid = listSalesmanId.get(0);
            DateTime dateFrom = DateTime.parse(fromDate, dateTimeFormatterAPP);
            String fDate = dateTimeFormatterForServer.print(dateFrom);
            DateTime dateTo = DateTime.parse(toDate, dateTimeFormatterAPP);
            String tDate = dateTimeFormatterForServer.print(dateTo);
            DSRDetailInputModel out = new DSRDetailInputModel(suserid, "SALES", "S", fDate, tDate);
            reportFromDate = fromDate;
            reportToDate = toDate;
            getSalesDetail(out, isShowChart);
        }catch (Exception e){
            e.printStackTrace();
            collapseBottomSheet();
            showBottomSheet(getResources().getString(R.string.technical_fault) + " -- " + e.getMessage());
        }
    }

    private void getSalesDetail(DSRDetailInputModel out, final boolean isShowChart) {

        show();

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        final Call<PRFullDetailModel> call = apiInterface.getPRFullDetail(out);

        call.enqueue(new Callback<PRFullDetailModel>() {
            @Override
            public void onResponse(Call<PRFullDetailModel> call, Response<PRFullDetailModel> response) {
                Log.e(TAG,"......................response");
                hide();
                try {
                    if (response != null) {
                        if (response.isSuccessful()) {
                            Log.e(TAG, "......................Success");
                            PRFullDetailModel res = response.body();
                            if (res != null) {
                                String status = res.getStatus();
                                if (status.equalsIgnoreCase("true")) {
                                    List<PRModel> list = res.getData();
                                    if (list != null && list.size() > 0) {
                                        //DSRDetailModel model = new DSRDetailModel();
                                        //detailModelList.addAll(list);
                                        listItems.addAll(list);
                                        float total = 0f;
                                        float totalDebit = 0f;
                                        for(int i = 0; i < list.size(); i++){
                                            PRModel cur = list.get(i);
                                            String str = cur.getValue().replaceAll("[^\\d.]", "");
                                            float s = Float.parseFloat(str);
                                            if(cur.getUom().equalsIgnoreCase("Credit")){
                                                total = total + s;
                                            }else if(cur.getUom().equalsIgnoreCase("Debit")){
                                                totalDebit = totalDebit + s;
                                            }

                                            if( i == list.size() -1){
                                                float finalAmt = total - totalDebit;
                                                cur.setTotalSales(String.valueOf(finalAmt));
                                                list.set(i , cur);
                                            }
                                        }
                                        if (!isShowChart) {
                                            rvAdapter.notifyDataSetChanged();
                                        }else {
                                            displayDSRChart();
                                        }
                                        //showPopup(brName, date, totalSales);
                                    } else {
                                        showBottomSheet(PeriodicReportActivity.this.getResources().getString(R.string.no_data_avail));
                                    }
                                } else {
                                    showBottomSheet(PeriodicReportActivity.this.getResources().getString(R.string.no_data_avail));
                                }
                            } else {
                                showBottomSheet(PeriodicReportActivity.this.getResources().getString(R.string.no_res_server));
                            }
                        } else {

                            // error case
                            switch (response.code()) {
                                case 404:
                                    //Toast.makeText(ErrorHandlingActivity.this, "not found", Toast.LENGTH_SHORT).show();
                                    showBottomSheet(PeriodicReportActivity.this.getResources().getString(R.string.response_code_404));
                                    break;
                                case 500:
                                    //Toast.makeText(ErrorHandlingActivity.this, "server broken", Toast.LENGTH_SHORT).show();
                                    showBottomSheet(PeriodicReportActivity.this.getResources().getString(R.string.response_code_500));
                                    break;
                                default:
                                    //Toast.makeText(ErrorHandlingActivity.this, "unknown error", Toast.LENGTH_SHORT).show();
                                    showBottomSheet(PeriodicReportActivity.this.getResources().getString(R.string.unknown_error) + "_" + response.code());
                                    break;
                            }

                        }
                    } else {
                        showBottomSheet(PeriodicReportActivity.this.getResources().getString(R.string.no_res_server));
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    showBottomSheet(PeriodicReportActivity.this.getResources().getString(R.string.technical_fault)+e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<PRFullDetailModel> call, Throwable t) {
                hide();
                Log.e(TAG,"......................Fail");
                Log.e(TAG,"......................Fail" + t.getMessage());
                showBottomSheet("-------" +  t.getMessage());
            }
        });

    }

    private void setWeekCalender(){
        final DisplayMetrics displayMetrics = Resources.getSystem().getDisplayMetrics();
        selectDate = findViewById(R.id.id_PR_select_date);
        selectDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*if(!datePopupVisible){
                    PopupWeekCalender datePopup = new PopupWeekCalender(
                            PeriodicReportActivity.this, selectDate, displayMetrics, new PopupWeekCalender.WeekPopupListener() {
                        @Override
                        public void onDismiss() {
                            datePopupVisible = false;
                        }

                        @Override
                        public void isPopupAvail(boolean isShowing) {
                            if(isShowing) {
                                datePopupVisible = true;
                            }
                        }

                        @Override
                        public void interruptOccur(String msg) {

                        }
                    });
                    datePopup.showPopupWindow();
                }  */

                //showWeekSelectDialog();
                collapseBottomSheet();
                showWeekDateSelectActivity();



            }
        });
    }

    private void showWeekDateSelectActivity(){
        try {
            Intent intent = new Intent(PeriodicReportActivity.this, WeekDateSelectActivity.class);
            startActivityForResult(intent, 123);// Activity is started with requestCode 123
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    // Call Back method  to get the Message form other Activity
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data != null) {
            Bundle extras = data.getExtras();
            // check if the request code is same as what is passed  here it is 2
            Log.e(TAG,"...resultCode " + resultCode + "  ..... requestCode" + requestCode);
            if (resultCode == 123 &&
                    requestCode == 123) {
                String message = "$";
                if (extras != null) {
                    fromDate = data.getStringExtra("FROM_DATE");
                    toDate = data.getStringExtra("TO_DATE");
                }
                String queryDate = fromDate + "   To   " + toDate;
                selectDate.setText(queryDate);
                if(isDateMatchWithResult()){
                    Log.e(TAG,"...........reportFromDate" + reportFromDate + "...reportToDate.. " + reportToDate);
                }else {
                    Log.e(TAG,".......Result..Date..different..with..selected..date");
                    clearMapAndChart();
                }

                Log.e(TAG, "...................sel;cted" + queryDate);
            }else {
                Log.e(TAG, ".........Result not available....");
            }
        }else {
            Log.e(TAG, ".........data not available....");
        }
    }

    private void clearMapAndChart(){
        if(listItems != null){
            listItems.clear();
        }
        if(rvAdapter != null){
            rvAdapter.notifyDataSetChanged();
            recyclerview.removeAllViewsInLayout();
        }
        if(chart != null) {
            chart.clear();
            chart.notifyDataSetChanged();
            chart.invalidate();
        }
        reportFromDate = "";
        reportToDate = "";

    }

    private void showWeekSelectDialog(){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        DialogFragment dialogFragment = new PeriodicReportDialogFragment();
        dialogFragment.show(ft, "dialog");
    }

    private void setRecyclerView(){
        listItems = new ArrayList<>();
        recyclerview = (RecyclerView) findViewById(R.id.id_PR_RV);
        LinearLayoutManager layoutManager = new LinearLayoutManager(PeriodicReportActivity.this);
        recyclerview.setLayoutManager(layoutManager);
        //Add Divider
        DividerItemDecoration itemDecor = new DividerItemDecoration(recyclerview.getContext(), VERTICAL);
        recyclerview.addItemDecoration(itemDecor);
        //creating recyclerView adapter
        rvAdapter = new PRAdapter(PeriodicReportActivity.this, listItems,rootView, new PeriodicReportActivity.MyBSheet() {
            @Override
            public void show(String msg) {
                showBottomSheet(msg);
            }

            @Override
            public void hide() {
                collapseBottomSheet();
            }
        });
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        //setting adapter to recyclerView
        recyclerview.setAdapter(rvAdapter);
    }

    private void setBottomSheetListener(){

        layoutBottomSheet = findViewById(R.id.id_bottom_sheet_dsr);
        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
        btnBottomSheet = findViewById(R.id.id_dsr_btn_bottom_sheet);
        tvBottomSheet = findViewById(R.id.id_dsr_tv_notification);

        btnBottomSheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                collapseBottomSheet();
            }
        });
        /**
         * bottom sheet state change listener
         * we are changing button text when sheet changed state
         * */
        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        btnBottomSheet.setText(getResources().getString(R.string.close));
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        btnBottomSheet.setText(getResources().getString(R.string.expand_me));
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    private boolean isSalesItemsLoaded(){
        return listItems != null && listItems.size() > 0;
    }

    private void switchView(int id){


        switch (id){
            case R.id.id_pr_Table:
                reportView.setVisibility(View.VISIBLE);
                chartView.setVisibility(View.GONE);
                if(isDateMatchWithResult()) {
                    if (isSalesItemsLoaded()) {
                        rvAdapter.notifyDataSetChanged();
                    } else {
                        displayDSR(false);
                    }
                }else {
                    displayDSR(false);
                }

                break;
            case R.id.id_pr_Chart:
                reportView.setVisibility(View.GONE);
                chartView.setVisibility(View.VISIBLE);
                    if(isDateMatchWithResult()) {
                        if (isSalesItemsLoaded()) {
                            Log.e(TAG, "Already data avail for this date....");
                            displayDSRChart();
                        } else {
                            displayDSR(true);
                        }
                    }else {
                        displayDSR(true);
                    }

                break;

        }
    }

    private boolean isDateSelected(){
        return !fromDate.isEmpty() && !toDate.isEmpty();
    }

    private boolean isDateMatchWithResult(){
        if(!fromDate.isEmpty() && ! toDate.isEmpty()){
            if(reportFromDate.equalsIgnoreCase(fromDate) &&
            reportToDate.equalsIgnoreCase(toDate)){
                return true;
            }
        }
        return false;
    }

    private void showBottomSheet(String str){
        if(sheetBehavior != null) {
            if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                tvBottomSheet.setText(str);
            }else{
                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                tvBottomSheet.setText(str);
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        }
    }
    private void collapseBottomSheet(){
        if(sheetBehavior != null) {
            if (sheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        }
    }

    private void moveToHomeActivity(){
        Intent intent = new Intent(PeriodicReportActivity.this, ManagerActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
