package com.freshorders.freshorder.activity;

import android.content.Context;
import android.database.Cursor;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.model.errorModel;

import java.util.ArrayList;
import java.util.List;

public class LogList extends AppCompatActivity {
    DatabaseHandler databaseHandler;
    ListView listView;
    customadapter arrayAdapter;
    List<errorModel> locationdata = new ArrayList<>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview);
        databaseHandler = new DatabaseHandler(getApplicationContext());
        listView = findViewById(R.id.listview);
        refresh();
    }

    private void refresh() {
        Cursor cur = databaseHandler.getAllError();
        if(cur != null && cur.getCount() > 0){
            cur.moveToFirst();
            while (!cur.isAfterLast()){
                errorModel model = new errorModel(cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_ERROR_MESSAGE)),
                                                                cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_ERROR_DATE)),
                                                                cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_ERROR_TIME)));
                locationdata.add(model);
                cur.moveToNext();
            }
        }
        //locationdata= AppDatabase.getAppDatabase(LogList.this).errorDao().getAll();
        arrayAdapter = new customadapter(LogList.this,locationdata);
        listView.setAdapter(arrayAdapter);
    }

    public void refresh(View view) {
        refresh();
    }

    private class customadapter extends BaseAdapter {
        Context context;
        List<errorModel> locationdata;
        public customadapter(Context errorList, List<errorModel> locationdata) {
            this.context = errorList;
            this.locationdata = locationdata;
        }

        @Override
        public int getCount() {
            return locationdata.size();
        }

        @Override
        public Object getItem(int position) {
            return locationdata.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = getLayoutInflater().inflate(R.layout.error_item,null);

            TextView date = convertView.findViewById(R.id.date);
            TextView time = convertView.findViewById(R.id.time);
            TextView srno = convertView.findViewById(R.id.srno);
            TextView message = convertView.findViewById(R.id.message);

            date.setText("Date : "+locationdata.get(position).getDate());
            time.setText("Time : "+locationdata.get(position).getTime());
            srno.setText("Srno : "+locationdata.get(position).getSrno());
            message.setText(locationdata.get(position).getMessage());
            return convertView;
        }
    }
}
