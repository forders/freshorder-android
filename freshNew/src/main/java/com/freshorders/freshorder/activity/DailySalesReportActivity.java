package com.freshorders.freshorder.activity;

/*
    This is for chart view
    Copyright 2019 Philipp Jahoda

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
 */

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.DSRAdapter;
import com.freshorders.freshorder.asyntask.SalesmanListAsync;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.model.DSRDetailInputModel;
import com.freshorders.freshorder.model.DSRDetailModel;
import com.freshorders.freshorder.model.DSRFullDetailModel;
import com.freshorders.freshorder.model.DSRFullResponseModel;
import com.freshorders.freshorder.model.DSRModel;
import com.freshorders.freshorder.model.DSRResponseModel;
import com.freshorders.freshorder.model.SalesManFullResponseModel;
import com.freshorders.freshorder.model.SalesManResponseModel;
import com.freshorders.freshorder.model.SalesmanListFetchByIdModel;
import com.freshorders.freshorder.popup.PopupDSRDetail;
import com.freshorders.freshorder.popup.PopupOutletStockEntryDatePicker;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.CurrencyFormat;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.KeyboardUtils;
import com.freshorders.freshorder.utils.MobileNet;
import com.freshorders.freshorder.utils.Utils;

import com.freshorders.freshorder.volleylib.APIClient;
import com.freshorders.freshorder.volleylib.APIInterface;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.LinearLayout.VERTICAL;


public class DailySalesReportActivity extends AppCompatActivity {

    public interface MyBSheet{
        public void show(String msg);
        public void hide();
    }

    public static final String TAG = DailySalesReportActivity.class.getSimpleName();

    LinkedHashMap<String,String> salesMan_name_id_pair = new LinkedHashMap<>();
    List<String> listOfSalesman = new ArrayList<>();
    List<String> listSalesmanId = new ArrayList<>();

    LinkedHashMap<Integer,DSRModel> mapID_DSRModel = new LinkedHashMap<>();

    private LinearLayout lineHeaderProgress ;
    private EditText txtDate;
    private String strSelectedDate = "";
    private Button btnViewSalesDetail;
    private Button btnViewSalesDetailSAsChart;
    private DatabaseHandler databaseHandler;

    LinearLayout layoutBottomSheet;
    private BottomSheetBehavior sheetBehavior;
    private Button btnBottomSheet;
    private TextView tvBottomSheet;

    private RecyclerView recyclerview;
    private DSRAdapter rvAdapter;
    private List<DSRModel> listItems;
    private View rootView;

    HorizontalBarChart chart;

    LinearLayout salesView;
    LinearLayout chartView;

    private String resultDate = "";

    private Toolbar toolbar;

    private void moveToHomeActivity(){
        Intent intent = new Intent(DailySalesReportActivity.this, ManagerActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void onStart() {
        super.onStart();
        resultDate = "";
        updateUserId();
        if (listOfSalesman != null && listOfSalesman.size() > 0) {
            //returning from backstack, data is fine, do nothing
            Log.e(TAG,".............onStart..data is fine");
        } else {
            //newly created, compute data
            Log.e(TAG,".............onStart..newly created, compute data");
            fetchingData(false);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_sales_report);

        toolbar = (Toolbar) findViewById(R.id.id_toolbar_dsr);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
            ab.setDisplayHomeAsUpEnabled(true);

            ab.setDisplayShowHomeEnabled(true);
            ab.setTitle(getResources().getString(R.string.daily_sales_report));
        }
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // perform whatever you want on back arrow click
                Log.e(TAG,"...back arrow clicked");
                moveToHomeActivity();
            }
        });

        rootView = findViewById(R.id.LL_dsr_root);

        initializeList();

        lineHeaderProgress = findViewById(R.id.lineHeaderProgress_DSR);
        txtDate = findViewById(R.id.id_daily_sales_report_select_date);
        btnViewSalesDetail = findViewById(R.id.id_DSR_Table);
        btnViewSalesDetailSAsChart = findViewById(R.id.id_DSR_Chart);
        btnViewSalesDetail.setOnClickListener(viewSalesListener);
        btnViewSalesDetailSAsChart.setOnClickListener(chartListener);

        salesView = findViewById(R.id.id_dsr_LL_tableView_root);
        chartView = findViewById(R.id.id_dsr_LL_chartView_root);
        //////////
        chart = (HorizontalBarChart) findViewById(R.id.id_dsr_chart);
        initializeChart();
        //////////
        ///bottom sheet
        setBottomSheetListener();
        updateUserId();
        dateSetting();
        setRecyclerView();
        ///fetchingData(true);
        setChartListener();
    }



    private void setRecyclerView(){
        listItems = new ArrayList<>();
        recyclerview = (RecyclerView) findViewById(R.id.id_DSR_RV);
        LinearLayoutManager layoutManager = new LinearLayoutManager(DailySalesReportActivity.this);
        recyclerview.setLayoutManager(layoutManager);
        //Add Divider
        DividerItemDecoration itemDecor = new DividerItemDecoration(recyclerview.getContext(), VERTICAL);
        recyclerview.addItemDecoration(itemDecor);
        //creating recyclerView adapter
        rvAdapter = new DSRAdapter(DailySalesReportActivity.this, listItems,rootView, new DailySalesReportActivity.MyBSheet() {
            @Override
            public void show(String msg) {
                showBottomSheet(msg);
            }

            @Override
            public void hide() {
                collapseBottomSheet();
            }
        });
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        //setting adapter to recyclerView
        recyclerview.setAdapter(rvAdapter);
    }



    private void setBottomSheetListener(){

        layoutBottomSheet = findViewById(R.id.id_bottom_sheet_dsr);
        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
        btnBottomSheet = findViewById(R.id.id_dsr_btn_bottom_sheet);
        tvBottomSheet = findViewById(R.id.id_dsr_tv_notification);

        btnBottomSheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                collapseBottomSheet();
            }
        });
        /**
         * bottom sheet state change listener
         * we are changing button text when sheet changed state
         * */
        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                        btnBottomSheet.setText(getResources().getString(R.string.close));
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        btnBottomSheet.setText(getResources().getString(R.string.expand_me));
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    private void showBottomSheet(String str){
        if(sheetBehavior != null) {
            if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                tvBottomSheet.setText(str);
            }
        }
    }
    private void collapseBottomSheet(){
        if(sheetBehavior != null) {
            if (sheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        }
    }

    private boolean isSalesItemsLoaded(){
        return listItems != null && listItems.size() > 0;
    }

    private void switchView(int id){

        switch (id){
            case R.id.id_DSR_Table:
                salesView.setVisibility(View.VISIBLE);
                chartView.setVisibility(View.GONE);
                if(strSelectedDate.equalsIgnoreCase(resultDate)){
                    if(isSalesItemsLoaded()){
                        rvAdapter.notifyDataSetChanged();
                    }else {
                        displayDSR(false);
                    }
                }else {
                    displayDSR(false);
                }

                break;
            case R.id.id_DSR_Chart:
                salesView.setVisibility(View.GONE);
                chartView.setVisibility(View.VISIBLE);
                if(strSelectedDate.equalsIgnoreCase(resultDate)){
                    if(isSalesItemsLoaded()){
                        Log.e(TAG,"Already data avail for this date....");
                        displayDSRChart();
                    }else {
                        displayDSR(true);
                    }
                }else {
                    displayDSR(true);
                }
                break;

        }
    }

    View.OnClickListener viewSalesListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(isNameListLoaded()){
                switchView(btnViewSalesDetail.getId());

            }else {
                salesView.setVisibility(View.VISIBLE);
                chartView.setVisibility(View.GONE);
                fetchingData(true);
            }
        }
    };

    View.OnClickListener chartListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if(isNameListLoaded()){
                switchView(btnViewSalesDetailSAsChart.getId());

            }else {
                salesView.setVisibility(View.GONE);
                chartView.setVisibility(View.VISIBLE);
                fetchingData(false);
            }
        }
    };

    private void displayDSR(final boolean isShowChart){
        try {

            show();
            listItems.clear();
            if(!isShowChart) {
                rvAdapter.notifyDataSetChanged();
            }
            String strSelectedDate1 = "";


            strSelectedDate = txtDate.getText().toString().trim();

            try {
                Log.e("Selected",".........................................Date:" + strSelectedDate);
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
                SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                Date newDate = format.parse(strSelectedDate);
                Log.e("Date",".........................................." + newDate);
                strSelectedDate1 = format1.format(newDate);
            }catch (Exception e){
                hide();
                e.printStackTrace();
                showBottomSheet(getResources().getString(R.string.technical_fault) + e.getMessage());
                return;
            }

            JSONObject object = new JSONObject();
            JSONArray between = new JSONArray();

            //between.put(0, "2019-08-10" + "+00:00:00");
            between.put(0, strSelectedDate1 + "+00:00:00");
            between.put(1, strSelectedDate1 + "+23:59:59");

            //between.put(0, strSelectedDate1);
            //between.put(1, strSelectedDate1);

            //between.put(0, strSelectedDate1 + " 00:00:00");
            //between.put(1, strSelectedDate1 + " 23:59:59");


            JSONObject inqObj = new JSONObject();
            JSONArray inq = new JSONArray();
            int[] inqs ;

            for(int i = 0; i < listSalesmanId.size(); i++){
                Long longId = Long.valueOf(listSalesmanId.get(i));
                inq.put(longId);
            }
            inqObj.put("inq",inq);

            JSONObject mostInnerObj = new JSONObject();
            mostInnerObj.put("between",between);

            JSONArray includeArray = new JSONArray();
            includeArray.put("orderdetail");

            object.put("ordstatus","Pending");
            object.put("orderdt",mostInnerObj);
            object.put("suserid",inqObj);
            object.put("include",includeArray);


            ////object.put("include",include);
            JSONObject finalObj = new JSONObject();
            finalObj.put("where",object);

            Log.e(TAG, "............................."+finalObj.toString());

            JSONStringer jsonStringer = new JSONStringer();

            jsonStringer.object().key("where").object()
                    .key("ordstatus").value("Pending")
                    .key("orderdt").value(mostInnerObj)
                    .key("suserid").value(inqObj)
                    .key("include").value("orderdetail")
                    .endObject()
                    .endObject();


            Log.e(TAG,"......````````````````````````````Request :: " + jsonStringer.toString());



            String url = Utils.strDailySalesReport + jsonStringer.toString();

            APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
            final Call<DSRFullResponseModel> call = apiInterface.getDSR(url);

            final String finalStrSelectedDate = strSelectedDate1;
            call.enqueue(new Callback<DSRFullResponseModel>() {
                @Override
                public void onResponse(Call<DSRFullResponseModel> call, Response<DSRFullResponseModel> response) {
                    Log.e(TAG,"......................response");
                    try {
                        hide();
                        if (response != null) {
                            if (response.isSuccessful()) {
                                Log.e(TAG, "......................Success");
                                DSRFullResponseModel res = response.body();
                                if (res != null) {
                                    String status = res.getStatus();
                                    if (status.equalsIgnoreCase("true")) {
                                        List<DSRResponseModel> list = res.getData();
                                        if (list != null && list.size() > 0) {
                                            float fTotal = 0f;
                                            LinkedHashMap<String, Integer> repeatReduceMap = new LinkedHashMap<>();
                                            for (int i = 0; i < list.size(); i++) {
                                                DSRModel model = new DSRModel();
                                                DSRResponseModel cur = list.get(i);
                                                String sales_br_id = cur.getSuserid();
                                                model.setSuserid(sales_br_id);
                                                model.setMuserid(cur.getMuserid());
                                                model.setSalesValue(cur.getAprxordval());
                                                float total = Float.parseFloat(cur.getAprxordval());
                                                fTotal = fTotal + total;
                                                model.setSalesAmount(total);
                                                model.setSelectedDate(finalStrSelectedDate);
                                                if (salesMan_name_id_pair.get(sales_br_id) != null) {
                                                    model.setBranch_salesManName(salesMan_name_id_pair.get(sales_br_id));
                                                } else {
                                                    model.setBranch_salesManName("");
                                                }
                                                if( i == list.size() - 1){
                                                    model.setTotalSales(String.valueOf(fTotal));
                                                }
                                                ///avoid multiple entry convert into single one
                                                if(repeatReduceMap.get(sales_br_id) != null){
                                                    int position = repeatReduceMap.get(sales_br_id);
                                                    DSRModel in_model = listItems.get(position);
                                                    String id = in_model.getSuserid();
                                                    Log.e(TAG,"....iteration: " + i + "..previousID : " + id + " currentId : " + sales_br_id + " Position :" + position);
                                                    if(id.equals(sales_br_id)){
                                                        float preValue = in_model.getSalesAmount();
                                                        float insertValue = preValue + total;
                                                        /////////////////////because total value is in final object so i remove identical entry
                                                        ////listItems.remove(position);
                                                        ///////////////////
                                                        model.setSalesAmount(insertValue);
                                                        model.setSalesValue(String.valueOf(insertValue));
                                                        if( i == list.size() - 1){
                                                            listItems.remove(position);
                                                            listItems.add(model);
                                                            ///////
                                                            repeatReduceMap.put(sales_br_id,listItems.size()-1);
                                                            Log.e(TAG,"final........remove..add Index : "+(listItems.size()-1));
                                                        }else {
                                                            listItems.set(position,model);
                                                            ///////
                                                            repeatReduceMap.put(sales_br_id,position);
                                                        }
                                                    }else {
                                                        Log.e(TAG,"..............***********this should not happen****************");
                                                        listItems.add(model);
                                                        ///////
                                                        repeatReduceMap.put(sales_br_id,listItems.size()-1);
                                                    }
                                                }else {
                                                    listItems.add(model);
                                                    ///////
                                                    repeatReduceMap.put(sales_br_id,listItems.size()-1);
                                                }
                                            }
                                            resultDate = strSelectedDate; ///////////////////////////////////
                                            if (!isShowChart) {
                                                rvAdapter.notifyDataSetChanged();
                                            }else {
                                                displayDSRChart();
                                            }
                                        } else {
                                            showBottomSheet(getResources().getString(R.string.no_data_avail));
                                        }
                                    } else {
                                        showBottomSheet(getResources().getString(R.string.no_data_avail));
                                    }
                                } else {
                                    showBottomSheet(getResources().getString(R.string.no_res_server));
                                }
                            } else {
                                // error case
                                switch (response.code()) {
                                    case 404:
                                        //Toast.makeText(ErrorHandlingActivity.this, "not found", Toast.LENGTH_SHORT).show();
                                        showBottomSheet(getResources().getString(R.string.response_code_404));
                                        break;
                                    case 500:
                                        //Toast.makeText(ErrorHandlingActivity.this, "server broken", Toast.LENGTH_SHORT).show();
                                        showBottomSheet(getResources().getString(R.string.response_code_500));
                                        break;
                                    default:
                                        //Toast.makeText(ErrorHandlingActivity.this, "unknown error", Toast.LENGTH_SHORT).show();
                                        showBottomSheet(getResources().getString(R.string.unknown_error) + "_" + response.code());
                                        break;
                                }
                            }
                        } else {
                            showBottomSheet(getResources().getString(R.string.no_res_server));
                        }
                    }catch (Exception e){
                        hide();////
                        e.printStackTrace();
                        showBottomSheet(getResources().getString(R.string.technical_fault)+e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<DSRFullResponseModel> call, Throwable t) {
                    hide();
                    Log.e(TAG,"......................Fail");
                    Log.e(TAG,"......................Fail" + t.getMessage());
                    showBottomSheet("-------" +  t.getMessage());
                }
            });
            /*
            LoadDSRAsync obj = new LoadDSRAsync(jsonServiceHandler, new LoadDSRAsync._DSR_CompleteListener() {
                @Override
                public void onSuccess(JSONObject result) {
                    hide();
                    try {
                        if(result.has("data")) {
                            JSONArray resultArray = result.getJSONArray("data");
                            if(resultArray.length() > 0) {
                                Log.e(TAG,"................" + result.toString());
                                for (int i = 0; i < resultArray.length(); i++) {
                                    DSRModel currentItem = new DSRModel();

                                    listItems.add(currentItem);
                                }
                                rvAdapter.notifyDataSetChanged();
                            }else {
                                showBottomSheet(DSR_NO_DATA_FOUND);
                            }

                        }else {
                            showBottomSheet(DSR_NO_DATA_FOUND);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                        showBottomSheet(getResources().getString(R.string.technical_fault) + e.getMessage());
                    }
                }

                @Override
                public void onFailed(String msg) {
                    hide();
                    showBottomSheet(msg);
                }

                @Override
                public void onException(String msg) {
                    hide();
                    showBottomSheet(msg);
                }
            });
            obj.execute();  */


        }catch (Exception e){
            e.printStackTrace();
            showBottomSheet(getResources().getString(R.string.technical_fault) + e.getMessage());
            hide();
        }


    }

    private boolean isNameListLoaded(){
        return listOfSalesman != null && listOfSalesman.size() > 0;
    }

    private void fetchingData(boolean isDisplayDSR){
        if(MobileNet.isNetAvail(DailySalesReportActivity.this)) {
            getSalesmanList(isDisplayDSR);
        }else {
            //showAlertDialogToast(getResources().getString(R.string.no_active_network_avail));
            showBottomSheet(getResources().getString(R.string.no_active_network_avail));
        }
    }

    private void dateSetting() {
        ///Default date setting
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        txtDate.setText(formattedDate);
        strSelectedDate = formattedDate;

        txtDate.setOnClickListener(selectDate);
    }

    View.OnClickListener selectDate = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            PopupOutletStockEntryDatePicker datePopup = new
                    PopupOutletStockEntryDatePicker(
                    DailySalesReportActivity.this, txtDate,
                    new PopupOutletStockEntryDatePicker.PopupListener() {
                        @Override
                        public void onPopupClosed() {

                            String previousDate = strSelectedDate;
                            strSelectedDate = txtDate.getText().toString().trim();
                            if(!previousDate.isEmpty()){
                                if(!previousDate.equalsIgnoreCase(strSelectedDate)){
                                    //Clear Chart and Table
                                    listItems.clear();
                                    rvAdapter.notifyDataSetChanged();
                                    recyclerview.removeAllViewsInLayout();
                                    //////////////
                                    chart.clear();
                                    chart.notifyDataSetChanged();
                                    chart.invalidate();
                                }
                            }
                            Log.e("Selected",".........................................Date:" + strSelectedDate);
                        }
                    });
            datePopup.showPopup();
        }
    };

    private void initialize(){
        databaseHandler =  new DatabaseHandler(this);
    }
    private void updateUserId(){
        if (Constants.DUSER_ID == null || Constants.DUSER_ID.isEmpty()) {
            if(databaseHandler == null) {
                databaseHandler =  new DatabaseHandler(this);
            }
            Cursor curs;
            curs = databaseHandler.getDetails();
            if(curs != null && curs.getCount() > 0) {
                curs.moveToFirst();
                Constants.DUSER_ID = curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_id));
                Log.e("Constants.DUSER_ID", Constants.DUSER_ID);
                curs.close();
            }else {
                showAlertDialogToast(getResources().getString(R.string.need_to_re_open));
            }
        }
    }

    private void show(){
        if(lineHeaderProgress != null) {
            lineHeaderProgress.setVisibility(View.VISIBLE);
        }
    }

    private void hide(){
        if(lineHeaderProgress != null) {
            lineHeaderProgress.setVisibility(View.GONE);
        }
    }

    private void getSalesmanList(final boolean isDisplayDSR){
        clear();
        updateUserId();//////////////

        SalesmanListFetchByIdModel model = new SalesmanListFetchByIdModel();
        model.setUserid(Constants.DUSER_ID);

        show();//////////////

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        final Call<SalesManFullResponseModel> call = apiInterface.getSalesManOrBranchList(model);

        call.enqueue(new Callback<SalesManFullResponseModel>() {

            @Override
            public void onResponse(Call<SalesManFullResponseModel> call, Response<SalesManFullResponseModel> response) {
                Log.e(TAG,"......................response");
                hide();
                try {
                    if (response != null) {
                        if (response.isSuccessful()) {
                            Log.e(TAG, "......................Success");
                            SalesManFullResponseModel res = response.body();
                            if (res != null) {
                                String status = res.getStatus();
                                if (status.equalsIgnoreCase("true")) {
                                    List<SalesManResponseModel> list = res.getData();
                                    if (list != null && list.size() > 0) {
                                        for (int i = 0; i < list.size(); i++) {
                                            SalesManResponseModel model = new SalesManResponseModel();
                                            SalesManResponseModel cur = list.get(i);
                                            String strUserId = cur.getUserid();
                                            String strName = cur.getFullname();
                                            listOfSalesman.add(strName);
                                            listSalesmanId.add(strUserId);
                                            salesMan_name_id_pair.put(strUserId, strName);
                                        }
                                        if(listOfSalesman.size() > 0 && isDisplayDSR){
                                            salesView.setVisibility(View.VISIBLE);
                                            chartView.setVisibility(View.GONE);
                                            displayDSR(false);
                                        }else if(listOfSalesman.size() > 0 && !isDisplayDSR){
                                            salesView.setVisibility(View.GONE);
                                            chartView.setVisibility(View.VISIBLE);
                                            displayDSR(true);
                                        }else {
                                            showBottomSheet(getResources().getString(R.string.salesman_branch_not_found));
                                        }
                                    } else {
                                        showBottomSheet(getResources().getString(R.string.no_data_avail));
                                    }
                                } else {
                                    showBottomSheet(getResources().getString(R.string.no_data_avail));
                                }
                            } else {
                                showBottomSheet(getResources().getString(R.string.no_res_server));
                            }
                        } else {

                            // error case
                            switch (response.code()) {
                                case 404:
                                    //Toast.makeText(ErrorHandlingActivity.this, "not found", Toast.LENGTH_SHORT).show();
                                    showBottomSheet(getResources().getString(R.string.response_code_404));
                                    break;
                                case 500:
                                    //Toast.makeText(ErrorHandlingActivity.this, "server broken", Toast.LENGTH_SHORT).show();
                                    showBottomSheet(getResources().getString(R.string.response_code_500));
                                    break;
                                default:
                                    //Toast.makeText(ErrorHandlingActivity.this, "unknown error", Toast.LENGTH_SHORT).show();
                                    showBottomSheet(getResources().getString(R.string.unknown_error) + "_" + response.code());
                                    break;
                            }

                        }
                    } else {
                        showBottomSheet(getResources().getString(R.string.no_res_server));
                    }
                }catch (Exception e){
                    hide();////
                    e.printStackTrace();
                    showBottomSheet(getResources().getString(R.string.technical_fault)+e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SalesManFullResponseModel> call, Throwable t) {
                hide();///
                Log.e(TAG,"......................Fail");
                Log.e(TAG,"......................Fail" + t.getMessage());
                showBottomSheet("-------" +  t.getMessage());
            }
        });
    }

    private void getSalesmanORBranchList(){  //

        clear();
        updateUserId();//////////////

        SalesmanListFetchByIdModel model = new SalesmanListFetchByIdModel();
        model.setUserid(Constants.DUSER_ID);
        Gson gson = new Gson();
        String json = gson.toJson(model);
        Log.e(TAG, "............................."+json);
        JsonServiceHandler jsonServiceHandler = new JsonServiceHandler(Utils.strGetMerchantList,json , DailySalesReportActivity.this);
        SalesmanListAsync obj = new SalesmanListAsync(jsonServiceHandler,
                new SalesmanListAsync.SalesmanListCompleteListener() {
                    @Override
                    public void onSuccess(JSONObject result) {
                        hide();
                        if(result.has("data")) {
                            try {
                                JSONArray jsonArray = result.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject object = jsonArray.getJSONObject(i);
                                    String strUserId = object.getString("userid");
                                    String strName = object.getString("fullname");
                                    listOfSalesman.add(strName);
                                    listSalesmanId.add(strUserId);
                                    salesMan_name_id_pair.put(strUserId, strName);
                                }
                                //////////////spinnerUpdate(listOfSalesman);

                            }catch (Exception e){
                                e.printStackTrace();
                                showBottomSheet(getResources().getString(R.string.technical_fault)+e.getMessage());
                            }
                        }
                    }

                    @Override
                    public void onFailed(String msg) {
                        hide();
                        showBottomSheet(msg);
                    }

                    @Override
                    public void onException(String msg) {
                        hide();
                        showBottomSheet(msg);
                    }
                });
        obj.execute();
    }


    private void initializeList(){
        listOfSalesman = new ArrayList<>();
        listSalesmanId = new ArrayList<>();
        salesMan_name_id_pair = new LinkedHashMap<>();
    }

    private void clear(){
        if(listOfSalesman != null){
            listOfSalesman.clear();
        }else {
            listOfSalesman = new ArrayList<>();
        }
        if(listSalesmanId != null){
            listSalesmanId.clear();
        }else {
            listSalesmanId = new ArrayList<>();
        }
        if(salesMan_name_id_pair != null){
            salesMan_name_id_pair.clear();
        }else {
            salesMan_name_id_pair = new LinkedHashMap<>();
        }
    }

    public void showAlertDialogToast(String message) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(DailySalesReportActivity.this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }


    private boolean isNetAvail(Context context){
        try {
            boolean isConnected,isMobile = false;
            ConnectivityManager cm =
                    (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = null;
            if (cm != null) {
                activeNetwork = cm.getActiveNetworkInfo();
            }else {
                return false;
            }

            isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
            if (activeNetwork != null) {
                isMobile = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE;
            }
            if(isConnected && isMobile){
                return true;
            }
            return false;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }



    private void displayDSRChart(){
        //BarData data = new BarData(getDataSet());
        //chart.setData(data);
        //chart.animateXY(2000, 2000);

        //BarDataSet dataSet = new BarDataSet(getGraphData(),"Today Sales");
        //BarData data = new BarData(dataSet);
        //data.setBarWidth(0.5f);
        //chart.setData(data);
        setData();
        chart.setFitBars(true);
        chart.animateY(2000);
        chart.invalidate();
    }

    private void initializeChart(){
        chart.setDrawBarShadow(false);
        String strDescription = "";

        chart.setPinchZoom(false);
        chart.setDrawValueAboveBar(true);

        //Display the axis on the left (contains the labels 1*, 2* and so on)
        XAxis xAxis = chart.getXAxis();
        xAxis.setDrawGridLines(false);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setEnabled(true);
        xAxis.setDrawAxisLine(true);
        xAxis.setGranularity(1f);

        YAxis yLeft = chart.getAxisLeft();

        //Set the minimum and maximum bar lengths as per the values that they represent
        //yLeft.setAxisMaximum(100f);
        yLeft.setDrawAxisLine(true);
        yLeft.setDrawGridLines(true);
        yLeft.setAxisMinimum(0f);
        yLeft.setEnabled(true);


        YAxis yRight = chart.getAxisRight();
        yRight.setDrawAxisLine(true);
        yRight.setDrawGridLines(false);
        yRight.setEnabled(false);
        yRight.setAxisMinimum(0f);



    }

    private List<BarEntry> getGraphData(){


            //Add a list of bar entries
            List<BarEntry> entries = new ArrayList<BarEntry>();
            entries.add(new BarEntry(0f, 2700f));
            entries.add(new BarEntry(1f, 4500f));
            entries.add(new BarEntry(2f, 65000));
            entries.add(new BarEntry(3f, 7700000f));
            entries.add(new BarEntry(4f, 930000f));

            return entries;


    }

    private void setData() {

        ////////////
        String strTotal = listItems.get(listItems.size()-1).getTotalSales();
        String total = CurrencyFormat.getFormattedAmounts_Rs(strTotal);
        String strFinal = "Total Sales : " + total;
        chart.getDescription().setText(strFinal);
        ////////////

        mapID_DSRModel.clear();
        /*
        ///listItems.clear();
        for(int i = 0; i < listItems.size(); i++){
            DSRModel cur = listItems.get(i);
            cur.setBranch_salesManName(cur.getBranch_salesManName());
            cur.setSalesValue(cur.getSalesValue());
            cur.setSalesAmount(cur.getSalesAmount());
        } */

        XAxis xAxis = chart.getXAxis();
        //xAxis.setCenterAxisLabels(true);
        xAxis.setLabelCount(listItems.size());

        //Now add the labels to be added on the vertical axis
        final ArrayList<String> axiss = new ArrayList<>();


        float barWidth = 0.23f;
        float spaceForBar = 10f;
        ArrayList<BarEntry> values = new ArrayList<>();

        float item = 0f;
        for (int i = 0; i < listItems.size(); i++) {
            DSRModel cur = listItems.get(i);
            float val = cur.getSalesAmount();
            String brName = cur.getBranch_salesManName();
            axiss.add(brName);
            values.add(new BarEntry(item, val));
            mapID_DSRModel.put((int) item,cur);
            item ++;

        }
        xAxis.setValueFormatter(new IndexAxisValueFormatter(axiss));

        //BarDataSet set1;
        MyBarDataSet set1;


        if (chart.getData() != null &&
                chart.getData().getDataSetCount() > 0) {
            //set1 = (BarDataSet) chart.getData().getDataSetByIndex(0);
            set1 = (MyBarDataSet) chart.getData().getDataSetByIndex(0);

            //set1.setColors(Color.parseColor("#2DC9D7"));
            //set1.setColor(ResourcesCompat.getColor(getResources(), R.color.maroon, null));
            //setColorToBarChart(set1);

            set1.setValues(values);
            chart.getData().notifyDataChanged();
            chart.notifyDataSetChanged();
        } else {
            //set1 = new BarDataSet(values, "Today Sales");
            set1 = new MyBarDataSet(values, "Today Sales");

            //set1.setColors(Color.parseColor("#2DC9D7"));
            //set1.setColor(ResourcesCompat.getColor(getResources(), R.color.maroon, null));
            //setColorToBarChart(set1);
            set1.setColors(new int[]{R.color.Green,R.color.Red,R.color.Brown,R.color.Fuchsia,
                                    R.color.DarkSeaGreen,R.color.Yellow,R.color.Crimson,R.color.MediumSlateBlue,
                                    R.color.LimeGreen,R.color.Gold,R.color.DarkOrange,R.color.MidnightBlue,
                                    R.color.Navy,R.color.Chocolate,R.color.DarkViolet,R.color.DeepSkyBlue,
                                    R.color.Indigo,R.color.SeaGreen,R.color.DarkRed,R.color.DarkOliveGreen} ,
                                    DailySalesReportActivity.this);

            set1.setDrawIcons(false);

            ArrayList<IBarDataSet> dataSets = new ArrayList<>();
            dataSets.add(set1);

            BarData data = new BarData(dataSets);
            data.setValueTextSize(9f);
            ////data.setValueTypeface(tfLight);
            data.setBarWidth(barWidth);
            chart.setData(data);
        }
    }

    public class MyBarDataSet extends BarDataSet {


        public MyBarDataSet(List<BarEntry> vals, String label) {
            super(vals, label);
        }

        @Override
        public int getColor(int index) {

            switch(index % 20){
                case 0:
                    return mColors.get(0);
                case 1:
                    return mColors.get(1);
                case 2:
                    return mColors.get(2);
                case 3:
                    return mColors.get(3);
                case 4:
                    return mColors.get(4);
                case 5:
                    return mColors.get(5);
                case 6:
                    return mColors.get(6);
                case 7:
                    return mColors.get(7);
                case 8:
                    return mColors.get(8);
                case 9:
                    return mColors.get(9);
                case 10:
                    return mColors.get(10);
                case 11:
                    return mColors.get(11);
                case 12:
                    return mColors.get(12);
                case 13:
                    return mColors.get(13);
                case 14:
                    return mColors.get(14);
                case 15:
                    return mColors.get(15);
                case 16:
                    return mColors.get(16);
                case 17:
                    return mColors.get(17);
                case 18:
                    return mColors.get(18);
                case 19:
                    return mColors.get(19);
                default:
                    return mColors.get(5);
            }
            /*
            if(getEntryForXIndex(index).getVal() < 95) // less than 95 green
                return mColors.get(0);
            else if(getEntryForXIndex(index).getVal() < 100) // less than 100 orange
                return mColors.get(1);
            else // greater or equal than 100 red
                return mColors.get(2); */
        }

    }

    private void setColorToBarChart(MyBarDataSet set){
        String[] colorNames = getResources().getStringArray(R.array.barChartColors);
        TypedArray ta = getResources().obtainTypedArray(R.array.barChartColors);

        int[] colorArray = new int[colorNames.length];
        for(int i = 0; i < colorNames.length; i++) {
            //Getting the color resource id
            int colorToUse = ta.getResourceId(i, 0);
            colorArray[i] = colorToUse;
            Log.e(TAG, "............. in color..." + colorArray[i]);
        }
        set.setColors(colorArray);

        ta.recycle();
    }

    private void setChartListener(){
        chart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {
                try {
                    float x = e.getX();
                    float y = e.getY();
                    Log.e(TAG, "................Chart Clicked.. X: " + x + " Y: " + y);
                    DSRModel model = mapID_DSRModel.get((int) x);
                    String date = getDateForServer(strSelectedDate);
                    if(date.isEmpty()){
                        showBottomSheet(getResources().getString(R.string.technical_fault) + "--" + "Date conversion problem");
                        return;
                    }
                    DSRDetailInputModel out = new DSRDetailInputModel(model.getSuserid(), "SALES", "S", date , date);
                    getSalesDetail(out, model.getBranch_salesManName(), date, model.getSalesValue());
                }catch (Exception ex){
                    ex.printStackTrace();
                    showBottomSheet(getResources().getString(R.string.technical_fault) + "--" + ex.getMessage());
                }
            }

            @Override
            public void onNothingSelected() {

            }
        });
    }

    private void getSalesDetail(DSRDetailInputModel out, final String brName, final String date, final String salesTotal) {

        showBottomSheet(getResources().getString(R.string.wait_data_fetching));


        final List<DSRDetailModel>  detailModelList = new ArrayList<>();

        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);

        final Call<DSRFullDetailModel> call = apiInterface.getDSRFullDetail(out);

        call.enqueue(new Callback<DSRFullDetailModel>() {
            @Override
            public void onResponse(Call<DSRFullDetailModel> call, Response<DSRFullDetailModel> response) {
                Log.e(TAG,"......................response");
                collapseBottomSheet();//////////////

                try {
                    if (response != null) {
                        if (response.isSuccessful()) {
                            Log.e(TAG, "......................Success");
                            DSRFullDetailModel res = response.body();
                            if (res != null) {
                                String status = res.getStatus();
                                if (status.equalsIgnoreCase("true")) {
                                    List<DSRDetailModel> list = res.getData();
                                    if (list != null && list.size() > 0) {
                                        //DSRDetailModel model = new DSRDetailModel();
                                        detailModelList.addAll(list);
                                        showPopup(detailModelList, brName, date, salesTotal);
                                    } else {
                                        showBottomSheet(getResources().getString(R.string.no_data_avail));
                                    }
                                } else {
                                    showBottomSheet(getResources().getString(R.string.no_data_avail));
                                }
                            } else {
                                showBottomSheet(getResources().getString(R.string.no_res_server));
                            }
                        } else {

                            // error case
                            switch (response.code()) {
                                case 404:
                                    //Toast.makeText(ErrorHandlingActivity.this, "not found", Toast.LENGTH_SHORT).show();
                                    showBottomSheet(getResources().getString(R.string.response_code_404));
                                    break;
                                case 500:
                                    //Toast.makeText(ErrorHandlingActivity.this, "server broken", Toast.LENGTH_SHORT).show();
                                    showBottomSheet(getResources().getString(R.string.response_code_500));
                                    break;
                                default:
                                    //Toast.makeText(ErrorHandlingActivity.this, "unknown error", Toast.LENGTH_SHORT).show();
                                    showBottomSheet(getResources().getString(R.string.unknown_error) + "_" + response.code());
                                    break;
                            }

                        }
                    } else {
                        showBottomSheet(getResources().getString(R.string.no_res_server));
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    showBottomSheet(getResources().getString(R.string.technical_fault)+e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<DSRFullDetailModel> call, Throwable t) {
                collapseBottomSheet();//////////////
                Log.e(TAG,"......................Fail");
                Log.e(TAG,"......................Fail" + t.getMessage());
                showBottomSheet("-------" +  t.getMessage());
            }
        });
    }

    private void showPopup(List<DSRDetailModel>  detailModelList, String brName, String date, String totalSales){
        PopupDSRDetail popup = new PopupDSRDetail(rootView,DailySalesReportActivity.this,detailModelList, brName, date, totalSales);
        popup.showPopup();
    }

    private String getDateForServer(String str){
        try {
            Log.e("Selected", ".........................................Date:" + str);
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            Date newDate = format.parse(str);
            Log.e("Date", ".........................................." + newDate);
            return format1.format(newDate);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }


}
