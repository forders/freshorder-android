package com.freshorders.freshorder.activity;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.PRAdapter;
import com.freshorders.freshorder.adapter.TeamStatusAdapter;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.model.MyTeamStatusDetailModel;
import com.freshorders.freshorder.model.PRModel;
import com.freshorders.freshorder.model.TeamStatusModel;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

import java.util.ArrayList;
import java.util.List;

import static android.widget.LinearLayout.VERTICAL;

public class TeamStatusActivity extends AppCompatActivity {

    private DatabaseHandler databaseHandler;

    public interface MyBSheet{
        public void show(String msg);
        public void hide();
    }

    public static final String TAG = TeamStatusActivity.class.getSimpleName();

    private LinearLayout lineHeaderProgress ;
    private Toolbar toolbar;

    private RecyclerView recyclerview;
    private TeamStatusAdapter rvAdapter;
    private List<TeamStatusModel> listItems;
    private View rootView;

    LinearLayout layoutBottomSheet;
    private BottomSheetBehavior sheetBehavior;
    private Button btnBottomSheet;
    private TextView tvBottomSheet;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_task, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.id_menu_add_task:
                Toast.makeText(getApplicationContext(),"Item 1 Selected",Toast.LENGTH_LONG).show();
                Intent toActivity = new Intent(TeamStatusActivity.this, AddNewTask.class);
                startActivity(toActivity);
                return true;
            case R.id.id_menu_assign_task:
                Intent toAssignActivity = new Intent(TeamStatusActivity.this, AssignTaskActivity.class);
                startActivity(toAssignActivity);
                Toast.makeText(getApplicationContext(),"Item 2 Selected",Toast.LENGTH_LONG).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_status);
        /*
            test ....
         */
        listItems = new ArrayList<>();

        String str = "Name : ";

        TeamStatusModel temp = new TeamStatusModel();
        temp.setPercentageOfWorkCompleted("20");
        temp.setWorkerName("Kumaravel");
        List<MyTeamStatusDetailModel> list = new ArrayList<>();
        MyTeamStatusDetailModel model = new MyTeamStatusDetailModel();
        model.setWorkName("Client Visit");
        model.setWorkCompletionPercentage(50);
        list.add(model);
        MyTeamStatusDetailModel model1 = new MyTeamStatusDetailModel();
        model1.setWorkName("Cleaning Process");
        model1.setWorkCompletionPercentage(23);
        list.add(model1);
        MyTeamStatusDetailModel model2 = new MyTeamStatusDetailModel();
        model2.setWorkName("Daily Work");
        model2.setWorkCompletionPercentage(75);
        list.add(model2);
        MyTeamStatusDetailModel model3 = new MyTeamStatusDetailModel();
        model3.setWorkName("Weekend Work");
        model3.setWorkCompletionPercentage(100);
        list.add(model3);

        temp.setMyTeamStatusDetailModels(list);

        listItems.add(temp);

        TeamStatusModel temp2 = new TeamStatusModel();
        temp2.setPercentageOfWorkCompleted("50");
        temp2.setWorkerName("Nambi");

        List<MyTeamStatusDetailModel> list2 = new ArrayList<>();
        MyTeamStatusDetailModel model21 = new MyTeamStatusDetailModel();
        model21.setWorkName("Client Visit");
        model21.setWorkCompletionPercentage(50);
        list2.add(model21);
        MyTeamStatusDetailModel model22 = new MyTeamStatusDetailModel();
        model22.setWorkName("Cleaning Process");
        model22.setWorkCompletionPercentage(23);
        list2.add(model22);
        MyTeamStatusDetailModel model23 = new MyTeamStatusDetailModel();
        model23.setWorkName("Daily Work");
        model23.setWorkCompletionPercentage(75);
        list2.add(model23);
        MyTeamStatusDetailModel model24 = new MyTeamStatusDetailModel();
        model24.setWorkName("Weekend Work");
        model24.setWorkCompletionPercentage(100);
        list2.add(model24);

        temp2.setMyTeamStatusDetailModels(list2);

        listItems.add(temp2);

        TeamStatusModel temp3 = new TeamStatusModel();
        temp3.setPercentageOfWorkCompleted("75");
        temp3.setWorkerName("Prakash");

        List<MyTeamStatusDetailModel> list3 = new ArrayList<>();
        MyTeamStatusDetailModel model31 = new MyTeamStatusDetailModel();
        model31.setWorkName("Client Visit");
        model31.setWorkCompletionPercentage(50);
        list3.add(model31);
        MyTeamStatusDetailModel model32 = new MyTeamStatusDetailModel();
        model32.setWorkName("Cleaning Process");
        model32.setWorkCompletionPercentage(23);
        list3.add(model32);
        MyTeamStatusDetailModel model33 = new MyTeamStatusDetailModel();
        model33.setWorkName("Daily Work");
        model33.setWorkCompletionPercentage(75);
        list3.add(model33);
        MyTeamStatusDetailModel model34 = new MyTeamStatusDetailModel();
        model34.setWorkName("Weekend Work");
        model34.setWorkCompletionPercentage(100);
        list3.add(model34);

        temp3.setMyTeamStatusDetailModels(list3);

        listItems.add(temp3);

        TeamStatusModel temp4 = new TeamStatusModel();
        temp4.setPercentageOfWorkCompleted("100");
        temp4.setWorkerName("Tamil");

        List<MyTeamStatusDetailModel> list4 = new ArrayList<>();
        MyTeamStatusDetailModel model41 = new MyTeamStatusDetailModel();
        model41.setWorkName("Client Visit");
        model41.setWorkCompletionPercentage(50);
        list4.add(model41);
        MyTeamStatusDetailModel model42 = new MyTeamStatusDetailModel();
        model42.setWorkName("Cleaning Process");
        model42.setWorkCompletionPercentage(23);
        list4.add(model42);
        MyTeamStatusDetailModel model43 = new MyTeamStatusDetailModel();
        model43.setWorkName("Daily Work");
        model43.setWorkCompletionPercentage(80);
        list4.add(model43);
        MyTeamStatusDetailModel model44 = new MyTeamStatusDetailModel();
        model44.setWorkName("Weekend Work");
        model44.setWorkCompletionPercentage(100);
        list4.add(model44);

        temp4.setMyTeamStatusDetailModels(list4);

        listItems.add(temp4);



        toolbar = (Toolbar) findViewById(R.id.id_toolbar_team);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
            ab.setDisplayHomeAsUpEnabled(true);

            ab.setDisplayShowHomeEnabled(true);
            ab.setTitle(getResources().getString(R.string.team_status));
        }
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // perform whatever you want on back arrow click
                Log.e(TAG,"...back arrow clicked");
                moveToHomeActivity();
            }
        });

        setRecyclerView();////////////////
    }

    private void setRecyclerView(){

        recyclerview = (RecyclerView) findViewById(R.id.id_Team_RV);
        LinearLayoutManager layoutManager = new LinearLayoutManager(TeamStatusActivity.this);
        recyclerview.setLayoutManager(layoutManager);
        //Add Divider
        DividerItemDecoration itemDecor = new DividerItemDecoration(recyclerview.getContext(), VERTICAL);
        recyclerview.addItemDecoration(itemDecor);
        //creating recyclerView adapter
        rvAdapter = new TeamStatusAdapter(TeamStatusActivity.this, listItems,rootView, new TeamStatusActivity.MyBSheet() {
            @Override
            public void show(String msg) {
                showBottomSheet(msg);
            }

            @Override
            public void hide() {
                collapseBottomSheet();
            }
        });
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        //setting adapter to recyclerView
        recyclerview.setAdapter(rvAdapter);
    }

    private void showBottomSheet(String str){
        if(sheetBehavior != null) {
            if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                tvBottomSheet.setText(str);
            }else{
                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                tvBottomSheet.setText(str);
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        }
    }
    private void collapseBottomSheet(){
        if(sheetBehavior != null) {
            if (sheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        }
    }

    private void moveToHomeActivity(){
        Intent intent = new Intent(TeamStatusActivity.this, ManagerActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
