package com.freshorders.freshorder.activity;

import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapterrecy.SurveyViewPagerAdapter;
import com.freshorders.freshorder.fragment.SurveyPendingFragment;

public class SurveyPopupActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;
    SurveyViewPagerAdapter viewPagerAdapter;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey_popup);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
            ab.setDisplayHomeAsUpEnabled(true);
        }
        this.setTitle(getResources().getString(R.string.survey_detail));


        viewPager = (ViewPager) findViewById(R.id.viewPager);
        setupViewPager(viewPager);


        viewPager.setAdapter(viewPagerAdapter);
        tabLayout = (TabLayout) findViewById(R.id.id_tab_survey);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        viewPagerAdapter = new SurveyViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new SurveyPendingFragment(), "SUCCESS");
        viewPagerAdapter.addFragment(new SurveyPendingFragment(), "PENDING");
        viewPagerAdapter.addFragment(new SurveyPendingFragment(), "FAILED");
        viewPager.setAdapter(viewPagerAdapter);
    }

}
