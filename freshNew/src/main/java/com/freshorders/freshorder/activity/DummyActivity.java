package com.freshorders.freshorder.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.accuracy.location.track.LocationTrackForegroundService;

public class DummyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_dummy);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(new Intent(DummyActivity.this, LocationTrackForegroundService.class));
        }
        else
        {
            startService(new Intent(DummyActivity.this,LocationTrackForegroundService.class));
        }
        finish();
    }
}
