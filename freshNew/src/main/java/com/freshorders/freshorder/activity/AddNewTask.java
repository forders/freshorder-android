package com.freshorders.freshorder.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.popup.PopupMyTask;
import com.freshorders.freshorder.popup.PopupNewTask;
import com.freshorders.freshorder.popup.PopupOutletStockEntryDatePicker;
import com.google.android.material.textfield.TextInputEditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class AddNewTask extends AppCompatActivity {

    private TextInputEditText txtDate;
    private TextInputEditText txtPeriod;
    private String strSelectedDate = "";

    private DisplayMetrics displayMetrics;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_task);

        txtDate = findViewById(R.id.id_task_due_date);
        txtPeriod = findViewById(R.id.id_task_periodicity);
        dateSetting();

        txtPeriod.setOnClickListener(click);

        displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
    }

    private void dateSetting() {
        ///Default date setting
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        txtDate.setText(formattedDate);
        strSelectedDate = formattedDate;

        txtDate.setOnClickListener(selectDate);
    }


    View.OnClickListener selectDate = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            PopupOutletStockEntryDatePicker datePopup = new
                    PopupOutletStockEntryDatePicker(
                    AddNewTask.this, txtDate,
                    new PopupOutletStockEntryDatePicker.PopupListener() {
                        @Override
                        public void onPopupClosed() {

                            String previousDate = strSelectedDate;
                            strSelectedDate = txtDate.getText().toString().trim();
                            Log.e("Selected",".........................................Date:" + strSelectedDate);
                        }
                    });
            datePopup.showPopup();
        }
    };

    private void showPeriodWindow(View view){

        PopupNewTask pop = new PopupNewTask(AddNewTask.this,view,
                displayMetrics,
                new PopupMyTask.PopupListener() {
                    @Override
                    public void dismiss() {

                    }

                    @Override
                    public void isPopupAvail(boolean isShowing) {

                    }

                    @Override
                    public void interruptOccur(String msg) {

                    }
                }
        );
        pop.showPopupWindow();
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            showPeriodWindow(view);
        }
    };
}
