package com.freshorders.freshorder.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.asyntask.LoadOnlineOrderForManager;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.model.PetrolBunkModel;
import com.freshorders.freshorder.popup.PopupOutletStockEntryDatePicker;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class PetrolBunkActivity extends AppCompatActivity {

    private static final String TAG = PetrolBunkActivity.class.getSimpleName();

    private List<PetrolBunkModel> bunkModelList;
    private LinkedHashMap<String, Float> dailySalesList;
    private LinkedHashMap<String, Float> dailySalesRateList;
    private LinkedHashMap<String, Float> dailySalesAmountList;
    private LinkedHashMap<String, Float> summeryAmountList;
    private LinkedHashMap<String, Float> summeryCumulativeAmountList;

    private LinkedHashMap<String, Float> cashReleasedList;
    private LinkedHashMap<String, Float> creditSalesList;
    private LinkedHashMap<String, Float> expensestList;

    private LinkedHashMap<String, Float> denominationList;

    TableLayout tbl_DOMR;
    int DOMR_TBL_Id = 100;

    //Table DOMR
    private int DOMR_table_column_count = 3;
    private float DOMR_table_weight = 10f;
    private float DOMR_table_column1_weight = 4.5f;
    private float DOMR_table_column2_weight = 4.5f;
    private float DOMR_table_column3_weight = 1f;

    private LinearLayout lineHeaderProgress;

    private EditText txtDate;
    private Button btnViewSales;
    private String strSelectedDate = "";
    private ScrollView scrollViewTable;
    private DatabaseHandler databaseHandler;

    /*
        DOMR table Textview
     */
    TextView tvSKPL001,tvSKPL002,tvSKPL003,tvSKPL004;
    // Daily Sales table
    TextView tvSKPL005,tvSKPL006,tvSKPL007,tvSKPL008, tvSKPL009,tvSKPL010,tvSKPL011,tvSKPL012,
            tvSKPL013,tvSKPL014,tvSKPL015,tvSKPL016, tvSKPL017,tvSKPL018,tvSKPL019,tvSKPL020,
            tvSKPL021,tvSKPL022,tvSKPL023,tvSKPL024,
            tvSKPL025,tvSKPL026,tvSKPL027,tvSKPL028, tvSKPL029,tvSKPL030,tvSKPL031,tvSKPL032,
            tvSKPL033,tvSKPL034,tvSKPL035,tvSKPL036, tvSKPL037,tvSKPL038,tvSKPL039,tvSKPL040,
            tvSKPL041,tvSKPL042,tvSKPL043,tvSKPL044,
            tvSKPL045,tvSKPL046,tvSKPL047,tvSKPL048, tvSKPL049,tvSKPL050,tvSKPL051,tvSKPL052,
            tvSKPL053,tvSKPL054,tvSKPL055,tvSKPL056, tvSKPL057,tvSKPL058,tvSKPL059,tvSKPL060,
            tvSKPL061,tvSKPL062,tvSKPL063,tvSKPL064,
            tvSKPL065,tvSKPL066,tvSKPL067,tvSKPL068, tvSKPL069,tvSKPL070,tvSKPL071,tvSKPL072,
            tvSKPL073,tvSKPL074,tvSKPL075,tvSKPL076, tvSKPL077,tvSKPL078,tvSKPL079,tvSKPL080,
            tvSKPL081,tvSKPL082,tvSKPL083,tvSKPL084,

            tvSKPL084a,tvSKPL084b,tvSKPL084c,tvSKPL084d, tvSKPL084e,tvSKPL084f,tvSKPL084g,tvSKPL084h,
            tvSKPL084i,tvSKPL084j,tvSKPL084k,tvSKPL084l, tvSKPL084m,tvSKPL084n,tvSKPL084o,tvSKPL084p,
            tvSKPL084q,tvSKPL084r,tvSKPL084s,tvSKPL084t,

            tvSKPL084A,tvSKPL084B,tvSKPL084C,tvSKPL084D, tvSKPL084E,tvSKPL084F,tvSKPL084G,tvSKPL084H,
            tvSKPL084I,tvSKPL084J,tvSKPL084K,tvSKPL084L, tvSKPL084M,tvSKPL084N,tvSKPL084O,tvSKPL084P,
            tvSKPL084Q,tvSKPL084R,tvSKPL084S,tvSKPL084T, tvSKPL_DSS_final_total;

    //Petrol Tank 1, diesel tank1 , 2

    TextView tvSKPL085,tvSKPL086,tvSKPL087,tvSKPL088, tvSKPL089,tvSKPL090,tvSKPL090a,tvSKPL091,
            tvSKPL092,tvSKPL093,tvSKPL094,tvSKPL095, tvSKPL096,tvSKPL097,tvSKPL097a,tvSKPL098,
            tvSKPL099,tvSKPL100,tvSKPL101,tvSKPL102, tvSKPL103,tvSKPL104,tvSKPL104a,tvSKPL105;
    //Petrol Pump1 , Diesel Pump1, 2
    TextView tvSKPL106, tvSKPL107, tvSKPL108, tvSKPL109, tvSKPL110,
            tvSKPL110A, tvSKPL110J, tvSKPL110E, tvSKPL110B, tvSKPL110H,
            tvSKPL110F, tvSKPL110D, tvSKPL110I, tvSKPL110C, tvSKPL110G;
    //Sales Summery
    TextView tvSKPL111,tvSKPL112, tvSKPL113, tvSKPL114,tvSKPL115, tvSKPL116, tvSKPL117,tvSKPL118, tvSKPL119,
                tvSKPL120,tvSKPL121, tvSKPL122, tvSKPL123,tvSKPL124, tvSKPL125, tvSKPL_sales_summery_total;
    //Cumulative Sales Summery
    TextView tvSKPL111_Cumulative,tvSKPL112_Cumulative, tvSKPL113_Cumulative, tvSKPL116_Cumulative,
            tvSKPL117_Cumulative,tvSKPL118_Cumulative, tvSKPL121_Cumulative, tvSKPL122_Cumulative,
            tvSKPL123_Cumulative,tvSKPL_Cumulative_sales_summery_total;
    //Credit Sales Cash Released expenses
    TextView tvSKPL126,tvSKPL127, tvSKPL128, tvSKPL129,tvSKPL130,
            tvSKPL131,tvSKPL132,tvSKPL133,tvSKPL134,tvSKPL135,tvSKPL136,tvSKPL137,tvSKPL138,tvSKPL139,tvSKPL140,
            tvSKPL141,tvSKPL142,tvSKPL143,tvSKPL144,tvSKPL145,tvSKPL146, tvSKPL_cash_released_total, tvSKPL_credit_sales_total, tvSKPL_expenses_total;
    //Denomination
    TextView tvSKPL148, tvSKPL149, tvSKPL150, tvSKPL151, tvSKPL152, tvSKPL153, tvSKPL154, tvSKPL155, tvSKPL156, tvSKPL157, tvSKPL_denomination_total;
    //Net Cash
    TextView tvSKPL147;


    private void reSetDOMRTable(){
        String str = getResources().getString(R.string.p_nil);
        tvSKPL001.setText(str);
        tvSKPL002.setText(str);
        tvSKPL003.setText(str);
        tvSKPL004.setText(str);
    }

    private void reSetDailySalesTable(){
        String str = getResources().getString(R.string.p_nil);
        String empty = "";
        tvSKPL005.setText(str);tvSKPL006.setText(empty);tvSKPL007.setText(str);tvSKPL008.setText(empty);
        tvSKPL009.setText(str);tvSKPL010.setText(empty);tvSKPL011.setText(str);tvSKPL012.setText(empty);
        tvSKPL013.setText(str);tvSKPL014.setText(empty);tvSKPL015.setText(str);tvSKPL016.setText(empty);
        tvSKPL017.setText(str);tvSKPL018.setText(empty);tvSKPL019.setText(str);tvSKPL020.setText(empty);
        tvSKPL021.setText(str);tvSKPL022.setText(empty);tvSKPL023.setText(str);tvSKPL024.setText(empty);

        tvSKPL025.setText(str);tvSKPL026.setText(empty);tvSKPL027.setText(str);tvSKPL028.setText(empty);
        tvSKPL029.setText(str);tvSKPL030.setText(empty);tvSKPL031.setText(str);tvSKPL032.setText(empty);
        tvSKPL033.setText(str);tvSKPL034.setText(empty);tvSKPL035.setText(str);tvSKPL036.setText(empty);
        tvSKPL037.setText(str);tvSKPL038.setText(empty);tvSKPL039.setText(str);tvSKPL040.setText(empty);
        tvSKPL041.setText(str);tvSKPL042.setText(empty);tvSKPL043.setText(str);tvSKPL044.setText(empty);

        tvSKPL045.setText(str);tvSKPL046.setText(empty);tvSKPL047.setText(str);tvSKPL048.setText(empty);
        tvSKPL049.setText(str);tvSKPL050.setText(empty);tvSKPL051.setText(str);tvSKPL052.setText(empty);
        tvSKPL053.setText(str);tvSKPL054.setText(empty);tvSKPL055.setText(str);tvSKPL056.setText(empty);
        tvSKPL057.setText(str);tvSKPL058.setText(empty);tvSKPL059.setText(str);tvSKPL060.setText(empty);
        tvSKPL061.setText(str);tvSKPL062.setText(empty);tvSKPL063.setText(str);tvSKPL064.setText(empty);

        tvSKPL065.setText(str);tvSKPL066.setText(empty);tvSKPL067.setText(str);tvSKPL068.setText(empty);
        tvSKPL069.setText(str);tvSKPL070.setText(empty);tvSKPL071.setText(str);tvSKPL072.setText(empty);
        tvSKPL073.setText(str);tvSKPL074.setText(empty);tvSKPL075.setText(str);tvSKPL076.setText(empty);
        tvSKPL077.setText(str);tvSKPL078.setText(empty);tvSKPL079.setText(str);tvSKPL080.setText(empty);
        tvSKPL081.setText(str);tvSKPL082.setText(empty);tvSKPL083.setText(str);tvSKPL084.setText(empty);

        // rate textviews
        tvSKPL084a.setText(empty);tvSKPL084b.setText(empty);tvSKPL084c.setText(empty);tvSKPL084d.setText(empty);
        tvSKPL084e.setText(empty);tvSKPL084f.setText(empty);tvSKPL084g.setText(empty);tvSKPL084h.setText(empty);
        tvSKPL084i.setText(empty);tvSKPL084j.setText(empty);tvSKPL084k.setText(empty);tvSKPL084l.setText(empty);
        tvSKPL084m.setText(empty);tvSKPL084n.setText(empty);tvSKPL084o.setText(empty);tvSKPL084p.setText(empty);
        tvSKPL084q.setText(empty);tvSKPL084r.setText(empty);tvSKPL084s.setText(empty);tvSKPL084t.setText(empty);

        // amount textviews
        tvSKPL084A.setText(empty);tvSKPL084B.setText(empty);tvSKPL084C.setText(empty);tvSKPL084D.setText(empty);
        tvSKPL084E.setText(empty);tvSKPL084F.setText(empty);tvSKPL084G.setText(empty);tvSKPL084H.setText(empty);
        tvSKPL084I.setText(empty);tvSKPL084J.setText(empty);tvSKPL084K.setText(empty);tvSKPL084L.setText(empty);
        tvSKPL084M.setText(empty);tvSKPL084N.setText(empty);tvSKPL084O.setText(empty);tvSKPL084P.setText(empty);
        tvSKPL084Q.setText(empty);tvSKPL084R.setText(empty);tvSKPL084S.setText(empty);tvSKPL084T.setText(empty);

        tvSKPL_DSS_final_total.setText(str);//////////

    }

    private void reSetTankTable(){
        String str = getResources().getString(R.string.p_nil);
        //Petrol Tank1
        tvSKPL085.setText(str);
        tvSKPL086.setText(str);
        tvSKPL087.setText(str);
        tvSKPL088.setText(str);
        tvSKPL089.setText(str);
        tvSKPL090.setText(str);
        tvSKPL090a.setText(str);
        tvSKPL091.setText(str);

        //Diesel Tank1
        tvSKPL092.setText(str);
        tvSKPL093.setText(str);
        tvSKPL094.setText(str);
        tvSKPL095.setText(str);
        tvSKPL096.setText(str);
        tvSKPL097.setText(str);
        tvSKPL097a.setText(str);
        tvSKPL098.setText(str);

        //Diesel Tank2
        tvSKPL099.setText(str);
        tvSKPL100.setText(str);
        tvSKPL101.setText(str);
        tvSKPL102.setText(str);
        tvSKPL103.setText(str);
        tvSKPL104.setText(str);
        tvSKPL104a.setText(str);
        tvSKPL105.setText(str);
    }

    private void reSetPumpTables(){
        String str = getResources().getString(R.string.p_nil);
        //Petrol Pump1
        tvSKPL106.setText(str);
        tvSKPL107.setText(str);
        tvSKPL108.setText(str);
        tvSKPL109.setText(str);
        tvSKPL110.setText(str);

        //Diesel Pump1
        tvSKPL110A.setText(str);
        tvSKPL110J.setText(str);
        tvSKPL110E.setText(str);
        tvSKPL110B.setText(str);
        tvSKPL110H.setText(str);

        //Diesel Pump2
        tvSKPL110F.setText(str);
        tvSKPL110D.setText(str);
        tvSKPL110I.setText(str);
        tvSKPL110C.setText(str);
        tvSKPL110G.setText(str);
    }

    private void reSetSummeryTables(){
        String str = getResources().getString(R.string.p_nil);
        tvSKPL111.setText(str);
        tvSKPL112.setText(str);
        tvSKPL113.setText(str);
        tvSKPL114.setText(str);
        tvSKPL115.setText(str);
        tvSKPL116.setText(str);
        tvSKPL117.setText(str);
        tvSKPL118.setText(str);
        tvSKPL119.setText(str);
        tvSKPL120.setText(str);
        tvSKPL121.setText(str);
        tvSKPL122.setText(str);
        tvSKPL123.setText(str);
        tvSKPL124.setText(str);
        tvSKPL125.setText(str);
        tvSKPL_sales_summery_total.setText(str);

    }

    private void reSetCumulativeSummeryTables(){
        String str = getResources().getString(R.string.p_nil);
        tvSKPL111_Cumulative.setText(str);
        tvSKPL112_Cumulative.setText(str);
        tvSKPL113_Cumulative.setText(str);
        tvSKPL116_Cumulative.setText(str);
        tvSKPL117_Cumulative.setText(str);
        tvSKPL118_Cumulative.setText(str);
        tvSKPL121_Cumulative.setText(str);
        tvSKPL122_Cumulative.setText(str);
        tvSKPL123_Cumulative.setText(str);
        tvSKPL_Cumulative_sales_summery_total.setText(str);

    }

    private void reSetExpenseTables(){
        String str = getResources().getString(R.string.p_nil);
        tvSKPL126.setText(str);
        tvSKPL127.setText(str);
        tvSKPL128.setText(str);
        tvSKPL129.setText(str);
        tvSKPL130.setText(str);
        tvSKPL131.setText(str);
        tvSKPL132.setText(str);
        tvSKPL133.setText(str);
        tvSKPL134.setText(str);
        tvSKPL135.setText(str);
        tvSKPL136.setText(str);
        tvSKPL137.setText(str);
        tvSKPL138.setText(str);
        tvSKPL139.setText(str);
        tvSKPL140.setText(str);
        tvSKPL141.setText(str);
        tvSKPL142.setText(str);
        tvSKPL143.setText(str);
        tvSKPL144.setText(str);
        tvSKPL145.setText(str);
        tvSKPL146.setText(str);
        //Net Cash
        tvSKPL147.setText(str);

        tvSKPL_cash_released_total.setText(str);
        tvSKPL_credit_sales_total.setText(str);
        tvSKPL_expenses_total.setText(str);

    }

    private void reSetDenominationTables(){
        //String str = getResources().getString(R.string.p_nil);
        String str = "";
        tvSKPL148.setText(str);
        tvSKPL149.setText(str);
        tvSKPL150.setText(str);
        tvSKPL151.setText(str);
        tvSKPL152.setText(str);
        tvSKPL153.setText(str);
        tvSKPL154.setText(str);
        tvSKPL155.setText(str);
        tvSKPL156.setText(str);
        tvSKPL157.setText(str);
        tvSKPL_denomination_total.setText(str);

    }

    private void initializeExpenseTables(){

        tvSKPL126 = findViewById(R.id.id_petrol_SKPL126);
        tvSKPL127 = findViewById(R.id.id_petrol_SKPL127);
        tvSKPL128 = findViewById(R.id.id_petrol_SKPL128);
        tvSKPL129 = findViewById(R.id.id_petrol_SKPL129);
        tvSKPL130 = findViewById(R.id.id_petrol_SKPL130);
        tvSKPL131 = findViewById(R.id.id_petrol_SKPL131);
        tvSKPL132 = findViewById(R.id.id_petrol_SKPL132);
        tvSKPL133 = findViewById(R.id.id_petrol_SKPL133);
        tvSKPL134 = findViewById(R.id.id_petrol_SKPL134);
        tvSKPL135 = findViewById(R.id.id_petrol_SKPL135);
        tvSKPL136 = findViewById(R.id.id_petrol_SKPL136);
        tvSKPL137 = findViewById(R.id.id_petrol_SKPL137);
        tvSKPL138 = findViewById(R.id.id_petrol_SKPL138);
        tvSKPL139 = findViewById(R.id.id_petrol_SKPL139);
        tvSKPL140 = findViewById(R.id.id_petrol_SKPL140);
        tvSKPL141 = findViewById(R.id.id_petrol_SKPL141);
        tvSKPL142 = findViewById(R.id.id_petrol_SKPL142);
        tvSKPL143 = findViewById(R.id.id_petrol_SKPL143);
        tvSKPL144 = findViewById(R.id.id_petrol_SKPL144);
        tvSKPL145 = findViewById(R.id.id_petrol_SKPL145);
        tvSKPL146 = findViewById(R.id.id_petrol_SKPL146);
        //Net Cash
        tvSKPL147 = findViewById(R.id.id_petrol_SKPL147);
        tvSKPL_cash_released_total = findViewById(R.id.id_petrol_cash_released_total);
        tvSKPL_credit_sales_total = findViewById(R.id.id_petrol_credit_sales_total);
        tvSKPL_expenses_total = findViewById(R.id.id_petrol_expense_total);


    }

    private void initializeDOMRTable(){
        tvSKPL001 = findViewById(R.id.id_petrol_SKPL001);
        tvSKPL002 = findViewById(R.id.id_petrol_SKPL002);
        tvSKPL003 = findViewById(R.id.id_petrol_SKPL003);
        tvSKPL004 = findViewById(R.id.id_petrol_SKPL004);
    }
    private void initializeDailySalesTable(){
        tvSKPL005 = findViewById(R.id.id_petrol_SKPL005);tvSKPL006 = findViewById(R.id.id_petrol_SKPL006);tvSKPL007 = findViewById(R.id.id_petrol_SKPL007);tvSKPL008 = findViewById(R.id.id_petrol_SKPL008);
        tvSKPL009 = findViewById(R.id.id_petrol_SKPL009);tvSKPL010 = findViewById(R.id.id_petrol_SKPL010);tvSKPL011 = findViewById(R.id.id_petrol_SKPL011);tvSKPL012 = findViewById(R.id.id_petrol_SKPL012);
        tvSKPL013 = findViewById(R.id.id_petrol_SKPL013);tvSKPL014 = findViewById(R.id.id_petrol_SKPL014);tvSKPL015 = findViewById(R.id.id_petrol_SKPL015);tvSKPL016 = findViewById(R.id.id_petrol_SKPL016);
        tvSKPL017 = findViewById(R.id.id_petrol_SKPL017);tvSKPL018 = findViewById(R.id.id_petrol_SKPL018);tvSKPL019 = findViewById(R.id.id_petrol_SKPL019);tvSKPL020 = findViewById(R.id.id_petrol_SKPL020);

        tvSKPL021 = findViewById(R.id.id_petrol_SKPL021);tvSKPL022 = findViewById(R.id.id_petrol_SKPL022);tvSKPL023 = findViewById(R.id.id_petrol_SKPL023);tvSKPL024 = findViewById(R.id.id_petrol_SKPL024);
        tvSKPL025 = findViewById(R.id.id_petrol_SKPL025);tvSKPL026 = findViewById(R.id.id_petrol_SKPL026);tvSKPL027 = findViewById(R.id.id_petrol_SKPL027);tvSKPL028 = findViewById(R.id.id_petrol_SKPL028);
        tvSKPL029 = findViewById(R.id.id_petrol_SKPL029);tvSKPL030 = findViewById(R.id.id_petrol_SKPL030);tvSKPL031 = findViewById(R.id.id_petrol_SKPL031);tvSKPL032 = findViewById(R.id.id_petrol_SKPL032);
        tvSKPL033 = findViewById(R.id.id_petrol_SKPL033);tvSKPL034 = findViewById(R.id.id_petrol_SKPL034);tvSKPL035 = findViewById(R.id.id_petrol_SKPL035);tvSKPL036 = findViewById(R.id.id_petrol_SKPL036);

        tvSKPL037 = findViewById(R.id.id_petrol_SKPL037);tvSKPL038 = findViewById(R.id.id_petrol_SKPL038);tvSKPL039 = findViewById(R.id.id_petrol_SKPL039);tvSKPL040 = findViewById(R.id.id_petrol_SKPL040);
        tvSKPL041 = findViewById(R.id.id_petrol_SKPL041);tvSKPL042 = findViewById(R.id.id_petrol_SKPL042);tvSKPL043 = findViewById(R.id.id_petrol_SKPL043);tvSKPL044 = findViewById(R.id.id_petrol_SKPL044);
        tvSKPL045 = findViewById(R.id.id_petrol_SKPL045);tvSKPL046 = findViewById(R.id.id_petrol_SKPL046);tvSKPL047 = findViewById(R.id.id_petrol_SKPL047);tvSKPL048 = findViewById(R.id.id_petrol_SKPL048);
        tvSKPL049 = findViewById(R.id.id_petrol_SKPL049);tvSKPL050 = findViewById(R.id.id_petrol_SKPL050);tvSKPL051 = findViewById(R.id.id_petrol_SKPL051);tvSKPL052 = findViewById(R.id.id_petrol_SKPL052);

        tvSKPL053 = findViewById(R.id.id_petrol_SKPL053);tvSKPL054 = findViewById(R.id.id_petrol_SKPL054);tvSKPL055 = findViewById(R.id.id_petrol_SKPL055);tvSKPL056 = findViewById(R.id.id_petrol_SKPL056);
        tvSKPL057 = findViewById(R.id.id_petrol_SKPL057);tvSKPL058 = findViewById(R.id.id_petrol_SKPL058);tvSKPL059 = findViewById(R.id.id_petrol_SKPL059);tvSKPL060 = findViewById(R.id.id_petrol_SKPL060);
        tvSKPL061 = findViewById(R.id.id_petrol_SKPL061);tvSKPL062 = findViewById(R.id.id_petrol_SKPL062);tvSKPL063 = findViewById(R.id.id_petrol_SKPL063);tvSKPL064 = findViewById(R.id.id_petrol_SKPL064);
        tvSKPL065 = findViewById(R.id.id_petrol_SKPL065);tvSKPL066 = findViewById(R.id.id_petrol_SKPL066);tvSKPL067 = findViewById(R.id.id_petrol_SKPL067);tvSKPL068 = findViewById(R.id.id_petrol_SKPL068);

        tvSKPL069 = findViewById(R.id.id_petrol_SKPL069);tvSKPL070 = findViewById(R.id.id_petrol_SKPL070);tvSKPL071 = findViewById(R.id.id_petrol_SKPL071);tvSKPL072 = findViewById(R.id.id_petrol_SKPL072);
        tvSKPL073 = findViewById(R.id.id_petrol_SKPL073);tvSKPL074 = findViewById(R.id.id_petrol_SKPL074);tvSKPL075 = findViewById(R.id.id_petrol_SKPL075);tvSKPL076 = findViewById(R.id.id_petrol_SKPL076);
        tvSKPL077 = findViewById(R.id.id_petrol_SKPL077);tvSKPL078 = findViewById(R.id.id_petrol_SKPL078);tvSKPL079 = findViewById(R.id.id_petrol_SKPL079);tvSKPL080 = findViewById(R.id.id_petrol_SKPL080);
        tvSKPL081 = findViewById(R.id.id_petrol_SKPL081);tvSKPL082 = findViewById(R.id.id_petrol_SKPL082);tvSKPL083 = findViewById(R.id.id_petrol_SKPL083);tvSKPL084 = findViewById(R.id.id_petrol_SKPL084);

        tvSKPL084a = findViewById(R.id.id_petrol_SKPL008_rate);tvSKPL084b = findViewById(R.id.id_petrol_SKPL012_rate);tvSKPL084c = findViewById(R.id.id_petrol_SKPL016_rate);tvSKPL084d = findViewById(R.id.id_petrol_SKPL020_rate);
        tvSKPL084e = findViewById(R.id.id_petrol_SKPL024_rate);tvSKPL084f = findViewById(R.id.id_petrol_SKPL028_rate);tvSKPL084g = findViewById(R.id.id_petrol_SKPL032_rate);tvSKPL084h = findViewById(R.id.id_petrol_SKPL036_rate);
        tvSKPL084i = findViewById(R.id.id_petrol_SKPL040_rate);tvSKPL084j = findViewById(R.id.id_petrol_SKPL044_rate);tvSKPL084k = findViewById(R.id.id_petrol_SKPL048_rate);tvSKPL084l = findViewById(R.id.id_petrol_SKPL052_rate);
        tvSKPL084m = findViewById(R.id.id_petrol_SKPL056_rate);tvSKPL084n = findViewById(R.id.id_petrol_SKPL060_rate);tvSKPL084o = findViewById(R.id.id_petrol_SKPL064_rate);tvSKPL084p = findViewById(R.id.id_petrol_SKPL068_rate);
        tvSKPL084q = findViewById(R.id.id_petrol_SKPL072_rate);tvSKPL084r = findViewById(R.id.id_petrol_SKPL076_rate);tvSKPL084s = findViewById(R.id.id_petrol_SKPL080_rate);tvSKPL084t = findViewById(R.id.id_petrol_SKPL084_rate);

        tvSKPL084A = findViewById(R.id.id_petrol_SKPL008_amount);tvSKPL084B = findViewById(R.id.id_petrol_SKPL012_amount);tvSKPL084C = findViewById(R.id.id_petrol_SKPL016_amount);tvSKPL084D = findViewById(R.id.id_petrol_SKPL020_amount);
        tvSKPL084E = findViewById(R.id.id_petrol_SKPL024_amount);tvSKPL084F = findViewById(R.id.id_petrol_SKPL028_amount);tvSKPL084G = findViewById(R.id.id_petrol_SKPL032_amount);tvSKPL084H = findViewById(R.id.id_petrol_SKPL036_amount);
        tvSKPL084I = findViewById(R.id.id_petrol_SKPL040_amount);tvSKPL084J = findViewById(R.id.id_petrol_SKPL044_amount);tvSKPL084K = findViewById(R.id.id_petrol_SKPL048_amount);tvSKPL084L = findViewById(R.id.id_petrol_SKPL052_amount);
        tvSKPL084M = findViewById(R.id.id_petrol_SKPL056_amount);tvSKPL084N = findViewById(R.id.id_petrol_SKPL060_amount);tvSKPL084O = findViewById(R.id.id_petrol_SKPL064_amount);tvSKPL084P = findViewById(R.id.id_petrol_SKPL068_amount);
        tvSKPL084Q = findViewById(R.id.id_petrol_SKPL072_amount);tvSKPL084R = findViewById(R.id.id_petrol_SKPL076_amount);tvSKPL084S = findViewById(R.id.id_petrol_SKPL080_amount);tvSKPL084T = findViewById(R.id.id_petrol_SKPL084_amount);

        tvSKPL_DSS_final_total = findViewById(R.id.id_petrol_DSS_total_amount);
    }

    private void initializeTankTables(){
        //Petrol Tank1
        tvSKPL085 = findViewById(R.id.id_petrol_SKPL085);
        tvSKPL086 = findViewById(R.id.id_petrol_SKPL086);
        tvSKPL087 = findViewById(R.id.id_petrol_SKPL087);
        tvSKPL088 = findViewById(R.id.id_petrol_SKPL088);
        tvSKPL089 = findViewById(R.id.id_petrol_SKPL089);
        tvSKPL090 = findViewById(R.id.id_petrol_SKPL090);
        tvSKPL090a = findViewById(R.id.id_petrol_SKPL090a);
        tvSKPL091 = findViewById(R.id.id_petrol_SKPL091);

        //Diesel Tank1
        tvSKPL092 = findViewById(R.id.id_petrol_SKPL092);
        tvSKPL093 = findViewById(R.id.id_petrol_SKPL093);
        tvSKPL094 = findViewById(R.id.id_petrol_SKPL094);
        tvSKPL095 = findViewById(R.id.id_petrol_SKPL095);
        tvSKPL096 = findViewById(R.id.id_petrol_SKPL096);
        tvSKPL097 = findViewById(R.id.id_petrol_SKPL097);
        tvSKPL097a = findViewById(R.id.id_petrol_SKPL097a);
        tvSKPL098 = findViewById(R.id.id_petrol_SKPL098);

        //Diesel Tank2
        tvSKPL099 = findViewById(R.id.id_petrol_SKPL099);
        tvSKPL100 = findViewById(R.id.id_petrol_SKPL100);
        tvSKPL101 = findViewById(R.id.id_petrol_SKPL101);
        tvSKPL102 = findViewById(R.id.id_petrol_SKPL102);
        tvSKPL103 = findViewById(R.id.id_petrol_SKPL103);
        tvSKPL104 = findViewById(R.id.id_petrol_SKPL104);
        tvSKPL104a = findViewById(R.id.id_petrol_SKPL104a);
        tvSKPL105 = findViewById(R.id.id_petrol_SKPL105);
    }

    private void initializePumpTables(){
        //Petrol Pump1
        tvSKPL106 = findViewById(R.id.id_petrol_SKPL106);
        tvSKPL107 = findViewById(R.id.id_petrol_SKPL107);
        tvSKPL108 = findViewById(R.id.id_petrol_SKPL108);
        tvSKPL109 = findViewById(R.id.id_petrol_SKPL109);
        tvSKPL110 = findViewById(R.id.id_petrol_SKPL110);

        //Diesel Pump1
        tvSKPL110A = findViewById(R.id.id_petrol_SKPL110A);
        tvSKPL110J = findViewById(R.id.id_petrol_SKPL110J);
        tvSKPL110E = findViewById(R.id.id_petrol_SKPL110E);
        tvSKPL110B = findViewById(R.id.id_petrol_SKPL110B);
        tvSKPL110H = findViewById(R.id.id_petrol_SKPL110H);

        //Diesel Pump2
        tvSKPL110F = findViewById(R.id.id_petrol_SKPL110F);
        tvSKPL110D = findViewById(R.id.id_petrol_SKPL110D);
        tvSKPL110I = findViewById(R.id.id_petrol_SKPL110I);
        tvSKPL110C = findViewById(R.id.id_petrol_SKPL110C);
        tvSKPL110G = findViewById(R.id.id_petrol_SKPL110G);
    }


    private void initializeSummeryTables(){
        tvSKPL111 = findViewById(R.id.id_petrol_SKPL111);
        tvSKPL112 = findViewById(R.id.id_petrol_SKPL112);
        tvSKPL113 = findViewById(R.id.id_petrol_SKPL113);
        tvSKPL114 = findViewById(R.id.id_petrol_SKPL114);
        tvSKPL115 = findViewById(R.id.id_petrol_SKPL115);
        tvSKPL116 = findViewById(R.id.id_petrol_SKPL116);
        tvSKPL117 = findViewById(R.id.id_petrol_SKPL117);
        tvSKPL118 = findViewById(R.id.id_petrol_SKPL118);
        tvSKPL119 = findViewById(R.id.id_petrol_SKPL119);
        tvSKPL120 = findViewById(R.id.id_petrol_SKPL120);
        tvSKPL121 = findViewById(R.id.id_petrol_SKPL121);
        tvSKPL122 = findViewById(R.id.id_petrol_SKPL122);
        tvSKPL123 = findViewById(R.id.id_petrol_SKPL123);
        tvSKPL124 = findViewById(R.id.id_petrol_SKPL124);
        tvSKPL125 = findViewById(R.id.id_petrol_SKPL125);
        tvSKPL_sales_summery_total = findViewById(R.id.id_petrol_sales_summery_total_amount);
    }

    private void initializeCumulativeSummeryTables(){
        tvSKPL111_Cumulative = findViewById(R.id.id_petrol_SKPL111_Cumulative);
        tvSKPL112_Cumulative = findViewById(R.id.id_petrol_SKPL112_Cumulative);
        tvSKPL113_Cumulative = findViewById(R.id.id_petrol_SKPL113_Cumulative);
        tvSKPL116_Cumulative = findViewById(R.id.id_petrol_SKPL116_Cumulative);
        tvSKPL117_Cumulative = findViewById(R.id.id_petrol_SKPL117_Cumulative);
        tvSKPL118_Cumulative = findViewById(R.id.id_petrol_SKPL118_Cumulative);
        tvSKPL121_Cumulative = findViewById(R.id.id_petrol_SKPL121_Cumulative);
        tvSKPL122_Cumulative = findViewById(R.id.id_petrol_SKPL122_Cumulative);
        tvSKPL123_Cumulative = findViewById(R.id.id_petrol_SKPL123_Cumulative);
        tvSKPL_Cumulative_sales_summery_total = findViewById(R.id.id_petrol_Cumulative_sales_summery_total_amount);
    }


    private void initializeDenominationTable(){

        tvSKPL148 = findViewById(R.id.id_petrol_SKPL148);
        tvSKPL149 = findViewById(R.id.id_petrol_SKPL149);
        tvSKPL150 = findViewById(R.id.id_petrol_SKPL150);
        tvSKPL151 = findViewById(R.id.id_petrol_SKPL151);
        tvSKPL152 = findViewById(R.id.id_petrol_SKPL152);
        tvSKPL153 = findViewById(R.id.id_petrol_SKPL153);
        tvSKPL154 = findViewById(R.id.id_petrol_SKPL154);
        tvSKPL155 = findViewById(R.id.id_petrol_SKPL155);
        tvSKPL156 = findViewById(R.id.id_petrol_SKPL156);
        tvSKPL157 = findViewById(R.id.id_petrol_SKPL157);
        tvSKPL_denomination_total = findViewById(R.id.id_petrol_denominations_total);

    }


    @Override
    public void onStart() {
        super.onStart();
        updateUserId();
    }

    private void reSetAllTable(){
        reSetDOMRTable();
        reSetDailySalesTable();
        reSetTankTable();
        reSetPumpTables();

        reSetSummeryTables();
        reSetCumulativeSummeryTables();

        reSetExpenseTables();
        reSetDenominationTables();

    }

    private void initializeAllTables(){
        initializeDOMRTable();
        initializeDailySalesTable();
        initializeTankTables();
        initializePumpTables();

        initializeSummeryTables();
        initializeCumulativeSummeryTables();

        initializeExpenseTables();
        initializeDenominationTable();
    }

    private void formVerticalText(){

        TextView tvDailySales = findViewById(R.id.id_tv_vertical_daily_sales);
        TextView tvOP_stock = findViewById(R.id.id_tv_vertical_op_stock);
        TextView tvReceipt = findViewById(R.id.id_tv_vertical_receipt);
        TextView tvCL_stock = findViewById(R.id.id_tv_vertical_cl_stock);
        TextView tvRate = findViewById(R.id.id_tv_vertical_rate);
        TextView tvAmount = findViewById(R.id.id_tv_vertical_amount);

        String dailySales = getResources().getString(R.string.p_daily_sales);
        StringBuilder strParticulars = new StringBuilder();
        for(int i = 0; i < dailySales.length(); i++){
            strParticulars.append(dailySales.charAt(i));
            strParticulars.append("\n");
        }
        tvDailySales.setText(strParticulars.toString());
        //////////OP Stock
        String op_stock = getResources().getString(R.string.p_op_stock);
        StringBuilder strOP_stock = new StringBuilder();
        for(int i = 0; i < op_stock.length(); i++){
            strOP_stock.append(op_stock.charAt(i));
            strOP_stock.append("\n");
        }
        tvOP_stock.setText(strOP_stock.toString());
        //////////receipt
        String receipt = getResources().getString(R.string.p_receipt);
        StringBuilder strReceipt = new StringBuilder();
        for(int i = 0; i < receipt.length(); i++){
            strReceipt.append(receipt.charAt(i));
            strReceipt.append("\n");
        }
        tvReceipt.setText(strReceipt.toString());
        //////////closing Stock
        String closing = getResources().getString(R.string.p_cl_stock);
        StringBuilder strClosingStk = new StringBuilder();
        for(int i = 0; i < closing.length(); i++){
            strClosingStk.append(closing.charAt(i));
            strClosingStk.append("\n");
        }
        tvCL_stock.setText(strClosingStk.toString());
        //////////rate
        String rate = getResources().getString(R.string.p_rate);
        StringBuilder strRate = new StringBuilder();
        for(int i = 0; i < rate.length(); i++){
            strRate.append(rate.charAt(i));
            strRate.append("\n");
        }
        tvRate.setText(strRate.toString());
        //////////amount
        String amount = getResources().getString(R.string.p_amount);
        StringBuilder strAmount = new StringBuilder();
        for(int i = 0; i < amount.length(); i++){
            strAmount.append(amount.charAt(i));
            strAmount.append("\n");
        }
        tvAmount.setText(strAmount.toString());

        /////Summery table
        TextView tvTotalLiters = findViewById(R.id.id_vertical_total_liters);
        TextView tvRate_summery = findViewById(R.id.id_vertical_rate_summery);
        TextView tvAmountSummery = findViewById(R.id.id_vertical_amount_summery);
        //////////total liters
        String liters = getResources().getString(R.string.p_total_liters);
        StringBuilder strLiters = new StringBuilder();
        for(int i = 0; i < liters.length(); i++){
            strLiters.append(liters.charAt(i));
            strLiters.append("\n");
        }
        tvTotalLiters.setText(strLiters.toString());
        //////////rate summery
        String rate1 = getResources().getString(R.string.rate);
        StringBuilder strRate1 = new StringBuilder();
        for(int i = 0; i < rate1.length(); i++){
            strRate1.append(rate1.charAt(i));
            strRate1.append("\n");
        }
        tvRate_summery.setText(strRate1.toString());
        //////////amount summery
        String amount1 = getResources().getString(R.string.p_amount_);
        StringBuilder strAmount1 = new StringBuilder();
        for(int i = 0; i < amount1.length(); i++){
            strAmount1.append(amount1.charAt(i));
            strAmount1.append("\n");
        }
        tvAmountSummery.setText(strAmount1.toString());

        /////Cumulative Summery table
        TextView tvTotalLiters_Cumulative = findViewById(R.id.id_vertical_total_liters_Cumulative);
        TextView tvRate_summery_Cumulative = findViewById(R.id.id_vertical_rate_summery_Cumulative);
        TextView tvAmountSummery_Cumulative = findViewById(R.id.id_vertical_amount_summery_Cumulative);
        //////////total liters
        String liters_Cumulative = getResources().getString(R.string.p_total_liters);
        StringBuilder strLiters_Cumulative = new StringBuilder();
        for(int i = 0; i < liters_Cumulative.length(); i++){
            strLiters_Cumulative.append(liters.charAt(i));
            strLiters_Cumulative.append("\n");
        }
        tvTotalLiters_Cumulative.setText(strLiters_Cumulative.toString());
        //////////rate summery
        String rate1_Cumulative = getResources().getString(R.string.rate);
        StringBuilder strRate1_Cumulative = new StringBuilder();
        for(int i = 0; i < rate1_Cumulative.length(); i++){
            strRate1_Cumulative.append(rate1.charAt(i));
            strRate1_Cumulative.append("\n");
        }
        tvRate_summery_Cumulative.setText(strRate1_Cumulative.toString());
        //////////amount summery
        String amount1_Cumulative = getResources().getString(R.string.p_amount_);
        StringBuilder strAmount1_Cumulative = new StringBuilder();
        for(int i = 0; i < amount1_Cumulative.length(); i++){
            strAmount1_Cumulative.append(amount1.charAt(i));
            strAmount1_Cumulative.append("\n");
        }
        tvAmountSummery_Cumulative.setText(strAmount1_Cumulative.toString());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_petrol_bunk);
        lineHeaderProgress = findViewById(R.id.lineHeaderProgress_manager_petrol_bunk);
        txtDate = findViewById(R.id.id_manager_client_select_date);
        scrollViewTable = findViewById(R.id.id_SV_petrol_bunk_root);
        btnViewSales = findViewById(R.id.id_petrol_view_sales);
        btnViewSales.setOnClickListener(viewSalesListener);
        ///////////////////////////////////
        bunkModelList = new ArrayList<>();
        dailySalesList = new LinkedHashMap<>();
        dailySalesRateList = new LinkedHashMap<>();
        summeryAmountList = new LinkedHashMap<>();
        summeryCumulativeAmountList = new LinkedHashMap<>();
        cashReleasedList = new LinkedHashMap<>();
        creditSalesList = new LinkedHashMap<>();
        expensestList = new LinkedHashMap<>();
        denominationList = new LinkedHashMap<>();
        ////////////tablesHide();
        formVerticalText();
        dateSetting();
        updateUserId();

        initializeAllTables();
    }

    private void dateSetting() {
        ///Default date setting
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        String formattedDate = df.format(c);
        txtDate.setText(formattedDate);
        strSelectedDate = formattedDate;

        txtDate.setOnClickListener(selectDate);
    }

    private void reSetAllList(){
        bunkModelList.clear();
        dailySalesList.clear();
        dailySalesRateList.clear();
        summeryAmountList.clear();
        summeryCumulativeAmountList.clear();
        cashReleasedList.clear();
        creditSalesList.clear();
        expensestList.clear();
        denominationList.clear();
    }

    View.OnClickListener viewSalesListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            show();
            reSetAllTable();///////////////////////////
            reSetAllList();

            JSONStringer jsonStringer = null;
            String dateForRequest = "";
            try {
                Log.e("Selected",".........................................Date:" + strSelectedDate);
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
                SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                Date newDate = format.parse(strSelectedDate);
                Log.e("Date",".........................................." + newDate);
                dateForRequest = format1.format(newDate);
            }catch (Exception e){
                e.printStackTrace();
                showAlertDialogToast("Date Format Wrong : " + e.getMessage());
                return;
            }

            try{

                jsonStringer = new JSONStringer().object()
                        .key("duserid").value(Constants.DUSER_ID)
                        .key("mode").value("WEB")
                        .key("usertype").value("D")
                        .key("status")
                        .array()
                        .value("Pending")
                        .endArray()
                        .key("fromdt").value(dateForRequest)
                        .key("todt").value(dateForRequest)
                        .endObject();

                JsonServiceHandler jsonServiceHandler = new JsonServiceHandler(Utils.strManagerSalesman, jsonStringer.toString(), PetrolBunkActivity.this);
                JsonServiceHandler jsonServiceHandlerGet = new JsonServiceHandler(PetrolBunkActivity.this);
                new LoadOnlineOrderForManager.LoadOrderByDateAsyncTask(jsonServiceHandler,
                        jsonServiceHandlerGet,
                        Constants.DUSER_ID,
                        new LoadOnlineOrderForManager.OrderCompleteListener() {
                    @Override
                    public void onLoadFinished(JSONArray result) {
                        android.util.Log.e(TAG,"......final Result..." + result.toString());
                        try {
                            for (int i = 0; i < result.length(); i++) {
                                PetrolBunkModel obj = new PetrolBunkModel();
                                JSONObject jsonObject = result.getJSONObject(i);
                                JSONObject jsonObjectProduct = jsonObject.getJSONObject("product");
                                String qty = jsonObject.getString("qty");
                                String productCode = jsonObjectProduct.getString("prodcode");
                                obj.setProductCode(productCode);
                                obj.setQty(qty);
                                bunkModelList.add(obj);
                                ///////////////////////////////
                                setValueOnTable(obj);
                                ////////////////////////////////

                                setCumulativeSummery(obj);

                                ////////////////////////

                            }
                            //Final Calculation
                            float totalSales = 0f;
                            for (Map.Entry<String, Float> entry : dailySalesList.entrySet()) {

                                float currentSales = 0f;
                                String key = entry.getKey();
                                float value = entry.getValue();
                                // now work with key and value...
                                Float currentValue = dailySalesRateList.get(key);
                                if(currentValue != null){
                                    currentSales = value * currentValue;
                                    totalSales = totalSales + currentSales;
                                    ///////////////////////////////////////
                                    setCurrentSalesAmount(key,currentSales);
                                    //////////////////////////////////////
                                }
                            }
                            ///////////////////////////////////////total sum
                            if(totalSales > 0) {
                                setCurrentSalesAmount("SKPL084_DSS_TOTAL", totalSales);
                            }

                            //////////////////////////////////////

                            //////Sales Summery
                            float salesSummeryTotal = 0f;
                            for (Map.Entry<String, Float> entry : summeryAmountList.entrySet()) {

                                String key = entry.getKey();
                                float value = entry.getValue();
                                salesSummeryTotal = salesSummeryTotal + value;
                            }
                            if(salesSummeryTotal > 0){
                                String strSummeryTotal = String.format(Locale.getDefault(),"%.2f", salesSummeryTotal);
                                tvSKPL_sales_summery_total.setText(getFormattedAmounts(strSummeryTotal));
                            }
                            /////////////////////////////////////

                            //////Cumulative Sales Summery
                            float cumSalesSummeryTotal = 0f;
                            for (Map.Entry<String, Float> entry : summeryCumulativeAmountList.entrySet()) {

                                String key = entry.getKey();
                                float value = entry.getValue();
                                cumSalesSummeryTotal = cumSalesSummeryTotal + value;
                            }
                            if(cumSalesSummeryTotal > 0){
                                String strSummeryTotal = String.format(Locale.getDefault(),"%.2f", cumSalesSummeryTotal);
                                tvSKPL_Cumulative_sales_summery_total.setText(getFormattedAmounts(strSummeryTotal));
                            }

                            ////////////////////////////////////

                            //////////////////////////Cash Released
                            float cashReleasedTotal = 0f;
                            for (Map.Entry<String, Float> entry : cashReleasedList.entrySet()) {

                                String key = entry.getKey();
                                float value = entry.getValue();
                                cashReleasedTotal = cashReleasedTotal + value;
                            }
                            if(cashReleasedTotal > 0){
                                String strCashReleasedTotal = String.format(Locale.getDefault(),"%.2f", cashReleasedTotal);
                                tvSKPL_cash_released_total.setText(getFormattedAmounts(strCashReleasedTotal));
                            }
                            //////////////////////////Credit Sales
                            float creditSalesTotal = 0f;
                            for (Map.Entry<String, Float> entry : creditSalesList.entrySet()) {

                                String key = entry.getKey();
                                float value = entry.getValue();
                                creditSalesTotal = creditSalesTotal + value;
                            }
                            if(creditSalesTotal > 0){
                                String strCreditSalesTotal = String.format(Locale.getDefault(),"%.2f", creditSalesTotal);
                                tvSKPL_credit_sales_total.setText(getFormattedAmounts(strCreditSalesTotal));
                            }
                            //////////////////////////expenses
                            float expensesTotal = 0f;
                            for (Map.Entry<String, Float> entry : expensestList.entrySet()) {

                                String key = entry.getKey();
                                float value = entry.getValue();
                                expensesTotal = expensesTotal + value;
                            }
                            if(expensesTotal > 0){
                                String strExpensesTotal = String.format(Locale.getDefault(),"%.2f", expensesTotal);
                                tvSKPL_expenses_total.setText(getFormattedAmounts(strExpensesTotal));
                            }
                            //////////////////////////Denomination
                            float denominationTotal = 0f;
                            for (Map.Entry<String, Float> entry : denominationList.entrySet()) {

                                String key = entry.getKey();
                                float value = entry.getValue();
                                denominationTotal = denominationTotal + value;
                            }
                            if(denominationTotal > 0){
                                String strDenominationTotal = String.format(Locale.getDefault(),"%.2f", denominationTotal);
                                tvSKPL_denomination_total.setText(strDenominationTotal);
                            }
                            //////////////////////////


                        }catch (Exception e){
                            e.printStackTrace();
                        }finally {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    hide();
                                }
                            });
                        }
                    }

                    @Override
                    public void onLoadFailed(String msg) {
                        showAlertDialogToast(msg);
                        hide();
                    }

                    @Override
                    public void onError(String msg) {
                        showAlertDialogToast("Unexpected Happen : " + msg);
                        hide();
                    }

                }).execute();

            }catch (Exception e){
               e.printStackTrace();
                showAlertDialogToast("Unexpected Happen : " + e.getMessage());
                return;
            }

        }
    };

    private void setCurrentSalesAmount(String code, float amount){

        String qty = String.format(Locale.getDefault(),"%.2f", amount);

        qty = getFormattedAmounts(qty);

        switch (code){
            case "SKPL084A" :
                tvSKPL084A.setText(qty);
                break;
            case "SKPL084B" :
                tvSKPL084B.setText(qty);
                break;
            case "SKPL084C" :
                tvSKPL084C.setText(qty);
                break;
            case "SKPL084D" :
                tvSKPL084D.setText(qty);
                break;
            case "SKPL084E" :
                tvSKPL084E.setText(qty);
                break;
            case "SKPL084F" :
                tvSKPL084F.setText(qty);
                break;
            case "SKPL084G" :
                tvSKPL084G.setText(qty);
                break;
            case "SKPL084H" :
                tvSKPL084H.setText(qty);
                break;
            case "SKPL084I" :
                tvSKPL084I.setText(qty);
                break;
            case "SKPL084J" :
                tvSKPL084J.setText(qty);
                break;
            case "SKPL084K" :
                tvSKPL084K.setText(qty);
                break;
            case "SKPL084L" :
                tvSKPL084L.setText(qty);
                break;
            case "SKPL084M" :
                tvSKPL084M.setText(qty);
                break;
            case "SKPL084N" :
                tvSKPL084N.setText(qty);
                break;
            case "SKPL084O" :
                tvSKPL084O.setText(qty);
                break;
            case "SKPL084P" :
                tvSKPL084P.setText(qty);
                break;
            case "SKPL084Q" :
                tvSKPL084Q.setText(qty);
                break;
            case "SKPL084R" :
                tvSKPL084R.setText(qty);
                break;
            case "SKPL084S" :
                tvSKPL084S.setText(qty);
                break;
            case "SKPL084T" :
                tvSKPL084T.setText(qty);
                break;
            //DSS Total
            case "SKPL084_DSS_TOTAL" :
                tvSKPL_DSS_final_total.setText(qty);
                break;
        }

    }

    private String getFormattedAmountsInt(String amounts){

        try{
            if(amounts == null || amounts.isEmpty()) {
                return "";
            }
            NumberFormat nf = NumberFormat.getCurrencyInstance(new Locale("en", "in"));
            DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat) nf).getDecimalFormatSymbols();
            decimalFormatSymbols.setCurrencySymbol("");
            nf.setMaximumFractionDigits(0);
            ((DecimalFormat) nf).setDecimalFormatSymbols(decimalFormatSymbols);
            String str = (nf.format(new BigDecimal(amounts)).trim());
            return  str.replaceAll("\\s+","");

        }catch (Exception e){
            e.printStackTrace();
            return "";
        }
    }

    private String getFormattedAmounts(String amounts){  //formatter.setMaximumFractionDigits(2);

        try{
            if(amounts == null || amounts.isEmpty()) {
                return "";
            }
            NumberFormat nf = NumberFormat.getCurrencyInstance(new Locale("en", "in"));
            DecimalFormatSymbols decimalFormatSymbols = ((DecimalFormat) nf).getDecimalFormatSymbols();
            decimalFormatSymbols.setCurrencySymbol("");
            ((DecimalFormat) nf).setDecimalFormatSymbols(decimalFormatSymbols);
            String str = (nf.format(new BigDecimal(amounts)).trim());
            return  str.replaceAll("\\s+","");

        }catch (Exception e){
            e.printStackTrace();
            return "";
        }
    }

    private void setCumulativeSummery(PetrolBunkModel model){
        String qty_in = model.getQty();
        Float fQty = Float.parseFloat(qty_in);
        String qty = getFormattedAmountsInt(qty_in);

        String qtyF = getFormattedAmounts(qty_in);

        String code = model.getProductCode();

        switch (code){
            //Sales Summery
            case "SKPL111" :
                tvSKPL111_Cumulative.setText(qtyF);
                break;
            case "SKPL116" :
                tvSKPL116_Cumulative.setText(qtyF);
                break;
            case "SKPL121" :
                tvSKPL121_Cumulative.setText(qtyF);
                break;

            case "SKPL112" :
                tvSKPL112_Cumulative.setText(qtyF);
                break;
            case "SKPL117" :
                tvSKPL117_Cumulative.setText(qtyF);
                break;
            case "SKPL122" :
                tvSKPL122_Cumulative.setText(qtyF);
                break;

            case "SKPL113" :
                tvSKPL113_Cumulative.setText(qtyF);
                summeryCumulativeAmountList.put("SKPL113",fQty);
                break;
            case "SKPL118" :
                tvSKPL118_Cumulative.setText(qtyF);
                summeryCumulativeAmountList.put("SKPL118",fQty);
                break;
            case "SKPL123" :
                tvSKPL123_Cumulative.setText(qtyF);
                summeryCumulativeAmountList.put("SKPL123",fQty);
                break;
        }

    }

    private void setValueOnTable(PetrolBunkModel model){

        String qty_in = model.getQty();
        Float fQty = Float.parseFloat(qty_in);
        String qty = getFormattedAmountsInt(qty_in);

        String qtyF = getFormattedAmounts(qty_in);

        String code = model.getProductCode();

        switch (code){

            case "SKPL001" :
                 tvSKPL001.setText(qty);
                 break;
            case "SKPL002" :
                tvSKPL002.setText(qty);
                break;
            case "SKPL003" :
                tvSKPL001.setText(qty);
                break;
            case "SKPL004" :
                tvSKPL001.setText(qty);
                break;
            case "SKPL005" :
                tvSKPL005.setText(qty);
                break;
            case "SKPL006" :
                tvSKPL006.setText(qty);
                break;
            case "SKPL007" :
                tvSKPL007.setText(qty);
                break;
            case "SKPL008" :
                tvSKPL008.setText(qty);
                dailySalesList.put("SKPL008",fQty);
                break;
            case "SKPL009" :
                tvSKPL009.setText(qty);
                break;
            case "SKPL010" :
                tvSKPL010.setText(qty);
                break;
            case "SKPL011" :
                tvSKPL011.setText(qty);
                break;
            case "SKPL012" :
                tvSKPL012.setText(qty);
                dailySalesList.put("SKPL012",fQty);
                break;
            case "SKPL013" :
                tvSKPL013.setText(qty);
                break;
            case "SKPL014" :
                tvSKPL014.setText(qty);
                break;
            case "SKPL015" :
                tvSKPL015.setText(qty);
                break;
            case "SKPL016" :
                tvSKPL016.setText(qty);
                dailySalesList.put("SKPL016",fQty);
                break;
            case "SKPL017" :
                tvSKPL017.setText(qty);
                break;
            case "SKPL018" :
                tvSKPL018.setText(qty);
                break;
            case "SKPL019" :
                tvSKPL019.setText(qty);
                break;
            case "SKPL020" :
                tvSKPL020.setText(qty);
                dailySalesList.put("SKPL020",fQty);
                break;
            case "SKPL021" :
                tvSKPL021.setText(qty);
                break;
            case "SKPL022" :
                tvSKPL022.setText(qty);
                break;
            case "SKPL023" :
                tvSKPL023.setText(qty);
                break;
            case "SKPL024" :
                tvSKPL024.setText(qty);
                dailySalesList.put("SKPL024",fQty);
                break;
            case "SKPL025" :
                tvSKPL025.setText(qty);
                break;
            case "SKPL026" :
                tvSKPL026.setText(qty);
                break;
            case "SKPL027" :
                tvSKPL027.setText(qty);
                break;
            case "SKPL028" :
                tvSKPL028.setText(qty);
                dailySalesList.put("SKPL028",fQty);
                break;

            case "SKPL029" :
                tvSKPL029.setText(qty);
                break;
            case "SKPL030" :
                tvSKPL030.setText(qty);
                break;
            case "SKPL031" :
                tvSKPL031.setText(qty);
                break;
            case "SKPL032" :
                tvSKPL032.setText(qty);
                dailySalesList.put("SKPL032",fQty);
                break;

            case "SKPL033" :
                tvSKPL033.setText(qty);
                break;
            case "SKPL034" :
                tvSKPL034.setText(qty);
                break;
            case "SKPL035" :
                tvSKPL035.setText(qty);
                break;
            case "SKPL036" :
                tvSKPL036.setText(qty);
                dailySalesList.put("SKPL036",fQty);
                break;

            case "SKPL037" :
                tvSKPL037.setText(qty);
                break;
            case "SKPL038" :
                tvSKPL038.setText(qty);
                break;
            case "SKPL039" :
                tvSKPL039.setText(qty);
                break;
            case "SKPL040" :
                tvSKPL040.setText(qty);
                dailySalesList.put("SKPL040",fQty);
                break;

            case "SKPL041" :
                tvSKPL041.setText(qty);
                break;
            case "SKPL042" :
                tvSKPL042.setText(qty);
                break;
            case "SKPL043" :
                tvSKPL043.setText(qty);
                break;
            case "SKPL044" :
                tvSKPL044.setText(qty);
                dailySalesList.put("SKPL044",fQty);
                break;

            case "SKPL045" :
                tvSKPL045.setText(qty);
                break;
            case "SKPL046" :
                tvSKPL046.setText(qty);
                break;
            case "SKPL047" :
                tvSKPL047.setText(qty);
                break;
            case "SKPL048" :
                tvSKPL048.setText(qty);
                dailySalesList.put("SKPL048",fQty);
                break;

            case "SKPL049" :
                tvSKPL049.setText(qty);
                break;
            case "SKPL050" :
                tvSKPL050.setText(qty);
                break;
            case "SKPL051" :
                tvSKPL051.setText(qty);
                break;
            case "SKPL052" :
                tvSKPL052.setText(qty);
                dailySalesList.put("SKPL052",fQty);
                break;

            case "SKPL053" :
                tvSKPL053.setText(qty);
                break;
            case "SKPL054" :
                tvSKPL054.setText(qty);
                break;
            case "SKPL055" :
                tvSKPL055.setText(qty);
                break;
            case "SKPL056" :
                tvSKPL056.setText(qty);
                dailySalesList.put("SKPL056",fQty);
                break;

            case "SKPL057" :
                tvSKPL057.setText(qty);
                break;
            case "SKPL058" :
                tvSKPL058.setText(qty);
                break;
            case "SKPL059" :
                tvSKPL059.setText(qty);
                break;
            case "SKPL060" :
                tvSKPL060.setText(qty);
                dailySalesList.put("SKPL060",fQty);
                break;

            case "SKPL061" :
                tvSKPL061.setText(qty);
                break;
            case "SKPL062" :
                tvSKPL062.setText(qty);
                break;
            case "SKPL063" :
                tvSKPL063.setText(qty);
                break;
            case "SKPL064" :
                tvSKPL064.setText(qty);
                dailySalesList.put("SKPL064",fQty);
                break;

            case "SKPL065" :
                tvSKPL065.setText(qty);
                break;
            case "SKPL066" :
                tvSKPL066.setText(qty);
                break;
            case "SKPL067" :
                tvSKPL067.setText(qty);
                break;
            case "SKPL068" :
                tvSKPL068.setText(qty);
                dailySalesList.put("SKPL068",fQty);
                break;

            case "SKPL069" :
                tvSKPL069.setText(qty);
                break;
            case "SKPL070" :
                tvSKPL070.setText(qty);
                break;
            case "SKPL071" :
                tvSKPL071.setText(qty);
                break;
            case "SKPL072" :
                tvSKPL072.setText(qty);
                dailySalesList.put("SKPL072",fQty);
                break;

            case "SKPL073" :
                tvSKPL073.setText(qty);
                break;
            case "SKPL074" :
                tvSKPL074.setText(qty);
                break;
            case "SKPL075" :
                tvSKPL075.setText(qty);
                break;
            case "SKPL076" :
                tvSKPL076.setText(qty);
                dailySalesList.put("SKPL076",fQty);
                break;

            case "SKPL077" :
                tvSKPL077.setText(qty);
                break;
            case "SKPL078" :
                tvSKPL078.setText(qty);
                break;
            case "SKPL079" :
                tvSKPL079.setText(qty);
                break;
            case "SKPL080" :
                tvSKPL080.setText(qty);
                dailySalesList.put("SKPL080",fQty);
                break;

            case "SKPL081" :
                tvSKPL081.setText(qty);
                break;
            case "SKPL082" :
                tvSKPL082.setText(qty);
                break;
            case "SKPL083" :
                tvSKPL083.setText(qty);
                break;
            case "SKPL084" :
                tvSKPL084.setText(qty);
                dailySalesList.put("SKPL084",fQty);
                break;

            case "SKPL084a" :
                tvSKPL084a.setText(qtyF);
                dailySalesRateList.put("SKPL008",fQty);
                break;
            case "SKPL084b" :
                tvSKPL084b.setText(qtyF);
                dailySalesRateList.put("SKPL012",fQty);
                break;
            case "SKPL084c" :
                tvSKPL084c.setText(qtyF);
                dailySalesRateList.put("SKPL016",fQty);
                break;
            case "SKPL084d" :
                tvSKPL084d.setText(qtyF);
                dailySalesRateList.put("SKPL020",fQty);
                break;

            case "SKPL084e" :
                tvSKPL084e.setText(qtyF);
                dailySalesRateList.put("SKPL024",fQty);
                break;
            case "SKPL084f" :
                tvSKPL084f.setText(qtyF);
                dailySalesRateList.put("SKPL028",fQty);
                break;
            case "SKPL084g" :
                tvSKPL084g.setText(qtyF);
                dailySalesRateList.put("SKPL032",fQty);
                break;
            case "SKPL084h" :
                tvSKPL084h.setText(qtyF);
                dailySalesRateList.put("SKPL036",fQty);
                break;

            case "SKPL084i" :
                tvSKPL084i.setText(qtyF);
                dailySalesRateList.put("SKPL040",fQty);
                break;
            case "SKPL084j" :
                tvSKPL084j.setText(qtyF);
                dailySalesRateList.put("SKPL044",fQty);
                break;
            case "SKPL084k" :
                tvSKPL084k.setText(qtyF);
                dailySalesRateList.put("SKPL048",fQty);
                break;
            case "SKPL084l" :
                tvSKPL084l.setText(qtyF);
                dailySalesRateList.put("SKPL052",fQty);
                break;

            case "SKPL084m" :
                tvSKPL084m.setText(qtyF);
                dailySalesRateList.put("SKPL056",fQty);
                break;
            case "SKPL084n" :
                tvSKPL084n.setText(qtyF);
                dailySalesRateList.put("SKPL060",fQty);
                break;
            case "SKPL084o" :
                tvSKPL084o.setText(qtyF);
                dailySalesRateList.put("SKPL064",fQty);
                break;
            case "SKPL084p" :
                tvSKPL084p.setText(qtyF);
                dailySalesRateList.put("SKPL068",fQty);
                break;

            case "SKPL084q" :
                tvSKPL084q.setText(qtyF);
                dailySalesRateList.put("SKPL072",fQty);
                break;
            case "SKPL084r" :
                tvSKPL084r.setText(qtyF);
                dailySalesRateList.put("SKPL076",fQty);
                break;
            case "SKPL084s" :
                tvSKPL084s.setText(qtyF);
                dailySalesRateList.put("SKPL080",fQty);
                break;
            case "SKPL084t" :
                tvSKPL084t.setText(qtyF);
                dailySalesRateList.put("SKPL084",fQty);
                break;

            case "SKPL085" :
                tvSKPL085.setText(qty);
                break;
            case "SKPL086" :
                tvSKPL086.setText(qty);
                break;
            case "SKPL087" :
                tvSKPL087.setText(qty);
                break;
            case "SKPL088" :
                tvSKPL088.setText(qty);
                break;
            case "SKPL089" :
                tvSKPL089.setText(qty);
                break;
            case "SKPL090" :
                tvSKPL090.setText(qty);
                break;
            case "SKPL090a" :
                tvSKPL090a.setText(qty);
                break;
            case "SKPL091" :
                tvSKPL091.setText(qty);
                break;

            case "SKPL092" :
                tvSKPL092.setText(qty);
                break;
            case "SKPL093" :
                tvSKPL093.setText(qty);
                break;
            case "SKPL094" :
                tvSKPL094.setText(qty);
                break;
            case "SKPL095" :
                tvSKPL095.setText(qty);
                break;
            case "SKPL096" :
                tvSKPL096.setText(qty);
                break;
            case "SKPL097" :
                tvSKPL097.setText(qty);
                break;
            case "SKPL097a" :
                tvSKPL097a.setText(qty);
                break;
            case "SKPL098" :
                tvSKPL098.setText(qty);
                break;


            case "SKPL099" :
                tvSKPL099.setText(qty);
                break;
            case "SKPL100" :
                tvSKPL100.setText(qty);
                break;
            case "SKPL101" :
                tvSKPL101.setText(qty);
                break;
            case "SKPL102" :
                tvSKPL102.setText(qty);
                break;
            case "SKPL103" :
                tvSKPL103.setText(qty);
                break;
            case "SKPL104" :
                tvSKPL104.setText(qty);
                break;
            case "SKPL104a" :
                tvSKPL104a.setText(qty);
                break;
            case "SKPL105" :
                tvSKPL105.setText(qty);
                break;

            case "SKPL106" :
                tvSKPL106.setText(qty);
                break;
            case "SKPL107" :
                tvSKPL107.setText(qty);
                break;
            case "SKPL108" :
                tvSKPL108.setText(qty);
                break;
            case "SKPL109" :
                tvSKPL109.setText(qty);
                break;
            case "SKPL110" :
                tvSKPL110.setText(qty);
                break;

            case "SKPL110A" :
                tvSKPL110A.setText(qty);
                break;
            case "SKPL110J" :
                tvSKPL110J.setText(qty);
                break;
            case "SKPL110E" :
                tvSKPL110E.setText(qty);
                break;
            case "SKPL110B" :
                tvSKPL110B.setText(qty);
                break;
            case "SKPL110H" :
                tvSKPL110H.setText(qty);
                break;

            case "SKPL110F" :
                tvSKPL110F.setText(qty);
                break;
            case "SKPL110D" :
                tvSKPL110D.setText(qty);
                break;
            case "SKPL110I" :
                tvSKPL110I.setText(qty);
                break;
            case "SKPL110C" :
                tvSKPL110C.setText(qty);
                break;
            case "SKPL110G" :
                tvSKPL110G.setText(qty);
                break;
            //Sales Summery
            case "SKPL111" :
                tvSKPL111.setText(qtyF);
                break;
            case "SKPL116" :
                tvSKPL116.setText(qtyF);
                break;
            case "SKPL121" :
                tvSKPL121.setText(qtyF);
                break;

            case "SKPL112" :
                tvSKPL112.setText(qtyF);
                break;
            case "SKPL117" :
                tvSKPL117.setText(qtyF);
                break;
            case "SKPL122" :
                tvSKPL122.setText(qtyF);
                break;

            case "SKPL113" :
                tvSKPL113.setText(qtyF);
                summeryAmountList.put("SKPL113",fQty);
                break;
            case "SKPL118" :
                tvSKPL118.setText(qtyF);
                summeryAmountList.put("SKPL118",fQty);
                break;
            case "SKPL123" :
                tvSKPL123.setText(qtyF);
                summeryAmountList.put("SKPL123",fQty);
                break;

            //Density
            case "SKPL114" :
                tvSKPL114.setText(qty);
                break;
            case "SKPL115" :
                tvSKPL115.setText(qty);
                break;
            case "SKPL119" :
                tvSKPL119.setText(qty);
                break;
            case "SKPL120" :
                tvSKPL120.setText(qty);
                break;
            case "SKPL124" :
                tvSKPL124.setText(qty);
                break;
            case "SKPL125" :
                tvSKPL125.setText(qty);
                break;

            //Cash Released
            case "SKPL126" :
                tvSKPL126.setText(qtyF);
                cashReleasedList.put("SKPL126",fQty);
                break;
            case "SKPL127" :
                tvSKPL127.setText(qtyF);
                cashReleasedList.put("SKPL127",fQty);
                break;
            case "SKPL128" :
                tvSKPL128.setText(qtyF);
                cashReleasedList.put("SKPL128",fQty);
                break;
            case "SKPL129" :
                tvSKPL129.setText(qtyF);
                cashReleasedList.put("SKPL129",fQty);
                break;
            case "SKPL130" :
                tvSKPL130.setText(qtyF);
                cashReleasedList.put("SKPL130",fQty);
                break;

            //Credit Sales
            case "SKPL131" :
                tvSKPL131.setText(qtyF);
                creditSalesList.put("SKPL131",fQty);
                break;
            case "SKPL132" :
                tvSKPL132.setText(qtyF);
                creditSalesList.put("SKPL132",fQty);
                break;
            case "SKPL133" :
                tvSKPL133.setText(qtyF);
                creditSalesList.put("SKPL133",fQty);
                break;
            case "SKPL134" :
                tvSKPL134.setText(qtyF);
                creditSalesList.put("SKPL134",fQty);
                break;
            case "SKPL135" :
                tvSKPL135.setText(qtyF);
                creditSalesList.put("SKPL135",fQty);
                break;
            case "SKPL136" :
                tvSKPL136.setText(qtyF);
                creditSalesList.put("SKPL136",fQty);
                break;
            case "SKPL137" :
                tvSKPL137.setText(qtyF);
                creditSalesList.put("SKPL137",fQty);
                break;
            case "SKPL138" :
                tvSKPL138.setText(qtyF);
                creditSalesList.put("SKPL138",fQty);
                break;
            case "SKPL139" :
                tvSKPL139.setText(qtyF);
                creditSalesList.put("SKPL139",fQty);
                break;
            case "SKPL140" :
                tvSKPL140.setText(qtyF);
                creditSalesList.put("SKPL140",fQty);
                break;

            //Expenses
            case "SKPL141" :
                tvSKPL141.setText(qtyF);
                expensestList.put("SKPL141",fQty);
                break;
            case "SKPL142" :
                tvSKPL142.setText(qtyF);
                expensestList.put("SKPL142",fQty);
                break;
            case "SKPL143" :
                tvSKPL143.setText(qtyF);
                expensestList.put("SKPL143",fQty);
                break;
            case "SKPL144" :
                tvSKPL144.setText(qtyF);
                expensestList.put("SKPL144",fQty);
                break;
            case "SKPL145" :
                tvSKPL145.setText(qtyF);
                expensestList.put("SKPL145",fQty);
                break;
            case "SKPL146" :
                tvSKPL146.setText(qtyF);
                expensestList.put("SKPL146",fQty);
                break;

            //Denomination
            case "SKPL148" :
                tvSKPL148.setText(qty);
                denominationList.put("SKPL148",fQty);
                break;
            case "SKPL149" :
                tvSKPL149.setText(qty);
                denominationList.put("SKPL149",fQty);
                break;
            case "SKPL150" :
                tvSKPL150.setText(qty);
                denominationList.put("SKPL150",fQty);
                break;
            case "SKPL151" :
                tvSKPL151.setText(qty);
                denominationList.put("SKPL151",fQty);
                break;
            case "SKPL152" :
                tvSKPL152.setText(qty);
                denominationList.put("SKPL152",fQty);
                break;
            case "SKPL153" :
                tvSKPL153.setText(qty);
                denominationList.put("SKPL153",fQty);
                break;
            case "SKPL154" :
                tvSKPL154.setText(qty);
                denominationList.put("SKPL154",fQty);
                break;
            case "SKPL155" :
                tvSKPL155.setText(qty);
                denominationList.put("SKPL155",fQty);
                break;
            case "SKPL156" :
                tvSKPL156.setText(qty);
                denominationList.put("SKPL156",fQty);
                break;
            case "SKPL157" :
                tvSKPL157.setText(qty);
                denominationList.put("SKPL157",fQty);
                break;
            //Net Cash
            case "SKPL147" :
                final String q = qty;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        tvSKPL147.setText(getFormattedAmounts(q));
                    }
                });
                //tvSKPL147.setText(qty);
                break;




        }
    }


    View.OnClickListener selectDate = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            PopupOutletStockEntryDatePicker datePopup = new
                    PopupOutletStockEntryDatePicker(
                    PetrolBunkActivity.this, txtDate,
                    new PopupOutletStockEntryDatePicker.PopupListener() {
                        @Override
                        public void onPopupClosed() {
                            strSelectedDate = txtDate.getText().toString().trim();
                            Log.e("Selected",".........................................Date:" + strSelectedDate);
                        }
                    });
            datePopup.showPopup();
        }
    };

    private void tablesShow(){
        if(scrollViewTable != null){
            scrollViewTable.setVisibility(View.VISIBLE);
        }
    }

    private void tablesHide(){
        if(scrollViewTable != null){
            scrollViewTable.setVisibility(View.GONE);
        }
    }

    private void show(){
        lineHeaderProgress.setVisibility(View.VISIBLE);
    }

    private void hide(){
        lineHeaderProgress.setVisibility(View.GONE);
    }

    private void updateUserId(){
        if (Constants.DUSER_ID == null || Constants.DUSER_ID.isEmpty()) {
            if(databaseHandler == null) {
                databaseHandler =  new DatabaseHandler(this);
            }
            Cursor curs;
            curs = databaseHandler.getDetails();
            if(curs != null && curs.getCount() > 0) {
                curs.moveToFirst();
                Constants.DUSER_ID = curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_id));
                Log.e("Constants.DUSER_ID", Constants.DUSER_ID);
                curs.close();
            }else {
                showAlertDialogToast(getResources().getString(R.string.need_to_re_open));
            }
        }
    }

    public void showAlertDialogToast(String message) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(PetrolBunkActivity.this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }




























    private void createTables(){
        /*
        DAILY OPENING METER READING
         */
        tbl_DOMR = new TableLayout(this);
        tbl_DOMR.setLayoutParams(getTblLayoutParams());
        tbl_DOMR.setId(DOMR_TBL_Id);
        //Header
        LinkedHashMap<String, Float> items = new LinkedHashMap<>();
        items.put(getResources().getString(R.string.p_particulars),DOMR_table_column1_weight);
        items.put(getResources().getString(R.string.p_domr),DOMR_table_column2_weight);
        items.put(getResources().getString(R.string.dip),DOMR_table_column3_weight);
        addHeadersWithWeight(tbl_DOMR, items, DOMR_table_weight);
        //Data
        //LinkedHashMultimap.create();
        List<LinkedHashMap<String, Float>> listItems = new ArrayList<>();
        LinkedHashMap<String, Float> dataItems = new LinkedHashMap<>();
        dataItems.put(getResources().getString(R.string.petrol_pump1), DOMR_table_column1_weight);
        dataItems.put(getResources().getString(R.string.petrol_pump1), DOMR_table_column2_weight);
        dataItems.put(getResources().getString(R.string.petrol_pump1), DOMR_table_column3_weight);
        listItems.add(dataItems);
        addDataWithWeight(tbl_DOMR, listItems, DOMR_table_weight);

        //linearLayout.addView(tbl_DOMR);


    }

    /**
     * This function add the headers to the table
     **/
    public void addHeadersWithWeight(TableLayout tbl, HashMap<String, Float> items, float totalWeight) {

        TableRow tr = new TableRow(this);
        tr.setLayoutParams(getTblLayoutParamsWithWeight(totalWeight));
        for(Map.Entry<String,Float> entry : items.entrySet()){
            tr.addView(getTextView(0, entry.getKey(),
                    Color.WHITE, Typeface.BOLD, Color.BLUE,
                    getResources().getDimension(R.dimen.fourteen_sp)), getLayoutParamsWithWeight(entry.getValue()));
        }
        tbl.addView(tr);
    }

    public void addDataWithWeight(TableLayout tbl, List<LinkedHashMap<String, Float>> list, float totalWeight) {

        for(int i = 0; i < list.size(); i++) {
            LinkedHashMap<String, Float> items = list.get(i);
            TableRow tr = new TableRow(this);
            tr.setLayoutParams(getTblLayoutParamsWithWeight(totalWeight));
            for (Map.Entry<String, Float> entry : items.entrySet()) {
                tr.addView(getTextView(0, entry.getKey(),
                        Color.WHITE, Typeface.NORMAL, ContextCompat.getColor(this, R.color.gray),
                        getResources().getDimension(R.dimen.fourteen_sp)), getLayoutParamsWithWeight(entry.getValue()));
            }
            tbl.addView(tr);
        }
    }



    private TextView getTextView(int id, String title, int color, int typeface, int bgColor,float textSize) {
        TextView tv = new TextView(this);
        tv.setId(id);
        tv.setText(title.toUpperCase());
        tv.setTextColor(color);
        tv.setPadding(40, 40, 40, 40);
        tv.setTypeface(Typeface.DEFAULT, typeface);
        tv.setBackgroundColor(bgColor);
        tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        //tv.setLayoutParams(getLayoutParams());
        /////tv.setOnClickListener(this);
        return tv;
    }

    @NonNull
    private TableRow.LayoutParams getLayoutParams() {
        TableRow.LayoutParams params = new TableRow.LayoutParams(
                TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT);
        params.setMargins(2, 0, 0, 2);
        return params;
    }

    @NonNull
    private TableRow.LayoutParams getLayoutParamsWithWeight(float weight) {
        TableRow.LayoutParams params = new TableRow.LayoutParams(
                0,
                TableRow.LayoutParams.WRAP_CONTENT, weight);
        params.setMargins(2, 0, 0, 2);
        return params;
    }

    @NonNull
    private TableLayout.LayoutParams getTblLayoutParams() {
        return new TableLayout.LayoutParams(
                TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT);
    }

    @NonNull
    private TableLayout.LayoutParams getTblLayoutParamsWithWeight(float weight) {
        return new TableLayout.LayoutParams(
                TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT, weight);
    }

    /**
     * This function add the headers to the table
     **/
    public void addHeaders() {
        //TableLayout tl = findViewById(R.id.table);
        TableRow tr = new TableRow(this);
        tr.setLayoutParams(getLayoutParams());
        //tr.addView(getTextView(0, "COMPANY", Color.WHITE, Typeface.BOLD, Color.BLUE));
        //tr.addView(getTextView(0, "OS", Color.WHITE, Typeface.BOLD, Color.BLUE));
        //tl.addView(tr, getTblLayoutParams());
    }

    /**
     * This function add the data to the table
     **/
    public void addData(List<String> dataList) {
        int numCompanies = dataList.size();
       // TableLayout tl = findViewById(R.id.table);
        for (int i = 0; i < numCompanies; i++) {
            TableRow tr = new TableRow(this);
            tr.setLayoutParams(getLayoutParams());
            //tr.addView(getTextView(i + 1, dataList.get(i), Color.WHITE, Typeface.NORMAL, ContextCompat.getColor(this, R.color.colorAccent)));
           // tr.addView(getTextView(i + numCompanies, dataList.get(i), Color.WHITE, Typeface.NORMAL, ContextCompat.getColor(this, R.color.colorAccent)));
           // tl.addView(tr, getTblLayoutParams());
        }
    }
}
