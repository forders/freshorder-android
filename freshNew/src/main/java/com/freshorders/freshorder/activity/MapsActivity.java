package com.freshorders.freshorder.activity;

import android.database.Cursor;
import androidx.fragment.app.FragmentActivity;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import android.location.Location;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.model.locationModel;
import com.freshorders.freshorder.utils.allcontrols;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


import java.util.ArrayList;
import java.util.List;

import com.freshorders.freshorder.R;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class MapsActivity  extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private EditText editText;
    private TextView distancetext;

    DatabaseHandler databaseHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        databaseHandler = new DatabaseHandler(getApplicationContext());
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        editText = findViewById(R.id.datepicker);
        distancetext = findViewById(R.id.distance);
        allcontrols.setdatepicker(editText,MapsActivity.this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }

    public void buttonclick(View view) {
        List<locationModel> models = new ArrayList<>();
        Cursor cur = databaseHandler.getLocationsByDate(editText.getText().toString());
        if(cur != null && cur.getCount() > 0) {
            cur.moveToFirst();
            while (!cur.isAfterLast()) {
                byte[] blob = cur.getBlob(cur.getColumnIndex(DatabaseHandler.KEY_LOCATION_LOCATION));
                String json = new String(blob);
                Gson gson = new Gson();
                Location location = gson.fromJson(json,
                        new TypeToken<Location>() {}.getType());
                String date = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_LOCATION_DATE));
                String time = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_LOCATION_TIME));
                float accuracy = cur.getFloat(cur.getColumnIndex(DatabaseHandler.KEY_LOCATION_ACCURACY));
                locationModel model = new locationModel(location,date,time,accuracy);
                models.add(model);
                cur.moveToNext();///////////
            }
        }

        double distance = 0;
        //List<locationModel> models =  AppDatabase.getAppDatabase(MapsActivity.this).locationDao().findByDate(editText.getText().toString());

        mMap.clear();
        for ( int i =0;i<models.size();i++ )
        {
            if(i<models.size()-1)
            {
                Location start = models.get(i).getLocation();
                Location end = models.get(i + 1).getLocation();

                double temp_distance = start.distanceTo(end);
                distance += temp_distance;
            }
            // Creating a marker
            MarkerOptions markerOptions = new MarkerOptions();

            // Setting the position for the marker
            LatLng latLng = new LatLng(models.get(i).getLocation().getLatitude(),models.get(i).getLocation().getLongitude());
            markerOptions.position(latLng);

            markerOptions.title(latLng.latitude + " : " + latLng.longitude);

            mMap.addMarker(markerOptions);

        }
        distancetext.setText("Distance : "+distance);

    }
}

