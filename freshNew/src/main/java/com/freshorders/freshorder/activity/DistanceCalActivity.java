package com.freshorders.freshorder.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Build;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.accuracy.location.track.LocationTrackForegroundService;
import com.freshorders.freshorder.asyntask.GeneralAsyncTask;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.model.DateDiffModel;
import com.freshorders.freshorder.service.DistanceCalForegroundService;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.syncadapter.adapter.SyncAdapterManager;
import com.freshorders.freshorder.ui.SalesManOrderActivity;
import com.freshorders.freshorder.utils.Constants;
import com.freshorders.freshorder.utils.JsonServiceHandler;
import com.freshorders.freshorder.utils.Utils;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import org.json.JSONArray;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class DistanceCalActivity extends AppCompatActivity {

    private static final String TAG = DistanceCalActivity.class.getSimpleName();
    private int REQUEST_PERMISSIONS_REQUEST_CODE = 101;

    SyncAdapterManager syncAdapterManager;

    ProgressDialog dialog;
    private String bugPath;
    Button btnStart;
    Button btnStop;
    TextView tvShowDistance;

    public DatabaseHandler databaseHandler;
    private LinearLayout linearProgress;
    double preDistance;

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distance_cal);
        btnStart = findViewById(R.id.request_location_updates_button);
        btnStop = findViewById(R.id.remove_location_updates_button);
        tvShowDistance = findViewById(R.id.location_TV_dis_cal);
        linearProgress = findViewById(R.id.lineHeaderProgress_manager_live_inner);

        bugPath = new PrefManager(getApplicationContext()).getStringDataByKey(PrefManager.KEY_BUG_FILE_PATH);

        if(PrefManager.getStatus(DistanceCalActivity.this)){
            btnStart.setVisibility(View.INVISIBLE);
            btnStop.setVisibility(View.VISIBLE);
        }else {
            btnStart.setVisibility(View.VISIBLE);
            btnStop.setVisibility(View.INVISIBLE);
        }

        //////////////////////////////////////////////

        syncAdapterManager = new SyncAdapterManager(DistanceCalActivity.this.getApplicationContext());
        //syncAdapterManager.beginPeriodicSync(60);
        syncAdapterManager.initializePeriodicSync();

        ////////////////////////////////////////////////

        TedPermission.with(this)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {

                    }

                    @Override
                    public void onPermissionDenied(List<String> deniedPermissions) {

                    }
                })
                .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                .setPermissions(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION)
                .check();
    }

    public void startClick(View view) {

        btnStart.setVisibility(View.INVISIBLE);
        btnStop.setVisibility(View.VISIBLE);
        try {
            PrefManager.setStatus(DistanceCalActivity.this,true);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(new Intent(DistanceCalActivity.this, LocationTrackForegroundService.class));
            }
            else
            {
                startService(new Intent(DistanceCalActivity.this,LocationTrackForegroundService.class));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopClick(View view) {

        btnStart.setVisibility(View.VISIBLE);
        btnStop.setVisibility(View.INVISIBLE);
        try {
            PrefManager.setStatus(DistanceCalActivity.this,false);
            stopService(new Intent(DistanceCalActivity.this,LocationTrackForegroundService.class));
            Log.e(TAG,"....................stop called");

            //syncAdapterManager.syncImmediately();


            /*
            dialog = ProgressDialog.show(DistanceCalActivity.this, "", "Loading....", true, false);

            new LocationTrackAsyncTask().sendEveryPoints(new DatabaseHandler(DistanceCalActivity.this.getApplicationContext()),this,
                    new LocationTrackAsyncTask.LocationCompleteListener() {

                        @Override
                        public void onComplete(Result result) {
                            if(dialog != null) {
                                dialog.dismiss();
                            }
                            if(result != null){

                                switch (result.getStatusCode()){
                                    case 1:
                                        Log.e(TAG,".................."+result.getMsg());
                                        break;
                                    case 0:
                                        Log.e(TAG,".................."+result.getMsg());
                                        break;
                                    case -1:
                                        Log.e(TAG,".................."+result.getMsg());
                                        break;
                                }
                            }
                        }
                    });  */
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showlocation(View view) {
        startActivity(new Intent(DistanceCalActivity.this,LocationList.class));
    }
    public void showerror(View view) {
        startActivity(new Intent(DistanceCalActivity.this, LogList.class));
    }

    public void showpoints(View view) {
        startActivity(new Intent(DistanceCalActivity.this, MapsActivity.class));
    }

    public void checking(View view) {
        syncAdapterManager.syncImmediately();
        //DistanceUtils.startPowerSaverIntent(DistanceCalActivity.this);
    }

    public void homeClick(View view) {
        startActivity(new Intent(DistanceCalActivity.this, SalesManOrderActivity.class));
    }

    public void disCalClick(View view) {
        getSingleSalesmanTravelledDistance();
    }


    private void getSingleSalesmanTravelledDistance(){
        String userId = "";
        if(databaseHandler == null) {
            databaseHandler =  DatabaseHandler.getInstance(DistanceCalActivity.this);//new DatabaseHandler(getContext());
        }

        if (Constants.USER_ID == null || Constants.USER_ID.isEmpty()) {
            Cursor curs;
            curs = databaseHandler.getDetails();
            if (curs != null && curs.getCount() > 0) {
                curs.moveToFirst();
                Constants.USER_ID = curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_id));
                Log.e("Constants.USER_ID", Constants.USER_ID);
                userId = Constants.USER_ID;
                curs.close();
            }else {
                showAlertDialogToast(getResources().getString(R.string.dealer_not_proper_load));
            }
        }else {
            userId = Constants.USER_ID;
        }

        if (Constants.DUSER_ID == null || Constants.DUSER_ID.isEmpty()) {
            Cursor curs;
            curs = databaseHandler.getdealer();
            if(curs != null && curs.getCount() > 0) {
                curs.moveToFirst();
                Constants.DUSER_ID = curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_Duserid));
                Log.e("Constants.DUSER_ID", Constants.DUSER_ID);
                curs.close();
            }else {
                showAlertDialogToast(getResources().getString(R.string.dealer_not_proper_load));
            }
        }

        JSONObject model;
        String formattedDate = "";
        Date c = Calendar.getInstance().getTime();
        try {
                SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                formattedDate = format1.format(c);
                Log.e("Date",".........................................." + formattedDate);

            model = new JSONObject();
            model.put("duserid", Constants.DUSER_ID);
            model.put("date", formattedDate);
            model.put("suserid", userId);

        }catch (Exception e){
            e.printStackTrace();
            showAlertDialogToast("Some Think Wrong In App");
            return;
        }

        innerShow();

        JsonServiceHandler jsonServiceHandler = new JsonServiceHandler(Utils.salesmanLive,model.toString() , DistanceCalActivity.this);
        GeneralAsyncTask obj = new GeneralAsyncTask(jsonServiceHandler,
                new GeneralAsyncTask.CompleteListener() {
                    @Override
                    public void onSuccess(JSONObject result) {
                        /////hide();
                        try {
                            if (result != null) {
                                if (result.has("data")) {
                                    try {
                                        PolylineOptions options = new PolylineOptions().width(5).color(Color.BLUE).geodesic(true);
                                        JSONArray jsonArray = result.getJSONArray("data");
                                        if(jsonArray.length() > 0){
                                            double totalDistance = 0d;
                                            JSONObject jsonResult = jsonArray.getJSONObject(0);
                                            if(jsonResult != null && jsonResult.length() > 0){
                                                String strUserId = jsonResult.getString("userid");
                                                String strName = jsonResult.getString("fullname");
                                                String strMobile = jsonResult.getString("mobileno");

                                                if(jsonResult.has("traceroute")){
                                                    JSONArray jsonArray1 = jsonResult.getJSONArray("traceroute");
                                                    if(jsonArray1 != null && jsonArray1.length() > 0){
                                                        for(int i = 0; i < jsonArray1.length(); i++){
                                                            JSONObject obj = jsonArray1.getJSONObject(i);
                                                            double lat = obj.getDouble("geolat");
                                                            double lon = obj.getDouble("geolong");
                                                            double distance = obj.getDouble("distance");
                                                            Log.e(TAG,"......................CurrentDistance:: " + i +" :: " + distance);
                                                            totalDistance = totalDistance + distance;

                                                        }
                                                        innerHide();
                                                        setTraveledDistance(totalDistance);

                                                    }else {
                                                        showAlertDialogToast("No positions recorded yet");
                                                    }
                                                }else {
                                                    showAlertDialogToast("No positions recorded yet");
                                                }

                                            }else {
                                                showAlertDialogToast("No positions recorded yet");
                                            }
                                        }else {
                                            showAlertDialogToast("No positions recorded yet");
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        innerHide();
                                        showAlertDialogToast("Internal problem occur:: " + e.getMessage());
                                    }
                                }else {
                                    showAlertDialogToast("No data from cloud server");
                                }
                            }else {
                                showAlertDialogToast("No data from cloud server");
                            }
                            innerHide();
                        } catch (Exception e) {
                            e.printStackTrace();
                            showAlertDialogToast("Internal problem occur:: " + e.getMessage());
                        }finally {
                            innerHide();
                        }
                    }
                    @Override
                    public void onFailed(String msg) {
                        innerHide();
                        showAlertDialogToast(msg);
                    }

                    @Override
                    public void onException(String msg) {
                        innerHide();
                        showAlertDialogToast(msg);
                    }

                });
        obj.execute();
    }

    private void setTraveledDistance(double totalDistance) {
        if(tvShowDistance != null){
            double distance = BigDecimal.valueOf(totalDistance)
                    .setScale(1, RoundingMode.HALF_UP)
                    .doubleValue();
            String strDistance = String.valueOf(distance);
            String temp = "Travelled :";
            temp = temp + " " + strDistance + " KM(s)";
            tvShowDistance.setText(temp);
            tvShowDistance.setVisibility(View.VISIBLE);
        }
    }


    public void showAlertDialogToast(String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(DistanceCalActivity.this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    private void innerShow(){
        if(linearProgress != null){
            int visible = linearProgress.getVisibility();
            if( visible == View.INVISIBLE || visible == View.GONE){
                linearProgress.setVisibility(View.VISIBLE);
            }
        }
    }

    private void innerHide(){
        if(linearProgress != null){
            int visible = linearProgress.getVisibility();
            if( visible == View.VISIBLE){
                linearProgress.setVisibility(View.GONE);
            }
        }
    }


}
