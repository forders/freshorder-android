package com.freshorders.freshorder.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import androidx.annotation.NonNull;

import com.freshorders.freshorder.attendance.activity.BioAttendance;
import com.freshorders.freshorder.service.PrefManager;
import com.freshorders.freshorder.utils.Constants;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.fragment.ManagerClientVisitFragment;
import com.freshorders.freshorder.fragment.ManagerLiveVisitFragment;
import com.freshorders.freshorder.fragment.ManagerMarketVisitPlanFragment;
import com.freshorders.freshorder.fragment.ManagerSalesmanFragment;
import com.freshorders.freshorder.ui.SigninActivity;

public class ManagerActivity extends AppCompatActivity implements LifecycleObserver {

    private static final String FRAGMENT_ID_KEY = "fragment_id_key";
    private static final String TAG = ManagerActivity.class.getSimpleName();
    private DrawerLayout mDrawerLayout;
    private Toolbar toolbar;
    private LinearLayout lineHeaderProgress ;
    private Fragment fragment;

    private BottomNavigationView navigation;

    public static int navItemIndex = 0;
    public static int bottomNavItemId = 0;
    private NavigationView navigationView;

    DatabaseHandler databaseHandler;

    private int home_page;
    private String bugPath;

    private boolean hasUserReturned = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager);

        // register observer
        ProcessLifecycleOwner.get().getLifecycle().addObserver(new ManagerActivity());/////////////////////////////////////////
        bugPath = new PrefManager(this).getStringDataByKey(PrefManager.KEY_BUG_FILE_PATH);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setHomeAsUpIndicator(R.drawable.ic_menu_green_24dp);
            ab.setDisplayHomeAsUpEnabled(true);
        }

        databaseHandler = new DatabaseHandler(getApplicationContext());

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        // initializing navigation menu
        setUpNavigationView();
        //load bottom navigation
        loadBottomNavView();
        home_page = R.id.nav_client_visit;
        navigation.setSelectedItemId(home_page);///for default select
    }

    private void loadBottomNavView(){
        lineHeaderProgress = findViewById(R.id.lineHeaderProgress_home);

        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
                /*               /////////////////////
        // attaching bottom sheet behaviour - hide / show on scroll
        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) navigation.getLayoutParams();
        layoutParams.setBehavior(new BottomNavigationBehavior());
        */
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            //Fragment fragment;
            switch (item.getItemId()) {
                /*case R.id.nav_dash_board:
                    fragment = new ManagerDashBoardFragment();
                    loadFragment(fragment);
                    return true;  */
                case R.id.nav_client_visit:
                    bottomNavItemId = R.id.nav_client_visit;
                    fragment = new ManagerClientVisitFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.nav_sales_man_visit:
                    bottomNavItemId = R.id.nav_sales_man_visit;
                    fragment = new ManagerSalesmanFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.nav_market_visit:
                    bottomNavItemId = R.id.nav_market_visit;
                    fragment = new ManagerMarketVisitPlanFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.nav_live_visit:
                    bottomNavItemId = R.id.nav_live_visit;
                    fragment = new ManagerLiveVisitFragment();
                    loadFragment(fragment);
                    return true;
                /*case R.id.nav_target_management:
                    fragment = new ManagerMarketVisitPlanFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.nav_task_management:
                    fragment = new ManagerWorkManagementFragment();
                    loadFragment(fragment);
                    return true;  */
            }
            return false;
        }
    };

    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.nav_home_page:
                        navItemIndex = 0;
                        //startActivity(new Intent(MainActivity.this, HomeNavigationActivity.class));
                        mDrawerLayout.closeDrawers();
                        navigation.setSelectedItemId(home_page);
                        return true;
                    case R.id.nav_logout_page:
                        navItemIndex = 0;
                        mDrawerLayout.closeDrawers();
                        logoutPage();
                        return true;
                    case R.id.nav_petrol_page:
                        navItemIndex = 0;
                        mDrawerLayout.closeDrawers();
                        startActivity(new Intent(ManagerActivity.this, PetrolBunkActivity.class));
                        return true;
                    case R.id.nav_daily_sales_report_page:
                        navItemIndex = 0;
                        mDrawerLayout.closeDrawers();
                        startActivity(new Intent(ManagerActivity.this, DailySalesReportActivity.class));
                        return true;
                    case R.id.nav_periodic_report_page:
                        navItemIndex = 0;
                        mDrawerLayout.closeDrawers();
                        startActivity(new Intent(ManagerActivity.this, PeriodicReportActivity.class));
                        return true;
                    case R.id.nav_team_status_page:
                        navItemIndex = 0;
                        mDrawerLayout.closeDrawers();
                        startActivity(new Intent(ManagerActivity.this, TeamStatusActivity.class));
                        return true;

                    case R.id.nav_my_status_page:
                        navItemIndex = 0;
                        mDrawerLayout.closeDrawers();
                        startActivity(new Intent(ManagerActivity.this, MyTaskActivity.class));
                        return true;

                    case R.id.nav_attendance:
                        navItemIndex = 0;
                        mDrawerLayout.closeDrawers();
                        startActivity(new Intent(ManagerActivity.this, BioAttendance.class));
                        //startActivity(new Intent(ManagerActivity.this, AttendanceFace.class));
                        return true;
                    /*case R.id.nav_profile_page:
                        navItemIndex = 1;
                        //CURRENT_TAG = TAG_PHOTOS;
                        //startActivity(new Intent(MainActivity.this, WorkAssignmentActivity.class));
                        //CURRENT_TAG = TAG_MOVIES;
                        mDrawerLayout.closeDrawers();
                        return true;
                    case R.id.nav_previous_test_page:
                        navItemIndex = 2;
                        //CURRENT_TAG = TAG_PHOTOS;
                        //startActivity(new Intent(MainActivity.this, WorkAssignmentEditActivity.class));
                        //CURRENT_TAG = TAG_MOVIES;
                        mDrawerLayout.closeDrawers();
                        return true;
                    case R.id.nav_notification_page:
                        navItemIndex = 3;
                        //startActivity(new Intent(MainActivity.this, AddOutPersonActivity.class));
                        //CURRENT_TAG = TAG_MOVIES;
                        mDrawerLayout.closeDrawers();
                        return true;
                    case R.id.nav_membership_page:
                        navItemIndex = 4;
                        //startActivity(new Intent(MainActivity.this, EditOutPersonActivity.class));
                        //CURRENT_TAG = TAG_MOVIES;
                        mDrawerLayout.closeDrawers();
                        return true;
                    case R.id.nav_settings_page:
                        navItemIndex = 4;
                        //startActivity(new Intent(MainActivity.this, SettingActivity.class));
                        //CURRENT_TAG = TAG_MOVIES;
                        mDrawerLayout.closeDrawers();
                        return true;
                    case R.id.nav_feedback_page:
                        navItemIndex = 5;
                        //startActivity(new Intent(MainActivity.this, FeedBackActivity.class));
                        //CURRENT_TAG = TAG_MOVIES;
                        mDrawerLayout.closeDrawers();
                        return true;
                    case R.id.nav_about_us_page:
                        // launch new intent instead of loading fragment
                        //startActivity(new Intent(MainActivity.this, AboutUsActivity.class));
                        mDrawerLayout.closeDrawers();
                        return true;
                    case R.id.nav_privacy_policy_page:
                        // launch new intent instead of loading fragment
                        //startActivity(new Intent(MainActivity.this, ServerActivity.class));
                        mDrawerLayout.closeDrawers();
                        return true;  */
                    default:
                        navItemIndex = 0;
                }
                return true;
            }
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        ///////////////////////////////////////////mDrawerLayout.addDrawerListener(actionBarDrawerToggle);/////////
        mDrawerLayout.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container_attendance, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void logoutPage(){
        databaseHandler.delete();
        Intent io = new Intent(ManagerActivity.this, SigninActivity.class);
        startActivity(io);
        finish();
    }

    @Override
    public void onBackPressed() {
        /*if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }  */
        /////////////
        if(navigation != null) {
            navigation.setSelectedItemId(home_page);///for default select
        }
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        android.util.Log.e(TAG,"onRestoreInstanceState Called....................");
        bottomNavItemId = savedInstanceState.getInt(FRAGMENT_ID_KEY);
        new com.freshorders.freshorder.utils.Log(this).ee(TAG,"..................bottomNavItemId"+bottomNavItemId);
        /////////////navigation.setSelectedItemId(bottomNavItemId);
        ////////////loadFragmentFromBackground();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if(bottomNavItemId == 0){
            bottomNavItemId = R.id.nav_client_visit;
        }
        outState.putInt(FRAGMENT_ID_KEY, bottomNavItemId);
        // call superclass to save any view hierarchy
        super.onSaveInstanceState(outState);
    }




    private void loadFragmentFromBackground(){
        switch (bottomNavItemId) {

            case R.id.nav_client_visit:
                fragment = new ManagerClientVisitFragment();
                loadFragment(fragment);
                break;
            case R.id.nav_sales_man_visit:
                fragment = new ManagerSalesmanFragment();
                loadFragment(fragment);
                break;
            case R.id.nav_market_visit:
                fragment = new ManagerMarketVisitPlanFragment();
                loadFragment(fragment);
                break;
            case R.id.nav_live_visit:
                fragment = new ManagerLiveVisitFragment();
                loadFragment(fragment);
                break;
                /*case R.id.nav_target_management:
                    fragment = new ManagerMarketVisitPlanFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.nav_task_management:
                    fragment = new ManagerWorkManagementFragment();
                    loadFragment(fragment);
                    return true;  */
                default:
                    fragment = new ManagerClientVisitFragment();
                    loadFragment(fragment);
                    break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //updateUserId();
    }

    private void updateUserId(){
        if (Constants.DUSER_ID == null || Constants.DUSER_ID.isEmpty()) {
            if(databaseHandler == null) {
                databaseHandler =  new DatabaseHandler(this.getApplicationContext());//DatabaseHandler.getInstance(mContext.getApplicationContext());//new DatabaseHandler(getContext());
            }
            Cursor curs;
            curs = databaseHandler.getDetails();
            if(curs != null && curs.getCount() > 0) {
                curs.moveToFirst();
                Constants.DUSER_ID = curs.getString(curs.getColumnIndex(DatabaseHandler.KEY_id));
                Log.e("Constants.DUSER_ID", Constants.DUSER_ID);
                curs.close();
            }else {
                showAlertDialogToast(getResources().getString(R.string.need_to_re_open));
            }
        }
    }

    public void showAlertDialogToast(String message) {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setCancelable(true);
        builder1.setMessage(message);
        builder1.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

        Button buttonbackground1 = alert11.getButton(DialogInterface.BUTTON_POSITIVE);
        buttonbackground1.setBackgroundColor(Color.parseColor("#A9E2F3"));

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void onMoveToForeground() {
        // app moved to foreground
        //new com.freshorders.freshorder.utils.Log(bugPath).ee(TAG,"..................ON_START");
        Log.e(TAG,"..................ON_START");
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    public void fromPause() {
        // app moved to foreground
        if(hasUserReturned){
            //recreate();
        }
        Log.e(TAG,"..................ON_RESUME");
        //new com.freshorders.freshorder.utils.Log(bugPath).ee(TAG,"..................ON_RESUME");
        ////this.recreate();
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onMoveToBackground() {
        // app moved to background
        //new com.freshorders.freshorder.utils.Log(bugPath).ee(TAG,"..................ON_STOP");
        hasUserReturned = true;
        Log.e(TAG,"..................ON_STOP");
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    public void releaseCamera() {
        ////hasUserReturned = true;
        //new com.freshorders.freshorder.utils.Log(bugPath).ee(TAG,"..................ON_PAUSE");
        Log.e(TAG,"..................ON_PAUSE");
    }

}
