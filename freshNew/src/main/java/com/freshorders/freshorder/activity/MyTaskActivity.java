package com.freshorders.freshorder.activity;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.adapter.TeamStatusAdapter;
import com.freshorders.freshorder.adapter.TeamStatusDetailAdapter;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.model.MyTeamStatusDetailModel;
import com.freshorders.freshorder.model.TeamStatusModel;
import com.freshorders.freshorder.popup.PopupMyTask;
import com.freshorders.freshorder.popup.PopupNewTask;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.List;

import static android.widget.LinearLayout.VERTICAL;

public class MyTaskActivity extends AppCompatActivity {

    public static final String TAG = MyTaskActivity.class.getSimpleName();
    private DatabaseHandler databaseHandler;

    public interface MyBSheet{
        public void show(String msg);
        public void hide();
    }

    private LinearLayout lineHeaderProgress ;
    private Toolbar toolbar;

    private RecyclerView recyclerview;
    private TeamStatusDetailAdapter rvAdapter;
    private List<MyTeamStatusDetailModel> listItems;
    private TeamStatusModel currentItem;
    private View rootView;

    LinearLayout layoutBottomSheet;
    private BottomSheetBehavior sheetBehavior;
    private Button btnBottomSheet;
    private TextView tvBottomSheet;


    private DisplayMetrics displayMetrics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_task);

        TeamStatusModel get = (TeamStatusModel) getIntent().getSerializableExtra("MyTask");
        listItems = new ArrayList<>();
        currentItem = new TeamStatusModel();
        listItems.addAll(get.getMyTeamStatusDetailModels());
        currentItem = get;


        toolbar = (Toolbar) findViewById(R.id.id_toolbar_team_task);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);
            ab.setDisplayHomeAsUpEnabled(true);

            ab.setDisplayShowHomeEnabled(true);
            ab.setTitle(getResources().getString(R.string.work_status));
        }
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // perform whatever you want on back arrow click
                Log.e(TAG,"...back arrow clicked");
                moveToHomeActivity();
            }
        });

        initialSetting();

        setRecyclerView();////////////////


        displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
    }

    private void initialSetting(){
        TextView tvWorkerName = findViewById(R.id.id_work_status_worker_name);
        tvWorkerName.setText(currentItem.getWorkerName());

        MaterialButton mBtnAddNewTask = findViewById(R.id.id_work_status_M_icon_btn_add_new_task);
        mBtnAddNewTask.setOnClickListener(click);
    }

    private void setRecyclerView(){

        recyclerview = (RecyclerView) findViewById(R.id.id_Team_Task_RV);
        LinearLayoutManager layoutManager = new LinearLayoutManager(MyTaskActivity.this);
        recyclerview.setLayoutManager(layoutManager);
        //Add Divider
        DividerItemDecoration itemDecor = new DividerItemDecoration(recyclerview.getContext(), VERTICAL);
        recyclerview.addItemDecoration(itemDecor);
        //creating recyclerView adapter
        rvAdapter = new TeamStatusDetailAdapter(MyTaskActivity.this, currentItem, listItems,rootView, new MyTaskActivity.MyBSheet() {
            @Override
            public void show(String msg) {
                showBottomSheet(msg);
            }

            @Override
            public void hide() {
                collapseBottomSheet();
            }
        });
        recyclerview.setItemAnimator(new DefaultItemAnimator());
        //setting adapter to recyclerView
        recyclerview.setAdapter(rvAdapter);
    }

    private void moveToHomeActivity(){
        Intent intent = new Intent(MyTaskActivity.this, TeamStatusActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private void showBottomSheet(String str){
        if(sheetBehavior != null) {
            if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                tvBottomSheet.setText(str);
            }else{
                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                tvBottomSheet.setText(str);
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }
        }
    }
    private void collapseBottomSheet(){
        if(sheetBehavior != null) {
            if (sheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        }
    }


    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            Intent toActivity = new Intent(MyTaskActivity.this, AddNewTask.class);
            startActivity(toActivity);
        }
    };

}
