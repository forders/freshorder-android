package com.freshorders.freshorder.activity;

import android.database.Cursor;
import android.location.Location;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.List;

import com.freshorders.freshorder.R;
import com.freshorders.freshorder.db.DatabaseHandler;
import com.freshorders.freshorder.model.locationModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class LocationList extends AppCompatActivity {

    ListView listView;
    customadapter arrayAdapter;
    List<locationModel> locationdata = new ArrayList<>();

    DatabaseHandler databaseHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview);
        databaseHandler = new DatabaseHandler(getApplicationContext());
        listView = findViewById(R.id.listview);

        refresh();
    }

    private void refresh() {
        Cursor cur = databaseHandler.getAllLocation();
        if(cur != null && cur.getCount() > 0) {
            cur.moveToFirst();
            while (!cur.isAfterLast()) {
                byte[] blob = cur.getBlob(cur.getColumnIndex(DatabaseHandler.KEY_LOCATION_LOCATION));
                String json = new String(blob);
                Gson gson = new Gson();
                Location location = gson.fromJson(json,
                                            new TypeToken<Location>() {}.getType());
                int srno = cur.getInt(cur.getColumnIndex(DatabaseHandler.KEY_LOCATION_SERIAL_NO));
                String date = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_LOCATION_DATE));
                String time = cur.getString(cur.getColumnIndex(DatabaseHandler.KEY_LOCATION_TIME));
                float accuracy = cur.getFloat(cur.getColumnIndex(DatabaseHandler.KEY_LOCATION_ACCURACY));
                locationModel model = new locationModel(location,date,time,accuracy);
                model.setSrno(srno);
                locationdata.add(model);
                cur.moveToNext();
            }

        }
        ///////locationdata = AppDatabase.getAppDatabase(LocationList.this).locationDao().getAll();
        arrayAdapter = new customadapter(LocationList.this,locationdata);
        listView.setAdapter(arrayAdapter);
    }

    public void refresh(View view) {
        refresh();
    }

    private class customadapter extends BaseAdapter {
        Context context;
        List<locationModel> locationdata;
        customadapter(Context context, List<locationModel> locationdata) {
            this.context = context;
            this.locationdata = locationdata;
        }

        @Override
        public int getCount() {
            return locationdata.size();
        }

        @Override
        public Object getItem(int position) {
            return locationdata.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = getLayoutInflater().inflate(R.layout.location_item,null);
            TextView date = convertView.findViewById(R.id.date);
            TextView time = convertView.findViewById(R.id.time);
            TextView srno = convertView.findViewById(R.id.srno);
            TextView lat = convertView.findViewById(R.id.lat);
            TextView lng = convertView.findViewById(R.id.lng );
            TextView accuracy = convertView.findViewById(R.id.accuracy);

            date.setText("Date : "+locationdata.get(position).getDate());
            time.setText("Time : "+locationdata.get(position).getTime());
            srno.setText("Srno : "+locationdata.get(position).getSrno());
            lat.setText(""+locationdata.get(position).getLocation().getLatitude());
            lng.setText(","+locationdata.get(position).getLocation().getLongitude());
            accuracy.setText("Accuracy : "+locationdata.get(position).getAccuracy());

            return convertView;
        }
    }
}
